var fs = require('fs');

module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    var buildDir = "dist";

    var initConfig = {
        clean: {
            dist: 'dist/*.*'
        },
        concat: {
            definition: {
                dest: 'dist/ipushpull.d.ts',
                src: [
                    'js/**/*.d.ts',
                ]
            }
            // standalone: {
            //     dest: 'dist/ipushpull-standalone.js',
            //     src: [
            //         'node_modules/node-forge/dist/forge.min.js',
            //         'node_modules/socket.io-client/dist/socket.io.js',
            //         'dist/ipushpull.js'
            //     ]
            // }
        },
        webpack: require('./webpack.config.js')
    };

    // init config
    grunt.initConfig(initConfig);

    // npm tasks
    // grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-contrib-uglify');
    // grunt.loadNpmTasks('grunt-contrib-concat');
    // grunt.loadNpmTasks('grunt-contrib-clean');
    // grunt.loadNpmTasks('grunt-browserify');
    // grunt.loadNpmTasks('grunt-webpack');

    // register default
    grunt.registerTask('default', ['build']);

    // initial build
    grunt.registerTask('build', 'Run webpack and bundle the source', ['clean', 'webpack', 'concat']);

};