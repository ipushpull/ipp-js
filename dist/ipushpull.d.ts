import * as EventEmitter from "wolfy87-eventemitter";
declare class Api extends EventEmitter {
    constructor();
    foo(): string;
}
export default Api;
declare class Classy {
    constructor();
    hasClass(ele: HTMLElement, cls: string): boolean;
    addClass(ele: HTMLElement, cls: string): boolean;
    removeClass(ele: HTMLElement, cls: string): boolean;
}
export default Classy;
export interface IIPPConfig {
    api_url?: string;
    ws_url?: string;
    api_key: string;
    api_secret: string;
    transport?: string;
    storage_prefix?: string;
}
export declare class Config {
    private _config;
    set(config: IIPPConfig): void;
    readonly api_url: string;
    readonly ws_url: string;
    readonly api_key: string;
    readonly api_secret: string;
    readonly transport: string;
    readonly storage_prefix: string;
    $get(): IIPPConfig;
}
export interface IPageContentLink {
    external: boolean;
    address: string;
}
export interface IPageCellStyle {
    "background-color"?: string;
    "color"?: string;
    "font-family"?: string;
    "font-size"?: string;
    "font-style"?: string;
    "font-weight"?: string;
    "height"?: string;
    "number-format"?: string;
    "text-align"?: string;
    "text-wrap"?: string;
    "width"?: string;
    "tbs"?: string;
    "rbs"?: string;
    "bbs"?: string;
    "lbs"?: string;
    "tbc"?: string;
    "rbc"?: string;
    "bbc"?: string;
    "lbc"?: string;
    "tbw"?: string;
    "rbw"?: string;
    "bbw"?: string;
    "lbw"?: string;
}
export interface IPageContentCell {
    value: string;
    formatted_value?: string;
    link?: IPageContentLink;
    style?: IPageCellStyle;
    dirty?: boolean;
}
export interface IPageContent extends Array<any> {
    [index: number]: IPageContentCell[];
}
export interface IPageDeltaContentCol {
    col_index: number;
    cell_content: IPageContentCell;
}
export interface IPageDeltaContentRow {
    row_index: number;
    cols: IPageDeltaContentCol[];
}
export interface IPageDelta {
    new_rows: number[];
    new_cols: number[];
    content_delta: IPageDeltaContentRow[];
}
export interface IPageContentProvider {
    canDoDelta: boolean;
    update: (rawContent: IPageContent, isCurrent: boolean) => void;
    reset: () => void;
    getCell: (rowIndex: number, columnIndex: number) => IPageContentCell;
    updateCell: (rowIndex: number, columnIndex: number, data: IPageContentCell) => void;
    addRow: () => void;
    addColumn: () => void;
    removeRow: () => void;
    removeColumn: () => void;
    getDelta: () => IPageDelta;
    getFull: () => IPageContent;
    cleanDirty: () => void;
}
export declare class PageContent implements IPageContentProvider {
    canDoDelta: boolean;
    private _original;
    private _current;
    private _newRows;
    private _newCols;
    private _utils;
    readonly current: IPageContent;
    constructor(rawContent: IPageContent);
    update(rawContent: IPageContent, isCurrent: boolean): void;
    reset(): void;
    getCell(rowIndex: number, columnIndex: number): IPageContentCell;
    updateCell(rowIndex: number, columnIndex: number, data: IPageContentCell): void;
    addRow(index?: number): IPageContentCell[];
    addColumn(index?: number): void;
    removeRow(): void;
    removeColumn(): void;
    getDelta(): IPageDelta;
    getFull(): IPageContent;
    cleanDirty(): void;
}
declare class Emitter {
    listeners: any;
    onListeners: any;
    constructor();
    on: (name: string, callback: any) => void;
    off: (name: string, callback: any) => void;
    register: (callback: any) => void;
    unRegister(callback: any): void;
    emit(name: string, args: any): void;
}
export default Emitter;
declare class Helpers {
    constructor();
    validHex(hex: string): boolean;
    rgbToHex(rgb: string): string;
    componentToHex(c: any): string;
    addEvent(element: HTMLElement, eventName: string, func: any): void;
    capitalizeFirstLetter(string: string): string;
    removeEvent(element: HTMLElement, eventName: string, func: any): void;
    isTouch(): boolean;
    clickEvent(): string;
    openWindow(link: string, target?: string, params?: {
        [s: string]: string;
    }): Window;
    createSlug(str: string): string;
    getScrollbarWidth(): number;
}
export default Helpers;
export interface IPageProvider {
    start: () => void;
    stop: () => void;
    destroy: () => void;
}
export declare class ProviderREST extends EventEmitter implements IPageProvider {
    private _pageId;
    private _folderId;
    private _stopped;
    private _requestOngoing;
    private _timer;
    private _timeout;
    private _seqNo;
    seqNo: number;
    constructor(_pageId?: number, _folderId?: number);
    start(): void;
    stop(): void;
    destroy(): void;
    private startPolling();
    private load(ignoreSeqNo?);
    private tempGetContentOb(data);
    private tempGetSettingsOb(data);
}
export declare class ProviderSocket extends EventEmitter implements IPageProvider {
    private _pageId;
    private _folderId;
    static readonly SOCKET_EVENT_PAGE_ERROR: string;
    static readonly SOCKET_EVENT_PAGE_CONTENT: string;
    static readonly SOCKET_EVENT_PAGE_PUSH: string;
    static readonly SOCKET_EVENT_PAGE_SETTINGS: string;
    static readonly SOCKET_EVENT_PAGE_DATA: string;
    static readonly SOCKET_EVENT_PAGE_USER_JOINED: string;
    static readonly SOCKET_EVENT_PAGE_USER_LEFT: string;
    private _stopped;
    private _socket;
    private _wsUrl;
    constructor(_pageId?: number, _folderId?: number);
    start(): void;
    stop(): void;
    destroy(hard?: boolean): void;
    private init();
    private connect();
    private onConnect;
    private onReconnectError;
    private onDisconnect;
    private onRedirect;
    private onPageContent;
    private onPageSettings;
    private onPageError;
    private onOAuthError;
    private onAuthRefresh;
    private supportsWebSockets;
}
import { IIPPConfig } from "./Config";
export interface IStorageProvider {
    prefix: string;
    suffix: string;
    create: (key: string, value: string, expireDays?: number) => void;
    save: (key: string, value: string, expireDays?: number) => void;
    get: (key: string, defaultValue?: any) => any;
    remove: (key: string) => void;
}
export interface IStorageService {
    user: IStorageProvider;
    global: IStorageProvider;
    persistent: IStorageProvider;
}
export declare class StorageService {
    static $inject: string[];
    constructor(ippConfig: IIPPConfig);
}
import Emitter from "./Emitter";
declare class Table extends Emitter {
    readonly ON_CELL_CLICKED: string;
    readonly ON_CELL_DOUBLE_CLICKED: string;
    readonly ON_TAG_CLICKED: string;
    readonly ON_CELL_CHANGED: string;
    data: any;
    accessRanges: any;
    initialized: boolean;
    dirty: boolean;
    rendering: boolean;
    tags: boolean;
    isTouch: boolean;
    container: string;
    containerEl: any;
    viewEl: any;
    zoomEl: any;
    scrollEl: any;
    tableEl: any;
    tableHeadEl: any;
    tableBodyEl: any;
    tableFrozenRowsEl: any;
    tableFrozenColsEl: any;
    private validStyles;
    private _editing;
    private _tracking;
    private numOfCells;
    private classy;
    private helpers;
    private freeze;
    private freezeRange;
    private accessRangesClassNames;
    private clicked;
    constructor(container: string);
    editing: boolean;
    tracking: boolean;
    init(): boolean;
    render(data: any): boolean;
    update(data: any, content: any): boolean;
    getData(): any;
    setData(data: any): void;
    setAccessRanges(rangeAccess: any): void;
    setRanges(ranges: any, userId?: number): void;
    generateBlankData(): any;
    destroy(): void;
    getEditCell(): any;
    addRow(index: number, data: any): void;
    getRowIndex(): number;
    addColumn(): void;
    getColumnIndex(): number;
    setEditCell(el: any): any;
    freezePanes(): void;
    private renderRow(rowId, rowData);
    private renderCells(row, cellsData, startIndex);
    private setCellValues(cell, cellsData);
    private clickCellLink;
    private processLink(linkData);
    private getCellValue(cellData, escapeHtml);
    private onTableClick;
    private onWindowScroll;
    private showInput(cell);
    private nextCellInput(cell, direction?);
    private getClickedCell(clickedEl);
    private softMerges(rowEl);
    private updateFreezePanes(target);
    private clearCellClassNames(classNames);
}
export default Table;
export default class Test extends EventEmitter {
    constructor();
}

declare class Markup {
    cellValue: string;
    constructor(cellValue: string);
}
export default Markup;

import Emitter from "./Emitter";
import { IIPPConfig } from "./Config";
import { IStorageService } from "./Storage";
import IPromise = angular.IPromise;
export interface IRequestResult<T> {
    success: boolean;
    data: T;
    httpCode: number;
    httpText: string;
}
export interface IApiTokens {
    access_token: string;
    refresh_token: string;
}
export interface IApiService {
    tokens: IApiTokens;
    block: () => void;
    unblock: () => void;
    getSelfInfo: () => IPromise<IRequestResult>;
    refreshAccessTokens: (refreshToken: string) => IPromise<IRequestResult>;
    userLogin: (data: any) => IPromise<IRequestResult>;
    userLogout: (data?: any) => IPromise<IRequestResult>;
    createFolder: (data: any) => IPromise<IRequestResult>;
    getDomains: () => IPromise<IRequestResult>;
    getDomain: (domainId: number) => IPromise<IRequestResult>;
    updateDomain: (data: any) => IPromise<IRequestResult>;
    getDomainPages: (domainId: number) => IPromise<IRequestResult>;
    getDomainsAndPages: (client: string) => IPromise<IRequestResult>;
    getPage: (data: any) => IPromise<IRequestResult>;
    getPageByName: (data: any) => IPromise<IRequestResult>;
    getPageByUuid: (data: any) => IPromise<IRequestResult>;
    getPageAccess: (data: any) => IPromise<IRequestResult>;
    createPage: (data: any) => IPromise<IRequestResult>;
    createPageNotification: (data: any) => IPromise<IRequestResult>;
    createPageNotificationByUuid: (data: any) => IPromise<IRequestResult>;
    createAnonymousPage: (data: any) => IPromise<IRequestResult>;
    savePageContent: (data: any) => IPromise<IRequestResult>;
    savePageContentDelta: (data: any) => IPromise<IRequestResult>;
    savePageSettings: (data: any) => IPromise<IRequestResult>;
    deletePage: (data: any) => IPromise<IRequestResult>;
    saveUserInfo: (data: any) => IPromise<IRequestResult>;
    getUserMetaData: (data: any) => IPromise<IRequestResult>;
    saveUserMetaData: (data: any) => IPromise<IRequestResult>;
    deleteUserMetaData: (data: any) => IPromise<IRequestResult>;
    changePassword: (data: any) => IPromise<IRequestResult>;
    changeEmail: (data: any) => IPromise<IRequestResult>;
    forgotPassword: (data: any) => IPromise<IRequestResult>;
    resetPassword: (data: any) => IPromise<IRequestResult>;
    inviteUsers: (data: any) => IPromise<IRequestResult>;
    acceptInvitation: (data: any) => IPromise<IRequestResult>;
    refuseInvitation: (data: any) => IPromise<IRequestResult>;
    domainInvitations: (data: any) => IPromise<IRequestResult>;
    userInvitations: () => IPromise<IRequestResult>;
    domainAccessLog: (data: any) => IPromise<IRequestResult>;
    domainUsers: (data: any) => IPromise<IRequestResult>;
    signupUser: (data: any) => IPromise<IRequestResult>;
    activateUser: (data: any) => IPromise<IRequestResult>;
    setDomainDefault: (data: any) => IPromise<IRequestResult>;
    resendInvite: (data: any) => IPromise<IRequestResult>;
    updateDomainAccess: (data: any) => IPromise<IRequestResult>;
    removeUsersFromDomain: (data: any) => IPromise<IRequestResult>;
    getInvitation: (data: any) => IPromise<IRequestResult>;
    cancelInvitations: (data: any) => IPromise<IRequestResult>;
    getDomainAccessGroups: (data: any) => IPromise<IRequestResult>;
    getDomainAccessGroup: (data: any) => IPromise<IRequestResult>;
    addDomainAccessGroup: (data: any) => IPromise<IRequestResult>;
    putDomainAgroupMembers: (data: any) => IPromise<IRequestResult>;
    putDomainAgroupPages: (data: any) => IPromise<IRequestResult>;
    updateDomainAgroup: (data: any) => IPromise<IRequestResult>;
    deleteDomainAGroup: (data: any) => IPromise<IRequestResult>;
    getDomainPageAccess: (data: any) => IPromise<IRequestResult>;
    getDomainCustomers: (data: any) => IPromise<IRequestResult>;
    getDomainUsage: (data: any) => IPromise<IRequestResult>;
    saveDomainPageAccess: (data: any) => IPromise<IRequestResult>;
    getTemplates: (data: any) => IPromise<IRequestResult>;
    saveCustomer: (data: any) => IPromise<IRequestResult>;
    updateCustomer: (data: any) => IPromise<IRequestResult>;
    removeCustomer: (data: any) => IPromise<IRequestResult>;
    getDocEmailRules: (data: any) => IPromise<IRequestResult>;
    createDocEmailRule: (data: any) => IPromise<IRequestResult>;
    updateDocEmailRule: (data: any) => IPromise<IRequestResult>;
    deleteDocEmailRule: (data: any) => IPromise<IRequestResult>;
    getOrganization: (data: any) => IPromise<IRequestResult>;
    getOrganizationDomains: (data: any) => IPromise<IRequestResult>;
    getOrganizationUsers: (data: any) => IPromise<IRequestResult>;
    getOrganizationUsage: (data: any) => IPromise<IRequestResult>;
    getApplicationPageList: (client: string) => IPromise<IRequestResult>;
}
export declare class Api extends Emitter implements IApiService {
    private qs;
    private q;
    private storage;
    private config;
    private utils;
    private req;
    static $inject: string[];
    readonly EVENT_401: string;
    tokens: IApiTokens;
    private _endPoint;
    private _locked;
    private request;
    constructor(qs: any, q: any, storage: IStorageService, config: IIPPConfig, utils: any, req: any);
    block(): void;
    unblock(): void;
    getSelfInfo(): IPromise<IRequestResult>;
    refreshAccessTokens(refreshToken: string): IPromise<IRequestResult>;
    userLogin(data: any): IPromise<IRequestResult>;
    userLoginByCode(data: any): IPromise<IRequestResult>;
    userLogout(data?: any): IPromise<IRequestResult>;
    getDomains(): IPromise<IRequestResult>;
    getDomain(domainId: number): IPromise<IRequestResult>;
    createFolder(data: any): IPromise<IRequestResult>;
    createDomain(data: any): IPromise<IRequestResult>;
    updateDomain(data: any): IPromise<IRequestResult>;
    disableDomain(data: any): IPromise<IRequestResult>;
    getDomainPages(domainId: number): IPromise<IRequestResult>;
    getDomainsAndPages(client: string): IPromise<IRequestResult>;
    getPage(data: any): IPromise<IRequestResult>;
    getPageByName(data: any): IPromise<IRequestResult>;
    getPageByUuid(data: any): IPromise<IRequestResult>;
    getPageVerions(data: any): IPromise<IRequestResult>;
    getPageVerion(data: any): IPromise<IRequestResult>;
    restorePageVerion(data: any): IPromise<IRequestResult>;
    getPageAccess(data: any): IPromise<IRequestResult>;
    createPage(data: any): IPromise<IRequestResult>;
    createPageNotification(data: any): IPromise<IRequestResult>;
    createPageNotificationByUuid(data: any): IPromise<IRequestResult>;
    createAnonymousPage(data: any): IPromise<IRequestResult>;
    savePageContent(data: any): IPromise<IRequestResult>;
    savePageContentDelta(data: any): IPromise<IRequestResult>;
    savePageSettings(data: any): IPromise<IRequestResult>;
    deletePage(data: any): IPromise<IRequestResult>;
    saveUserInfo(data: any): IPromise<IRequestResult>;
    getUserMetaData(data: any): IPromise<IRequestResult>;
    saveUserMetaData(data: any): IPromise<IRequestResult>;
    deleteUserMetaData(data: any): IPromise<IRequestResult>;
    changePassword(data: any): IPromise<IRequestResult>;
    changeEmail(data: any): IPromise<IRequestResult>;
    forgotPassword(data: any): IPromise<IRequestResult>;
    resetPassword(data: any): IPromise<IRequestResult>;
    inviteUsers(data: any): IPromise<IRequestResult>;
    acceptInvitation(data: any): IPromise<IRequestResult>;
    refuseInvitation(data: any): IPromise<IRequestResult>;
    domainInvitations(data: any): IPromise<IRequestResult>;
    userInvitations(): IPromise<IRequestResult>;
    domainAccessLog(data: any): IPromise<IRequestResult>;
    domainUsers(data: any): IPromise<IRequestResult>;
    signupUser(data: any): IPromise<IRequestResult>;
    activateUser(data: any): IPromise<IRequestResult>;
    setDomainDefault(data: any): IPromise<IRequestResult>;
    resendInvite(data: any): IPromise<IRequestResult>;
    updateDomainAccess(data: any): IPromise<IRequestResult>;
    removeUsersFromDomain(data: any): IPromise<IRequestResult>;
    getInvitation(data: any): IPromise<IRequestResult>;
    cancelInvitations(data: any): IPromise<IRequestResult>;
    getDomainAccessGroups(data: any): IPromise<IRequestResult>;
    getDomainAccessGroup(data: any): IPromise<IRequestResult>;
    addDomainAccessGroup(data: any): IPromise<IRequestResult>;
    putDomainAgroupMembers(data: any): IPromise<IRequestResult>;
    putDomainAgroupPages(data: any): IPromise<IRequestResult>;
    updateDomainAgroup(data: any): IPromise<IRequestResult>;
    deleteDomainAGroup(data: any): IPromise<IRequestResult>;
    getDomainPageAccess(data: any): IPromise<IRequestResult>;
    getDomainCustomers(data: any): IPromise<IRequestResult>;
    getDomainUsage(data?: any): IPromise<IRequestResult>;
    saveDomainPageAccess(data: any): IPromise<IRequestResult>;
    getTemplates(data: any): IPromise<IRequestResult>;
    saveCustomer(data: any): IPromise<IRequestResult>;
    updateCustomer(data: any): IPromise<IRequestResult>;
    removeCustomer(data: any): IPromise<IRequestResult>;
    getDocEmailRules(data: any): IPromise<IRequestResult>;
    createDocEmailRule(data: any): IPromise<IRequestResult>;
    updateDocEmailRule(data: any): IPromise<IRequestResult>;
    deleteDocEmailRule(data: any): IPromise<IRequestResult>;
    private send;
    getApplicationPageList(client?: string): IPromise<IRequestResult>;
    getOrganization(data: any): IPromise<IRequestResult>;
    getOrganizationUsers(data: any): IPromise<IRequestResult>;
    getOrganizationUsage(data?: any): IPromise<IRequestResult>;
    getOrganizationUser(data: any): IPromise<IRequestResult>;
    createOrganizationUser(data: any): IPromise<IRequestResult>;
    saveOrganizationUser(data: any): IPromise<IRequestResult>;
    getOrganizationDomains(data: any): IPromise<IRequestResult>;
    getOrganizationDomain(data: any): IPromise<IRequestResult>;
    createOrganizationDomain(data: any): IPromise<IRequestResult>;
    saveOrganizationDomain(data: any): IPromise<IRequestResult>;
    private dummyRequest;
    private handleSuccess;
}

import Emitter, { IEmitter } from "./Emitter";
import { IStorageService } from "./Storage";
import { IApiService } from "./Api";
import IPromise = angular.IPromise;
import IQService = angular.IQService;
import ITimeoutService = angular.ITimeoutService;
export interface IAuthService extends IEmitter {
    EVENT_LOGGED_IN: string;
    EVENT_RE_LOGGING: string;
    EVENT_LOGIN_REFRESHED: string;
    EVENT_LOGGED_OUT: string;
    EVENT_ERROR: string;
    EVENT_401: string;
    EVENT_USER_UPDATED: string;
    user: IUserSelf;
    authenticate: (force?: boolean) => IPromise<any>;
    login: (username: string, password: string) => IPromise<any>;
    logout: () => void;
}
export interface IUserSelf {
    id: number;
    url: string;
    email: string;
    screen_name: string;
    first_name: string;
    last_name: string;
    mobile_phone_number: string;
    pushbullet_id: string;
    default_domain_id: number;
    default_page_id: number;
    default_domain_name: string;
    default_page_name: string;
    pending_invitation_count: number;
    can_create_folders: boolean;
    meta_data: any[];
}
export declare class Auth extends Emitter {
    private $q;
    private $timeout;
    private ippApi;
    private storage;
    private config;
    private utils;
    static $inject: string[];
    readonly EVENT_LOGGED_IN: string;
    readonly EVENT_RE_LOGGING: string;
    readonly EVENT_LOGIN_REFRESHED: string;
    readonly EVENT_LOGGED_OUT: string;
    readonly EVENT_ERROR: string;
    readonly EVENT_401: string;
    readonly EVENT_USER_UPDATED: string;
    polling: boolean;
    private _user;
    private _authenticated;
    private _authInProgress;
    private _selfTimer;
    private _selfTimeout;
    readonly user: IUserSelf;
    constructor($q: IQService, $timeout: ITimeoutService, ippApi: IApiService, storage: IStorageService, config: any, utils: any);
    authenticate(force?: boolean, tokens?: any): IPromise<any>;
    login(username: string, password: string): IPromise<any>;
    logout(all?: boolean): void;
    private processAuth;
    private refreshTokens;
    private saveTokens;
    private getUserInfo;
    private startPollingSelf;
    private on401;
}

declare class Classy {
    constructor();
    hasClass(ele: HTMLElement, cls: string): boolean;
    addClass(ele: HTMLElement, cls: string): boolean;
    removeClass(ele: HTMLElement, cls: string): boolean;
}
export default Classy;

import Emitter from "./Emitter";
export declare class Clipboard extends Emitter {
    readonly ON_DATA: string;
    editableAreaId: string;
    editableAreaEl: any;
    focus: boolean;
    clipboardTextPlain: string;
    private key;
    private doc;
    private validStyles;
    private excelStyles;
    private excelBorderStyles;
    private excelBorderWeights;
    private borderSides;
    private borderSyles;
    private utils;
    constructor(element: any, doc: boolean);
    init(element: any, doc: boolean): boolean;
    destroy(): any;
    copyTextToClipboard(text: any): boolean;
    private onEditFocus;
    private onEditBlur;
    private onPasteDocument;
    private getClipboardText;
    private onPaste;
    private createPastedElement;
    private getHtml;
    private onKeyDown;
    private parseText;
    private parseTable;
    private collectClipData;
    private tableColWidths;
    private getRawValue;
    private cssToStyles;
    private mapExcelBorder;
    private getTextWidth;
}
export declare class ClipboardWrap {
    static $inject: string[];
    constructor();
}

export interface IIPPConfig {
    api_url?: string;
    ws_url?: string;
    docs_url?: string;
    api_key: string;
    api_secret: string;
    transport?: string;
    storage_prefix?: string;
    cookie?: any;
}
export declare class Config {
    private _config;
    set(config: IIPPConfig): void;
    readonly api_url: string;
    readonly ws_url: string;
    readonly docs_url: string;
    readonly api_key: string;
    readonly api_secret: string;
    readonly transport: string;
    readonly storage_prefix: string;
    readonly cookie: any;
    $get(): IIPPConfig;
}

export interface IPageContentLink {
    external: boolean;
    address: string;
}
export interface IPageCellStyle {
    "background-color"?: string;
    "color"?: string;
    "font-family"?: string;
    "font-size"?: string;
    "font-style"?: string;
    "font-weight"?: string;
    "height"?: string;
    "number-format"?: string;
    "text-align"?: string;
    "text-wrap"?: string;
    "width"?: string;
    "tbs"?: string;
    "rbs"?: string;
    "bbs"?: string;
    "lbs"?: string;
    "tbc"?: string;
    "rbc"?: string;
    "bbc"?: string;
    "lbc"?: string;
    "tbw"?: string;
    "rbw"?: string;
    "bbw"?: string;
    "lbw"?: string;
}
export interface IPageContentCell {
    value: string;
    formatted_value?: string;
    link?: IPageContentLink;
    style?: IPageCellStyle;
    dirty?: boolean;
}
export interface IPageContent extends Array<any> {
    [index: number]: IPageContentCell[];
}
export interface IPageDeltaContentCol {
    col_index: number;
    cell_content: IPageContentCell;
}
export interface IPageDeltaContentRow {
    row_index: number;
    cols: IPageDeltaContentCol[];
}
export interface IPageDelta {
    new_rows: number[];
    new_cols: number[];
    content_delta: IPageDeltaContentRow[];
}
export interface IPageContentProvider {
    canDoDelta: boolean;
    update: (rawContent: IPageContent, isCurrent: boolean) => void;
    reset: () => void;
    getCell: (rowIndex: number, columnIndex: number) => IPageContentCell;
    updateCell: (rowIndex: number, columnIndex: number, data: IPageContentCell) => void;
    addRow: () => void;
    addColumn: () => void;
    removeRow: () => void;
    removeColumn: () => void;
    getDelta: () => IPageDelta;
    getFull: () => IPageContent;
    cleanDirty: () => void;
}
export declare class PageContent implements IPageContentProvider {
    canDoDelta: boolean;
    private _original;
    private _current;
    private _newRows;
    private _newCols;
    private _utils;
    readonly current: IPageContent;
    constructor(rawContent: IPageContent);
    update(rawContent: IPageContent, isCurrent: boolean): void;
    reset(): void;
    getCell(rowIndex: number, columnIndex: number): IPageContentCell;
    updateCell(rowIndex: number, columnIndex: number, data: IPageContentCell): void;
    addRow(index?: number): IPageContentCell[];
    addColumn(index?: number): void;
    removeRow(): void;
    removeColumn(): void;
    getDelta(): IPageDelta;
    getFull(): IPageContent;
    cleanDirty(): void;
}

export interface IEmitter {
    emit: (name: string, args?: any) => void;
    register: (callback: any) => void;
    unRegister: (callback: any) => void;
}
declare class Emitter implements IEmitter {
    listeners: any;
    onListeners: any;
    constructor();
    on: (name: string, callback: any) => void;
    off: (name: string, callback: any) => void;
    register: (callback: any) => void;
    unRegister(callback: any): void;
    emit(name: string, args?: any): void;
    removeEvent(): void;
}
export default Emitter;


import Emitter from "./Emitter";
export declare class Grid extends Emitter {
    readonly ON_CELL_CLICKED: string;
    readonly ON_CELL_CLICKED_RIGHT: string;
    readonly ON_CELL_HISTORY_CLICKED: string;
    readonly ON_CELL_LINK_CLICKED: string;
    readonly ON_CELL_DOUBLE_CLICKED: string;
    readonly ON_TAG_CLICKED: string;
    readonly ON_CELL_VALUE: string;
    readonly ON_CELL_SELECTED: string;
    readonly ON_CELL_RESET: string;
    readonly ON_EDITING: string;
    containerEl: any;
    scale: number;
    ratio: number;
    ratioScale: number;
    pause: boolean;
    canEdit: boolean;
    scrollbars: {
        width: number;
        inset: boolean;
        scrolling: boolean;
        x: {
            dimension: string;
            label: string;
            show: boolean;
            anchor: string;
            track: any;
            handle: any;
        };
        y: {
            dimension: string;
            label: string;
            show: boolean;
            anchor: string;
            track: any;
            handle: any;
        };
    };
    alwaysEditing: boolean;
    disallowSelection: boolean;
    historyIndicatorSize: number;
    trackingHighlightClassname: string;
    trackingHighlightMatchColor: boolean;
    trackingHighlightLifetime: number;
    hasFocus: boolean;
    fluid: boolean;
    noAccessImages: any;
    private stage;
    private borders;
    private originalContent;
    private content;
    private history;
    private trackingHighlights;
    private gc;
    private offsets;
    private width;
    private height;
    private visible;
    private gridLines;
    private found;
    private foundSelection;
    private touch;
    private allowBrowserScroll;
    private hammer;
    private links;
    private styleRanges;
    private buttonRanges;
    private accessRanges;
    private freezeRange;
    private classy;
    private helpers;
    private containerRect;
    private _tracking;
    private _editing;
    private _fit;
    private userTrackingColors;
    private selection;
    private historyIndicator;
    private input;
    private cellSelection;
    private filters;
    private sorting;
    private dirty;
    private rightClick;
    private animation;
    private keyValue;
    private ctrlDown;
    private shiftDown;
    private freeScroll;
    private cellStyles;
    private init;
    private hidden;
    constructor(container: any, fluid?: boolean, options?: any);
    fit: string;
    editing: boolean;
    tracking: boolean;
    destroy(): void;
    cellHistory(data: any): void;
    setTrackingHighlights(data: any): void;
    clearTrackingHighlights(): void;
    setContent(content: any): void;
    getContentHtml(): string;
    getCells(fromCell: any, toCell: any, refCell: any): any;
    renderGrid(clear?: boolean): void;
    drawGrid: () => void;
    setScale(n: any): void;
    setFit(n: any, retain: any): void;
    clearRanges(): void;
    setRanges(ranges: any, userId?: number): void;
    clearFilters(): void;
    setFilters(filters: any): void;
    clearSorting(): void;
    setSorting(filters: any): void;
    hideSelector(focus?: boolean): void;
    find(value: any): void;
    gotoFoundCell(which: any): void;
    setSelector(from: any, to?: any): void;
    private updateContent;
    private showInput;
    private setCellIndexes;
    private updateFound;
    private createHighlight;
    private applyFilters;
    private applySorting;
    private buildCells;
    private updateCellAccess;
    private updateFit;
    private setupTouch;
    private onWheelEvent;
    private onResizeEvent;
    private onMouseMove;
    private onMouseUp;
    private isRightClick;
    private onVisibilityEvent;
    private onMouseDown;
    private onMouseOver;
    private setCellSelection;
    private onKeydown;
    private onKeyup;
    private startFreeScroll;
    private keepSelectorWithinView;
    private goto;
    private restoreCell;
    private nextCellInput;
    private setEditCell;
    private emitCellChange;
    private isCanvas;
    private isInput;
    private getCell;
    private cellHasHistory;
    private createScrollbars;
    private updateScrollbars;
    private createSelector;
    private updateSelectorAttributes;
    private hideHistoryIndicator;
    private updateSelector;
    private setOffsets;
    private resetStage;
    private setSize;
    private getTextOverlayWidth;
    private softMerges;
    private getFontString;
    private flattenStyles;
    private getBackgroundColor;
    private drawGraphics;
    private drawLine;
    private drawTrackingRect;
    private drawSortingIndicator;
    private drawOffsetRect;
    private drawRect;
    private cleanValue;
    private drawText;
    private findLines;
    private translateCanvas;
    private resetCanvas;
}
export declare class GridWrap {
    static $inject: string[];
    constructor();
}

import { IGridModule } from "./Grid";
export interface ICanvas extends IGridModule {
    buffer: HTMLCanvasElement;
    bufferContext: CanvasRenderingContext2D;
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    update: (resize?: boolean) => void;
}
export declare class Canvas implements ICanvas {
    buffer: HTMLCanvasElement;
    bufferContext: CanvasRenderingContext2D;
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    private gridLines;
    private sized;
    constructor();
    init(): void;
    update(resize?: boolean): void;
    resize(): void;
    destroy(): void;
    private draw;
    private drawGraphics;
    private drawLine;
    private drawTrackingRect;
    private drawSortingIndicator;
    private drawOffsetRect;
    private drawRect;
    private drawText;
    private findLines;
    private translateCanvas;
    private resetCanvas;
    private getFontString;
    private getBackgroundColor;
    private cleanValue;
}


import { IGridModule } from "./Grid";
import { IContentCell } from "./Content";
interface ILinkOptions {
    address?: string;
    cell: any;
    className?: string;
    styles?: any;
    user?: number;
    color?: string;
}
export interface IContainer extends IGridModule {
    container: string | HTMLElement;
    element: HTMLElement;
    elementRect: IElementRect;
    input: HTMLInputElement;
    Scrollbars: IScrollbars;
    Selector: ISelector;
    addElement: (element: HTMLElement) => void;
    cleanLinks: () => void;
    addLink: (type: string, options: ILinkOptions) => void;
    addImage: (cell: IContentCell) => void;
    removeLink: (type: string, cell: IContentCell) => void;
    removeImage: (row: number, col: number) => void;
    updateSelectorAttributes: (cell: IContentCell, cellTo: IContentCell, selector: HTMLElement, history?: boolean) => void;
    showInput: () => void;
    hideInput: () => void;
    hasFocus: (evt: Event) => boolean;
    addGridLine(axis: string, cell: IContentCell): void;
    removeGridLine: (row: number, col: number) => void;
}
export interface IElementRect {
    left: number;
    top: number;
    width: number;
    height: number;
}
export interface IContainerRect {
    width: number;
    height: number;
}
export declare class Container implements IContainer {
    container: string | HTMLElement;
    element: HTMLElement;
    elementRect: IElementRect;
    input: HTMLInputElement;
    Scrollbars: IScrollbars;
    Selector: ISelector;
    links: any;
    lines: any;
    images: any;
    private zIndexes;
    private classy;
    constructor(container: string | HTMLElement);
    init(): void;
    update(): void;
    cleanLinks(): void;
    destroy(): void;
    hasFocus(evt: Event): boolean;
    addElement(element: HTMLElement): void;
    addLink(type: string, options: ILinkOptions): void;
    removeLink(type: string, cell: IContentCell): void;
    addImage(cell: IContentCell): void;
    removeImage(row: number, col: number): void;
    updateSelector(): void;
    showInput(): void;
    hideInput(): void;
    updateSelectorAttributes(cell: IContentCell, cellTo: IContentCell, selector: HTMLElement, history?: boolean): void;
    addGridLine(axis: string, cell: IContentCell): void;
    removeGridLine(row: number, col: number): void;
    private createImageElement;
    private createDomElement;
    private removeDomElement;
    private updateDomElement;
    private createInputElement;
    private isTarget;
    private updateLinks;
}
interface IScrollbars {
    visible: boolean;
    inset: boolean;
    size: number;
    xAxis: HTMLElement;
    xShow: boolean;
    yAxis: HTMLElement;
    yShow: boolean;
    update: () => void;
    destroy: () => void;
}
interface ISelector {
    visible: boolean;
    height: number;
    width: number;
    x: number;
    y: number;
    element: HTMLElement;
    update: () => void;
}
export {};

import { IGridModule } from "./Grid";
interface IStage {
    x: IStageCoords;
    y: IStageCoords;
}
interface IStageCoords {
    pos: number;
    increments: any;
    size: any;
    stop: boolean;
    label: string;
}
interface IOffets {
    x: IOffsetsCoords;
    y: IOffsetsCoords;
}
interface IOffsetsCoords {
    offset: number;
    index: number;
    max: number;
    maxOffset: number;
}
export interface IContentFind {
    query: string;
}
export interface IContent extends IGridModule {
    noAccess: boolean;
    dirty: boolean;
    hidden: boolean;
    ctrlDown: boolean;
    shiftDown: boolean;
    hasFocus: boolean;
    editing: boolean;
    scale: number;
    ratio: number;
    originalContent: any[];
    content: any[];
    cells: any[];
    visibleCells: any[];
    trackedCells: any[];
    frozenCells: any;
    stage: IStage;
    offsets: IOffets;
    borders: any;
    width: number;
    height: number;
    resizing: boolean;
    scrolling: boolean;
    scrollbarHeight: number;
    scrollbarWidth: number;
    headingSelected: string;
    history: any[];
    tracking: boolean;
    freezeRange: IContentFreezeRange;
    cellSelection: IContentCellSelection;
    keyValue: string;
    set: (data: any) => void;
    update: () => void;
    move: () => void;
    setOffsets: (deltas?: any, set?: boolean) => void;
    setScale: (n: number) => void;
    setFit: (n: string) => void;
    cellHistory: (data: any) => void;
    cellHighlights: (data: any) => void;
    setMergeRanges: (data: any) => void;
    setRanges: (data: any) => void;
    setSorting: (data: any) => void;
    clearSorting: () => void;
    setFilters: (data: any) => void;
    clearFilters: () => void;
    find: (data: IContentFind) => void;
    clearFound: () => void;
    getContentHtml: () => string;
    getCells: (fromCell: any, toCell: any) => any;
    setSelector(from?: any, to?: any): void;
    setColSize(headingCell: IContentCell, value: number): void;
    setRowSize(headingCell: IContentCell, value: number): void;
    keepSelectorWithinView(): void;
}
interface IContentCellPosition {
    row: number;
    col: number;
}
export interface IContentCellSelection {
    from: IContentCell;
    to: IContentCell;
    last: IContentCell;
    go: boolean;
    on: boolean;
    clicked: number;
    found: any;
}
interface IContentFreezeRangeIndex {
    row: number;
    col: number;
}
interface IContentFreezeRange {
    valid: boolean;
    width: number;
    height: number;
    index: IContentFreezeRangeIndex;
}
interface IContentCellCoords {
    a: number;
    b: number;
    height: number;
    width: number;
    x: number;
    y: number;
    _a: number;
    _x: number;
}
export interface IContentCell {
    allow: boolean | string;
    button: any;
    formatted_value: string;
    height: number;
    hidden: boolean;
    heading: boolean;
    image: boolean;
    visible: boolean;
    history: any;
    index: IContentCellPosition;
    link: any;
    overlayEnd: number;
    overlayStart: number;
    overlayWidth: number;
    position: IContentCellPosition;
    style: any;
    formatting: any;
    value: string;
    width: number;
    wrap: boolean;
    x: number;
    y: number;
    coords: IContentCellCoords;
}
export interface IHeadingSelection {
    rightClick: boolean;
    which: string;
    selection: IContentCellSelection;
    event: MouseEvent;
    count: number;
}
export declare class Content implements IContent {
    noAccess: boolean;
    dirty: boolean;
    hidden: boolean;
    ctrlDown: boolean;
    shiftDown: boolean;
    hasFocus: boolean;
    scale: number;
    ratio: number;
    originalContent: any[];
    content: any[];
    cells: any[];
    visibleCells: any[];
    frozenCells: any;
    trackedCells: any[];
    stage: IStage;
    width: number;
    height: number;
    resizing: boolean;
    scrolling: boolean;
    scrollbarHeight: number;
    scrollbarWidth: number;
    offsets: {
        x: {
            offset: number;
            index: number;
            max: number;
            maxOffset: number;
        };
        y: {
            offset: number;
            index: number;
            max: number;
            maxOffset: number;
        };
    };
    borders: {
        widths: {
            none: number;
            thin: number;
            medium: number;
            thick: number;
        };
        styles: {
            none: string;
            solid: string;
            double: string;
        };
        names: {
            t: string;
            r: string;
            b: string;
            l: string;
        };
    };
    history: any[];
    highlights: any[];
    tracking: boolean;
    cellSelection: IContentCellSelection;
    freezeRange: IContentFreezeRange;
    keyValue: string;
    headingSelected: string;
    private _editing;
    private mergeRanges;
    private styleRanges;
    private buttonRanges;
    private accessRanges;
    private found;
    private foundSelection;
    private sorting;
    private filters;
    private cellStyles;
    constructor();
    editing: boolean;
    init(): void;
    set(data: any): void;
    update(): void;
    move(): void;
    destroy(): void;
    setOffsets(deltas?: any, set?: boolean): void;
    setScale(n: number): void;
    setFit(n: any): void;
    cellHistory(data: any): void;
    cellHighlights(data: any): void;
    clearFilters(): void;
    setFilters(filters: any): void;
    clearSorting(): void;
    setSorting(filters: any): void;
    setMergeRanges(ranges: any): void;
    setRanges(ranges: any): void;
    find(data: IContentFind): void;
    clearFound(): void;
    getContentHtml(): string;
    getCells(fromCell: any, toCell: any): any;
    setSelector(from?: any, to?: any): void;
    setColSize(headingCell: IContentCell, value: number): void;
    setRowSize(headingCell: IContentCell, value: number): void;
    keepSelectorWithinView(): void;
    private flattenStyles;
    private resetStage;
    private resetRanges;
    private setCellAccess;
    private setStage;
    private setSize;
    private setVisibility;
    private softMerges;
    private getFontString;
    private cleanValue;
    private getTextOverlayWidth;
    private getCell;
    private onEvents;
    private onKeyDown;
    private onMouseUp;
    private selectHeadingCell;
    private selectCell;
    private selectNextCell;
    private setCellSelection;
    private clearCellSelection;
    private updateCellSelection;
    private updateFit;
    private applyFilters;
    private applySorting;
    private clone;
    private setHeadingCellStyle;
    private toColumnName;
}
export {};

import Emitter, { IEmitter } from "../Emitter";
export interface IEvents extends IEmitter {
    rightClick: boolean;
    preventContextMenu: boolean;
    init: () => void;
    destroy: () => void;
}
export declare class Events extends Emitter implements IEvents {
    rightClick: boolean;
    preventContextMenu: boolean;
    private hammer;
    private freeScroll;
    constructor();
    init(): void;
    destroy(): void;
    update(): void;
    private onVisibilityEvent;
    private setupTouch;
    private onResize;
    private onKeydown;
    private onKeyup;
    private onMouseDown;
    private onMouseOver;
    private onMouseUp;
    private onMouseMove;
    private onWheelEvent;
    private isRightClick;
}

import Emitter, { IEmitter } from '../Emitter';
import { IPageService } from '../Page/Page';
import { IContainer } from './Container';
import { ICanvas } from './Canvas';
import { IEvents } from './Events';
import { ISettings } from './Settings';
import { IContent, IContentFind, IContentCell } from './Content';
interface IBaseProvider extends IEmitter {
    ON_CELL_CLICKED: string;
    ON_CELL_CLICKED_RIGHT: string;
    ON_CELL_HISTORY_CLICKED: string;
    ON_CELL_LINK_CLICKED: string;
    ON_CELL_DOUBLE_CLICKED: string;
    ON_TAG_CLICKED: string;
    ON_CELL_VALUE: string;
    ON_CELL_SELECTED: string;
    ON_CELL_RESET: string;
    ON_EDITING: string;
    ON_RESIZE: string;
    ON_HEADING_CLICKED: string;
    Content: IContent;
    Container: IContainer;
    Canvas: ICanvas;
    Events: IEvents;
    Settings: ISettings;
    classy: any;
    helpers: any;
    update: () => void;
}
export declare let GridBase: IBaseProvider;
export interface IGridModule {
    init: () => void;
    destroy: () => void;
    update: () => void;
}
export interface IGridProvider {
    container: string | HTMLElement;
    Base: IBaseProvider;
    init: (container: string | HTMLElement, Page: IPageService) => void;
    destroy: () => void;
    render: (data: any, ranges?: any) => void;
    setFit(value: string): void;
    cellHistory(data: any): void;
    cellHighlights(data: any): void;
    setMergeRanges(data: any): void;
    setRanges(data: any): void;
    setSorting(data: any): void;
    setFilters(data: any): void;
    clearSorting(): void;
    clearFilters(): void;
    setOption(key: string, value: any): void;
    find(value: IContentFind): void;
    clearFound(): void;
    getContentHtml(): string;
    getCell(row: number, col: number): any;
    getCells(fromCell: any, toCell: any): any;
    setSelector(from: any, to?: any): void;
    hideSelector(): void;
}
export declare class Grid extends Emitter implements IGridProvider {
    container: string | HTMLElement;
    Base: IBaseProvider;
    private initialised;
    constructor(container: string | HTMLElement, options?: any);
    init(container: string | HTMLElement, options?: any): void;
    destroy(): void;
    render(data: any, ranges?: any): void;
    setFit(value: string): void;
    cellHistory(data: any): void;
    cellHighlights(data: any): void;
    setMergeRanges(data: any): void;
    setHiddenColumns(data: any): void;
    setRanges(data: any): void;
    setSorting(data: any): void;
    setFilters(data: any): void;
    clearSorting(): void;
    clearFilters(): void;
    setOption(key: string, value: any): void;
    find(value: IContentFind): void;
    clearFound(): void;
    getContentHtml(): string;
    getCell(row: number, col: number): IContentCell;
    getCells(fromCell: any, toCell: any): any;
    setSelector(from: any, to?: any): void;
    hideSelector(): void;
    onGridBaseEvent: (name: string, args?: any) => void;
}
export {};

import { IGridModule } from "./Grid";
interface INoAccessIMages {
    light: string;
    dark: string;
}
export interface ISettings extends IGridModule {
    contrast: string;
    tracking: boolean;
    highlights: boolean;
    highlightsShowTime: number;
    highlightsRemoveTime: number;
    touch: boolean;
    pause: boolean;
    headings: boolean;
    canEdit: boolean;
    alwaysEditing: boolean;
    disallowSelection: boolean;
    hasFocus: boolean;
    fluid: boolean;
    ratio: number;
    scale: number;
    fit: string;
    scrollbarWidth: number;
    scrollbarInset: boolean;
    userId: number;
    userTrackingColors: string[];
    noAccessImages: INoAccessIMages;
    historyIndicatorSize: number;
    backgroundColor: string;
    update: () => void;
    set: (key: string, value: any) => void;
}
export declare class Settings implements ISettings {
    contrast: string;
    tracking: boolean;
    highlights: boolean;
    highlightsShowTime: number;
    highlightsRemoveTime: number;
    touch: boolean;
    pause: boolean;
    headings: boolean;
    canEdit: boolean;
    alwaysEditing: boolean;
    disallowSelection: boolean;
    hasFocus: boolean;
    fluid: boolean;
    scrollbarWidth: number;
    scrollbarInset: boolean;
    scale: number;
    ratio: number;
    fit: string;
    userId: number;
    userTrackingColors: string[];
    noAccessImages: INoAccessIMages;
    historyIndicatorSize: number;
    backgroundColor: string;
    constructor(options?: any);
    init(): void;
    set(key: string, value: any): void;
    destroy(): void;
    update(): void;
}
export {};




import Emitter, { IEmitter } from '../Emitter';
export interface IEvents extends IEmitter {
    rightClick: boolean;
    preventContextMenu: boolean;
    init: () => void;
    destroy: () => void;
}
export declare class Events extends Emitter implements IEvents {
    rightClick: boolean;
    preventContextMenu: boolean;
    private hammer;
    private resizeTimer;
    private freeScroll;
    constructor();
    init(): void;
    destroy(): void;
    update(): void;
    private onVisibilityEvent;
    private setupTouch;
    private onResize;
    private onKeydown;
    private onKeyup;
    private onMouseDown;
    private onMouseOver;
    private onMouseUp;
    private onMouseMove;
    private onWheelEvent;
    private isRightClick;
}






import { IGridModule } from "./Grid";
interface INoAccessIMages {
    light: string;
    dark: string;
}
export interface ISettings extends IGridModule {
    contrast: string;
    tracking: boolean;
    highlights: boolean;
    highlightsShowTime: number;
    highlightsRemoveTime: number;
    touch: boolean;
    pause: boolean;
    headings: boolean;
    canEdit: boolean;
    alwaysEditing: boolean;
    disallowSelection: boolean;
    hasFocus: boolean;
    fluid: boolean;
    ratio: number;
    scale: number;
    fit: string;
    scrollbarWidth: number;
    scrollbarInset: boolean;
    userId: number;
    userTrackingColors: string[];
    noAccessImages: INoAccessIMages;
    historyIndicatorSize: number;
    backgroundColor: string;
    update: () => void;
    set: (key: string, value: any) => void;
}
export declare class Settings implements ISettings {
    contrast: string;
    tracking: boolean;
    highlights: boolean;
    highlightsShowTime: number;
    highlightsRemoveTime: number;
    touch: boolean;
    pause: boolean;
    headings: boolean;
    canEdit: boolean;
    alwaysEditing: boolean;
    disallowSelection: boolean;
    hasFocus: boolean;
    fluid: boolean;
    scrollbarWidth: number;
    scrollbarInset: boolean;
    scale: number;
    ratio: number;
    fit: string;
    userId: number;
    userTrackingColors: string[];
    noAccessImages: INoAccessIMages;
    historyIndicatorSize: number;
    backgroundColor: string;
    constructor(options?: any);
    init(): void;
    set(key: string, value: any): void;
    destroy(): void;
    update(): void;
}
export {};

declare class Helpers {
    constructor();
    isNumber(value: any): boolean;
    toColumnName(num: any): string;
    validHex(hex: string): boolean;
    rgbToHex(rgb: string): string;
    componentToHex(c: any): string;
    addEvent(element: HTMLElement, eventName: string, func: any): void;
    capitalizeFirstLetter(string: string): string;
    removeEvent(element: HTMLElement, eventName: string, func: any): void;
    isTouch(): boolean;
    clickEvent(): string;
    openWindow(link: string, target?: string, params?: {
        [s: string]: string;
    }): Window;
    createSlug(str: string): string;
    getScrollbarWidth(): number;
    parseDateExcel(excelTimestamp: any): number;
    getContrastYIQ(hexcolor: string): string;
    convertTime(timestamp: any): string;
    isValidEmail(string: string): boolean;
}
export default Helpers;

export {};

export {};

export {};


export interface IPageContentLink {
    external: boolean;
    address: string;
}
export interface IPageCellStyle {
    "background-color"?: string;
    "color"?: string;
    "font-family"?: string;
    "font-size"?: string;
    "font-style"?: string;
    "font-weight"?: string;
    "height"?: string;
    "number-format"?: string;
    "text-align"?: string;
    "text-wrap"?: string;
    "width"?: string;
    "tbs"?: string;
    "rbs"?: string;
    "bbs"?: string;
    "lbs"?: string;
    "tbc"?: string;
    "rbc"?: string;
    "bbc"?: string;
    "lbc"?: string;
    "tbw"?: string;
    "rbw"?: string;
    "bbw"?: string;
    "lbw"?: string;
}
export interface IPageContentCell {
    value: string;
    formatted_value?: string;
    link?: IPageContentLink;
    style?: IPageCellStyle;
    originalStyle?: IPageCellStyle;
    dirty?: boolean;
}
export interface IPageContent extends Array<any> {
    [index: number]: IPageContentCell[];
}
export interface IPageDeltaContentCol {
    col_index: number;
    cell_content: IPageContentCell;
}
export interface IPageDeltaContentRow {
    row_index: number;
    cols: IPageDeltaContentCol[];
}
export interface IPageDelta {
    new_rows: number[];
    new_cols: number[];
    content_delta: IPageDeltaContentRow[];
}
export interface IPageContentProvider {
    current: IPageContent;
    canDoDelta: boolean;
    dirty: boolean;
    update: (rawContent: IPageContent, clean?: boolean, replace?: boolean) => void;
    reset: () => void;
    getCell: (rowIndex: number, columnIndex: number) => IPageContentCell;
    getCells: (fromCell: any, toCell: any) => any;
    updateCell: (rowIndex: number, columnIndex: number, data: IPageContentCell) => void;
    addRow: (index?: number) => void;
    addColumn: (index?: number, direction?: string) => void;
    removeRow: (index?: number, direction?: string) => void;
    removeColumn: (index?: number) => void;
    setColSize(col: number, value: number): void;
    setRowSize(row: number, value: number): void;
    getDelta: () => IPageDelta;
    getFull: () => IPageContent;
}
export declare class PageContent implements IPageContentProvider {
    canDoDelta: boolean;
    dirty: boolean;
    private _original;
    private _current;
    private _newRows;
    private _newCols;
    private _utils;
    private _defaultStyles;
    private borders;
    private cellStyles;
    readonly current: IPageContent;
    constructor(rawContent: IPageContent);
    update(rawContent: IPageContent, clean?: boolean, replace?: boolean): void;
    reset(): void;
    getCell(rowIndex: number, columnIndex: number): IPageContentCell;
    getCells(fromCell: any, toCell: any): any;
    updateCell(rowIndex: number, columnIndex: number, data: IPageContentCell): void;
    addRow(index?: number, direction?: string): void;
    addColumn(index?: number, direction?: string): void;
    removeRow(index?: number): void;
    removeColumn(index: number): void;
    setRowSize(row: number, value: number): void;
    setColSize(col: number, value: number): void;
    getDelta(): IPageDelta;
    getFull(): IPageContent;
    getHtml(): any;
    private flattenStyles;
    private defaultContent;
}

import IPromise = angular.IPromise;
import IQService = angular.IQService;
import ITimeoutService = angular.ITimeoutService;
import IIntervalService = angular.IIntervalService;
import { IEmitter } from "../Emitter";
import { IPageContentProvider } from "./Content";
import { IPageContent } from "./Content";
import { IPageRangesCollection, IPageFreezingRange } from "./Range";
import { IEncryptionKey, ICryptoService } from "../Crypto";
import { IApiService } from "../Api";
import { IAuthService } from "../Auth";
import { IStorageService } from "../Storage";
import { IIPPConfig } from "../Config";
export interface IPageTypes {
    regular: number;
    pageAccessReport: number;
    domainUsageReport: number;
    globalUsageReport: number;
    pageUpdateReport: number;
    alert: number;
    pdf: number;
    liveUsage: number;
}
export interface IPageServiceContent {
    id: number;
    seq_no: number;
    content_modified_timestamp: Date;
    content: IPageContent;
    content_modified_by: any;
    push_interval: number;
    pull_interval: number;
    is_public: boolean;
    description: string;
    encrypted_content: string;
    encryption_key_used: string;
    encryption_type_used: number;
    special_page_type: number;
}
export interface IPageServiceMeta {
    by_name_url: string;
    id: number;
    name: string;
    description: string;
    url: string;
    uuid: string;
    access_rights: string;
    background_color: string;
    content: IPageContent;
    content_modified_by: any;
    content_modified_timestamp: Date;
    created_by: any;
    created_timestamp: Date;
    domain_id: number;
    domain_name: string;
    domain_url: string;
    encrypted_content: string;
    encryption_key_to_use: string;
    encryption_key_used: string;
    encryption_type_to_use: number;
    encryption_type_used: number;
    is_obscured_public: boolean;
    is_public: boolean;
    is_template: boolean;
    modified_by: any;
    modified_timestamp: Date;
    pull_interval: number;
    push_interval: number;
    record_history: boolean;
    seq_no: number;
    show_gridlines: boolean;
    special_page_type: number;
    ws_enabled: boolean;
}
export interface IPage extends IPageServiceMeta {
}
export interface IUserPageDomainCurrentUserAccess {
    default_page_id: number;
    default_page_url: string;
    domain_id: number;
    domain_url: string;
    is_active: boolean;
    is_administrator: boolean;
    is_default_domain: boolean;
    is_pending: boolean;
    page_count: number;
    user_id: number;
    user_url: string;
}
export interface IUserPageDomainAccess {
    alerts_enabled: boolean;
    by_name_url: string;
    current_user_domain_access: IUserPageDomainCurrentUserAccess;
    description: string;
    display_name: string;
    domain_type: number;
    encryption_enabled: boolean;
    id: number;
    is_page_access_mode_selectable: boolean;
    is_paying_customer: boolean;
    login_screen_background_color: "";
    logo_url: string;
    name: string;
    page_access_mode: number;
    page_access_url: string;
    url: string;
}
export interface IUserPageAccess {
    by_name_url: string;
    content_by_name_url: string;
    content_url: string;
    domain: IUserPageDomainAccess;
    domain_id: number;
    domain_name: string;
    domain_url: string;
    encryption_to_use: number;
    encryption_key_to_use: string;
    id: number;
    is_administrator: boolean;
    is_public: boolean;
    is_users_default_page: boolean;
    name: string;
    pull_interval: number;
    push_interval: number;
    special_page_type: number;
    url: string;
    write_access: boolean;
    ws_enabled: boolean;
}
export interface IPageTemplate {
    by_name_url: string;
    category: string;
    description: string;
    domain_id: number;
    domain_name: string;
    id: number;
    name: string;
    special_page_type: number;
    url: string;
    uuid: string;
}
export interface IPageCloneOptions {
    clone_ranges?: boolean;
}
export interface IPageCopyOptions {
    content?: boolean;
    access_rights?: boolean;
    settings?: boolean;
}
export interface IPageService extends IEmitter {
    TYPE_REGULAR: number;
    TYPE_ALERT: number;
    TYPE_PDF: number;
    TYPE_PAGE_ACCESS_REPORT: number;
    TYPE_DOMAIN_USAGE_REPORT: number;
    TYPE_GLOBAL_USAGE_REPORT: number;
    TYPE_PAGE_UPDATE_REPORT: number;
    TYPE_LIVE_USAGE_REPORT: number;
    EVENT_READY: string;
    EVENT_NEW_CONTENT: string;
    EVENT_NEW_META: string;
    EVENT_RANGES_UPDATED: string;
    EVENT_ACCESS_UPDATED: string;
    EVENT_DECRYPTED: string;
    EVENT_ERROR: string;
    ready: boolean;
    decrypted: boolean;
    updatesOn: boolean;
    types: IPageTypes;
    encryptionKeyPull: IEncryptionKey;
    encryptionKeyPush: IEncryptionKey;
    data: IPage;
    access: IUserPageAccess;
    Content: IPageContentProvider;
    Ranges: IPageRangesCollection;
    freeze: IPageFreezingRange;
    start: () => void;
    stop: () => void;
    push: (forceFull?: boolean) => IPromise<any>;
    saveMeta: (data: any) => IPromise<any>;
    destroy: () => void;
    decrypt: (key: IEncryptionKey) => void;
    clone: (folderId: number, name: string, options?: IPageCloneOptions) => IPromise<IPageService>;
    copy: (folderId: number, name: string, options?: IPageCopyOptions) => IPromise<IPageService>;
}
export declare class PageWrap {
    static $inject: string[];
    constructor(q: IQService, timeout: ITimeoutService, interval: IIntervalService, ippApi: IApiService, ippAuth: IAuthService, ippStorage: IStorageService, ippCrypto: ICryptoService, ippConf: IIPPConfig);
}



declare class PdfWrap {
    static $inject: string[];
    constructor(q: any, ippStorage: any, ippConf: any);
}
export default PdfWrap;

import * as request from "xhr";
export declare class Request {
    static _instance(): typeof request;
}

import { IIPPConfig } from "./Config";
export interface IStorageProvider {
    prefix: string;
    suffix: string;
    create: (key: string, value: string, expireDays?: number) => void;
    save: (key: string, value: string, expireDays?: number) => void;
    get: (key: string, defaultValue?: any) => any;
    remove: (key: string) => void;
}
export interface IStorageService {
    user: IStorageProvider;
    global: IStorageProvider;
    persistent: IStorageProvider;
}
export declare class StorageService {
    static $inject: string[];
    constructor(ippConfig: IIPPConfig);
}

import Emitter from "./Emitter";
declare class Table extends Emitter {
    readonly ON_CELL_CLICKED: string;
    readonly ON_CELL_DOUBLE_CLICKED: string;
    readonly ON_TAG_CLICKED: string;
    readonly ON_CELL_CHANGED: string;
    data: any;
    accessRanges: any;
    initialized: boolean;
    dirty: boolean;
    rendering: boolean;
    tags: boolean;
    isTouch: boolean;
    container: string;
    containerEl: any;
    viewEl: any;
    zoomEl: any;
    scrollEl: any;
    tableEl: any;
    tableHeadEl: any;
    tableBodyEl: any;
    tableFrozenRowsEl: any;
    tableFrozenColsEl: any;
    private validStyles;
    private _editing;
    private _tracking;
    private numOfCells;
    private classy;
    private helpers;
    private freeze;
    private freezeRange;
    private accessRangesClassNames;
    private clicked;
    constructor(container: string);
    editing: boolean;
    tracking: boolean;
    init(): boolean;
    render(data: any): boolean;
    update(data: any, content: any): boolean;
    getData(): any;
    setData(data: any): void;
    setAccessRanges(rangeAccess: any): void;
    setRanges(ranges: any, userId?: number): void;
    generateBlankData(): any;
    destroy(): void;
    getEditCell(): any;
    addRow(index: number, data: any): void;
    getRowIndex(): number;
    addColumn(): void;
    getColumnIndex(): number;
    setEditCell(el: any): any;
    freezePanes(): void;
    private renderRow(rowId, rowData);
    private renderCells(row, cellsData, startIndex);
    private setCellValues(cell, cellsData);
    private clickCellLink;
    private processLink(linkData);
    private getCellValue(cellData, escapeHtml);
    private onTableClick;
    private onWindowScroll;
    private showInput(cell);
    private nextCellInput(cell, direction?);
    private getClickedCell(clickedEl);
    private softMerges(rowEl);
    private updateFreezePanes(target);
    private clearCellClassNames(classNames);
}
export default Table;

declare function timers(func: any, delay: any): void;
declare function intervals(func: any, delay: any): void;
export { timers, intervals };

interface IData {
    content_diff: any;
    modified_by: any;
    modified_by_timestamp: any;
}
export declare class Tracking {
    private $interval;
    private storage;
    static $inject: string[];
    history: any;
    historyFirst: any;
    private _enabled;
    private _interval;
    private _length;
    constructor($interval: any, storage: any);
    createInstance(): Tracking;
    enabled: boolean;
    addHistory(pageId: number, data: IData, first?: boolean): void;
    getHistory(pageId: number): any;
    getHistoryForCell(pageId: number, cellRow: number, cellCol: number): any;
    restore(): any;
    reset(): any;
    cancel(): void;
    private totalChanges;
}
export default Tracking;

import { IPageContent } from "./Page/Content";
export interface IUtils {
    parseApiError: (err: any, def: string) => string;
    clonePageContent: (content: IPageContent) => IPageContent;
}
declare class Utils implements IUtils {
    constructor();
    parseApiError(err: any, def: string): string;
    clonePageContent(content: IPageContent): IPageContent;
    comparePageContent(currentContent: any, newContent: any, ignore_styles?: boolean): any;
}
export default Utils;


export {};

export {};
