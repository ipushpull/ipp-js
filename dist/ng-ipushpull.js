(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["ipushpull"] = factory();
	else
		root["ipushpull"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 14);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var dateFormat = __webpack_require__(22);
var Emitter_1 = __webpack_require__(1);
var $q, api;
var RangesWrap = (function () {
    function RangesWrap(q, ippApi) {
        $q = q;
        api = ippApi;
    }
    return RangesWrap;
}());
exports.RangesWrap = RangesWrap;
var AccessRange = (function () {
    function AccessRange(name, range, config) {
        this.name = name;
        this.range = range;
        this.config = config;
        this.rights = {};
        this.exp = config.exp;
        this.rights = config.rights;
        this.permission = config.permission;
        if (config.range === "self") {
            this.updateRange = {
                from: "R:C",
                to: "R:C"
            };
        }
        else {
            this.updateRange = {
                from: config.range.start,
                to: config.range.end
            };
        }
    }
    AccessRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: this.range.from.row + ":" + this.range.from.col,
            end: this.range.to.row + ":" + this.range.to.col,
            exp: this.exp,
            permission: this.permission,
            range: {
                start: "" + this.updateRange.from,
                end: "" + this.updateRange.to
            },
            rights: this.rights,
            freeze: false,
            style: false,
            button: false,
            notification: false,
            access: true
        };
        return range;
    };
    return AccessRange;
}());
exports.AccessRange = AccessRange;
var NotificationRange = (function () {
    function NotificationRange(name, range, config) {
        this.name = name;
        this.range = range;
        this.config = config;
        this.exp = config.exp;
        this.message = config.message;
    }
    NotificationRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: this.range.from.row + ":" + this.range.from.col,
            end: this.range.to.row + ":" + this.range.to.col,
            exp: this.exp,
            message: this.message,
            freeze: false,
            style: false,
            button: false,
            notification: true
        };
        return range;
    };
    return NotificationRange;
}());
exports.NotificationRange = NotificationRange;
var ListRange = (function () {
    function ListRange(name, range, config) {
        this.name = name;
        this.range = range;
        this.config = config;
        this.exp = config.exp;
        this.message = config.message;
    }
    ListRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: this.range.from.row + ":" + this.range.from.col,
            end: this.range.to.row + ":" + this.range.to.col,
            exp: this.exp,
            message: this.message,
            freeze: false,
            style: false,
            button: false,
            notification: true
        };
        return range;
    };
    return ListRange;
}());
exports.ListRange = ListRange;
var StyleRange = (function () {
    function StyleRange(name, range, config) {
        this.name = name;
        this.range = range;
        this.config = config;
        this.rangeExp = false;
        this.exp = config.exp;
        this.formatting = config.formatting;
    }
    StyleRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: this.range.from.row + ":" + this.range.from.col,
            end: this.range.to.row + ":" + this.range.to.col,
            exp: this.exp,
            formatting: this.formatting,
            freeze: false,
            button: false,
            style: true
        };
        return range;
    };
    StyleRange.prototype.parseFormat = function (format, row, col, rowEnd, colEnd) {
        if (rowEnd === void 0) { rowEnd = -1; }
        if (colEnd === void 0) { colEnd = -1; }
        var start = format.start
            .replace(/R/gi, row)
            .replace(/C/gi, col)
            .split(":");
        var end = format.end
            .replace(/R/gi, row)
            .replace(/C/gi, col)
            .split(":");
        var startRow = parseInt(eval(start[0]), 10);
        var startCol = parseInt(eval(start[1]), 10);
        var endRow = parseInt(eval(end[0]), 10);
        var endCol = parseInt(eval(end[1]), 10);
        return {
            from: {
                row: startRow,
                col: startCol
            },
            to: {
                row: endRow < 0 ? rowEnd : endRow,
                col: endCol < 0 ? colEnd : endCol
            },
            style: format.style
        };
    };
    StyleRange.prototype.match = function (value) {
        var VALUE = parseFloat(value) || value;
        try {
            return eval(this.exp);
        }
        catch (e) {
            return false;
        }
    };
    return StyleRange;
}());
exports.StyleRange = StyleRange;
var ButtonRange = (function () {
    function ButtonRange(name, range, config) {
        this.name = name;
        this.range = range;
        this.config = config;
        this.rangeExp = false;
        this.exp = config.exp;
        this.setters = config.setters;
        if (config.range === "self") {
            this.updateRange = {
                from: "R:C",
                to: "R:C"
            };
        }
        else {
            this.updateRange = {
                from: config.range.start,
                to: config.range.end
            };
        }
    }
    ButtonRange.prototype.run = function (cells, vars) {
        var _this = this;
        if (vars === void 0) { vars = {}; }
        var cellUpdates = [];
        cells.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                cellUpdates.push(_this.data(rowIndex, colIndex, col, vars));
            });
        });
        return cellUpdates;
    };
    ButtonRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: this.range.from.row + ":" + this.range.from.col,
            end: this.range.to.row + ":" + this.range.to.col,
            exp: this.exp,
            setters: this.setters,
            range: {
                start: "" + this.updateRange.from,
                end: "" + this.updateRange.to
            },
            rangeExp: this.rangeExp,
            freeze: false,
            style: false,
            button: true
        };
        if (this.config.range === "self") {
            range.range = "self";
        }
        return range;
    };
    ButtonRange.prototype.parseRange = function (pos) {
        var fromRow = this.updateRange.from
            .split(":")[0]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        var fromCol = this.updateRange.from
            .split(":")[1]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        var toRow = this.updateRange.to
            .split(":")[0]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        var toCol = this.updateRange.to
            .split(":")[1]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        return {
            from: {
                row: eval(fromRow),
                col: eval(fromCol)
            },
            to: {
                row: eval(toRow),
                col: eval(toCol)
            }
        };
    };
    ButtonRange.prototype.actionRotate = function (value, arr) {
        if (!arr || !arr.length) {
            return value;
        }
        var n = value;
        for (var i = 0; i < arr.length; i++) {
            if (n !== arr[i]) {
                continue;
            }
            if (!arr[i + 1]) {
                n = arr[0];
            }
            else {
                n = arr[i + 1];
            }
            break;
        }
        if (n === value) {
            n = arr[0];
        }
        return n;
    };
    ButtonRange.prototype.actionToday = function (format) {
        if (format === void 0) { format = "yyyy-mm-dd"; }
        var now = new Date();
        return dateFormat(now, format);
    };
    ButtonRange.prototype.actionLink = function (value, url) {
        if (url === void 0) { url = ""; }
        window.open(url, "_blank");
        return value;
    };
    ButtonRange.prototype.actionConcat = function (separator, arr) {
        if (typeof arr !== "object") {
            arr = [];
            for (var i = 1; i < arguments.length; i++) {
                arr.push("" + arguments[i]);
            }
        }
        return arr.join(separator);
    };
    ButtonRange.prototype.data = function (row, col, cell, vars) {
        if (vars === void 0) { vars = {}; }
        var data = {
            value: cell.value,
            formatted_value: cell.formatted_value
        };
        vars["=CONCAT"] = "this.actionConcat";
        vars["=ROTATE"] = "this.actionRotate";
        vars["=TODAY"] = "this.actionToday";
        vars["=LINK\\("] = "this.actionLink(VALUE, ";
        var exp = "" + this.exp;
        for (var key in vars) {
            exp = exp.replace(new RegExp(key, "g"), vars[key]);
        }
        var VALUE = !/^[0-9\.\-]+$/.test(cell.value) || isNaN(cell.value) ? cell.value : parseFloat(cell.value);
        try {
            VALUE = eval(exp);
        }
        catch (e) {
            VALUE = VALUE;
        }
        data.value = VALUE;
        data.formatted_value = VALUE;
        return {
            row: row,
            col: col,
            data: data
        };
    };
    return ButtonRange;
}());
exports.ButtonRange = ButtonRange;
var PermissionRange = (function () {
    function PermissionRange(name, rowStart, rowEnd, colStart, colEnd, permissions) {
        if (rowStart === void 0) { rowStart = 0; }
        if (rowEnd === void 0) { rowEnd = 0; }
        if (colStart === void 0) { colStart = 0; }
        if (colEnd === void 0) { colEnd = 0; }
        this.name = name;
        this.rowStart = rowStart;
        this.rowEnd = rowEnd;
        this.colStart = colStart;
        this.colEnd = colEnd;
        this._permissions = {
            ro: [],
            no: []
        };
        if (permissions) {
            this._permissions = permissions;
        }
    }
    PermissionRange.prototype.setPermission = function (userId, permission) {
        if (this._permissions.ro.indexOf(userId) >= 0) {
            this._permissions.ro.splice(this._permissions.ro.indexOf(userId), 1);
        }
        if (this._permissions.no.indexOf(userId) >= 0) {
            this._permissions.no.splice(this._permissions.no.indexOf(userId), 1);
        }
        if (permission) {
            this._permissions[permission].push(userId);
        }
    };
    PermissionRange.prototype.getPermission = function (userId) {
        var permission = "";
        if (this._permissions.ro.indexOf(userId) >= 0) {
            permission = "ro";
        }
        else if (this._permissions.no.indexOf(userId) >= 0) {
            permission = "no";
        }
        return permission;
    };
    PermissionRange.prototype.toObject = function () {
        return {
            name: this.name,
            start: this.rowStart + ":" + this.colStart,
            end: this.rowEnd + ":" + this.colEnd,
            rights: this._permissions,
            freeze: false,
            button: false,
            style: false,
            access: false,
            notification: false,
            list: false
        };
    };
    return PermissionRange;
}());
exports.PermissionRange = PermissionRange;
var FreezingRange = (function () {
    function FreezingRange(name, subject, count) {
        if (subject === void 0) { subject = "rows"; }
        if (count === void 0) { count = 1; }
        this.name = name;
        this.subject = subject;
        this.count = count;
    }
    Object.defineProperty(FreezingRange, "SUBJECT_ROWS", {
        get: function () {
            return "rows";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FreezingRange, "SUBJECT_COLUMNS", {
        get: function () {
            return "cols";
        },
        enumerable: true,
        configurable: true
    });
    FreezingRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: "0:0",
            end: "",
            rights: { ro: [], no: [] },
            freeze: true,
            button: false,
            style: false
        };
        if (this.subject === FreezingRange.SUBJECT_ROWS) {
            range.end = this.count - 1 + ":-1";
        }
        else {
            range.end = "-1:" + (this.count - 1);
        }
        return range;
    };
    return FreezingRange;
}());
exports.FreezingRange = FreezingRange;
var Ranges = (function (_super) {
    __extends(Ranges, _super);
    function Ranges(folderId, pageId, pageAccessRights) {
        var _this = _super.call(this) || this;
        _this._ranges = [];
        _this._folderId = folderId;
        _this._pageId = pageId;
        if (pageAccessRights) {
            _this.parse(pageAccessRights);
        }
        return _this;
    }
    Object.defineProperty(Ranges.prototype, "TYPE_PERMISSION_RANGE", {
        get: function () {
            return "permissions";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ranges.prototype, "TYPE_FREEZING_RANGE", {
        get: function () {
            return "freezing";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ranges.prototype, "EVENT_UPDATED", {
        get: function () {
            return "updated";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ranges.prototype, "ranges", {
        get: function () {
            return this._ranges;
        },
        enumerable: true,
        configurable: true
    });
    Ranges.prototype.setRanges = function (ranges) {
        this._ranges = ranges;
        return this;
    };
    Ranges.prototype.addRange = function (range) {
        if (range instanceof FreezingRange) {
            for (var i = 0; i < this._ranges.length; i++) {
                if (this._ranges[i].subject === range.subject) {
                    this.removeRange(this._ranges[i].name);
                    break;
                }
            }
        }
        var nameUnique = false;
        var newName = range.name;
        var count = 1;
        while (!nameUnique) {
            nameUnique = true;
            for (var i = 0; i < this._ranges.length; i++) {
                if (this._ranges[i].name === newName) {
                    nameUnique = false;
                    newName = range.name + "_" + count;
                    count++;
                }
            }
        }
        range.name = newName;
        this._ranges.push(range);
        return this;
    };
    Ranges.prototype.removeRange = function (rangeName) {
        var index = -1;
        for (var i = 0; i < this._ranges.length; i++) {
            if (this._ranges[i].name === rangeName) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            this._ranges.splice(index, 1);
        }
        return this;
    };
    Ranges.prototype.save = function () {
        var _this = this;
        var q = $q.defer();
        var ranges = [];
        for (var i = 0; i < this._ranges.length; i++) {
            ranges.push(this._ranges[i].toObject());
        }
        var requestData = {
            domainId: this._folderId,
            pageId: this._pageId,
            data: {
                range_access: ranges.length ? JSON.stringify(ranges) : '',
            }
        };
        api.savePageSettings(requestData).then(function (data) {
            _this.emit(_this.EVENT_UPDATED);
            q.resolve(data);
        }, q.reject);
        return q.promise;
    };
    Ranges.prototype.parse = function (pageAccessRights) {
        var ar = JSON.parse(pageAccessRights);
        this._ranges = [];
        for (var i = 0; i < ar.length; i++) {
            if (ar[i].data) {
                continue;
            }
            var range = {
                from: {
                    row: parseInt(ar[i].start.split(":")[0], 10),
                    col: parseInt(ar[i].start.split(":")[1], 10)
                },
                to: {
                    row: parseInt(ar[i].end.split(":")[0], 10),
                    col: parseInt(ar[i].end.split(":")[1], 10)
                }
            };
            if (ar[i].style) {
                this._ranges.push(new StyleRange(ar[i].name, range, ar[i]));
            }
            else if (ar[i].notification) {
                this._ranges.push(new NotificationRange(ar[i].name, range, ar[i]));
            }
            else if (ar[i].list) {
                this._ranges.push(new ListRange(ar[i].name, range, ar[i]));
            }
            else if (ar[i].access) {
                this._ranges.push(new AccessRange(ar[i].name, range, ar[i]));
            }
            else if (ar[i].button) {
                this._ranges.push(new ButtonRange(ar[i].name, range, ar[i]));
            }
            else if (ar[i].freeze) {
                var subject = range.to.col >= 0 ? "cols" : "rows";
                var count = range.to.col >= 0 ? range.to.col + 1 : range.to.row + 1;
                this._ranges.push(new FreezingRange(ar[i].name, subject, count));
            }
            else {
                this._ranges.push(new PermissionRange(ar[i].name, range.from.row, range.to.row, range.from.col, range.to.col, ar[i].rights));
            }
        }
        return this._ranges;
    };
    return Ranges;
}(Emitter_1.default));
exports.Ranges = Ranges;
//# sourceMappingURL=Range.js.map

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Emitter = (function () {
    function Emitter() {
        var _this = this;
        this.listeners = [];
        this.onListeners = {};
        this.on = function (name, callback) {
            if (!_this.onListeners[name]) {
                _this.onListeners[name] = [];
            }
            _this.onListeners[name].push(callback);
        };
        this.off = function (name, callback) {
            if (!_this.onListeners[name]) {
                return;
            }
            var i = _this.onListeners[name].indexOf(callback);
            if (i > -1) {
                _this.onListeners[name].splice(i, 1);
            }
        };
        this.register = function (callback) {
            _this.listeners.push(callback);
        };
        return;
    }
    Emitter.prototype.unRegister = function (callback) {
        var i = this.listeners.indexOf(callback);
        if (i > -1) {
            this.listeners.splice(i, 1);
        }
    };
    Emitter.prototype.emit = function (name, args) {
        this.listeners.forEach2(function (cb) {
            cb(name, args);
        });
        if (this.onListeners[name]) {
            var length_1 = this.onListeners[name].length * 1;
            for (var i = 0; i < length_1; i++) {
                if (this.onListeners[name] && this.onListeners[name][i]) {
                    this.onListeners[name][i](args);
                }
            }
        }
    };
    Emitter.prototype.removeEvent = function () {
        this.listeners = [];
        this.onListeners = {};
    };
    return Emitter;
}());
exports.default = Emitter;
//# sourceMappingURL=Emitter.js.map

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {/*!
 * @name JavaScript/NodeJS Merge v1.2.0
 * @author yeikos
 * @repository https://github.com/yeikos/js.merge

 * Copyright 2014 yeikos - MIT license
 * https://raw.github.com/yeikos/js.merge/master/LICENSE
 */

;(function(isNode) {

	/**
	 * Merge one or more objects 
	 * @param bool? clone
	 * @param mixed,... arguments
	 * @return object
	 */

	var Public = function(clone) {

		return merge(clone === true, false, arguments);

	}, publicName = 'merge';

	/**
	 * Merge two or more objects recursively 
	 * @param bool? clone
	 * @param mixed,... arguments
	 * @return object
	 */

	Public.recursive = function(clone) {

		return merge(clone === true, true, arguments);

	};

	/**
	 * Clone the input removing any reference
	 * @param mixed input
	 * @return mixed
	 */

	Public.clone = function(input) {

		var output = input,
			type = typeOf(input),
			index, size;

		if (type === 'array') {

			output = [];
			size = input.length;

			for (index=0;index<size;++index)

				output[index] = Public.clone(input[index]);

		} else if (type === 'object') {

			output = {};

			for (index in input)

				output[index] = Public.clone(input[index]);

		}

		return output;

	};

	/**
	 * Merge two objects recursively
	 * @param mixed input
	 * @param mixed extend
	 * @return mixed
	 */

	function merge_recursive(base, extend) {

		if (typeOf(base) !== 'object')

			return extend;

		for (var key in extend) {

			if (typeOf(base[key]) === 'object' && typeOf(extend[key]) === 'object') {

				base[key] = merge_recursive(base[key], extend[key]);

			} else {

				base[key] = extend[key];

			}

		}

		return base;

	}

	/**
	 * Merge two or more objects
	 * @param bool clone
	 * @param bool recursive
	 * @param array argv
	 * @return object
	 */

	function merge(clone, recursive, argv) {

		var result = argv[0],
			size = argv.length;

		if (clone || typeOf(result) !== 'object')

			result = {};

		for (var index=0;index<size;++index) {

			var item = argv[index],

				type = typeOf(item);

			if (type !== 'object') continue;

			for (var key in item) {

				var sitem = clone ? Public.clone(item[key]) : item[key];

				if (recursive) {

					result[key] = merge_recursive(result[key], sitem);

				} else {

					result[key] = sitem;

				}

			}

		}

		return result;

	}

	/**
	 * Get type of variable
	 * @param mixed input
	 * @return string
	 *
	 * @see http://jsperf.com/typeofvar
	 */

	function typeOf(input) {

		return ({}).toString.call(input).slice(8, -1).toLowerCase();

	}

	if (isNode) {

		module.exports = Public;

	} else {

		window[publicName] = Public;

	}

})(typeof module === 'object' && module && typeof module.exports === 'object' && module.exports);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(8)(module)))

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, module) {var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;//     Underscore.js 1.9.1
//     http://underscorejs.org
//     (c) 2009-2018 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.

(function() {

  // Baseline setup
  // --------------

  // Establish the root object, `window` (`self`) in the browser, `global`
  // on the server, or `this` in some virtual machines. We use `self`
  // instead of `window` for `WebWorker` support.
  var root = typeof self == 'object' && self.self === self && self ||
            typeof global == 'object' && global.global === global && global ||
            this ||
            {};

  // Save the previous value of the `_` variable.
  var previousUnderscore = root._;

  // Save bytes in the minified (but not gzipped) version:
  var ArrayProto = Array.prototype, ObjProto = Object.prototype;
  var SymbolProto = typeof Symbol !== 'undefined' ? Symbol.prototype : null;

  // Create quick reference variables for speed access to core prototypes.
  var push = ArrayProto.push,
      slice = ArrayProto.slice,
      toString = ObjProto.toString,
      hasOwnProperty = ObjProto.hasOwnProperty;

  // All **ECMAScript 5** native function implementations that we hope to use
  // are declared here.
  var nativeIsArray = Array.isArray,
      nativeKeys = Object.keys,
      nativeCreate = Object.create;

  // Naked function reference for surrogate-prototype-swapping.
  var Ctor = function(){};

  // Create a safe reference to the Underscore object for use below.
  var _ = function(obj) {
    if (obj instanceof _) return obj;
    if (!(this instanceof _)) return new _(obj);
    this._wrapped = obj;
  };

  // Export the Underscore object for **Node.js**, with
  // backwards-compatibility for their old module API. If we're in
  // the browser, add `_` as a global object.
  // (`nodeType` is checked to ensure that `module`
  // and `exports` are not HTML elements.)
  if (typeof exports != 'undefined' && !exports.nodeType) {
    if (typeof module != 'undefined' && !module.nodeType && module.exports) {
      exports = module.exports = _;
    }
    exports._ = _;
  } else {
    root._ = _;
  }

  // Current version.
  _.VERSION = '1.9.1';

  // Internal function that returns an efficient (for current engines) version
  // of the passed-in callback, to be repeatedly applied in other Underscore
  // functions.
  var optimizeCb = function(func, context, argCount) {
    if (context === void 0) return func;
    switch (argCount == null ? 3 : argCount) {
      case 1: return function(value) {
        return func.call(context, value);
      };
      // The 2-argument case is omitted because we’re not using it.
      case 3: return function(value, index, collection) {
        return func.call(context, value, index, collection);
      };
      case 4: return function(accumulator, value, index, collection) {
        return func.call(context, accumulator, value, index, collection);
      };
    }
    return function() {
      return func.apply(context, arguments);
    };
  };

  var builtinIteratee;

  // An internal function to generate callbacks that can be applied to each
  // element in a collection, returning the desired result — either `identity`,
  // an arbitrary callback, a property matcher, or a property accessor.
  var cb = function(value, context, argCount) {
    if (_.iteratee !== builtinIteratee) return _.iteratee(value, context);
    if (value == null) return _.identity;
    if (_.isFunction(value)) return optimizeCb(value, context, argCount);
    if (_.isObject(value) && !_.isArray(value)) return _.matcher(value);
    return _.property(value);
  };

  // External wrapper for our callback generator. Users may customize
  // `_.iteratee` if they want additional predicate/iteratee shorthand styles.
  // This abstraction hides the internal-only argCount argument.
  _.iteratee = builtinIteratee = function(value, context) {
    return cb(value, context, Infinity);
  };

  // Some functions take a variable number of arguments, or a few expected
  // arguments at the beginning and then a variable number of values to operate
  // on. This helper accumulates all remaining arguments past the function’s
  // argument length (or an explicit `startIndex`), into an array that becomes
  // the last argument. Similar to ES6’s "rest parameter".
  var restArguments = function(func, startIndex) {
    startIndex = startIndex == null ? func.length - 1 : +startIndex;
    return function() {
      var length = Math.max(arguments.length - startIndex, 0),
          rest = Array(length),
          index = 0;
      for (; index < length; index++) {
        rest[index] = arguments[index + startIndex];
      }
      switch (startIndex) {
        case 0: return func.call(this, rest);
        case 1: return func.call(this, arguments[0], rest);
        case 2: return func.call(this, arguments[0], arguments[1], rest);
      }
      var args = Array(startIndex + 1);
      for (index = 0; index < startIndex; index++) {
        args[index] = arguments[index];
      }
      args[startIndex] = rest;
      return func.apply(this, args);
    };
  };

  // An internal function for creating a new object that inherits from another.
  var baseCreate = function(prototype) {
    if (!_.isObject(prototype)) return {};
    if (nativeCreate) return nativeCreate(prototype);
    Ctor.prototype = prototype;
    var result = new Ctor;
    Ctor.prototype = null;
    return result;
  };

  var shallowProperty = function(key) {
    return function(obj) {
      return obj == null ? void 0 : obj[key];
    };
  };

  var has = function(obj, path) {
    return obj != null && hasOwnProperty.call(obj, path);
  }

  var deepGet = function(obj, path) {
    var length = path.length;
    for (var i = 0; i < length; i++) {
      if (obj == null) return void 0;
      obj = obj[path[i]];
    }
    return length ? obj : void 0;
  };

  // Helper for collection methods to determine whether a collection
  // should be iterated as an array or as an object.
  // Related: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
  // Avoids a very nasty iOS 8 JIT bug on ARM-64. #2094
  var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
  var getLength = shallowProperty('length');
  var isArrayLike = function(collection) {
    var length = getLength(collection);
    return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
  };

  // Collection Functions
  // --------------------

  // The cornerstone, an `each` implementation, aka `forEach`.
  // Handles raw objects in addition to array-likes. Treats all
  // sparse array-likes as if they were dense.
  _.each = _.forEach = function(obj, iteratee, context) {
    iteratee = optimizeCb(iteratee, context);
    var i, length;
    if (isArrayLike(obj)) {
      for (i = 0, length = obj.length; i < length; i++) {
        iteratee(obj[i], i, obj);
      }
    } else {
      var keys = _.keys(obj);
      for (i = 0, length = keys.length; i < length; i++) {
        iteratee(obj[keys[i]], keys[i], obj);
      }
    }
    return obj;
  };

  // Return the results of applying the iteratee to each element.
  _.map = _.collect = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length,
        results = Array(length);
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      results[index] = iteratee(obj[currentKey], currentKey, obj);
    }
    return results;
  };

  // Create a reducing function iterating left or right.
  var createReduce = function(dir) {
    // Wrap code that reassigns argument variables in a separate function than
    // the one that accesses `arguments.length` to avoid a perf hit. (#1991)
    var reducer = function(obj, iteratee, memo, initial) {
      var keys = !isArrayLike(obj) && _.keys(obj),
          length = (keys || obj).length,
          index = dir > 0 ? 0 : length - 1;
      if (!initial) {
        memo = obj[keys ? keys[index] : index];
        index += dir;
      }
      for (; index >= 0 && index < length; index += dir) {
        var currentKey = keys ? keys[index] : index;
        memo = iteratee(memo, obj[currentKey], currentKey, obj);
      }
      return memo;
    };

    return function(obj, iteratee, memo, context) {
      var initial = arguments.length >= 3;
      return reducer(obj, optimizeCb(iteratee, context, 4), memo, initial);
    };
  };

  // **Reduce** builds up a single result from a list of values, aka `inject`,
  // or `foldl`.
  _.reduce = _.foldl = _.inject = createReduce(1);

  // The right-associative version of reduce, also known as `foldr`.
  _.reduceRight = _.foldr = createReduce(-1);

  // Return the first value which passes a truth test. Aliased as `detect`.
  _.find = _.detect = function(obj, predicate, context) {
    var keyFinder = isArrayLike(obj) ? _.findIndex : _.findKey;
    var key = keyFinder(obj, predicate, context);
    if (key !== void 0 && key !== -1) return obj[key];
  };

  // Return all the elements that pass a truth test.
  // Aliased as `select`.
  _.filter = _.select = function(obj, predicate, context) {
    var results = [];
    predicate = cb(predicate, context);
    _.each(obj, function(value, index, list) {
      if (predicate(value, index, list)) results.push(value);
    });
    return results;
  };

  // Return all the elements for which a truth test fails.
  _.reject = function(obj, predicate, context) {
    return _.filter(obj, _.negate(cb(predicate)), context);
  };

  // Determine whether all of the elements match a truth test.
  // Aliased as `all`.
  _.every = _.all = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length;
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      if (!predicate(obj[currentKey], currentKey, obj)) return false;
    }
    return true;
  };

  // Determine if at least one element in the object matches a truth test.
  // Aliased as `any`.
  _.some = _.any = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length;
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      if (predicate(obj[currentKey], currentKey, obj)) return true;
    }
    return false;
  };

  // Determine if the array or object contains a given item (using `===`).
  // Aliased as `includes` and `include`.
  _.contains = _.includes = _.include = function(obj, item, fromIndex, guard) {
    if (!isArrayLike(obj)) obj = _.values(obj);
    if (typeof fromIndex != 'number' || guard) fromIndex = 0;
    return _.indexOf(obj, item, fromIndex) >= 0;
  };

  // Invoke a method (with arguments) on every item in a collection.
  _.invoke = restArguments(function(obj, path, args) {
    var contextPath, func;
    if (_.isFunction(path)) {
      func = path;
    } else if (_.isArray(path)) {
      contextPath = path.slice(0, -1);
      path = path[path.length - 1];
    }
    return _.map(obj, function(context) {
      var method = func;
      if (!method) {
        if (contextPath && contextPath.length) {
          context = deepGet(context, contextPath);
        }
        if (context == null) return void 0;
        method = context[path];
      }
      return method == null ? method : method.apply(context, args);
    });
  });

  // Convenience version of a common use case of `map`: fetching a property.
  _.pluck = function(obj, key) {
    return _.map(obj, _.property(key));
  };

  // Convenience version of a common use case of `filter`: selecting only objects
  // containing specific `key:value` pairs.
  _.where = function(obj, attrs) {
    return _.filter(obj, _.matcher(attrs));
  };

  // Convenience version of a common use case of `find`: getting the first object
  // containing specific `key:value` pairs.
  _.findWhere = function(obj, attrs) {
    return _.find(obj, _.matcher(attrs));
  };

  // Return the maximum element (or element-based computation).
  _.max = function(obj, iteratee, context) {
    var result = -Infinity, lastComputed = -Infinity,
        value, computed;
    if (iteratee == null || typeof iteratee == 'number' && typeof obj[0] != 'object' && obj != null) {
      obj = isArrayLike(obj) ? obj : _.values(obj);
      for (var i = 0, length = obj.length; i < length; i++) {
        value = obj[i];
        if (value != null && value > result) {
          result = value;
        }
      }
    } else {
      iteratee = cb(iteratee, context);
      _.each(obj, function(v, index, list) {
        computed = iteratee(v, index, list);
        if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
          result = v;
          lastComputed = computed;
        }
      });
    }
    return result;
  };

  // Return the minimum element (or element-based computation).
  _.min = function(obj, iteratee, context) {
    var result = Infinity, lastComputed = Infinity,
        value, computed;
    if (iteratee == null || typeof iteratee == 'number' && typeof obj[0] != 'object' && obj != null) {
      obj = isArrayLike(obj) ? obj : _.values(obj);
      for (var i = 0, length = obj.length; i < length; i++) {
        value = obj[i];
        if (value != null && value < result) {
          result = value;
        }
      }
    } else {
      iteratee = cb(iteratee, context);
      _.each(obj, function(v, index, list) {
        computed = iteratee(v, index, list);
        if (computed < lastComputed || computed === Infinity && result === Infinity) {
          result = v;
          lastComputed = computed;
        }
      });
    }
    return result;
  };

  // Shuffle a collection.
  _.shuffle = function(obj) {
    return _.sample(obj, Infinity);
  };

  // Sample **n** random values from a collection using the modern version of the
  // [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisher–Yates_shuffle).
  // If **n** is not specified, returns a single random element.
  // The internal `guard` argument allows it to work with `map`.
  _.sample = function(obj, n, guard) {
    if (n == null || guard) {
      if (!isArrayLike(obj)) obj = _.values(obj);
      return obj[_.random(obj.length - 1)];
    }
    var sample = isArrayLike(obj) ? _.clone(obj) : _.values(obj);
    var length = getLength(sample);
    n = Math.max(Math.min(n, length), 0);
    var last = length - 1;
    for (var index = 0; index < n; index++) {
      var rand = _.random(index, last);
      var temp = sample[index];
      sample[index] = sample[rand];
      sample[rand] = temp;
    }
    return sample.slice(0, n);
  };

  // Sort the object's values by a criterion produced by an iteratee.
  _.sortBy = function(obj, iteratee, context) {
    var index = 0;
    iteratee = cb(iteratee, context);
    return _.pluck(_.map(obj, function(value, key, list) {
      return {
        value: value,
        index: index++,
        criteria: iteratee(value, key, list)
      };
    }).sort(function(left, right) {
      var a = left.criteria;
      var b = right.criteria;
      if (a !== b) {
        if (a > b || a === void 0) return 1;
        if (a < b || b === void 0) return -1;
      }
      return left.index - right.index;
    }), 'value');
  };

  // An internal function used for aggregate "group by" operations.
  var group = function(behavior, partition) {
    return function(obj, iteratee, context) {
      var result = partition ? [[], []] : {};
      iteratee = cb(iteratee, context);
      _.each(obj, function(value, index) {
        var key = iteratee(value, index, obj);
        behavior(result, value, key);
      });
      return result;
    };
  };

  // Groups the object's values by a criterion. Pass either a string attribute
  // to group by, or a function that returns the criterion.
  _.groupBy = group(function(result, value, key) {
    if (has(result, key)) result[key].push(value); else result[key] = [value];
  });

  // Indexes the object's values by a criterion, similar to `groupBy`, but for
  // when you know that your index values will be unique.
  _.indexBy = group(function(result, value, key) {
    result[key] = value;
  });

  // Counts instances of an object that group by a certain criterion. Pass
  // either a string attribute to count by, or a function that returns the
  // criterion.
  _.countBy = group(function(result, value, key) {
    if (has(result, key)) result[key]++; else result[key] = 1;
  });

  var reStrSymbol = /[^\ud800-\udfff]|[\ud800-\udbff][\udc00-\udfff]|[\ud800-\udfff]/g;
  // Safely create a real, live array from anything iterable.
  _.toArray = function(obj) {
    if (!obj) return [];
    if (_.isArray(obj)) return slice.call(obj);
    if (_.isString(obj)) {
      // Keep surrogate pair characters together
      return obj.match(reStrSymbol);
    }
    if (isArrayLike(obj)) return _.map(obj, _.identity);
    return _.values(obj);
  };

  // Return the number of elements in an object.
  _.size = function(obj) {
    if (obj == null) return 0;
    return isArrayLike(obj) ? obj.length : _.keys(obj).length;
  };

  // Split a collection into two arrays: one whose elements all satisfy the given
  // predicate, and one whose elements all do not satisfy the predicate.
  _.partition = group(function(result, value, pass) {
    result[pass ? 0 : 1].push(value);
  }, true);

  // Array Functions
  // ---------------

  // Get the first element of an array. Passing **n** will return the first N
  // values in the array. Aliased as `head` and `take`. The **guard** check
  // allows it to work with `_.map`.
  _.first = _.head = _.take = function(array, n, guard) {
    if (array == null || array.length < 1) return n == null ? void 0 : [];
    if (n == null || guard) return array[0];
    return _.initial(array, array.length - n);
  };

  // Returns everything but the last entry of the array. Especially useful on
  // the arguments object. Passing **n** will return all the values in
  // the array, excluding the last N.
  _.initial = function(array, n, guard) {
    return slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
  };

  // Get the last element of an array. Passing **n** will return the last N
  // values in the array.
  _.last = function(array, n, guard) {
    if (array == null || array.length < 1) return n == null ? void 0 : [];
    if (n == null || guard) return array[array.length - 1];
    return _.rest(array, Math.max(0, array.length - n));
  };

  // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
  // Especially useful on the arguments object. Passing an **n** will return
  // the rest N values in the array.
  _.rest = _.tail = _.drop = function(array, n, guard) {
    return slice.call(array, n == null || guard ? 1 : n);
  };

  // Trim out all falsy values from an array.
  _.compact = function(array) {
    return _.filter(array, Boolean);
  };

  // Internal implementation of a recursive `flatten` function.
  var flatten = function(input, shallow, strict, output) {
    output = output || [];
    var idx = output.length;
    for (var i = 0, length = getLength(input); i < length; i++) {
      var value = input[i];
      if (isArrayLike(value) && (_.isArray(value) || _.isArguments(value))) {
        // Flatten current level of array or arguments object.
        if (shallow) {
          var j = 0, len = value.length;
          while (j < len) output[idx++] = value[j++];
        } else {
          flatten(value, shallow, strict, output);
          idx = output.length;
        }
      } else if (!strict) {
        output[idx++] = value;
      }
    }
    return output;
  };

  // Flatten out an array, either recursively (by default), or just one level.
  _.flatten = function(array, shallow) {
    return flatten(array, shallow, false);
  };

  // Return a version of the array that does not contain the specified value(s).
  _.without = restArguments(function(array, otherArrays) {
    return _.difference(array, otherArrays);
  });

  // Produce a duplicate-free version of the array. If the array has already
  // been sorted, you have the option of using a faster algorithm.
  // The faster algorithm will not work with an iteratee if the iteratee
  // is not a one-to-one function, so providing an iteratee will disable
  // the faster algorithm.
  // Aliased as `unique`.
  _.uniq = _.unique = function(array, isSorted, iteratee, context) {
    if (!_.isBoolean(isSorted)) {
      context = iteratee;
      iteratee = isSorted;
      isSorted = false;
    }
    if (iteratee != null) iteratee = cb(iteratee, context);
    var result = [];
    var seen = [];
    for (var i = 0, length = getLength(array); i < length; i++) {
      var value = array[i],
          computed = iteratee ? iteratee(value, i, array) : value;
      if (isSorted && !iteratee) {
        if (!i || seen !== computed) result.push(value);
        seen = computed;
      } else if (iteratee) {
        if (!_.contains(seen, computed)) {
          seen.push(computed);
          result.push(value);
        }
      } else if (!_.contains(result, value)) {
        result.push(value);
      }
    }
    return result;
  };

  // Produce an array that contains the union: each distinct element from all of
  // the passed-in arrays.
  _.union = restArguments(function(arrays) {
    return _.uniq(flatten(arrays, true, true));
  });

  // Produce an array that contains every item shared between all the
  // passed-in arrays.
  _.intersection = function(array) {
    var result = [];
    var argsLength = arguments.length;
    for (var i = 0, length = getLength(array); i < length; i++) {
      var item = array[i];
      if (_.contains(result, item)) continue;
      var j;
      for (j = 1; j < argsLength; j++) {
        if (!_.contains(arguments[j], item)) break;
      }
      if (j === argsLength) result.push(item);
    }
    return result;
  };

  // Take the difference between one array and a number of other arrays.
  // Only the elements present in just the first array will remain.
  _.difference = restArguments(function(array, rest) {
    rest = flatten(rest, true, true);
    return _.filter(array, function(value){
      return !_.contains(rest, value);
    });
  });

  // Complement of _.zip. Unzip accepts an array of arrays and groups
  // each array's elements on shared indices.
  _.unzip = function(array) {
    var length = array && _.max(array, getLength).length || 0;
    var result = Array(length);

    for (var index = 0; index < length; index++) {
      result[index] = _.pluck(array, index);
    }
    return result;
  };

  // Zip together multiple lists into a single array -- elements that share
  // an index go together.
  _.zip = restArguments(_.unzip);

  // Converts lists into objects. Pass either a single array of `[key, value]`
  // pairs, or two parallel arrays of the same length -- one of keys, and one of
  // the corresponding values. Passing by pairs is the reverse of _.pairs.
  _.object = function(list, values) {
    var result = {};
    for (var i = 0, length = getLength(list); i < length; i++) {
      if (values) {
        result[list[i]] = values[i];
      } else {
        result[list[i][0]] = list[i][1];
      }
    }
    return result;
  };

  // Generator function to create the findIndex and findLastIndex functions.
  var createPredicateIndexFinder = function(dir) {
    return function(array, predicate, context) {
      predicate = cb(predicate, context);
      var length = getLength(array);
      var index = dir > 0 ? 0 : length - 1;
      for (; index >= 0 && index < length; index += dir) {
        if (predicate(array[index], index, array)) return index;
      }
      return -1;
    };
  };

  // Returns the first index on an array-like that passes a predicate test.
  _.findIndex = createPredicateIndexFinder(1);
  _.findLastIndex = createPredicateIndexFinder(-1);

  // Use a comparator function to figure out the smallest index at which
  // an object should be inserted so as to maintain order. Uses binary search.
  _.sortedIndex = function(array, obj, iteratee, context) {
    iteratee = cb(iteratee, context, 1);
    var value = iteratee(obj);
    var low = 0, high = getLength(array);
    while (low < high) {
      var mid = Math.floor((low + high) / 2);
      if (iteratee(array[mid]) < value) low = mid + 1; else high = mid;
    }
    return low;
  };

  // Generator function to create the indexOf and lastIndexOf functions.
  var createIndexFinder = function(dir, predicateFind, sortedIndex) {
    return function(array, item, idx) {
      var i = 0, length = getLength(array);
      if (typeof idx == 'number') {
        if (dir > 0) {
          i = idx >= 0 ? idx : Math.max(idx + length, i);
        } else {
          length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
        }
      } else if (sortedIndex && idx && length) {
        idx = sortedIndex(array, item);
        return array[idx] === item ? idx : -1;
      }
      if (item !== item) {
        idx = predicateFind(slice.call(array, i, length), _.isNaN);
        return idx >= 0 ? idx + i : -1;
      }
      for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
        if (array[idx] === item) return idx;
      }
      return -1;
    };
  };

  // Return the position of the first occurrence of an item in an array,
  // or -1 if the item is not included in the array.
  // If the array is large and already in sort order, pass `true`
  // for **isSorted** to use binary search.
  _.indexOf = createIndexFinder(1, _.findIndex, _.sortedIndex);
  _.lastIndexOf = createIndexFinder(-1, _.findLastIndex);

  // Generate an integer Array containing an arithmetic progression. A port of
  // the native Python `range()` function. See
  // [the Python documentation](http://docs.python.org/library/functions.html#range).
  _.range = function(start, stop, step) {
    if (stop == null) {
      stop = start || 0;
      start = 0;
    }
    if (!step) {
      step = stop < start ? -1 : 1;
    }

    var length = Math.max(Math.ceil((stop - start) / step), 0);
    var range = Array(length);

    for (var idx = 0; idx < length; idx++, start += step) {
      range[idx] = start;
    }

    return range;
  };

  // Chunk a single array into multiple arrays, each containing `count` or fewer
  // items.
  _.chunk = function(array, count) {
    if (count == null || count < 1) return [];
    var result = [];
    var i = 0, length = array.length;
    while (i < length) {
      result.push(slice.call(array, i, i += count));
    }
    return result;
  };

  // Function (ahem) Functions
  // ------------------

  // Determines whether to execute a function as a constructor
  // or a normal function with the provided arguments.
  var executeBound = function(sourceFunc, boundFunc, context, callingContext, args) {
    if (!(callingContext instanceof boundFunc)) return sourceFunc.apply(context, args);
    var self = baseCreate(sourceFunc.prototype);
    var result = sourceFunc.apply(self, args);
    if (_.isObject(result)) return result;
    return self;
  };

  // Create a function bound to a given object (assigning `this`, and arguments,
  // optionally). Delegates to **ECMAScript 5**'s native `Function.bind` if
  // available.
  _.bind = restArguments(function(func, context, args) {
    if (!_.isFunction(func)) throw new TypeError('Bind must be called on a function');
    var bound = restArguments(function(callArgs) {
      return executeBound(func, bound, context, this, args.concat(callArgs));
    });
    return bound;
  });

  // Partially apply a function by creating a version that has had some of its
  // arguments pre-filled, without changing its dynamic `this` context. _ acts
  // as a placeholder by default, allowing any combination of arguments to be
  // pre-filled. Set `_.partial.placeholder` for a custom placeholder argument.
  _.partial = restArguments(function(func, boundArgs) {
    var placeholder = _.partial.placeholder;
    var bound = function() {
      var position = 0, length = boundArgs.length;
      var args = Array(length);
      for (var i = 0; i < length; i++) {
        args[i] = boundArgs[i] === placeholder ? arguments[position++] : boundArgs[i];
      }
      while (position < arguments.length) args.push(arguments[position++]);
      return executeBound(func, bound, this, this, args);
    };
    return bound;
  });

  _.partial.placeholder = _;

  // Bind a number of an object's methods to that object. Remaining arguments
  // are the method names to be bound. Useful for ensuring that all callbacks
  // defined on an object belong to it.
  _.bindAll = restArguments(function(obj, keys) {
    keys = flatten(keys, false, false);
    var index = keys.length;
    if (index < 1) throw new Error('bindAll must be passed function names');
    while (index--) {
      var key = keys[index];
      obj[key] = _.bind(obj[key], obj);
    }
  });

  // Memoize an expensive function by storing its results.
  _.memoize = function(func, hasher) {
    var memoize = function(key) {
      var cache = memoize.cache;
      var address = '' + (hasher ? hasher.apply(this, arguments) : key);
      if (!has(cache, address)) cache[address] = func.apply(this, arguments);
      return cache[address];
    };
    memoize.cache = {};
    return memoize;
  };

  // Delays a function for the given number of milliseconds, and then calls
  // it with the arguments supplied.
  _.delay = restArguments(function(func, wait, args) {
    return setTimeout(function() {
      return func.apply(null, args);
    }, wait);
  });

  // Defers a function, scheduling it to run after the current call stack has
  // cleared.
  _.defer = _.partial(_.delay, _, 1);

  // Returns a function, that, when invoked, will only be triggered at most once
  // during a given window of time. Normally, the throttled function will run
  // as much as it can, without ever going more than once per `wait` duration;
  // but if you'd like to disable the execution on the leading edge, pass
  // `{leading: false}`. To disable execution on the trailing edge, ditto.
  _.throttle = function(func, wait, options) {
    var timeout, context, args, result;
    var previous = 0;
    if (!options) options = {};

    var later = function() {
      previous = options.leading === false ? 0 : _.now();
      timeout = null;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    };

    var throttled = function() {
      var now = _.now();
      if (!previous && options.leading === false) previous = now;
      var remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0 || remaining > wait) {
        if (timeout) {
          clearTimeout(timeout);
          timeout = null;
        }
        previous = now;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
      } else if (!timeout && options.trailing !== false) {
        timeout = setTimeout(later, remaining);
      }
      return result;
    };

    throttled.cancel = function() {
      clearTimeout(timeout);
      previous = 0;
      timeout = context = args = null;
    };

    return throttled;
  };

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  _.debounce = function(func, wait, immediate) {
    var timeout, result;

    var later = function(context, args) {
      timeout = null;
      if (args) result = func.apply(context, args);
    };

    var debounced = restArguments(function(args) {
      if (timeout) clearTimeout(timeout);
      if (immediate) {
        var callNow = !timeout;
        timeout = setTimeout(later, wait);
        if (callNow) result = func.apply(this, args);
      } else {
        timeout = _.delay(later, wait, this, args);
      }

      return result;
    });

    debounced.cancel = function() {
      clearTimeout(timeout);
      timeout = null;
    };

    return debounced;
  };

  // Returns the first function passed as an argument to the second,
  // allowing you to adjust arguments, run code before and after, and
  // conditionally execute the original function.
  _.wrap = function(func, wrapper) {
    return _.partial(wrapper, func);
  };

  // Returns a negated version of the passed-in predicate.
  _.negate = function(predicate) {
    return function() {
      return !predicate.apply(this, arguments);
    };
  };

  // Returns a function that is the composition of a list of functions, each
  // consuming the return value of the function that follows.
  _.compose = function() {
    var args = arguments;
    var start = args.length - 1;
    return function() {
      var i = start;
      var result = args[start].apply(this, arguments);
      while (i--) result = args[i].call(this, result);
      return result;
    };
  };

  // Returns a function that will only be executed on and after the Nth call.
  _.after = function(times, func) {
    return function() {
      if (--times < 1) {
        return func.apply(this, arguments);
      }
    };
  };

  // Returns a function that will only be executed up to (but not including) the Nth call.
  _.before = function(times, func) {
    var memo;
    return function() {
      if (--times > 0) {
        memo = func.apply(this, arguments);
      }
      if (times <= 1) func = null;
      return memo;
    };
  };

  // Returns a function that will be executed at most one time, no matter how
  // often you call it. Useful for lazy initialization.
  _.once = _.partial(_.before, 2);

  _.restArguments = restArguments;

  // Object Functions
  // ----------------

  // Keys in IE < 9 that won't be iterated by `for key in ...` and thus missed.
  var hasEnumBug = !{toString: null}.propertyIsEnumerable('toString');
  var nonEnumerableProps = ['valueOf', 'isPrototypeOf', 'toString',
    'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];

  var collectNonEnumProps = function(obj, keys) {
    var nonEnumIdx = nonEnumerableProps.length;
    var constructor = obj.constructor;
    var proto = _.isFunction(constructor) && constructor.prototype || ObjProto;

    // Constructor is a special case.
    var prop = 'constructor';
    if (has(obj, prop) && !_.contains(keys, prop)) keys.push(prop);

    while (nonEnumIdx--) {
      prop = nonEnumerableProps[nonEnumIdx];
      if (prop in obj && obj[prop] !== proto[prop] && !_.contains(keys, prop)) {
        keys.push(prop);
      }
    }
  };

  // Retrieve the names of an object's own properties.
  // Delegates to **ECMAScript 5**'s native `Object.keys`.
  _.keys = function(obj) {
    if (!_.isObject(obj)) return [];
    if (nativeKeys) return nativeKeys(obj);
    var keys = [];
    for (var key in obj) if (has(obj, key)) keys.push(key);
    // Ahem, IE < 9.
    if (hasEnumBug) collectNonEnumProps(obj, keys);
    return keys;
  };

  // Retrieve all the property names of an object.
  _.allKeys = function(obj) {
    if (!_.isObject(obj)) return [];
    var keys = [];
    for (var key in obj) keys.push(key);
    // Ahem, IE < 9.
    if (hasEnumBug) collectNonEnumProps(obj, keys);
    return keys;
  };

  // Retrieve the values of an object's properties.
  _.values = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var values = Array(length);
    for (var i = 0; i < length; i++) {
      values[i] = obj[keys[i]];
    }
    return values;
  };

  // Returns the results of applying the iteratee to each element of the object.
  // In contrast to _.map it returns an object.
  _.mapObject = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var keys = _.keys(obj),
        length = keys.length,
        results = {};
    for (var index = 0; index < length; index++) {
      var currentKey = keys[index];
      results[currentKey] = iteratee(obj[currentKey], currentKey, obj);
    }
    return results;
  };

  // Convert an object into a list of `[key, value]` pairs.
  // The opposite of _.object.
  _.pairs = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var pairs = Array(length);
    for (var i = 0; i < length; i++) {
      pairs[i] = [keys[i], obj[keys[i]]];
    }
    return pairs;
  };

  // Invert the keys and values of an object. The values must be serializable.
  _.invert = function(obj) {
    var result = {};
    var keys = _.keys(obj);
    for (var i = 0, length = keys.length; i < length; i++) {
      result[obj[keys[i]]] = keys[i];
    }
    return result;
  };

  // Return a sorted list of the function names available on the object.
  // Aliased as `methods`.
  _.functions = _.methods = function(obj) {
    var names = [];
    for (var key in obj) {
      if (_.isFunction(obj[key])) names.push(key);
    }
    return names.sort();
  };

  // An internal function for creating assigner functions.
  var createAssigner = function(keysFunc, defaults) {
    return function(obj) {
      var length = arguments.length;
      if (defaults) obj = Object(obj);
      if (length < 2 || obj == null) return obj;
      for (var index = 1; index < length; index++) {
        var source = arguments[index],
            keys = keysFunc(source),
            l = keys.length;
        for (var i = 0; i < l; i++) {
          var key = keys[i];
          if (!defaults || obj[key] === void 0) obj[key] = source[key];
        }
      }
      return obj;
    };
  };

  // Extend a given object with all the properties in passed-in object(s).
  _.extend = createAssigner(_.allKeys);

  // Assigns a given object with all the own properties in the passed-in object(s).
  // (https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
  _.extendOwn = _.assign = createAssigner(_.keys);

  // Returns the first key on an object that passes a predicate test.
  _.findKey = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = _.keys(obj), key;
    for (var i = 0, length = keys.length; i < length; i++) {
      key = keys[i];
      if (predicate(obj[key], key, obj)) return key;
    }
  };

  // Internal pick helper function to determine if `obj` has key `key`.
  var keyInObj = function(value, key, obj) {
    return key in obj;
  };

  // Return a copy of the object only containing the whitelisted properties.
  _.pick = restArguments(function(obj, keys) {
    var result = {}, iteratee = keys[0];
    if (obj == null) return result;
    if (_.isFunction(iteratee)) {
      if (keys.length > 1) iteratee = optimizeCb(iteratee, keys[1]);
      keys = _.allKeys(obj);
    } else {
      iteratee = keyInObj;
      keys = flatten(keys, false, false);
      obj = Object(obj);
    }
    for (var i = 0, length = keys.length; i < length; i++) {
      var key = keys[i];
      var value = obj[key];
      if (iteratee(value, key, obj)) result[key] = value;
    }
    return result;
  });

  // Return a copy of the object without the blacklisted properties.
  _.omit = restArguments(function(obj, keys) {
    var iteratee = keys[0], context;
    if (_.isFunction(iteratee)) {
      iteratee = _.negate(iteratee);
      if (keys.length > 1) context = keys[1];
    } else {
      keys = _.map(flatten(keys, false, false), String);
      iteratee = function(value, key) {
        return !_.contains(keys, key);
      };
    }
    return _.pick(obj, iteratee, context);
  });

  // Fill in a given object with default properties.
  _.defaults = createAssigner(_.allKeys, true);

  // Creates an object that inherits from the given prototype object.
  // If additional properties are provided then they will be added to the
  // created object.
  _.create = function(prototype, props) {
    var result = baseCreate(prototype);
    if (props) _.extendOwn(result, props);
    return result;
  };

  // Create a (shallow-cloned) duplicate of an object.
  _.clone = function(obj) {
    if (!_.isObject(obj)) return obj;
    return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
  };

  // Invokes interceptor with the obj, and then returns obj.
  // The primary purpose of this method is to "tap into" a method chain, in
  // order to perform operations on intermediate results within the chain.
  _.tap = function(obj, interceptor) {
    interceptor(obj);
    return obj;
  };

  // Returns whether an object has a given set of `key:value` pairs.
  _.isMatch = function(object, attrs) {
    var keys = _.keys(attrs), length = keys.length;
    if (object == null) return !length;
    var obj = Object(object);
    for (var i = 0; i < length; i++) {
      var key = keys[i];
      if (attrs[key] !== obj[key] || !(key in obj)) return false;
    }
    return true;
  };


  // Internal recursive comparison function for `isEqual`.
  var eq, deepEq;
  eq = function(a, b, aStack, bStack) {
    // Identical objects are equal. `0 === -0`, but they aren't identical.
    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
    if (a === b) return a !== 0 || 1 / a === 1 / b;
    // `null` or `undefined` only equal to itself (strict comparison).
    if (a == null || b == null) return false;
    // `NaN`s are equivalent, but non-reflexive.
    if (a !== a) return b !== b;
    // Exhaust primitive checks
    var type = typeof a;
    if (type !== 'function' && type !== 'object' && typeof b != 'object') return false;
    return deepEq(a, b, aStack, bStack);
  };

  // Internal recursive comparison function for `isEqual`.
  deepEq = function(a, b, aStack, bStack) {
    // Unwrap any wrapped objects.
    if (a instanceof _) a = a._wrapped;
    if (b instanceof _) b = b._wrapped;
    // Compare `[[Class]]` names.
    var className = toString.call(a);
    if (className !== toString.call(b)) return false;
    switch (className) {
      // Strings, numbers, regular expressions, dates, and booleans are compared by value.
      case '[object RegExp]':
      // RegExps are coerced to strings for comparison (Note: '' + /a/i === '/a/i')
      case '[object String]':
        // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
        // equivalent to `new String("5")`.
        return '' + a === '' + b;
      case '[object Number]':
        // `NaN`s are equivalent, but non-reflexive.
        // Object(NaN) is equivalent to NaN.
        if (+a !== +a) return +b !== +b;
        // An `egal` comparison is performed for other numeric values.
        return +a === 0 ? 1 / +a === 1 / b : +a === +b;
      case '[object Date]':
      case '[object Boolean]':
        // Coerce dates and booleans to numeric primitive values. Dates are compared by their
        // millisecond representations. Note that invalid dates with millisecond representations
        // of `NaN` are not equivalent.
        return +a === +b;
      case '[object Symbol]':
        return SymbolProto.valueOf.call(a) === SymbolProto.valueOf.call(b);
    }

    var areArrays = className === '[object Array]';
    if (!areArrays) {
      if (typeof a != 'object' || typeof b != 'object') return false;

      // Objects with different constructors are not equivalent, but `Object`s or `Array`s
      // from different frames are.
      var aCtor = a.constructor, bCtor = b.constructor;
      if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor &&
                               _.isFunction(bCtor) && bCtor instanceof bCtor)
                          && ('constructor' in a && 'constructor' in b)) {
        return false;
      }
    }
    // Assume equality for cyclic structures. The algorithm for detecting cyclic
    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.

    // Initializing stack of traversed objects.
    // It's done here since we only need them for objects and arrays comparison.
    aStack = aStack || [];
    bStack = bStack || [];
    var length = aStack.length;
    while (length--) {
      // Linear search. Performance is inversely proportional to the number of
      // unique nested structures.
      if (aStack[length] === a) return bStack[length] === b;
    }

    // Add the first object to the stack of traversed objects.
    aStack.push(a);
    bStack.push(b);

    // Recursively compare objects and arrays.
    if (areArrays) {
      // Compare array lengths to determine if a deep comparison is necessary.
      length = a.length;
      if (length !== b.length) return false;
      // Deep compare the contents, ignoring non-numeric properties.
      while (length--) {
        if (!eq(a[length], b[length], aStack, bStack)) return false;
      }
    } else {
      // Deep compare objects.
      var keys = _.keys(a), key;
      length = keys.length;
      // Ensure that both objects contain the same number of properties before comparing deep equality.
      if (_.keys(b).length !== length) return false;
      while (length--) {
        // Deep compare each member
        key = keys[length];
        if (!(has(b, key) && eq(a[key], b[key], aStack, bStack))) return false;
      }
    }
    // Remove the first object from the stack of traversed objects.
    aStack.pop();
    bStack.pop();
    return true;
  };

  // Perform a deep comparison to check if two objects are equal.
  _.isEqual = function(a, b) {
    return eq(a, b);
  };

  // Is a given array, string, or object empty?
  // An "empty" object has no enumerable own-properties.
  _.isEmpty = function(obj) {
    if (obj == null) return true;
    if (isArrayLike(obj) && (_.isArray(obj) || _.isString(obj) || _.isArguments(obj))) return obj.length === 0;
    return _.keys(obj).length === 0;
  };

  // Is a given value a DOM element?
  _.isElement = function(obj) {
    return !!(obj && obj.nodeType === 1);
  };

  // Is a given value an array?
  // Delegates to ECMA5's native Array.isArray
  _.isArray = nativeIsArray || function(obj) {
    return toString.call(obj) === '[object Array]';
  };

  // Is a given variable an object?
  _.isObject = function(obj) {
    var type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
  };

  // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp, isError, isMap, isWeakMap, isSet, isWeakSet.
  _.each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error', 'Symbol', 'Map', 'WeakMap', 'Set', 'WeakSet'], function(name) {
    _['is' + name] = function(obj) {
      return toString.call(obj) === '[object ' + name + ']';
    };
  });

  // Define a fallback version of the method in browsers (ahem, IE < 9), where
  // there isn't any inspectable "Arguments" type.
  if (!_.isArguments(arguments)) {
    _.isArguments = function(obj) {
      return has(obj, 'callee');
    };
  }

  // Optimize `isFunction` if appropriate. Work around some typeof bugs in old v8,
  // IE 11 (#1621), Safari 8 (#1929), and PhantomJS (#2236).
  var nodelist = root.document && root.document.childNodes;
  if (typeof /./ != 'function' && typeof Int8Array != 'object' && typeof nodelist != 'function') {
    _.isFunction = function(obj) {
      return typeof obj == 'function' || false;
    };
  }

  // Is a given object a finite number?
  _.isFinite = function(obj) {
    return !_.isSymbol(obj) && isFinite(obj) && !isNaN(parseFloat(obj));
  };

  // Is the given value `NaN`?
  _.isNaN = function(obj) {
    return _.isNumber(obj) && isNaN(obj);
  };

  // Is a given value a boolean?
  _.isBoolean = function(obj) {
    return obj === true || obj === false || toString.call(obj) === '[object Boolean]';
  };

  // Is a given value equal to null?
  _.isNull = function(obj) {
    return obj === null;
  };

  // Is a given variable undefined?
  _.isUndefined = function(obj) {
    return obj === void 0;
  };

  // Shortcut function for checking if an object has a given property directly
  // on itself (in other words, not on a prototype).
  _.has = function(obj, path) {
    if (!_.isArray(path)) {
      return has(obj, path);
    }
    var length = path.length;
    for (var i = 0; i < length; i++) {
      var key = path[i];
      if (obj == null || !hasOwnProperty.call(obj, key)) {
        return false;
      }
      obj = obj[key];
    }
    return !!length;
  };

  // Utility Functions
  // -----------------

  // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
  // previous owner. Returns a reference to the Underscore object.
  _.noConflict = function() {
    root._ = previousUnderscore;
    return this;
  };

  // Keep the identity function around for default iteratees.
  _.identity = function(value) {
    return value;
  };

  // Predicate-generating functions. Often useful outside of Underscore.
  _.constant = function(value) {
    return function() {
      return value;
    };
  };

  _.noop = function(){};

  // Creates a function that, when passed an object, will traverse that object’s
  // properties down the given `path`, specified as an array of keys or indexes.
  _.property = function(path) {
    if (!_.isArray(path)) {
      return shallowProperty(path);
    }
    return function(obj) {
      return deepGet(obj, path);
    };
  };

  // Generates a function for a given object that returns a given property.
  _.propertyOf = function(obj) {
    if (obj == null) {
      return function(){};
    }
    return function(path) {
      return !_.isArray(path) ? obj[path] : deepGet(obj, path);
    };
  };

  // Returns a predicate for checking whether an object has a given set of
  // `key:value` pairs.
  _.matcher = _.matches = function(attrs) {
    attrs = _.extendOwn({}, attrs);
    return function(obj) {
      return _.isMatch(obj, attrs);
    };
  };

  // Run a function **n** times.
  _.times = function(n, iteratee, context) {
    var accum = Array(Math.max(0, n));
    iteratee = optimizeCb(iteratee, context, 1);
    for (var i = 0; i < n; i++) accum[i] = iteratee(i);
    return accum;
  };

  // Return a random integer between min and max (inclusive).
  _.random = function(min, max) {
    if (max == null) {
      max = min;
      min = 0;
    }
    return min + Math.floor(Math.random() * (max - min + 1));
  };

  // A (possibly faster) way to get the current timestamp as an integer.
  _.now = Date.now || function() {
    return new Date().getTime();
  };

  // List of HTML entities for escaping.
  var escapeMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '`': '&#x60;'
  };
  var unescapeMap = _.invert(escapeMap);

  // Functions for escaping and unescaping strings to/from HTML interpolation.
  var createEscaper = function(map) {
    var escaper = function(match) {
      return map[match];
    };
    // Regexes for identifying a key that needs to be escaped.
    var source = '(?:' + _.keys(map).join('|') + ')';
    var testRegexp = RegExp(source);
    var replaceRegexp = RegExp(source, 'g');
    return function(string) {
      string = string == null ? '' : '' + string;
      return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
    };
  };
  _.escape = createEscaper(escapeMap);
  _.unescape = createEscaper(unescapeMap);

  // Traverses the children of `obj` along `path`. If a child is a function, it
  // is invoked with its parent as context. Returns the value of the final
  // child, or `fallback` if any child is undefined.
  _.result = function(obj, path, fallback) {
    if (!_.isArray(path)) path = [path];
    var length = path.length;
    if (!length) {
      return _.isFunction(fallback) ? fallback.call(obj) : fallback;
    }
    for (var i = 0; i < length; i++) {
      var prop = obj == null ? void 0 : obj[path[i]];
      if (prop === void 0) {
        prop = fallback;
        i = length; // Ensure we don't continue iterating.
      }
      obj = _.isFunction(prop) ? prop.call(obj) : prop;
    }
    return obj;
  };

  // Generate a unique integer id (unique within the entire client session).
  // Useful for temporary DOM ids.
  var idCounter = 0;
  _.uniqueId = function(prefix) {
    var id = ++idCounter + '';
    return prefix ? prefix + id : id;
  };

  // By default, Underscore uses ERB-style template delimiters, change the
  // following template settings to use alternative delimiters.
  _.templateSettings = {
    evaluate: /<%([\s\S]+?)%>/g,
    interpolate: /<%=([\s\S]+?)%>/g,
    escape: /<%-([\s\S]+?)%>/g
  };

  // When customizing `templateSettings`, if you don't want to define an
  // interpolation, evaluation or escaping regex, we need one that is
  // guaranteed not to match.
  var noMatch = /(.)^/;

  // Certain characters need to be escaped so that they can be put into a
  // string literal.
  var escapes = {
    "'": "'",
    '\\': '\\',
    '\r': 'r',
    '\n': 'n',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  var escapeRegExp = /\\|'|\r|\n|\u2028|\u2029/g;

  var escapeChar = function(match) {
    return '\\' + escapes[match];
  };

  // JavaScript micro-templating, similar to John Resig's implementation.
  // Underscore templating handles arbitrary delimiters, preserves whitespace,
  // and correctly escapes quotes within interpolated code.
  // NB: `oldSettings` only exists for backwards compatibility.
  _.template = function(text, settings, oldSettings) {
    if (!settings && oldSettings) settings = oldSettings;
    settings = _.defaults({}, settings, _.templateSettings);

    // Combine delimiters into one regular expression via alternation.
    var matcher = RegExp([
      (settings.escape || noMatch).source,
      (settings.interpolate || noMatch).source,
      (settings.evaluate || noMatch).source
    ].join('|') + '|$', 'g');

    // Compile the template source, escaping string literals appropriately.
    var index = 0;
    var source = "__p+='";
    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
      source += text.slice(index, offset).replace(escapeRegExp, escapeChar);
      index = offset + match.length;

      if (escape) {
        source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
      } else if (interpolate) {
        source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
      } else if (evaluate) {
        source += "';\n" + evaluate + "\n__p+='";
      }

      // Adobe VMs need the match returned to produce the correct offset.
      return match;
    });
    source += "';\n";

    // If a variable is not specified, place data values in local scope.
    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

    source = "var __t,__p='',__j=Array.prototype.join," +
      "print=function(){__p+=__j.call(arguments,'');};\n" +
      source + 'return __p;\n';

    var render;
    try {
      render = new Function(settings.variable || 'obj', '_', source);
    } catch (e) {
      e.source = source;
      throw e;
    }

    var template = function(data) {
      return render.call(this, data, _);
    };

    // Provide the compiled source as a convenience for precompilation.
    var argument = settings.variable || 'obj';
    template.source = 'function(' + argument + '){\n' + source + '}';

    return template;
  };

  // Add a "chain" function. Start chaining a wrapped Underscore object.
  _.chain = function(obj) {
    var instance = _(obj);
    instance._chain = true;
    return instance;
  };

  // OOP
  // ---------------
  // If Underscore is called as a function, it returns a wrapped object that
  // can be used OO-style. This wrapper holds altered versions of all the
  // underscore functions. Wrapped objects may be chained.

  // Helper function to continue chaining intermediate results.
  var chainResult = function(instance, obj) {
    return instance._chain ? _(obj).chain() : obj;
  };

  // Add your own custom functions to the Underscore object.
  _.mixin = function(obj) {
    _.each(_.functions(obj), function(name) {
      var func = _[name] = obj[name];
      _.prototype[name] = function() {
        var args = [this._wrapped];
        push.apply(args, arguments);
        return chainResult(this, func.apply(_, args));
      };
    });
    return _;
  };

  // Add all of the Underscore functions to the wrapper object.
  _.mixin(_);

  // Add all mutator Array functions to the wrapper.
  _.each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      var obj = this._wrapped;
      method.apply(obj, arguments);
      if ((name === 'shift' || name === 'splice') && obj.length === 0) delete obj[0];
      return chainResult(this, obj);
    };
  });

  // Add all accessor Array functions to the wrapper.
  _.each(['concat', 'join', 'slice'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      return chainResult(this, method.apply(this._wrapped, arguments));
    };
  });

  // Extracts the result from a wrapped and chained object.
  _.prototype.value = function() {
    return this._wrapped;
  };

  // Provide unwrapping proxy for some methods used in engine operations
  // such as arithmetic and JSON stringification.
  _.prototype.valueOf = _.prototype.toJSON = _.prototype.value;

  _.prototype.toString = function() {
    return String(this._wrapped);
  };

  // AMD registration happens at the end for compatibility with AMD loaders
  // that may not enforce next-turn semantics on modules. Even though general
  // practice for AMD registration is to be anonymous, underscore registers
  // as a named module because, like jQuery, it is a base library that is
  // popular enough to be bundled in a third party lib, but not be part of
  // an AMD load request. Those cases could generate an error when an
  // anonymous define() is called outside of a loader request.
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function() {
      return _;
    }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  }
}());

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(9), __webpack_require__(8)(module)))

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var merge = __webpack_require__(2);
var _ = __webpack_require__(3);
var Utils = (function () {
    function Utils() {
        return;
    }
    Utils.prototype.parseApiError = function (err, def) {
        var msg = def;
        if (err.data) {
            var keys = Object.keys(err.data);
            if (keys.length) {
                if (_.isArray(err.data[keys[0]])) {
                    msg = err.data[keys[0]][0];
                }
                else if (typeof err.data[keys[0]] === "string") {
                    msg = err.data[keys[0]];
                }
                else {
                    msg = def;
                }
            }
            else {
                msg = def;
            }
        }
        else {
            msg = def;
        }
        return msg;
    };
    Utils.prototype.clonePageContent = function (content) {
        var copy = [];
        var deltaStyle = {};
        for (var i = 0; i < content.length; i++) {
            copy[i] = [];
            for (var j = 0; j < content[i].length; j++) {
                var cell = merge({}, content[i][j]);
                var style = {};
                for (var k in cell.style) {
                    if (cell.style[k] === deltaStyle[k])
                        continue;
                    style[k] = cell.style[k];
                }
                deltaStyle = merge({}, cell.style);
                cell.style = style;
                if (cell.index)
                    delete cell.index;
                if (cell.formatting)
                    delete cell.formatting;
                copy[i].push(cell);
            }
        }
        return copy;
    };
    Utils.prototype.comparePageContent = function (currentContent, newContent, ignore_styles) {
        if (ignore_styles === void 0) { ignore_styles = false; }
        if (!currentContent || !newContent) {
            return false;
        }
        var diffContent = [];
        var sizeDiff = false;
        for (var i = 0; i < newContent.length; i++) {
            for (var k = 0; k < newContent[i].length; k++) {
                var diff = false;
                if (currentContent[i] && currentContent[i][k]) {
                    if (newContent[i][k].value !== currentContent[i][k].value) {
                        diff = true;
                    }
                    if (!diff && !ignore_styles) {
                        for (var attr in newContent[i][k].style) {
                            if (newContent[i][k].style[attr] !== currentContent[i][k].style[attr]) {
                                diff = true;
                                if (['width', 'height'].indexOf(attr) > -1)
                                    sizeDiff = true;
                                break;
                            }
                        }
                    }
                }
                else {
                    diff = true;
                }
                if (diff) {
                    if (!diffContent[i]) {
                        diffContent[i] = [];
                    }
                    diffContent[i][k] = newContent[i][k];
                }
            }
        }
        if (diffContent.length || newContent.length < currentContent.length || newContent[0].length < currentContent[0].length) {
            var rowDiff = currentContent.length - newContent.length;
            var colDiff = currentContent[0].length - newContent[0].length;
            return { content: newContent, content_diff: diffContent, colDiff: colDiff, rowDiff: rowDiff, sizeDiff: sizeDiff };
        }
        return false;
    };
    Utils.prototype.mergePageContent = function (_original, _current, style, clean) {
        if (style === void 0) { style = {}; }
        if (clean === void 0) { clean = false; }
        var current = [];
        var colWidths = [];
        for (var rowIndex = 0; rowIndex < _original.length; rowIndex++) {
            var row = _original[rowIndex];
            current[rowIndex] = [];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = void 0;
                if (!clean && _current && _current[rowIndex] && _current[rowIndex][colIndex] && _current[rowIndex][colIndex].dirty) {
                    col = _current[rowIndex][colIndex];
                    style = merge({}, col.originalStyle || col.style);
                }
                else {
                    col = merge({}, row[colIndex]);
                    col.style = merge({}, style, col.style);
                    delete col.dirty;
                    style = merge({}, col.style);
                }
                if (!rowIndex) {
                    colWidths[colIndex] = col.style.width;
                }
                col.style.width = colWidths[colIndex];
                col.formatting = {};
                col.index = {
                    row: rowIndex,
                    col: colIndex
                };
                current[rowIndex].push(col);
            }
        }
        return current;
    };
    return Utils;
}());
exports.default = Utils;
//# sourceMappingURL=Utils.js.map

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var uuidV4 = __webpack_require__(7);
var Helpers = (function () {
    function Helpers() {
        this.letters = 'ABCDEFGHIJKLMNOPQRSTUVWXY';
        return;
    }
    Helpers.prototype.getUuid = function () {
        return uuidV4();
    };
    Helpers.prototype.getNextValueInArray = function (value, arr) {
        if (!arr || !arr.length) {
            return value;
        }
        var n = value;
        for (var i = 0; i < arr.length; i++) {
            if (n !== arr[i]) {
                continue;
            }
            if (!arr[i + 1]) {
                n = arr[0];
            }
            else {
                n = arr[i + 1];
            }
            break;
        }
        if (n === value) {
            n = arr[0];
        }
        return n;
    };
    Helpers.prototype.isNumber = function (value) {
        return !/^[0-9\.\-]+$/.test(value) || isNaN(value) ? false : true;
    };
    Helpers.prototype.quoteString = function (value, quote) {
        if (quote === void 0) { quote = '"'; }
        return this.isNumber(Number) ? parseFloat(value) : "" + quote + value + quote;
    };
    Helpers.prototype.getCellsReference = function (from, to, headingSelected) {
        var colFrom = this.toColumnName(from.col + 1);
        var rowFrom = from.row + 1;
        var colTo = this.toColumnName(to.col + 1);
        var rowTo = to.row + 1;
        if (headingSelected === 'all') {
            return '1:-1';
        }
        else if (headingSelected === 'col') {
            if (colFrom === colTo)
                return colFrom;
            return colFrom + ":" + colTo;
        }
        else if (headingSelected === 'row') {
            if (rowFrom === rowTo)
                return "" + rowFrom;
            return rowFrom + ":" + rowTo;
        }
        if (colFrom === colTo && rowFrom === rowTo) {
            return "" + colFrom + rowFrom;
        }
        if (!rowTo) {
            return "" + colFrom + rowFrom + ":" + colTo;
        }
        return "" + colFrom + rowFrom + ":" + colTo + rowTo;
    };
    Helpers.prototype.getRefIndex = function (str, obj) {
        var _this = this;
        str = str.toUpperCase();
        var regex = /[A-Z]/gm;
        var m;
        var col = -1;
        var count = 0;
        while ((m = regex.exec(str)) !== null) {
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            if (col < 0) {
                col = 0;
            }
            m.forEach(function (match) {
                var i = _this.letters.indexOf(match);
                col += i + count;
                count += 26;
            });
        }
        var numbers = str.match(/\d/g);
        var row = numbers ? parseFloat(numbers.join('')) - 1 : -1;
        return obj ? { row: row, col: col } : [row, col];
    };
    Helpers.prototype.toColumnName = function (num) {
        for (var ret = '', a = 1, b = 26; (num -= a) >= 0; a = b, b *= 26) {
            ret = String.fromCharCode(parseInt((num % b) / a) + 65) + ret;
        }
        return ret;
    };
    Helpers.prototype.cellRange = function (str, rows, cols) {
        var _a = str.split(':'), from = _a[0], to = _a[1];
        var fromRow = 0, fromCol = 0, toRow = 0, toCol = 0;
        var isFromNumber = this.isNumber(from);
        var fromCell = this.getRefIndex(from, true);
        if (to) {
            var isToNumber = this.isNumber(to);
            var toCell = this.getRefIndex(to, true);
            if (isFromNumber) {
                fromRow = parseInt(from) - 1;
                fromCol = 0;
            }
            else {
                fromCol = fromCell.col;
                fromRow = fromCell.row;
                if (fromCell.row < 0) {
                    fromRow = 0;
                }
            }
            if (isToNumber) {
                toRow = parseInt(to) < 0 ? rows - 1 : parseInt(to) - 1;
                toCol = cols - 1;
            }
            else {
                toRow = toCell.row < 0 ? -1 : toCell.row;
                toCol = toCell.col < 0 ? -1 : toCell.col;
            }
        }
        else {
            if (isFromNumber) {
                fromRow = parseInt(from) - 1;
                toRow = fromRow;
                fromCol = 0;
                toCol = cols - 1;
            }
            else {
                fromCol = fromCell.col;
                fromRow = fromCell.row;
                toCol = fromCol;
                if (fromCell.row < 0) {
                    fromRow = 0;
                    toRow = -1;
                }
                else {
                    toRow = fromRow;
                }
            }
        }
        return {
            from: { row: fromRow, col: fromCol },
            to: { row: toRow, col: toCol },
        };
    };
    Helpers.prototype.validHex = function (hex) {
        return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(hex);
    };
    Helpers.prototype.rgbToHex = function (rgb) {
        rgb = rgb
            .replace('rgba(', '')
            .replace('rgb(', '')
            .replace(')', '');
        var parts = rgb.split(',');
        if (parts.length === 4) {
            if (parts[3].trim() === '0') {
                return 'FFFFFF';
            }
        }
        return (this.componentToHex(parseInt(parts[0], 10)) +
            this.componentToHex(parseInt(parts[1], 10)) +
            this.componentToHex(parseInt(parts[2], 10)));
    };
    Helpers.prototype.componentToHex = function (c) {
        var hex = c.toString(16);
        return hex.length === 1 ? '0' + hex : hex;
    };
    Helpers.prototype.addEvent = function (element, eventName, func) {
        if (!element) {
            return;
        }
        if (element.addEventListener) {
            element.addEventListener(eventName, func, false);
            if (eventName === 'click') {
                element.addEventListener('touchstart', func, false);
            }
        }
        else if (element.attachEvent) {
            element.attachEvent('on' + eventName, func);
        }
    };
    Helpers.prototype.capitalizeFirstLetter = function (str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    };
    Helpers.prototype.removeEvent = function (element, eventName, func) {
        if (element.removeEventListener) {
            element.removeEventListener(eventName, func, false);
            if (eventName === 'click') {
                element.removeEventListener('touchstart', func, false);
            }
        }
        else if (element.detachEvent) {
            element.detachEvent('on' + eventName, func);
        }
    };
    Helpers.prototype.isTouch = function () {
        return 'ontouchstart' in document.documentElement;
    };
    Helpers.prototype.clickEvent = function () {
        return this.isTouch() ? 'touchstart' : 'click';
    };
    Helpers.prototype.openWindow = function (link, target, params) {
        if (target === void 0) { target = '_blank'; }
        if (params === void 0) { params = {}; }
        var paramsStr = '';
        for (var key in params) {
            if (!params.hasOwnProperty(key)) {
                continue;
            }
            paramsStr += key + "=" + params[key] + ",";
        }
        paramsStr = paramsStr.substring(0, paramsStr.length - 1);
        return window.open(link, target, paramsStr);
    };
    Helpers.prototype.createSlug = function (str) {
        return str.split(' ').join('_');
    };
    Helpers.prototype.getScrollbarWidth = function () {
        var outer = document.createElement('div');
        outer.style.visibility = 'hidden';
        outer.style.width = '100px';
        outer.style.msOverflowStyle = 'scrollbar';
        document.body.appendChild(outer);
        var widthNoScroll = outer.offsetWidth;
        outer.style.overflow = 'scroll';
        var inner = document.createElement('div');
        inner.style.width = '100%';
        outer.appendChild(inner);
        var widthWithScroll = inner.offsetWidth;
        outer.parentNode.removeChild(outer);
        return widthNoScroll - widthWithScroll;
    };
    Helpers.prototype.parseDateExcel = function (excelTimestamp) {
        var secondsInDay = 24 * 60 * 60;
        var excelEpoch = new Date(1899, 11, 31);
        var excelEpochAsUnixTimestamp = excelEpoch.getTime();
        var missingLeapYearDay = secondsInDay * 1000;
        var delta = excelEpochAsUnixTimestamp - missingLeapYearDay;
        var excelTimestampAsUnixTimestamp = excelTimestamp * secondsInDay * 1000;
        var parsed = excelTimestampAsUnixTimestamp + delta;
        return isNaN(parsed) ? null : parsed;
    };
    Helpers.prototype.getContrastYIQ = function (hexcolor) {
        hexcolor = hexcolor.replace('#', '');
        var r = parseInt(hexcolor.substr(0, 2), 16);
        var g = parseInt(hexcolor.substr(2, 2), 16);
        var b = parseInt(hexcolor.substr(4, 2), 16);
        var yiq = (r * 299 + g * 587 + b * 114) / 1000;
        return yiq >= 128 ? 'light' : 'dark';
    };
    Helpers.prototype.convertTime = function (timestamp) {
        if (typeof timestamp === 'string') {
            timestamp = new Date(timestamp);
        }
        var hours = timestamp.getHours();
        if (hours < 10) {
            hours = "0" + hours;
        }
        var minutes = timestamp.getMinutes();
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        var seconds = timestamp.getSeconds();
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        return hours + ":" + minutes + ":" + seconds;
    };
    Helpers.prototype.isValidEmail = function (str) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(str);
    };
    Helpers.prototype.safeTitle = function (str, replaceWith) {
        if (replaceWith === void 0) { replaceWith = '_'; }
        return str.replace(/[^a-zA-Z0-9_\.\-]/g, replaceWith);
    };
    Helpers.prototype.safeXmlChars = function (str) {
        var regex = /<[\w](.*?)\/>/gm;
        var m;
        var ignore = [];
        while ((m = regex.exec(str)) !== null) {
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            ignore.push(m[0]);
        }
        ignore.forEach(function (s, i) {
            str = str.replace(s, "[$" + i + "]");
        });
        str = str
            .replace(/&/g, '&amp;')
            .replace(/>/g, '&gt;')
            .replace(/</g, '&lt;')
            .replace(/'/g, '&apos;')
            .replace(/"/g, '&quot;');
        ignore.forEach(function (s, i) {
            str = str.replace("[$" + i + "]", s);
        });
        return str;
    };
    Helpers.prototype.isTarget = function (target, element) {
        var clickedEl = element;
        while (clickedEl && clickedEl !== target) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === target;
    };
    return Helpers;
}());
exports.default = Helpers;
//# sourceMappingURL=Helpers.js.map

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = __webpack_require__(1);
var $q, $timeout, api, auth, storage, config;
var Providers = (function () {
    function Providers(q, timeout, ippApi, ippAuth, ippStorage, ippConf) {
        $q = q;
        $timeout = timeout;
        api = ippApi;
        auth = ippAuth;
        storage = ippStorage;
        config = ippConf;
    }
    return Providers;
}());
exports.Providers = Providers;
var ProviderREST = (function (_super) {
    __extends(ProviderREST, _super);
    function ProviderREST(_pageId, _folderId, _uuid) {
        var _this = _super.call(this) || this;
        _this._pageId = _pageId;
        _this._folderId = _folderId;
        _this._uuid = _uuid;
        _this._stopped = false;
        _this._requestOngoing = false;
        _this._timeout = 1000;
        _this._seqNo = 0;
        _this._error = false;
        _this.start();
        return _this;
    }
    Object.defineProperty(ProviderREST.prototype, "seqNo", {
        set: function (seqNo) { this._seqNo = seqNo; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderREST.prototype, "requestOngoing", {
        set: function (value) { this._requestOngoing = value; },
        enumerable: true,
        configurable: true
    });
    ProviderREST.prototype.start = function () {
        this._stopped = false;
        this.startPolling();
    };
    ProviderREST.prototype.stop = function () {
        this._stopped = true;
        $timeout.cancel(this._timer);
    };
    ProviderREST.prototype.destroy = function () {
        this.stop();
        this.removeEvent();
    };
    ProviderREST.prototype.getData = function () {
        return this._data;
    };
    ProviderREST.prototype.startPolling = function () {
        var _this = this;
        this.load();
        this._timer = $timeout(function () {
            _this.startPolling();
        }, this._timeout);
    };
    ProviderREST.prototype.load = function (ignoreSeqNo) {
        var _this = this;
        if (ignoreSeqNo === void 0) { ignoreSeqNo = false; }
        var q = $q.defer();
        if (this._requestOngoing || this._stopped) {
            q.reject({});
            return q.promise;
        }
        this._requestOngoing = true;
        var success = function (res) {
            if (res.httpCode === 200 || res.httpCode === 204) {
                if (res.httpCode === 200) {
                    _this._seqNo = res.data.seq_no;
                    _this._timeout = res.data.pull_interval * 1000;
                    _this._data = res.data;
                    _this.emit("content_update", _this.tempGetContentOb(res.data));
                    _this.emit("meta_update", _this.tempGetSettingsOb(res.data));
                }
                else {
                    _this.emit("empty_update");
                }
                _this._error = false;
                q.resolve(res.data);
            }
            else {
                _this.onError(res.data);
                q.reject({});
            }
        };
        if (this._uuid) {
            api.getPageByUuid({
                uuid: this._uuid,
                seq_no: (!ignoreSeqNo) ? this._seqNo : undefined,
            }).then(success, function (err) {
                _this.onError(err);
                q.reject(err);
            }).finally(function () {
                _this._requestOngoing = false;
            });
        }
        else {
            api.getPage({
                domainId: this._folderId,
                pageId: this._pageId,
                seq_no: (!ignoreSeqNo) ? this._seqNo : undefined,
            }).then(success, function (err) {
                _this.onError(err);
                q.reject(err);
            }).finally(function () {
                _this._requestOngoing = false;
            });
        }
        return q.promise;
    };
    ProviderREST.prototype.onError = function (err) {
        this._error = true;
        this.emit("error", err);
    };
    ProviderREST.prototype.tempGetContentOb = function (data) {
        return {
            id: data.id,
            background_color: data.background_color,
            domain_id: data.domain_id,
            seq_no: data.seq_no,
            content_modified_timestamp: data.content_modified_timestamp,
            content: data.content,
            content_modified_by: data.content_modified_by,
            push_interval: data.push_interval,
            pull_interval: data.pull_interval,
            is_public: data.is_public,
            description: data.description,
            encrypted_content: data.encrypted_content,
            encryption_key_used: data.encryption_key_used,
            encryption_type_used: data.encryption_type_used,
            record_history: data.record_history,
            special_page_type: data.special_page_type,
            symphony_sid: data.symphony_sid,
            show_gridlines: data.show_gridlines,
        };
    };
    ProviderREST.prototype.tempGetSettingsOb = function (data) {
        return JSON.parse(JSON.stringify(data));
    };
    return ProviderREST;
}(Emitter_1.default));
exports.ProviderREST = ProviderREST;
var ProviderSocket = (function (_super) {
    __extends(ProviderSocket, _super);
    function ProviderSocket(_pageId, _folderId) {
        var _this = _super.call(this) || this;
        _this._pageId = _pageId;
        _this._folderId = _folderId;
        _this._stopped = false;
        _this._redirectCounter = 0;
        _this._redirectLimit = 10;
        _this._error = false;
        _this.onConnect = function () {
            return;
        };
        _this.onReconnectingError = function (attempt) {
            _this.emit("error", { message: "Streaming is currently not available", code: 502, type: "redirect" });
            _this.destroy(true);
            return;
        };
        _this.onReconnectError = function () {
            _this.destroy(false);
            _this._wsUrl = config.ws_url + "/page/" + _this._pageId;
            _this.start();
        };
        _this.onDisconnect = function () {
            return;
        };
        _this.onRedirect = function (msg) {
            _this._wsUrl = msg;
            _this._redirectCounter++;
            if (_this._redirectCounter >= _this._redirectLimit) {
                console.log("socket", _this._redirectCounter);
                _this.emit("error", { message: "Streaming connection limit reached", code: 500, type: "redirect" });
                _this.destroy(true);
                _this._redirectCounter = 0;
                return;
            }
            else {
                _this.destroy(false);
            }
            _this.start();
        };
        _this.onPageContent = function (data) {
            _this._data = data;
            if (!_this._stopped) {
                $timeout(function () {
                    _this.emit("content_update", data);
                });
            }
        };
        _this.onPageSettings = function (data) {
            $timeout(function () {
                _this.emit("meta_update", data);
            });
        };
        _this.onPageError = function (data) {
            $timeout(function () {
                _this._error = true;
                if (data.code === 401) {
                    auth.emit(auth.EVENT_401);
                }
                else {
                    _this.emit("error", data);
                }
            });
        };
        _this.onOAuthError = function (data) {
        };
        _this.onAuthRefresh = function () {
            var dummy = _this._pageId;
            _this.start();
        };
        _this.supportsWebSockets = function () {
            return "WebSocket" in window || "MozWebSocket" in window;
        };
        _this._wsUrl = config.ws_url + "/page/" + _this._pageId;
        _this.start();
        return _this;
    }
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_ERROR", {
        get: function () {
            return "page_error";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_CONTENT", {
        get: function () {
            return "page_content";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_PUSH", {
        get: function () {
            return "page_push";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_SETTINGS", {
        get: function () {
            return "page_settings";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_DATA", {
        get: function () {
            return "page_data";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_USER_JOINED", {
        get: function () {
            return "page_user_joined";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_USER_LEFT", {
        get: function () {
            return "page_user_left";
        },
        enumerable: true,
        configurable: true
    });
    ProviderSocket.prototype.start = function () {
        this._stopped = false;
        if (!this._socket || !this._socket.connected) {
            auth.on(auth.EVENT_LOGIN_REFRESHED, this.onAuthRefresh);
            this.init();
        }
        else {
            if (this._data) {
            }
        }
    };
    ProviderSocket.prototype.stop = function () {
        this._stopped = true;
    };
    ProviderSocket.prototype.destroy = function (hard) {
        if (hard === void 0) { hard = true; }
        this._socket.removeAllListeners();
        this._socket.disconnect();
        this.stop();
        auth.off(auth.EVENT_LOGIN_REFRESHED, this.onAuthRefresh);
        if (hard) {
            this.removeEvent();
        }
    };
    ProviderSocket.prototype.getData = function () {
        return this._data;
    };
    ProviderSocket.prototype.init = function () {
        this._socket = this.connect();
        this._socket.on("connect", this.onConnect);
        this._socket.on("reconnecting", this.onReconnectingError);
        this._socket.on("reconnect_error", this.onReconnectError);
        this._socket.on("redirect", this.onRedirect);
        this._socket.on(ProviderSocket.SOCKET_EVENT_PAGE_CONTENT, this.onPageContent);
        this._socket.on(ProviderSocket.SOCKET_EVENT_PAGE_SETTINGS, this.onPageSettings);
        this._socket.on(ProviderSocket.SOCKET_EVENT_PAGE_ERROR, this.onPageError);
        this._socket.on("oauth_error", this.onOAuthError);
        this._socket.on("disconnect", this.onDisconnect);
        this._stopped = false;
    };
    ProviderSocket.prototype.connect = function () {
        var token = api.tokens && api.tokens.access_token ? api.tokens.access_token : (storage ? storage.persistent.get("access_token") : "");
        var query = [
            "access_token=" + token,
        ];
        query = query.filter(function (val) {
            return (val.length > 0);
        });
        return io.connect(this._wsUrl, {
            query: query.join("&"),
            transports: (this.supportsWebSockets()) ? ["websocket"] : ["polling"],
            forceNew: true,
            reconnectionAttempts: 5,
        });
    };
    return ProviderSocket;
}(Emitter_1.default));
exports.ProviderSocket = ProviderSocket;
//# sourceMappingURL=Provider.js.map

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

var rng = __webpack_require__(18);
var bytesToUuid = __webpack_require__(19);

function v4(options, buf, offset) {
  var i = buf && offset || 0;

  if (typeof(options) == 'string') {
    buf = options === 'binary' ? new Array(16) : null;
    options = null;
  }
  options = options || {};

  var rnds = options.random || (options.rng || rng)();

  // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`
  rnds[6] = (rnds[6] & 0x0f) | 0x40;
  rnds[8] = (rnds[8] & 0x3f) | 0x80;

  // Copy bytes to buffer, if provided
  if (buf) {
    for (var ii = 0; ii < 16; ++ii) {
      buf[i + ii] = rnds[ii];
    }
  }

  return buf || bytesToUuid(rnds);
}

module.exports = v4;


/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 9 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Classy = (function () {
    function Classy() {
        return;
    }
    Classy.prototype.hasClass = function (ele, cls) {
        if (!ele || typeof ele.className === "undefined") {
            return false;
        }
        return (ele.className.match(new RegExp("(\\s|^)" + cls + "(\\s|$)"))) ? true : false;
    };
    Classy.prototype.addClass = function (ele, cls) {
        if (!ele) {
            return false;
        }
        ele.classList.add(cls);
    };
    Classy.prototype.removeClass = function (ele, cls) {
        if (!ele) {
            return false;
        }
        ele.classList.remove(cls);
    };
    return Classy;
}());
exports.default = Classy;
//# sourceMappingURL=Classy.js.map

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;/*! Hammer.JS - v2.0.7 - 2016-04-22
 * http://hammerjs.github.io/
 *
 * Copyright (c) 2016 Jorik Tangelder;
 * Licensed under the MIT license */
(function(window, document, exportName, undefined) {
  'use strict';

var VENDOR_PREFIXES = ['', 'webkit', 'Moz', 'MS', 'ms', 'o'];
var TEST_ELEMENT = document.createElement('div');

var TYPE_FUNCTION = 'function';

var round = Math.round;
var abs = Math.abs;
var now = Date.now;

/**
 * set a timeout with a given scope
 * @param {Function} fn
 * @param {Number} timeout
 * @param {Object} context
 * @returns {number}
 */
function setTimeoutContext(fn, timeout, context) {
    return setTimeout(bindFn(fn, context), timeout);
}

/**
 * if the argument is an array, we want to execute the fn on each entry
 * if it aint an array we don't want to do a thing.
 * this is used by all the methods that accept a single and array argument.
 * @param {*|Array} arg
 * @param {String} fn
 * @param {Object} [context]
 * @returns {Boolean}
 */
function invokeArrayArg(arg, fn, context) {
    if (Array.isArray(arg)) {
        each(arg, context[fn], context);
        return true;
    }
    return false;
}

/**
 * walk objects and arrays
 * @param {Object} obj
 * @param {Function} iterator
 * @param {Object} context
 */
function each(obj, iterator, context) {
    var i;

    if (!obj) {
        return;
    }

    if (obj.forEach) {
        obj.forEach(iterator, context);
    } else if (obj.length !== undefined) {
        i = 0;
        while (i < obj.length) {
            iterator.call(context, obj[i], i, obj);
            i++;
        }
    } else {
        for (i in obj) {
            obj.hasOwnProperty(i) && iterator.call(context, obj[i], i, obj);
        }
    }
}

/**
 * wrap a method with a deprecation warning and stack trace
 * @param {Function} method
 * @param {String} name
 * @param {String} message
 * @returns {Function} A new function wrapping the supplied method.
 */
function deprecate(method, name, message) {
    var deprecationMessage = 'DEPRECATED METHOD: ' + name + '\n' + message + ' AT \n';
    return function() {
        var e = new Error('get-stack-trace');
        var stack = e && e.stack ? e.stack.replace(/^[^\(]+?[\n$]/gm, '')
            .replace(/^\s+at\s+/gm, '')
            .replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@') : 'Unknown Stack Trace';

        var log = window.console && (window.console.warn || window.console.log);
        if (log) {
            log.call(window.console, deprecationMessage, stack);
        }
        return method.apply(this, arguments);
    };
}

/**
 * extend object.
 * means that properties in dest will be overwritten by the ones in src.
 * @param {Object} target
 * @param {...Object} objects_to_assign
 * @returns {Object} target
 */
var assign;
if (typeof Object.assign !== 'function') {
    assign = function assign(target) {
        if (target === undefined || target === null) {
            throw new TypeError('Cannot convert undefined or null to object');
        }

        var output = Object(target);
        for (var index = 1; index < arguments.length; index++) {
            var source = arguments[index];
            if (source !== undefined && source !== null) {
                for (var nextKey in source) {
                    if (source.hasOwnProperty(nextKey)) {
                        output[nextKey] = source[nextKey];
                    }
                }
            }
        }
        return output;
    };
} else {
    assign = Object.assign;
}

/**
 * extend object.
 * means that properties in dest will be overwritten by the ones in src.
 * @param {Object} dest
 * @param {Object} src
 * @param {Boolean} [merge=false]
 * @returns {Object} dest
 */
var extend = deprecate(function extend(dest, src, merge) {
    var keys = Object.keys(src);
    var i = 0;
    while (i < keys.length) {
        if (!merge || (merge && dest[keys[i]] === undefined)) {
            dest[keys[i]] = src[keys[i]];
        }
        i++;
    }
    return dest;
}, 'extend', 'Use `assign`.');

/**
 * merge the values from src in the dest.
 * means that properties that exist in dest will not be overwritten by src
 * @param {Object} dest
 * @param {Object} src
 * @returns {Object} dest
 */
var merge = deprecate(function merge(dest, src) {
    return extend(dest, src, true);
}, 'merge', 'Use `assign`.');

/**
 * simple class inheritance
 * @param {Function} child
 * @param {Function} base
 * @param {Object} [properties]
 */
function inherit(child, base, properties) {
    var baseP = base.prototype,
        childP;

    childP = child.prototype = Object.create(baseP);
    childP.constructor = child;
    childP._super = baseP;

    if (properties) {
        assign(childP, properties);
    }
}

/**
 * simple function bind
 * @param {Function} fn
 * @param {Object} context
 * @returns {Function}
 */
function bindFn(fn, context) {
    return function boundFn() {
        return fn.apply(context, arguments);
    };
}

/**
 * let a boolean value also be a function that must return a boolean
 * this first item in args will be used as the context
 * @param {Boolean|Function} val
 * @param {Array} [args]
 * @returns {Boolean}
 */
function boolOrFn(val, args) {
    if (typeof val == TYPE_FUNCTION) {
        return val.apply(args ? args[0] || undefined : undefined, args);
    }
    return val;
}

/**
 * use the val2 when val1 is undefined
 * @param {*} val1
 * @param {*} val2
 * @returns {*}
 */
function ifUndefined(val1, val2) {
    return (val1 === undefined) ? val2 : val1;
}

/**
 * addEventListener with multiple events at once
 * @param {EventTarget} target
 * @param {String} types
 * @param {Function} handler
 */
function addEventListeners(target, types, handler) {
    each(splitStr(types), function(type) {
        target.addEventListener(type, handler, false);
    });
}

/**
 * removeEventListener with multiple events at once
 * @param {EventTarget} target
 * @param {String} types
 * @param {Function} handler
 */
function removeEventListeners(target, types, handler) {
    each(splitStr(types), function(type) {
        target.removeEventListener(type, handler, false);
    });
}

/**
 * find if a node is in the given parent
 * @method hasParent
 * @param {HTMLElement} node
 * @param {HTMLElement} parent
 * @return {Boolean} found
 */
function hasParent(node, parent) {
    while (node) {
        if (node == parent) {
            return true;
        }
        node = node.parentNode;
    }
    return false;
}

/**
 * small indexOf wrapper
 * @param {String} str
 * @param {String} find
 * @returns {Boolean} found
 */
function inStr(str, find) {
    return str.indexOf(find) > -1;
}

/**
 * split string on whitespace
 * @param {String} str
 * @returns {Array} words
 */
function splitStr(str) {
    return str.trim().split(/\s+/g);
}

/**
 * find if a array contains the object using indexOf or a simple polyFill
 * @param {Array} src
 * @param {String} find
 * @param {String} [findByKey]
 * @return {Boolean|Number} false when not found, or the index
 */
function inArray(src, find, findByKey) {
    if (src.indexOf && !findByKey) {
        return src.indexOf(find);
    } else {
        var i = 0;
        while (i < src.length) {
            if ((findByKey && src[i][findByKey] == find) || (!findByKey && src[i] === find)) {
                return i;
            }
            i++;
        }
        return -1;
    }
}

/**
 * convert array-like objects to real arrays
 * @param {Object} obj
 * @returns {Array}
 */
function toArray(obj) {
    return Array.prototype.slice.call(obj, 0);
}

/**
 * unique array with objects based on a key (like 'id') or just by the array's value
 * @param {Array} src [{id:1},{id:2},{id:1}]
 * @param {String} [key]
 * @param {Boolean} [sort=False]
 * @returns {Array} [{id:1},{id:2}]
 */
function uniqueArray(src, key, sort) {
    var results = [];
    var values = [];
    var i = 0;

    while (i < src.length) {
        var val = key ? src[i][key] : src[i];
        if (inArray(values, val) < 0) {
            results.push(src[i]);
        }
        values[i] = val;
        i++;
    }

    if (sort) {
        if (!key) {
            results = results.sort();
        } else {
            results = results.sort(function sortUniqueArray(a, b) {
                return a[key] > b[key];
            });
        }
    }

    return results;
}

/**
 * get the prefixed property
 * @param {Object} obj
 * @param {String} property
 * @returns {String|Undefined} prefixed
 */
function prefixed(obj, property) {
    var prefix, prop;
    var camelProp = property[0].toUpperCase() + property.slice(1);

    var i = 0;
    while (i < VENDOR_PREFIXES.length) {
        prefix = VENDOR_PREFIXES[i];
        prop = (prefix) ? prefix + camelProp : property;

        if (prop in obj) {
            return prop;
        }
        i++;
    }
    return undefined;
}

/**
 * get a unique id
 * @returns {number} uniqueId
 */
var _uniqueId = 1;
function uniqueId() {
    return _uniqueId++;
}

/**
 * get the window object of an element
 * @param {HTMLElement} element
 * @returns {DocumentView|Window}
 */
function getWindowForElement(element) {
    var doc = element.ownerDocument || element;
    return (doc.defaultView || doc.parentWindow || window);
}

var MOBILE_REGEX = /mobile|tablet|ip(ad|hone|od)|android/i;

var SUPPORT_TOUCH = ('ontouchstart' in window);
var SUPPORT_POINTER_EVENTS = prefixed(window, 'PointerEvent') !== undefined;
var SUPPORT_ONLY_TOUCH = SUPPORT_TOUCH && MOBILE_REGEX.test(navigator.userAgent);

var INPUT_TYPE_TOUCH = 'touch';
var INPUT_TYPE_PEN = 'pen';
var INPUT_TYPE_MOUSE = 'mouse';
var INPUT_TYPE_KINECT = 'kinect';

var COMPUTE_INTERVAL = 25;

var INPUT_START = 1;
var INPUT_MOVE = 2;
var INPUT_END = 4;
var INPUT_CANCEL = 8;

var DIRECTION_NONE = 1;
var DIRECTION_LEFT = 2;
var DIRECTION_RIGHT = 4;
var DIRECTION_UP = 8;
var DIRECTION_DOWN = 16;

var DIRECTION_HORIZONTAL = DIRECTION_LEFT | DIRECTION_RIGHT;
var DIRECTION_VERTICAL = DIRECTION_UP | DIRECTION_DOWN;
var DIRECTION_ALL = DIRECTION_HORIZONTAL | DIRECTION_VERTICAL;

var PROPS_XY = ['x', 'y'];
var PROPS_CLIENT_XY = ['clientX', 'clientY'];

/**
 * create new input type manager
 * @param {Manager} manager
 * @param {Function} callback
 * @returns {Input}
 * @constructor
 */
function Input(manager, callback) {
    var self = this;
    this.manager = manager;
    this.callback = callback;
    this.element = manager.element;
    this.target = manager.options.inputTarget;

    // smaller wrapper around the handler, for the scope and the enabled state of the manager,
    // so when disabled the input events are completely bypassed.
    this.domHandler = function(ev) {
        if (boolOrFn(manager.options.enable, [manager])) {
            self.handler(ev);
        }
    };

    this.init();

}

Input.prototype = {
    /**
     * should handle the inputEvent data and trigger the callback
     * @virtual
     */
    handler: function() { },

    /**
     * bind the events
     */
    init: function() {
        this.evEl && addEventListeners(this.element, this.evEl, this.domHandler);
        this.evTarget && addEventListeners(this.target, this.evTarget, this.domHandler);
        this.evWin && addEventListeners(getWindowForElement(this.element), this.evWin, this.domHandler);
    },

    /**
     * unbind the events
     */
    destroy: function() {
        this.evEl && removeEventListeners(this.element, this.evEl, this.domHandler);
        this.evTarget && removeEventListeners(this.target, this.evTarget, this.domHandler);
        this.evWin && removeEventListeners(getWindowForElement(this.element), this.evWin, this.domHandler);
    }
};

/**
 * create new input type manager
 * called by the Manager constructor
 * @param {Hammer} manager
 * @returns {Input}
 */
function createInputInstance(manager) {
    var Type;
    var inputClass = manager.options.inputClass;

    if (inputClass) {
        Type = inputClass;
    } else if (SUPPORT_POINTER_EVENTS) {
        Type = PointerEventInput;
    } else if (SUPPORT_ONLY_TOUCH) {
        Type = TouchInput;
    } else if (!SUPPORT_TOUCH) {
        Type = MouseInput;
    } else {
        Type = TouchMouseInput;
    }
    return new (Type)(manager, inputHandler);
}

/**
 * handle input events
 * @param {Manager} manager
 * @param {String} eventType
 * @param {Object} input
 */
function inputHandler(manager, eventType, input) {
    var pointersLen = input.pointers.length;
    var changedPointersLen = input.changedPointers.length;
    var isFirst = (eventType & INPUT_START && (pointersLen - changedPointersLen === 0));
    var isFinal = (eventType & (INPUT_END | INPUT_CANCEL) && (pointersLen - changedPointersLen === 0));

    input.isFirst = !!isFirst;
    input.isFinal = !!isFinal;

    if (isFirst) {
        manager.session = {};
    }

    // source event is the normalized value of the domEvents
    // like 'touchstart, mouseup, pointerdown'
    input.eventType = eventType;

    // compute scale, rotation etc
    computeInputData(manager, input);

    // emit secret event
    manager.emit('hammer.input', input);

    manager.recognize(input);
    manager.session.prevInput = input;
}

/**
 * extend the data with some usable properties like scale, rotate, velocity etc
 * @param {Object} manager
 * @param {Object} input
 */
function computeInputData(manager, input) {
    var session = manager.session;
    var pointers = input.pointers;
    var pointersLength = pointers.length;

    // store the first input to calculate the distance and direction
    if (!session.firstInput) {
        session.firstInput = simpleCloneInputData(input);
    }

    // to compute scale and rotation we need to store the multiple touches
    if (pointersLength > 1 && !session.firstMultiple) {
        session.firstMultiple = simpleCloneInputData(input);
    } else if (pointersLength === 1) {
        session.firstMultiple = false;
    }

    var firstInput = session.firstInput;
    var firstMultiple = session.firstMultiple;
    var offsetCenter = firstMultiple ? firstMultiple.center : firstInput.center;

    var center = input.center = getCenter(pointers);
    input.timeStamp = now();
    input.deltaTime = input.timeStamp - firstInput.timeStamp;

    input.angle = getAngle(offsetCenter, center);
    input.distance = getDistance(offsetCenter, center);

    computeDeltaXY(session, input);
    input.offsetDirection = getDirection(input.deltaX, input.deltaY);

    var overallVelocity = getVelocity(input.deltaTime, input.deltaX, input.deltaY);
    input.overallVelocityX = overallVelocity.x;
    input.overallVelocityY = overallVelocity.y;
    input.overallVelocity = (abs(overallVelocity.x) > abs(overallVelocity.y)) ? overallVelocity.x : overallVelocity.y;

    input.scale = firstMultiple ? getScale(firstMultiple.pointers, pointers) : 1;
    input.rotation = firstMultiple ? getRotation(firstMultiple.pointers, pointers) : 0;

    input.maxPointers = !session.prevInput ? input.pointers.length : ((input.pointers.length >
        session.prevInput.maxPointers) ? input.pointers.length : session.prevInput.maxPointers);

    computeIntervalInputData(session, input);

    // find the correct target
    var target = manager.element;
    if (hasParent(input.srcEvent.target, target)) {
        target = input.srcEvent.target;
    }
    input.target = target;
}

function computeDeltaXY(session, input) {
    var center = input.center;
    var offset = session.offsetDelta || {};
    var prevDelta = session.prevDelta || {};
    var prevInput = session.prevInput || {};

    if (input.eventType === INPUT_START || prevInput.eventType === INPUT_END) {
        prevDelta = session.prevDelta = {
            x: prevInput.deltaX || 0,
            y: prevInput.deltaY || 0
        };

        offset = session.offsetDelta = {
            x: center.x,
            y: center.y
        };
    }

    input.deltaX = prevDelta.x + (center.x - offset.x);
    input.deltaY = prevDelta.y + (center.y - offset.y);
}

/**
 * velocity is calculated every x ms
 * @param {Object} session
 * @param {Object} input
 */
function computeIntervalInputData(session, input) {
    var last = session.lastInterval || input,
        deltaTime = input.timeStamp - last.timeStamp,
        velocity, velocityX, velocityY, direction;

    if (input.eventType != INPUT_CANCEL && (deltaTime > COMPUTE_INTERVAL || last.velocity === undefined)) {
        var deltaX = input.deltaX - last.deltaX;
        var deltaY = input.deltaY - last.deltaY;

        var v = getVelocity(deltaTime, deltaX, deltaY);
        velocityX = v.x;
        velocityY = v.y;
        velocity = (abs(v.x) > abs(v.y)) ? v.x : v.y;
        direction = getDirection(deltaX, deltaY);

        session.lastInterval = input;
    } else {
        // use latest velocity info if it doesn't overtake a minimum period
        velocity = last.velocity;
        velocityX = last.velocityX;
        velocityY = last.velocityY;
        direction = last.direction;
    }

    input.velocity = velocity;
    input.velocityX = velocityX;
    input.velocityY = velocityY;
    input.direction = direction;
}

/**
 * create a simple clone from the input used for storage of firstInput and firstMultiple
 * @param {Object} input
 * @returns {Object} clonedInputData
 */
function simpleCloneInputData(input) {
    // make a simple copy of the pointers because we will get a reference if we don't
    // we only need clientXY for the calculations
    var pointers = [];
    var i = 0;
    while (i < input.pointers.length) {
        pointers[i] = {
            clientX: round(input.pointers[i].clientX),
            clientY: round(input.pointers[i].clientY)
        };
        i++;
    }

    return {
        timeStamp: now(),
        pointers: pointers,
        center: getCenter(pointers),
        deltaX: input.deltaX,
        deltaY: input.deltaY
    };
}

/**
 * get the center of all the pointers
 * @param {Array} pointers
 * @return {Object} center contains `x` and `y` properties
 */
function getCenter(pointers) {
    var pointersLength = pointers.length;

    // no need to loop when only one touch
    if (pointersLength === 1) {
        return {
            x: round(pointers[0].clientX),
            y: round(pointers[0].clientY)
        };
    }

    var x = 0, y = 0, i = 0;
    while (i < pointersLength) {
        x += pointers[i].clientX;
        y += pointers[i].clientY;
        i++;
    }

    return {
        x: round(x / pointersLength),
        y: round(y / pointersLength)
    };
}

/**
 * calculate the velocity between two points. unit is in px per ms.
 * @param {Number} deltaTime
 * @param {Number} x
 * @param {Number} y
 * @return {Object} velocity `x` and `y`
 */
function getVelocity(deltaTime, x, y) {
    return {
        x: x / deltaTime || 0,
        y: y / deltaTime || 0
    };
}

/**
 * get the direction between two points
 * @param {Number} x
 * @param {Number} y
 * @return {Number} direction
 */
function getDirection(x, y) {
    if (x === y) {
        return DIRECTION_NONE;
    }

    if (abs(x) >= abs(y)) {
        return x < 0 ? DIRECTION_LEFT : DIRECTION_RIGHT;
    }
    return y < 0 ? DIRECTION_UP : DIRECTION_DOWN;
}

/**
 * calculate the absolute distance between two points
 * @param {Object} p1 {x, y}
 * @param {Object} p2 {x, y}
 * @param {Array} [props] containing x and y keys
 * @return {Number} distance
 */
function getDistance(p1, p2, props) {
    if (!props) {
        props = PROPS_XY;
    }
    var x = p2[props[0]] - p1[props[0]],
        y = p2[props[1]] - p1[props[1]];

    return Math.sqrt((x * x) + (y * y));
}

/**
 * calculate the angle between two coordinates
 * @param {Object} p1
 * @param {Object} p2
 * @param {Array} [props] containing x and y keys
 * @return {Number} angle
 */
function getAngle(p1, p2, props) {
    if (!props) {
        props = PROPS_XY;
    }
    var x = p2[props[0]] - p1[props[0]],
        y = p2[props[1]] - p1[props[1]];
    return Math.atan2(y, x) * 180 / Math.PI;
}

/**
 * calculate the rotation degrees between two pointersets
 * @param {Array} start array of pointers
 * @param {Array} end array of pointers
 * @return {Number} rotation
 */
function getRotation(start, end) {
    return getAngle(end[1], end[0], PROPS_CLIENT_XY) + getAngle(start[1], start[0], PROPS_CLIENT_XY);
}

/**
 * calculate the scale factor between two pointersets
 * no scale is 1, and goes down to 0 when pinched together, and bigger when pinched out
 * @param {Array} start array of pointers
 * @param {Array} end array of pointers
 * @return {Number} scale
 */
function getScale(start, end) {
    return getDistance(end[0], end[1], PROPS_CLIENT_XY) / getDistance(start[0], start[1], PROPS_CLIENT_XY);
}

var MOUSE_INPUT_MAP = {
    mousedown: INPUT_START,
    mousemove: INPUT_MOVE,
    mouseup: INPUT_END
};

var MOUSE_ELEMENT_EVENTS = 'mousedown';
var MOUSE_WINDOW_EVENTS = 'mousemove mouseup';

/**
 * Mouse events input
 * @constructor
 * @extends Input
 */
function MouseInput() {
    this.evEl = MOUSE_ELEMENT_EVENTS;
    this.evWin = MOUSE_WINDOW_EVENTS;

    this.pressed = false; // mousedown state

    Input.apply(this, arguments);
}

inherit(MouseInput, Input, {
    /**
     * handle mouse events
     * @param {Object} ev
     */
    handler: function MEhandler(ev) {
        var eventType = MOUSE_INPUT_MAP[ev.type];

        // on start we want to have the left mouse button down
        if (eventType & INPUT_START && ev.button === 0) {
            this.pressed = true;
        }

        if (eventType & INPUT_MOVE && ev.which !== 1) {
            eventType = INPUT_END;
        }

        // mouse must be down
        if (!this.pressed) {
            return;
        }

        if (eventType & INPUT_END) {
            this.pressed = false;
        }

        this.callback(this.manager, eventType, {
            pointers: [ev],
            changedPointers: [ev],
            pointerType: INPUT_TYPE_MOUSE,
            srcEvent: ev
        });
    }
});

var POINTER_INPUT_MAP = {
    pointerdown: INPUT_START,
    pointermove: INPUT_MOVE,
    pointerup: INPUT_END,
    pointercancel: INPUT_CANCEL,
    pointerout: INPUT_CANCEL
};

// in IE10 the pointer types is defined as an enum
var IE10_POINTER_TYPE_ENUM = {
    2: INPUT_TYPE_TOUCH,
    3: INPUT_TYPE_PEN,
    4: INPUT_TYPE_MOUSE,
    5: INPUT_TYPE_KINECT // see https://twitter.com/jacobrossi/status/480596438489890816
};

var POINTER_ELEMENT_EVENTS = 'pointerdown';
var POINTER_WINDOW_EVENTS = 'pointermove pointerup pointercancel';

// IE10 has prefixed support, and case-sensitive
if (window.MSPointerEvent && !window.PointerEvent) {
    POINTER_ELEMENT_EVENTS = 'MSPointerDown';
    POINTER_WINDOW_EVENTS = 'MSPointerMove MSPointerUp MSPointerCancel';
}

/**
 * Pointer events input
 * @constructor
 * @extends Input
 */
function PointerEventInput() {
    this.evEl = POINTER_ELEMENT_EVENTS;
    this.evWin = POINTER_WINDOW_EVENTS;

    Input.apply(this, arguments);

    this.store = (this.manager.session.pointerEvents = []);
}

inherit(PointerEventInput, Input, {
    /**
     * handle mouse events
     * @param {Object} ev
     */
    handler: function PEhandler(ev) {
        var store = this.store;
        var removePointer = false;

        var eventTypeNormalized = ev.type.toLowerCase().replace('ms', '');
        var eventType = POINTER_INPUT_MAP[eventTypeNormalized];
        var pointerType = IE10_POINTER_TYPE_ENUM[ev.pointerType] || ev.pointerType;

        var isTouch = (pointerType == INPUT_TYPE_TOUCH);

        // get index of the event in the store
        var storeIndex = inArray(store, ev.pointerId, 'pointerId');

        // start and mouse must be down
        if (eventType & INPUT_START && (ev.button === 0 || isTouch)) {
            if (storeIndex < 0) {
                store.push(ev);
                storeIndex = store.length - 1;
            }
        } else if (eventType & (INPUT_END | INPUT_CANCEL)) {
            removePointer = true;
        }

        // it not found, so the pointer hasn't been down (so it's probably a hover)
        if (storeIndex < 0) {
            return;
        }

        // update the event in the store
        store[storeIndex] = ev;

        this.callback(this.manager, eventType, {
            pointers: store,
            changedPointers: [ev],
            pointerType: pointerType,
            srcEvent: ev
        });

        if (removePointer) {
            // remove from the store
            store.splice(storeIndex, 1);
        }
    }
});

var SINGLE_TOUCH_INPUT_MAP = {
    touchstart: INPUT_START,
    touchmove: INPUT_MOVE,
    touchend: INPUT_END,
    touchcancel: INPUT_CANCEL
};

var SINGLE_TOUCH_TARGET_EVENTS = 'touchstart';
var SINGLE_TOUCH_WINDOW_EVENTS = 'touchstart touchmove touchend touchcancel';

/**
 * Touch events input
 * @constructor
 * @extends Input
 */
function SingleTouchInput() {
    this.evTarget = SINGLE_TOUCH_TARGET_EVENTS;
    this.evWin = SINGLE_TOUCH_WINDOW_EVENTS;
    this.started = false;

    Input.apply(this, arguments);
}

inherit(SingleTouchInput, Input, {
    handler: function TEhandler(ev) {
        var type = SINGLE_TOUCH_INPUT_MAP[ev.type];

        // should we handle the touch events?
        if (type === INPUT_START) {
            this.started = true;
        }

        if (!this.started) {
            return;
        }

        var touches = normalizeSingleTouches.call(this, ev, type);

        // when done, reset the started state
        if (type & (INPUT_END | INPUT_CANCEL) && touches[0].length - touches[1].length === 0) {
            this.started = false;
        }

        this.callback(this.manager, type, {
            pointers: touches[0],
            changedPointers: touches[1],
            pointerType: INPUT_TYPE_TOUCH,
            srcEvent: ev
        });
    }
});

/**
 * @this {TouchInput}
 * @param {Object} ev
 * @param {Number} type flag
 * @returns {undefined|Array} [all, changed]
 */
function normalizeSingleTouches(ev, type) {
    var all = toArray(ev.touches);
    var changed = toArray(ev.changedTouches);

    if (type & (INPUT_END | INPUT_CANCEL)) {
        all = uniqueArray(all.concat(changed), 'identifier', true);
    }

    return [all, changed];
}

var TOUCH_INPUT_MAP = {
    touchstart: INPUT_START,
    touchmove: INPUT_MOVE,
    touchend: INPUT_END,
    touchcancel: INPUT_CANCEL
};

var TOUCH_TARGET_EVENTS = 'touchstart touchmove touchend touchcancel';

/**
 * Multi-user touch events input
 * @constructor
 * @extends Input
 */
function TouchInput() {
    this.evTarget = TOUCH_TARGET_EVENTS;
    this.targetIds = {};

    Input.apply(this, arguments);
}

inherit(TouchInput, Input, {
    handler: function MTEhandler(ev) {
        var type = TOUCH_INPUT_MAP[ev.type];
        var touches = getTouches.call(this, ev, type);
        if (!touches) {
            return;
        }

        this.callback(this.manager, type, {
            pointers: touches[0],
            changedPointers: touches[1],
            pointerType: INPUT_TYPE_TOUCH,
            srcEvent: ev
        });
    }
});

/**
 * @this {TouchInput}
 * @param {Object} ev
 * @param {Number} type flag
 * @returns {undefined|Array} [all, changed]
 */
function getTouches(ev, type) {
    var allTouches = toArray(ev.touches);
    var targetIds = this.targetIds;

    // when there is only one touch, the process can be simplified
    if (type & (INPUT_START | INPUT_MOVE) && allTouches.length === 1) {
        targetIds[allTouches[0].identifier] = true;
        return [allTouches, allTouches];
    }

    var i,
        targetTouches,
        changedTouches = toArray(ev.changedTouches),
        changedTargetTouches = [],
        target = this.target;

    // get target touches from touches
    targetTouches = allTouches.filter(function(touch) {
        return hasParent(touch.target, target);
    });

    // collect touches
    if (type === INPUT_START) {
        i = 0;
        while (i < targetTouches.length) {
            targetIds[targetTouches[i].identifier] = true;
            i++;
        }
    }

    // filter changed touches to only contain touches that exist in the collected target ids
    i = 0;
    while (i < changedTouches.length) {
        if (targetIds[changedTouches[i].identifier]) {
            changedTargetTouches.push(changedTouches[i]);
        }

        // cleanup removed touches
        if (type & (INPUT_END | INPUT_CANCEL)) {
            delete targetIds[changedTouches[i].identifier];
        }
        i++;
    }

    if (!changedTargetTouches.length) {
        return;
    }

    return [
        // merge targetTouches with changedTargetTouches so it contains ALL touches, including 'end' and 'cancel'
        uniqueArray(targetTouches.concat(changedTargetTouches), 'identifier', true),
        changedTargetTouches
    ];
}

/**
 * Combined touch and mouse input
 *
 * Touch has a higher priority then mouse, and while touching no mouse events are allowed.
 * This because touch devices also emit mouse events while doing a touch.
 *
 * @constructor
 * @extends Input
 */

var DEDUP_TIMEOUT = 2500;
var DEDUP_DISTANCE = 25;

function TouchMouseInput() {
    Input.apply(this, arguments);

    var handler = bindFn(this.handler, this);
    this.touch = new TouchInput(this.manager, handler);
    this.mouse = new MouseInput(this.manager, handler);

    this.primaryTouch = null;
    this.lastTouches = [];
}

inherit(TouchMouseInput, Input, {
    /**
     * handle mouse and touch events
     * @param {Hammer} manager
     * @param {String} inputEvent
     * @param {Object} inputData
     */
    handler: function TMEhandler(manager, inputEvent, inputData) {
        var isTouch = (inputData.pointerType == INPUT_TYPE_TOUCH),
            isMouse = (inputData.pointerType == INPUT_TYPE_MOUSE);

        if (isMouse && inputData.sourceCapabilities && inputData.sourceCapabilities.firesTouchEvents) {
            return;
        }

        // when we're in a touch event, record touches to  de-dupe synthetic mouse event
        if (isTouch) {
            recordTouches.call(this, inputEvent, inputData);
        } else if (isMouse && isSyntheticEvent.call(this, inputData)) {
            return;
        }

        this.callback(manager, inputEvent, inputData);
    },

    /**
     * remove the event listeners
     */
    destroy: function destroy() {
        this.touch.destroy();
        this.mouse.destroy();
    }
});

function recordTouches(eventType, eventData) {
    if (eventType & INPUT_START) {
        this.primaryTouch = eventData.changedPointers[0].identifier;
        setLastTouch.call(this, eventData);
    } else if (eventType & (INPUT_END | INPUT_CANCEL)) {
        setLastTouch.call(this, eventData);
    }
}

function setLastTouch(eventData) {
    var touch = eventData.changedPointers[0];

    if (touch.identifier === this.primaryTouch) {
        var lastTouch = {x: touch.clientX, y: touch.clientY};
        this.lastTouches.push(lastTouch);
        var lts = this.lastTouches;
        var removeLastTouch = function() {
            var i = lts.indexOf(lastTouch);
            if (i > -1) {
                lts.splice(i, 1);
            }
        };
        setTimeout(removeLastTouch, DEDUP_TIMEOUT);
    }
}

function isSyntheticEvent(eventData) {
    var x = eventData.srcEvent.clientX, y = eventData.srcEvent.clientY;
    for (var i = 0; i < this.lastTouches.length; i++) {
        var t = this.lastTouches[i];
        var dx = Math.abs(x - t.x), dy = Math.abs(y - t.y);
        if (dx <= DEDUP_DISTANCE && dy <= DEDUP_DISTANCE) {
            return true;
        }
    }
    return false;
}

var PREFIXED_TOUCH_ACTION = prefixed(TEST_ELEMENT.style, 'touchAction');
var NATIVE_TOUCH_ACTION = PREFIXED_TOUCH_ACTION !== undefined;

// magical touchAction value
var TOUCH_ACTION_COMPUTE = 'compute';
var TOUCH_ACTION_AUTO = 'auto';
var TOUCH_ACTION_MANIPULATION = 'manipulation'; // not implemented
var TOUCH_ACTION_NONE = 'none';
var TOUCH_ACTION_PAN_X = 'pan-x';
var TOUCH_ACTION_PAN_Y = 'pan-y';
var TOUCH_ACTION_MAP = getTouchActionProps();

/**
 * Touch Action
 * sets the touchAction property or uses the js alternative
 * @param {Manager} manager
 * @param {String} value
 * @constructor
 */
function TouchAction(manager, value) {
    this.manager = manager;
    this.set(value);
}

TouchAction.prototype = {
    /**
     * set the touchAction value on the element or enable the polyfill
     * @param {String} value
     */
    set: function(value) {
        // find out the touch-action by the event handlers
        if (value == TOUCH_ACTION_COMPUTE) {
            value = this.compute();
        }

        if (NATIVE_TOUCH_ACTION && this.manager.element.style && TOUCH_ACTION_MAP[value]) {
            this.manager.element.style[PREFIXED_TOUCH_ACTION] = value;
        }
        this.actions = value.toLowerCase().trim();
    },

    /**
     * just re-set the touchAction value
     */
    update: function() {
        this.set(this.manager.options.touchAction);
    },

    /**
     * compute the value for the touchAction property based on the recognizer's settings
     * @returns {String} value
     */
    compute: function() {
        var actions = [];
        each(this.manager.recognizers, function(recognizer) {
            if (boolOrFn(recognizer.options.enable, [recognizer])) {
                actions = actions.concat(recognizer.getTouchAction());
            }
        });
        return cleanTouchActions(actions.join(' '));
    },

    /**
     * this method is called on each input cycle and provides the preventing of the browser behavior
     * @param {Object} input
     */
    preventDefaults: function(input) {
        var srcEvent = input.srcEvent;
        var direction = input.offsetDirection;

        // if the touch action did prevented once this session
        if (this.manager.session.prevented) {
            srcEvent.preventDefault();
            return;
        }

        var actions = this.actions;
        var hasNone = inStr(actions, TOUCH_ACTION_NONE) && !TOUCH_ACTION_MAP[TOUCH_ACTION_NONE];
        var hasPanY = inStr(actions, TOUCH_ACTION_PAN_Y) && !TOUCH_ACTION_MAP[TOUCH_ACTION_PAN_Y];
        var hasPanX = inStr(actions, TOUCH_ACTION_PAN_X) && !TOUCH_ACTION_MAP[TOUCH_ACTION_PAN_X];

        if (hasNone) {
            //do not prevent defaults if this is a tap gesture

            var isTapPointer = input.pointers.length === 1;
            var isTapMovement = input.distance < 2;
            var isTapTouchTime = input.deltaTime < 250;

            if (isTapPointer && isTapMovement && isTapTouchTime) {
                return;
            }
        }

        if (hasPanX && hasPanY) {
            // `pan-x pan-y` means browser handles all scrolling/panning, do not prevent
            return;
        }

        if (hasNone ||
            (hasPanY && direction & DIRECTION_HORIZONTAL) ||
            (hasPanX && direction & DIRECTION_VERTICAL)) {
            return this.preventSrc(srcEvent);
        }
    },

    /**
     * call preventDefault to prevent the browser's default behavior (scrolling in most cases)
     * @param {Object} srcEvent
     */
    preventSrc: function(srcEvent) {
        this.manager.session.prevented = true;
        srcEvent.preventDefault();
    }
};

/**
 * when the touchActions are collected they are not a valid value, so we need to clean things up. *
 * @param {String} actions
 * @returns {*}
 */
function cleanTouchActions(actions) {
    // none
    if (inStr(actions, TOUCH_ACTION_NONE)) {
        return TOUCH_ACTION_NONE;
    }

    var hasPanX = inStr(actions, TOUCH_ACTION_PAN_X);
    var hasPanY = inStr(actions, TOUCH_ACTION_PAN_Y);

    // if both pan-x and pan-y are set (different recognizers
    // for different directions, e.g. horizontal pan but vertical swipe?)
    // we need none (as otherwise with pan-x pan-y combined none of these
    // recognizers will work, since the browser would handle all panning
    if (hasPanX && hasPanY) {
        return TOUCH_ACTION_NONE;
    }

    // pan-x OR pan-y
    if (hasPanX || hasPanY) {
        return hasPanX ? TOUCH_ACTION_PAN_X : TOUCH_ACTION_PAN_Y;
    }

    // manipulation
    if (inStr(actions, TOUCH_ACTION_MANIPULATION)) {
        return TOUCH_ACTION_MANIPULATION;
    }

    return TOUCH_ACTION_AUTO;
}

function getTouchActionProps() {
    if (!NATIVE_TOUCH_ACTION) {
        return false;
    }
    var touchMap = {};
    var cssSupports = window.CSS && window.CSS.supports;
    ['auto', 'manipulation', 'pan-y', 'pan-x', 'pan-x pan-y', 'none'].forEach(function(val) {

        // If css.supports is not supported but there is native touch-action assume it supports
        // all values. This is the case for IE 10 and 11.
        touchMap[val] = cssSupports ? window.CSS.supports('touch-action', val) : true;
    });
    return touchMap;
}

/**
 * Recognizer flow explained; *
 * All recognizers have the initial state of POSSIBLE when a input session starts.
 * The definition of a input session is from the first input until the last input, with all it's movement in it. *
 * Example session for mouse-input: mousedown -> mousemove -> mouseup
 *
 * On each recognizing cycle (see Manager.recognize) the .recognize() method is executed
 * which determines with state it should be.
 *
 * If the recognizer has the state FAILED, CANCELLED or RECOGNIZED (equals ENDED), it is reset to
 * POSSIBLE to give it another change on the next cycle.
 *
 *               Possible
 *                  |
 *            +-----+---------------+
 *            |                     |
 *      +-----+-----+               |
 *      |           |               |
 *   Failed      Cancelled          |
 *                          +-------+------+
 *                          |              |
 *                      Recognized       Began
 *                                         |
 *                                      Changed
 *                                         |
 *                                  Ended/Recognized
 */
var STATE_POSSIBLE = 1;
var STATE_BEGAN = 2;
var STATE_CHANGED = 4;
var STATE_ENDED = 8;
var STATE_RECOGNIZED = STATE_ENDED;
var STATE_CANCELLED = 16;
var STATE_FAILED = 32;

/**
 * Recognizer
 * Every recognizer needs to extend from this class.
 * @constructor
 * @param {Object} options
 */
function Recognizer(options) {
    this.options = assign({}, this.defaults, options || {});

    this.id = uniqueId();

    this.manager = null;

    // default is enable true
    this.options.enable = ifUndefined(this.options.enable, true);

    this.state = STATE_POSSIBLE;

    this.simultaneous = {};
    this.requireFail = [];
}

Recognizer.prototype = {
    /**
     * @virtual
     * @type {Object}
     */
    defaults: {},

    /**
     * set options
     * @param {Object} options
     * @return {Recognizer}
     */
    set: function(options) {
        assign(this.options, options);

        // also update the touchAction, in case something changed about the directions/enabled state
        this.manager && this.manager.touchAction.update();
        return this;
    },

    /**
     * recognize simultaneous with an other recognizer.
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    recognizeWith: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'recognizeWith', this)) {
            return this;
        }

        var simultaneous = this.simultaneous;
        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        if (!simultaneous[otherRecognizer.id]) {
            simultaneous[otherRecognizer.id] = otherRecognizer;
            otherRecognizer.recognizeWith(this);
        }
        return this;
    },

    /**
     * drop the simultaneous link. it doesnt remove the link on the other recognizer.
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    dropRecognizeWith: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'dropRecognizeWith', this)) {
            return this;
        }

        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        delete this.simultaneous[otherRecognizer.id];
        return this;
    },

    /**
     * recognizer can only run when an other is failing
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    requireFailure: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'requireFailure', this)) {
            return this;
        }

        var requireFail = this.requireFail;
        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        if (inArray(requireFail, otherRecognizer) === -1) {
            requireFail.push(otherRecognizer);
            otherRecognizer.requireFailure(this);
        }
        return this;
    },

    /**
     * drop the requireFailure link. it does not remove the link on the other recognizer.
     * @param {Recognizer} otherRecognizer
     * @returns {Recognizer} this
     */
    dropRequireFailure: function(otherRecognizer) {
        if (invokeArrayArg(otherRecognizer, 'dropRequireFailure', this)) {
            return this;
        }

        otherRecognizer = getRecognizerByNameIfManager(otherRecognizer, this);
        var index = inArray(this.requireFail, otherRecognizer);
        if (index > -1) {
            this.requireFail.splice(index, 1);
        }
        return this;
    },

    /**
     * has require failures boolean
     * @returns {boolean}
     */
    hasRequireFailures: function() {
        return this.requireFail.length > 0;
    },

    /**
     * if the recognizer can recognize simultaneous with an other recognizer
     * @param {Recognizer} otherRecognizer
     * @returns {Boolean}
     */
    canRecognizeWith: function(otherRecognizer) {
        return !!this.simultaneous[otherRecognizer.id];
    },

    /**
     * You should use `tryEmit` instead of `emit` directly to check
     * that all the needed recognizers has failed before emitting.
     * @param {Object} input
     */
    emit: function(input) {
        var self = this;
        var state = this.state;

        function emit(event) {
            self.manager.emit(event, input);
        }

        // 'panstart' and 'panmove'
        if (state < STATE_ENDED) {
            emit(self.options.event + stateStr(state));
        }

        emit(self.options.event); // simple 'eventName' events

        if (input.additionalEvent) { // additional event(panleft, panright, pinchin, pinchout...)
            emit(input.additionalEvent);
        }

        // panend and pancancel
        if (state >= STATE_ENDED) {
            emit(self.options.event + stateStr(state));
        }
    },

    /**
     * Check that all the require failure recognizers has failed,
     * if true, it emits a gesture event,
     * otherwise, setup the state to FAILED.
     * @param {Object} input
     */
    tryEmit: function(input) {
        if (this.canEmit()) {
            return this.emit(input);
        }
        // it's failing anyway
        this.state = STATE_FAILED;
    },

    /**
     * can we emit?
     * @returns {boolean}
     */
    canEmit: function() {
        var i = 0;
        while (i < this.requireFail.length) {
            if (!(this.requireFail[i].state & (STATE_FAILED | STATE_POSSIBLE))) {
                return false;
            }
            i++;
        }
        return true;
    },

    /**
     * update the recognizer
     * @param {Object} inputData
     */
    recognize: function(inputData) {
        // make a new copy of the inputData
        // so we can change the inputData without messing up the other recognizers
        var inputDataClone = assign({}, inputData);

        // is is enabled and allow recognizing?
        if (!boolOrFn(this.options.enable, [this, inputDataClone])) {
            this.reset();
            this.state = STATE_FAILED;
            return;
        }

        // reset when we've reached the end
        if (this.state & (STATE_RECOGNIZED | STATE_CANCELLED | STATE_FAILED)) {
            this.state = STATE_POSSIBLE;
        }

        this.state = this.process(inputDataClone);

        // the recognizer has recognized a gesture
        // so trigger an event
        if (this.state & (STATE_BEGAN | STATE_CHANGED | STATE_ENDED | STATE_CANCELLED)) {
            this.tryEmit(inputDataClone);
        }
    },

    /**
     * return the state of the recognizer
     * the actual recognizing happens in this method
     * @virtual
     * @param {Object} inputData
     * @returns {Const} STATE
     */
    process: function(inputData) { }, // jshint ignore:line

    /**
     * return the preferred touch-action
     * @virtual
     * @returns {Array}
     */
    getTouchAction: function() { },

    /**
     * called when the gesture isn't allowed to recognize
     * like when another is being recognized or it is disabled
     * @virtual
     */
    reset: function() { }
};

/**
 * get a usable string, used as event postfix
 * @param {Const} state
 * @returns {String} state
 */
function stateStr(state) {
    if (state & STATE_CANCELLED) {
        return 'cancel';
    } else if (state & STATE_ENDED) {
        return 'end';
    } else if (state & STATE_CHANGED) {
        return 'move';
    } else if (state & STATE_BEGAN) {
        return 'start';
    }
    return '';
}

/**
 * direction cons to string
 * @param {Const} direction
 * @returns {String}
 */
function directionStr(direction) {
    if (direction == DIRECTION_DOWN) {
        return 'down';
    } else if (direction == DIRECTION_UP) {
        return 'up';
    } else if (direction == DIRECTION_LEFT) {
        return 'left';
    } else if (direction == DIRECTION_RIGHT) {
        return 'right';
    }
    return '';
}

/**
 * get a recognizer by name if it is bound to a manager
 * @param {Recognizer|String} otherRecognizer
 * @param {Recognizer} recognizer
 * @returns {Recognizer}
 */
function getRecognizerByNameIfManager(otherRecognizer, recognizer) {
    var manager = recognizer.manager;
    if (manager) {
        return manager.get(otherRecognizer);
    }
    return otherRecognizer;
}

/**
 * This recognizer is just used as a base for the simple attribute recognizers.
 * @constructor
 * @extends Recognizer
 */
function AttrRecognizer() {
    Recognizer.apply(this, arguments);
}

inherit(AttrRecognizer, Recognizer, {
    /**
     * @namespace
     * @memberof AttrRecognizer
     */
    defaults: {
        /**
         * @type {Number}
         * @default 1
         */
        pointers: 1
    },

    /**
     * Used to check if it the recognizer receives valid input, like input.distance > 10.
     * @memberof AttrRecognizer
     * @param {Object} input
     * @returns {Boolean} recognized
     */
    attrTest: function(input) {
        var optionPointers = this.options.pointers;
        return optionPointers === 0 || input.pointers.length === optionPointers;
    },

    /**
     * Process the input and return the state for the recognizer
     * @memberof AttrRecognizer
     * @param {Object} input
     * @returns {*} State
     */
    process: function(input) {
        var state = this.state;
        var eventType = input.eventType;

        var isRecognized = state & (STATE_BEGAN | STATE_CHANGED);
        var isValid = this.attrTest(input);

        // on cancel input and we've recognized before, return STATE_CANCELLED
        if (isRecognized && (eventType & INPUT_CANCEL || !isValid)) {
            return state | STATE_CANCELLED;
        } else if (isRecognized || isValid) {
            if (eventType & INPUT_END) {
                return state | STATE_ENDED;
            } else if (!(state & STATE_BEGAN)) {
                return STATE_BEGAN;
            }
            return state | STATE_CHANGED;
        }
        return STATE_FAILED;
    }
});

/**
 * Pan
 * Recognized when the pointer is down and moved in the allowed direction.
 * @constructor
 * @extends AttrRecognizer
 */
function PanRecognizer() {
    AttrRecognizer.apply(this, arguments);

    this.pX = null;
    this.pY = null;
}

inherit(PanRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof PanRecognizer
     */
    defaults: {
        event: 'pan',
        threshold: 10,
        pointers: 1,
        direction: DIRECTION_ALL
    },

    getTouchAction: function() {
        var direction = this.options.direction;
        var actions = [];
        if (direction & DIRECTION_HORIZONTAL) {
            actions.push(TOUCH_ACTION_PAN_Y);
        }
        if (direction & DIRECTION_VERTICAL) {
            actions.push(TOUCH_ACTION_PAN_X);
        }
        return actions;
    },

    directionTest: function(input) {
        var options = this.options;
        var hasMoved = true;
        var distance = input.distance;
        var direction = input.direction;
        var x = input.deltaX;
        var y = input.deltaY;

        // lock to axis?
        if (!(direction & options.direction)) {
            if (options.direction & DIRECTION_HORIZONTAL) {
                direction = (x === 0) ? DIRECTION_NONE : (x < 0) ? DIRECTION_LEFT : DIRECTION_RIGHT;
                hasMoved = x != this.pX;
                distance = Math.abs(input.deltaX);
            } else {
                direction = (y === 0) ? DIRECTION_NONE : (y < 0) ? DIRECTION_UP : DIRECTION_DOWN;
                hasMoved = y != this.pY;
                distance = Math.abs(input.deltaY);
            }
        }
        input.direction = direction;
        return hasMoved && distance > options.threshold && direction & options.direction;
    },

    attrTest: function(input) {
        return AttrRecognizer.prototype.attrTest.call(this, input) &&
            (this.state & STATE_BEGAN || (!(this.state & STATE_BEGAN) && this.directionTest(input)));
    },

    emit: function(input) {

        this.pX = input.deltaX;
        this.pY = input.deltaY;

        var direction = directionStr(input.direction);

        if (direction) {
            input.additionalEvent = this.options.event + direction;
        }
        this._super.emit.call(this, input);
    }
});

/**
 * Pinch
 * Recognized when two or more pointers are moving toward (zoom-in) or away from each other (zoom-out).
 * @constructor
 * @extends AttrRecognizer
 */
function PinchRecognizer() {
    AttrRecognizer.apply(this, arguments);
}

inherit(PinchRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof PinchRecognizer
     */
    defaults: {
        event: 'pinch',
        threshold: 0,
        pointers: 2
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_NONE];
    },

    attrTest: function(input) {
        return this._super.attrTest.call(this, input) &&
            (Math.abs(input.scale - 1) > this.options.threshold || this.state & STATE_BEGAN);
    },

    emit: function(input) {
        if (input.scale !== 1) {
            var inOut = input.scale < 1 ? 'in' : 'out';
            input.additionalEvent = this.options.event + inOut;
        }
        this._super.emit.call(this, input);
    }
});

/**
 * Press
 * Recognized when the pointer is down for x ms without any movement.
 * @constructor
 * @extends Recognizer
 */
function PressRecognizer() {
    Recognizer.apply(this, arguments);

    this._timer = null;
    this._input = null;
}

inherit(PressRecognizer, Recognizer, {
    /**
     * @namespace
     * @memberof PressRecognizer
     */
    defaults: {
        event: 'press',
        pointers: 1,
        time: 251, // minimal time of the pointer to be pressed
        threshold: 9 // a minimal movement is ok, but keep it low
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_AUTO];
    },

    process: function(input) {
        var options = this.options;
        var validPointers = input.pointers.length === options.pointers;
        var validMovement = input.distance < options.threshold;
        var validTime = input.deltaTime > options.time;

        this._input = input;

        // we only allow little movement
        // and we've reached an end event, so a tap is possible
        if (!validMovement || !validPointers || (input.eventType & (INPUT_END | INPUT_CANCEL) && !validTime)) {
            this.reset();
        } else if (input.eventType & INPUT_START) {
            this.reset();
            this._timer = setTimeoutContext(function() {
                this.state = STATE_RECOGNIZED;
                this.tryEmit();
            }, options.time, this);
        } else if (input.eventType & INPUT_END) {
            return STATE_RECOGNIZED;
        }
        return STATE_FAILED;
    },

    reset: function() {
        clearTimeout(this._timer);
    },

    emit: function(input) {
        if (this.state !== STATE_RECOGNIZED) {
            return;
        }

        if (input && (input.eventType & INPUT_END)) {
            this.manager.emit(this.options.event + 'up', input);
        } else {
            this._input.timeStamp = now();
            this.manager.emit(this.options.event, this._input);
        }
    }
});

/**
 * Rotate
 * Recognized when two or more pointer are moving in a circular motion.
 * @constructor
 * @extends AttrRecognizer
 */
function RotateRecognizer() {
    AttrRecognizer.apply(this, arguments);
}

inherit(RotateRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof RotateRecognizer
     */
    defaults: {
        event: 'rotate',
        threshold: 0,
        pointers: 2
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_NONE];
    },

    attrTest: function(input) {
        return this._super.attrTest.call(this, input) &&
            (Math.abs(input.rotation) > this.options.threshold || this.state & STATE_BEGAN);
    }
});

/**
 * Swipe
 * Recognized when the pointer is moving fast (velocity), with enough distance in the allowed direction.
 * @constructor
 * @extends AttrRecognizer
 */
function SwipeRecognizer() {
    AttrRecognizer.apply(this, arguments);
}

inherit(SwipeRecognizer, AttrRecognizer, {
    /**
     * @namespace
     * @memberof SwipeRecognizer
     */
    defaults: {
        event: 'swipe',
        threshold: 10,
        velocity: 0.3,
        direction: DIRECTION_HORIZONTAL | DIRECTION_VERTICAL,
        pointers: 1
    },

    getTouchAction: function() {
        return PanRecognizer.prototype.getTouchAction.call(this);
    },

    attrTest: function(input) {
        var direction = this.options.direction;
        var velocity;

        if (direction & (DIRECTION_HORIZONTAL | DIRECTION_VERTICAL)) {
            velocity = input.overallVelocity;
        } else if (direction & DIRECTION_HORIZONTAL) {
            velocity = input.overallVelocityX;
        } else if (direction & DIRECTION_VERTICAL) {
            velocity = input.overallVelocityY;
        }

        return this._super.attrTest.call(this, input) &&
            direction & input.offsetDirection &&
            input.distance > this.options.threshold &&
            input.maxPointers == this.options.pointers &&
            abs(velocity) > this.options.velocity && input.eventType & INPUT_END;
    },

    emit: function(input) {
        var direction = directionStr(input.offsetDirection);
        if (direction) {
            this.manager.emit(this.options.event + direction, input);
        }

        this.manager.emit(this.options.event, input);
    }
});

/**
 * A tap is ecognized when the pointer is doing a small tap/click. Multiple taps are recognized if they occur
 * between the given interval and position. The delay option can be used to recognize multi-taps without firing
 * a single tap.
 *
 * The eventData from the emitted event contains the property `tapCount`, which contains the amount of
 * multi-taps being recognized.
 * @constructor
 * @extends Recognizer
 */
function TapRecognizer() {
    Recognizer.apply(this, arguments);

    // previous time and center,
    // used for tap counting
    this.pTime = false;
    this.pCenter = false;

    this._timer = null;
    this._input = null;
    this.count = 0;
}

inherit(TapRecognizer, Recognizer, {
    /**
     * @namespace
     * @memberof PinchRecognizer
     */
    defaults: {
        event: 'tap',
        pointers: 1,
        taps: 1,
        interval: 300, // max time between the multi-tap taps
        time: 250, // max time of the pointer to be down (like finger on the screen)
        threshold: 9, // a minimal movement is ok, but keep it low
        posThreshold: 10 // a multi-tap can be a bit off the initial position
    },

    getTouchAction: function() {
        return [TOUCH_ACTION_MANIPULATION];
    },

    process: function(input) {
        var options = this.options;

        var validPointers = input.pointers.length === options.pointers;
        var validMovement = input.distance < options.threshold;
        var validTouchTime = input.deltaTime < options.time;

        this.reset();

        if ((input.eventType & INPUT_START) && (this.count === 0)) {
            return this.failTimeout();
        }

        // we only allow little movement
        // and we've reached an end event, so a tap is possible
        if (validMovement && validTouchTime && validPointers) {
            if (input.eventType != INPUT_END) {
                return this.failTimeout();
            }

            var validInterval = this.pTime ? (input.timeStamp - this.pTime < options.interval) : true;
            var validMultiTap = !this.pCenter || getDistance(this.pCenter, input.center) < options.posThreshold;

            this.pTime = input.timeStamp;
            this.pCenter = input.center;

            if (!validMultiTap || !validInterval) {
                this.count = 1;
            } else {
                this.count += 1;
            }

            this._input = input;

            // if tap count matches we have recognized it,
            // else it has began recognizing...
            var tapCount = this.count % options.taps;
            if (tapCount === 0) {
                // no failing requirements, immediately trigger the tap event
                // or wait as long as the multitap interval to trigger
                if (!this.hasRequireFailures()) {
                    return STATE_RECOGNIZED;
                } else {
                    this._timer = setTimeoutContext(function() {
                        this.state = STATE_RECOGNIZED;
                        this.tryEmit();
                    }, options.interval, this);
                    return STATE_BEGAN;
                }
            }
        }
        return STATE_FAILED;
    },

    failTimeout: function() {
        this._timer = setTimeoutContext(function() {
            this.state = STATE_FAILED;
        }, this.options.interval, this);
        return STATE_FAILED;
    },

    reset: function() {
        clearTimeout(this._timer);
    },

    emit: function() {
        if (this.state == STATE_RECOGNIZED) {
            this._input.tapCount = this.count;
            this.manager.emit(this.options.event, this._input);
        }
    }
});

/**
 * Simple way to create a manager with a default set of recognizers.
 * @param {HTMLElement} element
 * @param {Object} [options]
 * @constructor
 */
function Hammer(element, options) {
    options = options || {};
    options.recognizers = ifUndefined(options.recognizers, Hammer.defaults.preset);
    return new Manager(element, options);
}

/**
 * @const {string}
 */
Hammer.VERSION = '2.0.7';

/**
 * default settings
 * @namespace
 */
Hammer.defaults = {
    /**
     * set if DOM events are being triggered.
     * But this is slower and unused by simple implementations, so disabled by default.
     * @type {Boolean}
     * @default false
     */
    domEvents: false,

    /**
     * The value for the touchAction property/fallback.
     * When set to `compute` it will magically set the correct value based on the added recognizers.
     * @type {String}
     * @default compute
     */
    touchAction: TOUCH_ACTION_COMPUTE,

    /**
     * @type {Boolean}
     * @default true
     */
    enable: true,

    /**
     * EXPERIMENTAL FEATURE -- can be removed/changed
     * Change the parent input target element.
     * If Null, then it is being set the to main element.
     * @type {Null|EventTarget}
     * @default null
     */
    inputTarget: null,

    /**
     * force an input class
     * @type {Null|Function}
     * @default null
     */
    inputClass: null,

    /**
     * Default recognizer setup when calling `Hammer()`
     * When creating a new Manager these will be skipped.
     * @type {Array}
     */
    preset: [
        // RecognizerClass, options, [recognizeWith, ...], [requireFailure, ...]
        [RotateRecognizer, {enable: false}],
        [PinchRecognizer, {enable: false}, ['rotate']],
        [SwipeRecognizer, {direction: DIRECTION_HORIZONTAL}],
        [PanRecognizer, {direction: DIRECTION_HORIZONTAL}, ['swipe']],
        [TapRecognizer],
        [TapRecognizer, {event: 'doubletap', taps: 2}, ['tap']],
        [PressRecognizer]
    ],

    /**
     * Some CSS properties can be used to improve the working of Hammer.
     * Add them to this method and they will be set when creating a new Manager.
     * @namespace
     */
    cssProps: {
        /**
         * Disables text selection to improve the dragging gesture. Mainly for desktop browsers.
         * @type {String}
         * @default 'none'
         */
        userSelect: 'none',

        /**
         * Disable the Windows Phone grippers when pressing an element.
         * @type {String}
         * @default 'none'
         */
        touchSelect: 'none',

        /**
         * Disables the default callout shown when you touch and hold a touch target.
         * On iOS, when you touch and hold a touch target such as a link, Safari displays
         * a callout containing information about the link. This property allows you to disable that callout.
         * @type {String}
         * @default 'none'
         */
        touchCallout: 'none',

        /**
         * Specifies whether zooming is enabled. Used by IE10>
         * @type {String}
         * @default 'none'
         */
        contentZooming: 'none',

        /**
         * Specifies that an entire element should be draggable instead of its contents. Mainly for desktop browsers.
         * @type {String}
         * @default 'none'
         */
        userDrag: 'none',

        /**
         * Overrides the highlight color shown when the user taps a link or a JavaScript
         * clickable element in iOS. This property obeys the alpha value, if specified.
         * @type {String}
         * @default 'rgba(0,0,0,0)'
         */
        tapHighlightColor: 'rgba(0,0,0,0)'
    }
};

var STOP = 1;
var FORCED_STOP = 2;

/**
 * Manager
 * @param {HTMLElement} element
 * @param {Object} [options]
 * @constructor
 */
function Manager(element, options) {
    this.options = assign({}, Hammer.defaults, options || {});

    this.options.inputTarget = this.options.inputTarget || element;

    this.handlers = {};
    this.session = {};
    this.recognizers = [];
    this.oldCssProps = {};

    this.element = element;
    this.input = createInputInstance(this);
    this.touchAction = new TouchAction(this, this.options.touchAction);

    toggleCssProps(this, true);

    each(this.options.recognizers, function(item) {
        var recognizer = this.add(new (item[0])(item[1]));
        item[2] && recognizer.recognizeWith(item[2]);
        item[3] && recognizer.requireFailure(item[3]);
    }, this);
}

Manager.prototype = {
    /**
     * set options
     * @param {Object} options
     * @returns {Manager}
     */
    set: function(options) {
        assign(this.options, options);

        // Options that need a little more setup
        if (options.touchAction) {
            this.touchAction.update();
        }
        if (options.inputTarget) {
            // Clean up existing event listeners and reinitialize
            this.input.destroy();
            this.input.target = options.inputTarget;
            this.input.init();
        }
        return this;
    },

    /**
     * stop recognizing for this session.
     * This session will be discarded, when a new [input]start event is fired.
     * When forced, the recognizer cycle is stopped immediately.
     * @param {Boolean} [force]
     */
    stop: function(force) {
        this.session.stopped = force ? FORCED_STOP : STOP;
    },

    /**
     * run the recognizers!
     * called by the inputHandler function on every movement of the pointers (touches)
     * it walks through all the recognizers and tries to detect the gesture that is being made
     * @param {Object} inputData
     */
    recognize: function(inputData) {
        var session = this.session;
        if (session.stopped) {
            return;
        }

        // run the touch-action polyfill
        this.touchAction.preventDefaults(inputData);

        var recognizer;
        var recognizers = this.recognizers;

        // this holds the recognizer that is being recognized.
        // so the recognizer's state needs to be BEGAN, CHANGED, ENDED or RECOGNIZED
        // if no recognizer is detecting a thing, it is set to `null`
        var curRecognizer = session.curRecognizer;

        // reset when the last recognizer is recognized
        // or when we're in a new session
        if (!curRecognizer || (curRecognizer && curRecognizer.state & STATE_RECOGNIZED)) {
            curRecognizer = session.curRecognizer = null;
        }

        var i = 0;
        while (i < recognizers.length) {
            recognizer = recognizers[i];

            // find out if we are allowed try to recognize the input for this one.
            // 1.   allow if the session is NOT forced stopped (see the .stop() method)
            // 2.   allow if we still haven't recognized a gesture in this session, or the this recognizer is the one
            //      that is being recognized.
            // 3.   allow if the recognizer is allowed to run simultaneous with the current recognized recognizer.
            //      this can be setup with the `recognizeWith()` method on the recognizer.
            if (session.stopped !== FORCED_STOP && ( // 1
                    !curRecognizer || recognizer == curRecognizer || // 2
                    recognizer.canRecognizeWith(curRecognizer))) { // 3
                recognizer.recognize(inputData);
            } else {
                recognizer.reset();
            }

            // if the recognizer has been recognizing the input as a valid gesture, we want to store this one as the
            // current active recognizer. but only if we don't already have an active recognizer
            if (!curRecognizer && recognizer.state & (STATE_BEGAN | STATE_CHANGED | STATE_ENDED)) {
                curRecognizer = session.curRecognizer = recognizer;
            }
            i++;
        }
    },

    /**
     * get a recognizer by its event name.
     * @param {Recognizer|String} recognizer
     * @returns {Recognizer|Null}
     */
    get: function(recognizer) {
        if (recognizer instanceof Recognizer) {
            return recognizer;
        }

        var recognizers = this.recognizers;
        for (var i = 0; i < recognizers.length; i++) {
            if (recognizers[i].options.event == recognizer) {
                return recognizers[i];
            }
        }
        return null;
    },

    /**
     * add a recognizer to the manager
     * existing recognizers with the same event name will be removed
     * @param {Recognizer} recognizer
     * @returns {Recognizer|Manager}
     */
    add: function(recognizer) {
        if (invokeArrayArg(recognizer, 'add', this)) {
            return this;
        }

        // remove existing
        var existing = this.get(recognizer.options.event);
        if (existing) {
            this.remove(existing);
        }

        this.recognizers.push(recognizer);
        recognizer.manager = this;

        this.touchAction.update();
        return recognizer;
    },

    /**
     * remove a recognizer by name or instance
     * @param {Recognizer|String} recognizer
     * @returns {Manager}
     */
    remove: function(recognizer) {
        if (invokeArrayArg(recognizer, 'remove', this)) {
            return this;
        }

        recognizer = this.get(recognizer);

        // let's make sure this recognizer exists
        if (recognizer) {
            var recognizers = this.recognizers;
            var index = inArray(recognizers, recognizer);

            if (index !== -1) {
                recognizers.splice(index, 1);
                this.touchAction.update();
            }
        }

        return this;
    },

    /**
     * bind event
     * @param {String} events
     * @param {Function} handler
     * @returns {EventEmitter} this
     */
    on: function(events, handler) {
        if (events === undefined) {
            return;
        }
        if (handler === undefined) {
            return;
        }

        var handlers = this.handlers;
        each(splitStr(events), function(event) {
            handlers[event] = handlers[event] || [];
            handlers[event].push(handler);
        });
        return this;
    },

    /**
     * unbind event, leave emit blank to remove all handlers
     * @param {String} events
     * @param {Function} [handler]
     * @returns {EventEmitter} this
     */
    off: function(events, handler) {
        if (events === undefined) {
            return;
        }

        var handlers = this.handlers;
        each(splitStr(events), function(event) {
            if (!handler) {
                delete handlers[event];
            } else {
                handlers[event] && handlers[event].splice(inArray(handlers[event], handler), 1);
            }
        });
        return this;
    },

    /**
     * emit event to the listeners
     * @param {String} event
     * @param {Object} data
     */
    emit: function(event, data) {
        // we also want to trigger dom events
        if (this.options.domEvents) {
            triggerDomEvent(event, data);
        }

        // no handlers, so skip it all
        var handlers = this.handlers[event] && this.handlers[event].slice();
        if (!handlers || !handlers.length) {
            return;
        }

        data.type = event;
        data.preventDefault = function() {
            data.srcEvent.preventDefault();
        };

        var i = 0;
        while (i < handlers.length) {
            handlers[i](data);
            i++;
        }
    },

    /**
     * destroy the manager and unbinds all events
     * it doesn't unbind dom events, that is the user own responsibility
     */
    destroy: function() {
        this.element && toggleCssProps(this, false);

        this.handlers = {};
        this.session = {};
        this.input.destroy();
        this.element = null;
    }
};

/**
 * add/remove the css properties as defined in manager.options.cssProps
 * @param {Manager} manager
 * @param {Boolean} add
 */
function toggleCssProps(manager, add) {
    var element = manager.element;
    if (!element.style) {
        return;
    }
    var prop;
    each(manager.options.cssProps, function(value, name) {
        prop = prefixed(element.style, name);
        if (add) {
            manager.oldCssProps[prop] = element.style[prop];
            element.style[prop] = value;
        } else {
            element.style[prop] = manager.oldCssProps[prop] || '';
        }
    });
    if (!add) {
        manager.oldCssProps = {};
    }
}

/**
 * trigger dom event
 * @param {String} event
 * @param {Object} data
 */
function triggerDomEvent(event, data) {
    var gestureEvent = document.createEvent('Event');
    gestureEvent.initEvent(event, true, true);
    gestureEvent.gesture = data;
    data.target.dispatchEvent(gestureEvent);
}

assign(Hammer, {
    INPUT_START: INPUT_START,
    INPUT_MOVE: INPUT_MOVE,
    INPUT_END: INPUT_END,
    INPUT_CANCEL: INPUT_CANCEL,

    STATE_POSSIBLE: STATE_POSSIBLE,
    STATE_BEGAN: STATE_BEGAN,
    STATE_CHANGED: STATE_CHANGED,
    STATE_ENDED: STATE_ENDED,
    STATE_RECOGNIZED: STATE_RECOGNIZED,
    STATE_CANCELLED: STATE_CANCELLED,
    STATE_FAILED: STATE_FAILED,

    DIRECTION_NONE: DIRECTION_NONE,
    DIRECTION_LEFT: DIRECTION_LEFT,
    DIRECTION_RIGHT: DIRECTION_RIGHT,
    DIRECTION_UP: DIRECTION_UP,
    DIRECTION_DOWN: DIRECTION_DOWN,
    DIRECTION_HORIZONTAL: DIRECTION_HORIZONTAL,
    DIRECTION_VERTICAL: DIRECTION_VERTICAL,
    DIRECTION_ALL: DIRECTION_ALL,

    Manager: Manager,
    Input: Input,
    TouchAction: TouchAction,

    TouchInput: TouchInput,
    MouseInput: MouseInput,
    PointerEventInput: PointerEventInput,
    TouchMouseInput: TouchMouseInput,
    SingleTouchInput: SingleTouchInput,

    Recognizer: Recognizer,
    AttrRecognizer: AttrRecognizer,
    Tap: TapRecognizer,
    Pan: PanRecognizer,
    Swipe: SwipeRecognizer,
    Pinch: PinchRecognizer,
    Rotate: RotateRecognizer,
    Press: PressRecognizer,

    on: addEventListeners,
    off: removeEventListeners,
    each: each,
    merge: merge,
    extend: extend,
    assign: assign,
    inherit: inherit,
    bindFn: bindFn,
    prefixed: prefixed
});

// this prevents errors when Hammer is loaded in the presence of an AMD
//  style loader but by script tag, not by the loader.
var freeGlobal = (typeof window !== 'undefined' ? window : (typeof self !== 'undefined' ? self : {})); // jshint ignore:line
freeGlobal.Hammer = Hammer;

if (true) {
    !(__WEBPACK_AMD_DEFINE_RESULT__ = (function() {
        return Hammer;
    }).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
} else if (typeof module != 'undefined' && module.exports) {
    module.exports = Hammer;
} else {
    window[exportName] = Hammer;
}

})(window, document, 'Hammer');


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * UAParser.js v0.7.18
 * Lightweight JavaScript-based User-Agent string parser
 * https://github.com/faisalman/ua-parser-js
 *
 * Copyright © 2012-2016 Faisal Salman <fyzlman@gmail.com>
 * Dual licensed under GPLv2 or MIT
 */

(function (window, undefined) {

    'use strict';

    //////////////
    // Constants
    /////////////


    var LIBVERSION  = '0.7.18',
        EMPTY       = '',
        UNKNOWN     = '?',
        FUNC_TYPE   = 'function',
        UNDEF_TYPE  = 'undefined',
        OBJ_TYPE    = 'object',
        STR_TYPE    = 'string',
        MAJOR       = 'major', // deprecated
        MODEL       = 'model',
        NAME        = 'name',
        TYPE        = 'type',
        VENDOR      = 'vendor',
        VERSION     = 'version',
        ARCHITECTURE= 'architecture',
        CONSOLE     = 'console',
        MOBILE      = 'mobile',
        TABLET      = 'tablet',
        SMARTTV     = 'smarttv',
        WEARABLE    = 'wearable',
        EMBEDDED    = 'embedded';


    ///////////
    // Helper
    //////////


    var util = {
        extend : function (regexes, extensions) {
            var margedRegexes = {};
            for (var i in regexes) {
                if (extensions[i] && extensions[i].length % 2 === 0) {
                    margedRegexes[i] = extensions[i].concat(regexes[i]);
                } else {
                    margedRegexes[i] = regexes[i];
                }
            }
            return margedRegexes;
        },
        has : function (str1, str2) {
          if (typeof str1 === "string") {
            return str2.toLowerCase().indexOf(str1.toLowerCase()) !== -1;
          } else {
            return false;
          }
        },
        lowerize : function (str) {
            return str.toLowerCase();
        },
        major : function (version) {
            return typeof(version) === STR_TYPE ? version.replace(/[^\d\.]/g,'').split(".")[0] : undefined;
        },
        trim : function (str) {
          return str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
        }
    };


    ///////////////
    // Map helper
    //////////////


    var mapper = {

        rgx : function (ua, arrays) {

            //var result = {},
            var i = 0, j, k, p, q, matches, match;//, args = arguments;

            /*// construct object barebones
            for (p = 0; p < args[1].length; p++) {
                q = args[1][p];
                result[typeof q === OBJ_TYPE ? q[0] : q] = undefined;
            }*/

            // loop through all regexes maps
            while (i < arrays.length && !matches) {

                var regex = arrays[i],       // even sequence (0,2,4,..)
                    props = arrays[i + 1];   // odd sequence (1,3,5,..)
                j = k = 0;

                // try matching uastring with regexes
                while (j < regex.length && !matches) {

                    matches = regex[j++].exec(ua);

                    if (!!matches) {
                        for (p = 0; p < props.length; p++) {
                            match = matches[++k];
                            q = props[p];
                            // check if given property is actually array
                            if (typeof q === OBJ_TYPE && q.length > 0) {
                                if (q.length == 2) {
                                    if (typeof q[1] == FUNC_TYPE) {
                                        // assign modified match
                                        this[q[0]] = q[1].call(this, match);
                                    } else {
                                        // assign given value, ignore regex match
                                        this[q[0]] = q[1];
                                    }
                                } else if (q.length == 3) {
                                    // check whether function or regex
                                    if (typeof q[1] === FUNC_TYPE && !(q[1].exec && q[1].test)) {
                                        // call function (usually string mapper)
                                        this[q[0]] = match ? q[1].call(this, match, q[2]) : undefined;
                                    } else {
                                        // sanitize match using given regex
                                        this[q[0]] = match ? match.replace(q[1], q[2]) : undefined;
                                    }
                                } else if (q.length == 4) {
                                        this[q[0]] = match ? q[3].call(this, match.replace(q[1], q[2])) : undefined;
                                }
                            } else {
                                this[q] = match ? match : undefined;
                            }
                        }
                    }
                }
                i += 2;
            }
            // console.log(this);
            //return this;
        },

        str : function (str, map) {

            for (var i in map) {
                // check if array
                if (typeof map[i] === OBJ_TYPE && map[i].length > 0) {
                    for (var j = 0; j < map[i].length; j++) {
                        if (util.has(map[i][j], str)) {
                            return (i === UNKNOWN) ? undefined : i;
                        }
                    }
                } else if (util.has(map[i], str)) {
                    return (i === UNKNOWN) ? undefined : i;
                }
            }
            return str;
        }
    };


    ///////////////
    // String map
    //////////////


    var maps = {

        browser : {
            oldsafari : {
                version : {
                    '1.0'   : '/8',
                    '1.2'   : '/1',
                    '1.3'   : '/3',
                    '2.0'   : '/412',
                    '2.0.2' : '/416',
                    '2.0.3' : '/417',
                    '2.0.4' : '/419',
                    '?'     : '/'
                }
            }
        },

        device : {
            amazon : {
                model : {
                    'Fire Phone' : ['SD', 'KF']
                }
            },
            sprint : {
                model : {
                    'Evo Shift 4G' : '7373KT'
                },
                vendor : {
                    'HTC'       : 'APA',
                    'Sprint'    : 'Sprint'
                }
            }
        },

        os : {
            windows : {
                version : {
                    'ME'        : '4.90',
                    'NT 3.11'   : 'NT3.51',
                    'NT 4.0'    : 'NT4.0',
                    '2000'      : 'NT 5.0',
                    'XP'        : ['NT 5.1', 'NT 5.2'],
                    'Vista'     : 'NT 6.0',
                    '7'         : 'NT 6.1',
                    '8'         : 'NT 6.2',
                    '8.1'       : 'NT 6.3',
                    '10'        : ['NT 6.4', 'NT 10.0'],
                    'RT'        : 'ARM'
                }
            }
        }
    };


    //////////////
    // Regex map
    /////////////


    var regexes = {

        browser : [[

            // Presto based
            /(opera\smini)\/([\w\.-]+)/i,                                       // Opera Mini
            /(opera\s[mobiletab]+).+version\/([\w\.-]+)/i,                      // Opera Mobi/Tablet
            /(opera).+version\/([\w\.]+)/i,                                     // Opera > 9.80
            /(opera)[\/\s]+([\w\.]+)/i                                          // Opera < 9.80
            ], [NAME, VERSION], [

            /(opios)[\/\s]+([\w\.]+)/i                                          // Opera mini on iphone >= 8.0
            ], [[NAME, 'Opera Mini'], VERSION], [

            /\s(opr)\/([\w\.]+)/i                                               // Opera Webkit
            ], [[NAME, 'Opera'], VERSION], [

            // Mixed
            /(kindle)\/([\w\.]+)/i,                                             // Kindle
            /(lunascape|maxthon|netfront|jasmine|blazer)[\/\s]?([\w\.]*)/i,
                                                                                // Lunascape/Maxthon/Netfront/Jasmine/Blazer

            // Trident based
            /(avant\s|iemobile|slim|baidu)(?:browser)?[\/\s]?([\w\.]*)/i,
                                                                                // Avant/IEMobile/SlimBrowser/Baidu
            /(?:ms|\()(ie)\s([\w\.]+)/i,                                        // Internet Explorer

            // Webkit/KHTML based
            /(rekonq)\/([\w\.]*)/i,                                             // Rekonq
            /(chromium|flock|rockmelt|midori|epiphany|silk|skyfire|ovibrowser|bolt|iron|vivaldi|iridium|phantomjs|bowser|quark)\/([\w\.-]+)/i
                                                                                // Chromium/Flock/RockMelt/Midori/Epiphany/Silk/Skyfire/Bolt/Iron/Iridium/PhantomJS/Bowser
            ], [NAME, VERSION], [

            /(trident).+rv[:\s]([\w\.]+).+like\sgecko/i                         // IE11
            ], [[NAME, 'IE'], VERSION], [

            /(edge|edgios|edgea)\/((\d+)?[\w\.]+)/i                             // Microsoft Edge
            ], [[NAME, 'Edge'], VERSION], [

            /(yabrowser)\/([\w\.]+)/i                                           // Yandex
            ], [[NAME, 'Yandex'], VERSION], [

            /(puffin)\/([\w\.]+)/i                                              // Puffin
            ], [[NAME, 'Puffin'], VERSION], [

            /((?:[\s\/])uc?\s?browser|(?:juc.+)ucweb)[\/\s]?([\w\.]+)/i
                                                                                // UCBrowser
            ], [[NAME, 'UCBrowser'], VERSION], [

            /(comodo_dragon)\/([\w\.]+)/i                                       // Comodo Dragon
            ], [[NAME, /_/g, ' '], VERSION], [

            /(micromessenger)\/([\w\.]+)/i                                      // WeChat
            ], [[NAME, 'WeChat'], VERSION], [

            /(qqbrowserlite)\/([\w\.]+)/i                                       // QQBrowserLite
            ], [NAME, VERSION], [

            /(QQ)\/([\d\.]+)/i                                                  // QQ, aka ShouQ
            ], [NAME, VERSION], [

            /m?(qqbrowser)[\/\s]?([\w\.]+)/i                                    // QQBrowser
            ], [NAME, VERSION], [

            /(BIDUBrowser)[\/\s]?([\w\.]+)/i                                    // Baidu Browser
            ], [NAME, VERSION], [

            /(2345Explorer)[\/\s]?([\w\.]+)/i                                   // 2345 Browser
            ], [NAME, VERSION], [

            /(MetaSr)[\/\s]?([\w\.]+)/i                                         // SouGouBrowser
            ], [NAME], [

            /(LBBROWSER)/i                                      // LieBao Browser
            ], [NAME], [

            /xiaomi\/miuibrowser\/([\w\.]+)/i                                   // MIUI Browser
            ], [VERSION, [NAME, 'MIUI Browser']], [

            /;fbav\/([\w\.]+);/i                                                // Facebook App for iOS & Android
            ], [VERSION, [NAME, 'Facebook']], [

            /headlesschrome(?:\/([\w\.]+)|\s)/i                                 // Chrome Headless
            ], [VERSION, [NAME, 'Chrome Headless']], [

            /\swv\).+(chrome)\/([\w\.]+)/i                                      // Chrome WebView
            ], [[NAME, /(.+)/, '$1 WebView'], VERSION], [

            /((?:oculus|samsung)browser)\/([\w\.]+)/i
            ], [[NAME, /(.+(?:g|us))(.+)/, '$1 $2'], VERSION], [                // Oculus / Samsung Browser

            /android.+version\/([\w\.]+)\s+(?:mobile\s?safari|safari)*/i        // Android Browser
            ], [VERSION, [NAME, 'Android Browser']], [

            /(chrome|omniweb|arora|[tizenoka]{5}\s?browser)\/v?([\w\.]+)/i
                                                                                // Chrome/OmniWeb/Arora/Tizen/Nokia
            ], [NAME, VERSION], [

            /(dolfin)\/([\w\.]+)/i                                              // Dolphin
            ], [[NAME, 'Dolphin'], VERSION], [

            /((?:android.+)crmo|crios)\/([\w\.]+)/i                             // Chrome for Android/iOS
            ], [[NAME, 'Chrome'], VERSION], [

            /(coast)\/([\w\.]+)/i                                               // Opera Coast
            ], [[NAME, 'Opera Coast'], VERSION], [

            /fxios\/([\w\.-]+)/i                                                // Firefox for iOS
            ], [VERSION, [NAME, 'Firefox']], [

            /version\/([\w\.]+).+?mobile\/\w+\s(safari)/i                       // Mobile Safari
            ], [VERSION, [NAME, 'Mobile Safari']], [

            /version\/([\w\.]+).+?(mobile\s?safari|safari)/i                    // Safari & Safari Mobile
            ], [VERSION, NAME], [

            /webkit.+?(gsa)\/([\w\.]+).+?(mobile\s?safari|safari)(\/[\w\.]+)/i  // Google Search Appliance on iOS
            ], [[NAME, 'GSA'], VERSION], [

            /webkit.+?(mobile\s?safari|safari)(\/[\w\.]+)/i                     // Safari < 3.0
            ], [NAME, [VERSION, mapper.str, maps.browser.oldsafari.version]], [

            /(konqueror)\/([\w\.]+)/i,                                          // Konqueror
            /(webkit|khtml)\/([\w\.]+)/i
            ], [NAME, VERSION], [

            // Gecko based
            /(navigator|netscape)\/([\w\.-]+)/i                                 // Netscape
            ], [[NAME, 'Netscape'], VERSION], [
            /(swiftfox)/i,                                                      // Swiftfox
            /(icedragon|iceweasel|camino|chimera|fennec|maemo\sbrowser|minimo|conkeror)[\/\s]?([\w\.\+]+)/i,
                                                                                // IceDragon/Iceweasel/Camino/Chimera/Fennec/Maemo/Minimo/Conkeror
            /(firefox|seamonkey|k-meleon|icecat|iceape|firebird|phoenix|palemoon|basilisk|waterfox)\/([\w\.-]+)$/i,

                                                                                // Firefox/SeaMonkey/K-Meleon/IceCat/IceApe/Firebird/Phoenix
            /(mozilla)\/([\w\.]+).+rv\:.+gecko\/\d+/i,                          // Mozilla

            // Other
            /(polaris|lynx|dillo|icab|doris|amaya|w3m|netsurf|sleipnir)[\/\s]?([\w\.]+)/i,
                                                                                // Polaris/Lynx/Dillo/iCab/Doris/Amaya/w3m/NetSurf/Sleipnir
            /(links)\s\(([\w\.]+)/i,                                            // Links
            /(gobrowser)\/?([\w\.]*)/i,                                         // GoBrowser
            /(ice\s?browser)\/v?([\w\._]+)/i,                                   // ICE Browser
            /(mosaic)[\/\s]([\w\.]+)/i                                          // Mosaic
            ], [NAME, VERSION]

            /* /////////////////////
            // Media players BEGIN
            ////////////////////////

            , [

            /(apple(?:coremedia|))\/((\d+)[\w\._]+)/i,                          // Generic Apple CoreMedia
            /(coremedia) v((\d+)[\w\._]+)/i
            ], [NAME, VERSION], [

            /(aqualung|lyssna|bsplayer)\/((\d+)?[\w\.-]+)/i                     // Aqualung/Lyssna/BSPlayer
            ], [NAME, VERSION], [

            /(ares|ossproxy)\s((\d+)[\w\.-]+)/i                                 // Ares/OSSProxy
            ], [NAME, VERSION], [

            /(audacious|audimusicstream|amarok|bass|core|dalvik|gnomemplayer|music on console|nsplayer|psp-internetradioplayer|videos)\/((\d+)[\w\.-]+)/i,
                                                                                // Audacious/AudiMusicStream/Amarok/BASS/OpenCORE/Dalvik/GnomeMplayer/MoC
                                                                                // NSPlayer/PSP-InternetRadioPlayer/Videos
            /(clementine|music player daemon)\s((\d+)[\w\.-]+)/i,               // Clementine/MPD
            /(lg player|nexplayer)\s((\d+)[\d\.]+)/i,
            /player\/(nexplayer|lg player)\s((\d+)[\w\.-]+)/i                   // NexPlayer/LG Player
            ], [NAME, VERSION], [
            /(nexplayer)\s((\d+)[\w\.-]+)/i                                     // Nexplayer
            ], [NAME, VERSION], [

            /(flrp)\/((\d+)[\w\.-]+)/i                                          // Flip Player
            ], [[NAME, 'Flip Player'], VERSION], [

            /(fstream|nativehost|queryseekspider|ia-archiver|facebookexternalhit)/i
                                                                                // FStream/NativeHost/QuerySeekSpider/IA Archiver/facebookexternalhit
            ], [NAME], [

            /(gstreamer) souphttpsrc (?:\([^\)]+\)){0,1} libsoup\/((\d+)[\w\.-]+)/i
                                                                                // Gstreamer
            ], [NAME, VERSION], [

            /(htc streaming player)\s[\w_]+\s\/\s((\d+)[\d\.]+)/i,              // HTC Streaming Player
            /(java|python-urllib|python-requests|wget|libcurl)\/((\d+)[\w\.-_]+)/i,
                                                                                // Java/urllib/requests/wget/cURL
            /(lavf)((\d+)[\d\.]+)/i                                             // Lavf (FFMPEG)
            ], [NAME, VERSION], [

            /(htc_one_s)\/((\d+)[\d\.]+)/i                                      // HTC One S
            ], [[NAME, /_/g, ' '], VERSION], [

            /(mplayer)(?:\s|\/)(?:(?:sherpya-){0,1}svn)(?:-|\s)(r\d+(?:-\d+[\w\.-]+){0,1})/i
                                                                                // MPlayer SVN
            ], [NAME, VERSION], [

            /(mplayer)(?:\s|\/|[unkow-]+)((\d+)[\w\.-]+)/i                      // MPlayer
            ], [NAME, VERSION], [

            /(mplayer)/i,                                                       // MPlayer (no other info)
            /(yourmuze)/i,                                                      // YourMuze
            /(media player classic|nero showtime)/i                             // Media Player Classic/Nero ShowTime
            ], [NAME], [

            /(nero (?:home|scout))\/((\d+)[\w\.-]+)/i                           // Nero Home/Nero Scout
            ], [NAME, VERSION], [

            /(nokia\d+)\/((\d+)[\w\.-]+)/i                                      // Nokia
            ], [NAME, VERSION], [

            /\s(songbird)\/((\d+)[\w\.-]+)/i                                    // Songbird/Philips-Songbird
            ], [NAME, VERSION], [

            /(winamp)3 version ((\d+)[\w\.-]+)/i,                               // Winamp
            /(winamp)\s((\d+)[\w\.-]+)/i,
            /(winamp)mpeg\/((\d+)[\w\.-]+)/i
            ], [NAME, VERSION], [

            /(ocms-bot|tapinradio|tunein radio|unknown|winamp|inlight radio)/i  // OCMS-bot/tap in radio/tunein/unknown/winamp (no other info)
                                                                                // inlight radio
            ], [NAME], [

            /(quicktime|rma|radioapp|radioclientapplication|soundtap|totem|stagefright|streamium)\/((\d+)[\w\.-]+)/i
                                                                                // QuickTime/RealMedia/RadioApp/RadioClientApplication/
                                                                                // SoundTap/Totem/Stagefright/Streamium
            ], [NAME, VERSION], [

            /(smp)((\d+)[\d\.]+)/i                                              // SMP
            ], [NAME, VERSION], [

            /(vlc) media player - version ((\d+)[\w\.]+)/i,                     // VLC Videolan
            /(vlc)\/((\d+)[\w\.-]+)/i,
            /(xbmc|gvfs|xine|xmms|irapp)\/((\d+)[\w\.-]+)/i,                    // XBMC/gvfs/Xine/XMMS/irapp
            /(foobar2000)\/((\d+)[\d\.]+)/i,                                    // Foobar2000
            /(itunes)\/((\d+)[\d\.]+)/i                                         // iTunes
            ], [NAME, VERSION], [

            /(wmplayer)\/((\d+)[\w\.-]+)/i,                                     // Windows Media Player
            /(windows-media-player)\/((\d+)[\w\.-]+)/i
            ], [[NAME, /-/g, ' '], VERSION], [

            /windows\/((\d+)[\w\.-]+) upnp\/[\d\.]+ dlnadoc\/[\d\.]+ (home media server)/i
                                                                                // Windows Media Server
            ], [VERSION, [NAME, 'Windows']], [

            /(com\.riseupradioalarm)\/((\d+)[\d\.]*)/i                          // RiseUP Radio Alarm
            ], [NAME, VERSION], [

            /(rad.io)\s((\d+)[\d\.]+)/i,                                        // Rad.io
            /(radio.(?:de|at|fr))\s((\d+)[\d\.]+)/i
            ], [[NAME, 'rad.io'], VERSION]

            //////////////////////
            // Media players END
            ////////////////////*/

        ],

        cpu : [[

            /(?:(amd|x(?:(?:86|64)[_-])?|wow|win)64)[;\)]/i                     // AMD64
            ], [[ARCHITECTURE, 'amd64']], [

            /(ia32(?=;))/i                                                      // IA32 (quicktime)
            ], [[ARCHITECTURE, util.lowerize]], [

            /((?:i[346]|x)86)[;\)]/i                                            // IA32
            ], [[ARCHITECTURE, 'ia32']], [

            // PocketPC mistakenly identified as PowerPC
            /windows\s(ce|mobile);\sppc;/i
            ], [[ARCHITECTURE, 'arm']], [

            /((?:ppc|powerpc)(?:64)?)(?:\smac|;|\))/i                           // PowerPC
            ], [[ARCHITECTURE, /ower/, '', util.lowerize]], [

            /(sun4\w)[;\)]/i                                                    // SPARC
            ], [[ARCHITECTURE, 'sparc']], [

            /((?:avr32|ia64(?=;))|68k(?=\))|arm(?:64|(?=v\d+;))|(?=atmel\s)avr|(?:irix|mips|sparc)(?:64)?(?=;)|pa-risc)/i
                                                                                // IA64, 68K, ARM/64, AVR/32, IRIX/64, MIPS/64, SPARC/64, PA-RISC
            ], [[ARCHITECTURE, util.lowerize]]
        ],

        device : [[

            /\((ipad|playbook);[\w\s\);-]+(rim|apple)/i                         // iPad/PlayBook
            ], [MODEL, VENDOR, [TYPE, TABLET]], [

            /applecoremedia\/[\w\.]+ \((ipad)/                                  // iPad
            ], [MODEL, [VENDOR, 'Apple'], [TYPE, TABLET]], [

            /(apple\s{0,1}tv)/i                                                 // Apple TV
            ], [[MODEL, 'Apple TV'], [VENDOR, 'Apple']], [

            /(archos)\s(gamepad2?)/i,                                           // Archos
            /(hp).+(touchpad)/i,                                                // HP TouchPad
            /(hp).+(tablet)/i,                                                  // HP Tablet
            /(kindle)\/([\w\.]+)/i,                                             // Kindle
            /\s(nook)[\w\s]+build\/(\w+)/i,                                     // Nook
            /(dell)\s(strea[kpr\s\d]*[\dko])/i                                  // Dell Streak
            ], [VENDOR, MODEL, [TYPE, TABLET]], [

            /(kf[A-z]+)\sbuild\/.+silk\//i                                      // Kindle Fire HD
            ], [MODEL, [VENDOR, 'Amazon'], [TYPE, TABLET]], [
            /(sd|kf)[0349hijorstuw]+\sbuild\/.+silk\//i                         // Fire Phone
            ], [[MODEL, mapper.str, maps.device.amazon.model], [VENDOR, 'Amazon'], [TYPE, MOBILE]], [

            /\((ip[honed|\s\w*]+);.+(apple)/i                                   // iPod/iPhone
            ], [MODEL, VENDOR, [TYPE, MOBILE]], [
            /\((ip[honed|\s\w*]+);/i                                            // iPod/iPhone
            ], [MODEL, [VENDOR, 'Apple'], [TYPE, MOBILE]], [

            /(blackberry)[\s-]?(\w+)/i,                                         // BlackBerry
            /(blackberry|benq|palm(?=\-)|sonyericsson|acer|asus|dell|meizu|motorola|polytron)[\s_-]?([\w-]*)/i,
                                                                                // BenQ/Palm/Sony-Ericsson/Acer/Asus/Dell/Meizu/Motorola/Polytron
            /(hp)\s([\w\s]+\w)/i,                                               // HP iPAQ
            /(asus)-?(\w+)/i                                                    // Asus
            ], [VENDOR, MODEL, [TYPE, MOBILE]], [
            /\(bb10;\s(\w+)/i                                                   // BlackBerry 10
            ], [MODEL, [VENDOR, 'BlackBerry'], [TYPE, MOBILE]], [
                                                                                // Asus Tablets
            /android.+(transfo[prime\s]{4,10}\s\w+|eeepc|slider\s\w+|nexus 7|padfone)/i
            ], [MODEL, [VENDOR, 'Asus'], [TYPE, TABLET]], [

            /(sony)\s(tablet\s[ps])\sbuild\//i,                                  // Sony
            /(sony)?(?:sgp.+)\sbuild\//i
            ], [[VENDOR, 'Sony'], [MODEL, 'Xperia Tablet'], [TYPE, TABLET]], [
            /android.+\s([c-g]\d{4}|so[-l]\w+)\sbuild\//i
            ], [MODEL, [VENDOR, 'Sony'], [TYPE, MOBILE]], [

            /\s(ouya)\s/i,                                                      // Ouya
            /(nintendo)\s([wids3u]+)/i                                          // Nintendo
            ], [VENDOR, MODEL, [TYPE, CONSOLE]], [

            /android.+;\s(shield)\sbuild/i                                      // Nvidia
            ], [MODEL, [VENDOR, 'Nvidia'], [TYPE, CONSOLE]], [

            /(playstation\s[34portablevi]+)/i                                   // Playstation
            ], [MODEL, [VENDOR, 'Sony'], [TYPE, CONSOLE]], [

            /(sprint\s(\w+))/i                                                  // Sprint Phones
            ], [[VENDOR, mapper.str, maps.device.sprint.vendor], [MODEL, mapper.str, maps.device.sprint.model], [TYPE, MOBILE]], [

            /(lenovo)\s?(S(?:5000|6000)+(?:[-][\w+]))/i                         // Lenovo tablets
            ], [VENDOR, MODEL, [TYPE, TABLET]], [

            /(htc)[;_\s-]+([\w\s]+(?=\))|\w+)*/i,                               // HTC
            /(zte)-(\w*)/i,                                                     // ZTE
            /(alcatel|geeksphone|lenovo|nexian|panasonic|(?=;\s)sony)[_\s-]?([\w-]*)/i
                                                                                // Alcatel/GeeksPhone/Lenovo/Nexian/Panasonic/Sony
            ], [VENDOR, [MODEL, /_/g, ' '], [TYPE, MOBILE]], [

            /(nexus\s9)/i                                                       // HTC Nexus 9
            ], [MODEL, [VENDOR, 'HTC'], [TYPE, TABLET]], [

            /d\/huawei([\w\s-]+)[;\)]/i,
            /(nexus\s6p)/i                                                      // Huawei
            ], [MODEL, [VENDOR, 'Huawei'], [TYPE, MOBILE]], [

            /(microsoft);\s(lumia[\s\w]+)/i                                     // Microsoft Lumia
            ], [VENDOR, MODEL, [TYPE, MOBILE]], [

            /[\s\(;](xbox(?:\sone)?)[\s\);]/i                                   // Microsoft Xbox
            ], [MODEL, [VENDOR, 'Microsoft'], [TYPE, CONSOLE]], [
            /(kin\.[onetw]{3})/i                                                // Microsoft Kin
            ], [[MODEL, /\./g, ' '], [VENDOR, 'Microsoft'], [TYPE, MOBILE]], [

                                                                                // Motorola
            /\s(milestone|droid(?:[2-4x]|\s(?:bionic|x2|pro|razr))?:?(\s4g)?)[\w\s]+build\//i,
            /mot[\s-]?(\w*)/i,
            /(XT\d{3,4}) build\//i,
            /(nexus\s6)/i
            ], [MODEL, [VENDOR, 'Motorola'], [TYPE, MOBILE]], [
            /android.+\s(mz60\d|xoom[\s2]{0,2})\sbuild\//i
            ], [MODEL, [VENDOR, 'Motorola'], [TYPE, TABLET]], [

            /hbbtv\/\d+\.\d+\.\d+\s+\([\w\s]*;\s*(\w[^;]*);([^;]*)/i            // HbbTV devices
            ], [[VENDOR, util.trim], [MODEL, util.trim], [TYPE, SMARTTV]], [

            /hbbtv.+maple;(\d+)/i
            ], [[MODEL, /^/, 'SmartTV'], [VENDOR, 'Samsung'], [TYPE, SMARTTV]], [

            /\(dtv[\);].+(aquos)/i                                              // Sharp
            ], [MODEL, [VENDOR, 'Sharp'], [TYPE, SMARTTV]], [

            /android.+((sch-i[89]0\d|shw-m380s|gt-p\d{4}|gt-n\d+|sgh-t8[56]9|nexus 10))/i,
            /((SM-T\w+))/i
            ], [[VENDOR, 'Samsung'], MODEL, [TYPE, TABLET]], [                  // Samsung
            /smart-tv.+(samsung)/i
            ], [VENDOR, [TYPE, SMARTTV], MODEL], [
            /((s[cgp]h-\w+|gt-\w+|galaxy\snexus|sm-\w[\w\d]+))/i,
            /(sam[sung]*)[\s-]*(\w+-?[\w-]*)/i,
            /sec-((sgh\w+))/i
            ], [[VENDOR, 'Samsung'], MODEL, [TYPE, MOBILE]], [

            /sie-(\w*)/i                                                        // Siemens
            ], [MODEL, [VENDOR, 'Siemens'], [TYPE, MOBILE]], [

            /(maemo|nokia).*(n900|lumia\s\d+)/i,                                // Nokia
            /(nokia)[\s_-]?([\w-]*)/i
            ], [[VENDOR, 'Nokia'], MODEL, [TYPE, MOBILE]], [

            /android\s3\.[\s\w;-]{10}(a\d{3})/i                                 // Acer
            ], [MODEL, [VENDOR, 'Acer'], [TYPE, TABLET]], [

            /android.+([vl]k\-?\d{3})\s+build/i                                 // LG Tablet
            ], [MODEL, [VENDOR, 'LG'], [TYPE, TABLET]], [
            /android\s3\.[\s\w;-]{10}(lg?)-([06cv9]{3,4})/i                     // LG Tablet
            ], [[VENDOR, 'LG'], MODEL, [TYPE, TABLET]], [
            /(lg) netcast\.tv/i                                                 // LG SmartTV
            ], [VENDOR, MODEL, [TYPE, SMARTTV]], [
            /(nexus\s[45])/i,                                                   // LG
            /lg[e;\s\/-]+(\w*)/i,
            /android.+lg(\-?[\d\w]+)\s+build/i
            ], [MODEL, [VENDOR, 'LG'], [TYPE, MOBILE]], [

            /android.+(ideatab[a-z0-9\-\s]+)/i                                  // Lenovo
            ], [MODEL, [VENDOR, 'Lenovo'], [TYPE, TABLET]], [

            /linux;.+((jolla));/i                                               // Jolla
            ], [VENDOR, MODEL, [TYPE, MOBILE]], [

            /((pebble))app\/[\d\.]+\s/i                                         // Pebble
            ], [VENDOR, MODEL, [TYPE, WEARABLE]], [

            /android.+;\s(oppo)\s?([\w\s]+)\sbuild/i                            // OPPO
            ], [VENDOR, MODEL, [TYPE, MOBILE]], [

            /crkey/i                                                            // Google Chromecast
            ], [[MODEL, 'Chromecast'], [VENDOR, 'Google']], [

            /android.+;\s(glass)\s\d/i                                          // Google Glass
            ], [MODEL, [VENDOR, 'Google'], [TYPE, WEARABLE]], [

            /android.+;\s(pixel c)\s/i                                          // Google Pixel C
            ], [MODEL, [VENDOR, 'Google'], [TYPE, TABLET]], [

            /android.+;\s(pixel xl|pixel)\s/i                                   // Google Pixel
            ], [MODEL, [VENDOR, 'Google'], [TYPE, MOBILE]], [

            /android.+;\s(\w+)\s+build\/hm\1/i,                                 // Xiaomi Hongmi 'numeric' models
            /android.+(hm[\s\-_]*note?[\s_]*(?:\d\w)?)\s+build/i,               // Xiaomi Hongmi
            /android.+(mi[\s\-_]*(?:one|one[\s_]plus|note lte)?[\s_]*(?:\d?\w?)[\s_]*(?:plus)?)\s+build/i,    // Xiaomi Mi
            /android.+(redmi[\s\-_]*(?:note)?(?:[\s_]*[\w\s]+))\s+build/i       // Redmi Phones
            ], [[MODEL, /_/g, ' '], [VENDOR, 'Xiaomi'], [TYPE, MOBILE]], [
            /android.+(mi[\s\-_]*(?:pad)(?:[\s_]*[\w\s]+))\s+build/i            // Mi Pad tablets
            ],[[MODEL, /_/g, ' '], [VENDOR, 'Xiaomi'], [TYPE, TABLET]], [
            /android.+;\s(m[1-5]\snote)\sbuild/i                                // Meizu Tablet
            ], [MODEL, [VENDOR, 'Meizu'], [TYPE, TABLET]], [

            /android.+a000(1)\s+build/i,                                        // OnePlus
            /android.+oneplus\s(a\d{4})\s+build/i
            ], [MODEL, [VENDOR, 'OnePlus'], [TYPE, MOBILE]], [

            /android.+[;\/]\s*(RCT[\d\w]+)\s+build/i                            // RCA Tablets
            ], [MODEL, [VENDOR, 'RCA'], [TYPE, TABLET]], [

            /android.+[;\/\s]+(Venue[\d\s]{2,7})\s+build/i                      // Dell Venue Tablets
            ], [MODEL, [VENDOR, 'Dell'], [TYPE, TABLET]], [

            /android.+[;\/]\s*(Q[T|M][\d\w]+)\s+build/i                         // Verizon Tablet
            ], [MODEL, [VENDOR, 'Verizon'], [TYPE, TABLET]], [

            /android.+[;\/]\s+(Barnes[&\s]+Noble\s+|BN[RT])(V?.*)\s+build/i     // Barnes & Noble Tablet
            ], [[VENDOR, 'Barnes & Noble'], MODEL, [TYPE, TABLET]], [

            /android.+[;\/]\s+(TM\d{3}.*\b)\s+build/i                           // Barnes & Noble Tablet
            ], [MODEL, [VENDOR, 'NuVision'], [TYPE, TABLET]], [

            /android.+;\s(k88)\sbuild/i                                         // ZTE K Series Tablet
            ], [MODEL, [VENDOR, 'ZTE'], [TYPE, TABLET]], [

            /android.+[;\/]\s*(gen\d{3})\s+build.*49h/i                         // Swiss GEN Mobile
            ], [MODEL, [VENDOR, 'Swiss'], [TYPE, MOBILE]], [

            /android.+[;\/]\s*(zur\d{3})\s+build/i                              // Swiss ZUR Tablet
            ], [MODEL, [VENDOR, 'Swiss'], [TYPE, TABLET]], [

            /android.+[;\/]\s*((Zeki)?TB.*\b)\s+build/i                         // Zeki Tablets
            ], [MODEL, [VENDOR, 'Zeki'], [TYPE, TABLET]], [

            /(android).+[;\/]\s+([YR]\d{2})\s+build/i,
            /android.+[;\/]\s+(Dragon[\-\s]+Touch\s+|DT)(\w{5})\sbuild/i        // Dragon Touch Tablet
            ], [[VENDOR, 'Dragon Touch'], MODEL, [TYPE, TABLET]], [

            /android.+[;\/]\s*(NS-?\w{0,9})\sbuild/i                            // Insignia Tablets
            ], [MODEL, [VENDOR, 'Insignia'], [TYPE, TABLET]], [

            /android.+[;\/]\s*((NX|Next)-?\w{0,9})\s+build/i                    // NextBook Tablets
            ], [MODEL, [VENDOR, 'NextBook'], [TYPE, TABLET]], [

            /android.+[;\/]\s*(Xtreme\_)?(V(1[045]|2[015]|30|40|60|7[05]|90))\s+build/i
            ], [[VENDOR, 'Voice'], MODEL, [TYPE, MOBILE]], [                    // Voice Xtreme Phones

            /android.+[;\/]\s*(LVTEL\-)?(V1[12])\s+build/i                     // LvTel Phones
            ], [[VENDOR, 'LvTel'], MODEL, [TYPE, MOBILE]], [

            /android.+[;\/]\s*(V(100MD|700NA|7011|917G).*\b)\s+build/i          // Envizen Tablets
            ], [MODEL, [VENDOR, 'Envizen'], [TYPE, TABLET]], [

            /android.+[;\/]\s*(Le[\s\-]+Pan)[\s\-]+(\w{1,9})\s+build/i          // Le Pan Tablets
            ], [VENDOR, MODEL, [TYPE, TABLET]], [

            /android.+[;\/]\s*(Trio[\s\-]*.*)\s+build/i                         // MachSpeed Tablets
            ], [MODEL, [VENDOR, 'MachSpeed'], [TYPE, TABLET]], [

            /android.+[;\/]\s*(Trinity)[\-\s]*(T\d{3})\s+build/i                // Trinity Tablets
            ], [VENDOR, MODEL, [TYPE, TABLET]], [

            /android.+[;\/]\s*TU_(1491)\s+build/i                               // Rotor Tablets
            ], [MODEL, [VENDOR, 'Rotor'], [TYPE, TABLET]], [

            /android.+(KS(.+))\s+build/i                                        // Amazon Kindle Tablets
            ], [MODEL, [VENDOR, 'Amazon'], [TYPE, TABLET]], [

            /android.+(Gigaset)[\s\-]+(Q\w{1,9})\s+build/i                      // Gigaset Tablets
            ], [VENDOR, MODEL, [TYPE, TABLET]], [

            /\s(tablet|tab)[;\/]/i,                                             // Unidentifiable Tablet
            /\s(mobile)(?:[;\/]|\ssafari)/i                                     // Unidentifiable Mobile
            ], [[TYPE, util.lowerize], VENDOR, MODEL], [

            /(android[\w\.\s\-]{0,9});.+build/i                                 // Generic Android Device
            ], [MODEL, [VENDOR, 'Generic']]


        /*//////////////////////////
            // TODO: move to string map
            ////////////////////////////

            /(C6603)/i                                                          // Sony Xperia Z C6603
            ], [[MODEL, 'Xperia Z C6603'], [VENDOR, 'Sony'], [TYPE, MOBILE]], [
            /(C6903)/i                                                          // Sony Xperia Z 1
            ], [[MODEL, 'Xperia Z 1'], [VENDOR, 'Sony'], [TYPE, MOBILE]], [

            /(SM-G900[F|H])/i                                                   // Samsung Galaxy S5
            ], [[MODEL, 'Galaxy S5'], [VENDOR, 'Samsung'], [TYPE, MOBILE]], [
            /(SM-G7102)/i                                                       // Samsung Galaxy Grand 2
            ], [[MODEL, 'Galaxy Grand 2'], [VENDOR, 'Samsung'], [TYPE, MOBILE]], [
            /(SM-G530H)/i                                                       // Samsung Galaxy Grand Prime
            ], [[MODEL, 'Galaxy Grand Prime'], [VENDOR, 'Samsung'], [TYPE, MOBILE]], [
            /(SM-G313HZ)/i                                                      // Samsung Galaxy V
            ], [[MODEL, 'Galaxy V'], [VENDOR, 'Samsung'], [TYPE, MOBILE]], [
            /(SM-T805)/i                                                        // Samsung Galaxy Tab S 10.5
            ], [[MODEL, 'Galaxy Tab S 10.5'], [VENDOR, 'Samsung'], [TYPE, TABLET]], [
            /(SM-G800F)/i                                                       // Samsung Galaxy S5 Mini
            ], [[MODEL, 'Galaxy S5 Mini'], [VENDOR, 'Samsung'], [TYPE, MOBILE]], [
            /(SM-T311)/i                                                        // Samsung Galaxy Tab 3 8.0
            ], [[MODEL, 'Galaxy Tab 3 8.0'], [VENDOR, 'Samsung'], [TYPE, TABLET]], [

            /(T3C)/i                                                            // Advan Vandroid T3C
            ], [MODEL, [VENDOR, 'Advan'], [TYPE, TABLET]], [
            /(ADVAN T1J\+)/i                                                    // Advan Vandroid T1J+
            ], [[MODEL, 'Vandroid T1J+'], [VENDOR, 'Advan'], [TYPE, TABLET]], [
            /(ADVAN S4A)/i                                                      // Advan Vandroid S4A
            ], [[MODEL, 'Vandroid S4A'], [VENDOR, 'Advan'], [TYPE, MOBILE]], [

            /(V972M)/i                                                          // ZTE V972M
            ], [MODEL, [VENDOR, 'ZTE'], [TYPE, MOBILE]], [

            /(i-mobile)\s(IQ\s[\d\.]+)/i                                        // i-mobile IQ
            ], [VENDOR, MODEL, [TYPE, MOBILE]], [
            /(IQ6.3)/i                                                          // i-mobile IQ IQ 6.3
            ], [[MODEL, 'IQ 6.3'], [VENDOR, 'i-mobile'], [TYPE, MOBILE]], [
            /(i-mobile)\s(i-style\s[\d\.]+)/i                                   // i-mobile i-STYLE
            ], [VENDOR, MODEL, [TYPE, MOBILE]], [
            /(i-STYLE2.1)/i                                                     // i-mobile i-STYLE 2.1
            ], [[MODEL, 'i-STYLE 2.1'], [VENDOR, 'i-mobile'], [TYPE, MOBILE]], [

            /(mobiistar touch LAI 512)/i                                        // mobiistar touch LAI 512
            ], [[MODEL, 'Touch LAI 512'], [VENDOR, 'mobiistar'], [TYPE, MOBILE]], [

            /////////////
            // END TODO
            ///////////*/

        ],

        engine : [[

            /windows.+\sedge\/([\w\.]+)/i                                       // EdgeHTML
            ], [VERSION, [NAME, 'EdgeHTML']], [

            /(presto)\/([\w\.]+)/i,                                             // Presto
            /(webkit|trident|netfront|netsurf|amaya|lynx|w3m)\/([\w\.]+)/i,     // WebKit/Trident/NetFront/NetSurf/Amaya/Lynx/w3m
            /(khtml|tasman|links)[\/\s]\(?([\w\.]+)/i,                          // KHTML/Tasman/Links
            /(icab)[\/\s]([23]\.[\d\.]+)/i                                      // iCab
            ], [NAME, VERSION], [

            /rv\:([\w\.]{1,9}).+(gecko)/i                                       // Gecko
            ], [VERSION, NAME]
        ],

        os : [[

            // Windows based
            /microsoft\s(windows)\s(vista|xp)/i                                 // Windows (iTunes)
            ], [NAME, VERSION], [
            /(windows)\snt\s6\.2;\s(arm)/i,                                     // Windows RT
            /(windows\sphone(?:\sos)*)[\s\/]?([\d\.\s\w]*)/i,                   // Windows Phone
            /(windows\smobile|windows)[\s\/]?([ntce\d\.\s]+\w)/i
            ], [NAME, [VERSION, mapper.str, maps.os.windows.version]], [
            /(win(?=3|9|n)|win\s9x\s)([nt\d\.]+)/i
            ], [[NAME, 'Windows'], [VERSION, mapper.str, maps.os.windows.version]], [

            // Mobile/Embedded OS
            /\((bb)(10);/i                                                      // BlackBerry 10
            ], [[NAME, 'BlackBerry'], VERSION], [
            /(blackberry)\w*\/?([\w\.]*)/i,                                     // Blackberry
            /(tizen)[\/\s]([\w\.]+)/i,                                          // Tizen
            /(android|webos|palm\sos|qnx|bada|rim\stablet\sos|meego|contiki)[\/\s-]?([\w\.]*)/i,
                                                                                // Android/WebOS/Palm/QNX/Bada/RIM/MeeGo/Contiki
            /linux;.+(sailfish);/i                                              // Sailfish OS
            ], [NAME, VERSION], [
            /(symbian\s?os|symbos|s60(?=;))[\/\s-]?([\w\.]*)/i                  // Symbian
            ], [[NAME, 'Symbian'], VERSION], [
            /\((series40);/i                                                    // Series 40
            ], [NAME], [
            /mozilla.+\(mobile;.+gecko.+firefox/i                               // Firefox OS
            ], [[NAME, 'Firefox OS'], VERSION], [

            // Console
            /(nintendo|playstation)\s([wids34portablevu]+)/i,                   // Nintendo/Playstation

            // GNU/Linux based
            /(mint)[\/\s\(]?(\w*)/i,                                            // Mint
            /(mageia|vectorlinux)[;\s]/i,                                       // Mageia/VectorLinux
            /(joli|[kxln]?ubuntu|debian|suse|opensuse|gentoo|(?=\s)arch|slackware|fedora|mandriva|centos|pclinuxos|redhat|zenwalk|linpus)[\/\s-]?(?!chrom)([\w\.-]*)/i,
                                                                                // Joli/Ubuntu/Debian/SUSE/Gentoo/Arch/Slackware
                                                                                // Fedora/Mandriva/CentOS/PCLinuxOS/RedHat/Zenwalk/Linpus
            /(hurd|linux)\s?([\w\.]*)/i,                                        // Hurd/Linux
            /(gnu)\s?([\w\.]*)/i                                                // GNU
            ], [NAME, VERSION], [

            /(cros)\s[\w]+\s([\w\.]+\w)/i                                       // Chromium OS
            ], [[NAME, 'Chromium OS'], VERSION],[

            // Solaris
            /(sunos)\s?([\w\.\d]*)/i                                            // Solaris
            ], [[NAME, 'Solaris'], VERSION], [

            // BSD based
            /\s([frentopc-]{0,4}bsd|dragonfly)\s?([\w\.]*)/i                    // FreeBSD/NetBSD/OpenBSD/PC-BSD/DragonFly
            ], [NAME, VERSION],[

            /(haiku)\s(\w+)/i                                                   // Haiku
            ], [NAME, VERSION],[

            /cfnetwork\/.+darwin/i,
            /ip[honead]{2,4}(?:.*os\s([\w]+)\slike\smac|;\sopera)/i             // iOS
            ], [[VERSION, /_/g, '.'], [NAME, 'iOS']], [

            /(mac\sos\sx)\s?([\w\s\.]*)/i,
            /(macintosh|mac(?=_powerpc)\s)/i                                    // Mac OS
            ], [[NAME, 'Mac OS'], [VERSION, /_/g, '.']], [

            // Other
            /((?:open)?solaris)[\/\s-]?([\w\.]*)/i,                             // Solaris
            /(aix)\s((\d)(?=\.|\)|\s)[\w\.])*/i,                                // AIX
            /(plan\s9|minix|beos|os\/2|amigaos|morphos|risc\sos|openvms)/i,
                                                                                // Plan9/Minix/BeOS/OS2/AmigaOS/MorphOS/RISCOS/OpenVMS
            /(unix)\s?([\w\.]*)/i                                               // UNIX
            ], [NAME, VERSION]
        ]
    };


    /////////////////
    // Constructor
    ////////////////
    /*
    var Browser = function (name, version) {
        this[NAME] = name;
        this[VERSION] = version;
    };
    var CPU = function (arch) {
        this[ARCHITECTURE] = arch;
    };
    var Device = function (vendor, model, type) {
        this[VENDOR] = vendor;
        this[MODEL] = model;
        this[TYPE] = type;
    };
    var Engine = Browser;
    var OS = Browser;
    */
    var UAParser = function (uastring, extensions) {

        if (typeof uastring === 'object') {
            extensions = uastring;
            uastring = undefined;
        }

        if (!(this instanceof UAParser)) {
            return new UAParser(uastring, extensions).getResult();
        }

        var ua = uastring || ((window && window.navigator && window.navigator.userAgent) ? window.navigator.userAgent : EMPTY);
        var rgxmap = extensions ? util.extend(regexes, extensions) : regexes;
        //var browser = new Browser();
        //var cpu = new CPU();
        //var device = new Device();
        //var engine = new Engine();
        //var os = new OS();

        this.getBrowser = function () {
            var browser = { name: undefined, version: undefined };
            mapper.rgx.call(browser, ua, rgxmap.browser);
            browser.major = util.major(browser.version); // deprecated
            return browser;
        };
        this.getCPU = function () {
            var cpu = { architecture: undefined };
            mapper.rgx.call(cpu, ua, rgxmap.cpu);
            return cpu;
        };
        this.getDevice = function () {
            var device = { vendor: undefined, model: undefined, type: undefined };
            mapper.rgx.call(device, ua, rgxmap.device);
            return device;
        };
        this.getEngine = function () {
            var engine = { name: undefined, version: undefined };
            mapper.rgx.call(engine, ua, rgxmap.engine);
            return engine;
        };
        this.getOS = function () {
            var os = { name: undefined, version: undefined };
            mapper.rgx.call(os, ua, rgxmap.os);
            return os;
        };
        this.getResult = function () {
            return {
                ua      : this.getUA(),
                browser : this.getBrowser(),
                engine  : this.getEngine(),
                os      : this.getOS(),
                device  : this.getDevice(),
                cpu     : this.getCPU()
            };
        };
        this.getUA = function () {
            return ua;
        };
        this.setUA = function (uastring) {
            ua = uastring;
            //browser = new Browser();
            //cpu = new CPU();
            //device = new Device();
            //engine = new Engine();
            //os = new OS();
            return this;
        };
        return this;
    };

    UAParser.VERSION = LIBVERSION;
    UAParser.BROWSER = {
        NAME    : NAME,
        MAJOR   : MAJOR, // deprecated
        VERSION : VERSION
    };
    UAParser.CPU = {
        ARCHITECTURE : ARCHITECTURE
    };
    UAParser.DEVICE = {
        MODEL   : MODEL,
        VENDOR  : VENDOR,
        TYPE    : TYPE,
        CONSOLE : CONSOLE,
        MOBILE  : MOBILE,
        SMARTTV : SMARTTV,
        TABLET  : TABLET,
        WEARABLE: WEARABLE,
        EMBEDDED: EMBEDDED
    };
    UAParser.ENGINE = {
        NAME    : NAME,
        VERSION : VERSION
    };
    UAParser.OS = {
        NAME    : NAME,
        VERSION : VERSION
    };
    //UAParser.Utils = util;

    ///////////
    // Export
    //////////


    // check js environment
    if (typeof(exports) !== UNDEF_TYPE) {
        // nodejs env
        if (typeof module !== UNDEF_TYPE && module.exports) {
            exports = module.exports = UAParser;
        }
        // TODO: test!!!!!!!!
        /*
        if (require && require.main === module && process) {
            // cli
            var jsonize = function (arr) {
                var res = [];
                for (var i in arr) {
                    res.push(new UAParser(arr[i]).getResult());
                }
                process.stdout.write(JSON.stringify(res, null, 2) + '\n');
            };
            if (process.stdin.isTTY) {
                // via args
                jsonize(process.argv.slice(2));
            } else {
                // via pipe
                var str = '';
                process.stdin.on('readable', function() {
                    var read = process.stdin.read();
                    if (read !== null) {
                        str += read;
                    }
                });
                process.stdin.on('end', function () {
                    jsonize(str.replace(/\n$/, '').split('\n'));
                });
            }
        }
        */
        exports.UAParser = UAParser;
    } else {
        // requirejs env (optional)
        if ("function" === FUNC_TYPE && __webpack_require__(23)) {
            !(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
                return UAParser;
            }).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
        } else if (window) {
            // browser env
            window.UAParser = UAParser;
        }
    }

    // jQuery/Zepto specific (optional)
    // Note:
    //   In AMD env the global scope should be kept clean, but jQuery is an exception.
    //   jQuery always exports to global scope, unless jQuery.noConflict(true) is used,
    //   and we should catch that.
    var $ = window && (window.jQuery || window.Zepto);
    if (typeof $ !== UNDEF_TYPE) {
        var parser = new UAParser();
        $.ua = parser.getResult();
        $.ua.get = function () {
            return parser.getUA();
        };
        $.ua.set = function (uastring) {
            parser.setUA(uastring);
            var result = parser.getResult();
            for (var prop in result) {
                $.ua[prop] = result[prop];
            }
        };
    }

})(typeof window === 'object' ? window : this);


/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = isFunction

var toString = Object.prototype.toString

function isFunction (fn) {
  var string = toString.call(fn)
  return string === '[object Function]' ||
    (typeof fn === 'function' && string !== '[object RegExp]') ||
    (typeof window !== 'undefined' &&
     // IE8 and below
     (fn === window.setTimeout ||
      fn === window.alert ||
      fn === window.confirm ||
      fn === window.prompt))
};


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Config_1 = __webpack_require__(15);
var Storage_1 = __webpack_require__(16);
var Api_1 = __webpack_require__(17);
var Auth_1 = __webpack_require__(20);
var Classy_1 = __webpack_require__(10);
var Utils_1 = __webpack_require__(4);
var Helpers_1 = __webpack_require__(5);
var Grid_1 = __webpack_require__(21);
var Clipboard_1 = __webpack_require__(24);
var Pdf_1 = __webpack_require__(25);
var Tracking_1 = __webpack_require__(26);
var Crypto_1 = __webpack_require__(27);
var Request_1 = __webpack_require__(28);
var Page_1 = __webpack_require__(35);
var Range_1 = __webpack_require__(0);
var Range_2 = __webpack_require__(0);
var Range_3 = __webpack_require__(0);
var Range_4 = __webpack_require__(0);
var Range_5 = __webpack_require__(0);
var Range_6 = __webpack_require__(0);
Array.prototype.forEach2 = function (a) {
    var l = this.length;
    for (var i = 0; i < l; i++)
        a(this[i], i);
};
angular.module("ipushpull", [])
    .provider("ippConfig", Config_1.Config)
    .service("ippApiService", Api_1.Api)
    .service("ippPageService", Page_1.PageWrap)
    .factory("ippStorageService", Storage_1.StorageService)
    .service("ippAuthService", Auth_1.Auth)
    .factory("ippCryptoService", Crypto_1.Crypto._instance)
    .factory("ippReqService", Request_1.Request._instance)
    .service("ippClassyService", Classy_1.default)
    .service("ippUtilsService", Utils_1.default)
    .service("ippHelpersService", Helpers_1.default)
    .factory("ippClipboardService", Clipboard_1.ClipboardWrap)
    .factory("ippGridService", Grid_1.GridWrap)
    .factory("ippPdfService", Pdf_1.default)
    .service("ippTrackingService", Tracking_1.default);
var ipushpull = {
    FreezingRange: Range_1.FreezingRange,
    PermissionRange: Range_2.PermissionRange,
    ButtonRange: Range_3.ButtonRange,
    StyleRange: Range_4.StyleRange,
    NotificationRange: Range_5.NotificationRange,
    AccessRange: Range_6.AccessRange,
};
module.exports = ipushpull;
module.exports.defaults = ipushpull;
//# sourceMappingURL=index-ng.js.map

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Config = (function () {
    function Config() {
        this._config = {
            api_url: "https://www.ipushpull.com/api/1.0",
            ws_url: "https://www.ipushpull.com",
            docs_url: "https://docs.ipushpull.com",
            api_key: "",
            api_secret: "",
            transport: "",
            storage_prefix: "ipp",
            cookie: {
                oauth_access_token: "access_token",
                ouath_refresh_token: "refresh_token",
                uuid: "uuid",
            },
            client_version: "",
            uuid: "",
            hsts: true
        };
    }
    Config.prototype.set = function (config) {
        if (!config) {
            return;
        }
        for (var prop in config) {
            if (config.hasOwnProperty(prop)) {
                this._config[prop] = config[prop];
            }
        }
        if (config.api_url && !config.ws_url) {
            var parts = config.api_url.split("/");
            this._config.ws_url = parts[0] + "//" + parts[2];
        }
    };
    Object.defineProperty(Config.prototype, "api_url", {
        get: function () {
            return this._config.api_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "ws_url", {
        get: function () {
            return this._config.ws_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "docs_url", {
        get: function () {
            return this._config.docs_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "api_key", {
        get: function () {
            return this._config.api_key;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "api_secret", {
        get: function () {
            return this._config.api_secret;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "transport", {
        get: function () {
            return this._config.transport;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "storage_prefix", {
        get: function () {
            return this._config.storage_prefix;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "cookie", {
        get: function () {
            return this._config.cookie;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "uuid", {
        get: function () {
            return this._config.uuid;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "client_version", {
        get: function () {
            return this._config.client_version;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "hsts", {
        get: function () {
            return this._config.hsts;
        },
        enumerable: true,
        configurable: true
    });
    Config.prototype.$get = function () {
        return this._config;
    };
    return Config;
}());
exports.Config = Config;
//# sourceMappingURL=Config.js.map

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var LocalStorage = (function () {
    function LocalStorage() {
        this.prefix = "ipp";
    }
    LocalStorage.prototype.create = function (key, value) {
        localStorage.setItem(this.makeKey(key), value);
    };
    LocalStorage.prototype.save = function (key, value) {
        return this.create(key, value);
    };
    LocalStorage.prototype.get = function (key, defaultValue) {
        if (defaultValue === void 0) { defaultValue = null; }
        var val = localStorage.getItem(this.makeKey(key));
        if (!val) {
            return defaultValue;
        }
        if (this.isValidJSON(val)) {
            return JSON.parse(val);
        }
        else {
            return val;
        }
    };
    LocalStorage.prototype.remove = function (key) {
        localStorage.removeItem(this.makeKey(key));
    };
    LocalStorage.prototype.makeKey = function (key) {
        if (this.prefix && key.indexOf(this.prefix) !== 0) {
            key = this.prefix + "_" + key;
        }
        if (this.suffix) {
            key = key + "_" + this.suffix;
        }
        return key;
    };
    LocalStorage.prototype.isValidJSON = function (val) {
        try {
            var json = JSON.parse(val);
            return true;
        }
        catch (e) {
            return false;
        }
    };
    return LocalStorage;
}());
var CookieStorage = (function () {
    function CookieStorage() {
        this.prefix = "ipp";
        var domain = document.domain;
        if (document.domain.indexOf("ipushpull") > 0) {
            var domainParts = document.domain.split(".");
            domainParts.splice(0, 1);
            domain = domainParts.join(".");
        }
        this._domain = domain;
    }
    CookieStorage.prototype.create = function (key, value, expireDays, ignorePrefix) {
        var expires = "";
        if (expireDays) {
            var date = new Date();
            date.setTime(date.getTime() + (expireDays * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        var path = "path=/;";
        var newKey = ignorePrefix ? key : this.makeKey(key);
        document.cookie = newKey + "=" + value + expires + "; " + path + " domain=" + this._domain + (this.isSecure() ? ";secure;" : "");
    };
    CookieStorage.prototype.save = function (key, value, expireDays, ignorePrefix) {
        this.create(key, value, expireDays, ignorePrefix);
    };
    CookieStorage.prototype.get = function (key, defaultValue, ignorePrefix) {
        key = ignorePrefix ? key : this.makeKey(key);
        var nameEQ = key + "=";
        var ca = document.cookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === " ") {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) === 0) {
                var val = c.substring(nameEQ.length, c.length);
                if (this.isValidJSON(val)) {
                    return JSON.parse(val);
                }
                else {
                    return val;
                }
            }
        }
        return defaultValue;
    };
    CookieStorage.prototype.remove = function (key) {
        this.create(this.makeKey(key), "", -1);
    };
    CookieStorage.prototype.isSecure = function () {
        return window.location.protocol === "https:";
    };
    CookieStorage.prototype.makeKey = function (key) {
        if (this.prefix && key.indexOf(this.prefix) !== 0) {
            key = this.prefix + "_" + key;
        }
        if (this.suffix) {
            key = key + "_" + this.suffix;
        }
        return key;
    };
    CookieStorage.prototype.isValidJSON = function (val) {
        try {
            var json = JSON.parse(val);
            return true;
        }
        catch (e) {
            return false;
        }
    };
    return CookieStorage;
}());
var StorageService = (function () {
    function StorageService(ippConfig) {
        var userStorage = new LocalStorage();
        userStorage.suffix = "GUEST";
        var globalStorage = new LocalStorage();
        var persistentStorage = (typeof navigator !== "undefined" && navigator.cookieEnabled) ? new CookieStorage() : new LocalStorage();
        if (ippConfig.storage_prefix) {
            userStorage.prefix = ippConfig.storage_prefix;
            globalStorage.prefix = ippConfig.storage_prefix;
            persistentStorage.prefix = ippConfig.storage_prefix;
        }
        return {
            user: userStorage,
            global: globalStorage,
            persistent: persistentStorage,
        };
    }
    StorageService.$inject = ["ippConfig"];
    return StorageService;
}());
exports.StorageService = StorageService;
//# sourceMappingURL=Storage.js.map

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var uuidV4 = __webpack_require__(7);
var Emitter_1 = __webpack_require__(1);
var merge = __webpack_require__(2);
var Request = (function () {
    function Request(method, url) {
        this._headers = {};
        this._cache = false;
        this._overrideLock = false;
        this._json = true;
        this._reponse_type = '';
        this._method = method;
        this._url = url;
        this._headers = {
            "Content-Type": "application/json",
            "x-requested-with": "XMLHttpRequest",
            "x-ipp-device-uuid": Request.xipp.uuid,
            "x-ipp-client": Request.xipp.client,
            "x-ipp-client-version": Request.xipp.clientVersion
        };
        if (Request.xipp.hsts) {
            this._headers["Strict-Transport-Security"] = "max-age=15768000;includeSubDomains";
        }
    }
    Request.get = function (url) {
        return new Request("GET", url);
    };
    Request.post = function (url) {
        return new Request("POST", url);
    };
    Request.put = function (url) {
        return new Request("PUT", url);
    };
    Request.del = function (url) {
        return new Request("DELETE", url);
    };
    Object.defineProperty(Request.prototype, "METHOD", {
        get: function () {
            return this._method;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "URL", {
        get: function () {
            return this._url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "HEADERS", {
        get: function () {
            return this._headers;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "DATA", {
        get: function () {
            return this._data;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "PARAMS", {
        get: function () {
            return this._params;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "CACHE", {
        get: function () {
            return this._cache;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "OVERRIDE_LOCK", {
        get: function () {
            return this._overrideLock;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "JSON", {
        get: function () {
            return this._json;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "RESPONSE_TYPE", {
        get: function () {
            return this._reponse_type;
        },
        enumerable: true,
        configurable: true
    });
    Request.prototype.method = function (method) {
        this._method = method;
        return this;
    };
    Request.prototype.url = function (url) {
        this._url = url;
        return this;
    };
    Request.prototype.headers = function (headers, overwrite) {
        if (overwrite === void 0) { overwrite = false; }
        this._headers = overwrite ? headers : merge.recursive(true, this._headers, headers);
        return this;
    };
    Request.prototype.data = function (data, serialize) {
        if (serialize === void 0) { serialize = false; }
        if (serialize) {
            this._data = Object.keys(data)
                .map(function (k) {
                return encodeURIComponent(k) + "=" + encodeURIComponent(data[k]);
            })
                .join("&");
        }
        else {
            this._data = data;
        }
        return this;
    };
    Request.prototype.params = function (params, overwrite) {
        if (overwrite === void 0) { overwrite = false; }
        this._params = overwrite ? params : merge.recursive(true, this._params, params);
        return this;
    };
    Request.prototype.cache = function (cache) {
        if (cache && this._method === "GET") {
            this._cache = cache;
        }
        return this;
    };
    Request.prototype.overrideLock = function (override) {
        if (override === void 0) { override = true; }
        this._overrideLock = override;
        return this;
    };
    Request.prototype.json = function (json) {
        if (json === void 0) { json = true; }
        this._json = json;
        return this;
    };
    Request.prototype.responseType = function (str) {
        if (str === void 0) { str = ""; }
        this._reponse_type = str;
        return this;
    };
    Request.xipp = {
        uuid: '',
        client: '',
        clientVersion: '',
        hsts: true
    };
    return Request;
}());
var Api = (function (_super) {
    __extends(Api, _super);
    function Api(qs, q, storage, config, utils, req) {
        var _this = _super.call(this) || this;
        _this.qs = qs;
        _this.q = q;
        _this.storage = storage;
        _this.config = config;
        _this.utils = utils;
        _this.req = req;
        _this.tokens = {
            access_token: "",
            refresh_token: ""
        };
        _this._locked = false;
        _this.dummyRequest = function (data) {
            console.log("Api is locked down, preventing call " + data.url);
            var q = _this.q.defer();
            q.reject({
                data: {},
                status: 666,
                statusText: "Api is locked",
                config: data
            });
            return q.promise;
        };
        _this.handleSuccess = function (response) {
            return {
                success: true,
                data: response.body,
                httpCode: parseInt(response.statusCode, 10),
                httpText: response.statusMessage
            };
            var q = _this.q.defer();
            q.resolve({
                success: true,
                data: response.body,
                httpCode: parseInt(response.statusCode, 10),
                httpText: response.statusMessage
            });
            return q.promise;
        };
        _this.request = req;
        _this._endPoint = "" + _this.config.api_url;
        var uuid = storage.persistent.get('ipp_uuid', '', true);
        if (!uuid) {
            uuid = uuidV4();
            storage.persistent.save('ipp_uuid', uuid, 365, true);
        }
        Request.xipp = {
            uuid: _this.config.uuid || uuid,
            client: _this.config.api_key,
            clientVersion: _this.config.client_version || '1.0',
            hsts: _this.config.hsts === undefined || _this.config.hsts,
        };
        return _this;
    }
    Object.defineProperty(Api.prototype, "EVENT_401", {
        get: function () {
            return "401";
        },
        enumerable: true,
        configurable: true
    });
    Api.prototype.block = function () {
        this._locked = true;
    };
    Api.prototype.unblock = function () {
        this._locked = false;
    };
    Api.prototype.getSelfInfo = function () {
        return this.send(Request.get(this._endPoint + "/users/self/")
            .cache(false)
            .overrideLock());
    };
    Api.prototype.refreshAccessTokens = function (refreshToken) {
        return this.send(Request.post(this._endPoint + "/oauth/token/")
            .data({
            grant_type: "refresh_token",
            client_id: this.config.api_key,
            client_secret: this.config.api_secret,
            refresh_token: refreshToken
        }, true)
            .headers({
            "Content-Type": "application/x-www-form-urlencoded",
        })
            .json(false)
            .overrideLock());
    };
    Api.prototype.userLogin = function (data) {
        return this.send(Request.post(this._endPoint + "/oauth/token/")
            .data({
            grant_type: "password",
            client_id: this.config.api_key,
            client_secret: this.config.api_secret,
            username: data.email,
            password: data.password
        }, true)
            .headers({
            "Content-Type": "application/x-www-form-urlencoded"
        })
            .json(false));
    };
    Api.prototype.userLoginByCode = function (data) {
        return this.send(Request.post(this._endPoint + "/oauth/token/")
            .params(merge({
            grant_type: "authorization_code",
            client_id: this.config.api_key,
            client_secret: this.config.api_secret
        }, data))
            .headers({
            "Content-Type": "application/x-www-form-urlencoded"
        }));
    };
    Api.prototype.userLogout = function (data) {
        if (data === void 0) { data = {}; }
        return this.send(Request.post(this._endPoint + "/oauth/logout/").params({ all: data.all || "" }));
    };
    Api.prototype.getDomains = function () {
        return this.send(Request.get(this._endPoint + "/domains/"));
    };
    Api.prototype.getDomain = function (domainId) {
        return this.send(Request.get(this._endPoint + "/domains/" + domainId + "/"));
    };
    Api.prototype.createFolder = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/").data(data.data));
    };
    Api.prototype.createDomain = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/").data(data.data));
    };
    Api.prototype.updateDomain = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/").data(data.data));
    };
    Api.prototype.disableDomain = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/").data(data.data));
    };
    Api.prototype.getDomainPages = function (domainId) {
        return this.send(Request.get(this._endPoint + "/domains/" + domainId + "/page_access/"));
    };
    Api.prototype.getDomainsAndPages = function (client) {
        if (!client) {
            client = "";
        }
        return this.send(Request.get(this._endPoint + "/domain_page_access/").params({ client: client }));
    };
    Api.prototype.getPage = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/id/" + data.domainId + "/page_content/id/" + data.pageId + "/").params({ client_seq_no: data.seq_no }));
    };
    Api.prototype.getPageByName = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/name/" + data.domainId + "/page_content/name/" + data.pageId + "/").params({ client_seq_no: data.seq_no }));
    };
    Api.prototype.getPageByUuid = function (data) {
        return this.send(Request.get(this._endPoint + "/internal/page_content/" + data.uuid + "/").params({
            client_seq_no: data.seq_no
        }));
    };
    Api.prototype.getPageVerions = function (data) {
        return this.send(Request.get(this._endPoint + "/page/id/" + data.pageId + "/versions/").params({
            before: data.before,
            after: data.after,
            max: data.max,
            page: data.page
        }));
    };
    Api.prototype.getPageVerion = function (data) {
        return this.send(Request.get(this._endPoint + "/page/id/" + data.pageId + "/version/" + data.seqNo + "/"));
    };
    Api.prototype.restorePageVerion = function (data) {
        return this.send(Request.put(this._endPoint + "/page/id/" + data.pageId + "/version/" + data.seqNo + "/restore/"));
    };
    Api.prototype.getPageAccess = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/id/" + data.domainId + "/page_access/id/" + data.pageId + "/"));
    };
    Api.prototype.createPage = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/pages/").data(data.data));
    };
    Api.prototype.createPageNotification = function (data) {
        return this.send(Request.post(this._endPoint + "/page/" + data.pageId + "/notification/").data(data.data));
    };
    Api.prototype.createPageNotificationByUuid = function (data) {
        return this.send(Request.post(this._endPoint + "/page/" + data.uuid + "/notification/").data(data.data));
    };
    Api.prototype.createAnonymousPage = function (data) {
        return this.send(Request.post(this._endPoint + "/anonymous/page/").data(data.data));
    };
    Api.prototype.savePageContent = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/id/" + data.domainId + "/page_content/id/" + data.pageId + "/").data(data.data));
    };
    Api.prototype.savePageContentDelta = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/id/" + data.domainId + "/page_content_delta/id/" + data.pageId + "/").data(data.data));
    };
    Api.prototype.savePageSettings = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/pages/" + data.pageId + "/").data(data.data));
    };
    Api.prototype.deletePage = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/pages/" + data.pageId + "/"));
    };
    Api.prototype.saveUserInfo = function (data) {
        return this.send(Request.put(this._endPoint + "/users/self/").data(data));
    };
    Api.prototype.getUserMetaData = function (data) {
        return this.send(Request.get(this._endPoint + "/users/" + data.userId + "/meta/").data(data.data));
    };
    Api.prototype.saveUserMetaData = function (data) {
        return this.send(Request.put(this._endPoint + "/users/" + data.userId + "/meta/").data(data.data));
    };
    Api.prototype.deleteUserMetaData = function (data) {
        return this.send(Request.del(this._endPoint + "/users/" + data.userId + "/meta/").data(data.data));
    };
    Api.prototype.changePassword = function (data) {
        return this.send(Request.put(this._endPoint + "/credentials/self/").data(data));
    };
    Api.prototype.changeEmail = function (data) {
        return this.send(Request.put(this._endPoint + "/credentials/self/").data(data));
    };
    Api.prototype.forgotPassword = function (data) {
        return this.send(Request.post(this._endPoint + "/password_reset/").data(data));
    };
    Api.prototype.resetPassword = function (data) {
        return this.send(Request.post(this._endPoint + "/password_reset/confirm/").data(data));
    };
    Api.prototype.inviteUsers = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/invitations/").data(data.data));
    };
    Api.prototype.acceptInvitation = function (data) {
        return this.send(Request.post(this._endPoint + "/users/invitation/confirm/").data(data));
    };
    Api.prototype.refuseInvitation = function (data) {
        return this.send(Request.del(this._endPoint + "/users/invitation/confirm/").data(data));
    };
    Api.prototype.domainInvitations = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/invitations/").params({ is_complete: "False" }));
    };
    Api.prototype.userInvitations = function () {
        return this.send(Request.get(this._endPoint + "/users/self/invitations/").params({ is_complete: "False" }));
    };
    Api.prototype.domainAccessLog = function (data) {
        return this.send(Request.get(this._endPoint + "/domain_access/" + data.domainId + "/events/").params({
            page_size: data.limit
        }));
    };
    Api.prototype.domainUsers = function (data) {
        return this.send(Request.get(this._endPoint + "/domain_access/" + data.domainId + "/users/"));
    };
    Api.prototype.signupUser = function (data) {
        return this.send(Request.post(this._endPoint + "/users/signup/").data(data));
    };
    Api.prototype.activateUser = function (data) {
        return this.send(Request.post(this._endPoint + "/users/signup/confirm/").data(data));
    };
    Api.prototype.setDomainDefault = function (data) {
        return this.send(Request.put(this._endPoint + "/domain_access/" + data.domainId + "/users/self/").data(data.data));
    };
    Api.prototype.resendInvite = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/invitations/" + data.inviteId + "/resend/"));
    };
    Api.prototype.updateDomainAccess = function (data) {
        return this.send(Request.put(this._endPoint + "/domain_access/" + data.domainId + "/users/").data(data.data));
    };
    Api.prototype.removeUsersFromDomain = function (data) {
        return this.send(Request.del(this._endPoint + "/domain_access/" + data.domainId + "/users/").data(data.data));
    };
    Api.prototype.getInvitation = function (data) {
        return this.send(Request.get(this._endPoint + "/users/invitations/" + data.token + "/"));
    };
    Api.prototype.cancelInvitations = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/invitations/").data(data.data));
    };
    Api.prototype.getDomainAccessGroups = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/access_groups/"));
    };
    Api.prototype.getDomainAccessGroup = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.groupId + "/"));
    };
    Api.prototype.addDomainAccessGroup = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/access_groups/").data(data.data));
    };
    Api.prototype.putDomainAgroupMembers = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/members/").data(data.data));
    };
    Api.prototype.putDomainAgroupPages = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/pages/").data(data.data));
    };
    Api.prototype.updateDomainAgroup = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/").data(data.data));
    };
    Api.prototype.deleteDomainAGroup = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/"));
    };
    Api.prototype.getDomainPageAccess = function (data) {
        return this.send(Request.get(this._endPoint + "/domain_page_access/" + data.domainId + "/"));
    };
    Api.prototype.getDomainCustomers = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/customers/"));
    };
    Api.prototype.getDomainUsage = function (data) {
        if (data === void 0) { data = {}; }
        return this.send(Request.get(this._endPoint + "/domains/id/" + data.domainId + "/usage/").params({
            from_date: data.fromDate,
            to_date: data.toDate
        }));
    };
    Api.prototype.saveDomainPageAccess = function (data) {
        return this.send(Request.put(this._endPoint + "/domain_page_access/" + data.domainId + "/basic/").data(data.data));
    };
    Api.prototype.getTemplates = function (data) {
        return this.send(Request.get(this._endPoint + "/templates/"));
    };
    Api.prototype.saveCustomer = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/customers/").data(data.data));
    };
    Api.prototype.updateCustomer = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/customers/" + data.data.id + "/").data(data.data));
    };
    Api.prototype.removeCustomer = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/customers/" + data.customerId + "/"));
    };
    Api.prototype.getDocEmailRules = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/docsnames/"));
    };
    Api.prototype.createDocEmailRule = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/docsnames/").data(data.data));
    };
    Api.prototype.updateDocEmailRule = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/docsnames/" + data.docRuleId + "/").data(data.data));
    };
    Api.prototype.deleteDocEmailRule = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/docsnames/" + data.docRuleId + "/"));
    };
    Api.prototype.send = function (request) {
        var _this = this;
        var q = this.q.defer();
        var token = "";
        if (this.storage) {
            token = this.storage.persistent.get("access_token");
        }
        if (!token && this.tokens && this.tokens.access_token) {
            token = this.tokens.access_token;
        }
        if (token) {
            request.headers({
                Authorization: "Bearer " + token
            });
        }
        var provider = this._locked && !request.OVERRIDE_LOCK ? this.dummyRequest : this.request;
        request.cache(false);
        var r = provider({
            url: request.URL + "?" + this.qs(request.PARAMS),
            cache: request.CACHE,
            method: request.METHOD,
            data: request.DATA,
            headers: request.HEADERS,
            resolveWithFullResponse: true,
            json: request.JSON,
            responseType: request.RESPONSE_TYPE
        }, function (err, resp, body) {
            if (err || resp.statusCode >= 300) {
                if (err) {
                    q.reject(err);
                }
                else {
                    if (!request.JSON) {
                        try {
                            body = JSON.parse(body);
                        }
                        catch (e) {
                            body = {};
                        }
                    }
                    var error = {
                        method: resp.method,
                        data: body,
                        code: resp.statusCode,
                        statusCode: resp.statusCode,
                        httpCode: resp.statusCode,
                        error: _this.utils.parseApiError({ data: body }) || "Error",
                        httpText: _this.utils.parseApiError({ data: body }) || "Error",
                        message: _this.utils.parseApiError({ data: body }) || "Error"
                    };
                    if (error.code === 401 && !_this._locked && error.message !== "invalid_grant") {
                        _this.emit(_this.EVENT_401);
                    }
                    q.reject(error);
                }
            }
            else {
                if (!request.JSON) {
                    try {
                        resp.body = JSON.parse(resp.body);
                    }
                    catch (e) {
                        resp.body = {};
                    }
                }
                q.resolve(_this.handleSuccess(resp));
            }
        });
        return q.promise;
    };
    Api.prototype.getApplicationPageList = function (client) {
        if (client === void 0) { client = ""; }
        return this.send(Request.get(this._endPoint + "/application_page_list/").params({
            client: client
        }));
    };
    Api.prototype.getOrganization = function (data) {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/"));
    };
    Api.prototype.getOrganizationUsers = function (data) {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/users/").params({
            query: data.query
        }));
    };
    Api.prototype.getOrganizationUsage = function (data) {
        if (data === void 0) { data = {}; }
        return this.send(Request.get(this._endPoint + "/organizations/self/usage/").params({
            from_date: data.fromDate,
            to_date: data.toDate
        }));
    };
    Api.prototype.getOrganizationUser = function (data) {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/users/" + data.userId + "/"));
    };
    Api.prototype.createOrganizationUser = function (data) {
        return this.send(Request.post(this._endPoint + "/organizations/" + data.organizationId + "/users/").data(data.data));
    };
    Api.prototype.saveOrganizationUser = function (data) {
        return this.send(Request.put(this._endPoint + "/organizations/" + data.organizationId + "/users/" + data.userId + "/").data(data.data));
    };
    Api.prototype.getOrganizationDomains = function (data) {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/domains/"));
    };
    Api.prototype.getOrganizationDomain = function (data) {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/domains/" + data.domainId + "/"));
    };
    Api.prototype.createOrganizationDomain = function (data) {
        return this.send(Request.post(this._endPoint + "/organizations/" + data.organizationId + "/domains/").data(data.data));
    };
    Api.prototype.saveOrganizationDomain = function (data) {
        return this.send(Request.put(this._endPoint + "/organizations/" + data.organizationId + "/domains/" + data.domainId + "/").data(data.data));
    };
    Api.prototype.getSsoStatus = function (email) {
        return this.send(Request.get(this._endPoint + "/sso/status/").params({
            user: email,
        }));
    };
    Api.prototype.downloadPage = function (data) {
        return this.send(Request.get(this._endPoint + "/page/" + data.pageId + "/download/")
            .params({
            live: data.live === undefined ? "true" : "false",
            snapshot: data.snapshot === undefined ? "true" : "false"
        })
            .headers({})
            .responseType("arraybuffer"));
    };
    Api.$inject = [
        "$httpParamSerializerJQLike",
        "$q",
        "ippStorageService",
        "ippConfig",
        "ippUtilsService",
        "ippReqService"
    ];
    return Api;
}(Emitter_1.default));
exports.Api = Api;
//# sourceMappingURL=Api.js.map

/***/ }),
/* 18 */
/***/ (function(module, exports) {

// Unique ID creation requires a high quality random # generator.  In the
// browser this is a little complicated due to unknown quality of Math.random()
// and inconsistent support for the `crypto` API.  We do the best we can via
// feature-detection

// getRandomValues needs to be invoked in a context where "this" is a Crypto
// implementation. Also, find the complete implementation of crypto on IE11.
var getRandomValues = (typeof(crypto) != 'undefined' && crypto.getRandomValues && crypto.getRandomValues.bind(crypto)) ||
                      (typeof(msCrypto) != 'undefined' && typeof window.msCrypto.getRandomValues == 'function' && msCrypto.getRandomValues.bind(msCrypto));

if (getRandomValues) {
  // WHATWG crypto RNG - http://wiki.whatwg.org/wiki/Crypto
  var rnds8 = new Uint8Array(16); // eslint-disable-line no-undef

  module.exports = function whatwgRNG() {
    getRandomValues(rnds8);
    return rnds8;
  };
} else {
  // Math.random()-based (RNG)
  //
  // If all else fails, use Math.random().  It's fast, but is of unspecified
  // quality.
  var rnds = new Array(16);

  module.exports = function mathRNG() {
    for (var i = 0, r; i < 16; i++) {
      if ((i & 0x03) === 0) r = Math.random() * 0x100000000;
      rnds[i] = r >>> ((i & 0x03) << 3) & 0xff;
    }

    return rnds;
  };
}


/***/ }),
/* 19 */
/***/ (function(module, exports) {

/**
 * Convert array of 16 byte values to UUID string format of the form:
 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */
var byteToHex = [];
for (var i = 0; i < 256; ++i) {
  byteToHex[i] = (i + 0x100).toString(16).substr(1);
}

function bytesToUuid(buf, offset) {
  var i = offset || 0;
  var bth = byteToHex;
  // join used to fix memory issue caused by concatenation: https://bugs.chromium.org/p/v8/issues/detail?id=3175#c4
  return ([bth[buf[i++]], bth[buf[i++]], 
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]], '-',
	bth[buf[i++]], bth[buf[i++]],
	bth[buf[i++]], bth[buf[i++]],
	bth[buf[i++]], bth[buf[i++]]]).join('');
}

module.exports = bytesToUuid;


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = __webpack_require__(1);
var _ = __webpack_require__(3);
var Auth = (function (_super) {
    __extends(Auth, _super);
    function Auth($q, $timeout, ippApi, storage, config, utils) {
        var _this = _super.call(this) || this;
        _this.$q = $q;
        _this.$timeout = $timeout;
        _this.ippApi = ippApi;
        _this.storage = storage;
        _this.config = config;
        _this.utils = utils;
        _this.polling = false;
        _this._user = {};
        _this._authenticated = false;
        _this._authInProgress = false;
        _this._selfTimeout = 15 * 1000;
        _this.on401 = function () {
            if (_this.polling) {
                return;
            }
            var renew = _this.storage.persistent.get("renew");
            if (renew) {
                return;
            }
            _this.ippApi.block();
            _this.ippApi.tokens.access_token = "";
            _this.storage.persistent.remove("access_token");
            _this.storage.persistent.create("renew", 1);
            _this.emit(_this.EVENT_RE_LOGGING);
            _this.authenticate(true)
                .then(function () {
                _this.storage.user.suffix = _this._user.id;
                _this.emit(_this.EVENT_LOGIN_REFRESHED);
            }, function () {
                _this.emit(_this.EVENT_ERROR);
            })
                .finally(function () {
                _this.storage.persistent.remove("renew");
                _this.ippApi.unblock();
            });
        };
        _this._selfTimeout = _.random(15, 45) * 1000;
        ippApi.on(ippApi.EVENT_401, _this.on401);
        return _this;
    }
    Object.defineProperty(Auth.prototype, "EVENT_LOGGED_IN", {
        get: function () {
            return "logged_in";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_RE_LOGGING", {
        get: function () {
            return "re_logging";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_LOGIN_REFRESHED", {
        get: function () {
            return "login_refreshed";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_LOGGED_OUT", {
        get: function () {
            return "logged_out";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_ERROR", {
        get: function () {
            return "error";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_401", {
        get: function () {
            return "401";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_USER_UPDATED", {
        get: function () {
            return "user_updated";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "user", {
        get: function () {
            return this._user;
        },
        enumerable: true,
        configurable: true
    });
    Auth.prototype.authenticate = function (force, tokens) {
        var _this = this;
        if (force === void 0) { force = false; }
        if (tokens === void 0) { tokens = {}; }
        if (tokens && tokens.access_token) {
            this.ippApi.tokens = tokens;
        }
        var q = this.$q.defer();
        if (this._authInProgress) {
            q.reject("Auth already in progress");
            return q.promise;
        }
        this._authInProgress = true;
        if (this._authenticated && !force) {
            this._authInProgress = false;
            q.resolve();
            return q.promise;
        }
        this.processAuth(force)
            .then(function (res) {
            if (!_this._authenticated) {
                _this._authenticated = true;
                _this.storage.user.suffix = _this._user.id;
                _this.emit(_this.EVENT_LOGGED_IN);
                _this.startPollingSelf();
            }
            q.resolve();
        }, function (err) {
            _this.emit(_this.EVENT_ERROR, err);
            if (_this._authenticated) {
                _this.logout(false, true);
            }
            q.reject(err);
        })
            .finally(function () {
            _this._authInProgress = false;
        });
        return q.promise;
    };
    Auth.prototype.login = function (username, password) {
        var _this = this;
        var q = this.$q.defer();
        this.ippApi
            .userLogin({
            grant_type: "password",
            client_id: this.config.api_key,
            client_secret: this.config.api_secret,
            email: username,
            password: password
        })
            .then(function (res) {
            _this.saveTokens(res.data);
            _this.authenticate().then(q.resolve, q.reject);
        }, function (err) {
            if (err.httpCode === 400 || err.httpCode === 401) {
                switch (err.data) {
                    case "invalid_grant":
                        err.message =
                            "The username and password you entered did not match our records. Please double-check and try again.";
                        break;
                    case "invalid_client":
                        err.message =
                            "Your client doesn't have access to iPushPull system.";
                        break;
                    case "invalid_request":
                        break;
                    default:
                        break;
                }
            }
            _this.emit(_this.EVENT_ERROR, err);
            q.reject(err);
        });
        return q.promise;
    };
    Auth.prototype.logout = function (all, ignore) {
        if (all === void 0) { all = false; }
        if (ignore === void 0) { ignore = false; }
        if (!ignore) {
            this.ippApi.userLogout({ all: all });
        }
        this.ippApi.tokens = {
            access_token: "",
            refresh_token: ""
        };
        this.storage.persistent.remove("access_token");
        this.storage.persistent.remove("refresh_token");
        this.storage.persistent.remove("renew");
        this._authenticated = false;
        this._user = {};
        this.$timeout.cancel(this._selfTimer);
        this.storage.user.suffix = "GUEST";
        this.emit(this.EVENT_LOGGED_OUT);
    };
    Auth.prototype.processAuth = function (force) {
        var _this = this;
        if (force === void 0) { force = false; }
        var q = this.$q.defer();
        var accessToken = this.storage.persistent.get("access_token") || this.ippApi.tokens.access_token;
        var refreshToken = this.storage.persistent.get("refresh_token") || this.ippApi.tokens.refresh_token;
        if (accessToken && !force) {
            return this.getUserInfo();
        }
        else {
            if (refreshToken) {
                this.refreshTokens().then(function (data) {
                    _this.saveTokens(data.data);
                    _this.getUserInfo().then(q.resolve, q.reject);
                }, function (err) {
                    _this.storage.persistent.remove("refresh_token");
                    q.reject(err);
                });
            }
            else {
                q.reject("No tokens available");
            }
        }
        return q.promise;
    };
    Auth.prototype.refreshTokens = function () {
        var refreshToken = this.storage.persistent.get("refresh_token") || this.ippApi.tokens.refresh_token;
        return this.ippApi.refreshAccessTokens(refreshToken);
    };
    Auth.prototype.saveTokens = function (tokens) {
        this.storage.persistent.create("access_token", tokens.access_token, tokens.expires_in / 86400);
        this.storage.persistent.create("refresh_token", tokens.refresh_token, 365);
        this.ippApi.tokens = tokens;
        this.storage.persistent.remove("renew");
    };
    Auth.prototype.getUserInfo = function () {
        var _this = this;
        var q = this.$q.defer();
        this.ippApi.getSelfInfo().then(function (res) {
            if (!_.isEqual(_this._user, res.data)) {
                _this._user = res.data;
                _this.emit(_this.EVENT_USER_UPDATED);
            }
            q.resolve();
        }, q.reject);
        return q.promise;
    };
    Auth.prototype.startPollingSelf = function () {
        var _this = this;
        this.$timeout.cancel(this._selfTimer);
        this._selfTimer = this.$timeout(function () {
            _this.getUserInfo()
                .then(function () { }, function () { })
                .finally(function () {
                _this.startPollingSelf();
            });
        }, this._selfTimeout);
    };
    Auth.$inject = [
        "$q",
        "$timeout",
        "ippApiService",
        "ippStorageService",
        "ippConfig",
        "ippUtilsService"
    ];
    return Auth;
}(Emitter_1.default));
exports.Auth = Auth;
//# sourceMappingURL=Auth.js.map

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = __webpack_require__(1);
var Classy_1 = __webpack_require__(10);
var Helpers_1 = __webpack_require__(5);
var Range_1 = __webpack_require__(0);
var Range_2 = __webpack_require__(0);
var Range_3 = __webpack_require__(0);
var _ = __webpack_require__(3);
var Hammer = __webpack_require__(11);
var ua = __webpack_require__(12);
var merge = __webpack_require__(2);
var Grid = (function (_super) {
    __extends(Grid, _super);
    function Grid(container, fluid, options) {
        if (fluid === void 0) { fluid = false; }
        if (options === void 0) { options = {}; }
        var _this = _super.call(this) || this;
        _this.scale = 1;
        _this.ratio = 1;
        _this.ratioScale = 1;
        _this.pause = false;
        _this.canEdit = false;
        _this.scrollbars = {
            width: 8,
            inset: false,
            scrolling: false,
            x: {
                dimension: "width",
                label: "col",
                show: false,
                anchor: "left",
                track: undefined,
                handle: undefined
            },
            y: {
                dimension: "height",
                label: "row",
                show: false,
                anchor: "top",
                track: undefined,
                handle: undefined
            }
        };
        _this.alwaysEditing = false;
        _this.disallowSelection = false;
        _this.historyIndicatorSize = 16;
        _this.trackingHighlightClassname = "yellow";
        _this.trackingHighlightMatchColor = true;
        _this.trackingHighlightLifetime = 3000;
        _this.hasFocus = false;
        _this.fluid = false;
        _this.noAccessImages = {
            light: "",
            dark: ""
        };
        _this.borders = {
            widths: {
                none: 0,
                thin: 1,
                medium: 2,
                thick: 3
            },
            styles: {
                none: "solid",
                solid: "solid",
                double: "double"
            },
            names: {
                t: "top",
                r: "right",
                b: "bottom",
                l: "left"
            }
        };
        _this.originalContent = [];
        _this.content = [];
        _this.history = [];
        _this.trackingHighlights = [];
        _this.offsets = {
            x: {
                offset: 0,
                index: 0,
                max: 0,
                maxOffset: 0
            },
            y: {
                offset: 0,
                index: 0,
                max: 0,
                maxOffset: 0
            }
        };
        _this.width = 0;
        _this.height = 0;
        _this.visible = [];
        _this.gridLines = [];
        _this.found = [];
        _this.foundSelection = {
            count: 0,
            selected: 0,
            cell: undefined
        };
        _this.touch = false;
        _this.allowBrowserScroll = false;
        _this.links = [];
        _this.styleRanges = [];
        _this.buttonRanges = [];
        _this.accessRanges = [];
        _this.freezeRange = {
            valid: false,
            index: {
                row: 0,
                col: 0
            }
        };
        _this.containerRect = {
            x: 0,
            y: 0,
            height: 0,
            width: 0
        };
        _this._tracking = false;
        _this._editing = false;
        _this._fit = "scroll";
        _this.userTrackingColors = ["deeppink", "aqua", "tomato", "orange", "yellow", "limegreen"];
        _this.cellSelection = {
            from: undefined,
            to: undefined,
            last: undefined,
            go: false,
            on: false,
            clicked: 0,
            found: undefined
        };
        _this.filters = {};
        _this.sorting = {};
        _this.dirty = false;
        _this.rightClick = false;
        _this.keyValue = "";
        _this.ctrlDown = false;
        _this.shiftDown = false;
        _this.cellStyles = [
            "background-color",
            "color",
            "font-family",
            "font-size",
            "font-style",
            "font-weight",
            "height",
            "number-format",
            "text-align",
            "text-wrap",
            "width",
            "vertical-align"
        ];
        _this.init = false;
        _this.hidden = false;
        _this.drawGrid = function () {
            var stageWidth = _this.width * 1 / _this.scale * _this.ratio;
            var stageHeight = _this.height * 1 / _this.scale * _this.ratio;
            _this.gc.bufferContext.save();
            _this.gc.bufferContext.clearRect(0, 0, _this.width * _this.ratio, _this.height * _this.ratio);
            _this.gc.bufferContext.scale(_this.scale, _this.scale);
            _this.gridLines = [];
            _this.visible = [];
            var frozenCells = {
                cols: [],
                rows: [],
                common: []
            };
            var trackedCells = [];
            for (var rowIndex = 0; rowIndex < _this.content.length; rowIndex++) {
                var row = _this.content[rowIndex];
                var y = row[0].y - _this.offsets.y.offset;
                var b = row[0].y - _this.offsets.y.offset + row[0].height;
                var visibleRow = (y >= 0 && y <= stageHeight) || (b >= 0 && b <= stageHeight);
                if (visibleRow || (_this.freezeRange.valid && rowIndex < _this.freezeRange.index.row)) {
                    _this.visible[rowIndex] = [];
                }
                else {
                    continue;
                }
                if (!row[0].overlayWidth) {
                    _this.softMerges(row, rowIndex);
                }
                for (var colIndex = 0; colIndex < row.length; colIndex++) {
                    var col = row[colIndex];
                    var width = col.overlayWidth;
                    var height = col.height;
                    var xPos = col.overlayStart < colIndex ? _this.content[rowIndex][col.overlayStart].x : col.x;
                    var x = xPos - _this.offsets.x.offset;
                    var _x = col.x - _this.offsets.x.offset;
                    var y_1 = col.y - _this.offsets.y.offset;
                    var a = col.x - _this.offsets.x.offset + width;
                    var _a = col.x - _this.offsets.x.offset + col.width;
                    var b_1 = col.y - _this.offsets.y.offset + height;
                    var visibleCol = (x >= 0 && x <= stageWidth) ||
                        (a >= 0 && a <= stageWidth) ||
                        (col.overlayEnd && colIndex <= col.overlayEnd) ||
                        (col.overlayStart && colIndex >= col.overlayStart);
                    if (!col.width) {
                        continue;
                    }
                    if ((visibleRow && visibleCol) ||
                        (_this.freezeRange.valid && colIndex < _this.freezeRange.index.col) ||
                        (_this.freezeRange.valid && rowIndex < _this.freezeRange.index.row)) {
                        var originalCell = _this.originalContent[col.position.row][col.position.col];
                        _this.visible[rowIndex][colIndex] = {
                            originalCell: originalCell,
                            coords: { x: x, y: y_1, a: a, b: b_1, width: width, height: height, _x: _x, _a: _a },
                            cell: col
                        };
                        if (_this.freezeRange.valid) {
                            var freeze = "";
                            if (rowIndex < _this.freezeRange.index.row && colIndex < _this.freezeRange.index.col) {
                                x = _x = col.x;
                                a = _a = col.x + width;
                                y_1 = col.y;
                                b_1 = col.y + height;
                                freeze = "common";
                            }
                            else if (rowIndex < _this.freezeRange.index.row) {
                                y_1 = col.y;
                                b_1 = col.y + height;
                                freeze = visibleCol ? "rows" : "";
                            }
                            else if (colIndex < _this.freezeRange.index.col) {
                                x = _x = col.x;
                                a = _a = col.x + width;
                                freeze = visibleRow ? "cols" : "";
                            }
                            if (freeze) {
                                frozenCells[freeze].push({
                                    originalCell: originalCell,
                                    coords: { x: x, y: y_1, a: a, b: b_1, width: width, height: height, _x: _x, _a: _a },
                                    col: col
                                });
                                continue;
                            }
                        }
                        if (_this.tracking &&
                            _this.history[col.position.row] &&
                            _this.history[col.position.row][col.position.col] >= 0 &&
                            !col.button) {
                            trackedCells.push({
                                originalCell: originalCell,
                                coords: { x: x, y: y_1, a: a, b: b_1, width: width, height: height },
                                col: col
                            });
                        }
                    }
                }
            }
            for (var rowIndex = 0; rowIndex < _this.visible.length; rowIndex++) {
                if (!_this.visible[rowIndex])
                    continue;
                for (var colIndex = 0; colIndex < _this.visible[rowIndex].length; colIndex++) {
                    if (!_this.visible[rowIndex][colIndex])
                        continue;
                    var col = _this.visible[rowIndex][colIndex];
                    if (col.cell.allow !== "no" || !_this.imgPattern) {
                        _this.drawOffsetRect(col.originalCell, col.coords, col.cell);
                    }
                }
            }
            for (var rowIndex = 0; rowIndex < _this.visible.length; rowIndex++) {
                if (!_this.visible[rowIndex])
                    continue;
                for (var colIndex = 0; colIndex < _this.visible[rowIndex].length; colIndex++) {
                    if (!_this.visible[rowIndex][colIndex])
                        continue;
                    var col = _this.visible[rowIndex][colIndex];
                    _this.drawGraphics(col.originalCell, col.coords, col.cell);
                }
            }
            _this.gridLines.forEach2(function (line) {
                _this.drawLine(line[0], line[1], line[2]);
            });
            trackedCells.forEach2(function (cell) {
                _this.drawTrackingRect(cell.coords, cell.col);
            });
            for (var freeze in frozenCells) {
                _this.gridLines = [];
                frozenCells[freeze].forEach2(function (cell) {
                    _this.drawOffsetRect(cell.originalCell, cell.coords, cell.col);
                });
                frozenCells[freeze].forEach2(function (cell) {
                    _this.drawGraphics(cell.originalCell, cell.coords, cell.col);
                });
                _this.gridLines.forEach2(function (line) {
                    _this.drawLine(line[0], line[1], line[2]);
                });
            }
            for (var i in _this.sorting) {
                var col = _this.sorting[i];
                var cell = _this.content[0][col.col];
                _this.drawSortingIndicator(cell, col.direction);
            }
            _this.gc.bufferContext.restore();
            _this.gc.canvasContext.clearRect(0, 0, _this.width * _this.ratio, _this.height * _this.ratio);
            _this.gc.canvasContext.drawImage(_this.gc.buffer, 0, 0);
            _this.updateSelector(true);
        };
        _this.onWheelEvent = function (evt) {
            if (_this.animation) {
                cancelAnimationFrame(_this.animation);
            }
            var offsetX = evt.deltaX ? (Math.abs(evt.deltaX) >= 100 ? Math.round(Math.abs(evt.deltaX) / 100) : 1) : 0;
            var offsetY = evt.deltaY ? (Math.abs(evt.deltaY) >= 100 ? Math.round(Math.abs(evt.deltaY) / 100) : 1) : 0;
            var directionX = evt.deltaX > 0 ? 1 : -1;
            var directionY = evt.deltaY > 0 ? 1 : -1;
            offsetX *= directionX;
            offsetY *= directionY;
            if (_this.stage.y.size * _this.scale / _this.ratio <= _this.height &&
                _this.stage.x.size * _this.scale / _this.ratio > _this.width) {
                offsetX = offsetY;
            }
            var curretOffset = JSON.parse(JSON.stringify(_this.offsets));
            _this.setOffsets({
                x: offsetX,
                y: offsetY
            });
            _this.updateScrollbars();
            _this.animation = _this.drawGrid();
            if (curretOffset.x.offset !== _this.offsets.x.offset || curretOffset.y.offset !== _this.offsets.y.offset) {
                evt.preventDefault();
            }
        };
        _this.onResizeEvent = function (evt) {
            console.log("onResizeEvent");
            _this.ratio = window.devicePixelRatio;
            _this.setSize();
            if (!_this.touch) {
                _this.setScale(_this.ratio);
            }
            _this.setFit(_this.fit, true);
            if (_this.touch && _this.editing) {
                setTimeout(function () {
                    _this.keepSelectorWithinView();
                }, 300);
            }
            else if (_this.touch && _this.cellSelection.from) {
                _this.setOffsets({
                    y: _this.cellSelection.from.position.row
                });
            }
        };
        _this.onMouseMove = function (evt) {
            ["x", "y"].forEach2(function (axis) {
                if (!_this.scrollbars[axis].drag || !_this.scrollbars.scrolling) {
                    return;
                }
                var dimension = _this.stage[axis].label;
                var canvasSize = _this[dimension];
                var size = canvasSize * (canvasSize / (_this.stage[axis].size * _this.scale));
                var distance = canvasSize - size;
                var offset = evt[axis] - _this.scrollbars[axis].start + _this.scrollbars[axis].offset;
                if (offset > distance) {
                    offset = distance;
                }
                else if (offset < 0) {
                    offset = 0;
                }
                var anchor = _this.scrollbars[axis].anchor;
                _this.scrollbars[axis].handle.style[anchor] = offset + "px";
                var ratio = offset / distance;
                var index = Math.round(_this.offsets[axis].max * ratio);
                _this.offsets[axis].index = index;
                var scrollOffset = _this.stage[axis].increments[index];
                if (scrollOffset > _this.offsets[axis].maxOffset) {
                    scrollOffset = _this.offsets[axis].maxOffset;
                }
                _this.offsets[axis].offset = scrollOffset;
            });
            if (_this.scrollbars.scrolling) {
                _this.drawGrid();
            }
            else {
                _this.classy.removeClass(_this.containerEl, "link");
                _this.classy.removeClass(_this.containerEl, "denied");
                var cell = _this.getCell(evt.clientX, evt.clientY);
                if (cell) {
                    _this.cellHasHistory(cell, { x: evt.clientX, y: evt.clientY });
                    if (cell.allow !== true) {
                        _this.classy.addClass(_this.containerEl, "denied");
                    }
                    else if (cell.filter || cell.link || cell.button) {
                        _this.classy.addClass(_this.containerEl, "link");
                    }
                    if (cell.history.valid && !cell.button) {
                        if (!_this.containerEl.contains(_this.historyIndicator)) {
                            _this.containerEl.appendChild(_this.historyIndicator);
                        }
                        _this.updateSelectorAttributes(cell, cell, _this.historyIndicator, true);
                    }
                    else if (_this.containerEl.contains(_this.historyIndicator)) {
                        _this.containerEl.removeChild(_this.historyIndicator);
                    }
                    if (_this.freeScroll) {
                        var x = Math.round((evt.clientX - _this.freeScroll.startX) / 20);
                        var y = Math.round((evt.clientY - _this.freeScroll.startY) / 20);
                        _this.freeScroll.x = x;
                        _this.freeScroll.y = y;
                    }
                    if (_this.cellSelection.go && !_this.alwaysEditing) {
                        _this.cellSelection.to = cell;
                        _this.updateSelector();
                    }
                }
            }
        };
        _this.onMouseUp = function (evt) {
            console.log("onMouseUp");
            _this.scrollbars.scrolling = false;
            _this.cellSelection.go = false;
            ["x", "y"].forEach2(function (axis) {
                _this.scrollbars[axis].drag = false;
            });
            _this.emit(_this.ON_CELL_SELECTED, _this.cellSelection);
            _this.rightClick = _this.isRightClick(evt);
            if (evt.which === 2) {
                if (_this.freeScroll) {
                    _this.classy.removeClass(_this.containerEl, "move");
                    cancelAnimationFrame(_this.freeScroll.animation);
                    _this.freeScroll = undefined;
                }
                else {
                    _this.freeScroll = {
                        x: 0,
                        y: 0,
                        startX: evt.clientX,
                        startY: evt.clientY,
                        row: _this.cellSelection.from.position.row * 1,
                        col: _this.cellSelection.from.position.col * 1,
                        animation: requestAnimationFrame(_this.startFreeScroll)
                    };
                    _this.classy.addClass(_this.containerEl, "move");
                }
            }
        };
        _this.onVisibilityEvent = function (evt) {
            console.log("hidden", document.hidden);
            _this.hidden = document.hidden;
            if (!_this.hidden) {
                _this.renderGrid();
            }
        };
        _this.onMouseDown = function (evt) {
            console.log(evt);
            _this.rightClick = _this.isRightClick(evt);
            var scrollHandle = false;
            ["x", "y"].forEach2(function (axis) {
                if (_this.scrollbars[axis].handle === evt.target)
                    scrollHandle = true;
            });
            var hit = _this.isCanvas(evt.target) ||
                _this.isInput(evt.target) ||
                evt.target === _this.selection ||
                evt.target === _this.historyIndicator;
            if (!scrollHandle && hit) {
                if (evt.touches) {
                    if (evt.touches.length > 1) {
                        return true;
                    }
                    evt.clientX = evt.touches[0].pageX;
                    evt.clientY = evt.touches[0].pageY;
                }
                console.log(evt.clientX, evt.clientY, evt.clientX, evt.clientY);
                var cell = _this.getCell(evt.clientX, evt.clientY);
                if (cell) {
                    if (_this.rightClick) {
                        return;
                    }
                    var d = new Date();
                    if (_this.cellSelection.clicked) {
                        if (d.getTime() - _this.cellSelection.clicked <= 300 && !_this.cellSelection.from.button) {
                            _this.emit(_this.ON_CELL_DOUBLE_CLICKED, cell);
                            _this.cellSelection.clicked = d.getTime();
                            _this.editing = true;
                            if (_this.touch) {
                                evt.stopPropagation();
                                evt.preventDefault();
                            }
                            return;
                        }
                    }
                    _this.editing = false;
                    _this.setCellSelection(cell, true, d.getTime(), { x: evt.clientX, y: evt.clientY });
                    if (_this.touch) {
                        evt.stopPropagation();
                        evt.preventDefault();
                    }
                    return;
                }
                else {
                    _this.editing = false;
                    _this.hideSelector(true);
                }
            }
            else if (!hit && evt.target.nodeName === "INPUT") {
                _this.hasFocus = false;
            }
            else if (!scrollHandle && !hit) {
                _this.hasFocus = false;
            }
        };
        _this.onMouseOver = function (evt) {
            _this.hasFocus = true;
        };
        _this.onKeydown = function (evt) {
            if (evt.keyCode === 27) {
                if (_this.editing) {
                    _this.editing = false;
                }
                else {
                    _this.hideSelector(true);
                }
                _this.emit(_this.ON_CELL_RESET, { cell: _this.cellSelection.from });
                _this.drawGrid();
                return;
            }
            console.log("onKeydown", evt);
            if (!_this.hasFocus) {
                return;
            }
            if (evt.keyCode === 17 || evt.keyCode === 91) {
                _this.ctrlDown = true;
                return;
            }
            var x = 0;
            var y = 0;
            switch (evt.keyCode) {
                case 37:
                    x = -1;
                    break;
                case 38:
                    y = -1;
                    break;
                case 39:
                    x = 1;
                    break;
                case 40:
                    y = 1;
                    break;
            }
            if ((x !== 0 || y !== 0) && !_this.editing) {
                var cell = _this.cellSelection.from;
                var row = void 0;
                var col = void 0;
                if (_this.cellSelection.from && !_this.disallowSelection) {
                    row = cell.index.row + y;
                    col = cell.index.col + x;
                    if (row >= _this.content.length) {
                        row = 0;
                    }
                    else if (row < 0) {
                        row = _this.content.length - 1;
                    }
                    if (col >= _this.content[0].length) {
                        col = 0;
                    }
                    else if (col < 0) {
                        col = _this.content[0].length - 1;
                    }
                    _this.setCellSelection(_this.content[row][col]);
                    _this.keepSelectorWithinView();
                    evt.preventDefault();
                }
                else {
                    _this.setOffsets({
                        x: x,
                        y: y
                    });
                    _this.updateScrollbars();
                    _this.drawGrid();
                    if ((y > 0 && _this.offsets.y.index !== _this.offsets.y.max) || (y < 0 && _this.offsets.y.index)) {
                        evt.preventDefault();
                    }
                }
                return;
            }
            if (evt.keyCode === 13) {
                if (!_this.alwaysEditing && !_this.touch) {
                    _this.editing = false;
                }
                _this.nextCellInput(_this.cellSelection.from || _this.cellSelection.last || _this.content[0][0], _this.shiftDown ? "up" : "down");
                _this.keepSelectorWithinView();
                return;
            }
            if (evt.keyCode === 9 && _this.cellSelection.from) {
                if (!_this.alwaysEditing && !_this.touch) {
                    _this.editing = false;
                }
                _this.nextCellInput(_this.cellSelection.from, _this.shiftDown ? "left" : "");
                evt.preventDefault();
                _this.keepSelectorWithinView();
                return;
            }
            if (evt.key === "Shift") {
                _this.shiftDown = true;
            }
            if (!_this.editing && !_this.disallowSelection && _this.canEdit) {
                if (evt.key === "F2") {
                    if (!evt.shiftKey) {
                        _this.setEditCell(_this.cellSelection.from);
                        return;
                    }
                }
                if (_this.cellSelection.from && !_this.ctrlDown) {
                    if (evt.key.length === 1 && /[a-zA-Z0-9-_ ]/.test(evt.key)) {
                        _this.keyValue = evt.key;
                        _this.input.value = evt.key;
                    }
                    if (evt.key === "Delete") {
                        _this.keyValue = "";
                        _this.input.value = "";
                    }
                    if (_this.keyValue !== undefined) {
                        _this.cellSelection.on = true;
                        _this.setEditCell(_this.cellSelection.from);
                        _this.ctrlDown = false;
                    }
                }
            }
            else if (_this.editing) {
                _this.dirty = true;
            }
        };
        _this.onKeyup = function (evt) {
            if (evt.keyCode === 17 || evt.keyCode === 91) {
                _this.ctrlDown = false;
            }
            if (evt.key === "Shift") {
                _this.shiftDown = false;
            }
        };
        _this.startFreeScroll = function () {
            if (!_this.freeScroll) {
                return;
            }
            _this.setOffsets({ x: _this.freeScroll.x, y: _this.freeScroll.y });
            _this.updateScrollbars();
            _this.drawGrid();
            requestAnimationFrame(_this.startFreeScroll);
        };
        _this.destroy();
        _this.fluid = fluid;
        _this.init = false;
        if (options.inset_scrollbars) {
            _this.scrollbars.inset = options.inset_scrollbars;
        }
        var parser = new ua();
        var parseResult = parser.getResult();
        _this.touch =
            parseResult.device && (parseResult.device.type === "tablet" || parseResult.device.type === "mobile");
        _this.classy = new Classy_1.default();
        _this.helpers = new Helpers_1.default();
        if (typeof container === "string") {
            _this.containerEl = document.getElementById(container);
        }
        else {
            _this.containerEl = container;
        }
        _this.containerEl.style.overflow = "hidden";
        var buffer = document.createElement("canvas");
        var canvas = document.createElement("canvas");
        _this.gc = {
            buffer: buffer,
            bufferContext: buffer.getContext("2d"),
            canvas: canvas,
            canvasContext: canvas.getContext("2d")
        };
        _this.containerEl.appendChild(_this.gc.canvas);
        _this.ratio = window.devicePixelRatio;
        _this.scale = _this.ratio;
        _this.ratioScale = _this.scale;
        _this.resetStage();
        _this.createSelector();
        document.addEventListener("visibilitychange", _this.onVisibilityEvent, false);
        window.addEventListener("resize", _this.onResizeEvent);
        window.addEventListener("keydown", _this.onKeydown, false);
        window.addEventListener("keyup", _this.onKeyup, false);
        window.oncontextmenu = function () {
            if (_this.rightClick && !_this.cellSelection.from) {
            }
        };
        if (_this.touch) {
            _this.setupTouch();
            _this.containerEl.addEventListener("touchstart", _this.onMouseDown, false);
            _this.containerEl.addEventListener("touchend", _this.onMouseUp, false);
        }
        else {
            if (!fluid) {
                _this.createScrollbars();
            }
            _this.containerEl.addEventListener("wheel", _this.onWheelEvent, false);
            _this.containerEl.addEventListener("mouseover", _this.onMouseOver, false);
            window.addEventListener("mousemove", _this.onMouseMove, false);
            window.addEventListener("mouseup", _this.onMouseUp, false);
            window.addEventListener("mousedown", _this.onMouseDown, false);
        }
        return _this;
    }
    Object.defineProperty(Grid.prototype, "ON_CELL_CLICKED", {
        get: function () {
            return "cell_clicked";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_CLICKED_RIGHT", {
        get: function () {
            return "cell_clicked_right";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_HISTORY_CLICKED", {
        get: function () {
            return "cell_history_clicked";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_LINK_CLICKED", {
        get: function () {
            return "cell_link_clicked";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_DOUBLE_CLICKED", {
        get: function () {
            return "cell_double_clicked";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_TAG_CLICKED", {
        get: function () {
            return "tag_clicked";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_VALUE", {
        get: function () {
            return "cell_value";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_SELECTED", {
        get: function () {
            return "cell_selected";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_RESET", {
        get: function () {
            return "cell_reset";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_EDITING", {
        get: function () {
            return "editing";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "fit", {
        get: function () {
            return this._fit;
        },
        set: function (value) {
            if (["scroll", "width", "height", "contain"].indexOf(value) === -1) {
                return;
            }
            this._fit = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "editing", {
        get: function () {
            return this._editing;
        },
        set: function (value) {
            this._editing = value;
            this.emit(this.ON_EDITING, { cell: this.cellSelection.from, editing: this._editing, dirty: this.dirty });
            if (value) {
                if (!this.cellSelection.from || this.cellSelection.from.allow !== true || !this.canEdit) {
                    this._editing = false;
                    return;
                }
                this.showInput();
            }
            else {
                this.keyValue = undefined;
                if (this.selection.contains(this.input)) {
                    this.input.value = "";
                    this.input.blur();
                    this.input.style["visibility"] = "hidden";
                }
                if (!this.touch && this.selection.contains(this.input)) {
                    this.selection.removeChild(this.input);
                    this.selection.contentEditable = true;
                }
                if (this.dirty) {
                    this.dirty = false;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "tracking", {
        get: function () {
            return this._tracking;
        },
        set: function (value) {
            this._tracking = value;
            if (!value) {
                this.history = [];
            }
            this.drawGrid();
        },
        enumerable: true,
        configurable: true
    });
    Grid.prototype.destroy = function () {
        if (!this.containerEl) {
            return;
        }
        try {
            window.removeEventListener("resize", this.onResizeEvent);
            window.removeEventListener("keydown", this.onKeydown, false);
            window.removeEventListener("keyup", this.onKeyup, false);
            document.removeEventListener("visibilitychange", this.onVisibilityEvent, false);
            if (!this.touch) {
                this.containerEl.removeEventListener("wheel", this.onWheelEvent);
                this.containerEl.removeEventListener("mouseover", this.onMouseOver, false);
                window.removeEventListener("mousemove", this.onMouseMove, false);
                window.removeEventListener("mouseup", this.onMouseUp, false);
                window.removeEventListener("mousedown", this.onMouseDown, false);
            }
            else {
                this.containerEl.removeEventListener("touchstart", this.onMouseDown, false);
                this.containerEl.removeEventListener("touchend", this.onMouseUp, false);
            }
            this.containerEl.innerHTML = "";
            if (this.hammer) {
                this.hammer.destroy();
            }
            this.removeEvent();
        }
        catch (e) {
            console.log(e);
        }
    };
    Grid.prototype.cellHistory = function (data) {
        var _this = this;
        this.history = [];
        if (!data.length) {
            return;
        }
        var users = [];
        var count = 0;
        data.forEach2(function (push) {
            count++;
            if (count === 1) {
                return;
            }
            if (users.indexOf(push.modified_by.id) === -1) {
                users.push(push.modified_by.id);
            }
            var userIndex = users.indexOf(push.modified_by.id);
            push.content_diff.forEach2(function (row, rowIndex) {
                if (!row) {
                    return;
                }
                if (!_this.history[rowIndex]) {
                    _this.history[rowIndex] = [];
                }
                row.forEach2(function (cellData, cellIndex) {
                    if (!cellData) {
                        return;
                    }
                    _this.history[rowIndex][cellIndex] = userIndex;
                });
            });
        });
    };
    Grid.prototype.setTrackingHighlights = function (data) {
        this.clearTrackingHighlights();
        if (!data || !data.content_diff || !data.content_diff.length) {
            return;
        }
        for (var rowIndex = 0; rowIndex < data.content_diff.length; rowIndex++) {
            var row = data.content_diff[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                if (!row[colIndex])
                    continue;
                if (!this.trackingHighlights[rowIndex]) {
                    this.trackingHighlights[rowIndex] = [];
                }
                this.trackingHighlights[rowIndex][colIndex] = true;
            }
        }
    };
    Grid.prototype.clearTrackingHighlights = function () {
        var highlights = this.containerEl.getElementsByClassName("tracking");
        while (highlights[0]) {
            highlights[0].parentNode.removeChild(highlights[0]);
        }
        this.trackingHighlights = [];
    };
    Grid.prototype.setContent = function (content) {
        this.originalContent = content;
        this.updateContent();
    };
    Grid.prototype.getContentHtml = function () {
        var html = "<table style=\"border-collapse: collapse;\">";
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            if (this.cellSelection.from &&
                (rowIndex < this.cellSelection.from.index.row || rowIndex > this.cellSelection.to.index.row)) {
                continue;
            }
            html += "<tr>";
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                if (this.cellSelection.to &&
                    (colIndex < this.cellSelection.from.index.col || colIndex > this.cellSelection.to.index.col)) {
                    continue;
                }
                var cell = this.originalContent[rowIndex][colIndex];
                var styles = this.flattenStyles(cell.style);
                html += "<td style=\"" + styles + "\">" + (col.allow === "no" ? "" : cell.formatted_value) + "</td>";
            }
            html += "</tr>";
        }
        html += "</table>";
        return html;
    };
    Grid.prototype.getCells = function (fromCell, toCell, refCell) {
        var rowEnd = toCell.row;
        if (rowEnd === -1) {
            rowEnd = this.originalContent.length - 1;
        }
        var colEnd = toCell.col;
        if (colEnd === -1) {
            colEnd = this.originalContent[0].length - 1;
        }
        var cellUpdates = [];
        for (var row = fromCell.row; row <= rowEnd; row++) {
            for (var col = fromCell.col; col <= colEnd; col++) {
                if (!cellUpdates[row])
                    cellUpdates[row] = [];
                cellUpdates[row][col] = this.content[row][col];
            }
        }
        return cellUpdates;
    };
    Grid.prototype.renderGrid = function (clear) {
        if (clear === void 0) { clear = false; }
        if (this.hidden) {
            return;
        }
        var diff = !this.width ||
            clear ||
            (!this.content.length && !this.content.length) ||
            this.content.length !== this.content.length ||
            this.content[0].length !== this.content[0].length;
        this.buildCells();
        this.updateCellAccess();
        if (diff) {
            this.setSize();
        }
        this.setOffsets(undefined);
        if (diff) {
            this.setFit(this.fit);
        }
        this.updateScrollbars();
        if (!this.init) {
            this.init = true;
            requestAnimationFrame(this.drawGrid);
        }
        else {
            this.drawGrid();
        }
    };
    Grid.prototype.setScale = function (n) {
        this.scale = n * this.ratio;
        this.setOffsets(undefined);
        this.updateScrollbars();
        requestAnimationFrame(this.drawGrid);
    };
    Grid.prototype.setFit = function (n, retain) {
        if (!/(scroll|width|height|contain)/.test(n)) {
            return;
        }
        console.log("setFit", n);
        this.updateFit(n, !this.touch);
        this.setSize();
        if (!this.touch) {
            this.updateFit(n, true);
        }
    };
    Grid.prototype.clearRanges = function () {
        this.accessRanges = [];
        this.buttonRanges = [];
        this.styleRanges = [];
        this.freezeRange = {
            valid: false,
            index: {
                row: 0,
                col: 0
            }
        };
    };
    Grid.prototype.setRanges = function (ranges, userId) {
        if (userId === void 0) { userId = undefined; }
        this.clearRanges();
        for (var i = 0; i < ranges.length; i++) {
            var range = ranges[i];
            var rowEnd = void 0;
            var colEnd = void 0;
            if (range.range) {
                rowEnd = _.clone(range.range.to.row);
                colEnd = _.clone(range.range.to.col);
                if (rowEnd === -1) {
                    rowEnd = this.originalContent.length - 1;
                }
                if (colEnd === -1) {
                    colEnd = this.originalContent[0].length - 1;
                }
            }
            if (range.name === "frozen_rows") {
                this.freezeRange.index.row = range.count;
            }
            else if (range.name === "frozen_cols") {
                this.freezeRange.index.col = range.count;
            }
            else if (range instanceof Range_3.StyleRange) {
                for (var i_1 = range.range.from.row; i_1 <= rowEnd; i_1++) {
                    for (var k = range.range.from.col; k <= colEnd; k++) {
                        if (!this.styleRanges[i_1]) {
                            this.styleRanges[i_1] = [];
                        }
                        if (!this.styleRanges[i_1][k]) {
                            this.styleRanges[i_1][k] = [];
                        }
                        this.styleRanges[i_1][k].push(range);
                    }
                }
            }
            else if (range instanceof Range_2.ButtonRange) {
                for (var i_2 = range.range.from.row; i_2 <= rowEnd; i_2++) {
                    for (var k = range.range.from.col; k <= colEnd; k++) {
                        if (!this.buttonRanges[i_2]) {
                            this.buttonRanges[i_2] = [];
                        }
                        if (!this.buttonRanges[i_2][k]) {
                            this.buttonRanges[i_2][k] = [];
                        }
                        this.buttonRanges[i_2][k].push(range);
                    }
                }
            }
            else if (range instanceof Range_1.PermissionRange && this.originalContent.length) {
                var userRight = range.getPermission(userId || 0) || (userId ? "yes" : "no");
                var rowEnd_1 = _.clone(range.rowEnd);
                var colEnd_1 = _.clone(range.colEnd);
                if (rowEnd_1 === -1) {
                    rowEnd_1 = this.originalContent.length - 1;
                }
                if (colEnd_1 === -1) {
                    colEnd_1 = this.originalContent[0].length - 1;
                }
                for (var i_3 = range.rowStart; i_3 <= rowEnd_1; i_3++) {
                    for (var k = range.colStart; k <= colEnd_1; k++) {
                        if (!this.accessRanges[i_3]) {
                            this.accessRanges[i_3] = [];
                        }
                        if (!this.accessRanges[i_3][k]) {
                            this.accessRanges[i_3][k] = [];
                        }
                        if (userRight) {
                            this.accessRanges[i_3][k].push(userRight);
                        }
                    }
                }
            }
        }
        this.updateCellAccess();
        if (this.freezeRange.index.row > 0 || this.freezeRange.index.col > 0) {
            this.freezeRange.valid = true;
        }
    };
    Grid.prototype.clearFilters = function () {
        this.filters = {};
        this.updateContent();
    };
    Grid.prototype.setFilters = function (filters) {
        var _this = this;
        if (filters.col >= 0) {
            filters = [filters];
        }
        filters.forEach2(function (filter) {
            if (!filter.value || filter.remove) {
                if (_this.filters["col" + filter.col]) {
                    delete _this.filters["col" + filter.col];
                }
                return;
            }
            if (!filter.type)
                filter.type = "number";
            filter.type = /(number|string|date)/.test(filter.type) ? filter.type : "number";
            if (!filter.exp ||
                (filter.type === "number" && [">", "<", "<=", ">=", "==", "==="].indexOf(filter.exp) === -1)) {
                filter.exp = "==";
            }
            _this.filters["col" + filter.col] = filter;
        });
        this.updateContent();
    };
    Grid.prototype.clearSorting = function () {
        this.sorting = {};
        this.updateContent();
    };
    Grid.prototype.setSorting = function (filters) {
        var _this = this;
        this.clearSorting();
        if (filters.col >= 0) {
            filters = [filters];
        }
        filters.forEach2(function (filter) {
            if (filter.remove) {
                if (_this.filters["col" + filter.col]) {
                    delete _this.filters["col" + filter.col];
                }
                return;
            }
            if (!filter.direction)
                filter.direction = "ASC";
            filter.direction = ("" + filter.direction).toUpperCase();
            filter.direction = /(ASC|DESC)/.test(filter.direction) ? filter.direction : "ASC";
            filter.col = filter.col * 1 || 0;
            filter.type = /(number|string|date)/.test(filter.type) ? filter.type : "number";
            _this.sorting["col" + filter.col] = filter;
        });
        this.updateContent();
    };
    Grid.prototype.hideSelector = function (focus) {
        if (focus === void 0) { focus = false; }
        this.emitCellChange(this.cellSelection.from);
        this.cellSelection.last = this.cellSelection.from;
        this.cellSelection.on = false;
        this.cellSelection.from = undefined;
        this.cellSelection.to = undefined;
        this.selection.style["display"] = "none";
        this.emit(this.ON_CELL_SELECTED, this.cellSelection);
        this.hideHistoryIndicator();
    };
    Grid.prototype.find = function (value) {
        var highlights = this.containerEl.getElementsByClassName("highlight");
        while (highlights[0]) {
            highlights[0].parentNode.removeChild(highlights[0]);
        }
        this.found = [];
        this.foundSelection.count = 0;
        this.foundSelection.selected = 0;
        if (!value) {
            return;
        }
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                var regex = new RegExp(value, "ig");
                var cell = this.originalContent[rowIndex][colIndex];
                if (col.allow === "no" || (!regex.test(cell.value) && !regex.test(cell.formatted_value))) {
                    continue;
                }
                if (!this.found[rowIndex]) {
                    this.found[rowIndex] = [];
                }
                this.foundSelection.count++;
                this.found[rowIndex][colIndex] = {
                    cell: col,
                    highlight: this.createHighlight(cell),
                    originalCell: cell
                };
            }
        }
        this.updateSelector();
    };
    Grid.prototype.gotoFoundCell = function (which) {
        var _this = this;
        if (typeof which === "number") {
            if (which < 0 || which > this.foundSelection.count) {
                return;
            }
            this.foundSelection.selected = which;
        }
        else if (["prev", "next"].indexOf(which) > -1) {
            if (which === "next") {
                this.foundSelection.selected++;
            }
            else {
                this.foundSelection.selected--;
            }
            if (this.foundSelection.selected > this.foundSelection.count) {
                this.foundSelection.selected = 1;
            }
            else if (this.foundSelection.selected < 1) {
                this.foundSelection.selected = this.foundSelection.count;
            }
        }
        else {
            return;
        }
        var count = 0;
        this.found.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                count++;
                if (count === _this.foundSelection.selected) {
                    _this.cellSelection.from = col.cell;
                    _this.cellSelection.to = col.cell;
                    _this.cellSelection.on = true;
                    _this.goto(col.cell.index.col, col.cell.index.row, true);
                }
            });
        });
    };
    Grid.prototype.setSelector = function (from, to) {
        if (!to) {
            to = from;
        }
        if (!this.content[from.row] || !this.content[from.row][from.col]) {
            return;
        }
        this.updateSelectorAttributes(this.content[from.row][from.col], this.content[to.row][to.col], this.selection);
        this.cellSelection.from = this.content[from.row][from.col];
        this.cellSelection.to = this.content[to.row][to.col];
        this.emit(this.ON_CELL_SELECTED, this.cellSelection);
    };
    Grid.prototype.updateContent = function () {
        this.content = this.setCellIndexes(this.originalContent);
        this.applyFilters();
        this.applySorting();
        if (this.cellSelection.from && !this.content[this.cellSelection.from.position.row]) {
            this.hideSelector();
        }
    };
    Grid.prototype.showInput = function () {
        var _this = this;
        var cell = this.originalContent[this.cellSelection.from.position.row][this.cellSelection.from.position.col];
        var unit = cell.style["font-size"].indexOf("px") > -1 ? "px" : "pt";
        this.input.style["font-size"] = "" + parseFloat(cell.style["font-size"]) * this.scale / this.ratio + unit;
        this.input.style["font-family"] = cell.style["font-family"] + ", sans-serif";
        this.input.style["font-weight"] = cell.style["font-weight"];
        this.input.style["text-align"] = cell.style["text-align"];
        this.input.style["visibility"] = "visible";
        if (!this.touch) {
            this.selection.appendChild(this.input);
            this.selection.contentEditable = false;
            var interval_1 = setInterval(function () {
                if (!_this.selection.contains(_this.input))
                    return;
                clearInterval(interval_1);
                if (_this.keyValue === undefined) {
                    _this.input.value = "" + cell.formatted_value;
                }
                _this.input.focus();
                if (_this.keyValue !== undefined) {
                    _this.input.selectionStart = 999;
                }
                else if (_this.input.value) {
                    _this.input.selectionStart = 0;
                    _this.input.selectionEnd = 999;
                }
            }, 20);
        }
        else {
            if (this.keyValue === undefined) {
                this.input.value = "" + cell.formatted_value;
            }
            this.input.focus();
            if (this.keyValue !== undefined) {
                this.input.selectionStart = 999;
            }
            else if (this.input.value) {
                this.input.selectionStart = 0;
                this.input.selectionEnd = 999;
            }
        }
    };
    Grid.prototype.setCellIndexes = function (content) {
        var clone = [];
        this.links = [];
        var links = this.containerEl.getElementsByClassName("external-link");
        while (links[0]) {
            links[0].parentNode.removeChild(links[0]);
        }
        for (var rowIndex = 0; rowIndex < content.length; rowIndex++) {
            var row = content[rowIndex];
            clone[rowIndex] = [];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var cell = content[rowIndex][colIndex];
                var col = {
                    value: _.clone(cell.value),
                    formatted_value: _.clone(cell.formatted_value),
                    width: parseFloat(cell.style.width),
                    position: {
                        row: rowIndex,
                        col: colIndex
                    }
                };
                clone[rowIndex].push(col);
                if (cell.link && cell.link.external) {
                    if (!this.links[rowIndex])
                        this.links[rowIndex] = [];
                    this.links[rowIndex][colIndex] = {
                        cell: col,
                        highlight: this.createHighlight(col, {
                            className: "external-link",
                            href: cell.link.address,
                            target: "_blank"
                        })
                    };
                }
            }
        }
        return clone;
    };
    Grid.prototype.updateFound = function (cell) {
        var _this = this;
        if (!this.found[cell.index.row] || !this.found[cell.index.row][cell.index.col]) {
            return;
        }
        var count = 0;
        this.found.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                count++;
                if (col.cell === cell) {
                    _this.foundSelection.selected = count;
                }
            });
        });
    };
    Grid.prototype.createHighlight = function (cell, attrs, styles) {
        if (attrs === void 0) { attrs = {}; }
        if (styles === void 0) { styles = {}; }
        var highlight = document.createElement(attrs.href ? "a" : "div");
        var inner = document.createElement("div");
        for (var attr in attrs) {
            highlight[attr] = attrs[attr];
        }
        if (!highlight.className)
            highlight.className = "highlight";
        highlight.style.position = "absolute";
        highlight.style["z-index"] = 10;
        highlight.style["left"] = "0";
        highlight.style["top"] = "0";
        highlight.style["width"] = cell.width + "px";
        highlight.style["height"] = cell.height + "px";
        highlight.style["display"] = "none";
        for (var style in styles) {
            inner.style[style] = styles[style];
        }
        highlight.appendChild(inner);
        this.containerEl.appendChild(highlight);
        return highlight;
    };
    Grid.prototype.applyFilters = function () {
        if (!Object.keys(this.filters).length) {
            return false;
        }
        var rowsToRemove = [];
        for (var rowIndex = 0; rowIndex < this.originalContent.length; rowIndex++) {
            if (this.freezeRange.valid && rowIndex < this.freezeRange.index.row) {
                continue;
            }
            var _loop_1 = function (f) {
                var filter = this_1.filters[f];
                var col = this_1.originalContent[rowIndex][filter.col];
                if (!col || rowsToRemove.indexOf(rowIndex) > -1)
                    return "continue";
                var colValue = filter.type === "number" ? parseFloat(col.value) : "" + col.formatted_value;
                var filterValue = filter.type === "number" ? parseFloat(filter.value) : "" + filter.value;
                var validValue = false;
                switch (filter.type) {
                    case "number":
                        validValue = eval(colValue + " " + filter.exp + " " + filterValue);
                        break;
                    case "date":
                        var dates = filterValue.split(",");
                        var fromDate = new Date(dates[0]).toISOString();
                        var toDate = new Date(dates[1]).toISOString();
                        var date = new Date(this_1.helpers.parseDateExcel(colValue)).toISOString();
                        validValue = date >= fromDate && date <= toDate;
                        break;
                    default:
                        filterValue.split(",").forEach2(function (value) {
                            if (validValue) {
                                return;
                            }
                            switch (filter.exp) {
                                case "contains":
                                    validValue = colValue.toLowerCase().indexOf(value.toLowerCase()) > -1;
                                    break;
                                case "starts_with":
                                    validValue =
                                        colValue.toLowerCase().substring(0, value.length) === value.toLowerCase();
                                    break;
                                default:
                                    validValue = colValue == value;
                                    break;
                            }
                        });
                        break;
                }
                if (!validValue)
                    rowsToRemove.push(rowIndex);
            };
            var this_1 = this;
            for (var f in this.filters) {
                _loop_1(f);
            }
        }
        for (var i = rowsToRemove.length - 1; i >= 0; i--)
            this.content.splice(rowsToRemove[i], 1);
    };
    Grid.prototype.applySorting = function () {
        if (!Object.keys(this.sorting).length) {
            return false;
        }
        var key = Object.keys(this.sorting)[0];
        var filter = this.sorting[key];
        var data = this.freezeRange.index.row
            ? this.content.slice(this.freezeRange.index.row, this.content.length)
            : this.content.slice(0);
        var exp = filter.direction === "DESC" ? ">" : "<";
        var opp = filter.direction === "DESC" ? "<" : ">";
        data.sort(function (a, b) {
            var aVal = filter.type === "number" ? parseFloat(a[filter.col].value) || 0 : "\"" + a[filter.col].value + "\"";
            var bVal = filter.type === "number" ? parseFloat(b[filter.col].value) || 0 : "\"" + b[filter.col].value + "\"";
            if (eval(aVal + " " + exp + " " + bVal)) {
                return -1;
            }
            if (eval(aVal + " " + opp + " " + bVal)) {
                return 1;
            }
            return 0;
        });
        this.content = this.freezeRange.index.row
            ? this.content.slice(0, this.freezeRange.index.row).concat(data)
            : data;
    };
    Grid.prototype.buildCells = function () {
        this.resetStage();
        this.stage.x.increments.push(0);
        this.stage.y.increments.push(0);
        var freezeOffset = {
            x: 0,
            y: 0
        };
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            this.stage.x.pos = 0;
            var col = void 0;
            var originalCell = void 0;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                col = row[colIndex];
                originalCell = this.originalContent[col.position.row][col.position.col];
                var wrap = false;
                if ((originalCell.style["text-wrap"] && originalCell.style["text-wrap"] === "wrap") ||
                    originalCell.style["word-wrap"] === "break-word") {
                    wrap = true;
                }
                col.wrap = wrap;
                col.x = this.stage.x.pos;
                col.y = this.stage.y.pos;
                col.width = parseFloat(originalCell.style.width);
                col.height = parseFloat(originalCell.style.height);
                col.index = {
                    row: rowIndex,
                    col: colIndex
                };
                col.allow = true;
                col.link = originalCell.link;
                this.stage.x.pos += col.width;
                if (colIndex === row.length - 1 && this.borders.widths[originalCell.style.rbw]) {
                    this.stage.x.pos += this.borders.widths[originalCell.style.rbw];
                }
                if (!rowIndex) {
                    if (!this.freezeRange.valid || (this.freezeRange.valid && colIndex >= this.freezeRange.index.col)) {
                        this.stage.x.increments.push(this.stage.x.pos - freezeOffset.x);
                    }
                    else {
                        freezeOffset.x += col.width;
                    }
                }
                if (this.trackingHighlights[rowIndex] && this.trackingHighlights[rowIndex][colIndex]) {
                    var styles = {};
                    if (this.trackingHighlightMatchColor)
                        styles["background-color"] = "#" + originalCell.style.color.replace("#", "");
                    this.trackingHighlights[rowIndex][colIndex] = new GridTrackingHighlight(col, this.createHighlight(col, {
                        className: "tracking " + this.trackingHighlightClassname
                    }, styles));
                    this.trackingHighlights[rowIndex][colIndex].lifetime = this.trackingHighlightLifetime;
                }
            }
            if (this.stage.x.pos > this.stage.x.size) {
                this.stage.x.size = this.stage.x.pos;
            }
            this.stage.y.pos += col.height;
            this.stage.y.size += col.height;
            if (rowIndex === this.content.length - 1 && this.borders.widths[originalCell.style.bbw]) {
                this.stage.y.pos += this.borders.widths[originalCell.style.bbw];
                this.stage.y.size += this.borders.widths[originalCell.style.bbw];
            }
            if (!this.freezeRange.valid || (this.freezeRange.valid && rowIndex >= this.freezeRange.index.row)) {
                this.stage.y.increments.push(this.stage.y.pos - freezeOffset.y);
            }
            else {
                freezeOffset.y += col.height;
            }
        }
    };
    Grid.prototype.updateCellAccess = function () {
        var _this = this;
        if (!this.content.length) {
            return;
        }
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                col.allow = true;
                col.button = false;
                col.formatting = false;
            }
        }
        this.accessRanges.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col || col.indexOf("yes") !== -1)
                    return;
                col.forEach2(function (element) {
                    if (["ro", "no"].indexOf(element) === -1) {
                        return;
                    }
                    if (_this.content[rowIndex] && _this.content[rowIndex][colIndex]) {
                        _this.content[rowIndex][colIndex].allow = element;
                    }
                });
            });
        });
        this.buttonRanges.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                if (_this.content[rowIndex] && _this.content[rowIndex][colIndex]) {
                    _this.content[rowIndex][colIndex].button = col;
                }
            });
        });
        var rowEnd = this.originalContent.length - 1;
        var colEnd = this.originalContent[0].length - 1;
        this.styleRanges.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                if (_this.content[rowIndex] && _this.content[rowIndex][colIndex]) {
                    col.forEach2(function (styleRange) {
                        if (!styleRange.match(_this.content[rowIndex][colIndex].value)) {
                            return;
                        }
                        styleRange.formatting.forEach2(function (format) {
                            var cells = styleRange.parseFormat(format, rowIndex, colIndex, rowEnd, colEnd);
                            for (var i = cells.from.row; i <= cells.to.row; i++) {
                                for (var k = cells.from.col; k <= cells.to.col; k++) {
                                    if (_this.content[i] && _this.content[i][k]) {
                                        if (!_this.content[i][k].formatting) {
                                            _this.content[i][k].formatting = cells.style;
                                        }
                                        else {
                                            _this.content[i][k].formatting = merge(_this.content[i][k].formatting, cells.style);
                                        }
                                    }
                                }
                            }
                        });
                    });
                }
            });
        });
    };
    Grid.prototype.updateFit = function (n, retain) {
        this.fit = n;
        switch (n) {
            case "scroll":
                this.setScale(retain ? this.ratio : this.ratioScale / this.ratio);
                break;
            case "width":
                if (!this.stage.x.size) {
                    return;
                }
                this.setScale(this.width / this.stage.x.size);
                break;
            case "height":
                if (!this.stage.y.size) {
                    return;
                }
                this.setScale(this.height / this.stage.y.size);
                break;
            case "contain":
                if (!this.stage.x.size || !this.stage.y.size) {
                    return;
                }
                var w = this.width / this.stage.x.size;
                var h = this.height / this.stage.y.size;
                this.setScale(h < w ? h : w);
                break;
        }
    };
    Grid.prototype.setupTouch = function () {
        var _this = this;
        this.hammer = new Hammer(this.containerEl);
        var pan = new Hammer.Pan();
        var pinch = new Hammer.Pinch();
        this.hammer.add([pan, pinch]);
        var currentScale = this.scale / this.ratio * 1;
        this.hammer.on("pinchstart", function (evt) {
            _this.fit = "scroll";
            currentScale = _this.scale / _this.ratio * 1;
        });
        this.hammer.on("pinchmove", function (evt) {
            _this.setScale(currentScale * evt.scale);
        });
        this.hammer.on("pinchend", function (evt) {
            currentScale = _this.scale / _this.ratio * 1;
        });
        var currentOffsets = {
            x: 0,
            y: 0
        };
        this.hammer.on("panstart", function (evt) {
            currentOffsets = {
                x: evt.center.x,
                y: evt.center.y
            };
        });
        this.hammer.on("panmove", function (evt) {
            var moveBy = Math.round(_this.ratio / _this.scale);
            if (moveBy < 1)
                moveBy = 1;
            var x = 0;
            if (Math.abs(evt.center.x - currentOffsets.x) > 5 * _this.scale) {
                var directionX = evt.velocityX > 0 ? -1 : 1;
                currentOffsets.x = evt.center.x;
                x = moveBy * directionX;
            }
            var y = 0;
            if (Math.abs(evt.center.y - currentOffsets.y) > 5 * _this.scale) {
                var directionY = evt.velocityY > 0 ? -1 : 1;
                currentOffsets.y = evt.center.y;
                y = moveBy * directionY;
            }
            _this.setOffsets({ x: x, y: y });
            _this.drawGrid();
        });
        this.hammer.on("panend", function (evt) { });
    };
    Grid.prototype.isRightClick = function (evt) {
        var isRightMB;
        if ("which" in evt)
            isRightMB = evt.which === 3;
        else if ("button" in evt)
            isRightMB = evt.button === 2;
        return isRightMB;
    };
    Grid.prototype.setCellSelection = function (cell, go, clickTime, coords) {
        if (go === void 0) { go = false; }
        if (coords === void 0) { coords = { x: 0, y: 0 }; }
        if (this.editing) {
        }
        this.cellHasHistory(cell, coords);
        this.cellSelection.clicked = clickTime;
        this.cellSelection.from = cell;
        this.cellSelection.to = cell;
        this.cellSelection.go = false;
        this.cellSelection.on = false;
        if (!this.disallowSelection) {
            if (!cell.link) {
                this.cellSelection.go = go;
                this.cellSelection.on = true;
                this.updateSelector();
            }
        }
        this.updateFound(cell);
        this.emit(this.ON_CELL_SELECTED, this.cellSelection);
        if (clickTime) {
            this.emit(this.ON_CELL_CLICKED, { cell: cell, history: cell.history.show });
        }
        this.hasFocus = true;
        this.keyValue = undefined;
        if (!this.touch)
            this.selection.innerText = "";
        if (this.editing) {
            this.showInput();
        }
        else if (this.alwaysEditing) {
            this.editing = true;
        }
    };
    Grid.prototype.keepSelectorWithinView = function () {
        var _this = this;
        if (this.touch) {
            var yIndex = this.cellSelection.from.index.row;
            if (this.freezeRange.valid) {
                yIndex += this.freezeRange.index.row;
            }
            if (!this.visible[yIndex]) {
                this.setOffsets({
                    y: yIndex
                });
                requestAnimationFrame(this.drawGrid);
            }
            return;
        }
        var row = this.cellSelection.from.index.row;
        var col = this.cellSelection.from.index.col;
        var offsetX = 0, offsetY = 0;
        var rowStartIndex = -1, rowEndIndex = -1, colStartIndex = -1, colEndIndex = -1;
        var colRow;
        this.visible.forEach2(function (cellRow, cellRowIndex) {
            if (!cellRow)
                return;
            if (rowStartIndex < 0 && (!_this.freezeRange.valid || cellRowIndex > _this.freezeRange.index.row)) {
                rowStartIndex = cellRowIndex + 0;
            }
            rowEndIndex = cellRowIndex;
            if (!colRow && (!_this.freezeRange.valid || cellRowIndex > _this.freezeRange.index.row)) {
                colRow = cellRow;
            }
        });
        colRow.forEach2(function (cellCol, cellColIndex) {
            if (!cellCol)
                return;
            if (colStartIndex < 0 && (!_this.freezeRange.valid || cellColIndex > _this.freezeRange.index.col)) {
                colStartIndex = cellColIndex + 0;
            }
            colEndIndex = cellColIndex + 0;
        });
        if (!row) {
            this.offsets.y.index = 0;
        }
        else if (row === this.content.length - 1) {
            this.offsets.y.index = this.offsets.y.max;
        }
        else if (row < rowStartIndex + 2 + this.freezeRange.index.row) {
            offsetY = -1;
        }
        else if (row > rowEndIndex - 2) {
            offsetY = 1;
        }
        if (!col) {
            this.offsets.x.index = 0;
        }
        else if (col === this.content[0].length - 1) {
            this.offsets.x.index = this.offsets.x.max;
        }
        else {
            offsetX = col - this.offsets.x.index - this.freezeRange.index.col;
        }
        console.log("offset: " + offsetX + ", col: " + col + ", start: " + colStartIndex + ", end: " + colEndIndex);
        this.goto(offsetX, offsetY);
    };
    Grid.prototype.goto = function (x, y, set) {
        if (set === void 0) { set = false; }
        this.setOffsets({ x: x, y: y }, set);
        this.updateScrollbars();
        this.drawGrid();
        setTimeout(function () {
        }, 50);
    };
    Grid.prototype.restoreCell = function (cell) {
        if (!cell) {
            return;
        }
        cell.dirty = false;
        cell.formatted_value = this.content[cell.position.row][cell.position.col].formatted_value;
        cell.value = this.content[cell.position.row][cell.position.col].value;
    };
    Grid.prototype.nextCellInput = function (cell, direction) {
        if (direction === void 0) { direction = ""; }
        var row = cell.index.row;
        var col = cell.index.col;
        if (direction === "left") {
            if (this.content[row][col - 1]) {
                this.setCellSelection(this.content[row][col - 1]);
            }
            else if (this.content[row - 1]) {
                this.setCellSelection(this.content[row - 1][this.content[row].length - 1]);
            }
            return;
        }
        if (direction === "up") {
            if (this.content[row - 1] && this.content[row - 1][col]) {
                this.setCellSelection(this.content[row - 1][col]);
                return;
            }
            else if (this.content[this.content.length - 1] && this.content[this.content.length - 1][col - 1]) {
                this.setCellSelection(this.content[this.content.length - 1][col - 1]);
                return;
            }
        }
        if (direction === "down") {
            if (this.content[row + 1] && this.content[row + 1][col]) {
                this.setCellSelection(this.content[row + 1][col]);
                return;
            }
            else if (this.content[0][col + 1]) {
                this.setCellSelection(this.content[0][col + 1]);
                return;
            }
        }
        if (this.content[row][col + 1]) {
            this.setCellSelection(this.content[row][col + 1]);
            return;
        }
        if (this.content[row + 1]) {
            this.setCellSelection(this.content[row + 1][0]);
            return;
        }
        this.setCellSelection(this.content[0][0]);
        return;
    };
    Grid.prototype.setEditCell = function (cell) {
        if (cell.allow !== true) {
            this.editing = false;
            return false;
        }
        else {
            this.emitCellChange(this.cellSelection.from);
        }
        if (this.keyValue !== undefined) {
            cell.formatted_value = cell.value = _.clone(this.keyValue);
            cell.dirty = true;
        }
        this.cellSelection.from = cell;
        this.cellSelection.to = cell;
        this.keepSelectorWithinView();
        this.editing = true;
        if (this.keyValue !== undefined) {
        }
        return true;
    };
    Grid.prototype.emitCellChange = function (cell) {
        if (!cell || !cell.dirty) {
            return;
        }
        this.keyValue = undefined;
        this.input.value = "";
        this.softMerges(this.content[this.cellSelection.from.index.row], this.cellSelection.from.index.row);
        this.drawGrid();
        cell.dirty = false;
    };
    Grid.prototype.isCanvas = function (target) {
        var clickedEl = target;
        while (clickedEl && clickedEl !== this.gc.canvas) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === this.gc.canvas;
    };
    Grid.prototype.isInput = function (target) {
        var clickedEl = target;
        while (clickedEl && clickedEl !== this.input) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === this.input;
    };
    Grid.prototype.getCell = function (x, y) {
        var rect = this.containerEl.getBoundingClientRect();
        x -= rect.left - this.offsets.x.offset * this.scale / this.ratio;
        y -= rect.top - this.offsets.y.offset * this.scale / this.ratio;
        var cell;
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                if (x < col.x * this.scale / this.ratio)
                    continue;
                if (x > (col.x + col.width) * this.scale / this.ratio)
                    continue;
                if (y < col.y * this.scale / this.ratio)
                    continue;
                if (y > (col.y + col.height) * this.scale / this.ratio)
                    continue;
                cell = col;
                break;
            }
            if (cell)
                break;
        }
        return cell;
    };
    Grid.prototype.cellHasHistory = function (cell, coords) {
        if (coords === void 0) { coords = { x: 0, y: 0 }; }
        var rect = this.containerEl.getBoundingClientRect();
        coords.x -= rect.left - this.offsets.x.offset * this.scale / this.ratio;
        coords.y -= rect.top - this.offsets.y.offset * this.scale / this.ratio;
        var history = this.history[cell.position.row] && this.history[cell.position.row][cell.position.col] >= 0;
        cell.history = {
            valid: history,
            show: history &&
                coords.x > (cell.x + cell.overlayWidth - 16) * this.scale / this.ratio &&
                coords.y < (cell.y + 16) * this.scale / this.ratio
                ? true
                : false,
            color: history ? this.userTrackingColors[this.history[cell.position.row][cell.position.col]] : ""
        };
    };
    Grid.prototype.createScrollbars = function () {
        var _this = this;
        ["x", "y"].forEach2(function (axis) {
            var anchor = _this.scrollbars[axis].anchor;
            _this.scrollbars[axis].track = document.createElement("div");
            _this.scrollbars[axis].handle = document.createElement("div");
            _this.scrollbars[axis].track.style["position"] = "absolute";
            _this.scrollbars[axis].track.style["z-index"] = 100;
            _this.scrollbars[axis].track.style["display"] = "none";
            _this.scrollbars[axis].track.className = "track";
            _this.scrollbars[axis].handle.style["position"] = "absolute";
            _this.scrollbars[axis].handle.style["z-index"] = 1;
            _this.scrollbars[axis].handle.style["left"] = 0;
            _this.scrollbars[axis].handle.style["top"] = 0;
            _this.scrollbars[axis].handle.className = "handle";
            _this.scrollbars[axis].handle.addEventListener("mousedown", function (evt) {
                console.log("mousedown");
                _this.scrollbars.scrolling = true;
                _this.scrollbars[axis].start = evt[axis];
                _this.scrollbars[axis].drag = true;
                _this.scrollbars[axis].offset = parseInt(_this.scrollbars[axis].handle.style[anchor], 10);
                evt.preventDefault();
            }, false);
            if (axis === "x") {
                _this.scrollbars[axis].track.style["left"] = 0;
                _this.scrollbars[axis].track.style["bottom"] = 0;
                _this.scrollbars[axis].track.style["right"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].track.style["height"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].handle.style["height"] = _this.scrollbars.width + "px";
            }
            else {
                _this.scrollbars[axis].track.style["top"] = 0;
                _this.scrollbars[axis].track.style["right"] = 0;
                _this.scrollbars[axis].track.style["bottom"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].track.style["width"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].handle.style["width"] = _this.scrollbars.width + "px";
            }
            _this.scrollbars[axis].track.appendChild(_this.scrollbars[axis].handle);
            _this.containerEl.appendChild(_this.scrollbars[axis].track);
        });
    };
    Grid.prototype.updateScrollbars = function () {
        var _this = this;
        if (this.touch || this.fluid) {
            return;
        }
        var show = false;
        ["x", "y"].forEach2(function (axis) {
            if (!_this.offsets[axis].maxOffset) {
                _this.scrollbars[axis].track.style.display = "none";
                _this.scrollbars[axis].handle.style.display = "none";
                _this.scrollbars[axis].show = false;
                _this.offsets[axis].index = 0;
                _this.offsets[axis].offset = 0;
                return;
            }
            var dimension = _this.stage[axis].label;
            var canvasSize = _this[dimension];
            _this.scrollbars[axis].show = true;
            _this.scrollbars[axis].handle.style.display = "block";
            var size = canvasSize * (canvasSize / (_this.stage[axis].size * _this.scale));
            _this.scrollbars[axis].handle.style[dimension] = size + "px";
            var offset = _this.offsets[axis].index / _this.offsets[axis].max * (canvasSize - size);
            var anchor = _this.scrollbars[axis].anchor;
            _this.scrollbars[axis].handle.style[anchor] = offset + "px";
            _this.scrollbars[axis].track.style.display = "block";
            _this.scrollbars[axis].track.style[dimension] = _this[dimension] + "px";
        });
    };
    Grid.prototype.createSelector = function () {
        var _this = this;
        this.selection = document.createElement("div");
        this.selection.className = "selection";
        if (!this.touch) {
            this.selection.contentEditable = true;
        }
        this.selection.style.position = "absolute";
        this.selection.style["z-index"] = 20;
        this.selection.style["left"] = 0;
        this.selection.style["top"] = 0;
        this.selection.style["display"] = "none";
        this.selection.addEventListener("paste", function (evt) {
            evt.preventDefault();
        });
        this.historyIndicator = document.createElement("div");
        this.historyIndicator.className = "history";
        this.historyIndicator.style.position = "absolute";
        this.historyIndicator.style["z-index"] = 30;
        this.historyIndicator.style["display"] = "none";
        this.input = document.createElement("input");
        this.input.setAttribute("autocapitalize", "none");
        this.input.style["position"] = "absolute";
        this.input.style["border"] = "0";
        this.input.style["font-size"] = "12px";
        this.input.style["color"] = "black";
        this.input.style["background-color"] = "ivory";
        this.input.style["box-sizing"] = "border-box";
        this.input.style["margin"] = "0";
        this.input.style["padding"] = "0 2px";
        this.input.style["outline"] = "0";
        this.input.style["left"] = "0px";
        this.input.style["right"] = "0px";
        this.input.style["bottom"] = "0px";
        this.input.style["top"] = "0px";
        this.input.style["width"] = "100%";
        this.input.style["text-align"] = "left";
        this.input.style["z-index"] = 10;
        this.input.style["visibility"] = "hidden";
        this.input.style["display"] = "display";
        this.input.style["height"] = "100%";
        this.input.addEventListener("keyup", function (evt) {
            _this.dirty = true;
            _this.emit(_this.ON_CELL_VALUE, { cell: _this.cellSelection.from, value: "" + _this.input.value });
        });
        this.input.addEventListener("touchstart", function (evt) {
        });
        this.input.addEventListener("paste", function (evt) {
            evt.stopPropagation();
            var text = "";
            try {
                text = evt.clipboardData.getData("text/plain");
            }
            catch (exception) { }
            try {
                if (!text) {
                    text = window.clipboardData.getData("Text");
                }
            }
            catch (exception) { }
            _this.dirty = true;
            _this.emit(_this.ON_CELL_VALUE, { cell: _this.cellSelection.from, value: "" + text });
        });
        if (this.touch) {
            this.selection.appendChild(this.input);
        }
        this.containerEl.appendChild(this.selection);
    };
    Grid.prototype.updateSelectorAttributes = function (cell, cellTo, selector, history) {
        if (history === void 0) { history = false; }
        var x = cell.x;
        var y = cell.y;
        var width = cellTo.x + cellTo.width - cell.x;
        var height = cellTo.y + cellTo.height - cell.y;
        if (cellTo.x < x) {
            x = cellTo.x;
            width = cell.x + cell.width - cellTo.x;
        }
        if (cellTo.y < y) {
            y = cellTo.y;
            height = cell.y + cell.height - cellTo.y;
        }
        if (!this.freezeRange.valid || cell.position.col >= this.freezeRange.index.col) {
            x -= this.offsets.x.offset;
        }
        x = x * this.scale / this.ratio;
        if (!this.freezeRange.valid || cell.position.row >= this.freezeRange.index.row) {
            y -= this.offsets.y.offset;
        }
        y = y * this.scale / this.ratio;
        width = (width + 1) * this.scale / this.ratio;
        height = (height + 1) * this.scale / this.ratio;
        if (history) {
            selector.style["left"] = x + width - this.historyIndicatorSize + "px";
            selector.style["background-color"] = cell.history.color;
        }
        else {
            selector.style["left"] = x + "px";
            selector.style["width"] = width + "px";
            selector.style["height"] = height + "px";
        }
        selector.style["top"] = y + "px";
        var show = true;
        if (this.freezeRange.valid) {
            if (cell.position.row > this.freezeRange.index.row - 1 &&
                cell.position.row - this.freezeRange.index.row < this.offsets.y.index) {
                show = false;
            }
            if (cell.position.col > this.freezeRange.index.col - 1 &&
                cell.position.col - this.freezeRange.index.col < this.offsets.x.index) {
                show = false;
            }
        }
        selector.style["display"] = show ? "block" : "none";
    };
    Grid.prototype.hideHistoryIndicator = function () {
        if (this.containerEl.contains(this.historyIndicator)) {
            this.containerEl.removeChild(this.historyIndicator);
        }
    };
    Grid.prototype.updateSelector = function (history) {
        var _this = this;
        if (!history) {
            this.hideHistoryIndicator();
        }
        if (this.cellSelection.on) {
            this.updateSelectorAttributes(this.cellSelection.from, this.cellSelection.to, this.selection);
        }
        this.found.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                if (_this.visible[rowIndex] && _this.visible[rowIndex][colIndex]) {
                    if (!_this.containerEl.contains(col.highlight)) {
                        _this.containerEl.appendChild(col.highlight);
                    }
                    _this.updateSelectorAttributes(col.cell, col.cell, col.highlight);
                }
                else {
                    if (_this.containerEl.contains(col.highlight)) {
                        _this.containerEl.removeChild(col.highlight);
                    }
                }
            });
        });
        this.links.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                if (_this.visible[rowIndex] && _this.visible[rowIndex][colIndex]) {
                    if (!_this.containerEl.contains(col.highlight)) {
                        _this.containerEl.appendChild(col.highlight);
                    }
                    _this.updateSelectorAttributes(col.cell, col.cell, col.highlight);
                }
                else {
                    if (_this.containerEl.contains(col.highlight)) {
                        _this.containerEl.removeChild(col.highlight);
                    }
                }
            });
        });
        this.trackingHighlights.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (highlight, colIndex) {
                if (!highlight || highlight === true)
                    return;
                if (_this.visible[rowIndex] && _this.visible[rowIndex][colIndex]) {
                    _this.updateSelectorAttributes(highlight.cell, highlight.cell, highlight.highlight);
                    if (!_this.containerEl.contains(highlight.highlight)) {
                        _this.containerEl.appendChild(highlight.highlight);
                    }
                    if (!highlight.run) {
                        highlight.flash();
                    }
                    else {
                        highlight.destroy();
                    }
                }
                else {
                    highlight.destroy();
                }
            });
        });
    };
    Grid.prototype.setOffsets = function (deltas, set) {
        var _this = this;
        if (set === void 0) { set = false; }
        ["x", "y"].forEach2(function (axis) {
            var stage = _this[_this.stage[axis].label] * 1 / _this.scale * _this.ratio;
            _this.offsets[axis].max = 0;
            _this.offsets[axis].maxOffset = _this.stage[axis].size - stage;
            if (_this.offsets[axis].maxOffset < 0) {
                _this.offsets[axis].maxOffset = 0;
            }
            _this.stage[axis].increments.forEach2(function (offset, index) {
                if (offset > _this.offsets[axis].maxOffset && !_this.offsets[axis].max) {
                    _this.offsets[axis].max = index;
                }
            });
        });
        if (deltas) {
            for (var axis in deltas) {
                if (!deltas[axis])
                    continue;
                if (set) {
                    var index = deltas[axis];
                    var label = axis === "x" ? "col" : "row";
                    if (this.freezeRange.valid) {
                        index -= this.freezeRange.index[label] + 2;
                    }
                    this.offsets[axis].index = index;
                }
                else {
                    this.offsets[axis].index += deltas[axis];
                }
                if (this.offsets[axis].index < 0) {
                    this.offsets[axis].index = 0;
                }
                if (this.offsets[axis].index >= this.offsets[axis].max) {
                    this.offsets[axis].index = this.offsets[axis].max;
                }
            }
        }
        ["x", "y"].forEach2(function (axis) {
            var offset = _this.stage[axis].increments[_this.offsets[axis].index];
            if (offset > _this.offsets[axis].maxOffset) {
                offset = _this.offsets[axis].maxOffset;
            }
            _this.offsets[axis].offset = offset;
            _this.allowBrowserScroll = _this.offsets[axis].index === _this.offsets[axis].max;
        });
    };
    Grid.prototype.resetStage = function () {
        this.stage = {
            x: {
                label: "width",
                pos: 0,
                increments: [],
                size: 0,
                stop: false
            },
            y: {
                label: "height",
                pos: 0,
                increments: [],
                size: 0,
                stop: false
            }
        };
    };
    Grid.prototype.setSize = function () {
        var rect = (this.containerRect = this.containerEl.getBoundingClientRect());
        if (!rect.width) {
            rect.width = this.stage.x.size;
            rect.height = this.stage.y.size;
        }
        var scrollbarWidth = (this.offsets.x.maxOffset || this.offsets.y.maxOffset) && !this.touch ? this.scrollbars.width : 0;
        var scrollbarHeight = (this.offsets.x.maxOffset || this.offsets.y.maxOffset) && !this.touch ? this.scrollbars.width : 0;
        var stageWidth = this.stage.x.size * this.scale / this.ratio;
        var stageHeight = this.stage.y.size * this.scale / this.ratio;
        if (rect.width > stageWidth || this.fluid || this.scrollbars.inset) {
            scrollbarHeight = 0;
        }
        if (rect.height > stageHeight || this.fluid || this.scrollbars.inset) {
            scrollbarWidth = 0;
        }
        this.width = this.fluid ? this.stage.x.size : rect.width - scrollbarWidth;
        this.height = this.fluid ? this.stage.y.size : rect.height - scrollbarHeight;
        this.gc.buffer.width = (this.fluid ? this.stage.x.size : rect.width - scrollbarWidth) * this.ratio;
        this.gc.buffer.height = (this.fluid ? this.stage.y.size : rect.height - scrollbarHeight) * this.ratio;
        this.gc.canvas.width = (this.fluid ? this.stage.x.size : rect.width - scrollbarWidth) * this.ratio;
        this.gc.canvas.height = (this.fluid ? this.stage.y.size : rect.height - scrollbarHeight) * this.ratio;
        this.gc.canvas.style.width = this.fluid
            ? this.stage.x.size + "px"
            : Math.floor(rect.width - scrollbarWidth) + "px";
        this.gc.canvas.style.height = this.fluid
            ? this.stage.y.size + "px"
            : Math.floor(rect.height - scrollbarHeight) + "px";
    };
    Grid.prototype.getTextOverlayWidth = function (row, col, width, textWidth) {
        var cell = this.originalContent[row][col];
        var textAlign = cell.style["text-align"];
        var left = 0;
        var right = 0;
        switch (textAlign) {
            case "right":
                left = -1;
                break;
            case "center":
                left = -1;
                right = 1;
                break;
            default:
                right = 1;
                break;
        }
        if (!this.content[row][col + right] || !this.content[row][col + left]) {
            return {
                col: col,
                width: width
            };
        }
        var endCol = col;
        var startCol = col;
        if (right > 0) {
            for (var i = col + 1; i < this.content[row].length; i++) {
                if (this.content[row][i].formatted_value ||
                    "" + this.content[row][i].value ||
                    this.content[row][i].allow === "no") {
                    break;
                }
                width += this.content[row][i].width;
                endCol = i;
                this.content[row][i].hidden = true;
                if (width > textWidth) {
                    break;
                }
            }
        }
        if (left < 0) {
            for (var i = col - 1; i >= 0; i--) {
                if (this.content[row][i].formatted_value ||
                    "" + this.content[row][i].value ||
                    this.content[row][i].allow === "no") {
                    break;
                }
                width += this.content[row][i].width;
                startCol = i;
                this.content[row][i].hidden = true;
                if (width > textWidth) {
                    break;
                }
            }
        }
        return {
            left: left,
            right: right,
            endCol: endCol,
            startCol: startCol,
            width: width
        };
    };
    Grid.prototype.softMerges = function (dataRow, dataRowIndex) {
        var _this = this;
        if (dataRow === void 0) { dataRow = []; }
        if (dataRowIndex === void 0) { dataRowIndex = -1; }
        dataRow.forEach2(function (col, colIndex) {
            var cell = _this.originalContent[dataRowIndex][colIndex];
            var width = parseFloat(cell.style.width);
            col.overlayWidth = width;
            col.overlayEnd = colIndex;
            col.overlayStart = colIndex;
            if ((cell.formatted_value || cell.value) && !col.wrap) {
                _this.gc.bufferContext.font = _this.getFontString(cell);
                var textWidth = _this.gc.bufferContext.measureText(_this.cleanValue(cell.formatted_value || cell.value)).width + 3;
                if (textWidth > width) {
                    var overlay = _this.getTextOverlayWidth(dataRowIndex, colIndex, width, textWidth);
                    col.overlayWidth = overlay.width;
                    col.overlayEnd = overlay.endCol;
                    col.overlayStart = overlay.startCol;
                }
            }
        });
    };
    Grid.prototype.getFontString = function (col) {
        return col.style["font-style"] + " " + col.style["font-weight"] + " " + col.style["font-size"] + " \"" + col.style["font-family"] + "\", sans-serif";
    };
    Grid.prototype.flattenStyles = function (styles) {
        var htmlStyle = {};
        this.cellStyles.forEach(function (s) {
            htmlStyle[s] = styles[s];
        });
        for (var key in this.borders.names) {
            var name_1 = this.borders.names[key];
            var width = this.borders.widths[styles[key + "bw"]] + "px";
            var style = styles[key + "bs"];
            var color = "#" + styles[key + "bc"].replace("#", "");
            htmlStyle["border-" + name_1] = width + " " + style + " " + color;
        }
        var str = "";
        for (var attr in htmlStyle) {
            var value = htmlStyle[attr];
            if ((attr === "background-color" || attr === "color") && value.indexOf("#") === -1) {
                value = "#" + value;
            }
            str += attr + ": " + value + "; ";
        }
        return str;
    };
    Grid.prototype.getBackgroundColor = function (originalCell, cell) {
        var color = "" + originalCell.style["background-color"] || "white";
        var hexColor = "#" + color.replace("#", "");
        if (this.helpers.validHex(hexColor)) {
            color = hexColor;
        }
        else if (color === "none") {
            color = "white";
        }
        if (cell.formatting && cell.formatting.background) {
            color = cell.formatting.background;
        }
        return color;
    };
    Grid.prototype.drawGraphics = function (cell, coords, col) {
        var _this = this;
        if (("" + col.value).indexOf("data:image") === 0) {
            var image_1 = new Image();
            image_1.onload = function () {
                _this.gc.canvasContext.drawImage(image_1, coords.x, coords.y, coords.width * _this.scale, coords.height * _this.scale);
            };
            image_1.src = col.value;
        }
        else if (!col.hidden) {
            this.drawRect(cell, { x: coords.x, y: coords.y, width: coords.width, height: coords.height }, col);
            this.drawText(cell, { x: coords.x, y: coords.y, width: coords.width, height: coords.height }, col);
            if (col.allow === "no") {
                var color = this.getBackgroundColor(cell, col);
                var contrast = this.helpers.getContrastYIQ(color);
                for (var img in this.noAccessImages) {
                    if (contrast !== img || !this.noAccessImages[img]) {
                        continue;
                    }
                    var pat = this.gc.bufferContext.createPattern(document.getElementById(this.noAccessImages[img]), "repeat");
                    this.gc.bufferContext.beginPath();
                    this.gc.bufferContext.rect(coords.x, coords.y, coords.width, coords.height);
                    this.gc.bufferContext.fillStyle = pat;
                    this.gc.bufferContext.fill();
                }
            }
        }
        if (cell.style.tbs !== "none") {
            this.gridLines.push([
                { x: coords._x - this.borders.widths[cell.style.tbw] / 2, y: coords.y },
                { x: coords._a, y: coords.y },
                { width: this.borders.widths[cell.style.tbw], color: cell.style.tbc }
            ]);
        }
        if (cell.style.rbs !== "none" && coords.a === coords._a && !col.hidden) {
            this.gridLines.push([
                { x: coords._a, y: coords.y },
                { x: coords._a, y: coords.b },
                { width: this.borders.widths[cell.style.rbw], color: cell.style.rbc }
            ]);
        }
        if (cell.style.bbs !== "none") {
            this.gridLines.push([
                { x: coords._x, y: coords.b },
                { x: coords._a, y: coords.b },
                { width: this.borders.widths[cell.style.bbw], color: cell.style.bbc }
            ]);
        }
        if (cell.style.lbs !== "none" && coords.x === coords._x && !col.hidden) {
            this.gridLines.push([
                { x: coords.x, y: coords.y },
                { x: coords.x, y: coords.b },
                { width: this.borders.widths[cell.style.lbw], color: cell.style.lbc }
            ]);
        }
    };
    Grid.prototype.drawLine = function (start, end, options) {
        var t = this.translateCanvas(options.width || 1);
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.lineWidth = options.width || 1;
        this.gc.bufferContext.strokeStyle = "#" + (options.color || "000000").replace("#", "");
        this.gc.bufferContext.moveTo(start.x, start.y);
        this.gc.bufferContext.lineTo(end.x, end.y);
        this.gc.bufferContext.stroke();
        this.resetCanvas(t);
    };
    Grid.prototype.drawTrackingRect = function (coords, cell) {
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.strokeStyle = this.userTrackingColors[this.history[cell.position.row][cell.position.col]];
        this.gc.bufferContext.lineWidth = 2;
        this.gc.bufferContext.rect(coords.x + 2, coords.y + 2, coords.width - 3, coords.height - 3);
        this.gc.bufferContext.stroke();
    };
    Grid.prototype.drawSortingIndicator = function (cell, direction) {
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.fillStyle = "red";
        var x = cell.x + cell.width - 10 - this.offsets.x.offset;
        if (direction === "DESC") {
            var y = cell.y;
            this.gc.bufferContext.moveTo(x, y);
            this.gc.bufferContext.lineTo(x + 10, y);
            this.gc.bufferContext.lineTo(x + 5, y + 5);
            this.gc.bufferContext.lineTo(x, y);
        }
        else {
            var y = cell.y + cell.height - 5;
            this.gc.bufferContext.moveTo(x + 5, y);
            this.gc.bufferContext.lineTo(x + 10, y + 5);
            this.gc.bufferContext.lineTo(x, y + 5);
            this.gc.bufferContext.lineTo(x + 5, y);
        }
        this.gc.bufferContext.fill();
    };
    Grid.prototype.drawOffsetRect = function (originalCell, coords, cell) {
        var color = this.getBackgroundColor(originalCell, cell);
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.fillStyle = color;
        this.gc.bufferContext.fillRect(coords.x - 0.5, coords.y - 0.5, coords.width + 0.5, coords.height + 0.5);
    };
    Grid.prototype.drawRect = function (originalCell, coords, cell) {
        var color = this.getBackgroundColor(originalCell, cell);
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.fillStyle = color;
        this.gc.bufferContext.fillRect(coords.x, coords.y, coords.width, coords.height);
    };
    Grid.prototype.cleanValue = function (value) {
        return (value + "").trim().replace(/\s\s+/g, " ");
    };
    Grid.prototype.drawText = function (originalCell, coords, cell) {
        var _this = this;
        if (cell.allow === "no") {
            return;
        }
        var v = this.cleanValue(originalCell.formatted_value || originalCell.value);
        var color = "#" + originalCell.style["color"].replace("#", "");
        if (cell.formatting && cell.formatting.color) {
            color = cell.formatting.color;
        }
        this.gc.bufferContext.fillStyle = color;
        this.gc.bufferContext.font = this.getFontString(originalCell);
        this.gc.bufferContext.textAlign = originalCell.style["text-align"]
            .replace("justify", "left")
            .replace("start", "left");
        var x = 0;
        var xPad = 3;
        var y = 0;
        var yPad = 5;
        switch (originalCell.style["text-align"]) {
            case "right":
                x = coords.x + coords.width - xPad;
                break;
            case "middle":
            case "center":
                x = coords.x + coords.width / 2;
                break;
            default:
                x = coords.x + xPad;
                break;
        }
        var wrapOffset = 0;
        var fontHeight = this.gc.bufferContext.measureText("M").width;
        var lines = [];
        if (cell.wrap) {
            lines = this.findLines(v, fontHeight, coords.width - xPad * 2);
            wrapOffset = lines.offset;
        }
        switch (originalCell.style["vertical-align"]) {
            case "top":
                y = coords.y + fontHeight + yPad;
                break;
            case "center":
            case "middle":
                y = coords.y + coords.height / 2 - wrapOffset / 2 + fontHeight / 2;
                break;
            default:
                y = coords.y + coords.height - yPad - wrapOffset;
                break;
        }
        this.gc.bufferContext.save();
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.rect(coords.x, coords.y, coords.width, coords.height);
        this.gc.bufferContext.clip();
        if (cell.wrap) {
            lines.lines.forEach2(function (line) {
                _this.gc.bufferContext.fillText(line.value, x, y + line.y);
                if (originalCell.link) {
                    var yLine = Math.round(y + 2 + line.y);
                    var tWidth = Math.round(_this.gc.bufferContext.measureText(line.value).width);
                    var xLine = x;
                    switch (originalCell.style["text-align"]) {
                        case "right":
                            xLine = x - tWidth;
                            break;
                        case "middle":
                        case "center":
                            xLine = x - tWidth / 2;
                            break;
                        default:
                            break;
                    }
                    _this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
                }
            });
        }
        else {
            if (originalCell.style["number-format"] &&
                originalCell.style["number-format"].indexOf("0.00") > -1 &&
                /(¥|€|£|\$)/.test(v)) {
                var num = v.split(" ");
                this.gc.bufferContext.fillText(num.length > 1 ? num[1] : v, x, y);
                if (num.length > 1) {
                    this.gc.bufferContext.textAlign = "left";
                    this.gc.bufferContext.fillText(num[0], coords.x + xPad, y);
                }
            }
            else {
                this.gc.bufferContext.fillText(v, x, y);
            }
            if (originalCell.link) {
                var yLine = Math.round(y + 2);
                var tWidth = Math.round(this.gc.bufferContext.measureText(v).width);
                var xLine = x;
                switch (originalCell.style["text-align"]) {
                    case "right":
                        xLine = x - tWidth;
                        break;
                    case "middle":
                    case "center":
                        xLine = x - tWidth / 2;
                        break;
                    default:
                        break;
                }
                this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
            }
        }
        this.gc.bufferContext.restore();
    };
    Grid.prototype.findLines = function (str, fontHeight, cellWidth) {
        var words = str.split(" ");
        var lines = [];
        var word = [];
        var lineY = 0;
        var lineOffset = fontHeight + 4;
        for (var w = 0; w < words.length; w++) {
            var wordWidth = this.gc.bufferContext.measureText(words[w]).width + 2;
            if (wordWidth > cellWidth) {
                var letters = words[w].split("");
                var width_1 = 0;
                wordWidth = 0;
                word = [];
                for (var i = 0; i < letters.length; i++) {
                    width_1 = this.gc.bufferContext.measureText(word.join("")).width + 2;
                    if (width_1 < cellWidth) {
                        word.push(letters[i]);
                        wordWidth = width_1;
                    }
                    else {
                        lines.push({
                            value: word.join(""),
                            width: wordWidth
                        });
                        word = [letters[i]];
                    }
                }
                if (word.length) {
                    lines.push({
                        value: word.join(""),
                        width: wordWidth
                    });
                }
            }
            else {
                lines.push({
                    value: words[w],
                    width: wordWidth
                });
            }
        }
        var rows = [];
        var width = 0;
        var lineWords = [];
        for (var i = 0; i < lines.length; i++) {
            width += lines[i].width;
            if (width < cellWidth) {
                lineWords.push(lines[i].value);
            }
            else {
                rows.push({
                    value: lineWords.join(" "),
                    y: lineY
                });
                lineY += lineOffset;
                lineWords = [lines[i].value];
                width = lines[i].width;
            }
        }
        if (lineWords.length) {
            rows.push({
                value: lineWords.join(" "),
                y: lineY
            });
        }
        return {
            lines: rows,
            offset: fontHeight * (rows.length - 1) + (rows.length - 1) * 4
        };
    };
    Grid.prototype.translateCanvas = function (w) {
        var translate = (w % 2) / 2;
        this.gc.bufferContext.translate(translate, translate);
        return translate;
    };
    Grid.prototype.resetCanvas = function (translate) {
        this.gc.bufferContext.translate(-translate, -translate);
    };
    return Grid;
}(Emitter_1.default));
exports.Grid = Grid;
var GridTrackingHighlight = (function () {
    function GridTrackingHighlight(cell, highlight) {
        this.cell = cell;
        this.highlight = highlight;
        this.run = false;
        this.lifetime = 3000;
        this.done = false;
        this.classy = new Classy_1.default();
    }
    GridTrackingHighlight.prototype.flash = function () {
        var _this = this;
        this.run = true;
        this.interval = setInterval(function () {
            if (!_this.highlight.parentNode) {
                return;
            }
            _this.classy.addClass(_this.highlight, "flash");
            clearInterval(_this.interval);
            setTimeout(function () {
                _this.done = true;
                _this.classy.addClass(_this.highlight, "done");
            }, _this.lifetime);
        }, 50);
    };
    GridTrackingHighlight.prototype.destroy = function () {
        if (!this.done || !this.highlight.parentNode) {
            return;
        }
        this.highlight.parentNode.removeChild(this.highlight);
    };
    return GridTrackingHighlight;
}());
var GridWrap = (function () {
    function GridWrap() {
        return Grid;
    }
    GridWrap.$inject = [];
    return GridWrap;
}());
exports.GridWrap = GridWrap;
//# sourceMappingURL=Grid.js.map

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

(function(global) {
  'use strict';

  var dateFormat = (function() {
      var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZWN]|"[^"]*"|'[^']*'/g;
      var timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g;
      var timezoneClip = /[^-+\dA-Z]/g;
  
      // Regexes and supporting functions are cached through closure
      return function (date, mask, utc, gmt) {
  
        // You can't provide utc if you skip other args (use the 'UTC:' mask prefix)
        if (arguments.length === 1 && kindOf(date) === 'string' && !/\d/.test(date)) {
          mask = date;
          date = undefined;
        }
  
        date = date || new Date;
  
        if(!(date instanceof Date)) {
          date = new Date(date);
        }
  
        if (isNaN(date)) {
          throw TypeError('Invalid date');
        }
  
        mask = String(dateFormat.masks[mask] || mask || dateFormat.masks['default']);
  
        // Allow setting the utc/gmt argument via the mask
        var maskSlice = mask.slice(0, 4);
        if (maskSlice === 'UTC:' || maskSlice === 'GMT:') {
          mask = mask.slice(4);
          utc = true;
          if (maskSlice === 'GMT:') {
            gmt = true;
          }
        }
  
        var _ = utc ? 'getUTC' : 'get';
        var d = date[_ + 'Date']();
        var D = date[_ + 'Day']();
        var m = date[_ + 'Month']();
        var y = date[_ + 'FullYear']();
        var H = date[_ + 'Hours']();
        var M = date[_ + 'Minutes']();
        var s = date[_ + 'Seconds']();
        var L = date[_ + 'Milliseconds']();
        var o = utc ? 0 : date.getTimezoneOffset();
        var W = getWeek(date);
        var N = getDayOfWeek(date);
        var flags = {
          d:    d,
          dd:   pad(d),
          ddd:  dateFormat.i18n.dayNames[D],
          dddd: dateFormat.i18n.dayNames[D + 7],
          m:    m + 1,
          mm:   pad(m + 1),
          mmm:  dateFormat.i18n.monthNames[m],
          mmmm: dateFormat.i18n.monthNames[m + 12],
          yy:   String(y).slice(2),
          yyyy: y,
          h:    H % 12 || 12,
          hh:   pad(H % 12 || 12),
          H:    H,
          HH:   pad(H),
          M:    M,
          MM:   pad(M),
          s:    s,
          ss:   pad(s),
          l:    pad(L, 3),
          L:    pad(Math.round(L / 10)),
          t:    H < 12 ? dateFormat.i18n.timeNames[0] : dateFormat.i18n.timeNames[1],
          tt:   H < 12 ? dateFormat.i18n.timeNames[2] : dateFormat.i18n.timeNames[3],
          T:    H < 12 ? dateFormat.i18n.timeNames[4] : dateFormat.i18n.timeNames[5],
          TT:   H < 12 ? dateFormat.i18n.timeNames[6] : dateFormat.i18n.timeNames[7],
          Z:    gmt ? 'GMT' : utc ? 'UTC' : (String(date).match(timezone) || ['']).pop().replace(timezoneClip, ''),
          o:    (o > 0 ? '-' : '+') + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
          S:    ['th', 'st', 'nd', 'rd'][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10],
          W:    W,
          N:    N
        };
  
        return mask.replace(token, function (match) {
          if (match in flags) {
            return flags[match];
          }
          return match.slice(1, match.length - 1);
        });
      };
    })();

  dateFormat.masks = {
    'default':               'ddd mmm dd yyyy HH:MM:ss',
    'shortDate':             'm/d/yy',
    'mediumDate':            'mmm d, yyyy',
    'longDate':              'mmmm d, yyyy',
    'fullDate':              'dddd, mmmm d, yyyy',
    'shortTime':             'h:MM TT',
    'mediumTime':            'h:MM:ss TT',
    'longTime':              'h:MM:ss TT Z',
    'isoDate':               'yyyy-mm-dd',
    'isoTime':               'HH:MM:ss',
    'isoDateTime':           'yyyy-mm-dd\'T\'HH:MM:sso',
    'isoUtcDateTime':        'UTC:yyyy-mm-dd\'T\'HH:MM:ss\'Z\'',
    'expiresHeaderFormat':   'ddd, dd mmm yyyy HH:MM:ss Z'
  };

  // Internationalization strings
  dateFormat.i18n = {
    dayNames: [
      'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat',
      'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
    ],
    monthNames: [
      'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
      'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
    ],
    timeNames: [
      'a', 'p', 'am', 'pm', 'A', 'P', 'AM', 'PM'
    ]
  };

function pad(val, len) {
  val = String(val);
  len = len || 2;
  while (val.length < len) {
    val = '0' + val;
  }
  return val;
}

/**
 * Get the ISO 8601 week number
 * Based on comments from
 * http://techblog.procurios.nl/k/n618/news/view/33796/14863/Calculate-ISO-8601-week-and-year-in-javascript.html
 *
 * @param  {Object} `date`
 * @return {Number}
 */
function getWeek(date) {
  // Remove time components of date
  var targetThursday = new Date(date.getFullYear(), date.getMonth(), date.getDate());

  // Change date to Thursday same week
  targetThursday.setDate(targetThursday.getDate() - ((targetThursday.getDay() + 6) % 7) + 3);

  // Take January 4th as it is always in week 1 (see ISO 8601)
  var firstThursday = new Date(targetThursday.getFullYear(), 0, 4);

  // Change date to Thursday same week
  firstThursday.setDate(firstThursday.getDate() - ((firstThursday.getDay() + 6) % 7) + 3);

  // Check if daylight-saving-time-switch occurred and correct for it
  var ds = targetThursday.getTimezoneOffset() - firstThursday.getTimezoneOffset();
  targetThursday.setHours(targetThursday.getHours() - ds);

  // Number of weeks between target Thursday and first Thursday
  var weekDiff = (targetThursday - firstThursday) / (86400000*7);
  return 1 + Math.floor(weekDiff);
}

/**
 * Get ISO-8601 numeric representation of the day of the week
 * 1 (for Monday) through 7 (for Sunday)
 * 
 * @param  {Object} `date`
 * @return {Number}
 */
function getDayOfWeek(date) {
  var dow = date.getDay();
  if(dow === 0) {
    dow = 7;
  }
  return dow;
}

/**
 * kind-of shortcut
 * @param  {*} val
 * @return {String}
 */
function kindOf(val) {
  if (val === null) {
    return 'null';
  }

  if (val === undefined) {
    return 'undefined';
  }

  if (typeof val !== 'object') {
    return typeof val;
  }

  if (Array.isArray(val)) {
    return 'array';
  }

  return {}.toString.call(val)
    .slice(8, -1).toLowerCase();
};



  if (true) {
    !(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
      return dateFormat;
    }).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else if (typeof exports === 'object') {
    module.exports = dateFormat;
  } else {
    global.dateFormat = dateFormat;
  }
})(this);


/***/ }),
/* 23 */
/***/ (function(module, exports) {

/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {/* globals __webpack_amd_options__ */
module.exports = __webpack_amd_options__;

/* WEBPACK VAR INJECTION */}.call(exports, {}))

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = __webpack_require__(1);
var Helpers_1 = __webpack_require__(5);
var merge = __webpack_require__(2);
var Clipboard = (function (_super) {
    __extends(Clipboard, _super);
    function Clipboard(element, doc) {
        var _this = _super.call(this) || this;
        _this.editableAreaId = 'pages-editable';
        _this.focus = false;
        _this.clipboardTextPlain = '';
        _this.key = '';
        _this.doc = false;
        _this.validStyles = [
            'color',
            'background',
            'background-color',
            'border',
            'border-left',
            'border-right',
            'border-top',
            'border-bottom',
            'width',
            'height',
            'text-align',
            'vertical-align',
            'font-family',
            'font-size',
            'font-weight',
            'font-style',
            'text-wrap',
            'text-decoration',
            'number-format',
            'word-wrap',
            'white-space',
        ];
        _this.excelStyles = ['tb', 'rb', 'bb', 'lb'];
        _this.excelBorderStyles = {
            solid: 'solid',
            dashed: 'dashed',
            dashdotdot: 'dotted',
            double: 'double',
        };
        _this.excelBorderWeights = {
            '1px': 'thin',
            '2px': 'medium',
            '3px': 'thick',
        };
        _this.borderSides = ['top', 'right', 'bottom', 'left'];
        _this.borderSyles = ['width', 'style', 'color'];
        _this.onEditFocus = function (e) {
            _this.focus = true;
        };
        _this.onEditBlur = function (e) {
            _this.focus = false;
        };
        _this.onPasteDocument = function (e) {
            if (_this.focus) {
                return;
            }
            _this.emit(_this.ON_PASTED);
            var clipboard = _this.getClipboardText(e);
            _this.clipboardTextPlain = clipboard.text;
            var div = _this.createPastedElement(clipboard.html || clipboard.text);
            _this.getHtml(div);
            console.log('onPasteDocument', clipboard);
        };
        _this.onPaste = function (e) {
            console.log('onPaste', e);
            _this.emit(_this.ON_PASTED);
            var clipboard = _this.getClipboardText(e);
            _this.clipboardTextPlain = clipboard.text;
            try {
                var div = _this.createPastedElement(clipboard.html);
                _this.getHtml(div);
                e.preventDefault();
            }
            catch (exception) {
                _this.getHtml(e.target);
            }
            console.log('onPaste', clipboard);
        };
        _this.onKeyDown = function (e) {
            _this.key = e.key;
            if (_this.key === 'Control' && e.key === 'v') {
                _this.emit(_this.ON_PASTED);
            }
            else {
            }
        };
        _this.utils = new Helpers_1.default();
        _this.init(element, doc);
        return _this;
    }
    Object.defineProperty(Clipboard.prototype, "ON_DATA", {
        get: function () {
            return 'data';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Clipboard.prototype, "ON_PASTED", {
        get: function () {
            return 'pasted';
        },
        enumerable: true,
        configurable: true
    });
    Clipboard.prototype.init = function (element, doc) {
        this.destroy();
        if (typeof element === 'string') {
            this.editableAreaEl = document.getElementById(element);
        }
        else {
            this.editableAreaEl = element;
        }
        if (this.editableAreaEl) {
            this.utils.addEvent(this.editableAreaEl, 'paste', this.onPaste);
            this.utils.addEvent(this.editableAreaEl, 'focus', this.onEditFocus);
            this.utils.addEvent(this.editableAreaEl, 'blur', this.onEditBlur);
            this.utils.addEvent(this.editableAreaEl, 'keypress', this.onKeyDown);
            this.utils.addEvent(this.editableAreaEl, 'drop', this.onPaste);
            this.editableAreaEl.focus();
        }
        if (doc) {
            this.utils.addEvent(document, 'paste', this.onPasteDocument);
            this.doc = true;
        }
        else {
            this.doc = false;
        }
        return true;
    };
    Clipboard.prototype.destroy = function () {
        this.focus = false;
        if (this.editableAreaEl) {
            this.utils.removeEvent(this.editableAreaEl, 'paste', this.onPaste);
            this.utils.removeEvent(this.editableAreaEl, 'focus', this.onEditFocus);
            this.utils.removeEvent(this.editableAreaEl, 'blur', this.onEditBlur);
            this.utils.removeEvent(this.editableAreaEl, 'keypress', this.onKeyDown);
            this.utils.removeEvent(this.editableAreaEl, 'drop', this.onPaste);
        }
        if (this.doc) {
            this.utils.removeEvent(document, 'paste', this.onPasteDocument);
        }
    };
    Clipboard.prototype.copyTextToClipboard = function (text) {
        var div = document.createElement('div');
        div.style.position = 'fixed';
        div.style.bottom = '100vh';
        div.style.right = '100vw';
        div.innerHTML = text;
        document.body.appendChild(div);
        this.selectElContents(div.getElementsByTagName('table')[0]);
        var success = true;
        try {
            success = document.execCommand('copy');
        }
        catch (err) {
            success = false;
        }
        document.body.removeChild(div);
        return success;
    };
    Clipboard.prototype.selectElContents = function (el) {
        var body = document.body, range, sel;
        if (document.createRange && window.getSelection) {
            range = document.createRange();
            sel = window.getSelection();
            sel.removeAllRanges();
            try {
                range.selectNodeContents(el);
                sel.addRange(range);
            }
            catch (e) {
                range.selectNode(el);
                sel.addRange(range);
            }
        }
        else if (body.createTextRange) {
            range = body.createTextRange();
            range.moveToElementText(el);
            range.select();
        }
    };
    Clipboard.prototype.getClipboardText = function (e) {
        var clipboard = {
            html: '',
            text: '',
        };
        try {
            clipboard.text = e.clipboardData.getData('text/plain');
            clipboard.html = e.clipboardData.getData('text/html');
        }
        catch (exception) { }
        try {
            if (!clipboard.text) {
                clipboard.text = window.clipboardData.getData('Text');
            }
        }
        catch (exception) { }
        return clipboard;
    };
    Clipboard.prototype.createPastedElement = function (html) {
        var div = document.getElementById('pasted') || document.createElement('div');
        div.id = 'pasted';
        div.innerHTML = html;
        div.style.left = '-10000px';
        div.style.top = '-10000px';
        div.style.position = 'absolute';
        div.style['z-index'] = -1;
        document.getElementsByTagName('body')[0].appendChild(div);
        return div;
    };
    Clipboard.prototype.getHtml = function (el) {
        var _this = this;
        setTimeout(function () {
            var tables = el.getElementsByTagName('table');
            if (!tables.length) {
                el.innerHTML = '';
                if (!_this.clipboardTextPlain) {
                    _this.emit(_this.ON_DATA, false);
                }
                else {
                    console.log(_this.clipboardTextPlain);
                    var data = _this.parseText(_this.clipboardTextPlain);
                    _this.emit(_this.ON_DATA, data);
                }
                return;
            }
            _this.parseTable(tables[0]);
            el.style.visibility = 'hidden';
        }, 10);
    };
    Clipboard.prototype.parseText = function (text) {
        var _this = this;
        var data = [];
        var lines;
        var colWidths = [];
        var numOfCols = 0;
        var rows = [];
        try {
            var jsonData = JSON.parse(text);
            if (!jsonData.length) {
                throw 'Not an array';
            }
            var firstRow = jsonData[0];
            var cells_1 = [];
            Object.keys(firstRow).forEach(function (key) {
                if (typeof key !== 'string' && typeof key !== 'number') {
                    key = JSON.stringify(key);
                }
                cells_1.push(key);
            });
            numOfCols = cells_1.length;
            rows.push(cells_1);
            jsonData.forEach(function (row) {
                var cells = [];
                Object.keys(row).forEach(function (key, c) {
                    var value = row[key];
                    if (typeof value !== 'string' && typeof value !== 'number') {
                        value = JSON.stringify(value);
                    }
                    cells.push(value);
                    var width = Math.ceil(_this.getTextWidth(row[key], 'normal 12pt Arial'));
                    if (!colWidths[c] || colWidths[c] < width) {
                        colWidths[c] = width;
                    }
                });
                rows.push(cells);
            });
        }
        catch (e) {
            lines = text.split('\n');
            var tabs = lines[0].match(/\t/gi) ? lines[0].match(/\t/gi).length : 0;
            var commas = lines[0].match(/,/gi) ? lines[0].match(/,/gi).length : 0;
            for (var i = 0; i < lines.length; i++) {
                var line = lines[i];
                var cells = void 0;
                if (tabs >= commas) {
                    cells = line.split(/\t(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/);
                }
                else {
                    cells = line.split(/,(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/);
                }
                if (cells.length < 2) {
                    line = line.replace(/( {2,9})/gi, ',');
                    cells = line.split(/,(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/);
                }
                rows[i] = cells;
                if (cells.length > numOfCols) {
                    numOfCols = cells.length;
                }
                for (var c = 0; c < cells.length; c++) {
                    var width = Math.ceil(this.getTextWidth(cells[c], 'normal 12pt Arial'));
                    if (!colWidths[c] || colWidths[c] < width) {
                        colWidths[c] = width;
                    }
                }
            }
        }
        for (var i = 0; i < rows.length; i++) {
            var cells = rows[i];
            var cols = [];
            for (var c = 0; c < numOfCols; c++) {
                var value = cells[c] ? cells[c].replace(/['"]+/g, '') : '';
                var textAlign = /^\d+$/.test(value) ? 'right' : 'left';
                cols.push({
                    style: {
                        color: '000000',
                        'background-color': 'FFFFFF',
                        lbs: 'solid',
                        lbw: 'thin',
                        rbw: 'thin',
                        bbs: 'solid',
                        tbs: 'solid',
                        rbs: 'solid',
                        tbw: 'thin',
                        lbc: 'EFEFEF',
                        bbc: 'EFEFEF',
                        bbw: 'thin',
                        tbc: 'EFEFEF',
                        rbc: 'EFEFEF',
                        'font-weight': 'normal',
                        'font-style': 'normal',
                        'font-size': '11pt',
                        'font-family': 'Calibri',
                        'text-align': textAlign,
                        width: colWidths[c] + "px",
                        height: "20px",
                    },
                    formatted_value: value,
                    value: value,
                    index: {
                        row: i,
                        col: c,
                    },
                });
            }
            data.push(cols);
        }
        return data;
    };
    Clipboard.prototype.parseTable = function (table) {
        var data = this.collectClipData(table);
        this.emit(this.ON_DATA, data);
    };
    Clipboard.prototype.collectClipData = function (table) {
        var data = [];
        var colWidths = this.tableColWidths(table);
        console.log(colWidths);
        var rows = table.rows;
        var maxColumns = 0;
        var rowCount = 0;
        var rowSpans = [];
        for (var i = 0; i < rows.length; i++) {
            var cells = rows[i].cells;
            if (!cells.length) {
                continue;
            }
            if (cells.length > maxColumns) {
                maxColumns = cells.length;
                if (colWidths && colWidths.length > maxColumns) {
                    maxColumns = colWidths.length;
                }
            }
            data.push([]);
            var cellCount = 0;
            for (var j = 0; j < cells.length; j++) {
                var rowSpan = cells[j].rowSpan;
                var colSpan = cells[j].colSpan;
                var links = cells[j].getElementsByTagName('a');
                if (rowSpans[j] && rowSpans[j][rowCount]) {
                    data[rowCount].push(rowSpans[j][rowCount]);
                }
                var val = cells[j].textContent;
                if (links && links.length) {
                    val = links[0].textContent;
                }
                val = val
                    .replace(/&amp;/g, '&')
                    .replace(/&lt;/g, '<')
                    .replace(/&nbsp;/g, ' ')
                    .trim();
                var cellData = {
                    index: {
                        row: i,
                        col: j,
                    },
                    value: this.getRawValue(val, cells[j].dataset.format),
                    formatted_value: val,
                };
                if (links && links.length) {
                    cellData.link = {
                        external: true,
                        address: links[0].href,
                    };
                }
                cellData.style = this.cssToStyles(window.getComputedStyle(cells[j]).cssText);
                if (cells[j].dataset.format) {
                    cellData.style['number-format'] = cells[j].dataset.format;
                }
                if (colWidths && colWidths[j]) {
                    cellData.style.width = colWidths[cellCount] + "px";
                }
                else {
                    if (cellData.style.width === '0px') {
                        cellData.style.width = '80px';
                    }
                }
                cellCount++;
                var color = cellData.style.color;
                if (color && color.indexOf('#') === -1 && color.indexOf('rgb') === -1) {
                    color = "#" + color;
                }
                if (!this.utils.validHex(color)) {
                    cellData.style.color = '000000';
                }
                data[rowCount].push(cellData);
                if (colSpan > 1) {
                    var copyCellData = merge.recursive(true, {}, cellData);
                    copyCellData.value = '';
                    copyCellData.formatted_value = '';
                    for (var k = 1; k < colSpan; k++) {
                        if (colWidths && colWidths[cellCount + k]) {
                            copyCellData.style.width = colWidths[cellCount + k] + "px";
                        }
                        data[rowCount].push(copyCellData);
                        cellCount++;
                    }
                }
                if (rowSpan > 1) {
                    rowSpans[j] = [];
                    for (var s = 1; s < rowSpan; s++) {
                        var copyCellData = merge.recursive(true, {}, cellData);
                        copyCellData.value = '';
                        copyCellData.formatted_value = '';
                        copyCellData.style.height = 'auto';
                        rowSpans[j][rowCount + s] = merge.recursive(true, {}, copyCellData);
                    }
                }
            }
            rowCount++;
        }
        for (var i = 0; i < data.length; i++) {
            if (data[i].length >= maxColumns) {
                continue;
            }
            var cell = data[i][data[i].length - 1];
            cell.value = '';
            cell.formatted_value = '';
            for (var k = data[i].length; k < maxColumns; k++) {
                data[i].push(cell);
            }
        }
        console.log(JSON.stringify(data));
        return data;
    };
    Clipboard.prototype.tableColWidths = function (table) {
        var colGroup = table.getElementsByTagName('colgroup');
        if (!colGroup.length) {
            return undefined;
        }
        var colWidths = [];
        var cols = colGroup[0].getElementsByTagName('col');
        for (var i = 0; i < cols.length; i++) {
            colWidths.push(parseInt(cols[i].width, 10));
            if (cols[i].span && cols[i].span > 1) {
                for (var k = 0; k < cols[i].span - 1; k++) {
                    colWidths.push(parseInt(cols[i].width, 10));
                }
            }
        }
        return colWidths;
    };
    Clipboard.prototype.getRawValue = function (val, format) {
        if (typeof format === 'undefined') {
            format = '@';
        }
        if (format === '@') {
            return val;
        }
        return val;
    };
    Clipboard.prototype.cssToStyles = function (cssText) {
        var parts = cssText.split(';'), style = {};
        for (var i = 0; i < parts.length; i++) {
            var styleParts = parts[i].split(':');
            if (styleParts.length < 2) {
                continue;
            }
            var styleName = styleParts[0].trim();
            var styleVal = styleParts[1].trim();
            var hasBorder = false;
            if (styleName.indexOf('border-') > -1) {
                var nameParts = styleName.split('-');
                if (nameParts.length === 3 &&
                    this.borderSides.indexOf(nameParts[1]) !== -1 &&
                    this.borderSyles.indexOf(nameParts[2]) !== -1) {
                    hasBorder = true;
                }
            }
            if (this.validStyles.indexOf(styleName) === -1 && !hasBorder) {
                continue;
            }
            styleVal = styleVal.split('!')[0];
            if (styleName === 'font-family') {
                styleVal = styleVal.split(',')[0].replace(/"/gi, '');
            }
            if ((styleName === 'color' || styleName === 'background-color' || styleName.indexOf('-color') !== -1) &&
                styleVal.indexOf('rgb') >= 0) {
                styleVal = "" + this.utils.rgbToHex(styleVal);
            }
            if (styleName === 'font-size' && styleVal.indexOf('px')) {
                styleVal = Math.round(parseFloat(styleVal) * 0.73) + "pt";
            }
            if (styleName === 'word-wrap') {
                style['text-wrap'] = styleVal.trim();
            }
            if (styleName === 'white-space' && styleVal === 'normal') {
                styleVal = 'inherit';
            }
            if (styleName === 'text-align') {
                if (styleVal.indexOf('right') > -1) {
                    styleVal = 'right';
                }
                else if (styleVal.indexOf('justify') > -1 || styleVal.indexOf('left') > -1) {
                    styleVal = 'left';
                }
                else if (styleVal.indexOf('center') > -1) {
                    styleVal = 'center';
                }
            }
            style[styleName] = styleVal.trim();
            if (hasBorder) {
                var map = this.mapExcelBorder(styleName, styleVal);
                if (map) {
                    style[map.name] = map.value;
                }
            }
        }
        return style;
    };
    Clipboard.prototype.mapExcelBorder = function (key, val) {
        var parts = key.split('-');
        if (parts.length !== 3) {
            return undefined;
        }
        var name = [parts[1].substr(0, 1), parts[0].substr(0, 1)];
        if (this.excelStyles.indexOf(name.join('')) === -1 || this.borderSyles.indexOf(parts[2]) === -1) {
            return undefined;
        }
        var index = this.borderSyles.indexOf(parts[2]);
        var style = this.borderSyles[index];
        name.push(style.substr(0, 1));
        var value = '';
        switch (style) {
            case 'width':
                value = this.excelBorderWeights[Math.round(parseFloat(val)) + "px"] || 'none';
                break;
            case 'style':
                value = this.excelBorderStyles[val] || 'none';
                break;
            default:
                value = val;
                break;
        }
        var map = {
            name: name.join(''),
            value: value,
        };
        return map;
    };
    Clipboard.prototype.getTextWidth = function (text, font) {
        var canvas = this.getTextWidth.canvas || (this.getTextWidth.canvas = document.createElement('canvas'));
        var context = canvas.getContext('2d');
        context.font = font;
        var metrics = context.measureText(text);
        return metrics.width;
    };
    return Clipboard;
}(Emitter_1.default));
exports.Clipboard = Clipboard;
var ClipboardWrap = (function () {
    function ClipboardWrap() {
        return Clipboard;
    }
    ClipboardWrap.$inject = [];
    return ClipboardWrap;
}());
exports.ClipboardWrap = ClipboardWrap;
//# sourceMappingURL=Clipboard.js.map

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = __webpack_require__(1);
var Hammer = __webpack_require__(11);
var ua = __webpack_require__(12);
var $q, storage, config;
var PdfWrap = (function () {
    function PdfWrap(q, ippStorage, ippConf) {
        $q = q;
        storage = ippStorage;
        config = ippConf;
        return Pdf;
    }
    PdfWrap.$inject = [
        "$q",
        "ippStorageService",
        "ippConfig",
    ];
    return PdfWrap;
}());
exports.default = PdfWrap;
var Pdf = (function (_super) {
    __extends(Pdf, _super);
    function Pdf(container) {
        var _this = _super.call(this) || this;
        _this.container = container;
        _this.data = [];
        _this.rendering = false;
        _this.annotationId = "pdfAnnotations";
        _this.pdfPages = 1;
        _this.pdfPage = 1;
        _this._fit = "contain";
        _this.touch = false;
        _this.hammer = false;
        _this.scale = 1;
        _this.ratio = 1;
        _this.ratioScale = 1;
        _this.scrollbars = {
            width: 8,
            inset: false,
            scrolling: false,
            x: {
                size: 0,
                offset: 0,
                offsetPercent: 0,
                label: "width",
                style: "margin-left",
                show: false,
                anchor: "left",
                track: undefined,
                handle: undefined
            },
            y: {
                size: 0,
                offset: 0,
                offsetPercent: 0,
                label: "height",
                style: "margin-top",
                show: false,
                anchor: "top",
                track: undefined,
                handle: undefined
            }
        };
        _this.onResizeEvent = function (evt) {
            _this.setAnnotationsBounds();
            _this.updateScrollbars();
            _this.setFit(_this.fit);
        };
        _this.onWheelEvent = function (evt) {
            var offset = Math.abs(evt.deltaY) >= 100 ? Math.round(Math.abs(evt.deltaY) / 5) : 20;
            var direction = evt.deltaY > 0 ? 1 : -1;
            offset *= direction;
            var axis = "y";
            if (_this.canvasBounds.height <= _this.containerBounds.height && _this.canvasBounds.width > _this.containerBounds.width) {
                axis = "x";
            }
            console.log(axis, offset);
            _this.moveScrollbarTrack(axis, offset);
        };
        _this.onMouseUp = function (evt) {
            console.log("onMouseUp");
            _this.scrollbars.scrolling = false;
            ["x", "y"].forEach2(function (axis) {
                _this.scrollbars[axis].drag = false;
            });
        };
        _this.onMouseDown = function (evt) {
        };
        _this.onKeydown = function (evt) {
            switch (evt.keyCode) {
                case 37:
                    _this.page("prev");
                    break;
                case 39:
                    _this.page("next");
                    break;
            }
        };
        _this.onMouseMove = function (evt) {
            ["x", "y"].forEach2(function (axis) {
                if (!_this.scrollbars[axis].drag || !_this.scrollbars.scrolling) {
                    return;
                }
                var offset = evt[axis] - _this.scrollbars[axis].start;
                _this.moveScrollbarTrack(axis, offset);
                _this.scrollbars[axis].start = evt[axis];
            });
        };
        _this.destroy();
        var parser = new ua();
        var parseResult = parser.getResult();
        _this.touch = parseResult.device && (parseResult.device.type === "tablet" || parseResult.device.type === "mobile");
        _this.pdfLink = new PdfLink();
        _this.pdfLink.register(function (name, value) {
            console.log("pdf", name, value);
            if (name === _this.pdfLink.ON_GOTO_PAGE) {
                _this.pdfPage = value;
                _this.renderPdf();
            }
        });
        if (typeof container === "string") {
            _this.containerEl = document.getElementById(container);
        }
        else {
            _this.containerEl = container;
        }
        _this.containerEl.style["overflow"] = _this.touch ? "auto" : "hidden";
        _this.containerEl.style["text-align"] = "center";
        _this.canvasEl = document.createElement("canvas");
        _this.annotationsEl = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        _this.annotationsEl.style["position"] = "absolute";
        _this.annotationsEl.style["z-index"] = "1";
        _this.annotationsEl.style["left"] = "0";
        _this.annotationsEl.style["top"] = "0";
        _this.containerEl.appendChild(_this.canvasEl);
        _this.containerEl.appendChild(_this.annotationsEl);
        _this.ratio = window.devicePixelRatio;
        _this.scale = _this.ratio;
        _this.ratioScale = _this.scale;
        if (_this.touch) {
            _this.setupTouch();
        }
        else {
            _this.containerEl.addEventListener("wheel", _this.onWheelEvent, false);
            window.addEventListener("mouseup", _this.onMouseUp, false);
            window.addEventListener("mousedown", _this.onMouseDown, false);
            window.addEventListener("mousemove", _this.onMouseMove, false);
            window.addEventListener("keydown", _this.onKeydown, false);
            _this.createScrollbars();
        }
        window.addEventListener("resize", _this.onResizeEvent);
        return _this;
    }
    Object.defineProperty(Pdf.prototype, "ON_LOADING", {
        get: function () { return "loading"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pdf.prototype, "ON_LOADED", {
        get: function () { return "loaded"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pdf.prototype, "fit", {
        get: function () {
            return this._fit;
        },
        set: function (value) {
            if (["scroll", "width", "height", "contain"].indexOf(value) === -1) {
                return;
            }
            this._fit = value;
        },
        enumerable: true,
        configurable: true
    });
    Pdf.prototype.destroy = function () {
        if (!this.containerEl) {
            return;
        }
        document.removeEventListener("resize", this.onResizeEvent);
        this.containerEl.innerHTML = "";
        if (this.hammer) {
            this.hammer.destroy();
        }
        else {
            this.containerEl.removeEventListener("wheel", this.onWheelEvent, false);
            window.removeEventListener("mouseup", this.onMouseUp, false);
            window.removeEventListener("mousedown", this.onMouseDown, false);
            window.removeEventListener("mousemove", this.onMouseMove, false);
            window.removeEventListener("keydown", this.onKeydown, false);
        }
    };
    Pdf.prototype.render = function (data) {
        var _this = this;
        var q = $q.defer();
        this.data = data;
        if (!this.data || !this.data[0] || !this.data[0][0]) {
            q.reject(false);
            return q.promise;
        }
        this.fileUrl = this.handleURL(this.data[0][0].formatted_value || this.data[0][0].value);
        this.rendering = true;
        var headers = {};
        if (this.fileUrl.indexOf("ipushpull.") >= 0) {
            headers = {
                "Authorization": "Bearer " + storage.persistent.get("access_token"),
                "x-ipp-client": config.api_key,
            };
        }
        var loading = PDFJS.getDocument({
            url: this.fileUrl,
            httpHeaders: headers,
        }, undefined, undefined, function (result) {
            _this.emit(_this.ON_LOADING, result);
        });
        loading.then(function (pdf) {
            _this.pdfObj = pdf;
            _this.pdfLink.setDocument(pdf);
            _this.pdfPages = pdf.numPages;
            _this.renderPdf();
            q.resolve(true);
        }, function (err) {
            q.reject(err);
        });
        return q.promise;
    };
    Pdf.prototype.setFit = function (dimension) {
        if (["scroll", "width", "height", "contain"].indexOf(dimension) === -1) {
            return;
        }
        this.fit = dimension;
        if (dimension === "scroll") {
            this.canvasEl.style["width"] = "auto";
            this.canvasEl.style["height"] = "auto";
        }
        else if (dimension === "width") {
            this.canvasEl.style["width"] = "100%";
            this.canvasEl.style["height"] = "auto";
        }
        else if (dimension === "height") {
            this.canvasEl.style["width"] = "auto";
            this.canvasEl.style["height"] = "100%";
        }
        else if (dimension === "contain") {
            var containerBounds = this.containerEl.getBoundingClientRect();
            var canvasBounds = this.canvasEl.getBoundingClientRect();
            var canvasProp = this.canvasEl.width / this.canvasEl.height;
            var containerProp = containerBounds.width / containerBounds.height;
            if (canvasProp > containerProp) {
                this.canvasEl.style["width"] = "100%";
                this.canvasEl.style["height"] = "auto";
            }
            else {
                this.canvasEl.style["width"] = "auto";
                this.canvasEl.style["height"] = "100%";
            }
        }
        this.containerEl.scrollLeft = 0;
        this.containerEl.scrollTop = 0;
        this.resetScrollbars();
        this.setAnnotationsBounds();
        this.updateScrollbars();
    };
    Pdf.prototype.page = function (direction) {
        if (direction === "next") {
            this.pdfPage++;
        }
        else if (direction === "prev") {
            this.pdfPage--;
        }
        else {
            this.pdfPage = parseInt(direction, 10);
        }
        this.setPage();
    };
    Pdf.prototype.resetScrollbars = function () {
        var _this = this;
        ["x", "y"].forEach2(function (axis) {
            var anchor = _this.scrollbars[axis].anchor;
            _this.scrollbars[axis].offset = 0;
            _this.scrollbars[axis].size = 0;
            _this.scrollbars[axis].show = false;
            _this.canvasEl.style[_this.scrollbars[axis].style] = "0px";
            _this.annotationsEl.style[_this.scrollbars[axis].style] = "0px";
            if (_this.scrollbars[axis].handle) {
                _this.scrollbars[axis].handle.style[anchor] = "0px";
            }
        });
    };
    Pdf.prototype.moveScrollbarTrack = function (axis, offset) {
        this.scrollbars[axis].offset += offset;
        var dimension = this.scrollbars[axis].label;
        var canvasSize = this.canvasBounds[dimension];
        var distance = this.containerBounds[dimension] - this.scrollbars[axis].size;
        if (this.scrollbars[axis].offset > distance) {
            this.scrollbars[axis].offset = distance;
        }
        else if (this.scrollbars[axis].offset < 0) {
            this.scrollbars[axis].offset = 0;
        }
        var anchor = this.scrollbars[axis].anchor;
        this.scrollbars[axis].handle.style[anchor] = this.scrollbars[axis].offset + "px";
        var offsetPercent = this.scrollbars[axis].offset / (this.containerBounds[dimension] - this.scrollbars[axis].size);
        var offsetDistance = (this.containerBounds[dimension] - this.canvasBounds[dimension]) * offsetPercent;
        this.scrollbars[axis].offsetPercent = offsetPercent;
        this.canvasEl.style[this.scrollbars[axis].style] = offsetDistance + "px";
        this.annotationsEl.style[this.scrollbars[axis].style] = offsetDistance + "px";
    };
    Pdf.prototype.createScrollbars = function () {
        var _this = this;
        ["x", "y"].forEach2(function (axis) {
            var anchor = _this.scrollbars[axis].anchor;
            _this.scrollbars[axis].track = document.createElement("div");
            _this.scrollbars[axis].handle = document.createElement("div");
            _this.scrollbars[axis].track.style["position"] = "absolute";
            _this.scrollbars[axis].track.style["z-index"] = 10;
            _this.scrollbars[axis].track.style["display"] = "none";
            _this.scrollbars[axis].track.className = "track";
            _this.scrollbars[axis].handle.style["position"] = "absolute";
            _this.scrollbars[axis].handle.style["z-index"] = 1;
            _this.scrollbars[axis].handle.style["left"] = 0;
            _this.scrollbars[axis].handle.style["top"] = 0;
            _this.scrollbars[axis].handle.className = "handle";
            _this.scrollbars[axis].handle.addEventListener("mousedown", function (evt) {
                console.log("mousedown");
                _this.scrollbars.scrolling = true;
                _this.scrollbars[axis].start = evt[axis];
                _this.scrollbars[axis].drag = true;
                _this.scrollbars[axis].offset = parseInt(_this.scrollbars[axis].handle.style[anchor], 10);
                evt.preventDefault();
            }, false);
            if (axis === "x") {
                _this.scrollbars[axis].track.style["left"] = 0;
                _this.scrollbars[axis].track.style["bottom"] = 0;
                _this.scrollbars[axis].track.style["right"] = 0;
                _this.scrollbars[axis].track.style["height"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].handle.style["height"] = _this.scrollbars.width + "px";
            }
            else {
                _this.scrollbars[axis].track.style["top"] = 0;
                _this.scrollbars[axis].track.style["right"] = 0;
                _this.scrollbars[axis].track.style["bottom"] = 0;
                _this.scrollbars[axis].track.style["width"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].handle.style["width"] = _this.scrollbars.width + "px";
            }
            _this.scrollbars[axis].track.appendChild(_this.scrollbars[axis].handle);
            _this.containerEl.appendChild(_this.scrollbars[axis].track);
        });
    };
    Pdf.prototype.updateScrollbars = function () {
        var _this = this;
        if (this.touch) {
            return;
        }
        ["x", "y"].forEach2(function (axis) {
            var dimension = _this.scrollbars[axis].label;
            if (_this.containerBounds[dimension] >= _this.canvasBounds[dimension]) {
                _this.scrollbars[axis].track.style.display = "none";
                return;
            }
            var canvasSize = _this.canvasBounds[dimension];
            var size = _this.containerBounds[dimension] * (_this.containerBounds[dimension] / canvasSize);
            _this.scrollbars[axis].handle.style[dimension] = size + "px";
            _this.scrollbars[axis].size = size;
            var anchor = _this.scrollbars[axis].anchor;
            var offsetPercent = _this.scrollbars[axis].offsetPercent;
            var offsetDistance = (_this.containerBounds[dimension] - size) * offsetPercent;
            _this.scrollbars[axis].handle.style[anchor] = offsetDistance + "px";
            _this.scrollbars[axis].track.style.display = "block";
            offsetDistance = (_this.containerBounds[dimension] - _this.canvasBounds[dimension]) * offsetPercent;
            _this.canvasEl.style[_this.scrollbars[axis].style] = offsetDistance + "px";
            _this.annotationsEl.style[_this.scrollbars[axis].style] = offsetDistance + "px";
        });
    };
    Pdf.prototype.setAnnotationsBounds = function () {
        this.containerBounds = this.containerEl.getBoundingClientRect();
        this.canvasBounds = this.canvasEl.getBoundingClientRect();
        this.annotationsEl.style.left = (this.canvasBounds.left - this.containerBounds.left) + "px";
        this.annotationsEl.style.width = this.canvasBounds.width + "px";
        this.annotationsEl.style.height = this.canvasBounds.height + "px";
    };
    Pdf.prototype.setupTouch = function () {
        var _this = this;
        this.hammer = new Hammer(this.containerEl);
        var pan = new Hammer.Pan();
        var pinch = new Hammer.Pinch();
        this.hammer.add([pan, pinch]);
        var width;
        this.hammer.on("pinchstart", function (evt) {
            var rect = _this.canvasEl.getBoundingClientRect();
            width = rect.width;
            _this.fit = "scroll";
            _this.canvasEl.style.width = width + "px";
            _this.canvasEl.style.height = "auto";
        });
        this.hammer.on("pinchmove", function (evt) {
            console.log(evt);
            _this.canvasEl.style.width = (width * evt.scale) + "px";
            _this.setAnnotationsBounds();
        });
        this.hammer.on("pinchend", function (evt) {
        });
        var offsets = { x: 0, y: 0, left: this.containerEl.scrollLeft, top: this.containerEl.scrollTop };
        this.hammer.on("panstart", function (evt) {
            offsets.x = evt.center.x;
            offsets.y = evt.center.y;
            offsets.left = _this.containerEl.scrollLeft;
            offsets.top = _this.containerEl.scrollTop;
        });
        this.hammer.on("panmove", function (evt) {
            _this.containerEl.scrollTop = offsets.top + offsets.y - evt.center.y;
            _this.containerEl.scrollLeft = offsets.left + offsets.x - evt.center.x;
        });
        this.hammer.on("panend", function (evt) {
            ;
        });
    };
    Pdf.prototype.setPage = function () {
        if (this.pdfPage > this.pdfPages) {
            this.pdfPage = 1;
        }
        else if (this.pdfPage <= 0) {
            this.pdfPage = this.pdfPages;
        }
        this.renderPdf();
    };
    Pdf.prototype.renderPdf = function () {
        var _this = this;
        var q = $q.defer();
        this.pdfObj.getPage(this.pdfPage).then(function (page) {
            var viewport = page.getViewport(2);
            var context = _this.canvasEl.getContext("2d");
            _this.canvasEl.height = viewport.height;
            _this.canvasEl.width = viewport.width;
            var renderContext = {
                canvasContext: context,
                viewport: viewport,
            };
            var render = page.render(renderContext);
            render.promise.then(function (a) {
                console.log("renderPdf", a);
                _this.setFit(_this.fit);
                _this.emit(_this.ON_LOADED, true);
                _this.renderAnnotations(page, viewport, context);
                q.resolve(a);
            }, q.reject);
        }, function (err) {
            console.log("renderPdf", err);
        });
        return q.promise;
    };
    Pdf.prototype.renderAnnotations = function (page, viewport, canvas) {
        var _this = this;
        var layer = this.annotationsEl;
        if (!layer) {
            return;
        }
        layer.innerHTML = "";
        this.setAnnotationsBounds();
        console.log("pdf", layer);
        var appendAnchor = function (element, url) {
            var svgNS = _this.annotationsEl.namespaceURI;
            var a = document.createElementNS(svgNS, "a");
            a.setAttribute("class", "pdf-link");
            a.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "#");
            a.addEventListener("click", function (e) {
                console.log("pdf", "click", url);
                if (url.link) {
                    window.open(url.link, "_blank");
                    e.preventDefault();
                }
                else if (url.internal) {
                    _this.page(url.page + 1);
                }
            });
            var rect = document.createElementNS(svgNS, "rect");
            rect.setAttribute("x", element.rect[0]);
            rect.setAttribute("y", viewport.viewBox[3] - element.rect[3]);
            rect.setAttribute("width", element.rect[2] - element.rect[0]);
            rect.setAttribute("height", element.rect[3] - element.rect[1]);
            rect.setAttribute("fill", "rgba(30,144,255,1)");
            a.appendChild(rect);
            _this.annotationsEl.appendChild(a);
        };
        page.getAnnotations().then(function (annotationsData) {
            console.log(annotationsData);
            viewport = viewport.clone({
                dontFlip: true,
            });
            layer.setAttribute("viewBox", viewport.viewBox.join(" "));
            annotationsData.forEach2(function (element) {
                if (element.dest && typeof element.dest === "string") {
                    _this.pdfObj.getDestination(element.dest).then(function (res) {
                        console.log("pdf", res);
                        _this.pdfObj.getPageIndex(res[0]).then(function (page) {
                            console.log("pdf", page);
                            appendAnchor(element, { internal: true, page: page });
                        }, function (err) {
                            console.error("pdf", err);
                        });
                    }, function (err) {
                        console.error("pdf", err);
                    });
                }
                else if (element.url) {
                    appendAnchor(element, { link: element.url });
                }
            });
        });
    };
    Pdf.prototype.handleURL = function (url) {
        if (url.indexOf("dropbox.com") > -1) {
            url = url.split("www.dropbox.com").join("dl.dropboxusercontent.com");
            return config.docs_url + "/default/relay?url=" + url;
        }
        return url;
    };
    return Pdf;
}(Emitter_1.default));
var PdfLink = (function (_super) {
    __extends(PdfLink, _super);
    function PdfLink() {
        var _this = _super.call(this) || this;
        _this._pagesRefCache = undefined;
        return _this;
    }
    Object.defineProperty(PdfLink.prototype, "ON_GOTO_PAGE", {
        get: function () { return "goto_page"; },
        enumerable: true,
        configurable: true
    });
    PdfLink.prototype.setDocument = function (pdfDocument, baseUrl) {
        this.baseUrl = baseUrl;
        this.pdfDocument = pdfDocument;
        this._pagesRefCache = {};
    };
    Object.defineProperty(PdfLink.prototype, "pagesCount", {
        get: function () {
            return this.pdfDocument.numPages;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PdfLink.prototype, "page", {
        get: function () {
            return this.pdfViewer.currentPageNumber;
        },
        set: function (value) {
            this.pdfViewer.currentPageNumber = value;
        },
        enumerable: true,
        configurable: true
    });
    PdfLink.prototype.navigateTo = function (dest) {
        var _this = this;
        var destString = "";
        var goToDestination = function (destRef) {
            var pageNumber = destRef instanceof Object ?
                _this._pagesRefCache[destRef.num + " " + destRef.gen + " R"] :
                (destRef + 1);
            if (pageNumber) {
                if (pageNumber > _this.pagesCount) {
                    pageNumber = _this.pagesCount;
                }
                _this.emit(_this.ON_GOTO_PAGE, pageNumber);
            }
            else {
                _this.pdfDocument.getPageIndex(destRef).then(function (pageIndex) {
                    var pageNum = pageIndex + 1;
                    var cacheKey = destRef.num + " " + destRef.gen + " R";
                    _this._pagesRefCache[cacheKey] = pageNum;
                    goToDestination(destRef);
                });
            }
        };
        var destinationPromise;
        if (typeof dest === "string") {
            destString = dest;
            destinationPromise = this.pdfDocument.getDestination(dest);
        }
        else {
            destinationPromise = Promise.resolve(dest);
        }
        destinationPromise.then(function (destination) {
            dest = destination;
            if (!(destination instanceof Array)) {
                return;
            }
            goToDestination(destination[0]);
        });
    };
    PdfLink.prototype.getDestinationHash = function (dest) {
        if (typeof dest === "string") {
            return this.getAnchorUrl("#" + escape(dest));
        }
        if (dest instanceof Array) {
            var destRef = dest[0];
            var pageNumber = destRef instanceof Object ?
                this._pagesRefCache[destRef.num + " " + destRef.gen + " R"] :
                (destRef + 1);
            if (pageNumber) {
                var pdfOpenParams = this.getAnchorUrl("#page=" + pageNumber);
                var destKind = dest[1];
                if (typeof destKind === "object" && "name" in destKind &&
                    destKind.name === "XYZ") {
                    var scale = (dest[4]);
                    var scaleNumber = parseFloat(scale);
                    if (scaleNumber) {
                        scale = scaleNumber * 100;
                    }
                    pdfOpenParams += "&zoom=" + scale;
                    if (dest[2] || dest[3]) {
                        pdfOpenParams += "," + (dest[2] || 0) + "," + (dest[3] || 0);
                    }
                }
                return pdfOpenParams;
            }
        }
        return "";
    };
    PdfLink.prototype.getAnchorUrl = function (anchor) {
        return (this.baseUrl || "") + anchor;
    };
    PdfLink.prototype.setHash = function (hash) { };
    PdfLink.prototype.executeNamedAction = function (action) { };
    PdfLink.prototype.cachePageRef = function (pageNum, pageRef) {
        var refStr = pageRef.num + " " + pageRef.gen + " R";
        this._pagesRefCache[refStr] = pageNum;
    };
    return PdfLink;
}(Emitter_1.default));
//# sourceMappingURL=Pdf.js.map

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Utils_1 = __webpack_require__(4);
var Tracking = (function () {
    function Tracking($interval, storage) {
        this.$interval = $interval;
        this.storage = storage;
        this.history = {};
        this.historyFirst = {};
        this._enabled = false;
        this._length = 0;
        this._utils = new Utils_1.default();
    }
    Tracking.prototype.createInstance = function () {
        return new Tracking(this.$interval, this.storage);
    };
    Object.defineProperty(Tracking.prototype, "enabled", {
        get: function () {
            return this._enabled;
        },
        set: function (value) {
            var _this = this;
            if (typeof value !== 'boolean') {
                return;
            }
            this._enabled = value;
            this.cancel();
            if (!value) {
                this.reset();
            }
            else {
                this.restore();
                this._length = this.totalChanges();
                this._interval = this.$interval(function () {
                    var length = _this.totalChanges();
                    if (_this._length !== length) {
                        _this.storage.user.save('history', JSON.stringify(_this.history));
                    }
                    _this._length = length;
                }, 10000);
            }
        },
        enumerable: true,
        configurable: true
    });
    Tracking.prototype.addHistory = function (pageId, data, first) {
        if (first === void 0) { first = false; }
        if (!this.enabled) {
            return;
        }
        if (first && (this.history["page_" + pageId] && this.history["page_" + pageId].length)) {
            return;
        }
        if (!this.history["page_" + pageId]) {
            this.history["page_" + pageId] = [];
        }
        this.history["page_" + pageId].push(this.compressData(data, first));
    };
    Tracking.prototype.getHistory = function (pageId) {
        if (!this.history["page_" + pageId]) {
            return [];
        }
        return this.history["page_" + pageId];
    };
    Tracking.prototype.getHistoryForCell = function (pageId, cellRow, cellCol) {
        var history = this.getHistory(pageId);
        if (!history.length) {
            return [];
        }
        var cellHistory = [];
        history.forEach2(function (snapshot) {
            snapshot.content_diff.forEach2(function (row, rowIndex) {
                if (!row) {
                    return;
                }
                row.forEach2(function (col, colIndex) {
                    if (!col) {
                        return;
                    }
                    if (cellRow === rowIndex && cellCol === colIndex) {
                        cellHistory.push({
                            cell: JSON.parse(JSON.stringify(col)),
                            modified_by: snapshot.modified_by,
                            modified_by_timestamp: snapshot.modified_by_timestamp,
                        });
                    }
                });
            });
        });
        return cellHistory;
    };
    Tracking.prototype.restore = function () {
        this.history = this.storage.user.get('history') || {};
    };
    Tracking.prototype.reset = function () {
        this.history = {};
        this.storage.user.remove('history');
    };
    Tracking.prototype.cancel = function () {
        if (this._interval) {
            this.$interval.cancel(this._interval);
        }
    };
    Tracking.prototype.totalChanges = function () {
        var length = 0;
        for (var id in this.history) {
            if (!this.history.hasOwnProperty(id)) {
                continue;
            }
            length += this.history[id].length;
        }
        return length;
    };
    Tracking.prototype.compressData = function (data, first) {
        if (first === void 0) { first = false; }
        if (first) {
            data.content_diff = this._utils.clonePageContent(data.content_diff);
        }
        return JSON.parse(JSON.stringify(data));
    };
    Tracking.$inject = ['$interval', 'ippStorageService'];
    return Tracking;
}());
exports.Tracking = Tracking;
exports.default = Tracking;
//# sourceMappingURL=Tracking.js.map

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Crypto = (function () {
    function Crypto() {
    }
    Crypto._instance = function () {
        return new Crypto();
    };
    Crypto.prototype.decryptContent = function (key, data) {
        if (!this.libCheck()) {
            return;
        }
        if (!data)
            return undefined;
        var rawData = forge.util.decode64(data);
        var iv = rawData.substring(0, 16);
        var cleanData = rawData.substring(16);
        cleanData = forge.util.createBuffer(cleanData, "latin1");
        iv = forge.util.createBuffer(iv, "latin1");
        var decipher = forge.cipher.createDecipher("AES-CBC", this.hashPassphrase(key.passphrase));
        decipher.start({ iv: iv });
        decipher.update(cleanData);
        var pass = decipher.finish();
        var decrypted;
        try {
            decrypted = JSON.parse(decipher.output.toString());
        }
        catch (e) {
            decrypted = undefined;
        }
        return decrypted;
    };
    Crypto.prototype.encryptContent = function (key, data) {
        if (!this.libCheck()) {
            return;
        }
        var readyData = JSON.stringify(data);
        var hash = this.hashPassphrase(key.passphrase);
        var iv = forge.random.getBytesSync(16);
        var cipher = forge.cipher.createCipher("AES-CBC", hash);
        cipher.start({ iv: iv });
        cipher.update(forge.util.createBuffer(readyData, "utf8"));
        cipher.finish();
        var encrypted = cipher.output;
        var buffer = forge.util.createBuffer();
        buffer.putBytes(iv);
        buffer.putBytes(encrypted.bytes());
        var output = buffer.getBytes();
        return forge.util.encode64(output);
    };
    Crypto.prototype.hashPassphrase = function (passphrase) {
        var md = forge.md.sha256.create();
        md.update(passphrase);
        return md.digest().bytes();
    };
    Crypto.prototype.libCheck = function () {
        if (typeof forge === "undefined") {
            console.error("[iPushPull]", "If you want to use encryption make sure you include forge library in your header or use ng-ipushpull-standalone.min.js");
        }
        return typeof forge !== "undefined";
    };
    return Crypto;
}());
exports.Crypto = Crypto;
//# sourceMappingURL=Crypto.js.map

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var request = __webpack_require__(29);
var Request = (function () {
    function Request() {
    }
    Request._instance = function () {
        return request;
    };
    return Request;
}());
exports.Request = Request;
//# sourceMappingURL=Request.js.map

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var window = __webpack_require__(30)
var isFunction = __webpack_require__(13)
var parseHeaders = __webpack_require__(31)
var xtend = __webpack_require__(34)

module.exports = createXHR
// Allow use of default import syntax in TypeScript
module.exports.default = createXHR;
createXHR.XMLHttpRequest = window.XMLHttpRequest || noop
createXHR.XDomainRequest = "withCredentials" in (new createXHR.XMLHttpRequest()) ? createXHR.XMLHttpRequest : window.XDomainRequest

forEachArray(["get", "put", "post", "patch", "head", "delete"], function(method) {
    createXHR[method === "delete" ? "del" : method] = function(uri, options, callback) {
        options = initParams(uri, options, callback)
        options.method = method.toUpperCase()
        return _createXHR(options)
    }
})

function forEachArray(array, iterator) {
    for (var i = 0; i < array.length; i++) {
        iterator(array[i])
    }
}

function isEmpty(obj){
    for(var i in obj){
        if(obj.hasOwnProperty(i)) return false
    }
    return true
}

function initParams(uri, options, callback) {
    var params = uri

    if (isFunction(options)) {
        callback = options
        if (typeof uri === "string") {
            params = {uri:uri}
        }
    } else {
        params = xtend(options, {uri: uri})
    }

    params.callback = callback
    return params
}

function createXHR(uri, options, callback) {
    options = initParams(uri, options, callback)
    return _createXHR(options)
}

function _createXHR(options) {
    if(typeof options.callback === "undefined"){
        throw new Error("callback argument missing")
    }

    var called = false
    var callback = function cbOnce(err, response, body){
        if(!called){
            called = true
            options.callback(err, response, body)
        }
    }

    function readystatechange() {
        if (xhr.readyState === 4) {
            setTimeout(loadFunc, 0)
        }
    }

    function getBody() {
        // Chrome with requestType=blob throws errors arround when even testing access to responseText
        var body = undefined

        if (xhr.response) {
            body = xhr.response
        } else {
            body = xhr.responseText || getXml(xhr)
        }

        if (isJson) {
            try {
                body = JSON.parse(body)
            } catch (e) {}
        }

        return body
    }

    function errorFunc(evt) {
        clearTimeout(timeoutTimer)
        if(!(evt instanceof Error)){
            evt = new Error("" + (evt || "Unknown XMLHttpRequest Error") )
        }
        evt.statusCode = 0
        return callback(evt, failureResponse)
    }

    // will load the data & process the response in a special response object
    function loadFunc() {
        if (aborted) return
        var status
        clearTimeout(timeoutTimer)
        if(options.useXDR && xhr.status===undefined) {
            //IE8 CORS GET successful response doesn't have a status field, but body is fine
            status = 200
        } else {
            status = (xhr.status === 1223 ? 204 : xhr.status)
        }
        var response = failureResponse
        var err = null

        if (status !== 0){
            response = {
                body: getBody(),
                statusCode: status,
                method: method,
                headers: {},
                url: uri,
                rawRequest: xhr
            }
            if(xhr.getAllResponseHeaders){ //remember xhr can in fact be XDR for CORS in IE
                response.headers = parseHeaders(xhr.getAllResponseHeaders())
            }
        } else {
            err = new Error("Internal XMLHttpRequest Error")
        }
        return callback(err, response, response.body)
    }

    var xhr = options.xhr || null

    if (!xhr) {
        if (options.cors || options.useXDR) {
            xhr = new createXHR.XDomainRequest()
        }else{
            xhr = new createXHR.XMLHttpRequest()
        }
    }

    var key
    var aborted
    var uri = xhr.url = options.uri || options.url
    var method = xhr.method = options.method || "GET"
    var body = options.body || options.data
    var headers = xhr.headers = options.headers || {}
    var sync = !!options.sync
    var isJson = false
    var timeoutTimer
    var failureResponse = {
        body: undefined,
        headers: {},
        statusCode: 0,
        method: method,
        url: uri,
        rawRequest: xhr
    }

    if ("json" in options && options.json !== false) {
        isJson = true
        headers["accept"] || headers["Accept"] || (headers["Accept"] = "application/json") //Don't override existing accept header declared by user
        if (method !== "GET" && method !== "HEAD") {
            headers["content-type"] || headers["Content-Type"] || (headers["Content-Type"] = "application/json") //Don't override existing accept header declared by user
            body = JSON.stringify(options.json === true ? body : options.json)
        }
    }

    xhr.onreadystatechange = readystatechange
    xhr.onload = loadFunc
    xhr.onerror = errorFunc
    // IE9 must have onprogress be set to a unique function.
    xhr.onprogress = function () {
        // IE must die
    }
    xhr.onabort = function(){
        aborted = true;
    }
    xhr.ontimeout = errorFunc
    xhr.open(method, uri, !sync, options.username, options.password)
    //has to be after open
    if(!sync) {
        xhr.withCredentials = !!options.withCredentials
    }
    // Cannot set timeout with sync request
    // not setting timeout on the xhr object, because of old webkits etc. not handling that correctly
    // both npm's request and jquery 1.x use this kind of timeout, so this is being consistent
    if (!sync && options.timeout > 0 ) {
        timeoutTimer = setTimeout(function(){
            if (aborted) return
            aborted = true//IE9 may still call readystatechange
            xhr.abort("timeout")
            var e = new Error("XMLHttpRequest timeout")
            e.code = "ETIMEDOUT"
            errorFunc(e)
        }, options.timeout )
    }

    if (xhr.setRequestHeader) {
        for(key in headers){
            if(headers.hasOwnProperty(key)){
                xhr.setRequestHeader(key, headers[key])
            }
        }
    } else if (options.headers && !isEmpty(options.headers)) {
        throw new Error("Headers cannot be set on an XDomainRequest object")
    }

    if ("responseType" in options) {
        xhr.responseType = options.responseType
    }

    if ("beforeSend" in options &&
        typeof options.beforeSend === "function"
    ) {
        options.beforeSend(xhr)
    }

    // Microsoft Edge browser sends "undefined" when send is called with undefined value.
    // XMLHttpRequest spec says to pass null as body to indicate no body
    // See https://github.com/naugtur/xhr/issues/100.
    xhr.send(body || null)

    return xhr


}

function getXml(xhr) {
    // xhr.responseXML will throw Exception "InvalidStateError" or "DOMException"
    // See https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/responseXML.
    try {
        if (xhr.responseType === "document") {
            return xhr.responseXML
        }
        var firefoxBugTakenEffect = xhr.responseXML && xhr.responseXML.documentElement.nodeName === "parsererror"
        if (xhr.responseType === "" && !firefoxBugTakenEffect) {
            return xhr.responseXML
        }
    } catch (e) {}

    return null
}

function noop() {}


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var win;

if (typeof window !== "undefined") {
    win = window;
} else if (typeof global !== "undefined") {
    win = global;
} else if (typeof self !== "undefined"){
    win = self;
} else {
    win = {};
}

module.exports = win;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(9)))

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

var trim = __webpack_require__(32)
  , forEach = __webpack_require__(33)
  , isArray = function(arg) {
      return Object.prototype.toString.call(arg) === '[object Array]';
    }

module.exports = function (headers) {
  if (!headers)
    return {}

  var result = {}

  forEach(
      trim(headers).split('\n')
    , function (row) {
        var index = row.indexOf(':')
          , key = trim(row.slice(0, index)).toLowerCase()
          , value = trim(row.slice(index + 1))

        if (typeof(result[key]) === 'undefined') {
          result[key] = value
        } else if (isArray(result[key])) {
          result[key].push(value)
        } else {
          result[key] = [ result[key], value ]
        }
      }
  )

  return result
}

/***/ }),
/* 32 */
/***/ (function(module, exports) {


exports = module.exports = trim;

function trim(str){
  return str.replace(/^\s*|\s*$/g, '');
}

exports.left = function(str){
  return str.replace(/^\s*/, '');
};

exports.right = function(str){
  return str.replace(/\s*$/, '');
};


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

var isFunction = __webpack_require__(13)

module.exports = forEach

var toString = Object.prototype.toString
var hasOwnProperty = Object.prototype.hasOwnProperty

function forEach(list, iterator, context) {
    if (!isFunction(iterator)) {
        throw new TypeError('iterator must be a function')
    }

    if (arguments.length < 3) {
        context = this
    }
    
    if (toString.call(list) === '[object Array]')
        forEachArray(list, iterator, context)
    else if (typeof list === 'string')
        forEachString(list, iterator, context)
    else
        forEachObject(list, iterator, context)
}

function forEachArray(array, iterator, context) {
    for (var i = 0, len = array.length; i < len; i++) {
        if (hasOwnProperty.call(array, i)) {
            iterator.call(context, array[i], i, array)
        }
    }
}

function forEachString(string, iterator, context) {
    for (var i = 0, len = string.length; i < len; i++) {
        // no such thing as a sparse string.
        iterator.call(context, string.charAt(i), i, string)
    }
}

function forEachObject(object, iterator, context) {
    for (var k in object) {
        if (hasOwnProperty.call(object, k)) {
            iterator.call(context, object[k], k, object)
        }
    }
}


/***/ }),
/* 34 */
/***/ (function(module, exports) {

module.exports = extend

var hasOwnProperty = Object.prototype.hasOwnProperty;

function extend() {
    var target = {}

    for (var i = 0; i < arguments.length; i++) {
        var source = arguments[i]

        for (var key in source) {
            if (hasOwnProperty.call(source, key)) {
                target[key] = source[key]
            }
        }
    }

    return target
}


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Utils_1 = __webpack_require__(4);
var merge = __webpack_require__(2);
var extend = __webpack_require__(36);
var _ = __webpack_require__(3);
var Emitter_1 = __webpack_require__(1);
var Content_1 = __webpack_require__(37);
var Provider_1 = __webpack_require__(6);
var Provider_2 = __webpack_require__(6);
var Provider_3 = __webpack_require__(6);
var Range_1 = __webpack_require__(0);
var Range_2 = __webpack_require__(0);
var $q, $timeout, $interval, api, auth, storage, crypto, config, utils, providers, ranges;
var PageWrap = (function () {
    function PageWrap(q, timeout, interval, ippApi, ippAuth, ippStorage, ippCrypto, ippConf) {
        $q = q;
        $timeout = timeout;
        $interval = interval;
        api = ippApi;
        auth = ippAuth;
        storage = ippStorage;
        crypto = ippCrypto;
        config = ippConf;
        utils = new Utils_1.default();
        providers = new Provider_1.Providers($q, $timeout, api, auth, storage, config);
        ranges = new Range_1.RangesWrap($q, api);
        return Page;
    }
    PageWrap.$inject = [
        "$q",
        "$timeout",
        "$interval",
        "ippApiService",
        "ippAuthService",
        "ippStorageService",
        "ippCryptoService",
        "ippConfig"
    ];
    return PageWrap;
}());
exports.PageWrap = PageWrap;
var Page = (function (_super) {
    __extends(Page, _super);
    function Page(pageId, folderId, uuid) {
        var _this = _super.call(this) || this;
        _this.ready = false;
        _this.decrypted = true;
        _this.updatesOn = true;
        _this.freeze = {
            valid: false,
            row: -1,
            col: -1
        };
        _this._supportsWS = true;
        _this._wsDisabled = false;
        _this._checkAccess = true;
        _this._error = false;
        _this._encryptionKeyPull = {
            name: "",
            passphrase: ""
        };
        _this._encryptionKeyPush = {
            name: "",
            passphrase: ""
        };
        _this.onPageError = function (err) {
            if (!err) {
                return;
            }
            err.code = err.httpCode || err.code;
            err.message = err.httpText || err.message || (typeof err.data === "string" ? err.data : "Page not found");
            _this.emit(_this.EVENT_ERROR, err);
            if (err.code === 404) {
                _this.destroy();
            }
            if (err.type === "redirect") {
                _this._wsDisabled = true;
                _this.init(true);
            }
            else {
                _this._error = true;
            }
        };
        _this.types = {
            regular: _this.TYPE_REGULAR,
            pageAccessReport: _this.TYPE_PAGE_ACCESS_REPORT,
            domainUsageReport: _this.TYPE_DOMAIN_USAGE_REPORT,
            globalUsageReport: _this.TYPE_GLOBAL_USAGE_REPORT,
            pageUpdateReport: _this.TYPE_PAGE_UPDATE_REPORT,
            alert: _this.TYPE_ALERT,
            pdf: _this.TYPE_PDF,
            liveUsage: _this.TYPE_LIVE_USAGE_REPORT
        };
        _this._supportsWS = "WebSocket" in window || "MozWebSocket" in window;
        _this._folderId = !isNaN(+folderId) ? folderId : undefined;
        _this._pageId = !isNaN(+pageId) ? pageId : undefined;
        _this._folderName = isNaN(+folderId) ? folderId : undefined;
        _this._pageName = isNaN(+pageId) ? pageId : undefined;
        _this._uuid = uuid;
        if (!_this._pageId && !_this._uuid) {
            _this.getPageId(_this._folderName, _this._pageName).then(function (res) {
                if (!res.pageId) {
                    _this.onPageError({
                        code: 404,
                        message: "Page not found"
                    });
                    return;
                }
                _this._pageId = res.pageId;
                _this._folderId = res.folderId;
                _this.init();
            }, function (err) {
                _this.onPageError(err || {
                    code: 404,
                    message: "Page not found"
                });
            });
        }
        else {
            _this.init(_this._uuid ? true : false);
        }
        return _this;
    }
    Object.defineProperty(Page.prototype, "TYPE_REGULAR", {
        get: function () {
            return 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_ALERT", {
        get: function () {
            return 5;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_PDF", {
        get: function () {
            return 6;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_PAGE_ACCESS_REPORT", {
        get: function () {
            return 1001;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_DOMAIN_USAGE_REPORT", {
        get: function () {
            return 1002;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_GLOBAL_USAGE_REPORT", {
        get: function () {
            return 1003;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_PAGE_UPDATE_REPORT", {
        get: function () {
            return 1004;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_LIVE_USAGE_REPORT", {
        get: function () {
            return 1007;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_READY", {
        get: function () {
            return "ready";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_DECRYPTED", {
        get: function () {
            return "decrypted";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_NEW_CONTENT", {
        get: function () {
            return "new_content";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_NEW_CONTENT_DELTA", {
        get: function () {
            return "new_content_delta";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_EMPTY_UPDATE", {
        get: function () {
            return "empty_update";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_NEW_META", {
        get: function () {
            return "new_meta";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_RANGES_UPDATED", {
        get: function () {
            return "ranges_updated";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_ACCESS_UPDATED", {
        get: function () {
            return "access_updated";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_ERROR", {
        get: function () {
            return "error";
        },
        enumerable: true,
        configurable: true
    });
    Page.create = function (folderId, name, type, template, organization_public) {
        if (type === void 0) { type = 0; }
        if (organization_public === void 0) { organization_public = false; }
        var q = $q.defer();
        if (template) {
            var page_1 = new Page(template.id, template.domain_id);
            page_1.on(page_1.EVENT_READY, function () {
                page_1.clone(folderId, name)
                    .then(q.resolve, q.reject)
                    .finally(function () {
                    page_1.destroy();
                });
            });
        }
        else {
            api.createPage({
                domainId: folderId,
                data: {
                    name: name,
                    special_page_type: type,
                    organization_public: organization_public
                }
            }).then(function (res) {
                var page = new Page(res.data.id, folderId);
                page.on(page.EVENT_READY, function () {
                    page.stop();
                    q.resolve(page);
                });
                page.on(page.EVENT_ERROR, function (err) {
                    page.stop();
                    q.reject(err);
                });
            }, function (err) {
                q.reject(err);
            });
        }
        return q.promise;
    };
    Object.defineProperty(Page.prototype, "encryptionKeyPull", {
        set: function (key) {
            this._encryptionKeyPull = key;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "encryptionKeyPush", {
        set: function (key) {
            this._encryptionKeyPush = key;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "data", {
        get: function () {
            return this._data;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "access", {
        get: function () {
            return this._access;
        },
        enumerable: true,
        configurable: true
    });
    Page.prototype.start = function () {
        if (!this.updatesOn) {
            this._provider.start();
            this.updatesOn = true;
        }
    };
    Page.prototype.stop = function () {
        if (this.updatesOn) {
            this._provider.stop();
            this.updatesOn = false;
        }
    };
    Page.prototype.push = function (forceFull, otherRequestData) {
        var _this = this;
        if (forceFull === void 0) { forceFull = false; }
        if (otherRequestData === void 0) { otherRequestData = {}; }
        var q = $q.defer();
        var currentData = _.clone(this.Content ? this.Content._original : undefined);
        var delta = false;
        var onSuccess = function (data) {
            _this.Content.dirty = false;
            _this.Content.update(_this.Content.current);
            data = extend({}, _this._data, data.data);
            if (_this._provider instanceof Provider_2.ProviderREST) {
                _this._provider.seqNo = data.seq_no;
            }
            var diff = currentData ? utils.comparePageContent(currentData, _this.Content.current, true) : false;
            if (diff) {
                data.diff = diff;
            }
            else {
                data.diff = undefined;
            }
            data._provider = false;
            if (delta) {
                _this.emit(_this.EVENT_NEW_CONTENT_DELTA, data);
                if (_this._provider instanceof Provider_2.ProviderREST) {
                    _this._provider.requestOngoing = false;
                }
            }
            else {
                _this.emit(_this.EVENT_NEW_CONTENT, data);
            }
            q.resolve(data);
        };
        if (!this._data.encryption_type_to_use &&
            !this._data.encryption_type_used &&
            this.Content.canDoDelta &&
            !forceFull) {
            delta = true;
            if (this._provider instanceof Provider_2.ProviderREST) {
                this._provider.requestOngoing = true;
            }
            this.pushDelta(this.Content.getDelta(true), otherRequestData).then(onSuccess, q.reject);
        }
        else {
            this.pushFull(this.Content.getFull(), otherRequestData).then(onSuccess, q.reject);
        }
        return q.promise;
    };
    Page.prototype.saveMeta = function (data) {
        var _this = this;
        var q = $q.defer();
        if (data.encryption_type_to_use === 0) {
            data.encryption_key_to_use = "";
        }
        api.savePageSettings({
            domainId: this._folderId,
            pageId: this._pageId,
            data: data
        }).then(function (res) {
            _this._data = extend({}, _this._data, res.data);
            q.resolve(res);
        }, function (err) {
            q.reject(utils.parseApiError(err, "Could not save page settings"));
        });
        return q.promise;
    };
    Page.prototype.setAsFoldersDefault = function () {
        var _this = this;
        var q = $q.defer();
        var requestData = {
            domainId: this._folderId,
            data: {
                default_page_id: this._pageId
            }
        };
        api.setDomainDefault(requestData).then(function (res) {
            _this._access.is_users_default_page = true;
            q.resolve(res);
        }, q.reject);
        return q.promise;
    };
    Page.prototype.del = function () {
        var requestData = {
            domainId: this._folderId,
            pageId: this._pageId
        };
        return api.deletePage(requestData);
    };
    Page.prototype.decrypt = function (key) {
        if (!key) {
            key = this._encryptionKeyPull;
        }
        if (this._data.encryption_type_used && !key.passphrase) {
            this.decrypted = false;
            return;
        }
        if (this._data.encryption_type_used) {
            if (!crypto) {
                this.emit(this.EVENT_ERROR, new Error("Encrypted pages not supported"));
                this.decrypted = false;
                return;
            }
            var decrypted = crypto.decryptContent({
                name: key.name,
                passphrase: key.passphrase
            }, this._data.encrypted_content);
            if (decrypted) {
                this.decrypted = true;
                this._data.content = decrypted;
                this._encryptionKeyPull = key;
            }
            else {
                this.decrypted = false;
                this.emit(this.EVENT_ERROR, new Error("Could not decrypt page with key \"" + key.name + "\" and passphrase \"" + key.passphrase + "\""));
            }
        }
        else {
            this.decrypted = true;
        }
        if (this.decrypted) {
            if (this.Content) {
                this.Content.update(this._data.content, false, false, this._data.show_gridlines);
            }
            else {
                this.Content = new Content_1.PageContent(this._data.content);
            }
            this.emit(this.EVENT_DECRYPTED);
        }
    };
    Page.prototype.destroy = function () {
        if (this._provider) {
            this._provider.destroy();
        }
        this.removeEvent();
        this._checkAccess = false;
    };
    Page.prototype.clone = function (folderId, name, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var q = $q.defer();
        if (!this.ready) {
            q.reject("Page is not ready");
            return q.promise;
        }
        if (options.clone_ranges && this._folderId * 1 !== folderId * 1) {
            options.clone_ranges = false;
        }
        Page.create(folderId, name, this._data.special_page_type).then(function (newPage) {
            newPage.Content = _this.Content;
            $q.all([newPage.push(true)]).then(function (res) {
                if (options.clone_ranges) {
                    api.savePageSettings({
                        domainId: folderId,
                        pageId: newPage.data.id,
                        data: {
                            access_rights: _this._data.access_rights,
                            action_definitions: _this._data.action_definitions
                        }
                    }).finally(function () {
                        q.resolve(newPage);
                    });
                }
                else {
                    q.resolve(newPage);
                }
            }, q.reject);
        }, function (err) {
            q.reject(err);
        });
        return q.promise;
    };
    Page.prototype.copy = function (folderId, name, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var q = $q.defer();
        if (!this.ready) {
            q.reject("Page is not ready");
            return q.promise;
        }
        var data = {
            range_access: true,
            action_definitions: true,
            description: true,
            push_interval: true,
            pull_interval: true,
            is_public: true,
            encryption_type_to_use: true,
            encryption_key_to_use: true,
            background_color: true,
            is_obscured_public: true,
            record_history: true,
            symphony_sid: true
        };
        if (!options.access_rights || (options.access_rights && this._folderId * 1 !== folderId * 1)) {
            data.range_access = false;
            data.action_definitions = false;
        }
        var settingsData = {};
        for (var key in data) {
            if (!data[key] || !this._data[key]) {
                continue;
            }
            settingsData[key] = this._data[key];
        }
        Page.create(folderId, name, this._data.special_page_type).then(function (newPage) {
            if (options.content && Object.keys(settingsData).length > 0) {
                newPage.Content = _this.Content;
                newPage.push(true).then(function () {
                    api.savePageSettings({ domainId: folderId, pageId: newPage.data.id, data: settingsData }).then(function () {
                        q.resolve(newPage);
                    }, q.reject);
                }, q.reject);
            }
            else if (options.content) {
                newPage.Content = _this.Content;
                newPage.push(true).then(function () {
                    q.resolve(newPage);
                }, q.reject);
            }
            else if (Object.keys(settingsData).length > 0) {
                api.savePageSettings({ domainId: folderId, pageId: newPage.data.id, data: settingsData }).then(function () {
                    q.resolve(newPage);
                }, q.reject);
            }
            else {
                q.resolve(newPage);
            }
        }, function (err) {
            q.reject(err);
        });
        return q.promise;
    };
    Page.prototype.createProvider = function (ignoreWS) {
        if (this._provider) {
            this._provider.destroy();
        }
        this._provider =
            ignoreWS || !this._supportsWS || typeof io === "undefined" || config.transport === "polling"
                ? new Provider_2.ProviderREST(this._pageId, this._folderId, this._uuid)
                : new Provider_3.ProviderSocket(this._pageId, this._folderId);
        this._checkAccess = true;
    };
    Page.prototype.init = function (ignoreWS) {
        var _this = this;
        if (!this._supportsWS || typeof io === "undefined") {
            console.warn("[iPushPull] Cannot use websocket technology as it is not supported or websocket library is not included");
        }
        this.createProvider(ignoreWS);
        if (this._uuid) {
            this._accessLoaded = true;
            this.registerListeners();
            return;
        }
        this.Ranges = new Range_2.Ranges(this._folderId, this._pageId);
        this.Ranges.on(this.Ranges.EVENT_UPDATED, function () {
            _this.emit(_this.EVENT_RANGES_UPDATED);
        });
        this.checkPageAccess();
        this.registerListeners();
        this.setFreezeRange();
    };
    Page.prototype.setFreezeRange = function () {
        var _this = this;
        this.Ranges.ranges.forEach(function (range) {
            if (range.name === "frozen_rows") {
                _this.freeze.row = range.count;
            }
            else if (range.name === "frozen_cols") {
                _this.freeze.col = range.count;
            }
        });
        this.freeze.valid = this.freeze.row > 0 || this.freeze.col > 0;
    };
    Page.prototype.checkPageAccess = function () {
        var _this = this;
        if (!this._checkAccess) {
            return;
        }
        this.getPageAccess().finally(function () {
            $timeout(function () {
                _this.checkPageAccess();
            }, 30000);
        });
    };
    Page.prototype.getPageId = function (folderName, pageName) {
        var q = $q.defer();
        api.getPageByName({ domainId: folderName, pageId: pageName }).then(function (res) {
            q.resolve({ pageId: res.data.id, folderId: res.data.domain_id, wsEnabled: res.ws_enabled });
        }, function (err) {
            q.reject(err);
        });
        return q.promise;
    };
    Page.prototype.getPageAccess = function () {
        var _this = this;
        var q = $q.defer();
        api.getPageAccess({
            domainId: this._folderId,
            pageId: this._pageId
        }).then(function (res) {
            if (res.httpCode && res.httpCode >= 300) {
                q.reject({
                    code: res.httpCode,
                    message: res.data ? res.data.detail : "Unauthorized access"
                });
                return;
            }
            if (_this._error) {
                _this._error = false;
                _this.emit(_this.EVENT_READY, true);
                return;
            }
            var prevAccess = JSON.stringify(_this._access);
            var newAccess = JSON.stringify(res.data);
            _this._access = res.data;
            _this._accessLoaded = true;
            _this.checkReady();
            if (prevAccess !== newAccess) {
                _this.emit(_this.EVENT_ACCESS_UPDATED, {
                    before: prevAccess,
                    after: newAccess
                });
            }
            if (_this._access.ws_enabled && !(_this._provider instanceof Provider_3.ProviderSocket)) {
                if (_this._supportsWS && !_this._wsDisabled) {
                    _this._provider.destroy();
                    console.log("socket", "get");
                    _this._provider = new Provider_3.ProviderSocket(_this._pageId, _this._folderId);
                    _this.registerListeners();
                }
                else {
                    console.warn("Page should use websockets but cannot switch as client does not support them");
                }
            }
            q.resolve();
        }, function (err) {
            _this.onPageError(err);
            q.reject();
        });
        return q.promise;
    };
    Page.prototype.registerListeners = function () {
        var _this = this;
        this._provider.on("content_update", function (data) {
            data.special_page_type = _this.updatePageType(data.special_page_type);
            var currentData = _.clone(_this.Content ? _this.Content.current : 0);
            _this._data = merge(_this._data, data);
            _this.decrypt();
            _this._contentLoaded = true;
            _this.checkReady();
            var diff = currentData ? utils.comparePageContent(currentData, _this.Content.current, true) : false;
            if (diff) {
                _this._data.diff = diff;
            }
            else {
                _this._data.diff = undefined;
            }
            _this._data._provider = true;
            _this.emit(_this.EVENT_NEW_CONTENT, _this._data);
        });
        this._provider.on("meta_update", function (data) {
            data.special_page_type = _this.updatePageType(data.special_page_type);
            delete data.content;
            delete data.encrypted_content;
            if (!_this.Ranges) {
                _this.Ranges = new Range_2.Ranges(data.domain_id, data.id);
                _this.Ranges.on(_this.Ranges.EVENT_UPDATED, function () {
                    _this.emit(_this.EVENT_RANGES_UPDATED);
                });
            }
            var rangesUpdates = false;
            if (data.range_access && _this._data.range_access !== data.range_access) {
                _this.Ranges.parse(data.range_access || "[]");
                data.access_rights = JSON.stringify(_this.Ranges.ranges);
                rangesUpdates = true;
            }
            if (_this._data.action_definitions !== data.action_definitions) {
                rangesUpdates = true;
            }
            _this._data = extend({}, _this._data, data);
            _this._metaLoaded = true;
            _this.checkReady();
            _this.emit(_this.EVENT_NEW_META, data);
            if (rangesUpdates) {
                _this.emit(_this.EVENT_RANGES_UPDATED);
            }
            if (data.ws_enabled && !(_this._provider instanceof Provider_3.ProviderSocket)) {
                if (_this._supportsWS && !_this._wsDisabled) {
                    _this._provider.destroy();
                    _this._provider = new Provider_3.ProviderSocket(_this._pageId, _this._folderId);
                    _this.registerListeners();
                }
                else {
                    console.warn("Page should use websockets but cannot switch as client does not support them");
                }
            }
        });
        this._provider.on("error", this.onPageError);
        this._provider.on("empty_update", function () {
            _this.emit(_this.EVENT_EMPTY_UPDATE, _this._data);
        });
    };
    Page.prototype.pushFull = function (content, otherRequestData) {
        var _this = this;
        if (otherRequestData === void 0) { otherRequestData = {}; }
        var q = $q.defer();
        if (this._data.encryption_type_to_use) {
            if (!this._encryptionKeyPush || this._data.encryption_key_to_use !== this._encryptionKeyPush.name) {
                q.reject("None or wrong encryption key");
                return q.promise;
            }
            var encrypted = this.encrypt(this._encryptionKeyPush, content);
            if (encrypted) {
                this._data.encrypted_content = encrypted;
                this._data.encryption_type_used = 1;
                this._data.encryption_key_used = this._encryptionKeyPush.name;
                this._encryptionKeyPull = _.clone(this._encryptionKeyPush);
            }
            else {
                q.reject("Encryption failed");
                return q.promise;
            }
        }
        else {
            this._data.encryption_key_used = "";
            this._data.encryption_type_used = 0;
            this._data.content = content;
        }
        var data = {
            content: !this._data.encryption_type_used ? this._data.content : "",
            encrypted_content: this._data.encrypted_content,
            encryption_type_used: this._data.encryption_type_used,
            encryption_key_used: this._data.encryption_key_used
        };
        data = extend({}, data, otherRequestData);
        var requestData = {
            domainId: this._folderId,
            pageId: this._pageId,
            data: data
        };
        api.savePageContent(requestData).then(function (res) {
            _this._data.seq_no = res.data.seq_no;
            q.resolve(res);
        }, q.reject);
        return q.promise;
    };
    Page.prototype.pushDelta = function (data, otherRequestData) {
        if (otherRequestData === void 0) { otherRequestData = {}; }
        var q = $q.defer();
        data = extend({}, data, otherRequestData);
        var requestData = {
            domainId: this._folderId,
            pageId: this._pageId,
            data: data
        };
        api.savePageContentDelta(requestData).then(q.resolve, q.reject);
        return q.promise;
    };
    Page.prototype.checkReady = function () {
        if (this._contentLoaded && this._metaLoaded && this._accessLoaded && !this.ready) {
            this.ready = true;
            this.emit(this.EVENT_READY);
        }
    };
    Page.prototype.updatePageType = function (pageType) {
        if ((pageType > 0 && pageType < 5) || pageType === 7) {
            pageType += 1000;
        }
        return pageType;
    };
    Page.prototype.encrypt = function (key, content) {
        return crypto.encryptContent(key, content);
    };
    return Page;
}(Emitter_1.default));
//# sourceMappingURL=Page.js.map

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var hasOwn = Object.prototype.hasOwnProperty;
var toStr = Object.prototype.toString;
var defineProperty = Object.defineProperty;
var gOPD = Object.getOwnPropertyDescriptor;

var isArray = function isArray(arr) {
	if (typeof Array.isArray === 'function') {
		return Array.isArray(arr);
	}

	return toStr.call(arr) === '[object Array]';
};

var isPlainObject = function isPlainObject(obj) {
	if (!obj || toStr.call(obj) !== '[object Object]') {
		return false;
	}

	var hasOwnConstructor = hasOwn.call(obj, 'constructor');
	var hasIsPrototypeOf = obj.constructor && obj.constructor.prototype && hasOwn.call(obj.constructor.prototype, 'isPrototypeOf');
	// Not own constructor property must be Object
	if (obj.constructor && !hasOwnConstructor && !hasIsPrototypeOf) {
		return false;
	}

	// Own properties are enumerated firstly, so to speed up,
	// if last one is own, then all properties are own.
	var key;
	for (key in obj) { /**/ }

	return typeof key === 'undefined' || hasOwn.call(obj, key);
};

// If name is '__proto__', and Object.defineProperty is available, define __proto__ as an own property on target
var setProperty = function setProperty(target, options) {
	if (defineProperty && options.name === '__proto__') {
		defineProperty(target, options.name, {
			enumerable: true,
			configurable: true,
			value: options.newValue,
			writable: true
		});
	} else {
		target[options.name] = options.newValue;
	}
};

// Return undefined instead of __proto__ if '__proto__' is not an own property
var getProperty = function getProperty(obj, name) {
	if (name === '__proto__') {
		if (!hasOwn.call(obj, name)) {
			return void 0;
		} else if (gOPD) {
			// In early versions of node, obj['__proto__'] is buggy when obj has
			// __proto__ as an own property. Object.getOwnPropertyDescriptor() works.
			return gOPD(obj, name).value;
		}
	}

	return obj[name];
};

module.exports = function extend() {
	var options, name, src, copy, copyIsArray, clone;
	var target = arguments[0];
	var i = 1;
	var length = arguments.length;
	var deep = false;

	// Handle a deep copy situation
	if (typeof target === 'boolean') {
		deep = target;
		target = arguments[1] || {};
		// skip the boolean and the target
		i = 2;
	}
	if (target == null || (typeof target !== 'object' && typeof target !== 'function')) {
		target = {};
	}

	for (; i < length; ++i) {
		options = arguments[i];
		// Only deal with non-null/undefined values
		if (options != null) {
			// Extend the base object
			for (name in options) {
				src = getProperty(target, name);
				copy = getProperty(options, name);

				// Prevent never-ending loop
				if (target !== copy) {
					// Recurse if we're merging plain objects or arrays
					if (deep && copy && (isPlainObject(copy) || (copyIsArray = isArray(copy)))) {
						if (copyIsArray) {
							copyIsArray = false;
							clone = src && isArray(src) ? src : [];
						} else {
							clone = src && isPlainObject(src) ? src : {};
						}

						// Never move original objects, clone them
						setProperty(target, { name: name, newValue: extend(deep, clone, copy) });

					// Don't bring in undefined values
					} else if (typeof copy !== 'undefined') {
						setProperty(target, { name: name, newValue: copy });
					}
				}
			}
		}
	}

	// Return the modified object
	return target;
};


/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Utils_1 = __webpack_require__(4);
var merge = __webpack_require__(2);
var _ = __webpack_require__(3);
var Helpers_1 = __webpack_require__(5);
var PageContent = (function () {
    function PageContent(rawContent) {
        this.canDoDelta = true;
        this.dirty = false;
        this._original = [[]];
        this._current = [[]];
        this._newRows = [];
        this._newCols = [];
        this._defaultStyles = {
            "background-color": "FFFFFF",
            "color": "000000",
            "font-family": "sans-serif",
            "font-size": "11pt",
            "font-style": "normal",
            "font-weight": "normal",
            "height": "20px",
            "number-format": "",
            "text-align": "left",
            "text-wrap": "normal",
            "width": "64px",
            "tbs": "none",
            "rbs": "none",
            "bbs": "none",
            "lbs": "none",
            "tbc": "000000",
            "rbc": "000000",
            "bbc": "000000",
            "lbc": "000000",
            "tbw": "none",
            "rbw": "none",
            "bbw": "none",
            "lbw": "none",
        };
        this.borders = {
            widths: {
                none: 0,
                thin: 1,
                medium: 2,
                thick: 3
            },
            styles: {
                none: "solid",
                solid: "solid",
                double: "double"
            },
            names: {
                t: "top",
                r: "right",
                b: "bottom",
                l: "left",
            }
        };
        this.cellStyles = [
            "background-color",
            "color",
            "font-family",
            "font-size",
            "font-style",
            "font-weight",
            "height",
            "text-align",
            "text-wrap",
            "width",
            "vertical-align",
        ];
        if (!rawContent || rawContent.constructor !== Array || !rawContent.length || !rawContent[0].length) {
            rawContent = this.defaultContent();
        }
        this._utils = new Utils_1.default();
        this._helpers = new Helpers_1.default();
        this.update(rawContent, true);
    }
    Object.defineProperty(PageContent.prototype, "current", {
        get: function () { return this._current; },
        enumerable: true,
        configurable: true
    });
    PageContent.prototype.update = function (rawContent, clean, replace) {
        if (clean === void 0) { clean = false; }
        if (replace === void 0) { replace = false; }
        if (this.dirty && !replace) {
            return;
        }
        if (replace) {
            this.dirty = true;
            this._current = rawContent;
            return;
        }
        else {
            this._original = rawContent;
        }
        var style = merge({}, this._defaultStyles);
        this._current = this._utils.mergePageContent(this._original, this._current, style, clean);
    };
    PageContent.prototype.reset = function () {
        for (var i = 0; i < this._newRows.length; i++) {
            this._current.splice(this._newRows[i], 1);
        }
        for (var i = 0; i < this._newCols.length; i++) {
            for (var j = 0; j < this._current.length; j++) {
                this._current[j].splice(this._newCols[i], 1);
            }
        }
        this.dirty = false;
        this.update(this._original, true);
    };
    PageContent.prototype.getCellByRef = function (ref) {
        var rowCol = this._helpers.getRefIndex(ref);
        return this.getCell(rowCol[0], rowCol[1]);
    };
    PageContent.prototype.getCell = function (rowIndex, columnIndex) {
        if (!this._current[rowIndex]) {
            throw new Error("row out of bounds");
        }
        if (!this._current[rowIndex][columnIndex]) {
            throw new Error("column out of bounds");
        }
        return this._current[rowIndex][columnIndex];
    };
    PageContent.prototype.getCells = function (fromCell, toCell) {
        var cellUpdates = [];
        for (var rowIndex = fromCell.row; rowIndex <= toCell.row; rowIndex++) {
            if (!this._current[rowIndex])
                continue;
            for (var colIndex = fromCell.col; colIndex <= toCell.col; colIndex++) {
                if (!this._current[rowIndex][colIndex])
                    continue;
                if (!cellUpdates[rowIndex])
                    cellUpdates[rowIndex] = [];
                cellUpdates[rowIndex][colIndex] = this._current[rowIndex][colIndex];
            }
        }
        return cellUpdates;
    };
    PageContent.prototype.updateCellByRef = function (ref, data) {
        var rowCol = this._helpers.getRefIndex(ref);
        this.updateCell(rowCol[0], rowCol[1], data);
    };
    PageContent.prototype.updateCell = function (rowIndex, columnIndex, data) {
        if (!this._current[rowIndex]) {
            throw new Error("row out of bounds");
        }
        if (!this._current[rowIndex][columnIndex]) {
            throw new Error("column out of bounds");
        }
        data.dirty = true;
        var cell = merge({}, this._current[rowIndex][columnIndex], data);
        if (data.style) {
            cell.style = merge({}, this._current[rowIndex][columnIndex].style, data.style);
            cell.originalStyle = merge({}, this._current[rowIndex][columnIndex].style);
        }
        this._current[rowIndex][columnIndex] = cell;
    };
    PageContent.prototype.addRow = function (index, direction) {
        if (!this._current[index]) {
            index = this._current.length - 1;
            direction = "below";
        }
        var inc = direction === "below" ? 1 : 0;
        var newRowData = [];
        if (this._current.length) {
            for (var i = 0; i < this._current[index].length; i++) {
                var clone = JSON.parse(JSON.stringify(this._current[index][i]));
                clone.value = "";
                clone.formatted_value = "";
                clone.dirty = true;
                newRowData.push(clone);
            }
        }
        this._current.splice(index + inc, 0, newRowData);
        this._current = this._utils.mergePageContent(this._current);
    };
    PageContent.prototype.moveColumns = function (fromIndex, toIndex, newIndex) {
        if (!this._current[0]) {
            throw new Error("row out of bounds");
        }
        if (!this._current[0][fromIndex]) {
            throw new Error("column out of bounds");
        }
        if (!this._current[0][newIndex]) {
            throw new Error("column out of bounds");
        }
        for (var i = 0; i < this._current.length; i++) {
            var clone = void 0;
            var cells = [];
            for (var k = 0; k < this._current[i].length; k++) {
                if (k < fromIndex || k > toIndex) {
                    continue;
                }
                clone = JSON.parse(JSON.stringify(this._current[i][k]));
                clone.dirty = true;
                cells.push(clone);
            }
            var cellStr = [];
            for (var n = 0; n < cells.length; n++) {
                cellStr.push("cells[" + n + "]");
            }
            var evalStr = "this._current[i].splice(newIndex, 0, " + cellStr.join(",") + ");";
            eval(evalStr);
            if (newIndex > fromIndex) {
                this._current[i].splice(fromIndex, 1 + toIndex - fromIndex);
            }
            else {
                this._current[i].splice(fromIndex + 1 + toIndex - fromIndex, 1 + toIndex - fromIndex);
            }
        }
        this._current = this._utils.mergePageContent(this._current);
    };
    PageContent.prototype.addColumn = function (index, direction) {
        if (!this._current[0][index]) {
            index = this._current[0].length - 1;
            direction = "right";
        }
        var inc = direction === "right" ? 1 : 0;
        for (var i = 0; i < this._current.length; i++) {
            var clone = void 0;
            clone = JSON.parse(JSON.stringify(this._current[i][index]));
            clone.dirty = true;
            clone.value = "";
            clone.formatted_value = "";
            this._current[i].splice(index + inc, 0, clone);
        }
        this._current = this._utils.mergePageContent(this._current);
    };
    PageContent.prototype.removeRow = function (index) {
        if (!this._current[index] || this._current.length === 1) {
            return;
        }
        this._current.splice(index, 1);
        this._current = this._utils.mergePageContent(this._current);
    };
    PageContent.prototype.removeColumn = function (index) {
        if (!this._current[0][index] || this._current[0].length === 1) {
            return;
        }
        for (var i = 0; i < this._current.length; i++) {
            this._current[i].splice(index, 1);
        }
        this._current = this._utils.mergePageContent(this._current);
    };
    PageContent.prototype.setRowSize = function (row, value) {
        if (!this._current[row]) {
            return;
        }
        for (var colIndex = 0; colIndex < this._current[row].length; colIndex++) {
            var cell = this._current[row][colIndex];
            cell.style.height = value + "px";
            cell.dirty = true;
        }
    };
    PageContent.prototype.setColSize = function (col, value) {
        if (!this._current[0][col]) {
            return;
        }
        for (var rowIndex = 0; rowIndex < this._current.length; rowIndex++) {
            var cell = this._current[rowIndex][col];
            cell.style.width = value + "px";
            cell.dirty = true;
        }
    };
    PageContent.prototype.getDelta = function (clean) {
        if (clean === void 0) { clean = false; }
        var current = this._utils.clonePageContent(this._current);
        var deltaStructure = {
            new_rows: [],
            new_cols: [],
            content_delta: [
                {
                    row_index: 0,
                    cols: [
                        {
                            col_index: 0,
                            cell_content: {
                                value: "",
                            },
                        },
                    ],
                },
            ],
        };
        deltaStructure.content_delta = [];
        deltaStructure.new_rows = this._newRows;
        deltaStructure.new_cols = this._newCols;
        var rowMovedBy = 0;
        var colMovedBy = 0;
        for (var i = 0; i < current.length; i++) {
            var rowData = {};
            var newRow = (this._newRows.indexOf(i) >= 0);
            colMovedBy = 0;
            if (newRow) {
                rowData = {
                    row_index: i,
                    cols: [],
                };
                rowMovedBy++;
            }
            for (var j = 0; j < current[i].length; j++) {
                if (newRow) {
                    var cell = _.clone(current[i][j]);
                    if (clean) {
                        delete this._current[i][j].dirty;
                    }
                    delete cell.dirty;
                    delete cell.formatting;
                    rowData.cols.push({
                        col_index: j,
                        cell_content: cell,
                    });
                }
                else {
                    var newCol = (this._newCols.indexOf(j) >= 0);
                    if (newCol) {
                        colMovedBy++;
                    }
                    if (newCol || current[i][j].dirty) {
                        if (!Object.keys(rowData).length) {
                            rowData = {
                                row_index: i,
                                cols: [],
                            };
                        }
                        var cell = _.clone(current[i][j]);
                        if (clean) {
                            delete this._current[i][j].dirty;
                        }
                        delete cell.dirty;
                        delete cell.originalStyle;
                        delete cell.formatting;
                        rowData.cols.push({
                            col_index: j,
                            cell_content: cell,
                        });
                    }
                }
            }
            if (Object.keys(rowData).length) {
                deltaStructure.content_delta.push(rowData);
            }
        }
        return deltaStructure;
    };
    PageContent.prototype.getFull = function () {
        var content = this._utils.clonePageContent(this._current);
        for (var i = 0; i < content.length; i++) {
            for (var j = 0; j < content[i].length; j++) {
                delete content[i][j].dirty;
                delete content[i][j].originalStyle;
                delete content[i][j].formatting;
            }
        }
        return content;
    };
    PageContent.prototype.getHtml = function () {
        var left = 0;
        var width = 0;
        var height = 0;
        var cells = [];
        var html = "<table style=\"border-collapse: collapse;\">";
        for (var rowIndex = 0; rowIndex < this._current.length; rowIndex++) {
            cells[rowIndex] = [];
            var row = this._current[rowIndex];
            html += "<tr>";
            left = 0;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var cell = row[colIndex];
                var styles = this.flattenStyles(cell.style);
                html += "<td style=\"" + styles + "\"><div>" + cell.formatted_value + "</div></td>";
                left += parseFloat(cell.style.width);
                if (!rowIndex) {
                    width += parseFloat(cell.style.width);
                }
                if (!colIndex) {
                    height += parseFloat(cell.style.height);
                }
                cells[rowIndex].push({
                    value: cell.formatted_value,
                    style: styles,
                });
            }
            if (!rowIndex && row[row.length - 1].style.rbw !== "none") {
                width += parseFloat(row[row.length - 1].style.rbw);
            }
            html += "</tr>";
        }
        if (this._current[this._current.length - 1][0].style.rbw !== "none") {
            height += parseFloat(this._current[this._current.length - 1][0].style.rbw);
        }
        html += "</table>";
        return {
            width: width,
            height: height,
            html: html,
            cells: cells,
        };
    };
    PageContent.prototype.flattenStyles = function (styles) {
        var htmlStyle = {};
        this.cellStyles.forEach(function (s) {
            htmlStyle[s] = styles[s];
        });
        for (var key in this.borders.names) {
            var name_1 = this.borders.names[key];
            var width = this.borders.widths[styles[key + "bw"]] + "px";
            var style = styles[key + "bs"];
            var color = "#" + styles[key + "bc"].replace("#", "");
            htmlStyle["border-" + name_1] = width + " " + style + " " + color;
        }
        var str = "";
        for (var attr in htmlStyle) {
            var value = htmlStyle[attr];
            if ((attr === "background-color" || attr === "color") && value.indexOf("#") === -1) {
                value = "#" + value;
            }
            str += attr + ": " + value + "; ";
        }
        return str;
    };
    PageContent.prototype.defaultContent = function () {
        return [
            [
                {
                    value: "",
                    formatted_value: "",
                },
            ],
        ];
    };
    return PageContent;
}());
exports.PageContent = PageContent;
//# sourceMappingURL=Content.js.map

/***/ })
/******/ ]);
});
//# sourceMappingURL=ng-ipushpull.map