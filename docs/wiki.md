# ipushpull js

## Init

```
let ipp = ipushpull.create({
    api_url: "https://www.ipushpull.com/api/1.0",
    ws_url: "https://www.ipushpull.com",
    web_url: "https://www.ipushpull.com",
    docs_url: "https://docs.ipushpull.com",
    storage_prefix: "ipp_local",
    api_key: "",
    api_secret: "",
    transport: "polling"
});
```

## Auth

### Javascript

```
ipp.auth.on(ipp.auth.EVENT_LOGGED_IN, function () {
    console.log("ipp", "auth", "EVENT_LOGGED_IN", ipp.auth.user);
});
ipp.auth.on(ipp.auth.EVENT_LOGGED_OUT, function () {
    console.log("ipp", "auth", "EVENT_LOGGED_OUT");
});
ipp.auth.on(ipp.auth.EVENT_USER_UPDATED, function () {
    console.log("ipp", "auth", "EVENT_USER_UPDATED");
});

function login(email, password) {
    ipp.auth.login(email, password).then(function () {
        console.log("woo!");
    }).catch(function (err) {
        console.log(err);
    });
}
function logout() {
    ipp.auth.logout();
}
```

## Get page

### Javascript

```
let page = new ipp.page([page id|name], [folder id|name]);

page.on(page.EVENT_READY, () => {
    console.log("EVENT_READY");
    console.log("page data", page.data);
    console.log("page content", page.Content.current);
});

page.on(page.EVENT_NEW_CONTENT, (data) => {
    console.log("EVENT_NEW_CONTENT", data);
    console.log("EVENT_NEW_CONTENT", data.diff);
});

```

## Get domains and pages

### Javascript

```
ipp.api.getDomainsAndPages().then((res) => {
    console.log(res.data);
}, (err) => {
    console.error(err);
});
```

### Results

Authenticated users will return their own folders and pages

```
{
    "count": 4,
    "next": null,
    "previous": null,
    "domains": [
        {
            "id": 456,
            "url": "https://test.ipushpull.com/api/1.0/domains/456/",
            "by_name_url": "https://test.ipushpull.com/api/1.0/domains/name/live-test-pages/",
            "name": "live-test-pages",
            "display_name": "Live test pages",
            "description": "Live pages for testing purposes",
            "encryption_enabled": true,
            "page_access_mode": 1,
            "is_page_access_mode_selectable": true,
            "alerts_enabled": true,
            "domain_type": 0,
            "is_public": true,
            "current_user_domain_access": null,
            "current_user_domain_page_access": {
                "count": 5,
                "pages": [
                    {
                        "id": 32570,
                        "name": "randoms-10",
                        "is_users_default_page": false,
                        "push_interval": 1,
                        "pull_interval": 1,
                        "write_access": false,
                        "encryption_type_to_use": 0,
                        "encryption_key_to_use": "",
                        "encryption_type_used": 0,
                        "encryption_key_used": "",
                        "special_page_type": 0,
                        "website_url": "https://test.ipushpull.com/pages/domains/456/pages/32570",
                        "is_public": true,
                        "ws_enabled": true
                    },
                    ...
                ]
            }
        },
        ...
    ]
}
```