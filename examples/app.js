if (!Array.prototype.forEach)
Array.prototype.forEach = function (a) {
    var l = this.length;
    for (var i = 0; i < l; i++)a(this[i], i)
}

var App = (function () {

    var params;
    var ipp;
    var page;
    var pageReady = false;
    var view;
    var user;
    var clipboard;
    var clipboardData;
    var autoSave = false;
    var selectedCell;
    var selectedCellTo;
    var seqNo;
    var encryptionKey;
    var highlights = false;

    function start(p) {

        params = p;
        ipp = ipushpull.Create(p);
        page = new ipp.Page(params.page.page, params.page.folder, params.page.uuid);

        page.on(page.EVENT_READY, function () {
            console.log("ipp", "EVENT_READY");
            if (!page.decrypted) {
                var passphrase = '';
                var key = ipp.storage.user.get("key_" + page.data.domain_id, JSON.stringify(encryptionKey));
                if (key) {
                    passphrase = key.passphrase;
                } else {
                    passphrase = prompt("Please enter password");
                }
                if (passphrase) {
                    encryptionKey = {
                        name: page.data.encryption_key_to_use,
                        passphrase: passphrase
                    };
                    page.decrypt(encryptionKey);
                    if (page.decrypted) {
                        init();
                    }
                }
                return;
            }
            init();
        });

        var trackingColors = ["yellow", "red", "blue"];
        var trackingColorsIndex = 0;

        page.on(page.EVENT_NEW_CONTENT, function (data) {
            // console.log("ipp", "EVENT_NEW_CONTENT", data.diff);
            if (!pageReady) {
                return;
            }
            if (page.data.special_page_type !== 6 && data.diff) {
                if (ipp.storage.user.get("tracking") && data.seq_no > seqNo) {
                    console.log("ipp", "add history");
                    ipp.tracking.addHistory(page.data.id, {
                        modified_by: data.content_modified_by,
                        modified_by_timestamp: data.content_modified_timestamp,
                        content_diff: data.diff.content_diff,
                    }, false);
                    view.cellHistory(ipp.tracking.getHistory(page.data.id));
                    seqNo = data.seq_no;
                }
                // view.trackingHighlightClassname = trackingColors[trackingColorsIndex];
                // trackingColorsIndex++;
                // if (trackingColorsIndex == trackingColors.length) trackingColorsIndex = 0;
                if(highlights) {
                    view.setTrackingHighlights(data.diff);
                }
                renderSheet();
            }
        });

        page.on(page.EVENT_DECRYPTED, function (data) {
            // console.log("ipp", "EVENT_DECRYPTED");
            ipp.storage.user.save("key_" + page.data.domain_id, JSON.stringify(encryptionKey));
            if (page.data.encryption_type_used) {
                page.encryptionKeyPull = encryptionKey;
            }
            if (page.data.encryption_type_to_use) {
                page.encryptionKeyPush = encryptionKey;
            }
        });

        page.on(page.EVENT_ERROR, function (err) {
            console.error("EVENT_ERROR", err);
        });

    }
    function openHome() {
        window.open(params.web_url, "_blank");
    }
    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
    }
    function init() {
        seqNo = page.data.seq_no;
        ipp.auth.on(ipp.auth.EVENT_LOGGED_IN, function () {
            console.log("ipp", "auth", "EVENT_LOGGED_IN", ipp.auth.user);
            setCanEdit();
        });
        ipp.auth.getUserInfo()
            .then(function(){
                console.log('auth');
            })
            .finally(function () {
                pageReady = true;
                document.title = page.data.name;
                setUI();
                setView();
                setCanEdit();
                setBackgroundColor();
            });
    }
    function setUI() {
        $('#common-buttons').addClass('show');
        if (page.data.special_page_type !== 6) {
            $('#sheet-buttons').addClass('show');
        } else {
            $('#pdf-buttons').addClass('show');
        }
    }
    function setView() {
        if (page.data.special_page_type !== 6) {

            // setup grid
            view = new ipp.Grid("view");
            view.noAccessImages.light = "img-pattern-light";
            view.noAccessImages.dark = "img-pattern-dark";
            view.register(onSheetEvent);
            // view.setContent(page.Content._current);
            // view.fit = 'width';
            // view.fluid = true;
            // view.trackingHighlightMatchColor = true;

            if (ipp.storage.user.get("tracking")) {
                startTracking();
            } else {
                stopTracking();
            }

            renderSheet();

            // setup clipboard listener
            clipboard = new ipp.clipboard(null, true);
            clipboard.register(onClipboardEvent);
        } else {
            view = new ipp.pdf("view");
            view.fit = params.page.embed.fit;
            view.render(page.Content.current);
            view.register(function (event, data) {
                console.log("ipp", event, data);
                if (event === view.ON_LOADED) {
                    pdfCount();
                }
            });
        }
    }
    function setCanEdit() {
        // if (!page.data.is_anonymous) {
            if (page.data.encryption_type_used && !page.decrypted) {
                page.decrypt();
            }
            if (page.access && page.access.write_access) {
                view.canEdit = true;
            }
        // } else {
        //     if (!page.data.restricted_anonymous_update || hasPageId(app.anonymousUser, page.data.uuid)) {
        //         view.canEdit = true;
        //     }
        // }
    }
    function setBackgroundColor() {
        if (!page.data.background_color && !params.page.embed.backgroundColor) {
            return;
        }
        let color = "#" + (params.page.embed.backgroundColor || page.data.background_color).replace("#", "");
        let contrast = ipp.helpers.getContrastYIQ(color);
        let body = document.getElementsByTagName("body")[0];
        ipp.classy.addClass(body, contrast);
        body.style["background-color"] = ipp.helpers.validHex(color) ? color : "#FFFFFF";
    }
    function renderSheet() {
        view.cellHistory(ipp.tracking.getHistory(page.data.id));
        view.setContent(page.Content.current);
        // view.updateContent();
        view.setRanges(page.Ranges.ranges, ipp.auth.user.id);
        view.renderGrid();
        // console.log("ipp", 'render');
    }
    function startTracking() {
        ipp.storage.user.save("tracking", 1);
        ipp.tracking.enabled = true;
        view.tracking = true;
        console.log("ipp", "add history");
        ipp.tracking.addHistory(page.data.id, {
            modified_by: page.data.content_modified_by,
            modified_by_timestamp: page.data.content_modified_timestamp,
            content_diff: page.Content.current,
        }, true);
        $('#sheet-buttons .tracking-start').removeClass('show');
        $('#sheet-buttons .tracking-stop').addClass('show');
    }
    function stopTracking() {
        ipp.storage.user.remove("tracking");
        ipp.tracking.enabled = false;
        view.tracking = false;
        $('#sheet-buttons .tracking-start').addClass('show');
        $('#sheet-buttons .tracking-stop').removeClass('show');
    }
    function login(email, password) {
        ipp.auth.login(email, password)
            .then(function () {
                // setCanEdit();
            })
            .finally(function () {
                console.log("ipp", "auth");
                // setCanEdit();
                location.href = location.href;
            });
    }
    function pdfPage(direction) {
        view.page(direction);
        pdfCount();
    }
    function pdfCount() {
        $('#pdf-buttons .count').html(view.pdfPage + " of " + view.pdfPages);
    }
    function onSheetEvent(event, data) {
        console.log("ipp", "onSheetEvent", event, data);
        if (event == view.ON_CELL_SELECTED) {
            selectedCell = data.from;
            selectedCellTo = data.to;
            // renderSheet("cell_section");
        }
        if (event === view.ON_CELL_VALUE) {
            page.Content.updateCell(data.cell.position.row, data.cell.position.col, {
                value: data.value,
                formatted_value: data.value,
            });
            if (!autoSave){
                showSaveCancel();
            }
            renderSheet();
        }
        if (event === view.ON_CELL_RESET) {
            cancel();
        }
        if (event === view.ON_EDITING && data.dirty) {
            savePage();
        }        
        // if (event === view.ON_EDITING && data.cell && !data.editing) {
        //     renderSheet("editing");
        //     savePage();
        // }
        if (event === view.ON_CELL_CLICKED) {
            if (data.cell.link) {
                if (!data.cell.link.external) {
                    let parts = data.cell.link.address.split("/");
                    location.href = "/examples/?folder="+parts[0]+"&page="+parts[1];
                }
                view.hideSelector();
            } else if (data.history) {
                showCellHistory(ipp.tracking.getHistoryForCell(page.data.id, data.cell.index.row, data.cell.index.col));
                // alert(JSON.stringify(cellValues, null, 4));
            } else if (data.cell.button) {
                var cellUpdates = [];
                data.cell.button.forEach(buttonRange => {
                    let cells;

                    let cellRange = buttonRange.parseRange(data.cell.position);
                    cells = grid.getCells(cellRange.from, cellRange.to);
                    console.log("action", cells);

                    // let user: IUser = this.notifications.getUser();
                    let updates = buttonRange.run(cells, {
                        USER_FIRST_NAME: `'USER_FIRST_NAME'`,
                        USER_LAST_NAME: `'USER_LAST_NAME'`,
                        USER_EMAIL: `'USER_EMAIL'`,
                        USER_NAME: `'USER_NAME'`,
                        BUTTON_VALUE: `'BUTTON_VALUE'`
                    });
                    if (updates.length) {
                        cellUpdates = cellUpdates.concat(updates);
                    }
                });
                console.log("ipp", 'action', cellUpdates);
                cellUpdates.forEach(function (cell) {
                    page.Content.updateCell(cell.row, cell.col, cell.data);
                });
                savePage();
            }
        }
    }
    function showCellHistory(values) {
        if (!values.length) {
            return;
        }
        var table = $('#modal-tracking table');
        var style;
        table.html('');
        values.forEach(function (value) {
            var tr = $('<tr>');
            var cell = $('<td>')
            if (value.cell.style) {
                console.log(value.cell.style.color);
                ["background-color", "color"].forEach(function(attr) {
                    if (value.cell.style[attr] && value.cell.style[attr].indexOf('#') == -1) {
                        value.cell.style[attr] = '#' + value.cell.style[attr];
                    }
                });
                style = style ? jQuery.extend(style, value.cell.style) : value.cell.style;
                cell.css(style);
            }
            cell.html(value.cell.formatted_value || value.cell.value);
            tr.append(cell);
            // let name = value.modified_by.first_name ? value.modified_by.first_name + " " + value.modified_by.last_name : value.modified_by.screen_name;
            let name = value.modified_by.screen_name;
            var who = $('<td>').html(name);
            tr.append(who);
            var time = $('<td>').html(ipp.helpers.convertTime(new Date(value.modified_by_timestamp)));
            tr.append(time);
            table.append(tr);
        });
        openModal('tracking');
    }
    function freeze() {
        if (selectedCell) {
            setFreeze(selectedCell.index.row, selectedCell.index.col);
        } else {
            openModal('freeze');
        }
    }
    function setFreeze(rows, cols) {

        page.Ranges.addRange(new ipp.freezingRange("frozen_rows", ipp.freezingRange.SUBJECT_ROWS, rows));
        page.Ranges.addRange(new ipp.freezingRange("frozen_cols", ipp.freezingRange.SUBJECT_COLUMNS, cols));

        page.Ranges.save().then(function () {
            console.log("ipp", "ranges saved", page.Ranges.ranges);
            renderSheet();
        });

        // var ranges = page.Ranges.ranges.slice(0);
        // ranges.push({ "name": "frozen_rows", "subject": "rows", "count": 2 });
        // ranges.push({ "name": "frozen_cols", "subject": "cols", "count": 1 });
        // view.setRanges(ranges);
    }
    function removeFreeze() {
        page.Ranges.removeRange("frozen_rows");
        page.Ranges.removeRange("frozen_cols");
        page.Ranges.save().then(function () {
            console.log("ipp", "ranges saved", page.Ranges.ranges);
            renderSheet();
        });
    }
    function clearSorting() {
        view.clearSorting();
        view.renderGrid();
    }
    function onClipboardEvent(event, data) {
        console.log("ipp", "onClipboardEvent", event, data);
        if (event === clipboard.ON_DATA) {
            if (!data || !view.canEdit) {
                return;
            }
            clipboardData = data;
            // autoSave = false;
            // stopTracking();
            // page.stop();
            // view.setContent(data);
            // page.Content.update(clipboardData, true);
            // showSaveCancel();
            if (selectedCell) {
                var replace = confirm('Do you wish to replace entire sheet?');
                if (replace) {
                    page.Content.update(clipboardData, false, true);
                } else {
                    clipboardData.forEach(function (row, rowIndex) {
                        // if (rowIndex + selectedCell.index.row > selectedCellTo.index.row) return;
                        row.forEach(function (col, colIndex) {
                            // if (colIndex + selectedCell.index.col > selectedCellTo.index.col) return;
                            let rIndex = rowIndex + selectedCell.index.row;
                            let cIndex = colIndex + selectedCell.index.col;
                            if (!page.Content.current[rIndex] || !page.Content.current[rIndex][cIndex]) return;
                            delete col.style.width;
                            delete col.style.height;
                            console.log(col);
                            page.Content.updateCell(rIndex, cIndex, col);
                        });
                    });
                }
            } else {
                page.Content.update(clipboardData, false, true);
            }
            renderSheet();
            savePage();
        }
    }
    function copyToClipboard(text) {
        clipboard.copyTextToClipboard(view.getContentHtml());
    }
    var savingInterval;
    function savePage() {
        if (autoSave) {

            if (savingInterval) {
                clearTimeout(savingInterval);
                savingInterval = undefined;
            }

            savingInterval = setTimeout(function(){
                page.push(clipboardData && clipboardData.length).then(function () {
                    console.log("Push succeeded");
                    clipboardData = [];
                }).catch(function (err) {
                    console.error("Push failed", err);
                });
            }, 500);

        } else {
            console.log("ipp", 'save');
            showSaveCancel();
        }
    }
    // save clipboard data
    function save() {

        // var delta = page.Content.getDelta();
        // var date = new Date();
        // view.editing = false;
        
        page.push(true).then(function () {
            // if (delta && delta.length) {
            //     ipp.tracking.addDeltaHistory(page.data.id, {
            //         modified_by: ipp.auth.user,
            //         modified_by_timestamp: date.toISOString(),
            //         content_diff: delta,
            //     }, false);
            // }
            clipboardData = [];
            renderSheet();
            page.start();
        }).catch(function (err) {
            console.error("Push failed", err);
        });

        $('#sheet-buttons .save').removeClass('show');
        $('#sheet-buttons .cancel').removeClass('show');
    }
    // cancel clipboard data
    function cancel() {
        // view.hideSelector();
        page.Content.reset();
        renderSheet();
        // page.start();
        // autoSave = true;
        $('#sheet-buttons .save').removeClass('show');
        $('#sheet-buttons .cancel').removeClass('show');
    }
    // show save cancel
    function showSaveCancel() {
        $('#sheet-buttons .save').addClass('show');
        $('#sheet-buttons .cancel').addClass('show');
    }

    // modal
    function openModal(name) {
        $('#modal .view').hide();
        $('#modal-' + name).show();
        $('#modal').css('display', 'flex');
        setTimeout(function () {
            $('#modal').click(function (evt) {
                console.log("ipp", evt.target, $('#modal'));
                if (evt.target == $('#modal')[0])
                    closeModal();
            });
        }, 300);
        if (name == "search") {
            updateFoundCount();
        }
    }
    function closeModal() {
        $('#modal').unbind();
        $('#modal').css('display', 'none');
    }
    $('#modal-sorting form').submit(function (e) {
        e.preventDefault();
        var data = $('#modal-sorting form').serializeArray();
        var values = {};
        data.forEach(function (element) {
            values[element.name] = element.value;
        })
        view.clearSorting();
        view.setSorting({ 
            col: values.column > view.content[0].length ? view.content[0].length - 1 : values.column - 1, 
            direction: values.direction, 
            type: values.type 
        });
        view.renderGrid();
    })
    $('#modal-freeze form').submit(function (e) {
        e.preventDefault();
        var rows = $('#modal-freeze form input[name="rows"]').val();
        var cols = $('#modal-freeze form input[name="cols"]').val();
        setFreeze(rows, cols);
    });
    $('#modal-search form').submit(function (e) {
        e.preventDefault();
        var data = $('#modal-search form').serializeArray();
        var values = {};
        data.forEach(function (element) {
            values[element.name] = element.value;
        });
        view.hideSelector();
        view.find(values.query);
        if (values.query) {
            $('#modal-search form div').show();
            updateFoundCount();
        } else {
            $('#modal-search form div').hide();
        }
    });
    function clearFound() {
        view.find('');
        $('#modal-search form div').hide();
    }
    function find(which) {
        view.gotoFoundCell(which);
        updateFoundCount();
    }
    function updateFoundCount() {
        var html = view.foundSelection.selected + " of " + view.foundSelection.count;
        $('#modal-search form div span').html(html);
    }

    function clearFilter() {
        view.clearFilters();
        view.renderGrid();
    }
    function addFilter() {
        var filter = prompt("Filter", JSON.stringify({
            col: 3,
            type: "string",
            value: "Buy",
            exp: "equals"
        }));
        if (!filter){
            return;
        }
        view.clearFilters()
        view.setFilters(JSON.parse(filter));
        view.renderGrid();
    }

    function addColumn() {
        console.log("ipp", selectedCell);
        page.Content.addColumn(selectedCell ? selectedCell.index.col : page.Content.current[0].length);
        renderSheet();
        savePage();
    }
    function addRow() {
        console.log("ipp", selectedCell);
        page.Content.addRow(selectedCell ? selectedCell.index.row : page.Content.current.length);
        renderSheet();
        savePage();
    }
    function removeColumn() {
        console.log("ipp", selectedCell);
        page.Content.removeColumn(selectedCell ? selectedCell.index.col : page.Content.current[0].length - 1);
        renderSheet();
        savePage();
    }
    function removeRow() {
        console.log("ipp", selectedCell);
        page.Content.removeRow(selectedCell ? selectedCell.index.row : page.Content.current.length - 1);
        renderSheet();
        savePage();
    }
    function toggleHighlights() {
        highlights = !highlights;
        view.clearTrackingHighlights();
    }

    return {
        view: function () { return view },
        page: function () { return page },
        ipp: function () { return ipp },
        setAutoSave: function () {
            autoSave = !autoSave;
            if (autoSave) {
                $('#sheet-buttons .autosave').addClass('on');
            } else {
                $('#sheet-buttons .autosave').removeClass('on');
            }
        },
        // autoSave: autoSave,
        login: login,
        start: start,
        getURLParameter: getURLParameter,
        openModal: openModal,
        closeModal: closeModal,
        save: save,
        cancel: cancel,
        clearSorting: clearSorting,
        pdfPage: pdfPage,
        pdfCount: pdfCount,
        startTracking: startTracking,
        stopTracking: stopTracking,
        copyToClipboard: copyToClipboard,
        freeze: freeze,
        setFreeze: setFreeze,
        addColumn: addColumn,
        addRow: addRow,
        removeColumn: removeColumn,
        removeRow: removeRow,
        clearFilter: clearFilter,
        addFilter: addFilter,
        find: find,
        clearFound: clearFound,
        toggleHighlights: toggleHighlights,
    };

})();