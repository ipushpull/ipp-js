let env = {
    ipushpull: {
        api_url: "http://ipushpull.dev/api/1.0",
        storage_prefix: "ipp_local",
        api_key: "",
        api_secret: "",
        transport: "polling",
        page: {
            embed: {}
        },
    }
}
