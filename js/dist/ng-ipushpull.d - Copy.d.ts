import * as EventEmitter from "wolfy87-eventemitter";
declare class Api extends EventEmitter {
    constructor();
    foo(): string;
}
export default Api;
declare class Classy {
    constructor();
    hasClass(ele: HTMLElement, cls: string): boolean;
    addClass(ele: HTMLElement, cls: string): boolean;
    removeClass(ele: HTMLElement, cls: string): boolean;
}
export default Classy;
export interface IIPPConfig {
    api_url?: string;
    ws_url?: string;
    api_key: string;
    api_secret: string;
    transport?: string;
    storage_prefix?: string;
}
export declare class Config {
    private _config;
    set(config: IIPPConfig): void;
    readonly api_url: string;
    readonly ws_url: string;
    readonly api_key: string;
    readonly api_secret: string;
    readonly transport: string;
    readonly storage_prefix: string;
    $get(): IIPPConfig;
}
export interface IPageContentLink {
    external: boolean;
    address: string;
}
export interface IPageCellStyle {
    "background-color"?: string;
    "color"?: string;
    "font-family"?: string;
    "font-size"?: string;
    "font-style"?: string;
    "font-weight"?: string;
    "height"?: string;
    "number-format"?: string;
    "text-align"?: string;
    "text-wrap"?: string;
    "width"?: string;
    "tbs"?: string;
    "rbs"?: string;
    "bbs"?: string;
    "lbs"?: string;
    "tbc"?: string;
    "rbc"?: string;
    "bbc"?: string;
    "lbc"?: string;
    "tbw"?: string;
    "rbw"?: string;
    "bbw"?: string;
    "lbw"?: string;
}
export interface IPageContentCell {
    value: string;
    formatted_value?: string;
    link?: IPageContentLink;
    style?: IPageCellStyle;
    dirty?: boolean;
}
export interface IPageContent extends Array<any> {
    [index: number]: IPageContentCell[];
}
export interface IPageDeltaContentCol {
    col_index: number;
    cell_content: IPageContentCell;
}
export interface IPageDeltaContentRow {
    row_index: number;
    cols: IPageDeltaContentCol[];
}
export interface IPageDelta {
    new_rows: number[];
    new_cols: number[];
    content_delta: IPageDeltaContentRow[];
}
export interface IPageContentProvider {
    canDoDelta: boolean;
    update: (rawContent: IPageContent, isCurrent: boolean) => void;
    reset: () => void;
    getCell: (rowIndex: number, columnIndex: number) => IPageContentCell;
    updateCell: (rowIndex: number, columnIndex: number, data: IPageContentCell) => void;
    addRow: () => void;
    addColumn: () => void;
    removeRow: () => void;
    removeColumn: () => void;
    getDelta: () => IPageDelta;
    getFull: () => IPageContent;
    cleanDirty: () => void;
}
export declare class PageContent implements IPageContentProvider {
    canDoDelta: boolean;
    private _original;
    private _current;
    private _newRows;
    private _newCols;
    private _utils;
    readonly current: IPageContent;
    constructor(rawContent: IPageContent);
    update(rawContent: IPageContent, isCurrent: boolean): void;
    reset(): void;
    getCell(rowIndex: number, columnIndex: number): IPageContentCell;
    updateCell(rowIndex: number, columnIndex: number, data: IPageContentCell): void;
    addRow(index?: number): IPageContentCell[];
    addColumn(index?: number): void;
    removeRow(): void;
    removeColumn(): void;
    getDelta(): IPageDelta;
    getFull(): IPageContent;
    cleanDirty(): void;
}
declare class Emitter {
    listeners: any;
    onListeners: any;
    constructor();
    on: (name: string, callback: any) => void;
    off: (name: string, callback: any) => void;
    register: (callback: any) => void;
    unRegister(callback: any): void;
    emit(name: string, args: any): void;
}
export default Emitter;
declare class Helpers {
    constructor();
    validHex(hex: string): boolean;
    rgbToHex(rgb: string): string;
    componentToHex(c: any): string;
    addEvent(element: HTMLElement, eventName: string, func: any): void;
    capitalizeFirstLetter(string: string): string;
    removeEvent(element: HTMLElement, eventName: string, func: any): void;
    isTouch(): boolean;
    clickEvent(): string;
    openWindow(link: string, target?: string, params?: {
        [s: string]: string;
    }): Window;
    createSlug(str: string): string;
    getScrollbarWidth(): number;
}
export default Helpers;
export interface IPageProvider {
    start: () => void;
    stop: () => void;
    destroy: () => void;
}
export declare class ProviderREST extends EventEmitter implements IPageProvider {
    private _pageId;
    private _folderId;
    private _stopped;
    private _requestOngoing;
    private _timer;
    private _timeout;
    private _seqNo;
    seqNo: number;
    constructor(_pageId?: number, _folderId?: number);
    start(): void;
    stop(): void;
    destroy(): void;
    private startPolling();
    private load(ignoreSeqNo?);
    private tempGetContentOb(data);
    private tempGetSettingsOb(data);
}
export declare class ProviderSocket extends EventEmitter implements IPageProvider {
    private _pageId;
    private _folderId;
    static readonly SOCKET_EVENT_PAGE_ERROR: string;
    static readonly SOCKET_EVENT_PAGE_CONTENT: string;
    static readonly SOCKET_EVENT_PAGE_PUSH: string;
    static readonly SOCKET_EVENT_PAGE_SETTINGS: string;
    static readonly SOCKET_EVENT_PAGE_DATA: string;
    static readonly SOCKET_EVENT_PAGE_USER_JOINED: string;
    static readonly SOCKET_EVENT_PAGE_USER_LEFT: string;
    private _stopped;
    private _socket;
    private _wsUrl;
    constructor(_pageId?: number, _folderId?: number);
    start(): void;
    stop(): void;
    destroy(hard?: boolean): void;
    private init();
    private connect();
    private onConnect;
    private onReconnectError;
    private onDisconnect;
    private onRedirect;
    private onPageContent;
    private onPageSettings;
    private onPageError;
    private onOAuthError;
    private onAuthRefresh;
    private supportsWebSockets;
}
import { IIPPConfig } from "./Config";
export interface IStorageProvider {
    prefix: string;
    suffix: string;
    create: (key: string, value: string, expireDays?: number) => void;
    save: (key: string, value: string, expireDays?: number) => void;
    get: (key: string, defaultValue?: any) => any;
    remove: (key: string) => void;
}
export interface IStorageService {
    user: IStorageProvider;
    global: IStorageProvider;
    persistent: IStorageProvider;
}
export declare class StorageService {
    static $inject: string[];
    constructor(ippConfig: IIPPConfig);
}
import Emitter from "./Emitter";
declare class Table extends Emitter {
    readonly ON_CELL_CLICKED: string;
    readonly ON_CELL_DOUBLE_CLICKED: string;
    readonly ON_TAG_CLICKED: string;
    readonly ON_CELL_CHANGED: string;
    data: any;
    accessRanges: any;
    initialized: boolean;
    dirty: boolean;
    rendering: boolean;
    tags: boolean;
    isTouch: boolean;
    container: string;
    containerEl: any;
    viewEl: any;
    zoomEl: any;
    scrollEl: any;
    tableEl: any;
    tableHeadEl: any;
    tableBodyEl: any;
    tableFrozenRowsEl: any;
    tableFrozenColsEl: any;
    private validStyles;
    private _editing;
    private _tracking;
    private numOfCells;
    private classy;
    private helpers;
    private freeze;
    private freezeRange;
    private accessRangesClassNames;
    private clicked;
    constructor(container: string);
    editing: boolean;
    tracking: boolean;
    init(): boolean;
    render(data: any): boolean;
    update(data: any, content: any): boolean;
    getData(): any;
    setData(data: any): void;
    setAccessRanges(rangeAccess: any): void;
    setRanges(ranges: any, userId?: number): void;
    generateBlankData(): any;
    destroy(): void;
    getEditCell(): any;
    addRow(index: number, data: any): void;
    getRowIndex(): number;
    addColumn(): void;
    getColumnIndex(): number;
    setEditCell(el: any): any;
    freezePanes(): void;
    private renderRow(rowId, rowData);
    private renderCells(row, cellsData, startIndex);
    private setCellValues(cell, cellsData);
    private clickCellLink;
    private processLink(linkData);
    private getCellValue(cellData, escapeHtml);
    private onTableClick;
    private onWindowScroll;
    private showInput(cell);
    private nextCellInput(cell, direction?);
    private getClickedCell(clickedEl);
    private softMerges(rowEl);
    private updateFreezePanes(target);
    private clearCellClassNames(classNames);
}
export default Table;
export default class Test extends EventEmitter {
    constructor();
}
