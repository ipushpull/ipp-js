"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Action = (function () {
    function Action(markup) {
        this.markup = markup;
        this.valid = false;
        this.type = "";
        this.cellRange = {
            from: {
                row: 0,
                col: 0,
            },
            to: {
                row: 0,
                col: 0,
            }
        };
        this.func = "value";
        this.exp = "++";
        this.validFuncs = ["value", "row"];
        this.alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        this.styleConditions = [];
        if (typeof markup !== "string") {
            return;
        }
        if (markup.indexOf("_BUTTON") === 0) {
            this.type = "button";
            this.valid = this.initButton(markup);
        }
        if (markup.indexOf("_STYLE") === 0) {
            this.type = "style";
            this.valid = this.initStyle(markup);
            if (this.valid) {
                this.matchCondition();
            }
        }
    }
    Action.prototype.run = function (cells) {
        var _this = this;
        var cellUpdates = [];
        cells.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                cellUpdates.push(_this.data(rowIndex, colIndex, col));
            });
        });
        return cellUpdates;
    };
    Action.prototype.getAction = function (value) {
        var conditions = [];
        this.styleConditions.forEach(function (condition) {
            conditions.push(JSON.stringify(condition).replace(/,/g, ";"));
        });
        return "_STYLE(" + value + "," + (conditions.join("|")) + ")";
    };
    Action.prototype.data = function (row, col, cell) {
        var data = {
            value: cell.value,
            formatted_value: cell.formatted_value,
        };
        var value;
        if (cell.action.valid && cell.action.type === "style") {
            value = cell.action.value;
        }
        else {
            value = cell.value;
        }
        value = parseFloat(value);
        value = eval(this.exp);
        if (cell.action.valid && cell.action.type === "style") {
            value = cell.action.getAction(value);
        }
        data.value = value;
        data.formatted_value = value;
        return {
            row: row,
            col: col,
            data: data
        };
    };
    Action.prototype.getArguments = function (markup) {
        var regExp = /\(([^)]+)\)/;
        return regExp.exec(markup);
    };
    Action.prototype.initStyle = function (markup) {
        var _this = this;
        var matches = this.getArguments(markup);
        if (!matches || !matches.length)
            return false;
        var parts = matches[1].split(",");
        if (parts.length < 1)
            return false;
        this.value = parts[0];
        var conditions = parts[1].split("|");
        conditions.forEach2(function (string) {
            _this.styleConditions.push(JSON.parse(string.replace(/;/g, ",")));
        });
        return true;
    };
    Action.prototype.initButton = function (markup) {
        var matches = this.getArguments(markup);
        if (!matches || !matches.length)
            return false;
        var parts = matches[1].split(",");
        if (parts.length < 3)
            return false;
        this.value = parts[0];
        this.setCellRange(parts[1]);
        this.exp = parts[2];
        return true;
    };
    Action.prototype.setCellRange = function (string) {
        var _this = this;
        var cells = string.toUpperCase().split(":");
        if (cells.length < 2) {
            cells.push(cells[0]);
        }
        cells.forEach2(function (cell, index) {
            var col = 0;
            var colLastIndex = 0;
            var row = "0";
            if (index >= 2)
                return;
            var parts = cell.split("");
            parts.forEach2(function (part, partIndex) {
                if (_this.alpha.indexOf(part) > -1) {
                    if (col) {
                        col += 26 + _this.alpha.indexOf(part);
                    }
                    else {
                        col = _this.alpha.indexOf(part);
                    }
                }
                else {
                    colLastIndex = partIndex;
                }
            });
            row = parts.slice(colLastIndex, parts.length).join("");
            var where = index === 0 ? "from" : "to";
            _this.cellRange[where].col = col;
            _this.cellRange[where].row = parseInt(row, 10) - 1;
        });
    };
    Action.prototype.matchCondition = function () {
        var _this = this;
        this.conditionMatch = undefined;
        this.styleConditions.forEach(function (data) {
            var value = _this.value;
            if (eval(data.condition)) {
                _this.conditionMatch = data.style;
            }
        });
    };
    return Action;
}());
exports.default = Action;
//# sourceMappingURL=Action.js.map