"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var uuidV4 = require("uuid/v4");
var Emitter_1 = require("./Emitter");
var merge = require("merge");
var Request = (function () {
    function Request(method, url) {
        this._headers = {};
        this._cache = false;
        this._overrideLock = false;
        this._json = true;
        this._reponse_type = '';
        this._method = method;
        this._url = url;
        this._headers = {
            "Content-Type": "application/json",
            "x-requested-with": "XMLHttpRequest",
            "x-ipp-device-uuid": Request.xipp.uuid,
            "x-ipp-client": Request.xipp.client,
            "x-ipp-client-version": Request.xipp.clientVersion
        };
        if (Request.xipp.hsts) {
            this._headers["Strict-Transport-Security"] = "max-age=15768000;includeSubDomains";
        }
    }
    Request.get = function (url) {
        return new Request("GET", url);
    };
    Request.post = function (url) {
        return new Request("POST", url);
    };
    Request.put = function (url) {
        return new Request("PUT", url);
    };
    Request.del = function (url) {
        return new Request("DELETE", url);
    };
    Object.defineProperty(Request.prototype, "METHOD", {
        get: function () {
            return this._method;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "URL", {
        get: function () {
            return this._url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "HEADERS", {
        get: function () {
            return this._headers;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "DATA", {
        get: function () {
            return this._data;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "PARAMS", {
        get: function () {
            return this._params;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "CACHE", {
        get: function () {
            return this._cache;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "OVERRIDE_LOCK", {
        get: function () {
            return this._overrideLock;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "JSON", {
        get: function () {
            return this._json;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Request.prototype, "RESPONSE_TYPE", {
        get: function () {
            return this._reponse_type;
        },
        enumerable: true,
        configurable: true
    });
    Request.prototype.method = function (method) {
        this._method = method;
        return this;
    };
    Request.prototype.url = function (url) {
        this._url = url;
        return this;
    };
    Request.prototype.headers = function (headers, overwrite) {
        if (overwrite === void 0) { overwrite = false; }
        this._headers = overwrite ? headers : merge.recursive(true, this._headers, headers);
        return this;
    };
    Request.prototype.data = function (data, serialize) {
        if (serialize === void 0) { serialize = false; }
        if (serialize) {
            this._data = Object.keys(data)
                .map(function (k) {
                return encodeURIComponent(k) + "=" + encodeURIComponent(data[k]);
            })
                .join("&");
        }
        else {
            this._data = data;
        }
        return this;
    };
    Request.prototype.params = function (params, overwrite) {
        if (overwrite === void 0) { overwrite = false; }
        this._params = overwrite ? params : merge.recursive(true, this._params, params);
        return this;
    };
    Request.prototype.cache = function (cache) {
        if (cache && this._method === "GET") {
            this._cache = cache;
        }
        return this;
    };
    Request.prototype.overrideLock = function (override) {
        if (override === void 0) { override = true; }
        this._overrideLock = override;
        return this;
    };
    Request.prototype.json = function (json) {
        if (json === void 0) { json = true; }
        this._json = json;
        return this;
    };
    Request.prototype.responseType = function (str) {
        if (str === void 0) { str = ""; }
        this._reponse_type = str;
        return this;
    };
    Request.xipp = {
        uuid: '',
        client: '',
        clientVersion: '',
        hsts: true
    };
    return Request;
}());
var Api = (function (_super) {
    __extends(Api, _super);
    function Api(qs, q, storage, config, utils, req) {
        var _this = _super.call(this) || this;
        _this.qs = qs;
        _this.q = q;
        _this.storage = storage;
        _this.config = config;
        _this.utils = utils;
        _this.req = req;
        _this.tokens = {
            access_token: "",
            refresh_token: ""
        };
        _this._locked = false;
        _this.dummyRequest = function (data) {
            console.log("Api is locked down, preventing call " + data.url);
            var q = _this.q.defer();
            q.reject({
                data: {},
                status: 666,
                statusText: "Api is locked",
                config: data
            });
            return q.promise;
        };
        _this.handleSuccess = function (response) {
            return {
                success: true,
                data: response.body,
                httpCode: parseInt(response.statusCode, 10),
                httpText: response.statusMessage
            };
            var q = _this.q.defer();
            q.resolve({
                success: true,
                data: response.body,
                httpCode: parseInt(response.statusCode, 10),
                httpText: response.statusMessage
            });
            return q.promise;
        };
        _this.request = req;
        _this._endPoint = "" + _this.config.api_url;
        var uuid = storage.persistent.get('ipp_uuid', '', true);
        if (!uuid) {
            uuid = uuidV4();
            storage.persistent.save('ipp_uuid', uuid, 365, true);
        }
        Request.xipp = {
            uuid: _this.config.uuid || uuid,
            client: _this.config.api_key,
            clientVersion: _this.config.client_version || '1.0',
            hsts: _this.config.hsts === undefined || _this.config.hsts,
        };
        return _this;
    }
    Object.defineProperty(Api.prototype, "EVENT_401", {
        get: function () {
            return "401";
        },
        enumerable: true,
        configurable: true
    });
    Api.prototype.block = function () {
        this._locked = true;
    };
    Api.prototype.unblock = function () {
        this._locked = false;
    };
    Api.prototype.getSelfInfo = function () {
        return this.send(Request.get(this._endPoint + "/users/self/")
            .cache(false)
            .overrideLock());
    };
    Api.prototype.refreshAccessTokens = function (refreshToken) {
        return this.send(Request.post(this._endPoint + "/oauth/token/")
            .data({
            grant_type: "refresh_token",
            client_id: this.config.api_key,
            client_secret: this.config.api_secret,
            refresh_token: refreshToken
        }, true)
            .headers({
            "Content-Type": "application/x-www-form-urlencoded",
        })
            .json(false)
            .overrideLock());
    };
    Api.prototype.userLogin = function (data) {
        return this.send(Request.post(this._endPoint + "/oauth/token/")
            .data({
            grant_type: "password",
            client_id: this.config.api_key,
            client_secret: this.config.api_secret,
            username: data.email,
            password: data.password
        }, true)
            .headers({
            "Content-Type": "application/x-www-form-urlencoded"
        })
            .json(false));
    };
    Api.prototype.userLoginByCode = function (data) {
        return this.send(Request.post(this._endPoint + "/oauth/token/")
            .params(merge({
            grant_type: "authorization_code",
            client_id: this.config.api_key,
            client_secret: this.config.api_secret
        }, data))
            .headers({
            "Content-Type": "application/x-www-form-urlencoded"
        }));
    };
    Api.prototype.userLogout = function (data) {
        if (data === void 0) { data = {}; }
        return this.send(Request.post(this._endPoint + "/oauth/logout/").params({ all: data.all || "" }));
    };
    Api.prototype.getDomains = function () {
        return this.send(Request.get(this._endPoint + "/domains/"));
    };
    Api.prototype.getDomain = function (domainId) {
        return this.send(Request.get(this._endPoint + "/domains/" + domainId + "/"));
    };
    Api.prototype.createFolder = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/").data(data.data));
    };
    Api.prototype.createDomain = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/").data(data.data));
    };
    Api.prototype.updateDomain = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/").data(data.data));
    };
    Api.prototype.disableDomain = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/").data(data.data));
    };
    Api.prototype.getDomainPages = function (domainId) {
        return this.send(Request.get(this._endPoint + "/domains/" + domainId + "/page_access/"));
    };
    Api.prototype.getDomainsAndPages = function (client) {
        if (!client) {
            client = "";
        }
        return this.send(Request.get(this._endPoint + "/domain_page_access/").params({ client: client }));
    };
    Api.prototype.getPage = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/id/" + data.domainId + "/page_content/id/" + data.pageId + "/").params({ client_seq_no: data.seq_no }));
    };
    Api.prototype.getPageByName = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/name/" + data.domainId + "/page_content/name/" + data.pageId + "/").params({ client_seq_no: data.seq_no }));
    };
    Api.prototype.getPageByUuid = function (data) {
        return this.send(Request.get(this._endPoint + "/internal/page_content/" + data.uuid + "/").params({
            client_seq_no: data.seq_no
        }));
    };
    Api.prototype.getPageVerions = function (data) {
        return this.send(Request.get(this._endPoint + "/page/id/" + data.pageId + "/versions/").params({
            before: data.before,
            after: data.after,
            max: data.max,
            page: data.page
        }));
    };
    Api.prototype.getPageVerion = function (data) {
        return this.send(Request.get(this._endPoint + "/page/id/" + data.pageId + "/version/" + data.seqNo + "/"));
    };
    Api.prototype.restorePageVerion = function (data) {
        return this.send(Request.put(this._endPoint + "/page/id/" + data.pageId + "/version/" + data.seqNo + "/restore/"));
    };
    Api.prototype.getPageAccess = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/id/" + data.domainId + "/page_access/id/" + data.pageId + "/"));
    };
    Api.prototype.createPage = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/pages/").data(data.data));
    };
    Api.prototype.createPageNotification = function (data) {
        return this.send(Request.post(this._endPoint + "/page/" + data.pageId + "/notification/").data(data.data));
    };
    Api.prototype.createPageNotificationByUuid = function (data) {
        return this.send(Request.post(this._endPoint + "/page/" + data.uuid + "/notification/").data(data.data));
    };
    Api.prototype.createAnonymousPage = function (data) {
        return this.send(Request.post(this._endPoint + "/anonymous/page/").data(data.data));
    };
    Api.prototype.savePageContent = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/id/" + data.domainId + "/page_content/id/" + data.pageId + "/").data(data.data));
    };
    Api.prototype.savePageContentDelta = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/id/" + data.domainId + "/page_content_delta/id/" + data.pageId + "/").data(data.data));
    };
    Api.prototype.savePageSettings = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/pages/" + data.pageId + "/").data(data.data));
    };
    Api.prototype.deletePage = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/pages/" + data.pageId + "/"));
    };
    Api.prototype.saveUserInfo = function (data) {
        return this.send(Request.put(this._endPoint + "/users/self/").data(data));
    };
    Api.prototype.getUserMetaData = function (data) {
        return this.send(Request.get(this._endPoint + "/users/" + data.userId + "/meta/").data(data.data));
    };
    Api.prototype.saveUserMetaData = function (data) {
        return this.send(Request.put(this._endPoint + "/users/" + data.userId + "/meta/").data(data.data));
    };
    Api.prototype.deleteUserMetaData = function (data) {
        return this.send(Request.del(this._endPoint + "/users/" + data.userId + "/meta/").data(data.data));
    };
    Api.prototype.changePassword = function (data) {
        return this.send(Request.put(this._endPoint + "/credentials/self/").data(data));
    };
    Api.prototype.changeEmail = function (data) {
        return this.send(Request.put(this._endPoint + "/credentials/self/").data(data));
    };
    Api.prototype.forgotPassword = function (data) {
        return this.send(Request.post(this._endPoint + "/password_reset/").data(data));
    };
    Api.prototype.resetPassword = function (data) {
        return this.send(Request.post(this._endPoint + "/password_reset/confirm/").data(data));
    };
    Api.prototype.inviteUsers = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/invitations/").data(data.data));
    };
    Api.prototype.acceptInvitation = function (data) {
        return this.send(Request.post(this._endPoint + "/users/invitation/confirm/").data(data));
    };
    Api.prototype.refuseInvitation = function (data) {
        return this.send(Request.del(this._endPoint + "/users/invitation/confirm/").data(data));
    };
    Api.prototype.domainInvitations = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/invitations/").params({ is_complete: "False" }));
    };
    Api.prototype.userInvitations = function () {
        return this.send(Request.get(this._endPoint + "/users/self/invitations/").params({ is_complete: "False" }));
    };
    Api.prototype.domainAccessLog = function (data) {
        return this.send(Request.get(this._endPoint + "/domain_access/" + data.domainId + "/events/").params({
            page_size: data.limit
        }));
    };
    Api.prototype.domainUsers = function (data) {
        return this.send(Request.get(this._endPoint + "/domain_access/" + data.domainId + "/users/"));
    };
    Api.prototype.signupUser = function (data) {
        return this.send(Request.post(this._endPoint + "/users/signup/").data(data));
    };
    Api.prototype.activateUser = function (data) {
        return this.send(Request.post(this._endPoint + "/users/signup/confirm/").data(data));
    };
    Api.prototype.setDomainDefault = function (data) {
        return this.send(Request.put(this._endPoint + "/domain_access/" + data.domainId + "/users/self/").data(data.data));
    };
    Api.prototype.resendInvite = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/invitations/" + data.inviteId + "/resend/"));
    };
    Api.prototype.updateDomainAccess = function (data) {
        return this.send(Request.put(this._endPoint + "/domain_access/" + data.domainId + "/users/").data(data.data));
    };
    Api.prototype.removeUsersFromDomain = function (data) {
        return this.send(Request.del(this._endPoint + "/domain_access/" + data.domainId + "/users/").data(data.data));
    };
    Api.prototype.getInvitation = function (data) {
        return this.send(Request.get(this._endPoint + "/users/invitations/" + data.token + "/"));
    };
    Api.prototype.cancelInvitations = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/invitations/").data(data.data));
    };
    Api.prototype.getDomainAccessGroups = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/access_groups/"));
    };
    Api.prototype.getDomainAccessGroup = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.groupId + "/"));
    };
    Api.prototype.addDomainAccessGroup = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/access_groups/").data(data.data));
    };
    Api.prototype.putDomainAgroupMembers = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/members/").data(data.data));
    };
    Api.prototype.putDomainAgroupPages = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/pages/").data(data.data));
    };
    Api.prototype.updateDomainAgroup = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/").data(data.data));
    };
    Api.prototype.deleteDomainAGroup = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/"));
    };
    Api.prototype.getDomainPageAccess = function (data) {
        return this.send(Request.get(this._endPoint + "/domain_page_access/" + data.domainId + "/"));
    };
    Api.prototype.getDomainCustomers = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/customers/"));
    };
    Api.prototype.getDomainUsage = function (data) {
        if (data === void 0) { data = {}; }
        return this.send(Request.get(this._endPoint + "/domains/id/" + data.domainId + "/usage/").params({
            from_date: data.fromDate,
            to_date: data.toDate
        }));
    };
    Api.prototype.saveDomainPageAccess = function (data) {
        return this.send(Request.put(this._endPoint + "/domain_page_access/" + data.domainId + "/basic/").data(data.data));
    };
    Api.prototype.getTemplates = function (data) {
        return this.send(Request.get(this._endPoint + "/templates/"));
    };
    Api.prototype.saveCustomer = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/customers/").data(data.data));
    };
    Api.prototype.updateCustomer = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/customers/" + data.data.id + "/").data(data.data));
    };
    Api.prototype.removeCustomer = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/customers/" + data.customerId + "/"));
    };
    Api.prototype.getDocEmailRules = function (data) {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/docsnames/"));
    };
    Api.prototype.createDocEmailRule = function (data) {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/docsnames/").data(data.data));
    };
    Api.prototype.updateDocEmailRule = function (data) {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/docsnames/" + data.docRuleId + "/").data(data.data));
    };
    Api.prototype.deleteDocEmailRule = function (data) {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/docsnames/" + data.docRuleId + "/"));
    };
    Api.prototype.send = function (request) {
        var _this = this;
        var q = this.q.defer();
        var token = "";
        if (this.storage) {
            token = this.storage.persistent.get("access_token");
        }
        if (!token && this.tokens && this.tokens.access_token) {
            token = this.tokens.access_token;
        }
        if (token) {
            request.headers({
                Authorization: "Bearer " + token
            });
        }
        var provider = this._locked && !request.OVERRIDE_LOCK ? this.dummyRequest : this.request;
        request.cache(false);
        var r = provider({
            url: request.URL + "?" + this.qs(request.PARAMS),
            cache: request.CACHE,
            method: request.METHOD,
            data: request.DATA,
            headers: request.HEADERS,
            resolveWithFullResponse: true,
            json: request.JSON,
            responseType: request.RESPONSE_TYPE
        }, function (err, resp, body) {
            if (err || resp.statusCode >= 300) {
                if (err) {
                    q.reject(err);
                }
                else {
                    if (!request.JSON) {
                        try {
                            body = JSON.parse(body);
                        }
                        catch (e) {
                            body = {};
                        }
                    }
                    var error = {
                        method: resp.method,
                        data: body,
                        code: resp.statusCode,
                        statusCode: resp.statusCode,
                        httpCode: resp.statusCode,
                        error: _this.utils.parseApiError({ data: body }) || "Error",
                        httpText: _this.utils.parseApiError({ data: body }) || "Error",
                        message: _this.utils.parseApiError({ data: body }) || "Error"
                    };
                    if (error.code === 401 && !_this._locked && error.message !== "invalid_grant") {
                        _this.emit(_this.EVENT_401);
                    }
                    q.reject(error);
                }
            }
            else {
                if (!request.JSON) {
                    try {
                        resp.body = JSON.parse(resp.body);
                    }
                    catch (e) {
                        resp.body = {};
                    }
                }
                q.resolve(_this.handleSuccess(resp));
            }
        });
        return q.promise;
    };
    Api.prototype.getApplicationPageList = function (client) {
        if (client === void 0) { client = ""; }
        return this.send(Request.get(this._endPoint + "/application_page_list/").params({
            client: client
        }));
    };
    Api.prototype.getOrganization = function (data) {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/"));
    };
    Api.prototype.getOrganizationUsers = function (data) {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/users/").params({
            query: data.query
        }));
    };
    Api.prototype.getOrganizationUsage = function (data) {
        if (data === void 0) { data = {}; }
        return this.send(Request.get(this._endPoint + "/organizations/self/usage/").params({
            from_date: data.fromDate,
            to_date: data.toDate
        }));
    };
    Api.prototype.getOrganizationUser = function (data) {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/users/" + data.userId + "/"));
    };
    Api.prototype.createOrganizationUser = function (data) {
        return this.send(Request.post(this._endPoint + "/organizations/" + data.organizationId + "/users/").data(data.data));
    };
    Api.prototype.saveOrganizationUser = function (data) {
        return this.send(Request.put(this._endPoint + "/organizations/" + data.organizationId + "/users/" + data.userId + "/").data(data.data));
    };
    Api.prototype.getOrganizationDomains = function (data) {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/domains/"));
    };
    Api.prototype.getOrganizationDomain = function (data) {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/domains/" + data.domainId + "/"));
    };
    Api.prototype.createOrganizationDomain = function (data) {
        return this.send(Request.post(this._endPoint + "/organizations/" + data.organizationId + "/domains/").data(data.data));
    };
    Api.prototype.saveOrganizationDomain = function (data) {
        return this.send(Request.put(this._endPoint + "/organizations/" + data.organizationId + "/domains/" + data.domainId + "/").data(data.data));
    };
    Api.prototype.getSsoStatus = function (email) {
        return this.send(Request.get(this._endPoint + "/sso/status/").params({
            user: email,
        }));
    };
    Api.prototype.downloadPage = function (data) {
        return this.send(Request.get(this._endPoint + "/page/" + data.pageId + "/download/")
            .params({
            live: data.live === undefined ? "true" : "false",
            snapshot: data.snapshot === undefined ? "true" : "false"
        })
            .headers({})
            .responseType("arraybuffer"));
    };
    Api.$inject = [
        "$httpParamSerializerJQLike",
        "$q",
        "ippStorageService",
        "ippConfig",
        "ippUtilsService",
        "ippReqService"
    ];
    return Api;
}(Emitter_1.default));
exports.Api = Api;
//# sourceMappingURL=Api.js.map