import Emitter, { IEmitter } from "./Emitter";
import { IStorageService } from "./Storage";
import { IApiService } from "./Api";
import IPromise = angular.IPromise;
import IQService = angular.IQService;
import ITimeoutService = angular.ITimeoutService;
export interface IAuthService extends IEmitter {
    EVENT_LOGGED_IN: string;
    EVENT_RE_LOGGING: string;
    EVENT_LOGIN_REFRESHED: string;
    EVENT_LOGGED_OUT: string;
    EVENT_ERROR: string;
    EVENT_401: string;
    EVENT_USER_UPDATED: string;
    user: IUserSelf;
    authenticate: (force?: boolean) => IPromise<any>;
    login: (username: string, password: string) => IPromise<any>;
    logout: () => void;
}
export interface IUserSelf {
    id: number;
    url: string;
    email: string;
    screen_name: string;
    first_name: string;
    last_name: string;
    mobile_phone_number: string;
    pushbullet_id: string;
    default_domain_id: number;
    default_page_id: number;
    default_domain_name: string;
    default_page_name: string;
    pending_invitation_count: number;
    can_create_folders: boolean;
    meta_data: any[];
}
export declare class Auth extends Emitter {
    private $q;
    private $timeout;
    private ippApi;
    private storage;
    private config;
    private utils;
    static $inject: string[];
    readonly EVENT_LOGGED_IN: string;
    readonly EVENT_RE_LOGGING: string;
    readonly EVENT_LOGIN_REFRESHED: string;
    readonly EVENT_LOGGED_OUT: string;
    readonly EVENT_ERROR: string;
    readonly EVENT_401: string;
    readonly EVENT_USER_UPDATED: string;
    polling: boolean;
    private _user;
    private _authenticated;
    private _authInProgress;
    private _selfTimer;
    private _selfTimeout;
    readonly user: IUserSelf;
    constructor($q: IQService, $timeout: ITimeoutService, ippApi: IApiService, storage: IStorageService, config: any, utils: any);
    authenticate(force?: boolean, tokens?: any): IPromise<any>;
    login(username: string, password: string): IPromise<any>;
    logout(all?: boolean): void;
    private processAuth;
    private refreshTokens;
    private saveTokens;
    private getUserInfo;
    private startPollingSelf;
    private on401;
}
