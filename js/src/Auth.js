"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = require("./Emitter");
var _ = require("underscore");
var Auth = (function (_super) {
    __extends(Auth, _super);
    function Auth($q, $timeout, ippApi, storage, config, utils) {
        var _this = _super.call(this) || this;
        _this.$q = $q;
        _this.$timeout = $timeout;
        _this.ippApi = ippApi;
        _this.storage = storage;
        _this.config = config;
        _this.utils = utils;
        _this.polling = false;
        _this._user = {};
        _this._authenticated = false;
        _this._authInProgress = false;
        _this._selfTimeout = 15 * 1000;
        _this.on401 = function () {
            if (_this.polling) {
                return;
            }
            var renew = _this.storage.persistent.get("renew");
            if (renew) {
                return;
            }
            _this.ippApi.block();
            _this.ippApi.tokens.access_token = "";
            _this.storage.persistent.remove("access_token");
            _this.storage.persistent.create("renew", 1);
            _this.emit(_this.EVENT_RE_LOGGING);
            _this.authenticate(true)
                .then(function () {
                _this.storage.user.suffix = _this._user.id;
                _this.emit(_this.EVENT_LOGIN_REFRESHED);
            }, function () {
                _this.emit(_this.EVENT_ERROR);
            })
                .finally(function () {
                _this.storage.persistent.remove("renew");
                _this.ippApi.unblock();
            });
        };
        _this._selfTimeout = _.random(15, 45) * 1000;
        ippApi.on(ippApi.EVENT_401, _this.on401);
        return _this;
    }
    Object.defineProperty(Auth.prototype, "EVENT_LOGGED_IN", {
        get: function () {
            return "logged_in";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_RE_LOGGING", {
        get: function () {
            return "re_logging";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_LOGIN_REFRESHED", {
        get: function () {
            return "login_refreshed";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_LOGGED_OUT", {
        get: function () {
            return "logged_out";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_ERROR", {
        get: function () {
            return "error";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_401", {
        get: function () {
            return "401";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "EVENT_USER_UPDATED", {
        get: function () {
            return "user_updated";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Auth.prototype, "user", {
        get: function () {
            return this._user;
        },
        enumerable: true,
        configurable: true
    });
    Auth.prototype.authenticate = function (force, tokens) {
        var _this = this;
        if (force === void 0) { force = false; }
        if (tokens === void 0) { tokens = {}; }
        if (tokens && tokens.access_token) {
            this.ippApi.tokens = tokens;
        }
        var q = this.$q.defer();
        if (this._authInProgress) {
            q.reject("Auth already in progress");
            return q.promise;
        }
        this._authInProgress = true;
        if (this._authenticated && !force) {
            this._authInProgress = false;
            q.resolve();
            return q.promise;
        }
        this.processAuth(force)
            .then(function (res) {
            if (!_this._authenticated) {
                _this._authenticated = true;
                _this.storage.user.suffix = _this._user.id;
                _this.emit(_this.EVENT_LOGGED_IN);
                _this.startPollingSelf();
            }
            q.resolve();
        }, function (err) {
            _this.emit(_this.EVENT_ERROR, err);
            if (_this._authenticated) {
                _this.logout(false, true);
            }
            q.reject(err);
        })
            .finally(function () {
            _this._authInProgress = false;
        });
        return q.promise;
    };
    Auth.prototype.login = function (username, password) {
        var _this = this;
        var q = this.$q.defer();
        this.ippApi
            .userLogin({
            grant_type: "password",
            client_id: this.config.api_key,
            client_secret: this.config.api_secret,
            email: username,
            password: password
        })
            .then(function (res) {
            _this.saveTokens(res.data);
            _this.authenticate().then(q.resolve, q.reject);
        }, function (err) {
            if (err.httpCode === 400 || err.httpCode === 401) {
                switch (err.data) {
                    case "invalid_grant":
                        err.message =
                            "The username and password you entered did not match our records. Please double-check and try again.";
                        break;
                    case "invalid_client":
                        err.message =
                            "Your client doesn't have access to iPushPull system.";
                        break;
                    case "invalid_request":
                        break;
                    default:
                        break;
                }
            }
            _this.emit(_this.EVENT_ERROR, err);
            q.reject(err);
        });
        return q.promise;
    };
    Auth.prototype.logout = function (all, ignore) {
        if (all === void 0) { all = false; }
        if (ignore === void 0) { ignore = false; }
        if (!ignore) {
            this.ippApi.userLogout({ all: all });
        }
        this.ippApi.tokens = {
            access_token: "",
            refresh_token: ""
        };
        this.storage.persistent.remove("access_token");
        this.storage.persistent.remove("refresh_token");
        this.storage.persistent.remove("renew");
        this._authenticated = false;
        this._user = {};
        this.$timeout.cancel(this._selfTimer);
        this.storage.user.suffix = "GUEST";
        this.emit(this.EVENT_LOGGED_OUT);
    };
    Auth.prototype.processAuth = function (force) {
        var _this = this;
        if (force === void 0) { force = false; }
        var q = this.$q.defer();
        var accessToken = this.storage.persistent.get("access_token") || this.ippApi.tokens.access_token;
        var refreshToken = this.storage.persistent.get("refresh_token") || this.ippApi.tokens.refresh_token;
        if (accessToken && !force) {
            return this.getUserInfo();
        }
        else {
            if (refreshToken) {
                this.refreshTokens().then(function (data) {
                    _this.saveTokens(data.data);
                    _this.getUserInfo().then(q.resolve, q.reject);
                }, function (err) {
                    _this.storage.persistent.remove("refresh_token");
                    q.reject(err);
                });
            }
            else {
                q.reject("No tokens available");
            }
        }
        return q.promise;
    };
    Auth.prototype.refreshTokens = function () {
        var refreshToken = this.storage.persistent.get("refresh_token") || this.ippApi.tokens.refresh_token;
        return this.ippApi.refreshAccessTokens(refreshToken);
    };
    Auth.prototype.saveTokens = function (tokens) {
        this.storage.persistent.create("access_token", tokens.access_token, tokens.expires_in / 86400);
        this.storage.persistent.create("refresh_token", tokens.refresh_token, 365);
        this.ippApi.tokens = tokens;
        this.storage.persistent.remove("renew");
    };
    Auth.prototype.getUserInfo = function () {
        var _this = this;
        var q = this.$q.defer();
        this.ippApi.getSelfInfo().then(function (res) {
            if (!_.isEqual(_this._user, res.data)) {
                _this._user = res.data;
                _this.emit(_this.EVENT_USER_UPDATED);
            }
            q.resolve();
        }, q.reject);
        return q.promise;
    };
    Auth.prototype.startPollingSelf = function () {
        var _this = this;
        this.$timeout.cancel(this._selfTimer);
        this._selfTimer = this.$timeout(function () {
            _this.getUserInfo()
                .then(function () { }, function () { })
                .finally(function () {
                _this.startPollingSelf();
            });
        }, this._selfTimeout);
    };
    Auth.$inject = [
        "$q",
        "$timeout",
        "ippApiService",
        "ippStorageService",
        "ippConfig",
        "ippUtilsService"
    ];
    return Auth;
}(Emitter_1.default));
exports.Auth = Auth;
//# sourceMappingURL=Auth.js.map