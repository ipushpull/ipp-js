declare class Classy {
    constructor();
    hasClass(ele: HTMLElement, cls: string): boolean;
    addClass(ele: HTMLElement, cls: string): boolean;
    removeClass(ele: HTMLElement, cls: string): boolean;
}
export default Classy;
