"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Classy = (function () {
    function Classy() {
        return;
    }
    Classy.prototype.hasClass = function (ele, cls) {
        if (!ele || typeof ele.className === "undefined") {
            return false;
        }
        return (ele.className.match(new RegExp("(\\s|^)" + cls + "(\\s|$)"))) ? true : false;
    };
    Classy.prototype.addClass = function (ele, cls) {
        if (!ele) {
            return false;
        }
        ele.classList.add(cls);
    };
    Classy.prototype.removeClass = function (ele, cls) {
        if (!ele) {
            return false;
        }
        ele.classList.remove(cls);
    };
    return Classy;
}());
exports.default = Classy;
//# sourceMappingURL=Classy.js.map