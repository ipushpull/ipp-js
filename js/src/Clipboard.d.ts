import Emitter from "./Emitter";
export declare class Clipboard extends Emitter {
    readonly ON_DATA: string;
    editableAreaId: string;
    editableAreaEl: any;
    focus: boolean;
    clipboardTextPlain: string;
    private key;
    private doc;
    private validStyles;
    private excelStyles;
    private excelBorderStyles;
    private excelBorderWeights;
    private borderSides;
    private borderSyles;
    private utils;
    constructor(element: any, doc: boolean);
    init(element: any, doc: boolean): boolean;
    destroy(): any;
    copyTextToClipboard(text: any): boolean;
    private onEditFocus;
    private onEditBlur;
    private onPasteDocument;
    private getClipboardText;
    private onPaste;
    private createPastedElement;
    private getHtml;
    private onKeyDown;
    private parseText;
    private parseTable;
    private collectClipData;
    private tableColWidths;
    private getRawValue;
    private cssToStyles;
    private mapExcelBorder;
    private getTextWidth;
}
export declare class ClipboardWrap {
    static $inject: string[];
    constructor();
}
