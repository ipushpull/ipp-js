export interface IIPPConfig {
    api_url?: string;
    ws_url?: string;
    docs_url?: string;
    api_key: string;
    api_secret: string;
    transport?: string;
    storage_prefix?: string;
    cookie?: any;
}
export declare class Config {
    private _config;
    set(config: IIPPConfig): void;
    readonly api_url: string;
    readonly ws_url: string;
    readonly docs_url: string;
    readonly api_key: string;
    readonly api_secret: string;
    readonly transport: string;
    readonly storage_prefix: string;
    readonly cookie: any;
    $get(): IIPPConfig;
}
