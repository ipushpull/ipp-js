"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config = (function () {
    function Config() {
        this._config = {
            api_url: "https://www.ipushpull.com/api/1.0",
            ws_url: "https://www.ipushpull.com",
            docs_url: "https://docs.ipushpull.com",
            api_key: "",
            api_secret: "",
            transport: "",
            storage_prefix: "ipp",
            cookie: {
                oauth_access_token: "access_token",
                ouath_refresh_token: "refresh_token",
                uuid: "uuid",
            },
            client_version: "",
            uuid: "",
            hsts: true
        };
    }
    Config.prototype.set = function (config) {
        if (!config) {
            return;
        }
        for (var prop in config) {
            if (config.hasOwnProperty(prop)) {
                this._config[prop] = config[prop];
            }
        }
        if (config.api_url && !config.ws_url) {
            var parts = config.api_url.split("/");
            this._config.ws_url = parts[0] + "//" + parts[2];
        }
    };
    Object.defineProperty(Config.prototype, "api_url", {
        get: function () {
            return this._config.api_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "ws_url", {
        get: function () {
            return this._config.ws_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "docs_url", {
        get: function () {
            return this._config.docs_url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "api_key", {
        get: function () {
            return this._config.api_key;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "api_secret", {
        get: function () {
            return this._config.api_secret;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "transport", {
        get: function () {
            return this._config.transport;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "storage_prefix", {
        get: function () {
            return this._config.storage_prefix;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "cookie", {
        get: function () {
            return this._config.cookie;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "uuid", {
        get: function () {
            return this._config.uuid;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "client_version", {
        get: function () {
            return this._config.client_version;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Config.prototype, "hsts", {
        get: function () {
            return this._config.hsts;
        },
        enumerable: true,
        configurable: true
    });
    Config.prototype.$get = function () {
        return this._config;
    };
    return Config;
}());
exports.Config = Config;
//# sourceMappingURL=Config.js.map