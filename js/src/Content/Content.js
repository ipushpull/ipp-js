"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils_1 = require("../Utils");
var Helpers_1 = require("../Helpers");
var merge = require("merge");
var _ = require("underscore");
var PageContent = (function () {
    function PageContent(rawContent) {
        this.canDoDelta = true;
        this._original = [[]];
        this._current = [[]];
        this._newRows = [];
        this._newCols = [];
        if (!rawContent || rawContent.constructor !== Array || !rawContent.length || !rawContent[0].length) {
            rawContent = [
                [
                    {
                        value: "",
                        formatted_value: "",
                    },
                ],
            ];
            this.canDoDelta = false;
        }
        this._utils = new Utils_1.default();
        this._original = this._utils.clonePageContent(rawContent);
        this._current = PageStyles.decompressStyles(rawContent);
    }
    Object.defineProperty(PageContent.prototype, "current", {
        get: function () { return this._current; },
        enumerable: true,
        configurable: true
    });
    PageContent.prototype.update = function (rawContent, isCurrent) {
        if (!isCurrent) {
            this._original = this._utils.clonePageContent(rawContent);
        }
        var current = this._utils.clonePageContent(this._current);
        if (rawContent.length < current.length) {
            current.splice(rawContent.length - 1, (current.length - rawContent.length));
            this._newRows = this._newRows.filter(function (a) {
                return (a < rawContent.length);
            });
        }
        if (rawContent[0].length < current[0].length) {
            var diff = current[0].length - rawContent[0].length;
            for (var i = 0; i < current.length; i++) {
                current[i].splice(rawContent[0].length - 1, diff);
            }
        }
        for (var i = 0; i < this._newRows.length; i++) {
            rawContent.splice(this._newRows[i], 0, current[i]);
        }
        for (var i = 0; i < this._newCols.length; i++) {
            for (var j = 0; j < rawContent.length; j++) {
                if (this._newRows.indexOf(j) >= 0 || j >= current.length) {
                    continue;
                }
                rawContent[j].splice(this._newCols[i], 0, current[j][i]);
            }
        }
        for (var i = 0; i < rawContent.length; i++) {
            if (!current[i]) {
                current.push(rawContent[i]);
                continue;
            }
            for (var j = 0; j < rawContent[i].length; j++) {
                if (!current[i][j]) {
                    current[i].push(rawContent[i][j]);
                    continue;
                }
                if (current[i][j].dirty) {
                    current[i][j].style = rawContent[i][j].style;
                    continue;
                }
                current[i][j] = rawContent[i][j];
            }
        }
        if (!current[0].length) {
            current[0][0] = { value: "", formatted_value: "" };
        }
        this._current = PageStyles.decompressStyles(current);
    };
    PageContent.prototype.reset = function () {
        for (var i = 0; i < this._newRows.length; i++) {
            this._current.splice(this._newRows[i], 1);
        }
        for (var i = 0; i < this._newCols.length; i++) {
            for (var j = 0; j < this._current.length; j++) {
                this._current[j].splice(this._newCols[i], 1);
            }
        }
        this.cleanDirty();
        this.update(this._original);
    };
    PageContent.prototype.getCell = function (rowIndex, columnIndex) {
        if (!this._current[rowIndex]) {
            throw new Error("row out of bounds");
        }
        if (!this._current[rowIndex][columnIndex]) {
            throw new Error("column out of bounds");
        }
        return this._current[rowIndex][columnIndex];
    };
    PageContent.prototype.getCells = function (fromCell, toCell) {
        var cellUpdates = [];
        for (var row = fromCell.row; row <= toCell.row; row++) {
            for (var col = fromCell.col; col <= toCell.col; col++) {
                if (!cellUpdates[row])
                    cellUpdates[row] = [];
                cellUpdates[row][col] = this._current[row][col];
            }
        }
        return cellUpdates;
    };
    PageContent.prototype.updateCell = function (rowIndex, columnIndex, data) {
        if (!this._current[rowIndex]) {
            throw new Error("row out of bounds");
        }
        if (!this._current[rowIndex][columnIndex]) {
            throw new Error("column out of bounds");
        }
        if (this._original[rowIndex] && this._original[rowIndex][columnIndex]) {
            data.dirty = (this._original[rowIndex][columnIndex].formatted_value !== data.formatted_value);
        }
        else {
            data.dirty = true;
        }
        this._current[rowIndex][columnIndex] = merge.recursive(true, this._current[rowIndex][columnIndex], data);
    };
    PageContent.prototype.addRow = function (index) {
        if (!this._current[index]) {
            index = this._current.length - 1;
        }
        var newRowData = [];
        if (this._current.length) {
            for (var i = 0; i < this._current[index].length; i++) {
                var clone = _.clone(this._current[index][i]);
                clone.value = "";
                clone.formatted_value = "";
                clone.formatted_value = "";
                clone.dirty = true;
                newRowData.push(clone);
            }
        }
        this._current.splice(index + 1, 0, newRowData);
    };
    PageContent.prototype.addColumn = function (index) {
        if (!this._current[0][index]) {
            index = this._current[0].length - 1;
        }
        for (var i = 0; i < this._current.length; i++) {
            var clone = void 0;
            for (var k = 0; k < this._current[i].length; k++) {
                if (k !== index) {
                    continue;
                }
                clone = _.clone(this._current[i][k]);
                clone.value = "";
                clone.formatted_value = "";
                clone.formatted_value = "";
                clone.dirty = true;
            }
            this._current[i].splice(index + 1, 0, clone);
        }
    };
    PageContent.prototype.removeRow = function (index) {
        if (!this._current[index] || this._current.length === 1) {
            return;
        }
        this._current.splice(index, 1);
    };
    PageContent.prototype.removeColumn = function (index) {
        if (!this._current[0][index] || this._current[0].length === 1) {
            return;
        }
        for (var i = 0; i < this._current.length; i++) {
            this._current[i].splice(index, 1);
        }
    };
    PageContent.prototype.getDelta = function () {
        var current = PageStyles.compressStyles(this._utils.clonePageContent(this._current));
        var deltaStructure = {
            new_rows: [],
            new_cols: [],
            content_delta: [
                {
                    row_index: 0,
                    cols: [
                        {
                            col_index: 0,
                            cell_content: {
                                value: "",
                            },
                        },
                    ],
                },
            ],
        };
        deltaStructure.content_delta = [];
        deltaStructure.new_rows = this._newRows;
        deltaStructure.new_cols = this._newCols;
        var rowMovedBy = 0;
        var colMovedBy = 0;
        for (var i = 0; i < current.length; i++) {
            var rowData = {};
            var newRow = (this._newRows.indexOf(i) >= 0);
            colMovedBy = 0;
            if (newRow) {
                rowData = {
                    row_index: i,
                    cols: [],
                };
                rowMovedBy++;
            }
            for (var j = 0; j < current[i].length; j++) {
                if (newRow) {
                    var cell = _.clone(current[i][j]);
                    delete cell.dirty;
                    rowData.cols.push({
                        col_index: j,
                        cell_content: cell,
                    });
                }
                else {
                    var newCol = (this._newCols.indexOf(j) >= 0);
                    if (newCol) {
                        colMovedBy++;
                    }
                    if (newCol || current[i][j].dirty) {
                        if (!Object.keys(rowData).length) {
                            rowData = {
                                row_index: i,
                                cols: [],
                            };
                        }
                        var cell = _.clone(current[i][j]);
                        delete cell.dirty;
                        rowData.cols.push({
                            col_index: j,
                            cell_content: cell,
                        });
                    }
                }
            }
            if (Object.keys(rowData).length) {
                deltaStructure.content_delta.push(rowData);
            }
        }
        return deltaStructure;
    };
    PageContent.prototype.getFull = function () {
        var content = this._utils.clonePageContent(this._current);
        for (var i = 0; i < content.length; i++) {
            for (var j = 0; j < content[i].length; j++) {
                delete content[i][j].dirty;
            }
        }
        return PageStyles.compressStyles(content);
    };
    PageContent.prototype.cleanDirty = function () {
        for (var i = 0; i < this._current.length; i++) {
            for (var j = 0; j < this._current[i].length; j++) {
                delete this._current[i][j].dirty;
            }
        }
        this._newCols = [];
        this._newRows = [];
        this.canDoDelta = true;
    };
    return PageContent;
}());
exports.PageContent = PageContent;
var PageStyles = (function () {
    function PageStyles() {
        this.currentStyle = {};
        this.currentBorders = { top: {}, right: {}, bottom: {}, left: {} };
        this.excelStyles = {
            "text-wrap": "word-wrap",
            "tbs": "border-top-style",
            "rbs": "border-right-style",
            "bbs": "border-bottom-style",
            "lbs": "border-left-style",
            "tbc": "border-top-color",
            "rbc": "border-right-color",
            "bbc": "border-bottom-color",
            "lbc": "border-left-color",
            "tbw": "border-top-width",
            "rbw": "border-right-width",
            "bbw": "border-bottom-width",
            "lbw": "border-left-width",
        };
        this.excelBorderStyles = {
            "solid": "solid",
            "thin": "solid",
            "thick": "solid",
            "hair": "solid",
            "dash": "dashed",
            "dashed": "dashed",
            "dashdot": "dashed",
            "mediumdashed": "dashed",
            "mediumdashdot": "dashed",
            "slantdashdot": "dashed",
            "dot": "dotted",
            "dotted": "dotted",
            "hairline": "dotted",
            "mediumdashdotdot": "dotted",
            "dashdotdot": "dotted",
            "double": "double",
        };
        this.excelBorderWeights = {
            "thin": "1px",
            "medium": "1px",
            "thick": "2px",
            "hair": "1px",
            "hairline": "1px",
            "double": "3px",
        };
        this.ignoreStyles = [
            "number-format",
        ];
        this._helpers = new Helpers_1.default();
    }
    PageStyles.decompressStyles = function (content) {
        var styler = new PageStyles();
        var colWidths = [];
        for (var i = 0; i < content.length; i++) {
            for (var j = 0; j < content[i].length; j++) {
                if (i === 0) {
                    colWidths[j] = content[i][j].style && content[i][j].style.width ? content[i][j].style.width : colWidths[j - 1];
                }
                if (i == 1 && j == 0 && content[i][j].dirty) {
                }
                content[i][j].style = styler.makeStyle(content[i][j].style);
                content[i][j].style.width = colWidths[j];
            }
        }
        return content;
    };
    PageStyles.compressStyles = function (content) {
        var styler = new PageStyles();
        for (var i = 0; i < content.length; i++) {
            for (var j = 0; j < content[i].length; j++) {
                content[i][j].style = styler.reverseCellStyle(content[i][j].style);
            }
        }
        return styler.cleanUpReversed(content);
    };
    PageStyles.prototype.reset = function () {
        this.currentStyle = {};
        this.currentBorders = { top: {}, right: {}, bottom: {}, left: {} };
    };
    PageStyles.prototype.resetBorders = function () {
        this.currentBorders = { top: {}, right: {}, bottom: {}, left: {} };
    };
    PageStyles.prototype.makeStyle = function (cellStyle) {
        var styleName, style = _.clone(cellStyle);
        var numberFormat = false;
        this.currentStyle["border-top"] = "none";
        this.currentStyle["border-right"] = "none";
        this.currentStyle["border-bottom"] = "none";
        this.currentStyle["border-left"] = "none";
        for (var item in style) {
            if (!style.hasOwnProperty(item)) {
                continue;
            }
            if (item === "number-format") {
                numberFormat = true;
            }
            if (this.ignoreStyles.indexOf(item) >= 0) {
                continue;
            }
            styleName = this.excelToCSS(item);
            var prefix = "", suffix = "";
            if ((styleName === "color" || styleName === "background-color") && style[item] !== "none" && style[item].indexOf("#") < 0) {
                if (this._helpers.validHex(style[item])) {
                    prefix = "#";
                }
            }
            if (styleName === "height") {
                this.currentStyle["max-height"] = style[item];
            }
            if (styleName === "word-wrap") {
                style[item] = (style[item] === "normal") ? "normal" : "break-word";
                this.currentStyle["white-space"] = (style[item] === "normal") ? "pre" : "normal";
            }
            if (styleName.indexOf("border") === 0 && this.excelStyles[item]) {
                var parts = styleName.split("-");
                var pos = parts[1];
                if (styleName.indexOf("-style") >= 0) {
                    this.currentBorders[pos].style = this.excelBorderStyles[style[item]] || undefined;
                }
                if (styleName.indexOf("-width") >= 0) {
                    this.currentBorders[pos].width = (style[item] !== "none") ? this.excelBorderWeights[style[item]] : undefined;
                }
                if (styleName.indexOf("-color") >= 0) {
                    this.currentBorders[pos].color = (style[item] === "none") ? "transparent" : "#" + style[item];
                }
                continue;
            }
            this.currentStyle[styleName] = prefix + style[item] + suffix;
        }
        if (numberFormat) {
            this.currentStyle["white-space"] = "pre";
        }
        var resultStyles = _.clone(this.currentStyle);
        for (var borderPos in this.currentBorders) {
            if (typeof this.currentBorders[borderPos].style === "undefined" || !this.currentBorders[borderPos].style) {
                continue;
            }
            resultStyles["border-" + borderPos] = this.currentBorders[borderPos].width + " " + this.currentBorders[borderPos].style + " " + this.currentBorders[borderPos].color;
        }
        return resultStyles;
    };
    PageStyles.prototype.reverseCellStyle = function (cellStyle, fullStyles) {
        if (fullStyles === void 0) { fullStyles = true; }
        var genericStyle = {};
        for (var style in cellStyle) {
            if (!cellStyle.hasOwnProperty(style)) {
                continue;
            }
            if (/^border/.test(style)) {
                var params = style.split("-");
                if (params.length > 1) {
                    var parts = cellStyle[style].split(" ");
                    var pos = params[1].charAt(0);
                    if (params.length === 2 && parts.length === 3) {
                        genericStyle[pos + "bw"] = this.excelBorderWeight(Math.round(parseFloat(parts[0])) + "px");
                        genericStyle[pos + "bs"] = parts[1];
                        genericStyle[pos + "bc"] = parts[2].replace("#", "");
                    }
                    else if (params.length === 3) {
                        var dimension = params[2].charAt(0);
                        if (dimension === "w")
                            genericStyle[pos + "bw"] = this.excelBorderWeight(Math.round(parseFloat(cellStyle[style])) + "px");
                        if (dimension === "s")
                            genericStyle[pos + "bs"] = cellStyle[style];
                        if (dimension === "c")
                            genericStyle[pos + "bc"] = cellStyle[style].replace("#", "");
                    }
                }
            }
            else {
                if (style === "color" || style === "background-color") {
                    cellStyle[style] = cellStyle[style].replace("#", "");
                }
                if (style === "font-family") {
                    cellStyle[style] = cellStyle[style].split(",")[0];
                }
                if (cellStyle[style]) {
                    genericStyle[this.CSSToExcel(style)] = cellStyle[style].trim();
                }
            }
        }
        if (fullStyles) {
            for (var eStyle in this.excelStyles) {
                if (!this.excelStyles.hasOwnProperty(eStyle)) {
                    continue;
                }
                if (eStyle === "text-wrap") {
                    continue;
                }
                if (!genericStyle[eStyle]) {
                    genericStyle[eStyle] = "none";
                }
            }
        }
        return genericStyle;
    };
    PageStyles.prototype.cleanUpReversed = function (content) {
        var styleCurrent = {};
        for (var i = 0; i < content.length; i++) {
            for (var j = 0; j < content[i].length; j++) {
                var styleCopy = _.clone(content[i][j].style);
                for (var styleName in styleCopy) {
                    if (!styleCopy.hasOwnProperty(styleName)) {
                        continue;
                    }
                    if (styleCurrent[styleName] && (styleCurrent[styleName] === styleCopy[styleName])) {
                        delete content[i][j].style[styleName];
                    }
                    else {
                        styleCurrent[styleName] = styleCopy[styleName];
                    }
                }
                if (!Object.keys(content[i][j].style).length) {
                    delete content[i][j].style;
                }
            }
        }
        return content;
    };
    PageStyles.prototype.excelToCSS = function (val) {
        return (this.excelStyles[val]) ? this.excelStyles[val] : val;
    };
    PageStyles.prototype.CSSToExcel = function (val) {
        var excelVal = val;
        for (var style in this.excelStyles) {
            if (this.excelStyles[style] === val) {
                excelVal = style;
                break;
            }
        }
        return excelVal;
    };
    PageStyles.prototype.excelBorderWeight = function (pixels) {
        var bWeight = "";
        for (var weight in this.excelBorderWeights) {
            if (!this.excelBorderWeights.hasOwnProperty(weight)) {
                continue;
            }
            if (this.excelBorderWeights[weight] === pixels) {
                bWeight = weight;
                break;
            }
        }
        return bWeight;
    };
    PageStyles.prototype.rgbToHex = function (rgb) {
        rgb = rgb.replace("rgb(", "").replace(")", "");
        var parts = rgb.split(",");
        return this.componentToHex(parseInt(parts[0], 10)) + this.componentToHex(parseInt(parts[1], 10)) + this.componentToHex(parseInt(parts[2], 10));
    };
    PageStyles.prototype.componentToHex = function (c) {
        var hex = c.toString(16);
        return hex.length === 1 ? "0" + hex : hex;
    };
    return PageStyles;
}());
//# sourceMappingURL=Content.js.map