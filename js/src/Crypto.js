"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Crypto = (function () {
    function Crypto() {
    }
    Crypto._instance = function () {
        return new Crypto();
    };
    Crypto.prototype.decryptContent = function (key, data) {
        if (!this.libCheck()) {
            return;
        }
        if (!data)
            return undefined;
        var rawData = forge.util.decode64(data);
        var iv = rawData.substring(0, 16);
        var cleanData = rawData.substring(16);
        cleanData = forge.util.createBuffer(cleanData, "latin1");
        iv = forge.util.createBuffer(iv, "latin1");
        var decipher = forge.cipher.createDecipher("AES-CBC", this.hashPassphrase(key.passphrase));
        decipher.start({ iv: iv });
        decipher.update(cleanData);
        var pass = decipher.finish();
        var decrypted;
        try {
            decrypted = JSON.parse(decipher.output.toString());
        }
        catch (e) {
            decrypted = undefined;
        }
        return decrypted;
    };
    Crypto.prototype.encryptContent = function (key, data) {
        if (!this.libCheck()) {
            return;
        }
        var readyData = JSON.stringify(data);
        var hash = this.hashPassphrase(key.passphrase);
        var iv = forge.random.getBytesSync(16);
        var cipher = forge.cipher.createCipher("AES-CBC", hash);
        cipher.start({ iv: iv });
        cipher.update(forge.util.createBuffer(readyData, "utf8"));
        cipher.finish();
        var encrypted = cipher.output;
        var buffer = forge.util.createBuffer();
        buffer.putBytes(iv);
        buffer.putBytes(encrypted.bytes());
        var output = buffer.getBytes();
        return forge.util.encode64(output);
    };
    Crypto.prototype.hashPassphrase = function (passphrase) {
        var md = forge.md.sha256.create();
        md.update(passphrase);
        return md.digest().bytes();
    };
    Crypto.prototype.libCheck = function () {
        if (typeof forge === "undefined") {
            console.error("[iPushPull]", "If you want to use encryption make sure you include forge library in your header or use ng-ipushpull-standalone.min.js");
        }
        return typeof forge !== "undefined";
    };
    return Crypto;
}());
exports.Crypto = Crypto;
//# sourceMappingURL=Crypto.js.map