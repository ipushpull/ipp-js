export interface IEmitter {
    emit: (name: string, args?: any) => void;
    register: (callback: any) => void;
    unRegister: (callback: any) => void;
}
declare class Emitter implements IEmitter {
    listeners: any;
    onListeners: any;
    constructor();
    on: (name: string, callback: any) => void;
    off: (name: string, callback: any) => void;
    register: (callback: any) => void;
    unRegister(callback: any): void;
    emit(name: string, args?: any): void;
    removeEvent(): void;
}
export default Emitter;
