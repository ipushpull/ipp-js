"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = require("./Emitter");
var Helpers_1 = require("./Helpers");
var hot_formula_parser_1 = require("hot-formula-parser");
var Functions = (function (_super) {
    __extends(Functions, _super);
    function Functions(page, vars) {
        var _this = _super.call(this) || this;
        _this.page = page;
        _this.vars = vars;
        _this.userMapping = {
            USER_ID: 'id',
            USER_NAME: 'username',
            USER_FIRST_NAME: 'first_name',
            USER_LAST_NAME: 'last_name',
            USER_EMAIL: 'email',
        };
        _this.functions = ['VALUE', 'OFFSET', 'USER', 'SYMPHONY_TAG', 'DATE_TIME'];
        _this.helpers = new Helpers_1.default();
        _this.parser = new hot_formula_parser_1.Parser();
        _this.parser.on('callCellValue', function (cellCoord, done) {
            if (_this.page.Content.current[cellCoord.row.index] &&
                _this.page.Content.current[cellCoord.row.index][cellCoord.column.index]) {
                done(_this.page.Content.current[cellCoord.row.index][cellCoord.column.index]);
            }
            else {
                done('#ERROR!');
            }
        });
        _this.parser.setFunction('IPPVAR', function (params) {
            if (!params[0]) {
                return '#ERROR!';
            }
            var p = ("" + params[0]).toLowerCase();
            if (!_this.vars[p]) {
                return '#ERROR!';
            }
            return _this.vars[p];
        });
        _this.parser.setFunction('VALUE', function (params) {
            if (params[0] === '#ERROR!') {
                return '#ERROR!';
            }
            if (!params[0]) {
                return _this.cell ? _this.getCellValue(_this.cell) : '#ERROR!';
            }
            return _this.getCellValue(params[0]);
        });
        _this.parser.setFunction('RAWVALUE', function (params) {
            if (params[0] === '#ERROR!') {
                return '#ERROR!';
            }
            if (!params[0]) {
                return _this.cell ? _this.getCellValue(_this.cell, true) : '#ERROR!';
            }
            return _this.getCellValue(params[0], true);
        });
        _this.parser.setFunction('OFFSET', function (params) {
            if (!_this.cell || params[0] === undefined || params[1] === undefined || isNaN(params[0]) || isNaN(params[1])) {
                return '#ERROR!';
            }
            var row = parseFloat(params[0]);
            var col = parseFloat(params[1]);
            if (_this.page.Content.current[_this.cell.index.row + row] &&
                _this.page.Content.current[_this.cell.index.row + row][_this.cell.index.col + col]) {
                return _this.page.Content.current[_this.cell.index.row + row][_this.cell.index.col + col];
            }
            return '#ERROR!';
        });
        _this.parser.setFunction('DATETIME', function (params) {
            var time = moment
                .tz(params[1] || 'UTC')
                .add(params[2] || 0, params[3] || 'minutes')
                .format(params[0]);
            return time;
        });
        _this.parser.setFunction('SYTAG', function (params) {
            if (params[0] === undefined) {
                return '#ERROR!';
            }
            var message = '';
            var arg = params[1];
            switch (params[0]) {
                case 'mention':
                    if (_this.helpers.isValidEmail(arg)) {
                        message = "<mention email=\"" + arg + "\" strict=\"false\"/>";
                    }
                    else {
                        message = '#ERROR!';
                    }
                    break;
                case 'cash':
                    if (!arg) {
                        arg = "ipp." + _this.page.data.domain_name + "." + _this.page.data.name;
                    }
                    message = "<cash tag=\"" + _this.helpers.safeTitle(arg) + "\" />";
                    break;
                case 'hash':
                    message = "<hash tag=\"" + _this.helpers.safeTitle(arg) + "\" />";
                    break;
                case 'chime':
                    message = "<chime />";
                    break;
                case 'emoji':
                    message = "<emoji shortcode=\"" + _this.helpers.safeTitle(arg) + "\" />";
                    break;
                default:
                    message = '#ERROR!';
                    break;
            }
            return message;
        });
        return _this;
    }
    Functions.prototype.parse = function (str) {
        var _this = this;
        var results = [];
        var lines = str.split('\n');
        lines.forEach(function (line) {
            line = line.trim();
            if (!line || line[0] !== '=') {
                results.push(line);
                return;
            }
            var result = _this.parser.parse(_this.toUppercaseFunctions(line.replace('=', '')));
            results.push(result.result || result.error);
        });
        return results.join('<br />');
    };
    Functions.prototype.getCellReference = function (str) {
        var result = this.parser.parse(str.trim());
        return result.result || null;
    };
    Functions.prototype.setCellReference = function (cell) {
        this.cell = cell;
    };
    Functions.prototype.isButtonWithinTaskRange = function (buttonCell, taskGroup) {
        if (!taskGroup.range && taskGroup.ref !== buttonCell.button.name)
            return false;
        try {
            var range = taskGroup.ref ? buttonCell.button.range : taskGroup.range;
            var cellRange = this.helpers.cellRange(range, this.page.Content.current.length, this.page.Content.current[0].length);
            var endRow = cellRange.to.row < 0 ? this.page.Content.current.length : cellRange.to.row;
            if (buttonCell.position.row < cellRange.from.row || buttonCell.position.row > endRow)
                return false;
            if (buttonCell.position.col < cellRange.from.col || buttonCell.position.col > cellRange.to.col)
                return false;
            return true;
        }
        catch (e) {
            return false;
        }
    };
    Functions.prototype.isTaskValid = function (task) {
        if (!task.condition)
            return true;
        try {
            return this.parse(task.condition) === 'true';
        }
        catch (e) {
            return false;
        }
    };
    Functions.prototype.updateCell = function (action) {
        try {
            if (!action.cell)
                return null;
            var cell = this.getCellReference(action.cell.replace('=', '').toUpperCase());
            if (!cell)
                return null;
            var formatted_value = this.parse(action.data.formatted_value);
            var value = this.parse(action.data.value);
            if (this.helpers.isNumber(formatted_value)) {
                formatted_value = parseFloat(formatted_value);
            }
            if (this.helpers.isNumber(value)) {
                value = parseFloat(value);
            }
            var update = {
                row: cell.index.row,
                col: cell.index.col,
                data: {
                    formatted_value: formatted_value,
                    value: value,
                },
            };
            this.page.Content.updateCell(update.row, update.col, update.data);
            return update;
        }
        catch (e) {
            return null;
        }
    };
    Functions.prototype.setCellFormatting = function (styles, buttons, contentDiff) {
        var _this = this;
        if (!styles || !styles.length) {
            return [];
        }
        var cellStyles = [];
        styles.forEach(function (style) {
            var range = style.range || '';
            if (!style.range) {
                buttons.forEach(function (button) {
                    if (button.name === style.ref) {
                        range = button.range;
                    }
                });
            }
            if (!range) {
                return;
            }
            var cellRange = _this.helpers.cellRange(range, _this.page.Content.current.length, _this.page.Content.current[0].length);
            var endRow = cellRange.to.row < 0 ? _this.page.Content.current.length : cellRange.to.row;
            var _loop_1 = function (colIndex) {
                var _loop_2 = function (rowIndex) {
                    if (rowIndex < cellRange.from.row || rowIndex > endRow)
                        return "continue";
                    var cell = contentDiff && contentDiff[rowIndex] && contentDiff[rowIndex][colIndex] ? contentDiff[rowIndex][colIndex] : _this.page.Content.getCell(rowIndex, colIndex);
                    _this.setCellReference(cell);
                    style.conditions.forEach(function (condition) {
                        if (_this.parse(condition.exp) !== 'true')
                            return;
                        if (condition.columns.indexOf(-1) > -1) {
                            for (var colConditionIndex = 0; colConditionIndex < _this.page.Content.current[rowIndex].length; colConditionIndex++) {
                                if (!cellStyles[rowIndex]) {
                                    cellStyles[rowIndex] = [];
                                }
                                cellStyles[rowIndex][colConditionIndex] = condition.style;
                            }
                        }
                        else {
                            if (!cellStyles[rowIndex]) {
                                cellStyles[rowIndex] = [];
                            }
                            cellStyles[rowIndex][colIndex] = condition.style;
                        }
                    });
                };
                for (var rowIndex = 0; rowIndex < _this.page.Content.current.length; rowIndex++) {
                    _loop_2(rowIndex);
                }
            };
            for (var colIndex = cellRange.from.col; colIndex <= cellRange.to.col; colIndex++) {
                _loop_1(colIndex);
            }
        });
        return cellStyles;
    };
    Functions.prototype.toUppercaseFunctions = function (expression) {
        var regex = /("(.*?)")/gm;
        var m;
        var replace = [];
        while ((m = regex.exec(expression)) !== null) {
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            replace.push(m[1]);
        }
        replace.forEach(function (str, i) {
            expression = expression.replace(str, "[" + i + "]");
        });
        expression = expression.toUpperCase();
        replace.forEach(function (str, i) {
            expression = expression.replace("[" + i + "]", str);
        });
        return expression;
    };
    Functions.prototype.getCellValue = function (cell, raw) {
        if (raw === void 0) { raw = false; }
        var value;
        if (raw) {
            value = cell.value;
        }
        else {
            value = cell.formatted_value || cell.value;
        }
        return this.helpers.isNumber(value) ? parseFloat(value) : value;
    };
    return Functions;
}(Emitter_1.default));
exports.Functions = Functions;
//# sourceMappingURL=Functions.js.map