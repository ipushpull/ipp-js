"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = require("./Emitter");
var Classy_1 = require("./Classy");
var Helpers_1 = require("./Helpers");
var Range_1 = require("./Page/Range");
var Range_2 = require("./Page/Range");
var Range_3 = require("./Page/Range");
var _ = require("underscore");
var Hammer = require("hammerjs");
var ua = require("ua-parser-js");
var merge = require("merge");
var Grid = (function (_super) {
    __extends(Grid, _super);
    function Grid(container, fluid, options) {
        if (fluid === void 0) { fluid = false; }
        if (options === void 0) { options = {}; }
        var _this = _super.call(this) || this;
        _this.scale = 1;
        _this.ratio = 1;
        _this.ratioScale = 1;
        _this.pause = false;
        _this.canEdit = false;
        _this.scrollbars = {
            width: 8,
            inset: false,
            scrolling: false,
            x: {
                dimension: "width",
                label: "col",
                show: false,
                anchor: "left",
                track: undefined,
                handle: undefined
            },
            y: {
                dimension: "height",
                label: "row",
                show: false,
                anchor: "top",
                track: undefined,
                handle: undefined
            }
        };
        _this.alwaysEditing = false;
        _this.disallowSelection = false;
        _this.historyIndicatorSize = 16;
        _this.trackingHighlightClassname = "yellow";
        _this.trackingHighlightMatchColor = true;
        _this.trackingHighlightLifetime = 3000;
        _this.hasFocus = false;
        _this.fluid = false;
        _this.noAccessImages = {
            light: "",
            dark: ""
        };
        _this.borders = {
            widths: {
                none: 0,
                thin: 1,
                medium: 2,
                thick: 3
            },
            styles: {
                none: "solid",
                solid: "solid",
                double: "double"
            },
            names: {
                t: "top",
                r: "right",
                b: "bottom",
                l: "left"
            }
        };
        _this.originalContent = [];
        _this.content = [];
        _this.history = [];
        _this.trackingHighlights = [];
        _this.offsets = {
            x: {
                offset: 0,
                index: 0,
                max: 0,
                maxOffset: 0
            },
            y: {
                offset: 0,
                index: 0,
                max: 0,
                maxOffset: 0
            }
        };
        _this.width = 0;
        _this.height = 0;
        _this.visible = [];
        _this.gridLines = [];
        _this.found = [];
        _this.foundSelection = {
            count: 0,
            selected: 0,
            cell: undefined
        };
        _this.touch = false;
        _this.allowBrowserScroll = false;
        _this.links = [];
        _this.styleRanges = [];
        _this.buttonRanges = [];
        _this.accessRanges = [];
        _this.freezeRange = {
            valid: false,
            index: {
                row: 0,
                col: 0
            }
        };
        _this.containerRect = {
            x: 0,
            y: 0,
            height: 0,
            width: 0
        };
        _this._tracking = false;
        _this._editing = false;
        _this._fit = "scroll";
        _this.userTrackingColors = ["deeppink", "aqua", "tomato", "orange", "yellow", "limegreen"];
        _this.cellSelection = {
            from: undefined,
            to: undefined,
            last: undefined,
            go: false,
            on: false,
            clicked: 0,
            found: undefined
        };
        _this.filters = {};
        _this.sorting = {};
        _this.dirty = false;
        _this.rightClick = false;
        _this.keyValue = "";
        _this.ctrlDown = false;
        _this.shiftDown = false;
        _this.cellStyles = [
            "background-color",
            "color",
            "font-family",
            "font-size",
            "font-style",
            "font-weight",
            "height",
            "number-format",
            "text-align",
            "text-wrap",
            "width",
            "vertical-align"
        ];
        _this.init = false;
        _this.hidden = false;
        _this.drawGrid = function () {
            var stageWidth = _this.width * 1 / _this.scale * _this.ratio;
            var stageHeight = _this.height * 1 / _this.scale * _this.ratio;
            _this.gc.bufferContext.save();
            _this.gc.bufferContext.clearRect(0, 0, _this.width * _this.ratio, _this.height * _this.ratio);
            _this.gc.bufferContext.scale(_this.scale, _this.scale);
            _this.gridLines = [];
            _this.visible = [];
            var frozenCells = {
                cols: [],
                rows: [],
                common: []
            };
            var trackedCells = [];
            for (var rowIndex = 0; rowIndex < _this.content.length; rowIndex++) {
                var row = _this.content[rowIndex];
                var y = row[0].y - _this.offsets.y.offset;
                var b = row[0].y - _this.offsets.y.offset + row[0].height;
                var visibleRow = (y >= 0 && y <= stageHeight) || (b >= 0 && b <= stageHeight);
                if (visibleRow || (_this.freezeRange.valid && rowIndex < _this.freezeRange.index.row)) {
                    _this.visible[rowIndex] = [];
                }
                else {
                    continue;
                }
                if (!row[0].overlayWidth) {
                    _this.softMerges(row, rowIndex);
                }
                for (var colIndex = 0; colIndex < row.length; colIndex++) {
                    var col = row[colIndex];
                    var width = col.overlayWidth;
                    var height = col.height;
                    var xPos = col.overlayStart < colIndex ? _this.content[rowIndex][col.overlayStart].x : col.x;
                    var x = xPos - _this.offsets.x.offset;
                    var _x = col.x - _this.offsets.x.offset;
                    var y_1 = col.y - _this.offsets.y.offset;
                    var a = col.x - _this.offsets.x.offset + width;
                    var _a = col.x - _this.offsets.x.offset + col.width;
                    var b_1 = col.y - _this.offsets.y.offset + height;
                    var visibleCol = (x >= 0 && x <= stageWidth) ||
                        (a >= 0 && a <= stageWidth) ||
                        (col.overlayEnd && colIndex <= col.overlayEnd) ||
                        (col.overlayStart && colIndex >= col.overlayStart);
                    if (!col.width) {
                        continue;
                    }
                    if ((visibleRow && visibleCol) ||
                        (_this.freezeRange.valid && colIndex < _this.freezeRange.index.col) ||
                        (_this.freezeRange.valid && rowIndex < _this.freezeRange.index.row)) {
                        var originalCell = _this.originalContent[col.position.row][col.position.col];
                        _this.visible[rowIndex][colIndex] = {
                            originalCell: originalCell,
                            coords: { x: x, y: y_1, a: a, b: b_1, width: width, height: height, _x: _x, _a: _a },
                            cell: col
                        };
                        if (_this.freezeRange.valid) {
                            var freeze = "";
                            if (rowIndex < _this.freezeRange.index.row && colIndex < _this.freezeRange.index.col) {
                                x = _x = col.x;
                                a = _a = col.x + width;
                                y_1 = col.y;
                                b_1 = col.y + height;
                                freeze = "common";
                            }
                            else if (rowIndex < _this.freezeRange.index.row) {
                                y_1 = col.y;
                                b_1 = col.y + height;
                                freeze = visibleCol ? "rows" : "";
                            }
                            else if (colIndex < _this.freezeRange.index.col) {
                                x = _x = col.x;
                                a = _a = col.x + width;
                                freeze = visibleRow ? "cols" : "";
                            }
                            if (freeze) {
                                frozenCells[freeze].push({
                                    originalCell: originalCell,
                                    coords: { x: x, y: y_1, a: a, b: b_1, width: width, height: height, _x: _x, _a: _a },
                                    col: col
                                });
                                continue;
                            }
                        }
                        if (_this.tracking &&
                            _this.history[col.position.row] &&
                            _this.history[col.position.row][col.position.col] >= 0 &&
                            !col.button) {
                            trackedCells.push({
                                originalCell: originalCell,
                                coords: { x: x, y: y_1, a: a, b: b_1, width: width, height: height },
                                col: col
                            });
                        }
                    }
                }
            }
            for (var rowIndex = 0; rowIndex < _this.visible.length; rowIndex++) {
                if (!_this.visible[rowIndex])
                    continue;
                for (var colIndex = 0; colIndex < _this.visible[rowIndex].length; colIndex++) {
                    if (!_this.visible[rowIndex][colIndex])
                        continue;
                    var col = _this.visible[rowIndex][colIndex];
                    if (col.cell.allow !== "no" || !_this.imgPattern) {
                        _this.drawOffsetRect(col.originalCell, col.coords, col.cell);
                    }
                }
            }
            for (var rowIndex = 0; rowIndex < _this.visible.length; rowIndex++) {
                if (!_this.visible[rowIndex])
                    continue;
                for (var colIndex = 0; colIndex < _this.visible[rowIndex].length; colIndex++) {
                    if (!_this.visible[rowIndex][colIndex])
                        continue;
                    var col = _this.visible[rowIndex][colIndex];
                    _this.drawGraphics(col.originalCell, col.coords, col.cell);
                }
            }
            _this.gridLines.forEach2(function (line) {
                _this.drawLine(line[0], line[1], line[2]);
            });
            trackedCells.forEach2(function (cell) {
                _this.drawTrackingRect(cell.coords, cell.col);
            });
            for (var freeze in frozenCells) {
                _this.gridLines = [];
                frozenCells[freeze].forEach2(function (cell) {
                    _this.drawOffsetRect(cell.originalCell, cell.coords, cell.col);
                });
                frozenCells[freeze].forEach2(function (cell) {
                    _this.drawGraphics(cell.originalCell, cell.coords, cell.col);
                });
                _this.gridLines.forEach2(function (line) {
                    _this.drawLine(line[0], line[1], line[2]);
                });
            }
            for (var i in _this.sorting) {
                var col = _this.sorting[i];
                var cell = _this.content[0][col.col];
                _this.drawSortingIndicator(cell, col.direction);
            }
            _this.gc.bufferContext.restore();
            _this.gc.canvasContext.clearRect(0, 0, _this.width * _this.ratio, _this.height * _this.ratio);
            _this.gc.canvasContext.drawImage(_this.gc.buffer, 0, 0);
            _this.updateSelector(true);
        };
        _this.onWheelEvent = function (evt) {
            if (_this.animation) {
                cancelAnimationFrame(_this.animation);
            }
            var offsetX = evt.deltaX ? (Math.abs(evt.deltaX) >= 100 ? Math.round(Math.abs(evt.deltaX) / 100) : 1) : 0;
            var offsetY = evt.deltaY ? (Math.abs(evt.deltaY) >= 100 ? Math.round(Math.abs(evt.deltaY) / 100) : 1) : 0;
            var directionX = evt.deltaX > 0 ? 1 : -1;
            var directionY = evt.deltaY > 0 ? 1 : -1;
            offsetX *= directionX;
            offsetY *= directionY;
            if (_this.stage.y.size * _this.scale / _this.ratio <= _this.height &&
                _this.stage.x.size * _this.scale / _this.ratio > _this.width) {
                offsetX = offsetY;
            }
            var curretOffset = JSON.parse(JSON.stringify(_this.offsets));
            _this.setOffsets({
                x: offsetX,
                y: offsetY
            });
            _this.updateScrollbars();
            _this.animation = _this.drawGrid();
            if (curretOffset.x.offset !== _this.offsets.x.offset || curretOffset.y.offset !== _this.offsets.y.offset) {
                evt.preventDefault();
            }
        };
        _this.onResizeEvent = function (evt) {
            console.log("onResizeEvent");
            _this.ratio = window.devicePixelRatio;
            _this.setSize();
            if (!_this.touch) {
                _this.setScale(_this.ratio);
            }
            _this.setFit(_this.fit, true);
            if (_this.touch && _this.editing) {
                setTimeout(function () {
                    _this.keepSelectorWithinView();
                }, 300);
            }
            else if (_this.touch && _this.cellSelection.from) {
                _this.setOffsets({
                    y: _this.cellSelection.from.position.row
                });
            }
        };
        _this.onMouseMove = function (evt) {
            ["x", "y"].forEach2(function (axis) {
                if (!_this.scrollbars[axis].drag || !_this.scrollbars.scrolling) {
                    return;
                }
                var dimension = _this.stage[axis].label;
                var canvasSize = _this[dimension];
                var size = canvasSize * (canvasSize / (_this.stage[axis].size * _this.scale));
                var distance = canvasSize - size;
                var offset = evt[axis] - _this.scrollbars[axis].start + _this.scrollbars[axis].offset;
                if (offset > distance) {
                    offset = distance;
                }
                else if (offset < 0) {
                    offset = 0;
                }
                var anchor = _this.scrollbars[axis].anchor;
                _this.scrollbars[axis].handle.style[anchor] = offset + "px";
                var ratio = offset / distance;
                var index = Math.round(_this.offsets[axis].max * ratio);
                _this.offsets[axis].index = index;
                var scrollOffset = _this.stage[axis].increments[index];
                if (scrollOffset > _this.offsets[axis].maxOffset) {
                    scrollOffset = _this.offsets[axis].maxOffset;
                }
                _this.offsets[axis].offset = scrollOffset;
            });
            if (_this.scrollbars.scrolling) {
                _this.drawGrid();
            }
            else {
                _this.classy.removeClass(_this.containerEl, "link");
                _this.classy.removeClass(_this.containerEl, "denied");
                var cell = _this.getCell(evt.clientX, evt.clientY);
                if (cell) {
                    _this.cellHasHistory(cell, { x: evt.clientX, y: evt.clientY });
                    if (cell.allow !== true) {
                        _this.classy.addClass(_this.containerEl, "denied");
                    }
                    else if (cell.filter || cell.link || cell.button) {
                        _this.classy.addClass(_this.containerEl, "link");
                    }
                    if (cell.history.valid && !cell.button) {
                        if (!_this.containerEl.contains(_this.historyIndicator)) {
                            _this.containerEl.appendChild(_this.historyIndicator);
                        }
                        _this.updateSelectorAttributes(cell, cell, _this.historyIndicator, true);
                    }
                    else if (_this.containerEl.contains(_this.historyIndicator)) {
                        _this.containerEl.removeChild(_this.historyIndicator);
                    }
                    if (_this.freeScroll) {
                        var x = Math.round((evt.clientX - _this.freeScroll.startX) / 20);
                        var y = Math.round((evt.clientY - _this.freeScroll.startY) / 20);
                        _this.freeScroll.x = x;
                        _this.freeScroll.y = y;
                    }
                    if (_this.cellSelection.go && !_this.alwaysEditing) {
                        _this.cellSelection.to = cell;
                        _this.updateSelector();
                    }
                }
            }
        };
        _this.onMouseUp = function (evt) {
            console.log("onMouseUp");
            _this.scrollbars.scrolling = false;
            _this.cellSelection.go = false;
            ["x", "y"].forEach2(function (axis) {
                _this.scrollbars[axis].drag = false;
            });
            _this.emit(_this.ON_CELL_SELECTED, _this.cellSelection);
            _this.rightClick = _this.isRightClick(evt);
            if (evt.which === 2) {
                if (_this.freeScroll) {
                    _this.classy.removeClass(_this.containerEl, "move");
                    cancelAnimationFrame(_this.freeScroll.animation);
                    _this.freeScroll = undefined;
                }
                else {
                    _this.freeScroll = {
                        x: 0,
                        y: 0,
                        startX: evt.clientX,
                        startY: evt.clientY,
                        row: _this.cellSelection.from.position.row * 1,
                        col: _this.cellSelection.from.position.col * 1,
                        animation: requestAnimationFrame(_this.startFreeScroll)
                    };
                    _this.classy.addClass(_this.containerEl, "move");
                }
            }
        };
        _this.onVisibilityEvent = function (evt) {
            console.log("hidden", document.hidden);
            _this.hidden = document.hidden;
            if (!_this.hidden) {
                _this.renderGrid();
            }
        };
        _this.onMouseDown = function (evt) {
            console.log(evt);
            _this.rightClick = _this.isRightClick(evt);
            var scrollHandle = false;
            ["x", "y"].forEach2(function (axis) {
                if (_this.scrollbars[axis].handle === evt.target)
                    scrollHandle = true;
            });
            var hit = _this.isCanvas(evt.target) ||
                _this.isInput(evt.target) ||
                evt.target === _this.selection ||
                evt.target === _this.historyIndicator;
            if (!scrollHandle && hit) {
                if (evt.touches) {
                    if (evt.touches.length > 1) {
                        return true;
                    }
                    evt.clientX = evt.touches[0].pageX;
                    evt.clientY = evt.touches[0].pageY;
                }
                console.log(evt.clientX, evt.clientY, evt.clientX, evt.clientY);
                var cell = _this.getCell(evt.clientX, evt.clientY);
                if (cell) {
                    if (_this.rightClick) {
                        return;
                    }
                    var d = new Date();
                    if (_this.cellSelection.clicked) {
                        if (d.getTime() - _this.cellSelection.clicked <= 300 && !_this.cellSelection.from.button) {
                            _this.emit(_this.ON_CELL_DOUBLE_CLICKED, cell);
                            _this.cellSelection.clicked = d.getTime();
                            _this.editing = true;
                            if (_this.touch) {
                                evt.stopPropagation();
                                evt.preventDefault();
                            }
                            return;
                        }
                    }
                    _this.editing = false;
                    _this.setCellSelection(cell, true, d.getTime(), { x: evt.clientX, y: evt.clientY });
                    if (_this.touch) {
                        evt.stopPropagation();
                        evt.preventDefault();
                    }
                    return;
                }
                else {
                    _this.editing = false;
                    _this.hideSelector(true);
                }
            }
            else if (!hit && evt.target.nodeName === "INPUT") {
                _this.hasFocus = false;
            }
            else if (!scrollHandle && !hit) {
                _this.hasFocus = false;
            }
        };
        _this.onMouseOver = function (evt) {
            _this.hasFocus = true;
        };
        _this.onKeydown = function (evt) {
            if (evt.keyCode === 27) {
                if (_this.editing) {
                    _this.editing = false;
                }
                else {
                    _this.hideSelector(true);
                }
                _this.emit(_this.ON_CELL_RESET, { cell: _this.cellSelection.from });
                _this.drawGrid();
                return;
            }
            console.log("onKeydown", evt);
            if (!_this.hasFocus) {
                return;
            }
            if (evt.keyCode === 17 || evt.keyCode === 91) {
                _this.ctrlDown = true;
                return;
            }
            var x = 0;
            var y = 0;
            switch (evt.keyCode) {
                case 37:
                    x = -1;
                    break;
                case 38:
                    y = -1;
                    break;
                case 39:
                    x = 1;
                    break;
                case 40:
                    y = 1;
                    break;
            }
            if ((x !== 0 || y !== 0) && !_this.editing) {
                var cell = _this.cellSelection.from;
                var row = void 0;
                var col = void 0;
                if (_this.cellSelection.from && !_this.disallowSelection) {
                    row = cell.index.row + y;
                    col = cell.index.col + x;
                    if (row >= _this.content.length) {
                        row = 0;
                    }
                    else if (row < 0) {
                        row = _this.content.length - 1;
                    }
                    if (col >= _this.content[0].length) {
                        col = 0;
                    }
                    else if (col < 0) {
                        col = _this.content[0].length - 1;
                    }
                    _this.setCellSelection(_this.content[row][col]);
                    _this.keepSelectorWithinView();
                    evt.preventDefault();
                }
                else {
                    _this.setOffsets({
                        x: x,
                        y: y
                    });
                    _this.updateScrollbars();
                    _this.drawGrid();
                    if ((y > 0 && _this.offsets.y.index !== _this.offsets.y.max) || (y < 0 && _this.offsets.y.index)) {
                        evt.preventDefault();
                    }
                }
                return;
            }
            if (evt.keyCode === 13) {
                if (!_this.alwaysEditing && !_this.touch) {
                    _this.editing = false;
                }
                _this.nextCellInput(_this.cellSelection.from || _this.cellSelection.last || _this.content[0][0], _this.shiftDown ? "up" : "down");
                _this.keepSelectorWithinView();
                return;
            }
            if (evt.keyCode === 9 && _this.cellSelection.from) {
                if (!_this.alwaysEditing && !_this.touch) {
                    _this.editing = false;
                }
                _this.nextCellInput(_this.cellSelection.from, _this.shiftDown ? "left" : "");
                evt.preventDefault();
                _this.keepSelectorWithinView();
                return;
            }
            if (evt.key === "Shift") {
                _this.shiftDown = true;
            }
            if (!_this.editing && !_this.disallowSelection && _this.canEdit) {
                if (evt.key === "F2") {
                    if (!evt.shiftKey) {
                        _this.setEditCell(_this.cellSelection.from);
                        return;
                    }
                }
                if (_this.cellSelection.from && !_this.ctrlDown) {
                    if (evt.key.length === 1 && /[a-zA-Z0-9-_ ]/.test(evt.key)) {
                        _this.keyValue = evt.key;
                        _this.input.value = evt.key;
                    }
                    if (evt.key === "Delete") {
                        _this.keyValue = "";
                        _this.input.value = "";
                    }
                    if (_this.keyValue !== undefined) {
                        _this.cellSelection.on = true;
                        _this.setEditCell(_this.cellSelection.from);
                        _this.ctrlDown = false;
                    }
                }
            }
            else if (_this.editing) {
                _this.dirty = true;
            }
        };
        _this.onKeyup = function (evt) {
            if (evt.keyCode === 17 || evt.keyCode === 91) {
                _this.ctrlDown = false;
            }
            if (evt.key === "Shift") {
                _this.shiftDown = false;
            }
        };
        _this.startFreeScroll = function () {
            if (!_this.freeScroll) {
                return;
            }
            _this.setOffsets({ x: _this.freeScroll.x, y: _this.freeScroll.y });
            _this.updateScrollbars();
            _this.drawGrid();
            requestAnimationFrame(_this.startFreeScroll);
        };
        _this.destroy();
        _this.fluid = fluid;
        _this.init = false;
        if (options.inset_scrollbars) {
            _this.scrollbars.inset = options.inset_scrollbars;
        }
        var parser = new ua();
        var parseResult = parser.getResult();
        _this.touch =
            parseResult.device && (parseResult.device.type === "tablet" || parseResult.device.type === "mobile");
        _this.classy = new Classy_1.default();
        _this.helpers = new Helpers_1.default();
        if (typeof container === "string") {
            _this.containerEl = document.getElementById(container);
        }
        else {
            _this.containerEl = container;
        }
        _this.containerEl.style.overflow = "hidden";
        var buffer = document.createElement("canvas");
        var canvas = document.createElement("canvas");
        _this.gc = {
            buffer: buffer,
            bufferContext: buffer.getContext("2d"),
            canvas: canvas,
            canvasContext: canvas.getContext("2d")
        };
        _this.containerEl.appendChild(_this.gc.canvas);
        _this.ratio = window.devicePixelRatio;
        _this.scale = _this.ratio;
        _this.ratioScale = _this.scale;
        _this.resetStage();
        _this.createSelector();
        document.addEventListener("visibilitychange", _this.onVisibilityEvent, false);
        window.addEventListener("resize", _this.onResizeEvent);
        window.addEventListener("keydown", _this.onKeydown, false);
        window.addEventListener("keyup", _this.onKeyup, false);
        window.oncontextmenu = function () {
            if (_this.rightClick && !_this.cellSelection.from) {
            }
        };
        if (_this.touch) {
            _this.setupTouch();
            _this.containerEl.addEventListener("touchstart", _this.onMouseDown, false);
            _this.containerEl.addEventListener("touchend", _this.onMouseUp, false);
        }
        else {
            if (!fluid) {
                _this.createScrollbars();
            }
            _this.containerEl.addEventListener("wheel", _this.onWheelEvent, false);
            _this.containerEl.addEventListener("mouseover", _this.onMouseOver, false);
            window.addEventListener("mousemove", _this.onMouseMove, false);
            window.addEventListener("mouseup", _this.onMouseUp, false);
            window.addEventListener("mousedown", _this.onMouseDown, false);
        }
        return _this;
    }
    Object.defineProperty(Grid.prototype, "ON_CELL_CLICKED", {
        get: function () {
            return "cell_clicked";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_CLICKED_RIGHT", {
        get: function () {
            return "cell_clicked_right";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_HISTORY_CLICKED", {
        get: function () {
            return "cell_history_clicked";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_LINK_CLICKED", {
        get: function () {
            return "cell_link_clicked";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_DOUBLE_CLICKED", {
        get: function () {
            return "cell_double_clicked";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_TAG_CLICKED", {
        get: function () {
            return "tag_clicked";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_VALUE", {
        get: function () {
            return "cell_value";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_SELECTED", {
        get: function () {
            return "cell_selected";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_CELL_RESET", {
        get: function () {
            return "cell_reset";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "ON_EDITING", {
        get: function () {
            return "editing";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "fit", {
        get: function () {
            return this._fit;
        },
        set: function (value) {
            if (["scroll", "width", "height", "contain"].indexOf(value) === -1) {
                return;
            }
            this._fit = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "editing", {
        get: function () {
            return this._editing;
        },
        set: function (value) {
            this._editing = value;
            this.emit(this.ON_EDITING, { cell: this.cellSelection.from, editing: this._editing, dirty: this.dirty });
            if (value) {
                if (!this.cellSelection.from || this.cellSelection.from.allow !== true || !this.canEdit) {
                    this._editing = false;
                    return;
                }
                this.showInput();
            }
            else {
                this.keyValue = undefined;
                if (this.selection.contains(this.input)) {
                    this.input.value = "";
                    this.input.blur();
                    this.input.style["visibility"] = "hidden";
                }
                if (!this.touch && this.selection.contains(this.input)) {
                    this.selection.removeChild(this.input);
                    this.selection.contentEditable = true;
                }
                if (this.dirty) {
                    this.dirty = false;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Grid.prototype, "tracking", {
        get: function () {
            return this._tracking;
        },
        set: function (value) {
            this._tracking = value;
            if (!value) {
                this.history = [];
            }
            this.drawGrid();
        },
        enumerable: true,
        configurable: true
    });
    Grid.prototype.destroy = function () {
        if (!this.containerEl) {
            return;
        }
        try {
            window.removeEventListener("resize", this.onResizeEvent);
            window.removeEventListener("keydown", this.onKeydown, false);
            window.removeEventListener("keyup", this.onKeyup, false);
            document.removeEventListener("visibilitychange", this.onVisibilityEvent, false);
            if (!this.touch) {
                this.containerEl.removeEventListener("wheel", this.onWheelEvent);
                this.containerEl.removeEventListener("mouseover", this.onMouseOver, false);
                window.removeEventListener("mousemove", this.onMouseMove, false);
                window.removeEventListener("mouseup", this.onMouseUp, false);
                window.removeEventListener("mousedown", this.onMouseDown, false);
            }
            else {
                this.containerEl.removeEventListener("touchstart", this.onMouseDown, false);
                this.containerEl.removeEventListener("touchend", this.onMouseUp, false);
            }
            this.containerEl.innerHTML = "";
            if (this.hammer) {
                this.hammer.destroy();
            }
            this.removeEvent();
        }
        catch (e) {
            console.log(e);
        }
    };
    Grid.prototype.cellHistory = function (data) {
        var _this = this;
        this.history = [];
        if (!data.length) {
            return;
        }
        var users = [];
        var count = 0;
        data.forEach2(function (push) {
            count++;
            if (count === 1) {
                return;
            }
            if (users.indexOf(push.modified_by.id) === -1) {
                users.push(push.modified_by.id);
            }
            var userIndex = users.indexOf(push.modified_by.id);
            push.content_diff.forEach2(function (row, rowIndex) {
                if (!row) {
                    return;
                }
                if (!_this.history[rowIndex]) {
                    _this.history[rowIndex] = [];
                }
                row.forEach2(function (cellData, cellIndex) {
                    if (!cellData) {
                        return;
                    }
                    _this.history[rowIndex][cellIndex] = userIndex;
                });
            });
        });
    };
    Grid.prototype.setTrackingHighlights = function (data) {
        this.clearTrackingHighlights();
        if (!data || !data.content_diff || !data.content_diff.length) {
            return;
        }
        for (var rowIndex = 0; rowIndex < data.content_diff.length; rowIndex++) {
            var row = data.content_diff[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                if (!row[colIndex])
                    continue;
                if (!this.trackingHighlights[rowIndex]) {
                    this.trackingHighlights[rowIndex] = [];
                }
                this.trackingHighlights[rowIndex][colIndex] = true;
            }
        }
    };
    Grid.prototype.clearTrackingHighlights = function () {
        var highlights = this.containerEl.getElementsByClassName("tracking");
        while (highlights[0]) {
            highlights[0].parentNode.removeChild(highlights[0]);
        }
        this.trackingHighlights = [];
    };
    Grid.prototype.setContent = function (content) {
        this.originalContent = content;
        this.updateContent();
    };
    Grid.prototype.getContentHtml = function () {
        var html = "<table style=\"border-collapse: collapse;\">";
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            if (this.cellSelection.from &&
                (rowIndex < this.cellSelection.from.index.row || rowIndex > this.cellSelection.to.index.row)) {
                continue;
            }
            html += "<tr>";
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                if (this.cellSelection.to &&
                    (colIndex < this.cellSelection.from.index.col || colIndex > this.cellSelection.to.index.col)) {
                    continue;
                }
                var cell = this.originalContent[rowIndex][colIndex];
                var styles = this.flattenStyles(cell.style);
                html += "<td style=\"" + styles + "\">" + (col.allow === "no" ? "" : cell.formatted_value) + "</td>";
            }
            html += "</tr>";
        }
        html += "</table>";
        return html;
    };
    Grid.prototype.getCells = function (fromCell, toCell, refCell) {
        var rowEnd = toCell.row;
        if (rowEnd === -1) {
            rowEnd = this.originalContent.length - 1;
        }
        var colEnd = toCell.col;
        if (colEnd === -1) {
            colEnd = this.originalContent[0].length - 1;
        }
        var cellUpdates = [];
        for (var row = fromCell.row; row <= rowEnd; row++) {
            for (var col = fromCell.col; col <= colEnd; col++) {
                if (!cellUpdates[row])
                    cellUpdates[row] = [];
                cellUpdates[row][col] = this.content[row][col];
            }
        }
        return cellUpdates;
    };
    Grid.prototype.renderGrid = function (clear) {
        if (clear === void 0) { clear = false; }
        if (this.hidden) {
            return;
        }
        var diff = !this.width ||
            clear ||
            (!this.content.length && !this.content.length) ||
            this.content.length !== this.content.length ||
            this.content[0].length !== this.content[0].length;
        this.buildCells();
        this.updateCellAccess();
        if (diff) {
            this.setSize();
        }
        this.setOffsets(undefined);
        if (diff) {
            this.setFit(this.fit);
        }
        this.updateScrollbars();
        if (!this.init) {
            this.init = true;
            requestAnimationFrame(this.drawGrid);
        }
        else {
            this.drawGrid();
        }
    };
    Grid.prototype.setScale = function (n) {
        this.scale = n * this.ratio;
        this.setOffsets(undefined);
        this.updateScrollbars();
        requestAnimationFrame(this.drawGrid);
    };
    Grid.prototype.setFit = function (n, retain) {
        if (!/(scroll|width|height|contain)/.test(n)) {
            return;
        }
        console.log("setFit", n);
        this.updateFit(n, !this.touch);
        this.setSize();
        if (!this.touch) {
            this.updateFit(n, true);
        }
    };
    Grid.prototype.clearRanges = function () {
        this.accessRanges = [];
        this.buttonRanges = [];
        this.styleRanges = [];
        this.freezeRange = {
            valid: false,
            index: {
                row: 0,
                col: 0
            }
        };
    };
    Grid.prototype.setRanges = function (ranges, userId) {
        if (userId === void 0) { userId = undefined; }
        this.clearRanges();
        for (var i = 0; i < ranges.length; i++) {
            var range = ranges[i];
            var rowEnd = void 0;
            var colEnd = void 0;
            if (range.range) {
                rowEnd = _.clone(range.range.to.row);
                colEnd = _.clone(range.range.to.col);
                if (rowEnd === -1) {
                    rowEnd = this.originalContent.length - 1;
                }
                if (colEnd === -1) {
                    colEnd = this.originalContent[0].length - 1;
                }
            }
            if (range.name === "frozen_rows") {
                this.freezeRange.index.row = range.count;
            }
            else if (range.name === "frozen_cols") {
                this.freezeRange.index.col = range.count;
            }
            else if (range instanceof Range_3.StyleRange) {
                for (var i_1 = range.range.from.row; i_1 <= rowEnd; i_1++) {
                    for (var k = range.range.from.col; k <= colEnd; k++) {
                        if (!this.styleRanges[i_1]) {
                            this.styleRanges[i_1] = [];
                        }
                        if (!this.styleRanges[i_1][k]) {
                            this.styleRanges[i_1][k] = [];
                        }
                        this.styleRanges[i_1][k].push(range);
                    }
                }
            }
            else if (range instanceof Range_2.ButtonRange) {
                for (var i_2 = range.range.from.row; i_2 <= rowEnd; i_2++) {
                    for (var k = range.range.from.col; k <= colEnd; k++) {
                        if (!this.buttonRanges[i_2]) {
                            this.buttonRanges[i_2] = [];
                        }
                        if (!this.buttonRanges[i_2][k]) {
                            this.buttonRanges[i_2][k] = [];
                        }
                        this.buttonRanges[i_2][k].push(range);
                    }
                }
            }
            else if (range instanceof Range_1.PermissionRange && this.originalContent.length) {
                var userRight = range.getPermission(userId || 0) || (userId ? "yes" : "no");
                var rowEnd_1 = _.clone(range.rowEnd);
                var colEnd_1 = _.clone(range.colEnd);
                if (rowEnd_1 === -1) {
                    rowEnd_1 = this.originalContent.length - 1;
                }
                if (colEnd_1 === -1) {
                    colEnd_1 = this.originalContent[0].length - 1;
                }
                for (var i_3 = range.rowStart; i_3 <= rowEnd_1; i_3++) {
                    for (var k = range.colStart; k <= colEnd_1; k++) {
                        if (!this.accessRanges[i_3]) {
                            this.accessRanges[i_3] = [];
                        }
                        if (!this.accessRanges[i_3][k]) {
                            this.accessRanges[i_3][k] = [];
                        }
                        if (userRight) {
                            this.accessRanges[i_3][k].push(userRight);
                        }
                    }
                }
            }
        }
        this.updateCellAccess();
        if (this.freezeRange.index.row > 0 || this.freezeRange.index.col > 0) {
            this.freezeRange.valid = true;
        }
    };
    Grid.prototype.clearFilters = function () {
        this.filters = {};
        this.updateContent();
    };
    Grid.prototype.setFilters = function (filters) {
        var _this = this;
        if (filters.col >= 0) {
            filters = [filters];
        }
        filters.forEach2(function (filter) {
            if (!filter.value || filter.remove) {
                if (_this.filters["col" + filter.col]) {
                    delete _this.filters["col" + filter.col];
                }
                return;
            }
            if (!filter.type)
                filter.type = "number";
            filter.type = /(number|string|date)/.test(filter.type) ? filter.type : "number";
            if (!filter.exp ||
                (filter.type === "number" && [">", "<", "<=", ">=", "==", "==="].indexOf(filter.exp) === -1)) {
                filter.exp = "==";
            }
            _this.filters["col" + filter.col] = filter;
        });
        this.updateContent();
    };
    Grid.prototype.clearSorting = function () {
        this.sorting = {};
        this.updateContent();
    };
    Grid.prototype.setSorting = function (filters) {
        var _this = this;
        this.clearSorting();
        if (filters.col >= 0) {
            filters = [filters];
        }
        filters.forEach2(function (filter) {
            if (filter.remove) {
                if (_this.filters["col" + filter.col]) {
                    delete _this.filters["col" + filter.col];
                }
                return;
            }
            if (!filter.direction)
                filter.direction = "ASC";
            filter.direction = ("" + filter.direction).toUpperCase();
            filter.direction = /(ASC|DESC)/.test(filter.direction) ? filter.direction : "ASC";
            filter.col = filter.col * 1 || 0;
            filter.type = /(number|string|date)/.test(filter.type) ? filter.type : "number";
            _this.sorting["col" + filter.col] = filter;
        });
        this.updateContent();
    };
    Grid.prototype.hideSelector = function (focus) {
        if (focus === void 0) { focus = false; }
        this.emitCellChange(this.cellSelection.from);
        this.cellSelection.last = this.cellSelection.from;
        this.cellSelection.on = false;
        this.cellSelection.from = undefined;
        this.cellSelection.to = undefined;
        this.selection.style["display"] = "none";
        this.emit(this.ON_CELL_SELECTED, this.cellSelection);
        this.hideHistoryIndicator();
    };
    Grid.prototype.find = function (value) {
        var highlights = this.containerEl.getElementsByClassName("highlight");
        while (highlights[0]) {
            highlights[0].parentNode.removeChild(highlights[0]);
        }
        this.found = [];
        this.foundSelection.count = 0;
        this.foundSelection.selected = 0;
        if (!value) {
            return;
        }
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                var regex = new RegExp(value, "ig");
                var cell = this.originalContent[rowIndex][colIndex];
                if (col.allow === "no" || (!regex.test(cell.value) && !regex.test(cell.formatted_value))) {
                    continue;
                }
                if (!this.found[rowIndex]) {
                    this.found[rowIndex] = [];
                }
                this.foundSelection.count++;
                this.found[rowIndex][colIndex] = {
                    cell: col,
                    highlight: this.createHighlight(cell),
                    originalCell: cell
                };
            }
        }
        this.updateSelector();
    };
    Grid.prototype.gotoFoundCell = function (which) {
        var _this = this;
        if (typeof which === "number") {
            if (which < 0 || which > this.foundSelection.count) {
                return;
            }
            this.foundSelection.selected = which;
        }
        else if (["prev", "next"].indexOf(which) > -1) {
            if (which === "next") {
                this.foundSelection.selected++;
            }
            else {
                this.foundSelection.selected--;
            }
            if (this.foundSelection.selected > this.foundSelection.count) {
                this.foundSelection.selected = 1;
            }
            else if (this.foundSelection.selected < 1) {
                this.foundSelection.selected = this.foundSelection.count;
            }
        }
        else {
            return;
        }
        var count = 0;
        this.found.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                count++;
                if (count === _this.foundSelection.selected) {
                    _this.cellSelection.from = col.cell;
                    _this.cellSelection.to = col.cell;
                    _this.cellSelection.on = true;
                    _this.goto(col.cell.index.col, col.cell.index.row, true);
                }
            });
        });
    };
    Grid.prototype.setSelector = function (from, to) {
        if (!to) {
            to = from;
        }
        if (!this.content[from.row] || !this.content[from.row][from.col]) {
            return;
        }
        this.updateSelectorAttributes(this.content[from.row][from.col], this.content[to.row][to.col], this.selection);
        this.cellSelection.from = this.content[from.row][from.col];
        this.cellSelection.to = this.content[to.row][to.col];
        this.emit(this.ON_CELL_SELECTED, this.cellSelection);
    };
    Grid.prototype.updateContent = function () {
        this.content = this.setCellIndexes(this.originalContent);
        this.applyFilters();
        this.applySorting();
        if (this.cellSelection.from && !this.content[this.cellSelection.from.position.row]) {
            this.hideSelector();
        }
    };
    Grid.prototype.showInput = function () {
        var _this = this;
        var cell = this.originalContent[this.cellSelection.from.position.row][this.cellSelection.from.position.col];
        var unit = cell.style["font-size"].indexOf("px") > -1 ? "px" : "pt";
        this.input.style["font-size"] = "" + parseFloat(cell.style["font-size"]) * this.scale / this.ratio + unit;
        this.input.style["font-family"] = cell.style["font-family"] + ", sans-serif";
        this.input.style["font-weight"] = cell.style["font-weight"];
        this.input.style["text-align"] = cell.style["text-align"];
        this.input.style["visibility"] = "visible";
        if (!this.touch) {
            this.selection.appendChild(this.input);
            this.selection.contentEditable = false;
            var interval_1 = setInterval(function () {
                if (!_this.selection.contains(_this.input))
                    return;
                clearInterval(interval_1);
                if (_this.keyValue === undefined) {
                    _this.input.value = "" + cell.formatted_value;
                }
                _this.input.focus();
                if (_this.keyValue !== undefined) {
                    _this.input.selectionStart = 999;
                }
                else if (_this.input.value) {
                    _this.input.selectionStart = 0;
                    _this.input.selectionEnd = 999;
                }
            }, 20);
        }
        else {
            if (this.keyValue === undefined) {
                this.input.value = "" + cell.formatted_value;
            }
            this.input.focus();
            if (this.keyValue !== undefined) {
                this.input.selectionStart = 999;
            }
            else if (this.input.value) {
                this.input.selectionStart = 0;
                this.input.selectionEnd = 999;
            }
        }
    };
    Grid.prototype.setCellIndexes = function (content) {
        var clone = [];
        this.links = [];
        var links = this.containerEl.getElementsByClassName("external-link");
        while (links[0]) {
            links[0].parentNode.removeChild(links[0]);
        }
        for (var rowIndex = 0; rowIndex < content.length; rowIndex++) {
            var row = content[rowIndex];
            clone[rowIndex] = [];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var cell = content[rowIndex][colIndex];
                var col = {
                    value: _.clone(cell.value),
                    formatted_value: _.clone(cell.formatted_value),
                    width: parseFloat(cell.style.width),
                    position: {
                        row: rowIndex,
                        col: colIndex
                    }
                };
                clone[rowIndex].push(col);
                if (cell.link && cell.link.external) {
                    if (!this.links[rowIndex])
                        this.links[rowIndex] = [];
                    this.links[rowIndex][colIndex] = {
                        cell: col,
                        highlight: this.createHighlight(col, {
                            className: "external-link",
                            href: cell.link.address,
                            target: "_blank"
                        })
                    };
                }
            }
        }
        return clone;
    };
    Grid.prototype.updateFound = function (cell) {
        var _this = this;
        if (!this.found[cell.index.row] || !this.found[cell.index.row][cell.index.col]) {
            return;
        }
        var count = 0;
        this.found.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                count++;
                if (col.cell === cell) {
                    _this.foundSelection.selected = count;
                }
            });
        });
    };
    Grid.prototype.createHighlight = function (cell, attrs, styles) {
        if (attrs === void 0) { attrs = {}; }
        if (styles === void 0) { styles = {}; }
        var highlight = document.createElement(attrs.href ? "a" : "div");
        var inner = document.createElement("div");
        for (var attr in attrs) {
            highlight[attr] = attrs[attr];
        }
        if (!highlight.className)
            highlight.className = "highlight";
        highlight.style.position = "absolute";
        highlight.style["z-index"] = 10;
        highlight.style["left"] = "0";
        highlight.style["top"] = "0";
        highlight.style["width"] = cell.width + "px";
        highlight.style["height"] = cell.height + "px";
        highlight.style["display"] = "none";
        for (var style in styles) {
            inner.style[style] = styles[style];
        }
        highlight.appendChild(inner);
        this.containerEl.appendChild(highlight);
        return highlight;
    };
    Grid.prototype.applyFilters = function () {
        if (!Object.keys(this.filters).length) {
            return false;
        }
        var rowsToRemove = [];
        for (var rowIndex = 0; rowIndex < this.originalContent.length; rowIndex++) {
            if (this.freezeRange.valid && rowIndex < this.freezeRange.index.row) {
                continue;
            }
            var _loop_1 = function (f) {
                var filter = this_1.filters[f];
                var col = this_1.originalContent[rowIndex][filter.col];
                if (!col || rowsToRemove.indexOf(rowIndex) > -1)
                    return "continue";
                var colValue = filter.type === "number" ? parseFloat(col.value) : "" + col.formatted_value;
                var filterValue = filter.type === "number" ? parseFloat(filter.value) : "" + filter.value;
                var validValue = false;
                switch (filter.type) {
                    case "number":
                        validValue = eval(colValue + " " + filter.exp + " " + filterValue);
                        break;
                    case "date":
                        var dates = filterValue.split(",");
                        var fromDate = new Date(dates[0]).toISOString();
                        var toDate = new Date(dates[1]).toISOString();
                        var date = new Date(this_1.helpers.parseDateExcel(colValue)).toISOString();
                        validValue = date >= fromDate && date <= toDate;
                        break;
                    default:
                        filterValue.split(",").forEach2(function (value) {
                            if (validValue) {
                                return;
                            }
                            switch (filter.exp) {
                                case "contains":
                                    validValue = colValue.toLowerCase().indexOf(value.toLowerCase()) > -1;
                                    break;
                                case "starts_with":
                                    validValue =
                                        colValue.toLowerCase().substring(0, value.length) === value.toLowerCase();
                                    break;
                                default:
                                    validValue = colValue == value;
                                    break;
                            }
                        });
                        break;
                }
                if (!validValue)
                    rowsToRemove.push(rowIndex);
            };
            var this_1 = this;
            for (var f in this.filters) {
                _loop_1(f);
            }
        }
        for (var i = rowsToRemove.length - 1; i >= 0; i--)
            this.content.splice(rowsToRemove[i], 1);
    };
    Grid.prototype.applySorting = function () {
        if (!Object.keys(this.sorting).length) {
            return false;
        }
        var key = Object.keys(this.sorting)[0];
        var filter = this.sorting[key];
        var data = this.freezeRange.index.row
            ? this.content.slice(this.freezeRange.index.row, this.content.length)
            : this.content.slice(0);
        var exp = filter.direction === "DESC" ? ">" : "<";
        var opp = filter.direction === "DESC" ? "<" : ">";
        data.sort(function (a, b) {
            var aVal = filter.type === "number" ? parseFloat(a[filter.col].value) || 0 : "\"" + a[filter.col].value + "\"";
            var bVal = filter.type === "number" ? parseFloat(b[filter.col].value) || 0 : "\"" + b[filter.col].value + "\"";
            if (eval(aVal + " " + exp + " " + bVal)) {
                return -1;
            }
            if (eval(aVal + " " + opp + " " + bVal)) {
                return 1;
            }
            return 0;
        });
        this.content = this.freezeRange.index.row
            ? this.content.slice(0, this.freezeRange.index.row).concat(data)
            : data;
    };
    Grid.prototype.buildCells = function () {
        this.resetStage();
        this.stage.x.increments.push(0);
        this.stage.y.increments.push(0);
        var freezeOffset = {
            x: 0,
            y: 0
        };
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            this.stage.x.pos = 0;
            var col = void 0;
            var originalCell = void 0;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                col = row[colIndex];
                originalCell = this.originalContent[col.position.row][col.position.col];
                var wrap = false;
                if ((originalCell.style["text-wrap"] && originalCell.style["text-wrap"] === "wrap") ||
                    originalCell.style["word-wrap"] === "break-word") {
                    wrap = true;
                }
                col.wrap = wrap;
                col.x = this.stage.x.pos;
                col.y = this.stage.y.pos;
                col.width = parseFloat(originalCell.style.width);
                col.height = parseFloat(originalCell.style.height);
                col.index = {
                    row: rowIndex,
                    col: colIndex
                };
                col.allow = true;
                col.link = originalCell.link;
                this.stage.x.pos += col.width;
                if (colIndex === row.length - 1 && this.borders.widths[originalCell.style.rbw]) {
                    this.stage.x.pos += this.borders.widths[originalCell.style.rbw];
                }
                if (!rowIndex) {
                    if (!this.freezeRange.valid || (this.freezeRange.valid && colIndex >= this.freezeRange.index.col)) {
                        this.stage.x.increments.push(this.stage.x.pos - freezeOffset.x);
                    }
                    else {
                        freezeOffset.x += col.width;
                    }
                }
                if (this.trackingHighlights[rowIndex] && this.trackingHighlights[rowIndex][colIndex]) {
                    var styles = {};
                    if (this.trackingHighlightMatchColor)
                        styles["background-color"] = "#" + originalCell.style.color.replace("#", "");
                    this.trackingHighlights[rowIndex][colIndex] = new GridTrackingHighlight(col, this.createHighlight(col, {
                        className: "tracking " + this.trackingHighlightClassname
                    }, styles));
                    this.trackingHighlights[rowIndex][colIndex].lifetime = this.trackingHighlightLifetime;
                }
            }
            if (this.stage.x.pos > this.stage.x.size) {
                this.stage.x.size = this.stage.x.pos;
            }
            this.stage.y.pos += col.height;
            this.stage.y.size += col.height;
            if (rowIndex === this.content.length - 1 && this.borders.widths[originalCell.style.bbw]) {
                this.stage.y.pos += this.borders.widths[originalCell.style.bbw];
                this.stage.y.size += this.borders.widths[originalCell.style.bbw];
            }
            if (!this.freezeRange.valid || (this.freezeRange.valid && rowIndex >= this.freezeRange.index.row)) {
                this.stage.y.increments.push(this.stage.y.pos - freezeOffset.y);
            }
            else {
                freezeOffset.y += col.height;
            }
        }
    };
    Grid.prototype.updateCellAccess = function () {
        var _this = this;
        if (!this.content.length) {
            return;
        }
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                col.allow = true;
                col.button = false;
                col.formatting = false;
            }
        }
        this.accessRanges.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col || col.indexOf("yes") !== -1)
                    return;
                col.forEach2(function (element) {
                    if (["ro", "no"].indexOf(element) === -1) {
                        return;
                    }
                    if (_this.content[rowIndex] && _this.content[rowIndex][colIndex]) {
                        _this.content[rowIndex][colIndex].allow = element;
                    }
                });
            });
        });
        this.buttonRanges.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                if (_this.content[rowIndex] && _this.content[rowIndex][colIndex]) {
                    _this.content[rowIndex][colIndex].button = col;
                }
            });
        });
        var rowEnd = this.originalContent.length - 1;
        var colEnd = this.originalContent[0].length - 1;
        this.styleRanges.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                if (_this.content[rowIndex] && _this.content[rowIndex][colIndex]) {
                    col.forEach2(function (styleRange) {
                        if (!styleRange.match(_this.content[rowIndex][colIndex].value)) {
                            return;
                        }
                        styleRange.formatting.forEach2(function (format) {
                            var cells = styleRange.parseFormat(format, rowIndex, colIndex, rowEnd, colEnd);
                            for (var i = cells.from.row; i <= cells.to.row; i++) {
                                for (var k = cells.from.col; k <= cells.to.col; k++) {
                                    if (_this.content[i] && _this.content[i][k]) {
                                        if (!_this.content[i][k].formatting) {
                                            _this.content[i][k].formatting = cells.style;
                                        }
                                        else {
                                            _this.content[i][k].formatting = merge(_this.content[i][k].formatting, cells.style);
                                        }
                                    }
                                }
                            }
                        });
                    });
                }
            });
        });
    };
    Grid.prototype.updateFit = function (n, retain) {
        this.fit = n;
        switch (n) {
            case "scroll":
                this.setScale(retain ? this.ratio : this.ratioScale / this.ratio);
                break;
            case "width":
                if (!this.stage.x.size) {
                    return;
                }
                this.setScale(this.width / this.stage.x.size);
                break;
            case "height":
                if (!this.stage.y.size) {
                    return;
                }
                this.setScale(this.height / this.stage.y.size);
                break;
            case "contain":
                if (!this.stage.x.size || !this.stage.y.size) {
                    return;
                }
                var w = this.width / this.stage.x.size;
                var h = this.height / this.stage.y.size;
                this.setScale(h < w ? h : w);
                break;
        }
    };
    Grid.prototype.setupTouch = function () {
        var _this = this;
        this.hammer = new Hammer(this.containerEl);
        var pan = new Hammer.Pan();
        var pinch = new Hammer.Pinch();
        this.hammer.add([pan, pinch]);
        var currentScale = this.scale / this.ratio * 1;
        this.hammer.on("pinchstart", function (evt) {
            _this.fit = "scroll";
            currentScale = _this.scale / _this.ratio * 1;
        });
        this.hammer.on("pinchmove", function (evt) {
            _this.setScale(currentScale * evt.scale);
        });
        this.hammer.on("pinchend", function (evt) {
            currentScale = _this.scale / _this.ratio * 1;
        });
        var currentOffsets = {
            x: 0,
            y: 0
        };
        this.hammer.on("panstart", function (evt) {
            currentOffsets = {
                x: evt.center.x,
                y: evt.center.y
            };
        });
        this.hammer.on("panmove", function (evt) {
            var moveBy = Math.round(_this.ratio / _this.scale);
            if (moveBy < 1)
                moveBy = 1;
            var x = 0;
            if (Math.abs(evt.center.x - currentOffsets.x) > 5 * _this.scale) {
                var directionX = evt.velocityX > 0 ? -1 : 1;
                currentOffsets.x = evt.center.x;
                x = moveBy * directionX;
            }
            var y = 0;
            if (Math.abs(evt.center.y - currentOffsets.y) > 5 * _this.scale) {
                var directionY = evt.velocityY > 0 ? -1 : 1;
                currentOffsets.y = evt.center.y;
                y = moveBy * directionY;
            }
            _this.setOffsets({ x: x, y: y });
            _this.drawGrid();
        });
        this.hammer.on("panend", function (evt) { });
    };
    Grid.prototype.isRightClick = function (evt) {
        var isRightMB;
        if ("which" in evt)
            isRightMB = evt.which === 3;
        else if ("button" in evt)
            isRightMB = evt.button === 2;
        return isRightMB;
    };
    Grid.prototype.setCellSelection = function (cell, go, clickTime, coords) {
        if (go === void 0) { go = false; }
        if (coords === void 0) { coords = { x: 0, y: 0 }; }
        if (this.editing) {
        }
        this.cellHasHistory(cell, coords);
        this.cellSelection.clicked = clickTime;
        this.cellSelection.from = cell;
        this.cellSelection.to = cell;
        this.cellSelection.go = false;
        this.cellSelection.on = false;
        if (!this.disallowSelection) {
            if (!cell.link) {
                this.cellSelection.go = go;
                this.cellSelection.on = true;
                this.updateSelector();
            }
        }
        this.updateFound(cell);
        this.emit(this.ON_CELL_SELECTED, this.cellSelection);
        if (clickTime) {
            this.emit(this.ON_CELL_CLICKED, { cell: cell, history: cell.history.show });
        }
        this.hasFocus = true;
        this.keyValue = undefined;
        if (!this.touch)
            this.selection.innerText = "";
        if (this.editing) {
            this.showInput();
        }
        else if (this.alwaysEditing) {
            this.editing = true;
        }
    };
    Grid.prototype.keepSelectorWithinView = function () {
        var _this = this;
        if (this.touch) {
            var yIndex = this.cellSelection.from.index.row;
            if (this.freezeRange.valid) {
                yIndex += this.freezeRange.index.row;
            }
            if (!this.visible[yIndex]) {
                this.setOffsets({
                    y: yIndex
                });
                requestAnimationFrame(this.drawGrid);
            }
            return;
        }
        var row = this.cellSelection.from.index.row;
        var col = this.cellSelection.from.index.col;
        var offsetX = 0, offsetY = 0;
        var rowStartIndex = -1, rowEndIndex = -1, colStartIndex = -1, colEndIndex = -1;
        var colRow;
        this.visible.forEach2(function (cellRow, cellRowIndex) {
            if (!cellRow)
                return;
            if (rowStartIndex < 0 && (!_this.freezeRange.valid || cellRowIndex > _this.freezeRange.index.row)) {
                rowStartIndex = cellRowIndex + 0;
            }
            rowEndIndex = cellRowIndex;
            if (!colRow && (!_this.freezeRange.valid || cellRowIndex > _this.freezeRange.index.row)) {
                colRow = cellRow;
            }
        });
        colRow.forEach2(function (cellCol, cellColIndex) {
            if (!cellCol)
                return;
            if (colStartIndex < 0 && (!_this.freezeRange.valid || cellColIndex > _this.freezeRange.index.col)) {
                colStartIndex = cellColIndex + 0;
            }
            colEndIndex = cellColIndex + 0;
        });
        if (!row) {
            this.offsets.y.index = 0;
        }
        else if (row === this.content.length - 1) {
            this.offsets.y.index = this.offsets.y.max;
        }
        else if (row < rowStartIndex + 2 + this.freezeRange.index.row) {
            offsetY = -1;
        }
        else if (row > rowEndIndex - 2) {
            offsetY = 1;
        }
        if (!col) {
            this.offsets.x.index = 0;
        }
        else if (col === this.content[0].length - 1) {
            this.offsets.x.index = this.offsets.x.max;
        }
        else {
            offsetX = col - this.offsets.x.index - this.freezeRange.index.col;
        }
        console.log("offset: " + offsetX + ", col: " + col + ", start: " + colStartIndex + ", end: " + colEndIndex);
        this.goto(offsetX, offsetY);
    };
    Grid.prototype.goto = function (x, y, set) {
        if (set === void 0) { set = false; }
        this.setOffsets({ x: x, y: y }, set);
        this.updateScrollbars();
        this.drawGrid();
        setTimeout(function () {
        }, 50);
    };
    Grid.prototype.restoreCell = function (cell) {
        if (!cell) {
            return;
        }
        cell.dirty = false;
        cell.formatted_value = this.content[cell.position.row][cell.position.col].formatted_value;
        cell.value = this.content[cell.position.row][cell.position.col].value;
    };
    Grid.prototype.nextCellInput = function (cell, direction) {
        if (direction === void 0) { direction = ""; }
        var row = cell.index.row;
        var col = cell.index.col;
        if (direction === "left") {
            if (this.content[row][col - 1]) {
                this.setCellSelection(this.content[row][col - 1]);
            }
            else if (this.content[row - 1]) {
                this.setCellSelection(this.content[row - 1][this.content[row].length - 1]);
            }
            return;
        }
        if (direction === "up") {
            if (this.content[row - 1] && this.content[row - 1][col]) {
                this.setCellSelection(this.content[row - 1][col]);
                return;
            }
            else if (this.content[this.content.length - 1] && this.content[this.content.length - 1][col - 1]) {
                this.setCellSelection(this.content[this.content.length - 1][col - 1]);
                return;
            }
        }
        if (direction === "down") {
            if (this.content[row + 1] && this.content[row + 1][col]) {
                this.setCellSelection(this.content[row + 1][col]);
                return;
            }
            else if (this.content[0][col + 1]) {
                this.setCellSelection(this.content[0][col + 1]);
                return;
            }
        }
        if (this.content[row][col + 1]) {
            this.setCellSelection(this.content[row][col + 1]);
            return;
        }
        if (this.content[row + 1]) {
            this.setCellSelection(this.content[row + 1][0]);
            return;
        }
        this.setCellSelection(this.content[0][0]);
        return;
    };
    Grid.prototype.setEditCell = function (cell) {
        if (cell.allow !== true) {
            this.editing = false;
            return false;
        }
        else {
            this.emitCellChange(this.cellSelection.from);
        }
        if (this.keyValue !== undefined) {
            cell.formatted_value = cell.value = _.clone(this.keyValue);
            cell.dirty = true;
        }
        this.cellSelection.from = cell;
        this.cellSelection.to = cell;
        this.keepSelectorWithinView();
        this.editing = true;
        if (this.keyValue !== undefined) {
        }
        return true;
    };
    Grid.prototype.emitCellChange = function (cell) {
        if (!cell || !cell.dirty) {
            return;
        }
        this.keyValue = undefined;
        this.input.value = "";
        this.softMerges(this.content[this.cellSelection.from.index.row], this.cellSelection.from.index.row);
        this.drawGrid();
        cell.dirty = false;
    };
    Grid.prototype.isCanvas = function (target) {
        var clickedEl = target;
        while (clickedEl && clickedEl !== this.gc.canvas) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === this.gc.canvas;
    };
    Grid.prototype.isInput = function (target) {
        var clickedEl = target;
        while (clickedEl && clickedEl !== this.input) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === this.input;
    };
    Grid.prototype.getCell = function (x, y) {
        var rect = this.containerEl.getBoundingClientRect();
        x -= rect.left - this.offsets.x.offset * this.scale / this.ratio;
        y -= rect.top - this.offsets.y.offset * this.scale / this.ratio;
        var cell;
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                if (x < col.x * this.scale / this.ratio)
                    continue;
                if (x > (col.x + col.width) * this.scale / this.ratio)
                    continue;
                if (y < col.y * this.scale / this.ratio)
                    continue;
                if (y > (col.y + col.height) * this.scale / this.ratio)
                    continue;
                cell = col;
                break;
            }
            if (cell)
                break;
        }
        return cell;
    };
    Grid.prototype.cellHasHistory = function (cell, coords) {
        if (coords === void 0) { coords = { x: 0, y: 0 }; }
        var rect = this.containerEl.getBoundingClientRect();
        coords.x -= rect.left - this.offsets.x.offset * this.scale / this.ratio;
        coords.y -= rect.top - this.offsets.y.offset * this.scale / this.ratio;
        var history = this.history[cell.position.row] && this.history[cell.position.row][cell.position.col] >= 0;
        cell.history = {
            valid: history,
            show: history &&
                coords.x > (cell.x + cell.overlayWidth - 16) * this.scale / this.ratio &&
                coords.y < (cell.y + 16) * this.scale / this.ratio
                ? true
                : false,
            color: history ? this.userTrackingColors[this.history[cell.position.row][cell.position.col]] : ""
        };
    };
    Grid.prototype.createScrollbars = function () {
        var _this = this;
        ["x", "y"].forEach2(function (axis) {
            var anchor = _this.scrollbars[axis].anchor;
            _this.scrollbars[axis].track = document.createElement("div");
            _this.scrollbars[axis].handle = document.createElement("div");
            _this.scrollbars[axis].track.style["position"] = "absolute";
            _this.scrollbars[axis].track.style["z-index"] = 100;
            _this.scrollbars[axis].track.style["display"] = "none";
            _this.scrollbars[axis].track.className = "track";
            _this.scrollbars[axis].handle.style["position"] = "absolute";
            _this.scrollbars[axis].handle.style["z-index"] = 1;
            _this.scrollbars[axis].handle.style["left"] = 0;
            _this.scrollbars[axis].handle.style["top"] = 0;
            _this.scrollbars[axis].handle.className = "handle";
            _this.scrollbars[axis].handle.addEventListener("mousedown", function (evt) {
                console.log("mousedown");
                _this.scrollbars.scrolling = true;
                _this.scrollbars[axis].start = evt[axis];
                _this.scrollbars[axis].drag = true;
                _this.scrollbars[axis].offset = parseInt(_this.scrollbars[axis].handle.style[anchor], 10);
                evt.preventDefault();
            }, false);
            if (axis === "x") {
                _this.scrollbars[axis].track.style["left"] = 0;
                _this.scrollbars[axis].track.style["bottom"] = 0;
                _this.scrollbars[axis].track.style["right"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].track.style["height"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].handle.style["height"] = _this.scrollbars.width + "px";
            }
            else {
                _this.scrollbars[axis].track.style["top"] = 0;
                _this.scrollbars[axis].track.style["right"] = 0;
                _this.scrollbars[axis].track.style["bottom"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].track.style["width"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].handle.style["width"] = _this.scrollbars.width + "px";
            }
            _this.scrollbars[axis].track.appendChild(_this.scrollbars[axis].handle);
            _this.containerEl.appendChild(_this.scrollbars[axis].track);
        });
    };
    Grid.prototype.updateScrollbars = function () {
        var _this = this;
        if (this.touch || this.fluid) {
            return;
        }
        var show = false;
        ["x", "y"].forEach2(function (axis) {
            if (!_this.offsets[axis].maxOffset) {
                _this.scrollbars[axis].track.style.display = "none";
                _this.scrollbars[axis].handle.style.display = "none";
                _this.scrollbars[axis].show = false;
                _this.offsets[axis].index = 0;
                _this.offsets[axis].offset = 0;
                return;
            }
            var dimension = _this.stage[axis].label;
            var canvasSize = _this[dimension];
            _this.scrollbars[axis].show = true;
            _this.scrollbars[axis].handle.style.display = "block";
            var size = canvasSize * (canvasSize / (_this.stage[axis].size * _this.scale));
            _this.scrollbars[axis].handle.style[dimension] = size + "px";
            var offset = _this.offsets[axis].index / _this.offsets[axis].max * (canvasSize - size);
            var anchor = _this.scrollbars[axis].anchor;
            _this.scrollbars[axis].handle.style[anchor] = offset + "px";
            _this.scrollbars[axis].track.style.display = "block";
            _this.scrollbars[axis].track.style[dimension] = _this[dimension] + "px";
        });
    };
    Grid.prototype.createSelector = function () {
        var _this = this;
        this.selection = document.createElement("div");
        this.selection.className = "selection";
        if (!this.touch) {
            this.selection.contentEditable = true;
        }
        this.selection.style.position = "absolute";
        this.selection.style["z-index"] = 20;
        this.selection.style["left"] = 0;
        this.selection.style["top"] = 0;
        this.selection.style["display"] = "none";
        this.selection.addEventListener("paste", function (evt) {
            evt.preventDefault();
        });
        this.historyIndicator = document.createElement("div");
        this.historyIndicator.className = "history";
        this.historyIndicator.style.position = "absolute";
        this.historyIndicator.style["z-index"] = 30;
        this.historyIndicator.style["display"] = "none";
        this.input = document.createElement("input");
        this.input.setAttribute("autocapitalize", "none");
        this.input.style["position"] = "absolute";
        this.input.style["border"] = "0";
        this.input.style["font-size"] = "12px";
        this.input.style["color"] = "black";
        this.input.style["background-color"] = "ivory";
        this.input.style["box-sizing"] = "border-box";
        this.input.style["margin"] = "0";
        this.input.style["padding"] = "0 2px";
        this.input.style["outline"] = "0";
        this.input.style["left"] = "0px";
        this.input.style["right"] = "0px";
        this.input.style["bottom"] = "0px";
        this.input.style["top"] = "0px";
        this.input.style["width"] = "100%";
        this.input.style["text-align"] = "left";
        this.input.style["z-index"] = 10;
        this.input.style["visibility"] = "hidden";
        this.input.style["display"] = "display";
        this.input.style["height"] = "100%";
        this.input.addEventListener("keyup", function (evt) {
            _this.dirty = true;
            _this.emit(_this.ON_CELL_VALUE, { cell: _this.cellSelection.from, value: "" + _this.input.value });
        });
        this.input.addEventListener("touchstart", function (evt) {
        });
        this.input.addEventListener("paste", function (evt) {
            evt.stopPropagation();
            var text = "";
            try {
                text = evt.clipboardData.getData("text/plain");
            }
            catch (exception) { }
            try {
                if (!text) {
                    text = window.clipboardData.getData("Text");
                }
            }
            catch (exception) { }
            _this.dirty = true;
            _this.emit(_this.ON_CELL_VALUE, { cell: _this.cellSelection.from, value: "" + text });
        });
        if (this.touch) {
            this.selection.appendChild(this.input);
        }
        this.containerEl.appendChild(this.selection);
    };
    Grid.prototype.updateSelectorAttributes = function (cell, cellTo, selector, history) {
        if (history === void 0) { history = false; }
        var x = cell.x;
        var y = cell.y;
        var width = cellTo.x + cellTo.width - cell.x;
        var height = cellTo.y + cellTo.height - cell.y;
        if (cellTo.x < x) {
            x = cellTo.x;
            width = cell.x + cell.width - cellTo.x;
        }
        if (cellTo.y < y) {
            y = cellTo.y;
            height = cell.y + cell.height - cellTo.y;
        }
        if (!this.freezeRange.valid || cell.position.col >= this.freezeRange.index.col) {
            x -= this.offsets.x.offset;
        }
        x = x * this.scale / this.ratio;
        if (!this.freezeRange.valid || cell.position.row >= this.freezeRange.index.row) {
            y -= this.offsets.y.offset;
        }
        y = y * this.scale / this.ratio;
        width = (width + 1) * this.scale / this.ratio;
        height = (height + 1) * this.scale / this.ratio;
        if (history) {
            selector.style["left"] = x + width - this.historyIndicatorSize + "px";
            selector.style["background-color"] = cell.history.color;
        }
        else {
            selector.style["left"] = x + "px";
            selector.style["width"] = width + "px";
            selector.style["height"] = height + "px";
        }
        selector.style["top"] = y + "px";
        var show = true;
        if (this.freezeRange.valid) {
            if (cell.position.row > this.freezeRange.index.row - 1 &&
                cell.position.row - this.freezeRange.index.row < this.offsets.y.index) {
                show = false;
            }
            if (cell.position.col > this.freezeRange.index.col - 1 &&
                cell.position.col - this.freezeRange.index.col < this.offsets.x.index) {
                show = false;
            }
        }
        selector.style["display"] = show ? "block" : "none";
    };
    Grid.prototype.hideHistoryIndicator = function () {
        if (this.containerEl.contains(this.historyIndicator)) {
            this.containerEl.removeChild(this.historyIndicator);
        }
    };
    Grid.prototype.updateSelector = function (history) {
        var _this = this;
        if (!history) {
            this.hideHistoryIndicator();
        }
        if (this.cellSelection.on) {
            this.updateSelectorAttributes(this.cellSelection.from, this.cellSelection.to, this.selection);
        }
        this.found.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                if (_this.visible[rowIndex] && _this.visible[rowIndex][colIndex]) {
                    if (!_this.containerEl.contains(col.highlight)) {
                        _this.containerEl.appendChild(col.highlight);
                    }
                    _this.updateSelectorAttributes(col.cell, col.cell, col.highlight);
                }
                else {
                    if (_this.containerEl.contains(col.highlight)) {
                        _this.containerEl.removeChild(col.highlight);
                    }
                }
            });
        });
        this.links.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                if (_this.visible[rowIndex] && _this.visible[rowIndex][colIndex]) {
                    if (!_this.containerEl.contains(col.highlight)) {
                        _this.containerEl.appendChild(col.highlight);
                    }
                    _this.updateSelectorAttributes(col.cell, col.cell, col.highlight);
                }
                else {
                    if (_this.containerEl.contains(col.highlight)) {
                        _this.containerEl.removeChild(col.highlight);
                    }
                }
            });
        });
        this.trackingHighlights.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (highlight, colIndex) {
                if (!highlight || highlight === true)
                    return;
                if (_this.visible[rowIndex] && _this.visible[rowIndex][colIndex]) {
                    _this.updateSelectorAttributes(highlight.cell, highlight.cell, highlight.highlight);
                    if (!_this.containerEl.contains(highlight.highlight)) {
                        _this.containerEl.appendChild(highlight.highlight);
                    }
                    if (!highlight.run) {
                        highlight.flash();
                    }
                    else {
                        highlight.destroy();
                    }
                }
                else {
                    highlight.destroy();
                }
            });
        });
    };
    Grid.prototype.setOffsets = function (deltas, set) {
        var _this = this;
        if (set === void 0) { set = false; }
        ["x", "y"].forEach2(function (axis) {
            var stage = _this[_this.stage[axis].label] * 1 / _this.scale * _this.ratio;
            _this.offsets[axis].max = 0;
            _this.offsets[axis].maxOffset = _this.stage[axis].size - stage;
            if (_this.offsets[axis].maxOffset < 0) {
                _this.offsets[axis].maxOffset = 0;
            }
            _this.stage[axis].increments.forEach2(function (offset, index) {
                if (offset > _this.offsets[axis].maxOffset && !_this.offsets[axis].max) {
                    _this.offsets[axis].max = index;
                }
            });
        });
        if (deltas) {
            for (var axis in deltas) {
                if (!deltas[axis])
                    continue;
                if (set) {
                    var index = deltas[axis];
                    var label = axis === "x" ? "col" : "row";
                    if (this.freezeRange.valid) {
                        index -= this.freezeRange.index[label] + 2;
                    }
                    this.offsets[axis].index = index;
                }
                else {
                    this.offsets[axis].index += deltas[axis];
                }
                if (this.offsets[axis].index < 0) {
                    this.offsets[axis].index = 0;
                }
                if (this.offsets[axis].index >= this.offsets[axis].max) {
                    this.offsets[axis].index = this.offsets[axis].max;
                }
            }
        }
        ["x", "y"].forEach2(function (axis) {
            var offset = _this.stage[axis].increments[_this.offsets[axis].index];
            if (offset > _this.offsets[axis].maxOffset) {
                offset = _this.offsets[axis].maxOffset;
            }
            _this.offsets[axis].offset = offset;
            _this.allowBrowserScroll = _this.offsets[axis].index === _this.offsets[axis].max;
        });
    };
    Grid.prototype.resetStage = function () {
        this.stage = {
            x: {
                label: "width",
                pos: 0,
                increments: [],
                size: 0,
                stop: false
            },
            y: {
                label: "height",
                pos: 0,
                increments: [],
                size: 0,
                stop: false
            }
        };
    };
    Grid.prototype.setSize = function () {
        var rect = (this.containerRect = this.containerEl.getBoundingClientRect());
        if (!rect.width) {
            rect.width = this.stage.x.size;
            rect.height = this.stage.y.size;
        }
        var scrollbarWidth = (this.offsets.x.maxOffset || this.offsets.y.maxOffset) && !this.touch ? this.scrollbars.width : 0;
        var scrollbarHeight = (this.offsets.x.maxOffset || this.offsets.y.maxOffset) && !this.touch ? this.scrollbars.width : 0;
        var stageWidth = this.stage.x.size * this.scale / this.ratio;
        var stageHeight = this.stage.y.size * this.scale / this.ratio;
        if (rect.width > stageWidth || this.fluid || this.scrollbars.inset) {
            scrollbarHeight = 0;
        }
        if (rect.height > stageHeight || this.fluid || this.scrollbars.inset) {
            scrollbarWidth = 0;
        }
        this.width = this.fluid ? this.stage.x.size : rect.width - scrollbarWidth;
        this.height = this.fluid ? this.stage.y.size : rect.height - scrollbarHeight;
        this.gc.buffer.width = (this.fluid ? this.stage.x.size : rect.width - scrollbarWidth) * this.ratio;
        this.gc.buffer.height = (this.fluid ? this.stage.y.size : rect.height - scrollbarHeight) * this.ratio;
        this.gc.canvas.width = (this.fluid ? this.stage.x.size : rect.width - scrollbarWidth) * this.ratio;
        this.gc.canvas.height = (this.fluid ? this.stage.y.size : rect.height - scrollbarHeight) * this.ratio;
        this.gc.canvas.style.width = this.fluid
            ? this.stage.x.size + "px"
            : Math.floor(rect.width - scrollbarWidth) + "px";
        this.gc.canvas.style.height = this.fluid
            ? this.stage.y.size + "px"
            : Math.floor(rect.height - scrollbarHeight) + "px";
    };
    Grid.prototype.getTextOverlayWidth = function (row, col, width, textWidth) {
        var cell = this.originalContent[row][col];
        var textAlign = cell.style["text-align"];
        var left = 0;
        var right = 0;
        switch (textAlign) {
            case "right":
                left = -1;
                break;
            case "center":
                left = -1;
                right = 1;
                break;
            default:
                right = 1;
                break;
        }
        if (!this.content[row][col + right] || !this.content[row][col + left]) {
            return {
                col: col,
                width: width
            };
        }
        var endCol = col;
        var startCol = col;
        if (right > 0) {
            for (var i = col + 1; i < this.content[row].length; i++) {
                if (this.content[row][i].formatted_value ||
                    "" + this.content[row][i].value ||
                    this.content[row][i].allow === "no") {
                    break;
                }
                width += this.content[row][i].width;
                endCol = i;
                this.content[row][i].hidden = true;
                if (width > textWidth) {
                    break;
                }
            }
        }
        if (left < 0) {
            for (var i = col - 1; i >= 0; i--) {
                if (this.content[row][i].formatted_value ||
                    "" + this.content[row][i].value ||
                    this.content[row][i].allow === "no") {
                    break;
                }
                width += this.content[row][i].width;
                startCol = i;
                this.content[row][i].hidden = true;
                if (width > textWidth) {
                    break;
                }
            }
        }
        return {
            left: left,
            right: right,
            endCol: endCol,
            startCol: startCol,
            width: width
        };
    };
    Grid.prototype.softMerges = function (dataRow, dataRowIndex) {
        var _this = this;
        if (dataRow === void 0) { dataRow = []; }
        if (dataRowIndex === void 0) { dataRowIndex = -1; }
        dataRow.forEach2(function (col, colIndex) {
            var cell = _this.originalContent[dataRowIndex][colIndex];
            var width = parseFloat(cell.style.width);
            col.overlayWidth = width;
            col.overlayEnd = colIndex;
            col.overlayStart = colIndex;
            if ((cell.formatted_value || cell.value) && !col.wrap) {
                _this.gc.bufferContext.font = _this.getFontString(cell);
                var textWidth = _this.gc.bufferContext.measureText(_this.cleanValue(cell.formatted_value || cell.value)).width + 3;
                if (textWidth > width) {
                    var overlay = _this.getTextOverlayWidth(dataRowIndex, colIndex, width, textWidth);
                    col.overlayWidth = overlay.width;
                    col.overlayEnd = overlay.endCol;
                    col.overlayStart = overlay.startCol;
                }
            }
        });
    };
    Grid.prototype.getFontString = function (col) {
        return col.style["font-style"] + " " + col.style["font-weight"] + " " + col.style["font-size"] + " \"" + col.style["font-family"] + "\", sans-serif";
    };
    Grid.prototype.flattenStyles = function (styles) {
        var htmlStyle = {};
        this.cellStyles.forEach(function (s) {
            htmlStyle[s] = styles[s];
        });
        for (var key in this.borders.names) {
            var name_1 = this.borders.names[key];
            var width = this.borders.widths[styles[key + "bw"]] + "px";
            var style = styles[key + "bs"];
            var color = "#" + styles[key + "bc"].replace("#", "");
            htmlStyle["border-" + name_1] = width + " " + style + " " + color;
        }
        var str = "";
        for (var attr in htmlStyle) {
            var value = htmlStyle[attr];
            if ((attr === "background-color" || attr === "color") && value.indexOf("#") === -1) {
                value = "#" + value;
            }
            str += attr + ": " + value + "; ";
        }
        return str;
    };
    Grid.prototype.getBackgroundColor = function (originalCell, cell) {
        var color = "" + originalCell.style["background-color"] || "white";
        var hexColor = "#" + color.replace("#", "");
        if (this.helpers.validHex(hexColor)) {
            color = hexColor;
        }
        else if (color === "none") {
            color = "white";
        }
        if (cell.formatting && cell.formatting.background) {
            color = cell.formatting.background;
        }
        return color;
    };
    Grid.prototype.drawGraphics = function (cell, coords, col) {
        var _this = this;
        if (("" + col.value).indexOf("data:image") === 0) {
            var image_1 = new Image();
            image_1.onload = function () {
                _this.gc.canvasContext.drawImage(image_1, coords.x, coords.y, coords.width * _this.scale, coords.height * _this.scale);
            };
            image_1.src = col.value;
        }
        else if (!col.hidden) {
            this.drawRect(cell, { x: coords.x, y: coords.y, width: coords.width, height: coords.height }, col);
            this.drawText(cell, { x: coords.x, y: coords.y, width: coords.width, height: coords.height }, col);
            if (col.allow === "no") {
                var color = this.getBackgroundColor(cell, col);
                var contrast = this.helpers.getContrastYIQ(color);
                for (var img in this.noAccessImages) {
                    if (contrast !== img || !this.noAccessImages[img]) {
                        continue;
                    }
                    var pat = this.gc.bufferContext.createPattern(document.getElementById(this.noAccessImages[img]), "repeat");
                    this.gc.bufferContext.beginPath();
                    this.gc.bufferContext.rect(coords.x, coords.y, coords.width, coords.height);
                    this.gc.bufferContext.fillStyle = pat;
                    this.gc.bufferContext.fill();
                }
            }
        }
        if (cell.style.tbs !== "none") {
            this.gridLines.push([
                { x: coords._x - this.borders.widths[cell.style.tbw] / 2, y: coords.y },
                { x: coords._a, y: coords.y },
                { width: this.borders.widths[cell.style.tbw], color: cell.style.tbc }
            ]);
        }
        if (cell.style.rbs !== "none" && coords.a === coords._a && !col.hidden) {
            this.gridLines.push([
                { x: coords._a, y: coords.y },
                { x: coords._a, y: coords.b },
                { width: this.borders.widths[cell.style.rbw], color: cell.style.rbc }
            ]);
        }
        if (cell.style.bbs !== "none") {
            this.gridLines.push([
                { x: coords._x, y: coords.b },
                { x: coords._a, y: coords.b },
                { width: this.borders.widths[cell.style.bbw], color: cell.style.bbc }
            ]);
        }
        if (cell.style.lbs !== "none" && coords.x === coords._x && !col.hidden) {
            this.gridLines.push([
                { x: coords.x, y: coords.y },
                { x: coords.x, y: coords.b },
                { width: this.borders.widths[cell.style.lbw], color: cell.style.lbc }
            ]);
        }
    };
    Grid.prototype.drawLine = function (start, end, options) {
        var t = this.translateCanvas(options.width || 1);
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.lineWidth = options.width || 1;
        this.gc.bufferContext.strokeStyle = "#" + (options.color || "000000").replace("#", "");
        this.gc.bufferContext.moveTo(start.x, start.y);
        this.gc.bufferContext.lineTo(end.x, end.y);
        this.gc.bufferContext.stroke();
        this.resetCanvas(t);
    };
    Grid.prototype.drawTrackingRect = function (coords, cell) {
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.strokeStyle = this.userTrackingColors[this.history[cell.position.row][cell.position.col]];
        this.gc.bufferContext.lineWidth = 2;
        this.gc.bufferContext.rect(coords.x + 2, coords.y + 2, coords.width - 3, coords.height - 3);
        this.gc.bufferContext.stroke();
    };
    Grid.prototype.drawSortingIndicator = function (cell, direction) {
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.fillStyle = "red";
        var x = cell.x + cell.width - 10 - this.offsets.x.offset;
        if (direction === "DESC") {
            var y = cell.y;
            this.gc.bufferContext.moveTo(x, y);
            this.gc.bufferContext.lineTo(x + 10, y);
            this.gc.bufferContext.lineTo(x + 5, y + 5);
            this.gc.bufferContext.lineTo(x, y);
        }
        else {
            var y = cell.y + cell.height - 5;
            this.gc.bufferContext.moveTo(x + 5, y);
            this.gc.bufferContext.lineTo(x + 10, y + 5);
            this.gc.bufferContext.lineTo(x, y + 5);
            this.gc.bufferContext.lineTo(x + 5, y);
        }
        this.gc.bufferContext.fill();
    };
    Grid.prototype.drawOffsetRect = function (originalCell, coords, cell) {
        var color = this.getBackgroundColor(originalCell, cell);
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.fillStyle = color;
        this.gc.bufferContext.fillRect(coords.x - 0.5, coords.y - 0.5, coords.width + 0.5, coords.height + 0.5);
    };
    Grid.prototype.drawRect = function (originalCell, coords, cell) {
        var color = this.getBackgroundColor(originalCell, cell);
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.fillStyle = color;
        this.gc.bufferContext.fillRect(coords.x, coords.y, coords.width, coords.height);
    };
    Grid.prototype.cleanValue = function (value) {
        return (value + "").trim().replace(/\s\s+/g, " ");
    };
    Grid.prototype.drawText = function (originalCell, coords, cell) {
        var _this = this;
        if (cell.allow === "no") {
            return;
        }
        var v = this.cleanValue(originalCell.formatted_value || originalCell.value);
        var color = "#" + originalCell.style["color"].replace("#", "");
        if (cell.formatting && cell.formatting.color) {
            color = cell.formatting.color;
        }
        this.gc.bufferContext.fillStyle = color;
        this.gc.bufferContext.font = this.getFontString(originalCell);
        this.gc.bufferContext.textAlign = originalCell.style["text-align"]
            .replace("justify", "left")
            .replace("start", "left");
        var x = 0;
        var xPad = 3;
        var y = 0;
        var yPad = 5;
        switch (originalCell.style["text-align"]) {
            case "right":
                x = coords.x + coords.width - xPad;
                break;
            case "middle":
            case "center":
                x = coords.x + coords.width / 2;
                break;
            default:
                x = coords.x + xPad;
                break;
        }
        var wrapOffset = 0;
        var fontHeight = this.gc.bufferContext.measureText("M").width;
        var lines = [];
        if (cell.wrap) {
            lines = this.findLines(v, fontHeight, coords.width - xPad * 2);
            wrapOffset = lines.offset;
        }
        switch (originalCell.style["vertical-align"]) {
            case "top":
                y = coords.y + fontHeight + yPad;
                break;
            case "center":
            case "middle":
                y = coords.y + coords.height / 2 - wrapOffset / 2 + fontHeight / 2;
                break;
            default:
                y = coords.y + coords.height - yPad - wrapOffset;
                break;
        }
        this.gc.bufferContext.save();
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.rect(coords.x, coords.y, coords.width, coords.height);
        this.gc.bufferContext.clip();
        if (cell.wrap) {
            lines.lines.forEach2(function (line) {
                _this.gc.bufferContext.fillText(line.value, x, y + line.y);
                if (originalCell.link) {
                    var yLine = Math.round(y + 2 + line.y);
                    var tWidth = Math.round(_this.gc.bufferContext.measureText(line.value).width);
                    var xLine = x;
                    switch (originalCell.style["text-align"]) {
                        case "right":
                            xLine = x - tWidth;
                            break;
                        case "middle":
                        case "center":
                            xLine = x - tWidth / 2;
                            break;
                        default:
                            break;
                    }
                    _this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
                }
            });
        }
        else {
            if (originalCell.style["number-format"] &&
                originalCell.style["number-format"].indexOf("0.00") > -1 &&
                /(¥|€|£|\$)/.test(v)) {
                var num = v.split(" ");
                this.gc.bufferContext.fillText(num.length > 1 ? num[1] : v, x, y);
                if (num.length > 1) {
                    this.gc.bufferContext.textAlign = "left";
                    this.gc.bufferContext.fillText(num[0], coords.x + xPad, y);
                }
            }
            else {
                this.gc.bufferContext.fillText(v, x, y);
            }
            if (originalCell.link) {
                var yLine = Math.round(y + 2);
                var tWidth = Math.round(this.gc.bufferContext.measureText(v).width);
                var xLine = x;
                switch (originalCell.style["text-align"]) {
                    case "right":
                        xLine = x - tWidth;
                        break;
                    case "middle":
                    case "center":
                        xLine = x - tWidth / 2;
                        break;
                    default:
                        break;
                }
                this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
            }
        }
        this.gc.bufferContext.restore();
    };
    Grid.prototype.findLines = function (str, fontHeight, cellWidth) {
        var words = str.split(" ");
        var lines = [];
        var word = [];
        var lineY = 0;
        var lineOffset = fontHeight + 4;
        for (var w = 0; w < words.length; w++) {
            var wordWidth = this.gc.bufferContext.measureText(words[w]).width + 2;
            if (wordWidth > cellWidth) {
                var letters = words[w].split("");
                var width_1 = 0;
                wordWidth = 0;
                word = [];
                for (var i = 0; i < letters.length; i++) {
                    width_1 = this.gc.bufferContext.measureText(word.join("")).width + 2;
                    if (width_1 < cellWidth) {
                        word.push(letters[i]);
                        wordWidth = width_1;
                    }
                    else {
                        lines.push({
                            value: word.join(""),
                            width: wordWidth
                        });
                        word = [letters[i]];
                    }
                }
                if (word.length) {
                    lines.push({
                        value: word.join(""),
                        width: wordWidth
                    });
                }
            }
            else {
                lines.push({
                    value: words[w],
                    width: wordWidth
                });
            }
        }
        var rows = [];
        var width = 0;
        var lineWords = [];
        for (var i = 0; i < lines.length; i++) {
            width += lines[i].width;
            if (width < cellWidth) {
                lineWords.push(lines[i].value);
            }
            else {
                rows.push({
                    value: lineWords.join(" "),
                    y: lineY
                });
                lineY += lineOffset;
                lineWords = [lines[i].value];
                width = lines[i].width;
            }
        }
        if (lineWords.length) {
            rows.push({
                value: lineWords.join(" "),
                y: lineY
            });
        }
        return {
            lines: rows,
            offset: fontHeight * (rows.length - 1) + (rows.length - 1) * 4
        };
    };
    Grid.prototype.translateCanvas = function (w) {
        var translate = (w % 2) / 2;
        this.gc.bufferContext.translate(translate, translate);
        return translate;
    };
    Grid.prototype.resetCanvas = function (translate) {
        this.gc.bufferContext.translate(-translate, -translate);
    };
    return Grid;
}(Emitter_1.default));
exports.Grid = Grid;
var GridTrackingHighlight = (function () {
    function GridTrackingHighlight(cell, highlight) {
        this.cell = cell;
        this.highlight = highlight;
        this.run = false;
        this.lifetime = 3000;
        this.done = false;
        this.classy = new Classy_1.default();
    }
    GridTrackingHighlight.prototype.flash = function () {
        var _this = this;
        this.run = true;
        this.interval = setInterval(function () {
            if (!_this.highlight.parentNode) {
                return;
            }
            _this.classy.addClass(_this.highlight, "flash");
            clearInterval(_this.interval);
            setTimeout(function () {
                _this.done = true;
                _this.classy.addClass(_this.highlight, "done");
            }, _this.lifetime);
        }, 50);
    };
    GridTrackingHighlight.prototype.destroy = function () {
        if (!this.done || !this.highlight.parentNode) {
            return;
        }
        this.highlight.parentNode.removeChild(this.highlight);
    };
    return GridTrackingHighlight;
}());
var GridWrap = (function () {
    function GridWrap() {
        return Grid;
    }
    GridWrap.$inject = [];
    return GridWrap;
}());
exports.GridWrap = GridWrap;
//# sourceMappingURL=Grid.js.map