import { IGridModule } from "./Grid";
export interface ICanvas extends IGridModule {
    buffer: HTMLCanvasElement;
    bufferContext: CanvasRenderingContext2D;
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    update: (resize?: boolean) => void;
}
export declare class Canvas implements ICanvas {
    buffer: HTMLCanvasElement;
    bufferContext: CanvasRenderingContext2D;
    canvas: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    private gridLines;
    private sized;
    constructor();
    init(): void;
    update(resize?: boolean): void;
    resize(): void;
    destroy(): void;
    private draw;
    private drawGraphics;
    private drawLine;
    private drawTrackingRect;
    private drawSortingIndicator;
    private drawOffsetRect;
    private drawRect;
    private drawText;
    private findLines;
    private translateCanvas;
    private resetCanvas;
    private getFontString;
    private getBackgroundColor;
    private cleanValue;
}
