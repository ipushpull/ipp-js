"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Grid_1 = require("./Grid");
var Canvas = (function () {
    function Canvas() {
        var _this = this;
        this.gridLines = [];
        this.sized = false;
        this.draw = function () {
            if (!_this.canvas.width) {
                return;
            }
            _this.bufferContext.save();
            _this.bufferContext.clearRect(0, 0, Grid_1.GridBase.Content.width * Grid_1.GridBase.Content.ratio, Grid_1.GridBase.Content.height * Grid_1.GridBase.Content.ratio);
            _this.bufferContext.scale(Grid_1.GridBase.Content.scale, Grid_1.GridBase.Content.scale);
            _this.gridLines = [];
            for (var i = 0; i < Grid_1.GridBase.Content.visibleCells.length; i++) {
                var col = Grid_1.GridBase.Content.visibleCells[i];
                _this.drawOffsetRect(col);
            }
            for (var i = 0; i < Grid_1.GridBase.Content.visibleCells.length; i++) {
                var col = Grid_1.GridBase.Content.visibleCells[i];
                _this.drawGraphics(col);
            }
            _this.gridLines.forEach2(function (line) {
                _this.drawLine(line[0], line[1], line[2]);
            });
            for (var freeze in Grid_1.GridBase.Content.frozenCells) {
                _this.gridLines = [];
                Grid_1.GridBase.Content.frozenCells[freeze].forEach2(function (cell) {
                    _this.drawOffsetRect(cell);
                });
                Grid_1.GridBase.Content.frozenCells[freeze].forEach2(function (cell) {
                    _this.drawGraphics(cell);
                });
                _this.gridLines.forEach2(function (line) {
                    _this.drawLine(line[0], line[1], line[2]);
                });
            }
            if (Grid_1.GridBase.Content.visibleCells.length && Object.keys(Grid_1.GridBase.Content.sorting).length) {
                var colName = Object.keys(Grid_1.GridBase.Content.sorting)[0];
                var col = Grid_1.GridBase.Content.sorting[colName];
                var offset = Grid_1.GridBase.Settings.headings ? 1 : 0;
                var firstCell = Grid_1.GridBase.Content.getFirstAvailableCell(col.col);
                if (firstCell.cell) {
                    _this.drawSortingIndicator(firstCell.cell, col.direction);
                }
            }
            _this.bufferContext.restore();
        };
        this.init();
    }
    Canvas.prototype.init = function () {
        this.canvas = document.createElement("canvas");
        this.bufferContext = this.canvas.getContext("2d");
        Grid_1.GridBase.Container.addElement(this.canvas);
    };
    Canvas.prototype.update = function (resize) {
        if (Grid_1.GridBase.Content.hidden) {
            return;
        }
        if (resize || !this.sized) {
            this.sized = true;
            this.resize();
        }
        this.draw();
    };
    Canvas.prototype.resize = function () {
        this.canvas.width =
            (Grid_1.GridBase.Settings.fluid ? Grid_1.GridBase.Content.stage.x.size : Grid_1.GridBase.Content.width) * Grid_1.GridBase.Content.ratio;
        this.canvas.height =
            (Grid_1.GridBase.Settings.fluid ? Grid_1.GridBase.Content.stage.y.size : Grid_1.GridBase.Content.height) *
                Grid_1.GridBase.Content.ratio;
        this.canvas.style.width = Grid_1.GridBase.Settings.fluid
            ? Grid_1.GridBase.Content.stage.x.size + "px"
            : Math.floor(Grid_1.GridBase.Content.width) + "px";
        this.canvas.style.height = Grid_1.GridBase.Settings.fluid
            ? Grid_1.GridBase.Content.stage.y.size + "px"
            : Math.floor(Grid_1.GridBase.Content.height) + "px";
    };
    Canvas.prototype.destroy = function () {
        return;
    };
    Canvas.prototype.drawGraphics = function (col) {
        if (("" + col.value).indexOf("data:image") === 0) {
        }
        else if (!col.hidden) {
            this.drawRect(col);
            this.drawText(col);
            if (col.allow === "no" && !col.heading) {
                var color = this.getBackgroundColor(col);
                var contrast = Grid_1.GridBase.helpers.getContrastYIQ(color);
                for (var img in Grid_1.GridBase.Settings.noAccessImages) {
                    if (contrast !== img || !Grid_1.GridBase.Settings.noAccessImages[img]) {
                        continue;
                    }
                    var pat = this.bufferContext.createPattern(document.getElementById(Grid_1.GridBase.Settings.noAccessImages[img]), "repeat");
                    this.bufferContext.beginPath();
                    this.bufferContext.rect(col.coords.x, col.coords.y, col.coords.width, col.coords.height);
                    this.bufferContext.fillStyle = pat;
                    this.bufferContext.fill();
                }
            }
        }
        if (col.style.tbs !== "none") {
            this.gridLines.push([
                { x: col.coords._x - Grid_1.GridBase.Content.borders.widths[col.style.tbw] / 2, y: col.coords.y },
                { x: col.coords._a, y: col.coords.y },
                { width: Grid_1.GridBase.Content.borders.widths[col.style.tbw], color: col.style.tbc }
            ]);
        }
        if (col.style.rbs !== "none" && col.coords.a === col.coords._a && !col.hidden) {
            this.gridLines.push([
                { x: col.coords._a, y: col.coords.y },
                { x: col.coords._a, y: col.coords.b },
                { width: Grid_1.GridBase.Content.borders.widths[col.style.rbw], color: col.style.rbc }
            ]);
        }
        if (col.style.bbs !== "none") {
            this.gridLines.push([
                { x: col.coords._x, y: col.coords.b },
                { x: col.coords._a, y: col.coords.b },
                { width: Grid_1.GridBase.Content.borders.widths[col.style.bbw], color: col.style.bbc }
            ]);
        }
        if (col.style.lbs !== "none" && col.coords.x === col.coords._x && !col.hidden) {
            this.gridLines.push([
                { x: col.coords.x, y: col.coords.y },
                { x: col.coords.x, y: col.coords.b },
                { width: Grid_1.GridBase.Content.borders.widths[col.style.lbw], color: col.style.lbc }
            ]);
        }
    };
    Canvas.prototype.drawLine = function (start, end, options) {
        var t = this.translateCanvas(options.width || 1);
        this.bufferContext.beginPath();
        this.bufferContext.lineWidth = options.width || 1;
        this.bufferContext.strokeStyle = "#" + (options.color || "000000").replace("#", "");
        this.bufferContext.moveTo(start.x, start.y);
        this.bufferContext.lineTo(end.x, end.y);
        this.bufferContext.stroke();
        this.resetCanvas(t);
    };
    Canvas.prototype.drawTrackingRect = function (cell) {
        this.bufferContext.beginPath();
        this.bufferContext.strokeStyle =
            Grid_1.GridBase.Settings.userTrackingColors[Grid_1.GridBase.Content.history[cell.position.row][cell.position.col]];
        this.bufferContext.lineWidth = 2;
        this.bufferContext.rect(cell.coords.x + 2, cell.coords.y + 2, cell.coords.width - 3, cell.coords.height - 3);
        this.bufferContext.stroke();
    };
    Canvas.prototype.drawSortingIndicator = function (cell, direction) {
        this.bufferContext.beginPath();
        this.bufferContext.fillStyle = "red";
        var x = cell.x + cell.width - 10 - Grid_1.GridBase.Content.offsets.x.offset;
        if (direction === "DESC") {
            var y = cell.y;
            this.bufferContext.moveTo(x, y);
            this.bufferContext.lineTo(x + 10, y);
            this.bufferContext.lineTo(x + 5, y + 5);
            this.bufferContext.lineTo(x, y);
        }
        else {
            var y = cell.y + cell.height - 5;
            this.bufferContext.moveTo(x + 5, y);
            this.bufferContext.lineTo(x + 10, y + 5);
            this.bufferContext.lineTo(x, y + 5);
            this.bufferContext.lineTo(x + 5, y);
        }
        this.bufferContext.fill();
    };
    Canvas.prototype.drawOffsetRect = function (cell) {
        var color = this.getBackgroundColor(cell);
        this.bufferContext.beginPath();
        this.bufferContext.fillStyle = color;
        this.bufferContext.fillRect(cell.coords.x - 0.5, cell.coords.y - 0.5, cell.coords.width + 0.5, cell.coords.height + 0.5);
    };
    Canvas.prototype.drawRect = function (cell) {
        var color = this.getBackgroundColor(cell);
        this.bufferContext.beginPath();
        this.bufferContext.fillStyle = color;
        this.bufferContext.fillRect(cell.coords.x, cell.coords.y, cell.coords.width, cell.coords.height);
    };
    Canvas.prototype.drawText = function (cell) {
        var _this = this;
        if (!cell.heading && (cell.allow === "no" || cell.button)) {
            return;
        }
        var v = this.cleanValue(cell.formatted_value || cell.value);
        var color = "#" + cell.style["color"].replace("#", "");
        if (cell.formatting && cell.formatting.color) {
            color = cell.formatting.color;
        }
        this.bufferContext.fillStyle = color;
        this.bufferContext.font = this.getFontString(cell);
        this.bufferContext.textAlign = cell.style["text-align"]
            .replace("justify", "left")
            .replace("start", "left");
        var x = 0;
        var xPad = 3;
        var y = 0;
        var yPad = 5;
        switch (cell.style["text-align"]) {
            case "right":
                x = cell.coords.x + cell.coords.width - xPad;
                break;
            case "middle":
            case "center":
                x = cell.coords.x + cell.coords.width / 2;
                break;
            default:
                x = cell.coords.x + xPad;
                break;
        }
        var wrapOffset = 0;
        var fontHeight = this.bufferContext.measureText("M").width;
        var lines = [];
        if (cell.wrap) {
            lines = this.findLines(v, fontHeight, cell.coords.width - xPad * 2);
            wrapOffset = lines.offset;
        }
        switch (cell.style["vertical-align"]) {
            case "top":
                y = cell.coords.y + fontHeight + yPad;
                break;
            case "center":
            case "middle":
                y = cell.coords.y + cell.coords.height / 2 - wrapOffset / 2 + fontHeight / 2;
                break;
            default:
                y = cell.coords.y + cell.coords.height - yPad - wrapOffset;
                break;
        }
        this.bufferContext.save();
        this.bufferContext.beginPath();
        this.bufferContext.rect(cell.coords.x, cell.coords.y, cell.coords.width, cell.coords.height);
        this.bufferContext.clip();
        if (cell.wrap) {
            lines.lines.forEach2(function (line) {
                _this.bufferContext.fillText(line.value, x, y + line.y);
                if (cell.link && !cell.heading) {
                    var yLine = Math.round(y + 2 + line.y);
                    var tWidth = Math.round(_this.bufferContext.measureText(line.value).width);
                    var xLine = x;
                    switch (cell.style["text-align"]) {
                        case "right":
                            xLine = x - tWidth;
                            break;
                        case "middle":
                        case "center":
                            xLine = x - tWidth / 2;
                            break;
                        default:
                            break;
                    }
                    _this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
                }
            });
        }
        else {
            if (cell.style["number-format"] &&
                cell.style["number-format"].indexOf("0.00") > -1 &&
                /(¥|€|£|\$)/.test(v)) {
                var num = v.split(" ");
                this.bufferContext.fillText(num.length > 1 ? num[1] : v, x, y);
                if (num.length > 1) {
                    this.bufferContext.textAlign = "left";
                    this.bufferContext.fillText(num[0], cell.coords.x + xPad, y);
                }
            }
            else {
                this.bufferContext.fillText(v, x, y);
            }
            if (cell.link && !cell.heading) {
                var yLine = Math.round(y + 2);
                var tWidth = Math.round(this.bufferContext.measureText(v).width);
                var xLine = x;
                switch (cell.style["text-align"]) {
                    case "right":
                        xLine = x - tWidth;
                        break;
                    case "middle":
                    case "center":
                        xLine = x - tWidth / 2;
                        break;
                    default:
                        break;
                }
                this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
            }
        }
        this.bufferContext.restore();
    };
    Canvas.prototype.findLines = function (str, fontHeight, cellWidth) {
        var words = str.split(" ");
        var lines = [];
        var word = [];
        var lineY = 0;
        var lineOffset = fontHeight + 4;
        for (var w = 0; w < words.length; w++) {
            var wordWidth = this.bufferContext.measureText(words[w]).width + 2;
            if (wordWidth > cellWidth) {
                var letters = words[w].split("");
                var width_1 = 0;
                wordWidth = 0;
                word = [];
                for (var i = 0; i < letters.length; i++) {
                    width_1 = this.bufferContext.measureText(word.join("")).width + 2;
                    if (width_1 < cellWidth) {
                        word.push(letters[i]);
                        wordWidth = width_1;
                    }
                    else {
                        lines.push({
                            value: word.join(""),
                            width: wordWidth
                        });
                        word = [letters[i]];
                    }
                }
                if (word.length) {
                    lines.push({
                        value: word.join(""),
                        width: wordWidth
                    });
                }
            }
            else {
                lines.push({
                    value: words[w],
                    width: wordWidth
                });
            }
        }
        var rows = [];
        var width = 0;
        var lineWords = [];
        for (var i = 0; i < lines.length; i++) {
            width += lines[i].width;
            if (width < cellWidth) {
                lineWords.push(lines[i].value);
            }
            else {
                rows.push({
                    value: lineWords.join(" "),
                    y: lineY
                });
                lineY += lineOffset;
                lineWords = [lines[i].value];
                width = lines[i].width;
            }
        }
        if (lineWords.length) {
            rows.push({
                value: lineWords.join(" "),
                y: lineY
            });
        }
        return {
            lines: rows,
            offset: fontHeight * (rows.length - 1) + (rows.length - 1) * 4
        };
    };
    Canvas.prototype.translateCanvas = function (w) {
        var translate = (w % 2) / 2;
        this.bufferContext.translate(translate, translate);
        return translate;
    };
    Canvas.prototype.resetCanvas = function (translate) {
        this.bufferContext.translate(-translate, -translate);
    };
    Canvas.prototype.getFontString = function (col) {
        return col.style["font-style"] + " " + col.style["font-weight"] + " " + col.style["font-size"] + " \"" + col.style["font-family"] + "\", sans-serif";
    };
    Canvas.prototype.getBackgroundColor = function (cell) {
        var color = "" + cell.style["background-color"] || "white";
        var hexColor = "#" + color.replace("#", "");
        if (Grid_1.GridBase.helpers.validHex(hexColor)) {
            color = hexColor;
        }
        else if (color === "none") {
            color = "white";
        }
        if (cell.formatting && cell.formatting.background) {
            color = cell.formatting.background;
        }
        return color;
    };
    Canvas.prototype.cleanValue = function (value) {
        return (value + "").trim().replace(/\s\s+/g, " ");
    };
    return Canvas;
}());
exports.Canvas = Canvas;
//# sourceMappingURL=Canvas.js.map