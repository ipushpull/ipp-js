import { IGridModule } from "./Grid";
import { IContentCell } from "./Content";
interface ILinkOptions {
    address?: string;
    cell: any;
    className?: string;
    styles?: any;
    user?: number;
    color?: string;
}
export interface IContainer extends IGridModule {
    container: string | HTMLElement;
    element: HTMLElement;
    elementRect: IElementRect;
    input: HTMLInputElement;
    Scrollbars: IScrollbars;
    Selector: ISelector;
    addElement: (element: HTMLElement) => void;
    cleanLinks: () => void;
    addLink: (type: string, options: ILinkOptions) => void;
    addImage: (cell: IContentCell) => void;
    removeLink: (type: string, cell: IContentCell) => void;
    removeImage: (row: number, col: number) => void;
    updateSelectorAttributes: (cell: IContentCell, cellTo: IContentCell, selector: HTMLElement, history?: boolean) => void;
    showInput: () => void;
    hideInput: () => void;
    hasFocus: (evt: Event) => boolean;
    addGridLine(axis: string, cell: IContentCell): void;
    removeGridLine: (row: number, col: number) => void;
}
export interface IElementRect {
    left: number;
    top: number;
    width: number;
    height: number;
}
export interface IContainerRect {
    width: number;
    height: number;
}
export declare class Container implements IContainer {
    container: string | HTMLElement;
    element: HTMLElement;
    elementRect: IElementRect;
    input: HTMLInputElement;
    Scrollbars: IScrollbars;
    Selector: ISelector;
    links: any;
    lines: any;
    images: any;
    private zIndexes;
    private classy;
    constructor(container: string | HTMLElement);
    init(): void;
    update(): void;
    cleanLinks(): void;
    destroy(): void;
    hasFocus(evt: Event): boolean;
    addElement(element: HTMLElement): void;
    addLink(type: string, options: ILinkOptions): void;
    removeLink(type: string, cell: IContentCell): void;
    addImage(cell: IContentCell): void;
    removeImage(row: number, col: number): void;
    updateSelector(): void;
    showInput(): void;
    hideInput(): void;
    updateSelectorAttributes(cell: IContentCell, cellTo: IContentCell, selector: HTMLElement, history?: boolean): void;
    addGridLine(axis: string, cell: IContentCell): void;
    removeGridLine(row: number, col: number): void;
    private createImageElement;
    private createDomElement;
    private removeDomElement;
    private updateDomElement;
    private createInputElement;
    private isTarget;
    private updateLinks;
}
interface IScrollbars {
    visible: boolean;
    inset: boolean;
    size: number;
    xAxis: HTMLElement;
    xShow: boolean;
    yAxis: HTMLElement;
    yShow: boolean;
    update: () => void;
    destroy: () => void;
}
interface ISelector {
    visible: boolean;
    height: number;
    width: number;
    x: number;
    y: number;
    element: HTMLElement;
    update: () => void;
}
export {};
