"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var popper_js_1 = require("popper.js");
var Grid_1 = require("./Grid");
var Classy_1 = require("../Classy");
var Container = (function () {
    function Container(container) {
        var _this = this;
        this.container = container;
        this.links = {};
        this.lines = {};
        this.images = {};
        this.zIndexes = {
            history: 10,
            button: 20,
            highlight: 300,
        };
        this.onEvents = function (name, evt) {
            if (name === "mousedown") {
                if (_this.popper.style.display === "block"
                    && !_this.isTarget(evt.target, _this.Selector.element)
                    && !_this.isTarget(evt.target, _this.popperControl.reference)
                    && !_this.isTarget(evt.target, _this.popper)) {
                    _this.popper.style.display = "none";
                }
            }
        };
        this.init();
        this.classy = new Classy_1.default();
    }
    Container.prototype.init = function () {
        this.destroy();
        Grid_1.GridBase.Events.register(this.onEvents);
        if (typeof this.container === "string") {
            this.element = document.getElementById(this.container);
        }
        else {
            this.element = this.container;
        }
        this.element.style.overflow = "hidden";
        this.element.style.textAlign = "inherit";
        this.elementRect = this.element.getBoundingClientRect();
        this.createInputElement();
        this.Scrollbars = new Scrollbars();
        this.element.appendChild(this.Scrollbars.xAxis);
        this.element.appendChild(this.Scrollbars.yAxis);
        this.Selector = new Selector();
        this.Selector.element.appendChild(this.input);
        this.Selector.element.appendChild(this.select);
        this.element.appendChild(this.Selector.element);
    };
    Container.prototype.update = function () {
        this.elementRect = this.element.getBoundingClientRect();
        this.updateLinks();
        this.Scrollbars.update();
        this.Selector.update();
        if (!Grid_1.GridBase.Content.hasFocus) {
            this.hideInput();
        }
        if (Grid_1.GridBase.Content.noAccess) {
            this.classy.addClass(this.element, "denied");
        }
        else {
            this.classy.removeClass(this.element, "denied");
        }
    };
    Container.prototype.cleanLinks = function () {
        var link = this.element.getElementsByClassName("overlay-link");
        while (link[0]) {
            link[0].parentNode.removeChild(link[0]);
        }
        for (var i in this.links) {
            if (this.links[i] && this.links[i].line) {
                this.links[i].line.destroy();
            }
        }
        this.links = {};
        this.images = {};
    };
    Container.prototype.destroy = function () {
        Grid_1.GridBase.Events.unRegister(this.onEvents);
        if (!this.element) {
            return;
        }
        this.element.innerHTML = "";
        if (this.Scrollbars) {
            this.Scrollbars.destroy();
        }
    };
    Container.prototype.hasFocus = function (evt) {
        var _this = this;
        var focus = false;
        [Grid_1.GridBase.Canvas.canvas, this.Selector.element, this.input, this.select].forEach2(function (element) {
            if (_this.isTarget(evt.target, element)) {
                focus = true;
            }
        });
        if (!focus) {
            if (evt.target.className && evt.target.className.indexOf && evt.target.className.indexOf("-link") > -1) {
                focus = true;
            }
        }
        return focus;
    };
    Container.prototype.addElement = function (element) {
        this.element.appendChild(element);
    };
    Container.prototype.addLink = function (type, options) {
        var key = type + "-" + options.cell.position.row + "-" + options.cell.position.col;
        if (this.links[key]) {
            return;
        }
        var className = "overlay-link " + type + "-link";
        if (type === "history") {
            className += " user-" + options.user;
        }
        var elOptions = {
            cell: options.cell,
            className: className,
            color: options.color
        };
        if (options.external) {
            elOptions.href = options.address;
            elOptions.target = "_blank";
        }
        var link = {
            cell: options.cell,
            element: this.createDomElement(type, elOptions)
        };
        this.links[key] = link;
    };
    Container.prototype.getLink = function (type, cell) {
        var key = type + "-" + cell.position.row + "-" + cell.position.col;
        return this.links[key] || null;
    };
    Container.prototype.removeLink = function (type, cell) {
        var key = type + "-" + cell.position.row + "-" + cell.position.col;
        var link = this.links[key];
        if (!link) {
            return;
        }
        if (link.line) {
            link.line.destroy();
        }
        this.removeDomElement(this.links[key].element);
        this.links[key] = undefined;
    };
    Container.prototype.addImage = function (cell) {
        var key = "image-" + cell.position.row + "-" + cell.position.col;
        var image = this.images[key];
        if (image) {
            if (image.element.src !== cell.value) {
                image.element.src = cell.value;
            }
        }
        else {
            var image_1 = {
                cell: cell,
                element: this.createImageElement(cell)
            };
            this.images[key] = image_1;
            var element = this.createDomElement("image", {
                cell: cell,
                className: "overlay-link image-link",
            });
            element.appendChild(image_1.element);
            var link = {
                cell: cell,
                element: element
            };
            this.links[key] = link;
        }
    };
    Container.prototype.removeImage = function (row, col) {
        if (this.images[row] && this.images[row][col]) {
            this.removeDomElement(this.images[row][col].element);
            this.images[row][col] = undefined;
        }
    };
    Container.prototype.updateSelector = function () {
        this.Selector.update();
    };
    Container.prototype.showInput = function () {
        var _this = this;
        console.log("showInput");
        var cell = Grid_1.GridBase.Content.cells[Grid_1.GridBase.Content.cellSelection.from.index.row][Grid_1.GridBase.Content.cellSelection.from.index.col];
        var element = cell.button && cell.button.type ? cell.button.type : "input";
        if (["select"].indexOf(element) > -1) {
            if (element === "chat") {
                this.createSelectOptions(["Embed", "New Window"]);
            }
            else {
                this.createSelectOptions(cell.button.options);
            }
            this.popper.style.display = "block";
            var link = this.getLink("button", cell);
            this.popperControl = new popper_js_1.default(link.element, this.popper, { placement: "bottom-start" });
            return;
        }
        var unit = cell.style["font-size"].indexOf("px") > -1 ? "px" : "pt";
        this.input.style.visibility = "visible";
        this.input.style["font-size"] = "" + (parseFloat(cell.style["font-size"]) * Grid_1.GridBase.Content.scale) /
            Grid_1.GridBase.Content.ratio + unit;
        this.input.style["font-family"] = cell.style["font-family"] + ", sans-serif";
        this.input.style["font-weight"] = cell.style["font-weight"];
        this.input.style["text-align"] = cell.style["text-align"];
        this.input.style["visibility"] = "visible";
        if (Grid_1.GridBase.Content.keyValue !== undefined) {
            this.input.value = Grid_1.GridBase.Content.keyValue;
        }
        if (!Grid_1.GridBase.Settings.touch) {
            var interval_1 = setInterval(function () {
                if (_this.input.offsetParent === null)
                    return;
                clearInterval(interval_1);
                if (Grid_1.GridBase.Content.keyValue === undefined) {
                    _this.input.value = "" + cell.formatted_value;
                }
                _this.input.focus();
                if (element === "input") {
                    if (Grid_1.GridBase.Content.keyValue !== undefined) {
                        _this.input.selectionStart = 999;
                    }
                    else if (_this.input.value) {
                        _this.input.selectionStart = 0;
                        _this.input.selectionEnd = 999;
                    }
                }
            }, 20);
        }
        else {
            if (Grid_1.GridBase.Content.keyValue === undefined) {
                this.input.value = "" + cell.formatted_value;
            }
            this.input.focus();
            if (element === "input") {
                if (Grid_1.GridBase.Content.keyValue !== undefined) {
                    this.input.selectionStart = 999;
                }
                else if (this.input.value) {
                    this.input.selectionStart = 0;
                    this.input.selectionEnd = 999;
                }
            }
        }
    };
    Container.prototype.hideInput = function () {
        this.select.blur();
        this.input.blur();
        this.select.style.visibility = "hidden";
        this.input.style.visibility = "hidden";
        this.input.value = "";
    };
    Container.prototype.updateSelectorAttributes = function (cell, cellTo, selector, history) {
        if (history === void 0) { history = false; }
        var x = cell.x;
        var y = cell.y;
        var width = cellTo.x + cellTo.width - cell.x;
        var height = cellTo.y + cellTo.height - cell.y;
        var xFlip = false;
        var yFlip = false;
        if (cellTo.x < x) {
            xFlip = true;
            x = cellTo.x;
            width = cell.x + cell.width - cellTo.x;
        }
        if (cellTo.y < y) {
            yFlip = true;
            y = cellTo.y;
            height = cell.y + cell.height - cellTo.y;
        }
        if (!Grid_1.GridBase.Content.freezeRange.valid || cell.position.col >= Grid_1.GridBase.Content.freezeRange.index.col) {
            x -= Grid_1.GridBase.Content.offsets.x.offset;
            if (!xFlip && x < Grid_1.GridBase.Content.freezeRange.width) {
                x = Grid_1.GridBase.Content.freezeRange.width;
                width = cellTo.coords.x - Grid_1.GridBase.Content.freezeRange.width + cellTo.width;
            }
        }
        else {
            if (Grid_1.GridBase.Content.freezeRange.valid && cell.position.col < Grid_1.GridBase.Content.freezeRange.index.col) {
                var _width = cellTo.x + cellTo.width - x;
                var _freezeWidth = Grid_1.GridBase.Settings.headings ? Grid_1.GridBase.Content.freezeRange.width - 40 : Grid_1.GridBase.Content.freezeRange.width;
                if (_width < _freezeWidth) {
                    _freezeWidth = _width;
                }
                width = _width - Grid_1.GridBase.Content.offsets.x.offset < Grid_1.GridBase.Content.freezeRange.width ? _freezeWidth : _width - Grid_1.GridBase.Content.offsets.x.offset;
            }
            else {
                width = width - Grid_1.GridBase.Content.offsets.x.offset;
            }
        }
        if (width < 0)
            width = 0;
        x = (x * Grid_1.GridBase.Content.scale) / Grid_1.GridBase.Content.ratio;
        if (!Grid_1.GridBase.Content.freezeRange.valid || cell.position.row >= Grid_1.GridBase.Content.freezeRange.index.row) {
            y -= Grid_1.GridBase.Content.offsets.y.offset;
            if (!yFlip && y < Grid_1.GridBase.Content.freezeRange.height) {
                y = Grid_1.GridBase.Content.freezeRange.height;
                height = cellTo.coords.y - Grid_1.GridBase.Content.freezeRange.height + cellTo.height;
            }
        }
        else {
            if (Grid_1.GridBase.Content.freezeRange.valid && cell.position.row < Grid_1.GridBase.Content.freezeRange.index.row) {
                var _height = cellTo.y + cellTo.height - y;
                var _freezeHeight = Grid_1.GridBase.Settings.headings ? Grid_1.GridBase.Content.freezeRange.height - 20 : Grid_1.GridBase.Content.freezeRange.height;
                if (_height < _freezeHeight) {
                    _freezeHeight = _height;
                }
                height = _height - Grid_1.GridBase.Content.offsets.y.offset < Grid_1.GridBase.Content.freezeRange.height ? _freezeHeight : _height - Grid_1.GridBase.Content.offsets.y.offset;
            }
            else {
                height = height - Grid_1.GridBase.Content.offsets.y.offset;
            }
        }
        if (height < 0)
            height = 0;
        y = (y * Grid_1.GridBase.Content.scale) / Grid_1.GridBase.Content.ratio;
        width = ((width + 1) * Grid_1.GridBase.Content.scale) / Grid_1.GridBase.Content.ratio;
        height = ((height + 1) * Grid_1.GridBase.Content.scale) / Grid_1.GridBase.Content.ratio;
        selector.style["left"] = x + "px";
        selector.style["width"] = width + "px";
        selector.style["height"] = height + "px";
        selector.style["top"] = y + "px";
        selector.style["display"] = "block";
    };
    Container.prototype.addGridLine = function (axis, cell) {
        if (Grid_1.GridBase.Settings.touch) {
            return;
        }
        var key = "line-" + cell.position.row + "-" + cell.position.col;
        if (this.links[key]) {
            return;
        }
        var line = {
            cell: cell,
            element: this.createDomElement("line-" + axis, {
                cell: cell,
                className: "overlay-link line-link line-" + axis
            })
        };
        line.line = new GridLine(line.element, axis, cell);
        this.links[key] = line;
    };
    Container.prototype.removeGridLine = function (row, col) {
        if (this.links[row] && this.lines[row][col]) {
            this.removeDomElement(this.links[row][col].element);
            this.lines[row][col] = undefined;
        }
    };
    Container.prototype.createImageElement = function (cell) {
        var image = new Image();
        image.id = "image-" + cell.position.row + "-" + cell.position.col;
        image.className = "img-link";
        image.style.position = "absolute";
        image.style["z-index"] = 10;
        image.style["left"] = "0";
        image.style["top"] = "0";
        image.style["width"] = "100%";
        image.style["height"] = "100%";
        image.onload = function () {
            image.dataset.height = "" + image.height;
            image.dataset.width = "" + image.width;
        };
        image.src = cell.value;
        return image;
    };
    Container.prototype.createDomElement = function (type, options) {
        var element = document.createElement("a");
        element.dataset.type = type;
        if (options.attrs && Object.keys(options.attrs).length) {
            for (var a in options.attrs) {
                element[a] = options.attrs[a];
            }
        }
        if (type === "external" && options.href) {
            element.href = options.href;
            element.target = options.target;
        }
        element.className = options.className || "";
        element.style.position = "absolute";
        element.style["id"] = "overlay-" + options.cell.index.row + "-" + options.cell.index.col;
        element.style["z-index"] = this.zIndexes[type] || 10;
        element.style["top"] = "0";
        element.style["left"] = "0";
        element.style["width"] = options.cell.overlayWidth + "px";
        element.style["height"] = options.cell.height + "px";
        element.style["display"] = "block";
        var inner = document.createElement("div");
        inner.className = "inner-link";
        if (type === "highlight") {
            inner.style["background-color"] = "#" + options.color;
        }
        if (options.styles && Object.keys(options.styles).length) {
            for (var style in options.styles) {
                inner.style[style] = options.styles[style];
            }
        }
        if (type === "button") {
            inner.innerHTML = options.cell.formatted_value;
        }
        if (type.indexOf("line") > -1) {
            var inner2 = document.createElement("div");
            inner2.className = "inner-line";
            element.appendChild(inner2);
        }
        element.appendChild(inner);
        return element;
    };
    Container.prototype.removeDomElement = function (element) {
        if (this.element.contains(element)) {
            this.element.removeChild(element);
        }
    };
    Container.prototype.updateDomElement = function (link) {
        var x = (link.cell.coords.x * Grid_1.GridBase.Content.scale) / Grid_1.GridBase.Content.ratio;
        var y = (link.cell.coords.y * Grid_1.GridBase.Content.scale) / Grid_1.GridBase.Content.ratio;
        var height = (link.cell.height * Grid_1.GridBase.Content.scale) / Grid_1.GridBase.Content.ratio;
        var width = (link.cell.overlayWidth * Grid_1.GridBase.Content.scale) / Grid_1.GridBase.Content.ratio;
        link.element.style.left = x + "px";
        link.element.style.top = y + "px";
        link.element.style.height = height + "px";
        link.element.style.width = width + "px";
        var unit = link.cell.style["font-size"].indexOf("px") > -1 ? "px" : "pt";
        link.element.style["font-size"] = "" + (parseFloat(link.cell.style["font-size"]) * Grid_1.GridBase.Content.scale) /
            Grid_1.GridBase.Content.ratio + unit;
        link.element.style["font-family"] = link.cell.style["font-family"] + ", sans-serif";
        link.element.style["font-weight"] = link.cell.style["font-weight"];
        var inner = link.element.getElementsByClassName("inner-link")[0];
        if (link.element.dataset.type === "button") {
            if (link.cell.formatting) {
                inner.style["color"] = link.cell.formatting.color;
                inner.style["background-color"] = link.cell.formatting.background;
            }
            else {
                inner.style["color"] = "#" + link.cell.style.color;
                inner.style["background-color"] = "#" + link.cell.style["background-color"];
            }
            inner.innerHTML = link.cell.formatted_value;
        }
    };
    Container.prototype.createInputElement = function () {
        var _this = this;
        this.input = document.createElement("input");
        this.input.setAttribute("autocapitalize", "none");
        this.input.setAttribute("list", "gridlist");
        this.input.style["position"] = "absolute";
        this.input.style["border"] = "0";
        this.input.style["font-size"] = "12px";
        this.input.style["color"] = "black";
        this.input.style["background-color"] = "ivory";
        this.input.style["box-sizing"] = "border-box";
        this.input.style["margin"] = "0";
        this.input.style["padding"] = "0 2px";
        this.input.style["outline"] = "0";
        this.input.style["left"] = "0px";
        this.input.style["right"] = "0px";
        this.input.style["bottom"] = "0px";
        this.input.style["top"] = "0px";
        this.input.style["width"] = "100%";
        this.input.style["text-align"] = "left";
        this.input.style["z-index"] = 10;
        this.input.style["visibility"] = "hidden";
        this.input.style["display"] = "display";
        this.input.style["height"] = "100%";
        this.input.addEventListener("keyup", function (evt) {
            Grid_1.GridBase.Content.dirty = true;
            Grid_1.GridBase.emit(Grid_1.GridBase.ON_CELL_VALUE, {
                cell: Grid_1.GridBase.Content.cellSelection.from,
                value: "" + _this.input.value
            });
        });
        this.input.addEventListener("paste", function (evt) {
            evt.stopPropagation();
            var text = "";
            try {
                text = evt.clipboardData.getData("text/plain");
            }
            catch (exception) { }
            try {
                if (!text) {
                    text = window.clipboardData.getData("Text");
                }
            }
            catch (exception) { }
            Grid_1.GridBase.Content.dirty = true;
            Grid_1.GridBase.emit(Grid_1.GridBase.ON_CELL_VALUE, { cell: Grid_1.GridBase.Content.cellSelection.from, value: "" + text });
        });
        this.select = document.createElement("select");
        this.select.style["position"] = "absolute";
        this.select.style["border"] = "0";
        this.select.style["font-size"] = "12px";
        this.select.style["color"] = "black";
        this.select.style["background-color"] = "ivory";
        this.select.style["box-sizing"] = "border-box";
        this.select.style["margin"] = "0";
        this.select.style["padding"] = "0 2px";
        this.select.style["outline"] = "0";
        this.select.style["left"] = "0px";
        this.select.style["right"] = "0px";
        this.select.style["bottom"] = "0px";
        this.select.style["top"] = "0px";
        this.select.style["width"] = "100%";
        this.select.style["text-align"] = "left";
        this.select.style["z-index"] = 10;
        this.select.style["visibility"] = "hidden";
        this.select.style["display"] = "display";
        this.select.style["height"] = "100%";
        this.select.addEventListener("change", function (evt) {
            Grid_1.GridBase.emit(Grid_1.GridBase.ON_CELL_VALUE, {
                cell: Grid_1.GridBase.Content.cellSelection.from,
                value: "" + _this.select.value
            });
        });
        this.popper = document.createElement("div");
        this.popper.className = "button-menu";
        this.popper.style["display"] = "none";
        this.popper.addEventListener("mouseleave", function (evt) {
            _this.popper.style["display"] = "none";
        });
        this.element.appendChild(this.popper);
    };
    Container.prototype.createSelectOptions = function (options) {
        var _this = this;
        this.popper.innerHTML = "";
        options.forEach(function (value) {
            var option = document.createElement("a");
            option.innerText = value;
            option.addEventListener("click", function () {
                Grid_1.GridBase.emit(Grid_1.GridBase.ON_CELL_VALUE, {
                    cell: Grid_1.GridBase.Content.cellSelection.from,
                    value: "" + value
                });
                _this.popper.style.display = "none";
            });
            _this.popper.appendChild(option);
        });
    };
    Container.prototype.isTarget = function (evtTarget, element) {
        var clickedEl = evtTarget;
        while (clickedEl && clickedEl !== element) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === element;
    };
    Container.prototype.updateLinks = function () {
        var _this = this;
        var _loop_1 = function (key) {
            var link = this_1.links[key];
            if (!link) {
                return "continue";
            }
            if (link.element.dataset.type === "highlight" && !link.element.dataset.active) {
                link.element.dataset.active = "1";
                link.element.classList.add("flash");
                if (link.cell.visible) {
                    this_1.updateDomElement(link);
                    this_1.element.appendChild(link.element);
                }
                setTimeout(function () {
                    link.element.classList.add("done");
                    setTimeout(function () {
                        if (_this.element.contains(link.element)) {
                            _this.element.removeChild(link.element);
                        }
                        if (_this.links[key]) {
                            delete _this.links[key];
                        }
                    }, Grid_1.GridBase.Settings.highlightsRemoveTime);
                }, Grid_1.GridBase.Settings.highlightsShowTime);
            }
            else if (link.cell.visible) {
                this_1.updateDomElement(link);
                if (!this_1.element.contains(link.element)) {
                    this_1.element.appendChild(link.element);
                }
            }
            else {
                if (this_1.element.contains(link.element)) {
                    this_1.element.removeChild(link.element);
                }
            }
        };
        var this_1 = this;
        for (var key in this.links) {
            _loop_1(key);
        }
    };
    return Container;
}());
exports.Container = Container;
var Scrollbars = (function () {
    function Scrollbars() {
        var _this = this;
        this.inset = false;
        this.size = 8;
        this.xShow = false;
        this.yShow = false;
        this.scrollbars = {
            x: {
                drag: false,
                dimension: "width",
                label: "col",
                anchor: "left"
            },
            y: {
                drag: false,
                dimension: "height",
                label: "row",
                anchor: "top"
            }
        };
        this._visible = false;
        this.onEvents = function (name, args) {
            if (name === "mousedown") {
            }
            if (name === "mouseup") {
                _this.onMouseUp(args);
            }
            if (name === "mousemove") {
                _this.onMouseMove(args);
            }
        };
        this.onMouseDown = function (evt) {
            var scrolling = false;
            ["x", "y"].forEach2(function (axis) {
                if (_this[axis + "AxisHandle"] === evt.target) {
                    var anchor = _this.scrollbars[axis].anchor;
                    _this.scrollbars[axis].start = evt[axis];
                    _this.scrollbars[axis].drag = true;
                    _this.scrollbars[axis].offset = parseInt(_this[axis + "AxisHandle"].style[anchor], 10);
                    scrolling = true;
                }
            });
            if (scrolling) {
                Grid_1.GridBase.Content.scrolling = true;
            }
            evt.preventDefault();
        };
        this.onMouseUp = function (evt) {
            ["x", "y"].forEach2(function (axis) {
                _this.scrollbars[axis].drag = false;
            });
            Grid_1.GridBase.Content.scrolling = false;
        };
        this.onMouseMove = function (evt) {
            ["x", "y"].forEach2(function (axis) {
                if (!_this.scrollbars[axis].drag)
                    return;
                var dimension = Grid_1.GridBase.Content.stage[axis].label;
                var canvasSize = Grid_1.GridBase.Content[dimension];
                var size = canvasSize * (canvasSize / (Grid_1.GridBase.Content.stage[axis].size * Grid_1.GridBase.Content.scale));
                var distance = canvasSize - size;
                var offset = evt[axis] - _this.scrollbars[axis].start + _this.scrollbars[axis].offset;
                if (offset > distance) {
                    offset = distance;
                }
                else if (offset < 0) {
                    offset = 0;
                }
                var anchor = _this.scrollbars[axis].anchor;
                _this[axis + "AxisHandle"].style[anchor] = offset + "px";
                var ratio = offset / distance;
                var index = Math.round(Grid_1.GridBase.Content.offsets[axis].max * ratio);
                Grid_1.GridBase.Content.offsets[axis].index = index;
                var scrollOffset = Grid_1.GridBase.Content.stage[axis].increments[index];
                if (scrollOffset > Grid_1.GridBase.Content.offsets[axis].maxOffset) {
                    scrollOffset = Grid_1.GridBase.Content.offsets[axis].maxOffset;
                }
                Grid_1.GridBase.Content.offsets[axis].offset = scrollOffset;
            });
        };
        this.xAxis = document.createElement("div");
        this.xAxis.className = "track track-x";
        this.xAxis.style.display = "none";
        this.xAxis.style.height = this.size + "px";
        this.xAxis.style.position = "absolute";
        this.xAxis.style.left = "0px";
        this.xAxis.style.bottom = "0px";
        this.xAxis.style.right = "0px";
        this.xAxis.style["z-index"] = "1000";
        this.xAxisHandle = document.createElement("div");
        this.xAxisHandle.className = "handle";
        this.xAxisHandle.style.position = "absolute";
        this.xAxisHandle.style.height = this.size + "px";
        this.xAxisHandle.style.top = "0px";
        this.xAxisHandle.style.left = "0px";
        this.xAxisHandle.style["z-index"] = 1;
        this.xAxis.appendChild(this.xAxisHandle);
        this.yAxis = document.createElement("div");
        this.yAxis.className = "track track-y";
        this.yAxis.style.display = "none";
        this.yAxis.style.width = this.size + "px";
        this.yAxis.style.position = "absolute";
        this.yAxis.style.top = "0px";
        this.yAxis.style.bottom = "0px";
        this.yAxis.style.right = "0px";
        this.yAxis.style["z-index"] = "1000";
        this.yAxisHandle = document.createElement("div");
        this.yAxisHandle.className = "handle";
        this.yAxisHandle.style.width = this.size + "px";
        this.yAxisHandle.style.position = "absolute";
        this.yAxisHandle.style.top = "0px";
        this.yAxisHandle.style.left = "0px";
        this.yAxisHandle.style["z-index"] = 1;
        this.yAxis.appendChild(this.yAxisHandle);
        Grid_1.GridBase.Events.register(this.onEvents);
        this.xAxisHandle.addEventListener("mousedown", this.onMouseDown);
        this.yAxisHandle.addEventListener("mousedown", this.onMouseDown);
    }
    Object.defineProperty(Scrollbars.prototype, "visible", {
        get: function () {
            return this._visible;
        },
        set: function (value) {
            this._visible = value;
        },
        enumerable: true,
        configurable: true
    });
    Scrollbars.prototype.update = function () {
        var _this = this;
        if (Grid_1.GridBase.Settings.touch || Grid_1.GridBase.Settings.fluid) {
            return;
        }
        ["x", "y"].forEach2(function (axis) {
            if (!Grid_1.GridBase.Content.offsets[axis].maxOffset) {
                _this[axis + "Axis"].style.display = "none";
                _this[axis + "AxisHandle"].style.display = "none";
                _this.scrollbars[axis].show = false;
                Grid_1.GridBase.Content.offsets[axis].index = 0;
                Grid_1.GridBase.Content.offsets[axis].offset = 0;
                return;
            }
            var dimension = Grid_1.GridBase.Content.stage[axis].label;
            var canvasSize = Grid_1.GridBase.Content[dimension];
            _this.scrollbars[axis].show = true;
            _this[axis + "AxisHandle"].style.display = "block";
            var size = canvasSize * (canvasSize / (Grid_1.GridBase.Content.stage[axis].size * Grid_1.GridBase.Content.scale));
            _this[axis + "AxisHandle"].style[dimension] = size + "px";
            var offset = (Grid_1.GridBase.Content.offsets[axis].index / Grid_1.GridBase.Content.offsets[axis].max) * (canvasSize - size);
            var anchor = _this.scrollbars[axis].anchor;
            _this[axis + "AxisHandle"].style[anchor] = offset + "px";
            _this[axis + "Axis"].style.display = "block";
            _this[axis + "Axis"].style[dimension] = Grid_1.GridBase.Content[dimension] + "px";
        });
    };
    Scrollbars.prototype.destroy = function () {
        Grid_1.GridBase.Events.unRegister(this.onEvents);
        this.xAxisHandle.removeEventListener("mousedown", this.onMouseDown);
        this.yAxisHandle.removeEventListener("mousedown", this.onMouseDown);
    };
    return Scrollbars;
}());
var Selector = (function () {
    function Selector() {
        this._visible = false;
        this._height = 0;
        this._width = 0;
        this._x = 0;
        this._y = 0;
        this.buttons = {};
        this.element = document.createElement("div");
        this.element.className = "selection";
        this.element.style.position = "absolute";
        this.element.style["z-index"] = 100;
        this.element.style.left = "0px";
        this.element.style.top = "0px";
        this.element.style.display = "none";
    }
    Object.defineProperty(Selector.prototype, "visible", {
        get: function () {
            return this._visible;
        },
        set: function (value) {
            this._visible = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Selector.prototype, "height", {
        set: function (value) {
            this._height = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Selector.prototype, "width", {
        set: function (value) {
            this._width = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Selector.prototype, "x", {
        set: function (value) {
            this._x = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Selector.prototype, "y", {
        set: function (value) {
            this._y = value;
        },
        enumerable: true,
        configurable: true
    });
    Selector.prototype.update = function () {
        var show = Grid_1.GridBase.Content.cellSelection.from ? true : false;
        if (!show || Grid_1.GridBase.Settings.disallowSelection) {
            this.element.style.display = "none";
            return;
        }
        Grid_1.GridBase.Container.updateSelectorAttributes(Grid_1.GridBase.Content.cellSelection.from, Grid_1.GridBase.Content.cellSelection.to || Grid_1.GridBase.Content.cellSelection.from, this.element);
    };
    return Selector;
}());
var GridLine = (function () {
    function GridLine(element, axis, cell) {
        var _this = this;
        this.element = element;
        this.axis = axis;
        this.cell = cell;
        this.moving = false;
        this.width = 0;
        this.height = 0;
        this.onEvents = function (name, evt) {
            if (name === "mousedown") {
                _this.onMouseDown(evt);
            }
            if (name === "mouseup") {
                _this.onMouseUp(evt);
            }
            if (name === "mousemove") {
                _this.onMouseMove(evt);
            }
        };
        this.onMouseDown = function (evt) {
            if (evt.target !== _this.innerLine) {
                return;
            }
            Grid_1.GridBase.Content.setSelector();
            Grid_1.GridBase.Content.resizing = true;
            _this.moving = true;
            _this.width = _this.cell.width * 1;
            _this.height = _this.cell.height * 1;
            _this.startX = evt.x;
            _this.startY = evt.y;
            _this.zIndex = _this.element.style["z-index"];
            _this.element.style["z-index"] = _this.zIndex * 1000;
            _this.element.classList.add("active");
        };
        this.onMouseUp = function (evt) {
            if (!_this.moving) {
                return;
            }
            Grid_1.GridBase.Content.resizing = false;
            _this.moving = false;
            _this.element.style["z-index"] = _this.zIndex / 1000;
            _this.element.classList.remove("active");
            _this.emitSize();
        };
        this.onMouseMove = function (evt) {
            if (!_this.moving) {
                return;
            }
            _this.offsetX = evt.x - _this.startX;
            _this.offsetY = evt.y - _this.startY;
            var width = _this.width + _this.offsetX;
            if (width < 20)
                width = 20;
            var height = _this.height + _this.offsetY;
            if (height < 20)
                height = 20;
            if (_this.axis === "col") {
                Grid_1.GridBase.Content.setColSize(_this.cell, width);
            }
            else {
                Grid_1.GridBase.Content.setRowSize(_this.cell, height);
            }
        };
        this.innerLine = element.getElementsByClassName("inner-line")[0];
        if (!this.innerLine) {
            return;
        }
        Grid_1.GridBase.Events.register(this.onEvents);
        this.width = cell.width * 1;
        this.height = cell.height * 1;
    }
    GridLine.prototype.destroy = function () {
        Grid_1.GridBase.Events.unRegister(this.onEvents);
    };
    GridLine.prototype.emitSize = function () {
        Grid_1.GridBase.emit(Grid_1.GridBase.ON_RESIZE, { axis: this.axis, x: this.offsetX, y: this.offsetY, cell: this.cell });
    };
    return GridLine;
}());
//# sourceMappingURL=Container.js.map