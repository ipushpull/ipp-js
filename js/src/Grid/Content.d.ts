import { IGridModule } from "./Grid";
interface IStage {
    x: IStageCoords;
    y: IStageCoords;
}
interface IStageCoords {
    pos: number;
    increments: any;
    size: any;
    stop: boolean;
    label: string;
}
interface IOffets {
    x: IOffsetsCoords;
    y: IOffsetsCoords;
}
interface IOffsetsCoords {
    offset: number;
    index: number;
    max: number;
    maxOffset: number;
}
export interface IContentFind {
    query: string;
}
export interface IContent extends IGridModule {
    noAccess: boolean;
    dirty: boolean;
    hidden: boolean;
    ctrlDown: boolean;
    shiftDown: boolean;
    hasFocus: boolean;
    editing: boolean;
    scale: number;
    ratio: number;
    originalContent: any[];
    content: any[];
    cells: any[];
    visibleCells: any[];
    trackedCells: any[];
    frozenCells: any;
    stage: IStage;
    offsets: IOffets;
    borders: any;
    width: number;
    height: number;
    resizing: boolean;
    scrolling: boolean;
    scrollbarHeight: number;
    scrollbarWidth: number;
    headingSelected: string;
    history: any[];
    tracking: boolean;
    freezeRange: IContentFreezeRange;
    cellSelection: IContentCellSelection;
    keyValue: string;
    set: (data: any) => void;
    update: () => void;
    move: () => void;
    setOffsets: (deltas?: any, set?: boolean) => void;
    setScale: (n: number) => void;
    setFit: (n: string) => void;
    cellHistory: (data: any) => void;
    cellHighlights: (data: any) => void;
    setMergeRanges: (data: any) => void;
    setRanges: (data: any) => void;
    setSorting: (data: any) => void;
    clearSorting: () => void;
    setFilters: (data: any) => void;
    clearFilters: () => void;
    find: (data: IContentFind) => void;
    clearFound: () => void;
    getContentHtml: () => string;
    getCells: (fromCell: any, toCell: any) => any;
    setSelector(from?: any, to?: any): void;
    setColSize(headingCell: IContentCell, value: number): void;
    setRowSize(headingCell: IContentCell, value: number): void;
    keepSelectorWithinView(): void;
}
interface IContentCellPosition {
    row: number;
    col: number;
}
export interface IContentCellSelection {
    from: IContentCell;
    to: IContentCell;
    last: IContentCell;
    go: boolean;
    on: boolean;
    clicked: number;
    found: any;
}
interface IContentFreezeRangeIndex {
    row: number;
    col: number;
}
interface IContentFreezeRange {
    valid: boolean;
    width: number;
    height: number;
    index: IContentFreezeRangeIndex;
}
interface IContentCellCoords {
    a: number;
    b: number;
    height: number;
    width: number;
    x: number;
    y: number;
    _a: number;
    _x: number;
}
export interface IContentCell {
    allow: boolean | string;
    button: any;
    formatted_value: string;
    height: number;
    hidden: boolean;
    heading: boolean;
    image: boolean;
    visible: boolean;
    history: any;
    index: IContentCellPosition;
    link: any;
    overlayEnd: number;
    overlayStart: number;
    overlayWidth: number;
    position: IContentCellPosition;
    style: any;
    formatting: any;
    value: string;
    width: number;
    wrap: boolean;
    x: number;
    y: number;
    coords: IContentCellCoords;
}
export interface IHeadingSelection {
    rightClick: boolean;
    which: string;
    selection: IContentCellSelection;
    event: MouseEvent;
    count: number;
}
export declare class Content implements IContent {
    noAccess: boolean;
    dirty: boolean;
    hidden: boolean;
    ctrlDown: boolean;
    shiftDown: boolean;
    hasFocus: boolean;
    scale: number;
    ratio: number;
    originalContent: any[];
    content: any[];
    cells: any[];
    visibleCells: any[];
    frozenCells: any;
    trackedCells: any[];
    stage: IStage;
    width: number;
    height: number;
    resizing: boolean;
    scrolling: boolean;
    scrollbarHeight: number;
    scrollbarWidth: number;
    offsets: {
        x: {
            offset: number;
            index: number;
            max: number;
            maxOffset: number;
        };
        y: {
            offset: number;
            index: number;
            max: number;
            maxOffset: number;
        };
    };
    borders: {
        widths: {
            none: number;
            thin: number;
            medium: number;
            thick: number;
        };
        styles: {
            none: string;
            solid: string;
            double: string;
        };
        names: {
            t: string;
            r: string;
            b: string;
            l: string;
        };
    };
    history: any[];
    highlights: any[];
    tracking: boolean;
    cellSelection: IContentCellSelection;
    freezeRange: IContentFreezeRange;
    keyValue: string;
    headingSelected: string;
    private _editing;
    private mergeRanges;
    private styleRanges;
    private buttonRanges;
    private accessRanges;
    private found;
    private foundSelection;
    private sorting;
    private filters;
    private cellStyles;
    constructor();
    editing: boolean;
    init(): void;
    set(data: any): void;
    update(): void;
    move(): void;
    destroy(): void;
    setOffsets(deltas?: any, set?: boolean): void;
    setScale(n: number): void;
    setFit(n: any): void;
    cellHistory(data: any): void;
    cellHighlights(data: any): void;
    clearFilters(): void;
    setFilters(filters: any): void;
    clearSorting(): void;
    setSorting(filters: any): void;
    setMergeRanges(ranges: any): void;
    setRanges(ranges: any): void;
    find(data: IContentFind): void;
    clearFound(): void;
    getContentHtml(): string;
    getCells(fromCell: any, toCell: any): any;
    setSelector(from?: any, to?: any): void;
    setColSize(headingCell: IContentCell, value: number): void;
    setRowSize(headingCell: IContentCell, value: number): void;
    keepSelectorWithinView(): void;
    private flattenStyles;
    private resetStage;
    private resetRanges;
    private setCellAccess;
    private setStage;
    private setSize;
    private setVisibility;
    private softMerges;
    private getFontString;
    private cleanValue;
    private getTextOverlayWidth;
    private getCell;
    private onEvents;
    private onKeyDown;
    private onMouseUp;
    private selectHeadingCell;
    private selectCell;
    private selectNextCell;
    private setCellSelection;
    private clearCellSelection;
    private updateCellSelection;
    private updateFit;
    private applyFilters;
    private applySorting;
    private clone;
    private setHeadingCellStyle;
    private toColumnName;
}
export {};
