"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("underscore");
var Range_1 = require("../Page/Range");
var Range_2 = require("../Page/Range");
var Range_3 = require("../Page/Range");
var Grid_1 = require("./Grid");
var Content = (function () {
    function Content() {
        var _this = this;
        this.noAccess = false;
        this.dirty = false;
        this.hidden = false;
        this.ctrlDown = false;
        this.shiftDown = false;
        this.hasFocus = false;
        this.scale = 1;
        this.ratio = 1;
        this.originalContent = [];
        this.content = [];
        this.cells = [];
        this.visibleCells = [];
        this.frozenCells = {
            cols: [],
            rows: [],
            common: []
        };
        this.hiddenColumns = [];
        this.trackedCells = [];
        this.width = 0;
        this.height = 0;
        this.resizing = false;
        this.scrolling = false;
        this.scrollbarHeight = 8;
        this.scrollbarWidth = 8;
        this.offsets = {
            x: {
                offset: 0,
                index: 0,
                max: 0,
                maxOffset: 0
            },
            y: {
                offset: 0,
                index: 0,
                max: 0,
                maxOffset: 0
            }
        };
        this.borders = {
            widths: {
                none: 0,
                thin: 1,
                medium: 2,
                thick: 3
            },
            styles: {
                none: "solid",
                solid: "solid",
                double: "double"
            },
            names: {
                t: "top",
                r: "right",
                b: "bottom",
                l: "left"
            }
        };
        this.history = [];
        this.highlights = [];
        this.tracking = false;
        this.cellSelection = {
            from: undefined,
            to: undefined,
            last: undefined,
            go: false,
            on: false,
            clicked: 0,
            found: undefined,
            multiple: false
        };
        this.freezeRange = {
            valid: false,
            height: 0,
            width: 0,
            index: {
                row: 1,
                col: 1
            }
        };
        this.headingSelected = "";
        this._editing = false;
        this.mergeRanges = [];
        this.styleRanges = [];
        this.buttonRanges = [];
        this.buttons = [];
        this.styles = [];
        this.accessRanges = [];
        this.found = [];
        this.foundSelection = {
            query: "",
            count: 0,
            selected: 0,
            cell: undefined
        };
        this.sorting = {};
        this.filters = {};
        this.cellStyles = [
            "background-color",
            "color",
            "font-family",
            "font-size",
            "font-style",
            "font-weight",
            "height",
            "number-format",
            "text-align",
            "text-wrap",
            "width",
            "vertical-align"
        ];
        this.onEvents = function (name, evt) {
            if (name === "keydown") {
                if (evt.keyCode === 27) {
                    _this.dirty = false;
                    _this.clearCellSelection();
                    Grid_1.GridBase.emit(Grid_1.GridBase.ON_CELL_RESET, _this.cellSelection.last);
                    return;
                }
            }
            if (name === "mousemove") {
                _this.updateCellSelection(evt);
                if (_this.scrolling) {
                    _this.setVisibility();
                }
            }
            if (!_this.hasFocus) {
                if (name === "keydown" && evt.key === "F2" && !evt.shiftKey) {
                    _this.setCellSelection(_this.cells[0][0]);
                    _this.hasFocus = true;
                }
                return;
            }
            if (name === "mousedown") {
                _this.selectCell(evt);
            }
            if (name === "mouseup") {
                _this.onMouseUp(evt);
            }
            if (name === "keydown") {
                _this.onKeyDown(evt);
            }
            if (name === "keyup") {
                if (evt.keyCode === 17 || evt.keyCode === 91) {
                    _this.ctrlDown = false;
                }
                if (evt.key === "Shift") {
                    _this.shiftDown = false;
                }
            }
        };
        Grid_1.GridBase.Events.register(this.onEvents);
        this.ratio = window.devicePixelRatio;
        this.scale = this.ratio;
    }
    Object.defineProperty(Content.prototype, "editing", {
        get: function () {
            return this._editing;
        },
        set: function (value) {
            this._editing = value;
            Grid_1.GridBase.emit(Grid_1.GridBase.ON_EDITING, {
                cell: this.cellSelection.from,
                editing: this._editing,
                dirty: this.dirty
            });
            if (value) {
                if (!this.cellSelection.from ||
                    this.cellSelection.from.heading ||
                    this.cellSelection.from.allow !== true ||
                    !Grid_1.GridBase.Settings.canEdit) {
                    this._editing = false;
                    return;
                }
                this.cellSelection.go = false;
                Grid_1.GridBase.Container.showInput();
            }
            else {
                this.keyValue = undefined;
                Grid_1.GridBase.Container.hideInput();
                if (this.dirty) {
                    this.dirty = false;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Content.prototype.init = function () { };
    Content.prototype.set = function (data) {
        this.originalContent = data;
        var rows = this.cells[0] ? this.cells.length : 0;
        var cols = this.cells[0] ? this.cells[0].length : 0;
        this.content = this.clone(data);
        if (!rows || this.originalContent.length !== rows || this.originalContent[0].length !== cols) {
            this.setSelector();
        }
        else if (this.cellSelection.from) {
            this.cellSelection.from = this.cells[this.cellSelection.from.index.row][this.cellSelection.from.index.col];
            this.cellSelection.to = this.cells[this.cellSelection.to.index.row][this.cellSelection.to.index.col];
        }
        this.applyFilters();
        this.applySorting();
    };
    Content.prototype.update = function () {
        this.ratio = window.devicePixelRatio;
        this.resetStage();
        this.setStage();
        this.setCellAccess();
        this.setSize();
        this.setFit(Grid_1.GridBase.Settings.fit);
        this.setOffsets();
        this.keepSelectorWithinView();
    };
    Content.prototype.move = function () {
        this.setVisibility();
    };
    Content.prototype.destroy = function () { };
    Content.prototype.setOffsets = function (deltas, set) {
        var _this = this;
        if (!deltas) {
            ["x", "y"].forEach(function (axis) {
                var stage = ((_this[_this.stage[axis].label] * 1) / _this.scale) * _this.ratio;
                _this.offsets[axis].max = 0;
                _this.offsets[axis].maxOffset =
                    Grid_1.GridBase.Settings.fit === _this.stage[axis].label || Grid_1.GridBase.Settings.fit === "contain"
                        ? 0
                        : _this.stage[axis].size - stage;
                if (_this.offsets[axis].maxOffset < 0) {
                    _this.offsets[axis].maxOffset = 0;
                }
                _this.stage[axis].increments.forEach(function (offset, index) {
                    if (offset > _this.offsets[axis].maxOffset && !_this.offsets[axis].max) {
                        _this.offsets[axis].max = index;
                    }
                });
            });
        }
        if (deltas) {
            for (var axis in deltas) {
                if (parseFloat(deltas[axis]) === NaN)
                    continue;
                if (set) {
                    var index = deltas[axis];
                    var label = axis === "x" ? "col" : "row";
                    if (this.freezeRange.valid) {
                        index -= this.freezeRange.index[label] + 2;
                    }
                    this.offsets[axis].index = index;
                }
                else {
                    this.offsets[axis].index += deltas[axis];
                }
                if (this.offsets[axis].index < 0) {
                    this.offsets[axis].index = 0;
                }
                if (this.offsets[axis].index >= this.offsets[axis].max) {
                    this.offsets[axis].index = this.offsets[axis].max;
                }
            }
        }
        ["x", "y"].forEach(function (axis) {
            var offset = _this.stage[axis].increments[_this.offsets[axis].index];
            if (offset > _this.offsets[axis].maxOffset) {
                offset = _this.offsets[axis].maxOffset;
            }
            _this.offsets[axis].offset = offset;
        });
        this.setVisibility();
    };
    Content.prototype.setScale = function (n) {
        this.scale = n * this.ratio;
    };
    Content.prototype.setFit = function (n) {
        if (!/(scroll|width|height|contain)/.test(n)) {
            return;
        }
        this.updateFit(n, !Grid_1.GridBase.Settings.touch);
    };
    Content.prototype.cellHistory = function (data) {
        var _this = this;
        this.history = [];
        if (!data.length || !Grid_1.GridBase.Settings.tracking) {
            return;
        }
        var users = [];
        var count = 0;
        data.forEach2(function (push) {
            count++;
            if (count === 1) {
                return;
            }
            if (users.indexOf(push.modified_by.id) === -1) {
                users.push(push.modified_by.id);
            }
            var userIndex = users.indexOf(push.modified_by.id);
            push.content_diff.forEach2(function (row, rowIndex) {
                if (!row) {
                    return;
                }
                if (!_this.history[rowIndex]) {
                    _this.history[rowIndex] = [];
                }
                row.forEach2(function (cellData, cellIndex) {
                    if (!cellData) {
                        return;
                    }
                    _this.history[rowIndex][cellIndex] = userIndex;
                });
            });
        });
    };
    Content.prototype.cellHighlights = function (data) {
        this.highlights = [];
        if (!data.content_diff || !data.content_diff.length) {
            return;
        }
        for (var rowIndex = 0; rowIndex < data.content_diff.length; rowIndex++) {
            var row = data.content_diff[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                if (!row[colIndex])
                    continue;
                if (this.cells[rowIndex] && this.cells[rowIndex][colIndex] && !this.cells[rowIndex][colIndex].visible) {
                    continue;
                }
                if (!this.highlights[rowIndex]) {
                    this.highlights[rowIndex] = [];
                }
                this.highlights[rowIndex][colIndex] = true;
            }
        }
    };
    Content.prototype.clearFilters = function () {
        this.filters = {};
    };
    Content.prototype.setFilters = function (filters) {
        var _this = this;
        if (filters.col >= 0) {
            filters = [filters];
        }
        filters.forEach2(function (filter) {
            var key = "col-" + filter.col + "-" + filter.exp;
            if (!filter.value || filter.remove) {
                if (_this.filters["col" + filter.col]) {
                    delete _this.filters[key];
                }
                return;
            }
            if (!filter.type)
                filter.type = "number";
            filter.type = /(number|string|date)/.test(filter.type) ? filter.type : "number";
            if (!filter.exp ||
                (filter.type === "number" && [">", "<", "<=", ">=", "==", "==="].indexOf(filter.exp) === -1)) {
                filter.exp = "==";
            }
            _this.filters[key] = filter;
        });
    };
    Content.prototype.clearSorting = function () {
        this.sorting = {};
    };
    Content.prototype.setSorting = function (filters) {
        var _this = this;
        this.clearSorting();
        if (filters.col >= 0) {
            filters = [filters];
        }
        filters.forEach2(function (filter) {
            if (filter.remove) {
                if (_this.filters["col" + filter.col]) {
                    delete _this.filters["col" + filter.col];
                }
                return;
            }
            if (!filter.direction)
                filter.direction = "ASC";
            filter.direction = ("" + filter.direction).toUpperCase();
            filter.direction = /(ASC|DESC)/.test(filter.direction) ? filter.direction : "ASC";
            filter.col = filter.col * 1 || 0;
            filter.type = /(number|string|date)/.test(filter.type) ? filter.type : "number";
            _this.sorting["col" + filter.col] = filter;
        });
    };
    Content.prototype.setMergeRanges = function (ranges) {
        this.mergeRanges = ranges;
    };
    Content.prototype.setHiddenColumns = function (data) {
        this.hiddenColumns = data && data.length ? data : [];
    };
    Content.prototype.setFreeze = function (row, col) {
        if (row === void 0) { row = 0; }
        if (col === void 0) { col = 0; }
        this.freezeRange.valid = false;
        this.freezeRange.index.row = row;
        this.freezeRange.index.col = col;
        if (this.freezeRange.index.row > 0 || this.freezeRange.index.col > 0) {
            this.freezeRange.valid = true;
        }
        if (Grid_1.GridBase.Settings.headings) {
            this.freezeRange.valid = true;
            this.freezeRange.index.row++;
            this.freezeRange.index.col++;
        }
    };
    Content.prototype.setRanges = function (ranges) {
        this.resetRanges();
        for (var i = 0; i < ranges.length; i++) {
            var range = ranges[i];
            var rowStart = void 0;
            var colStart = void 0;
            var rowEnd = void 0;
            var colEnd = void 0;
            if (range.range) {
                rowStart = _.clone(range.range.from.row);
                colStart = _.clone(range.range.from.col);
                rowEnd = _.clone(range.range.to.row);
                colEnd = _.clone(range.range.to.col);
                if (rowEnd === -1) {
                    rowEnd = this.originalContent.length - 1;
                }
                if (colEnd === -1) {
                    colEnd = this.originalContent[0].length - 1;
                }
            }
            if (range.name === "frozen_rows") {
                this.freezeRange.index.row = range.count;
            }
            else if (range.name === "frozen_cols") {
                this.freezeRange.index.col = range.count;
            }
            else if (range instanceof Range_3.StyleRange) {
                for (var i_1 = rowStart; i_1 <= rowEnd; i_1++) {
                    for (var k = colStart; k <= colEnd; k++) {
                        if (!this.styleRanges[i_1]) {
                            this.styleRanges[i_1] = [];
                        }
                        if (!this.styleRanges[i_1][k]) {
                            this.styleRanges[i_1][k] = [];
                        }
                        this.styleRanges[i_1][k].push(range);
                    }
                }
            }
            else if (range instanceof Range_2.ButtonRange) {
                for (var i_2 = rowStart; i_2 <= rowEnd; i_2++) {
                    for (var k = colStart; k <= colEnd; k++) {
                        if (!this.buttonRanges[i_2]) {
                            this.buttonRanges[i_2] = [];
                        }
                        if (!this.buttonRanges[i_2][k]) {
                            this.buttonRanges[i_2][k] = [];
                        }
                        this.buttonRanges[i_2][k].push(range);
                    }
                }
            }
            else if (range instanceof Range_1.PermissionRange && this.originalContent.length) {
                var userRight = range.getPermission(Grid_1.GridBase.Settings.userId || 0) || (Grid_1.GridBase.Settings.userId ? "yes" : "no");
                var rowStart_1 = _.clone(range.rowStart);
                var colStart_1 = _.clone(range.colStart);
                var rowEnd_1 = _.clone(range.rowEnd);
                var colEnd_1 = _.clone(range.colEnd);
                if (rowEnd_1 === -1) {
                    rowEnd_1 = this.originalContent.length - 1;
                }
                if (colEnd_1 === -1) {
                    colEnd_1 = this.originalContent[0].length - 1;
                }
                for (var i_3 = rowStart_1; i_3 <= rowEnd_1; i_3++) {
                    for (var k = colStart_1; k <= colEnd_1; k++) {
                        if (!this.accessRanges[i_3]) {
                            this.accessRanges[i_3] = [];
                        }
                        if (!this.accessRanges[i_3][k]) {
                            this.accessRanges[i_3][k] = [];
                        }
                        if (userRight) {
                            this.accessRanges[i_3][k].push(userRight);
                        }
                    }
                }
            }
        }
        if (!this.freezeRange.valid) {
            if (this.freezeRange.index.row > 0 || this.freezeRange.index.col > 0) {
                this.freezeRange.valid = true;
            }
            if (Grid_1.GridBase.Settings.headings) {
                this.freezeRange.valid = true;
                this.freezeRange.index.row++;
                this.freezeRange.index.col++;
            }
        }
    };
    Content.prototype.find = function (data) {
        this.clearFound();
        var value = data.query;
        if (!value) {
            return;
        }
        this.foundSelection.query = value;
        for (var rowIndex = 0; rowIndex < this.cells.length; rowIndex++) {
            var row = this.cells[rowIndex];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                var regex = new RegExp(value, "ig");
                if (col.allow === "no" || (!regex.test(col.value) && !regex.test(col.formatted_value))) {
                    continue;
                }
                if (!this.found[rowIndex]) {
                    this.found[rowIndex] = [];
                }
                this.found[rowIndex][colIndex] = col;
                this.foundSelection.count++;
            }
        }
    };
    Content.prototype.clearFound = function () {
        for (var rowIndex = 0; rowIndex < this.found.length; rowIndex++) {
            var row = this.found[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                if (!col)
                    continue;
                Grid_1.GridBase.Container.removeLink("found", col);
            }
        }
        this.found = [];
        this.foundSelection.count = 0;
        this.foundSelection.selected = 0;
    };
    Content.prototype.getContentHtml = function () {
        var html = "<table style=\"border-collapse: collapse;\">";
        for (var rowIndex = 0; rowIndex < this.cells.length; rowIndex++) {
            var row = this.cells[rowIndex];
            if (this.cellSelection.from &&
                (rowIndex < this.cellSelection.from.index.row || rowIndex > this.cellSelection.to.index.row)) {
                continue;
            }
            html += "<tr>";
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                if (this.cellSelection.from && this.cellSelection.to &&
                    (colIndex < this.cellSelection.from.index.col || colIndex > this.cellSelection.to.index.col)) {
                    continue;
                }
                var cell = this.originalContent[rowIndex][colIndex];
                var styles = this.flattenStyles(cell.style);
                html += "<td style=\"" + styles + "\">" + (col.allow === "no" ? "" : cell.formatted_value) + "</td>";
            }
            html += "</tr>";
        }
        html += "</table>";
        return html;
    };
    Content.prototype.getFirstAvailableCell = function (columnIndex) {
        if (columnIndex === void 0) { columnIndex = -1; }
        var cell;
        var indexRow = 0;
        var indexCol = 0;
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                if (!col || (columnIndex > -1 && columnIndex !== col.index.col))
                    continue;
                cell = col;
                indexRow = rowIndex;
                indexCol = colIndex;
                break;
            }
            if (cell)
                break;
        }
        return {
            cell: cell,
            row: indexRow,
            col: indexCol,
        };
    };
    Content.prototype.getCells = function (fromCell, toCell) {
        var rowStart = fromCell.row;
        var rowEnd = toCell.row;
        if (rowEnd === -1) {
            rowEnd = this.originalContent.length - 1;
        }
        var colStart = fromCell.col;
        var colEnd = toCell.col;
        if (colEnd === -1) {
            colEnd = this.originalContent[0].length - 1;
        }
        var cellUpdates = [];
        for (var row = rowStart; row <= rowEnd; row++) {
            for (var col = colStart; col <= colEnd; col++) {
                if (!cellUpdates[row])
                    cellUpdates[row] = [];
                cellUpdates[row][col] = this.cells[row][col];
            }
        }
        return cellUpdates;
    };
    Content.prototype.setSelector = function (from, to) {
        if (!from) {
            this.clearCellSelection();
            return;
        }
        if (!to) {
            to = from;
        }
        if (!this.cells[from.row] || !this.cells[from.row][from.col]) {
            return;
        }
        this.cellSelection.from = this.cells[from.row][from.col];
        this.cellSelection.to = this.cells[to.row][to.col];
        this.keepSelectorWithinView();
    };
    Content.prototype.setColSize = function (headingCell, value) {
        headingCell.style.width = value + "px";
        for (var rowIndex = 0; rowIndex < this.cells.length; rowIndex++) {
            var cell = this.cells[rowIndex][headingCell.index.col];
            cell.style.width = value + "px";
        }
        this.update();
    };
    Content.prototype.setRowSize = function (headingCell, value) {
        headingCell.style.height = value + "px";
        for (var colIndex = 0; colIndex < this.cells[headingCell.index.row].length; colIndex++) {
            var cell = this.cells[headingCell.index.row][colIndex];
            cell.style.height = value + "px";
        }
        this.update();
    };
    Content.prototype.keepSelectorWithinView = function () {
        if (!this.cellSelection.from) {
            return;
        }
        var rect = Grid_1.GridBase.Container.elementRect;
        var stageWidth = rect.width - Grid_1.GridBase.Settings.scrollbarWidth;
        var stageHeight = rect.height - Grid_1.GridBase.Settings.scrollbarWidth;
        var offsets = {
            x: 0,
            y: 0
        };
        var set = false;
        var cell = this.cellSelection.to || this.cellSelection.from;
        if (cell.coords.x > stageWidth || cell.coords.a < 0) {
            offsets.x = cell.position.col;
            offsets.y = cell.position.row;
            set = true;
        }
        else if (cell.coords.x < this.freezeRange.width) {
            offsets.x = -1;
        }
        else if (cell.coords.a > stageWidth) {
            offsets.x = 1;
        }
        else if (cell.coords.y > stageHeight || cell.coords.b < 0) {
            offsets.x = cell.position.col;
            offsets.y = cell.position.row;
            set = true;
        }
        else if (cell.coords.y < this.freezeRange.height) {
            offsets.y = -1;
        }
        else if (cell.coords.b > stageHeight) {
            offsets.y = 1;
        }
        this.setOffsets(offsets, set);
    };
    Content.prototype.flattenStyles = function (styles) {
        var htmlStyle = {};
        this.cellStyles.forEach(function (s) {
            htmlStyle[s] = styles[s];
        });
        for (var key in this.borders.names) {
            var name_1 = this.borders.names[key];
            var width = this.borders.widths[styles[key + "bw"]] + "px";
            var style = styles[key + "bs"];
            var color = "#" + styles[key + "bc"].replace("#", "");
            htmlStyle["border-" + name_1] = width + " " + style + " " + color;
        }
        var str = "";
        for (var attr in htmlStyle) {
            var value = htmlStyle[attr];
            if ((attr === "background-color" || attr === "color") && value.indexOf("#") === -1) {
                value = "#" + value;
            }
            str += attr + ": " + value + "; ";
        }
        return str;
    };
    Content.prototype.resetStage = function () {
        this.stage = {
            x: {
                label: "width",
                pos: 0,
                increments: [],
                size: 0,
                stop: false
            },
            y: {
                label: "height",
                pos: 0,
                increments: [],
                size: 0,
                stop: false
            }
        };
    };
    Content.prototype.resetRanges = function () {
        this.accessRanges = [];
        this.buttonRanges = [];
        this.styleRanges = [];
    };
    Content.prototype.setCellAccess = function () {
        var _this = this;
        if (!this.content.length) {
            return;
        }
        for (var rowIndex = 0; rowIndex < this.cells.length; rowIndex++) {
            var row = this.cells[rowIndex];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var cell = this.cells[rowIndex][colIndex];
                cell.allow = true;
                cell.button = false;
            }
        }
        this.accessRanges.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col || col.indexOf("yes") !== -1)
                    return;
                col.forEach2(function (element) {
                    if (["ro", "no"].indexOf(element) === -1) {
                        return;
                    }
                    if (_this.cells[rowIndex] && _this.cells[rowIndex][colIndex]) {
                        _this.cells[rowIndex][colIndex].allow = element;
                    }
                });
            });
        });
        this.buttons.forEach(function (button) {
            var _b = button.range.split(":").map(function (str) { return Grid_1.GridBase.helpers.getRefIndex(str); }), from = _b[0], to = _b[1];
            var endRow = to[0] < 0 ? _this.cells.length : to[0];
            for (var colIndex = from[1]; colIndex <= to[1]; colIndex++) {
                for (var rowIndex = 0; rowIndex < _this.cells.length; rowIndex++) {
                    if (rowIndex < from[0] || rowIndex > endRow)
                        continue;
                    var cell = _this.cells[rowIndex][colIndex];
                    if (!cell)
                        continue;
                    Grid_1.GridBase.Container.removeLink("button", cell);
                    if (cell.allow === true && !cell.hidden) {
                        cell.button = button;
                        Grid_1.GridBase.Container.addLink("button", {
                            cell: cell,
                            className: "button-link"
                        });
                    }
                }
            }
        });
    };
    Content.prototype.setStage = function () {
        this.stage.x.increments.push(0);
        this.stage.y.increments.push(0);
        var freezeOffset = {
            x: 0,
            y: 0
        };
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            if (!row)
                continue;
            var col = void 0;
            this.stage.x.pos = 0;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                col = row[colIndex];
                if (!col)
                    continue;
                var wrap = false;
                if ((col.style["text-wrap"] && col.style["text-wrap"] === "wrap") ||
                    col.style["word-wrap"] === "break-word") {
                    wrap = true;
                }
                col.wrap = wrap;
                col.x = this.stage.x.pos;
                col.y = this.stage.y.pos;
                col.width = parseFloat(col.style.width);
                col.height = parseFloat(col.style.height);
                col.allow = true;
                this.stage.x.pos += col.width;
                if (colIndex === row.length - 1 && this.borders.widths[col.style.rbw]) {
                    this.stage.x.pos += this.borders.widths[col.style.rbw];
                }
                if (!rowIndex) {
                    if (!this.freezeRange.valid || (this.freezeRange.valid && colIndex >= this.freezeRange.index.col)) {
                        this.stage.x.increments.push(this.stage.x.pos - freezeOffset.x);
                    }
                    else {
                        freezeOffset.x += col.width;
                    }
                }
            }
            this.softMerges(this.content[rowIndex], rowIndex);
            if (this.stage.x.pos > this.stage.x.size) {
                this.stage.x.size = this.stage.x.pos;
            }
            this.stage.y.pos += col.height;
            this.stage.y.size += col.height;
            if (rowIndex === this.content.length - 1 && this.borders.widths[col.style.bbw]) {
                this.stage.y.pos += this.borders.widths[col.style.bbw];
                this.stage.y.size += this.borders.widths[col.style.bbw];
            }
            if (!this.freezeRange.valid || (this.freezeRange.valid && rowIndex >= this.freezeRange.index.row)) {
                this.stage.y.increments.push(this.stage.y.pos - freezeOffset.y);
            }
            else {
                freezeOffset.y += col.height;
            }
        }
        for (var i = 0; i < this.mergeRanges.length; i++) {
            var r = this.mergeRanges[i];
            var rowFrom = r.from.row;
            var rowTo = r.to.row;
            var colFrom = r.from.col;
            var colTo = r.to.col;
            if (Grid_1.GridBase.Settings.headings) {
                rowFrom++;
                rowTo++;
                colFrom++;
                colTo++;
            }
            for (var row = rowFrom; row <= rowTo; row++) {
                if (row > rowFrom) {
                    this.content[rowFrom][colFrom].height += this.content[row][colFrom].height;
                }
                for (var col = colFrom; col <= colTo; col++) {
                    if (row === rowFrom && col > colFrom) {
                        this.content[rowFrom][colFrom].width += this.content[rowFrom][col].width;
                    }
                    if (row === rowFrom && col === colFrom)
                        continue;
                    this.content[row][col].hidden = true;
                }
            }
            this.content[rowFrom][colFrom].overlayWidth = this.content[rowFrom][colFrom].width;
            this.content[rowFrom][colFrom].overlayEnd = colTo;
        }
        this.freezeRange.width = freezeOffset.x;
        this.freezeRange.height = freezeOffset.y;
    };
    Content.prototype.setSize = function () {
        var rect = Grid_1.GridBase.Container.element.getBoundingClientRect();
        if (!rect.width) {
            rect.width = this.stage.x.size;
            rect.height = this.stage.y.size;
        }
        var stageWidth = (this.stage.x.size * this.scale) / this.ratio;
        var stageHeight = (this.stage.y.size * this.scale) / this.ratio;
        var scrollbarWidth = stageHeight > rect.height && !Grid_1.GridBase.Settings.touch ? Grid_1.GridBase.Settings.scrollbarWidth : 0;
        var scrollbarHeight = stageWidth > rect.width && !Grid_1.GridBase.Settings.touch ? Grid_1.GridBase.Settings.scrollbarWidth : 0;
        if (rect.width > stageWidth || Grid_1.GridBase.Settings.fluid || Grid_1.GridBase.Settings.scrollbarInset) {
            scrollbarHeight = 0;
        }
        if (rect.height > stageHeight || Grid_1.GridBase.Settings.fluid || Grid_1.GridBase.Settings.scrollbarInset) {
            scrollbarWidth = 0;
        }
        this.width = Grid_1.GridBase.Settings.fluid ? this.stage.x.size : rect.width - scrollbarWidth;
        this.height = Grid_1.GridBase.Settings.fluid ? this.stage.y.size : rect.height - scrollbarHeight;
    };
    Content.prototype.setVisibility = function () {
        this.visibleCells = [];
        this.frozenCells = {
            cols: [],
            rows: [],
            common: []
        };
        this.trackedCells = [];
        var stageWidth = ((this.width * 1) / this.scale) * this.ratio;
        var stageHeight = ((this.height * 1) / this.scale) * this.ratio;
        console.log(this.height, stageHeight);
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            if (!row)
                continue;
            var y = row[0].y - this.offsets.y.offset;
            var b = row[0].y - this.offsets.y.offset + row[0].height;
            var visibleRow = (y >= 0 && y <= stageHeight) || (b >= 0 && b <= stageHeight);
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                if (!col)
                    continue;
                var width = col.overlayWidth;
                var height = col.height;
                var xPos = col.overlayStart < colIndex ? this.content[rowIndex][col.overlayStart].x : col.x;
                var x = xPos - this.offsets.x.offset;
                var _x = col.x - this.offsets.x.offset;
                var y_1 = col.y - this.offsets.y.offset;
                var a = col.x - this.offsets.x.offset + width;
                var _a = col.x - this.offsets.x.offset + col.width;
                var b_1 = col.y - this.offsets.y.offset + height;
                var visibleCol = (x >= 0 && x <= stageWidth) ||
                    (a >= 0 && a <= stageWidth);
                if (!col.width) {
                    continue;
                }
                col.coords = { x: x, y: y_1, a: a, b: b_1, width: width, height: height, _x: _x, _a: _a };
                if (col.heading && !rowIndex && colIndex > 0) {
                    Grid_1.GridBase.Container.addGridLine("col", col);
                }
                if (col.heading && rowIndex > 0 && !colIndex) {
                    Grid_1.GridBase.Container.addGridLine("row", col);
                }
                if ((visibleRow && visibleCol) ||
                    (this.freezeRange.valid && colIndex < this.freezeRange.index.col) ||
                    (this.freezeRange.valid && rowIndex < this.freezeRange.index.row)) {
                    col.visible = true;
                    this.visibleCells.push(col);
                    if (this.freezeRange.valid) {
                        var freeze = "";
                        if (rowIndex < this.freezeRange.index.row && colIndex < this.freezeRange.index.col) {
                            x = _x = col.x;
                            a = _a = col.x + width;
                            y_1 = col.y;
                            b_1 = col.y + height;
                            freeze = "common";
                        }
                        else if (rowIndex < this.freezeRange.index.row) {
                            y_1 = col.y;
                            b_1 = col.y + height;
                            freeze = visibleCol ? "rows" : "";
                        }
                        else if (colIndex < this.freezeRange.index.col) {
                            x = _x = col.x;
                            a = _a = col.x + width;
                            freeze = visibleRow ? "cols" : "";
                        }
                        if (freeze) {
                            col.coords = { x: x, y: y_1, a: a, b: b_1, width: width, height: height, _x: _x, _a: _a };
                            this.frozenCells[freeze].push(col);
                        }
                        var visibleRow2 = (y_1 >= this.freezeRange.height && y_1 <= stageHeight);
                        var visibleCol2 = (x >= this.freezeRange.width && x <= stageWidth);
                        col.visible = freeze === "common" ? true : (freeze ? visibleRow2 || visibleCol2 : visibleRow2 && visibleCol2);
                    }
                    if (!col.heading && this.found[col.index.row] && this.found[col.index.row][col.index.col]) {
                        Grid_1.GridBase.Container.addLink("found", {
                            cell: col
                        });
                    }
                    if (!col.heading &&
                        this.highlights[col.index.row] &&
                        this.highlights[col.index.row][col.index.col]) {
                        Grid_1.GridBase.Container.addLink("highlight", {
                            cell: col,
                            color: Grid_1.GridBase.helpers.getContrastYIQ(col.style["background-color"]) === 'light' ? '000000' : 'FFFFFF'
                        });
                        this.highlights[col.index.row][col.index.col] = false;
                    }
                    if (!col.heading &&
                        this.history[col.index.row] &&
                        this.history[col.index.row][col.index.col] >= 0) {
                        Grid_1.GridBase.Container.addLink("history", {
                            cell: col,
                            user: this.history[col.index.row][col.index.col]
                        });
                    }
                    if (!col.heading && col.link) {
                        Grid_1.GridBase.Container.addLink("external", {
                            address: col.link.address,
                            external: col.link.external,
                            cell: col
                        });
                    }
                    if (("" + col.value).indexOf("data:image") === 0) {
                        col.image = true;
                        Grid_1.GridBase.Container.addImage(col);
                    }
                }
                else {
                    col.visible = false;
                }
            }
        }
    };
    Content.prototype.softMerges = function (dataRow, dataRowIndex) {
        var _this = this;
        if (dataRow === void 0) { dataRow = []; }
        if (dataRowIndex === void 0) { dataRowIndex = -1; }
        dataRow.forEach2(function (col, colIndex) {
            var width = parseFloat(col.style.width);
            col.overlayWidth = width;
            col.overlayEnd = colIndex;
            col.overlayStart = colIndex;
            if ((col.formatted_value || col.value) && !col.wrap) {
                Grid_1.GridBase.Canvas.bufferContext.font = _this.getFontString(col);
                var textWidth = Grid_1.GridBase.Canvas.bufferContext.measureText(_this.cleanValue(col.formatted_value || col.value)).width +
                    3;
                if (textWidth > width) {
                    var overlay = _this.getTextOverlayWidth(dataRowIndex, colIndex, width, textWidth);
                    col.overlayWidth = overlay.width;
                    col.overlayEnd = overlay.endCol;
                    col.overlayStart = overlay.startCol;
                }
            }
        });
    };
    Content.prototype.getFontString = function (col) {
        return col.style["font-style"] + " " + col.style["font-weight"] + " " + col.style["font-size"] + " \"" + col.style["font-family"] + "\", sans-serif";
    };
    Content.prototype.cleanValue = function (value) {
        return (value + "").trim().replace(/\s\s+/g, " ");
    };
    Content.prototype.getTextOverlayWidth = function (row, col, width, textWidth) {
        var cell = this.content[row][col];
        var textAlign = cell.style["text-align"];
        var left = 0;
        var right = 0;
        switch (textAlign) {
            case "right":
                left = -1;
                break;
            case "center":
                left = -1;
                right = 1;
                break;
            default:
                right = 1;
                break;
        }
        if (!this.content[row][col + right] || !this.content[row][col + left]) {
            return {
                col: col,
                width: width
            };
        }
        var endCol = col;
        var startCol = col;
        if (right > 0) {
            for (var i = col + 1; i < this.content[row].length; i++) {
                if (this.content[row][i].formatted_value ||
                    "" + this.content[row][i].value ||
                    this.content[row][i].allow === "no") {
                    break;
                }
                width += this.content[row][i].width;
                endCol = i;
                this.content[row][i].hidden = true;
                if (width > textWidth) {
                    break;
                }
            }
        }
        if (left < 0) {
            for (var i = col - 1; i >= 0; i--) {
                if (this.content[row][i].formatted_value ||
                    "" + this.content[row][i].value ||
                    this.content[row][i].allow === "no") {
                    break;
                }
                width += this.content[row][i].width;
                startCol = i;
                this.content[row][i].hidden = true;
                if (width > textWidth) {
                    break;
                }
            }
        }
        return {
            left: left,
            right: right,
            endCol: endCol,
            startCol: startCol,
            width: width
        };
    };
    Content.prototype.getCell = function (x, y) {
        var rect = Grid_1.GridBase.Container.elementRect;
        var offsetX = x - rect.left;
        var offsetY = y - rect.top;
        x -= rect.left - (this.offsets.x.offset * this.scale) / this.ratio;
        y -= rect.top - (this.offsets.y.offset * this.scale) / this.ratio;
        var cell;
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                if (!col)
                    continue;
                if (rowIndex < this.freezeRange.index.row && colIndex < this.freezeRange.index.col) {
                    if (offsetY >= (col.y * this.scale) / this.ratio &&
                        offsetY <= ((col.y + col.height) * this.scale) / this.ratio &&
                        offsetX >= (col.x * this.scale) / this.ratio &&
                        offsetX <= ((col.x + col.width) * this.scale) / this.ratio) {
                        cell = col;
                        break;
                    }
                }
                else if (rowIndex < this.freezeRange.index.row) {
                    if (offsetY >= (col.y * this.scale) / this.ratio &&
                        offsetY <= ((col.y + col.height) * this.scale) / this.ratio &&
                        x >= (col.x * this.scale) / this.ratio &&
                        x <= ((col.x + col.width) * this.scale) / this.ratio) {
                        cell = col;
                        break;
                    }
                }
                else if (colIndex < this.freezeRange.index.col) {
                    if (y >= (col.y * this.scale) / this.ratio &&
                        y <= ((col.y + col.height) * this.scale) / this.ratio &&
                        offsetX >= (col.x * this.scale) / this.ratio &&
                        offsetX <= ((col.x + col.width) * this.scale) / this.ratio) {
                        cell = col;
                        break;
                    }
                }
                if (cell)
                    break;
                if (x < (col.x * this.scale) / this.ratio)
                    continue;
                if (x > ((col.x + col.width) * this.scale) / this.ratio)
                    continue;
                if (y < (col.y * this.scale) / this.ratio)
                    continue;
                if (y > ((col.y + col.height) * this.scale) / this.ratio)
                    continue;
                cell = col;
                break;
            }
            if (cell)
                break;
        }
        return cell;
    };
    Content.prototype.onKeyDown = function (evt) {
        if (evt.keyCode === 17 || evt.keyCode === 91) {
            this.ctrlDown = true;
            return;
        }
        if (evt.key === "Shift") {
            this.shiftDown = true;
        }
        var moved = this.onCtrlNavigate(evt.key);
        if (moved)
            return;
        var x = 0;
        var y = 0;
        var set = false;
        switch (evt.key) {
            case 'ArrowLeft':
                x = -1;
                break;
            case 'ArrowUp':
                y = -1;
                break;
            case 'ArrowRight':
                x = 1;
                break;
            case 'ArrowDown':
                y = 1;
                break;
            case 'PageUp':
            case 'PageDown':
                set = true;
                var row = -1;
                var rows = 0;
                var dir = evt.key === 'PageDown' ? 1 : -1;
                var min = this.freezeRange.index.row;
                var yOffset = Grid_1.GridBase.Settings.headings ? 2 : 0;
                var yOffsetSelection = Grid_1.GridBase.Settings.headings ? -2 : 0;
                for (var i = 0; i < this.visibleCells.length; i++) {
                    if (this.visibleCells[i].heading || !this.visibleCells[i].visible || this.visibleCells[i].index.row === row) {
                        continue;
                    }
                    rows++;
                    row = this.visibleCells[i].index.row;
                }
                y = this.offsets.y.index + rows * dir + yOffset;
                var contentY = y + yOffsetSelection;
                if (y > this.offsets.y.max) {
                    y = this.content.length - 1;
                    contentY = this.content.length - 1;
                }
                else if (y < min) {
                    y = min;
                    contentY = min;
                }
                this.setOffsets({ y: y }, true);
                var cell = this.shiftDown ? this.cellSelection.to : this.cellSelection.from;
                if (cell && !Grid_1.GridBase.Settings.disallowSelection) {
                    this.setCellSelection(this.content[contentY][cell.position.col]);
                }
                return;
                break;
        }
        if ((x !== 0 || y !== 0 || set) && !this.editing) {
            var cell = this.shiftDown ? this.cellSelection.to : this.cellSelection.from;
            var row = void 0;
            var col = void 0;
            if (this.cellSelection.from && !Grid_1.GridBase.Settings.disallowSelection) {
                row = cell.position.row + y;
                var startRow = Grid_1.GridBase.Settings.headings ? 1 : 0;
                var startCol = Grid_1.GridBase.Settings.headings ? 1 : 0;
                col = cell.position.col + x;
                if (row >= this.content.length || row < startRow) {
                    return;
                }
                if (col >= this.content[0].length || col < startCol) {
                    return;
                }
                if (set) {
                    this.setCellSelection(this.content[y][col]);
                }
                else {
                    this.setCellSelection(this.content[row][col]);
                }
                evt.preventDefault();
            }
            else {
                this.setOffsets({
                    x: x,
                    y: y
                }, set);
                if ((y > 0 && this.offsets.y.index !== this.offsets.y.max) || (y < 0 && this.offsets.y.index)) {
                    evt.preventDefault();
                }
            }
            return;
        }
        if (evt.keyCode === 13) {
            if (!Grid_1.GridBase.Settings.alwaysEditing && !Grid_1.GridBase.Settings.touch) {
                this.editing = false;
            }
            this.selectNextCell(this.cellSelection.from || this.cellSelection.last || this.cells[0][0], this.shiftDown ? "up" : "down");
            this.keepSelectorWithinView();
            return;
        }
        if (evt.keyCode === 9 && this.cellSelection.from) {
            if (!Grid_1.GridBase.Settings.alwaysEditing && !Grid_1.GridBase.Settings.touch) {
                this.editing = false;
            }
            this.selectNextCell(this.cellSelection.from, this.shiftDown ? "left" : "");
            evt.preventDefault();
            this.keepSelectorWithinView();
            return;
        }
        if (!this.editing && !Grid_1.GridBase.Settings.disallowSelection && Grid_1.GridBase.Settings.canEdit) {
            if (evt.key === "F2") {
                if (!evt.shiftKey) {
                    this.setCellSelection(this.cellSelection.from, 0, true);
                    return;
                }
            }
            if (this.cellSelection.from && !this.ctrlDown) {
                if (evt.key.length === 1 && /[a-zA-Z0-9-_ ]/.test(evt.key)) {
                    this.keyValue = evt.key;
                }
                if (evt.key === "Delete") {
                    this.keyValue = "";
                }
                if (this.keyValue !== undefined) {
                    this.cellSelection.on = true;
                    if (!this.editing) {
                        this.setCellSelection(this.cellSelection.from, 0, true);
                    }
                    this.ctrlDown = false;
                }
            }
        }
        else if (this.editing) {
            this.dirty = true;
        }
    };
    Content.prototype.onCtrlNavigate = function (which) {
        if (['Home', 'End', 'ArrowLeft', 'ArrowRight', 'ArrowDown', 'ArrowUp'].indexOf(which) < 0
            || !this.ctrlDown
            || !this.cellSelection.from
            || Grid_1.GridBase.Settings.disallowSelection) {
            return false;
        }
        if (which === 'Home') {
            this.setOffsets({ x: 0, y: 0 }, true);
            this.setCellSelection(this.cells[0][0]);
            return true;
        }
        if (which === 'End') {
            this.setOffsets({ x: this.offsets.x.max, y: this.offsets.y.max }, true);
            this.setCellSelection(this.cells[this.cells.length - 1][this.cells[0].length - 1]);
            return true;
        }
        var x = 0;
        var y = 0;
        var lookForEmptyValue = true;
        var refCell = this.shiftDown ? this.cellSelection.to : this.cellSelection.from;
        if (which === 'ArrowRight') {
            if (refCell.index.col + 1 >= this.cells[0].length - 1) {
                this.setCellSelection(this.cells[refCell.index.row][this.cells[0].length - 1]);
                return true;
            }
            for (var colIndex = refCell.index.col + 1; colIndex < this.cells[0].length; colIndex++) {
                var cell = this.cells[refCell.index.row][colIndex];
                if (colIndex === refCell.index.col + 1 && !cell.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell.value && lookForEmptyValue) {
                    x = colIndex - 1;
                    break;
                }
                if (cell.value && !lookForEmptyValue) {
                    x = colIndex;
                    break;
                }
            }
            if (!x) {
                x = this.cells[0].length - 1;
            }
            this.setCellSelection(this.cells[refCell.index.row][x]);
        }
        else if (which === 'ArrowLeft') {
            if (refCell.index.col - 1 <= 0) {
                this.setCellSelection(this.cells[refCell.index.row][0]);
                return true;
            }
            for (var colIndex = refCell.index.col - 1; colIndex >= 0; colIndex--) {
                var cell = this.cells[refCell.index.row][colIndex];
                if (colIndex === refCell.index.col - 1 && !cell.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell.value && lookForEmptyValue) {
                    x = colIndex + 1;
                    break;
                }
                if (cell.value && !lookForEmptyValue) {
                    x = colIndex;
                    break;
                }
            }
            this.setCellSelection(this.cells[refCell.index.row][x]);
        }
        else if (which === 'ArrowDown') {
            if (refCell.index.row + 1 >= this.cells.length - 1) {
                this.setCellSelection(this.cells[this.cells.length - 1][refCell.index.col]);
                return true;
            }
            for (var rowIndex = refCell.index.row + 1; rowIndex < this.cells.length; rowIndex++) {
                var cell = this.cells[rowIndex][refCell.index.col];
                if (rowIndex === refCell.index.row + 1 && !cell.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell.value && lookForEmptyValue) {
                    y = rowIndex - 1;
                    break;
                }
                if (cell.value && !lookForEmptyValue) {
                    y = rowIndex;
                    break;
                }
            }
            if (!y) {
                y = this.cells.length - 1;
            }
            this.setCellSelection(this.cells[y][refCell.index.col]);
        }
        else if (which === 'ArrowUp') {
            if (refCell.index.row - 1 <= 0) {
                this.setCellSelection(this.cells[0][refCell.index.col]);
                return true;
            }
            for (var rowIndex = refCell.index.row - 1; rowIndex >= 0; rowIndex--) {
                var cell = this.cells[rowIndex][refCell.index.col];
                if (rowIndex === refCell.index.row - 1 && !cell.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell.value && lookForEmptyValue) {
                    y = rowIndex + 1;
                    break;
                }
                if (cell.value && !lookForEmptyValue) {
                    y = rowIndex;
                    break;
                }
            }
            this.setCellSelection(this.cells[y][refCell.index.col]);
        }
        return true;
    };
    Content.prototype.onMouseUp = function (evt) {
        this.cellSelection.go = false;
        if (!this.cellSelection.from) {
            return;
        }
        if (this.cellSelection.to && (this.cellSelection.from.position.col > this.cellSelection.to.position.col || this.cellSelection.from.position.row > this.cellSelection.to.position.row)) {
            var from = JSON.parse(JSON.stringify(this.cellSelection.to.position));
            var to = JSON.parse(JSON.stringify(this.cellSelection.from.position));
            for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
                var row = this.content[rowIndex];
                if (!row)
                    continue;
                for (var colIndex = 0; colIndex < row.length; colIndex++) {
                    var col = row[colIndex];
                    if (!col)
                        continue;
                    if (from.row === col.position.row && from.col === col.position.col) {
                        this.cellSelection.from = this.content[rowIndex][colIndex];
                    }
                    if (to.row === col.position.row && to.col === col.position.col) {
                        this.cellSelection.to = this.content[rowIndex][colIndex];
                    }
                }
            }
        }
        if (this.headingSelected) {
            this.selectHeadingCell(evt);
            return;
        }
        this.cellSelection.from = this.scaleCellCoords(this.cellSelection.from);
        this.cellSelection.to = this.scaleCellCoords(this.cellSelection.to);
        if (this.cellSelection.from === this.cellSelection.to) {
            this.cellSelection.multiple = true;
        }
        Grid_1.GridBase.emit(Grid_1.GridBase.ON_CELL_SELECTED, {
            rightClick: Grid_1.GridBase.Events.rightClick,
            selection: this.cellSelection,
            event: evt,
        });
    };
    Content.prototype.scaleCellCoords = function (cell) {
        cell.scaledCoords = {
            x: (cell.coords.x * this.scale) / this.ratio,
            y: (cell.coords.y * this.scale) / this.ratio,
            width: (cell.coords.width * this.scale) / this.ratio,
            height: (cell.coords.height * this.scale) / this.ratio,
        };
        return cell;
    };
    Content.prototype.selectHeadingCell = function (evt) {
        var maxRow = 0;
        var maxCol = 0;
        var indexFromRow = 0;
        var indexToRow = 0;
        var indexFromCol = 0;
        var indexToCol = 0;
        for (var rowIndex = this.content.length; rowIndex >= 0; rowIndex--) {
            var row = this.content[rowIndex];
            if (!row)
                continue;
            if (!maxRow) {
                maxRow = rowIndex;
            }
            var col = this.content[rowIndex][0];
            if (this.cellSelection.from.position.row === col.position.row) {
                indexFromRow = rowIndex;
            }
            if (this.cellSelection.to.position.row === col.position.row) {
                indexToRow = rowIndex;
            }
        }
        for (var colIndex = this.content[0].length; colIndex >= 0; colIndex--) {
            var col = this.content[0][colIndex];
            if (!col)
                continue;
            if (!maxCol) {
                maxCol = colIndex;
            }
            if (this.cellSelection.from.position.col === col.position.col) {
                indexFromCol = colIndex;
            }
            if (this.cellSelection.to.position.col === col.position.col) {
                indexToCol = colIndex;
            }
        }
        if (this.headingSelected === "all") {
            this.cellSelection.from = this.content[0][0];
            this.cellSelection.to = this.content[maxRow][maxCol];
        }
        else if (this.headingSelected === "col") {
            this.cellSelection.from = this.content[0][indexFromCol];
            this.cellSelection.to = this.content[maxRow][indexToCol];
        }
        else {
            this.cellSelection.from = this.content[indexFromRow][0];
            this.cellSelection.to = this.content[indexToRow][maxCol];
        }
        this.cellSelection.from = this.scaleCellCoords(this.cellSelection.from);
        this.cellSelection.to = this.scaleCellCoords(this.cellSelection.to);
        var n = {
            rightClick: Grid_1.GridBase.Events.rightClick,
            which: this.headingSelected,
            selection: this.cellSelection,
            event: evt,
            count: this.headingSelected === "all"
                ? -1
                : this.cellSelection.to.position[this.headingSelected] -
                    this.cellSelection.from.position[this.headingSelected] +
                    1
        };
        Grid_1.GridBase.emit(Grid_1.GridBase.ON_HEADING_CLICKED, n);
        this.headingSelected = "";
    };
    Content.prototype.selectCell = function (evt) {
        if (evt.target === Grid_1.GridBase.Container.input || evt.target === Grid_1.GridBase.Container.select) {
            return;
        }
        if (evt.touches) {
            if (evt.touches.length > 1) {
                return;
            }
            evt.clientX = evt.touches[0].pageX;
            evt.clientY = evt.touches[0].pageY;
        }
        var cell = this.getCell(evt.clientX, evt.clientY);
        if (!cell) {
            this.clearCellSelection(true);
            Grid_1.GridBase.emit(Grid_1.GridBase.ON_NO_CELL);
            return;
        }
        if (Grid_1.GridBase.Events.rightClick) {
            Grid_1.GridBase.Events.preventContextMenu = true;
        }
        if (this.editing) {
            this.editing = false;
        }
        if (cell.heading) {
            if (!cell.position.row && !cell.position.col) {
                this.headingSelected = "all";
            }
            else if (!cell.position.row) {
                this.headingSelected = "col";
            }
            else if (!cell.position.col) {
                this.headingSelected = "row";
            }
            if (Grid_1.GridBase.Events.rightClick) {
                if (this.cellSelection.from) {
                    if (this.headingSelected === "col" && cell.position.col >= this.cellSelection.from.position.col && cell.position.col <= this.cellSelection.to.position.col) {
                        return;
                    }
                    if (this.headingSelected === "row" && cell.position.row >= this.cellSelection.from.position.row && cell.position.row <= this.cellSelection.to.position.row) {
                        return;
                    }
                }
            }
        }
        var d = new Date();
        if (!Grid_1.GridBase.Events.rightClick && this.cellSelection.clicked && d.getTime() - this.cellSelection.clicked <= 300 && !cell.button) {
            Grid_1.GridBase.emit(Grid_1.GridBase.ON_CELL_DOUBLE_CLICKED, { cell: cell, event: evt });
            this.cellSelection.clicked = d.getTime();
            this.editing = true;
            if (Grid_1.GridBase.Settings.touch) {
                evt.stopPropagation();
                evt.preventDefault();
            }
            return;
        }
        if (!this.cellSelection.from
            || (cell.index.row < this.cellSelection.from.index.row || cell.index.col < this.cellSelection.from.index.col)
            || (cell.index.row > this.cellSelection.to.index.row || cell.index.col > this.cellSelection.to.index.col)) {
            this.setCellSelection(cell, d.getTime());
        }
    };
    Content.prototype.selectNextCell = function (cell, direction) {
        if (direction === void 0) { direction = ""; }
        var row = cell.index.row;
        var col = cell.index.col;
        if (direction === "left") {
            if (this.cells[row][col - 1]) {
                this.setCellSelection(this.cells[row][col - 1]);
            }
            else if (this.cells[row - 1]) {
                this.setCellSelection(this.cells[row - 1][this.cells[row].length - 1]);
            }
            return;
        }
        if (direction === "up") {
            if (this.cells[row - 1] && this.cells[row - 1][col]) {
                this.setCellSelection(this.cells[row - 1][col]);
                return;
            }
            else if (this.cells[this.cells.length - 1] && this.cells[this.cells.length - 1][col - 1]) {
                this.setCellSelection(this.cells[this.cells.length - 1][col - 1]);
                return;
            }
        }
        if (direction === "down") {
            if (this.cells[row + 1] && this.cells[row + 1][col]) {
                this.setCellSelection(this.cells[row + 1][col]);
                return;
            }
            else if (this.cells[0][col + 1]) {
                this.setCellSelection(this.cells[0][col + 1]);
                return;
            }
        }
        if (this.cells[row][col + 1]) {
            this.setCellSelection(this.cells[row][col + 1]);
            return;
        }
        if (this.cells[row + 1]) {
            this.setCellSelection(this.cells[row + 1][0]);
            return;
        }
        this.setCellSelection(this.cells[0][0]);
        return;
    };
    Content.prototype.setCellSelection = function (cell, time, editing) {
        if (editing === void 0) { editing = false; }
        if (editing) {
            if (cell.allow !== true) {
                this.editing = false;
                return;
            }
            else {
            }
            if (this.keyValue !== undefined) {
                cell.formatted_value = cell.value = _.clone(this.keyValue);
                cell.dirty = true;
            }
        }
        this.cellSelection.clicked = time;
        if (!this.shiftDown) {
            this.cellSelection.from = cell;
        }
        this.cellSelection.to = cell;
        this.cellSelection.on = false;
        if (editing || (time && cell.button && ["select"].indexOf(cell.button.type) > -1 && !Grid_1.GridBase.Events.rightClick)) {
            this.cellSelection.go = false;
            this.editing = true;
        }
        else if (time) {
            this.cellSelection.go = true;
        }
        if (!Grid_1.GridBase.Events.rightClick) {
            this.cellSelection.multiple = false;
        }
        this.keepSelectorWithinView();
        if (time && cell.button && cell.button.type === "button" && !Grid_1.GridBase.Events.rightClick) {
            var value = Grid_1.GridBase.helpers.getNextValueInArray(cell.formatted_value, cell.button.options);
            Grid_1.GridBase.emit(Grid_1.GridBase.ON_CELL_VALUE, {
                cell: Grid_1.GridBase.Content.cellSelection.from,
                value: value
            });
            Grid_1.GridBase.emit(Grid_1.GridBase.ON_EDITING, {
                cell: this.cellSelection.from,
                editing: this._editing,
                dirty: this.dirty
            });
        }
    };
    Content.prototype.clearCellSelection = function (hasFocus) {
        if (hasFocus === void 0) { hasFocus = false; }
        if (this.editing) {
            this.editing = false;
        }
        this.cellSelection.last = this.cellSelection.from;
        this.cellSelection.from = undefined;
        this.cellSelection.to = undefined;
        this.cellSelection.go = false;
        this.hasFocus = hasFocus;
    };
    Content.prototype.updateCellSelection = function (evt) {
        var cell = this.getCell(evt.clientX, evt.clientY);
        if (!cell) {
            this.noAccess = false;
            return;
        }
        if (this.cellSelection.go) {
            this.cellSelection.to = cell;
        }
        this.noAccess = cell.allow !== true;
    };
    Content.prototype.updateFit = function (n, retain) {
        Grid_1.GridBase.Settings.fit = n;
        switch (n) {
            case "scroll":
                if (!Grid_1.GridBase.Settings.touch) {
                    this.setScale(retain ? this.ratio : 1 / this.ratio);
                }
                break;
            case "width":
                if (!this.stage.x.size) {
                    return;
                }
                this.setScale(this.width / this.stage.x.size);
                break;
            case "height":
                if (!this.stage.y.size) {
                    return;
                }
                this.setScale(this.height / this.stage.y.size);
                break;
            case "contain":
                if (!this.stage.x.size || !this.stage.y.size) {
                    return;
                }
                var w = this.width / this.stage.x.size;
                var h = this.height / this.stage.y.size;
                this.setScale(h < w ? h : w);
                break;
        }
    };
    Content.prototype.applyFilters = function () {
        if (!Object.keys(this.filters).length) {
            return false;
        }
        var rowOffset = Grid_1.GridBase.Settings.headings ? 1 : 0;
        for (var rowIndex = 0; rowIndex < this.originalContent.length; rowIndex++) {
            var offset = Grid_1.GridBase.Settings.headings ? -1 : 0;
            if (this.freezeRange.valid && rowIndex < this.freezeRange.index.row + offset) {
                continue;
            }
            var _loop_1 = function (f) {
                var filter = this_1.filters[f];
                var col = this_1.originalContent[rowIndex][filter.col];
                if (!col)
                    return "continue";
                var colValue = filter.type === "number" ? parseFloat(col.value) : "" + col.formatted_value;
                var filterValue = filter.type === "number" ? parseFloat(filter.value) : "" + filter.value;
                var validValue = false;
                switch (filter.type) {
                    case "number":
                        validValue = eval(colValue + " " + filter.exp + " " + filterValue);
                        break;
                    case "date":
                        var dates = filterValue.split(",");
                        var fromDate = new Date(dates[0]).toISOString();
                        var toDate = new Date(dates[1]).toISOString();
                        var date = new Date(Grid_1.GridBase.helpers.parseDateExcel(colValue)).toISOString();
                        validValue = date >= fromDate && date <= toDate;
                        break;
                    default:
                        filterValue.split(",").forEach2(function (value) {
                            if (validValue) {
                                return;
                            }
                            switch (filter.exp) {
                                case "contains":
                                    validValue = colValue.toLowerCase().indexOf(value.toLowerCase()) > -1;
                                    break;
                                case "starts_with":
                                    validValue =
                                        colValue.toLowerCase().substring(0, value.length) === value.toLowerCase();
                                    break;
                                default:
                                    validValue = colValue == value;
                                    break;
                            }
                        });
                        break;
                }
                if (!validValue)
                    this_1.content[rowIndex + rowOffset] = null;
            };
            var this_1 = this;
            for (var f in this.filters) {
                _loop_1(f);
            }
        }
    };
    Content.prototype.applySorting = function () {
        if (!Object.keys(this.sorting).length) {
            return false;
        }
        var key = Object.keys(this.sorting)[0];
        var filter = this.sorting[key];
        var data = this.freezeRange.index.row
            ? this.content.slice(this.freezeRange.index.row, this.content.length)
            : this.content.slice(0);
        var exp = filter.direction === "DESC" ? ">" : "<";
        var opp = filter.direction === "DESC" ? "<" : ">";
        data.sort(function (a, b) {
            var col = Grid_1.GridBase.Settings.headings ? filter.col + 1 : filter.col;
            if (!a || !b || !a[col] || !b[col]) {
                return 1;
            }
            var aVal = filter.type === "number" ? parseFloat(a[col].value) || 0 : "\"" + a[col].value + "\"";
            var bVal = filter.type === "number" ? parseFloat(b[col].value) || 0 : "\"" + b[col].value + "\"";
            if (eval(aVal + " " + exp + " " + bVal)) {
                return -1;
            }
            if (eval(aVal + " " + opp + " " + bVal)) {
                return 1;
            }
            return 0;
        });
        this.content = this.freezeRange.index.row
            ? this.content.slice(0, this.freezeRange.index.row).concat(data)
            : data;
    };
    Content.prototype.clone = function (content) {
        if (content === void 0) { content = []; }
        if (!content || !content.length) {
            return [];
        }
        this.cells = [];
        var clone = [];
        var rowCount = 0;
        for (var rowIndex = 0; rowIndex < content.length; rowIndex++) {
            var row = JSON.parse(JSON.stringify(content[rowIndex]));
            this.cells[rowIndex] = [];
            if (Grid_1.GridBase.Settings.headings) {
                if (!rowIndex) {
                    clone[0] = [];
                    var headingCell_1 = JSON.parse(JSON.stringify(row[0]));
                    headingCell_1 = this.setHeadingCellStyle(headingCell_1, "corner");
                    headingCell_1.value = headingCell_1.formatted_value = "";
                    headingCell_1.position = {
                        row: 0,
                        col: 0
                    };
                    headingCell_1.index = {
                        row: -1,
                        col: -1
                    };
                    clone[0].push(headingCell_1);
                    for (var colIndex = 0; colIndex < row.length; colIndex++) {
                        if (this.hiddenColumns.indexOf(colIndex) > -1) {
                            continue;
                        }
                        var headingCell_2 = JSON.parse(JSON.stringify(row[colIndex]));
                        headingCell_2 = this.setHeadingCellStyle(headingCell_2, "col");
                        headingCell_2.value = headingCell_2.formatted_value = this.toColumnName(colIndex + 1);
                        headingCell_2.position = {
                            row: rowIndex,
                            col: colIndex + 1
                        };
                        headingCell_2.index = {
                            row: 0,
                            col: colIndex
                        };
                        headingCell_2.heading = true;
                        clone[0].push(headingCell_2);
                    }
                    rowCount++;
                }
                var headingCell = JSON.parse(JSON.stringify(row[0]));
                headingCell = this.setHeadingCellStyle(headingCell, "row");
                headingCell.value = headingCell.formatted_value = "" + (rowIndex + 1);
                headingCell.index = {
                    row: rowIndex,
                    col: 0
                };
                row.unshift(headingCell);
            }
            clone[rowCount] = [];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var cell = row[colIndex];
                cell.position = {
                    row: rowCount,
                    col: colIndex
                };
                cell.index = {
                    row: rowIndex,
                    col: Grid_1.GridBase.Settings.headings ? colIndex - 1 : colIndex
                };
                if (this.hiddenColumns.indexOf(cell.index.col) === -1) {
                    clone[rowCount].push(cell);
                }
                if (Grid_1.GridBase.Settings.headings && !colIndex) {
                    continue;
                }
                if (Grid_1.GridBase.Settings.headings && this.hiddenColumns.indexOf(colIndex - 1) > -1) {
                }
                this.cells[rowIndex].push(cell);
            }
            rowCount++;
        }
        return clone;
    };
    Content.prototype.setHeadingCellStyle = function (cell, pos) {
        cell.style["vertical-align"] = "center";
        cell.style["text-align"] = "center";
        cell.style["background-color"] = Grid_1.GridBase.Settings.contrast === "light" ? "e3e3e4" : "333333";
        if (pos === "corner" || pos === "row") {
            cell.style["width"] = "40px";
        }
        if (pos === "corner" || pos === "col") {
            cell.style["height"] = "20px";
        }
        cell.style["font-family"] = "Calibri";
        cell.style["font-size"] = "9pt";
        cell.style["font-weight"] = "normal";
        cell.style["font-style"] = "normal";
        cell.style["color"] = Grid_1.GridBase.Settings.contrast === "light" ? "000000" : "999999";
        ["b", "t", "r", "l"].forEach(function (d) {
            cell.style[d + "bc"] = Grid_1.GridBase.Settings.contrast === "light" ? "999999" : "000000";
            cell.style[d + "bs"] = "solid";
            cell.style[d + "bw"] = "thin";
            cell.heading = true;
        });
        return cell;
    };
    Content.prototype.toColumnName = function (num) {
        for (var ret = "", a = 1, b = 26; (num -= a) >= 0; a = b, b *= 26) {
            ret = String.fromCharCode(parseInt((num % b) / a) + 65) + ret;
        }
        return ret;
    };
    return Content;
}());
exports.Content = Content;
//# sourceMappingURL=Content.js.map