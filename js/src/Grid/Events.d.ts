import Emitter, { IEmitter } from "../Emitter";
export interface IEvents extends IEmitter {
    rightClick: boolean;
    preventContextMenu: boolean;
    init: () => void;
    destroy: () => void;
}
export declare class Events extends Emitter implements IEvents {
    rightClick: boolean;
    preventContextMenu: boolean;
    private hammer;
    private freeScroll;
    constructor();
    init(): void;
    destroy(): void;
    update(): void;
    private onVisibilityEvent;
    private setupTouch;
    private onResize;
    private onKeydown;
    private onKeyup;
    private onMouseDown;
    private onMouseOver;
    private onMouseUp;
    private onMouseMove;
    private onWheelEvent;
    private isRightClick;
}
