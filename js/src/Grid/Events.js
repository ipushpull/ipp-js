"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Hammer = require("hammerjs");
var Grid_1 = require("./Grid");
var Emitter_1 = require("../Emitter");
var Events = (function (_super) {
    __extends(Events, _super);
    function Events() {
        var _this = _super.call(this) || this;
        _this.rightClick = false;
        _this.preventContextMenu = false;
        _this.freeScroll = {
            go: false,
            startX: 0,
            startY: 0,
        };
        _this.onVisibilityEvent = function (evt) {
            Grid_1.GridBase.Content.hidden = document.hidden;
            if (!Grid_1.GridBase.Content.hidden) {
                Grid_1.GridBase.update();
            }
        };
        _this.onResize = function (evt) {
            if (_this.resizeTimer) {
                clearTimeout(_this.resizeTimer);
                _this.resizeTimer = null;
            }
            _this.resizeTimer = setTimeout(function () {
                Grid_1.GridBase.update();
            }, 300);
        };
        _this.onKeydown = function (evt) {
            _this.emit('keydown', evt);
            Grid_1.GridBase.Canvas.update();
            Grid_1.GridBase.Container.update();
        };
        _this.onKeyup = function (evt) {
            _this.emit('keyup', evt);
            if (Grid_1.GridBase.Content.editing)
                return;
            Grid_1.GridBase.Canvas.update();
            Grid_1.GridBase.Container.update();
        };
        _this.onMouseDown = function (evt) {
            console.log('onMouseDown', evt);
            var timer = 0;
            Grid_1.GridBase.Content.hasFocus = Grid_1.GridBase.Container.hasFocus(evt);
            _this.rightClick = _this.isRightClick(evt);
            _this.emit('mousedown', evt);
            Grid_1.GridBase.Canvas.update();
            if (Grid_1.GridBase.Content.hasFocus) {
            }
        };
        _this.onMouseOver = function (evt) {
            _this.emit('mouseover', evt);
            Grid_1.GridBase.Canvas.update();
            Grid_1.GridBase.Container.update();
        };
        _this.onMouseUp = function (evt) {
            _this.emit('mouseup', evt);
            if (evt.which === 2) {
                if (_this.freeScroll.go) {
                    _this.freeScroll.go = false;
                }
                else {
                    _this.freeScroll = {
                        go: true,
                        startX: evt.clientX,
                        startY: evt.clientY,
                    };
                }
            }
            Grid_1.GridBase.Canvas.update();
            Grid_1.GridBase.Container.update();
            _this.rightClick = false;
        };
        _this.onMouseMove = function (evt) {
            _this.emit('mousemove', evt);
            if (_this.freeScroll.go) {
                var x = Math.round((evt.clientX - _this.freeScroll.startX) / 20);
                var y = Math.round((evt.clientY - _this.freeScroll.startY) / 20);
                Grid_1.GridBase.Content.setOffsets({
                    x: x,
                    y: y,
                });
                Grid_1.GridBase.Canvas.update();
                Grid_1.GridBase.Container.update();
            }
            else if (Grid_1.GridBase.Content.scrolling || Grid_1.GridBase.Content.resizing) {
                Grid_1.GridBase.Canvas.update();
                Grid_1.GridBase.Container.update();
            }
            else if (Grid_1.GridBase.Content.cellSelection.go) {
                Grid_1.GridBase.Content.keepSelectorWithinView();
                Grid_1.GridBase.Canvas.update();
                Grid_1.GridBase.Container.update();
            }
        };
        _this.onWheelEvent = function (evt) {
            _this.emit('wheelevent', evt);
            var offsetX = evt.deltaX ? (Math.abs(evt.deltaX) >= 100 ? Math.round(Math.abs(evt.deltaX) / 100) : 1) : 0;
            var offsetY = evt.deltaY ? (Math.abs(evt.deltaY) >= 100 ? Math.round(Math.abs(evt.deltaY) / 100) : 1) : 0;
            var directionX = evt.deltaX > 0 ? 1 : -1;
            var directionY = evt.deltaY > 0 ? 1 : -1;
            offsetX *= directionX;
            offsetY *= directionY;
            if ((Grid_1.GridBase.Content.stage.y.size * Grid_1.GridBase.Settings.scale) / Grid_1.GridBase.Settings.ratio <= Grid_1.GridBase.Content.height &&
                (Grid_1.GridBase.Content.stage.x.size * Grid_1.GridBase.Settings.scale) / Grid_1.GridBase.Settings.ratio > Grid_1.GridBase.Content.width) {
                offsetX = offsetY;
            }
            var curretOffset = JSON.parse(JSON.stringify(Grid_1.GridBase.Content.offsets));
            Grid_1.GridBase.Content.setOffsets({
                x: offsetX,
                y: offsetY,
            });
            Grid_1.GridBase.Canvas.update();
            Grid_1.GridBase.Container.update();
            if (curretOffset.x.offset !== Grid_1.GridBase.Content.offsets.x.offset ||
                curretOffset.y.offset !== Grid_1.GridBase.Content.offsets.y.offset) {
                evt.preventDefault();
            }
        };
        return _this;
    }
    Events.prototype.init = function () {
        var _this = this;
        var interval = setInterval(function () {
            if (!Grid_1.GridBase.Container.element.contains(Grid_1.GridBase.Canvas.canvas)) {
                return;
            }
            clearInterval(interval);
            interval = undefined;
            document.addEventListener('visibilitychange', _this.onVisibilityEvent, false);
            window.addEventListener('resize', _this.onResize);
            window.addEventListener('keydown', _this.onKeydown, false);
            window.addEventListener('keyup', _this.onKeyup, false);
            if (Grid_1.GridBase.Settings.touch) {
                _this.setupTouch();
                Grid_1.GridBase.Container.element.addEventListener('touchstart', _this.onMouseDown, false);
                Grid_1.GridBase.Container.element.addEventListener('touchend', _this.onMouseUp, false);
            }
            else {
                Grid_1.GridBase.Container.element.addEventListener('wheel', _this.onWheelEvent, false);
                Grid_1.GridBase.Container.element.addEventListener('mouseover', _this.onMouseOver, false);
                window.addEventListener('mousemove', _this.onMouseMove, false);
                window.addEventListener('mouseup', _this.onMouseUp, false);
                window.addEventListener('mousedown', _this.onMouseDown, false);
            }
            window.oncontextmenu = function () {
                if (_this.preventContextMenu) {
                    _this.preventContextMenu = false;
                    return false;
                }
            };
        }, 50);
    };
    Events.prototype.destroy = function () {
        document.removeEventListener('visibilitychange', this.onVisibilityEvent, false);
        window.removeEventListener('reszie', this.onResize);
        window.removeEventListener('keydown', this.onKeydown, false);
        window.removeEventListener('keyup', this.onKeyup, false);
        if (Grid_1.GridBase.Settings.touch) {
            Grid_1.GridBase.Container.element.removeEventListener('touchstart', this.onMouseDown, false);
            Grid_1.GridBase.Container.element.removeEventListener('touchend', this.onMouseUp, false);
            this.setupTouch();
        }
        else {
            Grid_1.GridBase.Container.element.removeEventListener('wheel', this.onWheelEvent, false);
            Grid_1.GridBase.Container.element.removeEventListener('mouseover', this.onMouseOver, false);
            window.removeEventListener('mousemove', this.onMouseMove, false);
            window.removeEventListener('mouseup', this.onMouseUp, false);
            window.removeEventListener('mousedown', this.onMouseDown, false);
        }
        if (this.hammer) {
            this.hammer.destroy();
        }
    };
    Events.prototype.update = function () { };
    Events.prototype.setupTouch = function () {
        this.hammer = new Hammer(Grid_1.GridBase.Container.element);
        var pan = new Hammer.Pan();
        var pinch = new Hammer.Pinch();
        this.hammer.add([pan, pinch]);
        var currentScale = (Grid_1.GridBase.Content.scale / Grid_1.GridBase.Content.ratio) * 1;
        this.hammer.on('pinchstart', function (evt) {
            Grid_1.GridBase.Settings.fit = 'scroll';
            currentScale = (Grid_1.GridBase.Content.scale / Grid_1.GridBase.Content.ratio) * 1;
        });
        this.hammer.on('pinchmove', function (evt) {
            Grid_1.GridBase.Content.setScale(currentScale * evt.scale);
            Grid_1.GridBase.update();
        });
        this.hammer.on('pinchend', function (evt) {
            currentScale = (Grid_1.GridBase.Content.scale / Grid_1.GridBase.Content.ratio) * 1;
        });
        var currentOffsets = {
            x: 0,
            y: 0,
        };
        this.hammer.on('panstart', function (evt) {
            currentOffsets = {
                x: evt.center.x,
                y: evt.center.y,
            };
        });
        this.hammer.on('panmove', function (evt) {
            var moveBy = Math.round(Grid_1.GridBase.Content.ratio / Grid_1.GridBase.Content.scale);
            if (moveBy < 1)
                moveBy = 1;
            var x = 0;
            if (Math.abs(evt.center.x - currentOffsets.x) > 5 * Grid_1.GridBase.Content.scale) {
                var directionX = evt.velocityX > 0 ? -1 : 1;
                currentOffsets.x = evt.center.x;
                x = moveBy * directionX;
            }
            var y = 0;
            if (Math.abs(evt.center.y - currentOffsets.y) > 5 * Grid_1.GridBase.Content.scale) {
                var directionY = evt.velocityY > 0 ? -1 : 1;
                currentOffsets.y = evt.center.y;
                y = moveBy * directionY;
            }
            Grid_1.GridBase.Content.setOffsets({ x: x, y: y });
            Grid_1.GridBase.Canvas.update();
            Grid_1.GridBase.Container.update();
        });
        this.hammer.on('panend', function (evt) { });
    };
    Events.prototype.isRightClick = function (evt) {
        var isRightMB;
        if ('which' in evt)
            isRightMB = evt.which === 3;
        else if ('button' in evt)
            isRightMB = evt.button === 2;
        return isRightMB;
    };
    return Events;
}(Emitter_1.default));
exports.Events = Events;
//# sourceMappingURL=Events.js.map