import Emitter, { IEmitter } from '../Emitter';
import { IPageService } from '../Page/Page';
import { IContainer } from './Container';
import { ICanvas } from './Canvas';
import { IEvents } from './Events';
import { ISettings } from './Settings';
import { IContent, IContentFind, IContentCell } from './Content';
interface IBaseProvider extends IEmitter {
    ON_CELL_CLICKED: string;
    ON_CELL_CLICKED_RIGHT: string;
    ON_CELL_HISTORY_CLICKED: string;
    ON_CELL_LINK_CLICKED: string;
    ON_CELL_DOUBLE_CLICKED: string;
    ON_TAG_CLICKED: string;
    ON_CELL_VALUE: string;
    ON_CELL_SELECTED: string;
    ON_CELL_RESET: string;
    ON_EDITING: string;
    ON_RESIZE: string;
    ON_HEADING_CLICKED: string;
    Content: IContent;
    Container: IContainer;
    Canvas: ICanvas;
    Events: IEvents;
    Settings: ISettings;
    classy: any;
    helpers: any;
    update: () => void;
}
export declare let GridBase: IBaseProvider;
export interface IGridModule {
    init: () => void;
    destroy: () => void;
    update: () => void;
}
export interface IGridProvider {
    container: string | HTMLElement;
    Base: IBaseProvider;
    init: (container: string | HTMLElement, Page: IPageService) => void;
    destroy: () => void;
    render: (data: any, ranges?: any) => void;
    setFit(value: string): void;
    cellHistory(data: any): void;
    cellHighlights(data: any): void;
    setMergeRanges(data: any): void;
    setRanges(data: any): void;
    setSorting(data: any): void;
    setFilters(data: any): void;
    clearSorting(): void;
    clearFilters(): void;
    setOption(key: string, value: any): void;
    find(value: IContentFind): void;
    clearFound(): void;
    getContentHtml(): string;
    getCell(row: number, col: number): any;
    getCells(fromCell: any, toCell: any): any;
    setSelector(from: any, to?: any): void;
    hideSelector(): void;
}
export declare class Grid extends Emitter implements IGridProvider {
    container: string | HTMLElement;
    Base: IBaseProvider;
    private initialised;
    constructor(container: string | HTMLElement, options?: any);
    init(container: string | HTMLElement, options?: any): void;
    destroy(): void;
    render(data: any, ranges?: any): void;
    setFit(value: string): void;
    cellHistory(data: any): void;
    cellHighlights(data: any): void;
    setMergeRanges(data: any): void;
    setHiddenColumns(data: any): void;
    setRanges(data: any): void;
    setSorting(data: any): void;
    setFilters(data: any): void;
    clearSorting(): void;
    clearFilters(): void;
    setOption(key: string, value: any): void;
    find(value: IContentFind): void;
    clearFound(): void;
    getContentHtml(): string;
    getCell(row: number, col: number): IContentCell;
    getCells(fromCell: any, toCell: any): any;
    setSelector(from: any, to?: any): void;
    hideSelector(): void;
    onGridBaseEvent: (name: string, args?: any) => void;
}
export {};
