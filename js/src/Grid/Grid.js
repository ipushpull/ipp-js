"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = require("../Emitter");
var Container_1 = require("./Container");
var Canvas_1 = require("./Canvas");
var Events_1 = require("./Events");
var Settings_1 = require("./Settings");
var Content_1 = require("./Content");
var Helpers_1 = require("../Helpers");
var Classy_1 = require("../Classy");
var Base = (function (_super) {
    __extends(Base, _super);
    function Base() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Base.prototype, "ON_CELL_CLICKED", {
        get: function () {
            return 'cell_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_NO_CELL", {
        get: function () {
            return 'no_cell';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_CELL_CLICKED_RIGHT", {
        get: function () {
            return 'cell_clicked_right';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_CELL_HISTORY_CLICKED", {
        get: function () {
            return 'cell_history_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_CELL_LINK_CLICKED", {
        get: function () {
            return 'cell_link_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_CELL_DOUBLE_CLICKED", {
        get: function () {
            return 'cell_double_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_TAG_CLICKED", {
        get: function () {
            return 'tag_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_CELL_VALUE", {
        get: function () {
            return 'cell_value';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_CELL_SELECTED", {
        get: function () {
            return 'cell_selected';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_CELL_RESET", {
        get: function () {
            return 'cell_reset';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_EDITING", {
        get: function () {
            return 'editing';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_RESIZE", {
        get: function () {
            return 'resize';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Base.prototype, "ON_HEADING_CLICKED", {
        get: function () {
            return 'heading_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Base.prototype.update = function () {
        exports.GridBase.Content.update();
        exports.GridBase.Settings.update();
        setTimeout(function () {
            exports.GridBase.Canvas.update(true);
            exports.GridBase.Container.update();
        }, 50);
    };
    return Base;
}(Emitter_1.default));
exports.GridBase = new Base();
var Grid = (function (_super) {
    __extends(Grid, _super);
    function Grid(container, options) {
        var _this = _super.call(this) || this;
        _this.container = container;
        _this.initialised = false;
        _this.onGridBaseEvent = function (name, args) {
            _this.emit(name, args);
        };
        _this.init(container, options);
        return _this;
    }
    Grid.prototype.init = function (container, options) {
        this.destroy();
        exports.GridBase.register(this.onGridBaseEvent);
        this.Base = exports.GridBase;
        exports.GridBase.Events = new Events_1.Events();
        exports.GridBase.Settings = new Settings_1.Settings(options);
        exports.GridBase.Content = new Content_1.Content();
        exports.GridBase.Container = new Container_1.Container(container);
        exports.GridBase.Canvas = new Canvas_1.Canvas();
        exports.GridBase.helpers = new Helpers_1.default();
        exports.GridBase.classy = new Classy_1.default();
        exports.GridBase.Events.init();
        this.initialised = true;
    };
    Grid.prototype.destroy = function () {
        if (!this.initialised) {
            return;
        }
        this.Base.Container.destroy();
        this.Base.Events.destroy();
    };
    Grid.prototype.render = function (data, ranges) {
        exports.GridBase.Container.cleanLinks();
        exports.GridBase.Content.set(data);
        if (ranges) {
            exports.GridBase.Content.setRanges(ranges);
        }
        exports.GridBase.Content.update();
        exports.GridBase.Container.update();
        exports.GridBase.Canvas.update();
    };
    Grid.prototype.setFit = function (value) {
        exports.GridBase.Content.setFit(value);
        exports.GridBase.update();
    };
    Grid.prototype.cellHistory = function (data) {
        exports.GridBase.Content.cellHistory(data);
    };
    Grid.prototype.cellHighlights = function (data) {
        exports.GridBase.Content.cellHighlights(data);
    };
    Grid.prototype.setMergeRanges = function (data) {
        exports.GridBase.Content.setMergeRanges(data);
    };
    Grid.prototype.setHiddenColumns = function (data) {
        exports.GridBase.Content.setHiddenColumns(data);
    };
    Grid.prototype.setRanges = function (data) {
        exports.GridBase.Content.setRanges(data);
    };
    Grid.prototype.setSorting = function (data) {
        exports.GridBase.Content.setSorting(data);
    };
    Grid.prototype.setFilters = function (data) {
        exports.GridBase.Content.setFilters(data);
    };
    Grid.prototype.clearSorting = function () {
        exports.GridBase.Content.clearSorting();
    };
    Grid.prototype.clearFilters = function () {
        exports.GridBase.Content.clearFilters();
    };
    Grid.prototype.setOption = function (key, value) {
        exports.GridBase.Settings.set(key, value);
        exports.GridBase.Content.setSelector();
    };
    Grid.prototype.find = function (value) {
        exports.GridBase.Content.find(value);
    };
    Grid.prototype.clearFound = function () {
        exports.GridBase.Content.clearFound();
    };
    Grid.prototype.getContentHtml = function () {
        return exports.GridBase.Content.getContentHtml();
    };
    Grid.prototype.getCell = function (row, col) {
        if (!exports.GridBase.Content.cells[row] || !exports.GridBase.Content.cells[row][col]) {
            throw new Error("Cell does not exist");
        }
        return exports.GridBase.Content.cells[row][col];
    };
    Grid.prototype.getCells = function (fromCell, toCell) {
        return exports.GridBase.Content.getCells(fromCell, toCell);
    };
    Grid.prototype.setSelector = function (from, to) {
        exports.GridBase.Content.setSelector(from, to);
        exports.GridBase.Container.update();
        exports.GridBase.Canvas.update();
    };
    Grid.prototype.hideSelector = function () {
        exports.GridBase.Content.setSelector();
        exports.GridBase.Container.update();
        exports.GridBase.Canvas.update();
    };
    return Grid;
}(Emitter_1.default));
exports.Grid = Grid;
//# sourceMappingURL=Grid.js.map