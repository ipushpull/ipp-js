"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ua = require("ua-parser-js");
var Settings = (function () {
    function Settings(options) {
        if (options === void 0) { options = {}; }
        this.contrast = "light";
        this.tracking = false;
        this.highlights = false;
        this.highlightsShowTime = 3000;
        this.highlightsRemoveTime = 1000;
        this.touch = false;
        this.pause = false;
        this.headings = false;
        this.canEdit = true;
        this.alwaysEditing = false;
        this.disallowSelection = false;
        this.hasFocus = false;
        this.fluid = false;
        this.scrollbarWidth = 8;
        this.scrollbarInset = false;
        this.scale = 1;
        this.ratio = 1;
        this.fit = "scroll";
        this.userId = 0;
        this.userTrackingColors = ["deeppink", "aqua", "tomato", "orange", "yellow", "limegreen"];
        this.noAccessImages = {
            light: "",
            dark: ""
        };
        this.historyIndicatorSize = 16;
        this.backgroundColor = "transparent";
        for (var option in options) {
            if (!options.hasOwnProperty(option) || this[option] === undefined) {
                continue;
            }
            this[option] = options[option];
        }
        this.init();
    }
    Settings.prototype.init = function () {
        this.destroy();
        var parser = new ua();
        var parseResult = parser.getResult();
        this.touch =
            parseResult.device && (parseResult.device.type === "tablet" || parseResult.device.type === "mobile");
        this.update();
    };
    Settings.prototype.set = function (key, value) {
        if (this[key] === undefined) {
            return;
        }
        this[key] = value;
    };
    Settings.prototype.destroy = function () { };
    Settings.prototype.update = function () {
    };
    return Settings;
}());
exports.Settings = Settings;
//# sourceMappingURL=Settings.js.map