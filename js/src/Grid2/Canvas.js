"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Helpers_1 = require("../Helpers");
var Grid_1 = require("./Grid");
var Canvas = (function () {
    function Canvas() {
        this.rectangles = [];
        this.offsetRectangles = [];
        this.texts = [];
        this.lines = [];
        this.hidden = [];
        this.borders = {
            widths: {
                none: 0,
                thin: 1,
                medium: 2,
                thick: 3,
            },
            styles: {
                none: 'solid',
                solid: 'solid',
                double: 'double',
            },
            names: {
                t: 'top',
                r: 'right',
                b: 'bottom',
                l: 'left',
            },
        };
        this.selection = {
            from: undefined,
            to: undefined,
        };
        this.helpers = new Helpers_1.default();
    }
    Canvas.prototype.init = function () { };
    Canvas.prototype.destroy = function () { };
    Canvas.prototype.update = function () {
        this.render();
    };
    Canvas.prototype.render = function () {
        this.resetElements();
        this.clearCanvas();
        if (!Grid_1.GridBase.Content.content.length) {
            Grid_1.GridBase.Container.bufferCanvasContext.fillStyle = Grid_1.GridBase.Settings.contrast === 'dark' ? '#FFFFFF' : '#000000';
            Grid_1.GridBase.Container.bufferCanvasContext.font = "14px Arial";
            Grid_1.GridBase.Container.bufferCanvasContext.textAlign = 'left';
            Grid_1.GridBase.Container.bufferCanvasContext.fillText('No rows match filter', 5, 15);
            this.drawCanvas();
            return;
        }
        if (Grid_1.GridBase.Content.cellSelection.from) {
            this.selection.from = Grid_1.GridBase.Content.cellSelection.flipped ? Grid_1.GridBase.Content.cellSelection.flippedFrom : Grid_1.GridBase.Content.cellSelection.from.index;
            this.selection.to = Grid_1.GridBase.Content.cellSelection.flipped ? Grid_1.GridBase.Content.cellSelection.flippedTo : Grid_1.GridBase.Content.cellSelection.to.index;
        }
        var offsetCol = Grid_1.GridBase.Content.offsets.colOffset;
        var offsetRow = Grid_1.GridBase.Content.offsets.rowOffset;
        for (var rowIndex = Grid_1.GridBase.Content.offsets.row - 1; rowIndex <= Grid_1.GridBase.Content.offsets.rowTo; rowIndex++) {
            if (rowIndex < 0)
                continue;
            var row = Grid_1.GridBase.Content.content[rowIndex];
            if (!row) {
                continue;
            }
            for (var colIndex = Grid_1.GridBase.Content.offsets.col - 1; colIndex <= Grid_1.GridBase.Content.offsets.colTo; colIndex++) {
                if (colIndex < 0)
                    continue;
                var cell = row[colIndex];
                if (!cell)
                    continue;
                var cellMeta = Grid_1.GridBase.Content.contentMeta[rowIndex][colIndex];
                if (cellMeta.hidden === true) {
                    for (var i = colIndex; i >= 0; i--) {
                        var c = row[i];
                        var cM = Grid_1.GridBase.Content.contentMeta[rowIndex][i];
                        if (cM.hidden || cM.index.col >= Grid_1.GridBase.Content.offsets.col - 1)
                            continue;
                        this.pushElements(c, this.setCoords(cM.coords, offsetRow, offsetCol), cM);
                    }
                    continue;
                }
                this.pushElements(cell, this.setCoords(cellMeta.coords, offsetRow, offsetCol), cellMeta);
            }
        }
        this.createGraphics();
        this.resetElements();
        for (var rowIndex = Grid_1.GridBase.Content.offsets.row; rowIndex < Grid_1.GridBase.Content.content.length; rowIndex++) {
            var row = Grid_1.GridBase.Content.content[rowIndex];
            for (var colIndex = 0; colIndex < Grid_1.GridBase.Content.getFreezeIndex('col'); colIndex++) {
                var cell = row[colIndex];
                var cellMeta = Grid_1.GridBase.Content.contentMeta[rowIndex][colIndex];
                this.pushElements(cell, this.setCoords(cellMeta.coords, offsetRow, 0), cellMeta);
            }
        }
        for (var rowIndex = 0; rowIndex < Grid_1.GridBase.Content.getFreezeIndex('row'); rowIndex++) {
            var row = Grid_1.GridBase.Content.content[rowIndex];
            if (!row)
                continue;
            for (var colIndex = Grid_1.GridBase.Content.offsets.col; colIndex < row.length; colIndex++) {
                var cell = row[colIndex];
                var cellMeta = Grid_1.GridBase.Content.contentMeta[rowIndex][colIndex];
                this.pushElements(cell, this.setCoords(cellMeta.coords, 0, offsetCol), cellMeta);
            }
        }
        this.createGraphics();
        this.resetElements();
        for (var rowIndex = 0; rowIndex < Grid_1.GridBase.Content.getFreezeIndex('row'); rowIndex++) {
            var row = Grid_1.GridBase.Content.content[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < Grid_1.GridBase.Content.getFreezeIndex('col'); colIndex++) {
                var cell = row[colIndex];
                var cellMeta = Grid_1.GridBase.Content.contentMeta[rowIndex][colIndex];
                this.pushElements(cell, cellMeta.coords, cellMeta);
            }
        }
        this.createGraphics();
        if (Object.keys(Grid_1.GridBase.Content.sorting).length) {
            var colName = Object.keys(Grid_1.GridBase.Content.sorting)[0];
            var col = Grid_1.GridBase.Content.sorting[colName];
            var offset = Grid_1.GridBase.Settings.headings ? 1 : 0;
            var cellMeta = Grid_1.GridBase.Content.getContentCellMeta({ row: 0, col: col.col + offset });
            if (cellMeta) {
                this.drawSortingIndicator(cellMeta, col.direction);
            }
        }
        this.drawCanvas();
    };
    Canvas.prototype.resetElements = function () {
        this.lines = [];
        this.offsetRectangles = [];
        this.rectangles = [];
        this.texts = [];
        this.hidden = [];
    };
    Canvas.prototype.clearCanvas = function () {
        Grid_1.GridBase.Container.bufferCanvasContext.save();
        Grid_1.GridBase.Container.bufferCanvasContext.clearRect(0, 0, Grid_1.GridBase.Container.containerRect.width * Grid_1.GridBase.Settings.ratio, Grid_1.GridBase.Container.containerRect.height * Grid_1.GridBase.Settings.ratio);
        Grid_1.GridBase.Container.bufferCanvasContext.scale(Grid_1.GridBase.Settings.scale, Grid_1.GridBase.Settings.scale);
    };
    Canvas.prototype.drawCanvas = function () {
        Grid_1.GridBase.Container.bufferCanvasContext.restore();
        Grid_1.GridBase.Container.canvasContext.clearRect(0, 0, Grid_1.GridBase.Container.containerRect.width * Grid_1.GridBase.Settings.ratio, Grid_1.GridBase.Container.containerRect.height * Grid_1.GridBase.Settings.ratio);
        Grid_1.GridBase.Container.canvasContext.drawImage(Grid_1.GridBase.Container.bufferCanvas, 0, 0);
    };
    Canvas.prototype.createGraphics = function () {
        for (var i = 0; i < this.offsetRectangles.length; i++) {
            this.drawOffsetRect(this.offsetRectangles[i][0], this.offsetRectangles[i][1]);
        }
        for (var i = 0; i < this.rectangles.length; i++) {
            this.drawRect(this.rectangles[i][0], this.rectangles[i][1]);
        }
        for (var i = 0; i < this.texts.length; i++) {
            this.drawText(this.texts[i][0], this.texts[i][1], this.texts[i][2]);
        }
        for (var i = 0; i < this.hidden.length; i++) {
            this.drawHidden(this.hidden[i][0], this.hidden[i][1], this.hidden[i][2]);
        }
        for (var i = 0; i < this.lines.length; i++) {
            this.drawLine(this.lines[i][0], this.lines[i][1], this.lines[i][2]);
        }
    };
    Canvas.prototype.setCoords = function (coords, offsetRow, offsetCol) {
        return {
            x: coords.x - offsetCol,
            y: coords.y - offsetRow,
            width: coords.width,
            height: coords.height,
        };
    };
    Canvas.prototype.pushElements = function (cell, coords, cellMeta) {
        var color = '#' + cell.style['background-color'];
        if (cellMeta.heading && this.highlightHeading(cellMeta)) {
            color = Grid_1.GridBase.Settings.contrast === 'dark' ? '#555555' : '#b7b7b7';
        }
        else if (!cellMeta.heading && cellMeta.formatting && cellMeta.formatting.background) {
            color = cellMeta.formatting.background;
        }
        else if (!this.helpers.validHex(color)) {
            color = '#FFFFFF';
        }
        this.offsetRectangles.push([coords, color]);
        this.rectangles.push([coords, color]);
        if (cellMeta.allow !== 'no') {
            this.texts.push([cell, coords, cellMeta]);
        }
        if ((cell.access && cell.access === 'no') || cellMeta.allow === 'no') {
            this.hidden.push([cell, coords, cellMeta]);
        }
        this.drawLines(cell, coords, this.helpers.getContrastYIQ(color));
    };
    Canvas.prototype.highlightHeading = function (cellMeta) {
        if (!Grid_1.GridBase.Content.cellSelection.from)
            return false;
        if ((cellMeta.heading === 'row'
            && cellMeta.index.col >= this.selection.from.col
            && cellMeta.index.col <= this.selection.to.col)
            ||
                (cellMeta.heading === 'col'
                    && cellMeta.index.row >= this.selection.from.row
                    && cellMeta.index.row <= this.selection.to.row)) {
            return true;
        }
        return false;
    };
    Canvas.prototype.drawSortingIndicator = function (cell, direction) {
        Grid_1.GridBase.Container.bufferCanvasContext.beginPath();
        Grid_1.GridBase.Container.bufferCanvasContext.fillStyle = 'red';
        var x = cell.coords.x + cell.coords.width - 10 - Grid_1.GridBase.Content.offsets.colOffset;
        if (direction === 'DESC') {
            var y = cell.coords.y;
            Grid_1.GridBase.Container.bufferCanvasContext.moveTo(x, y);
            Grid_1.GridBase.Container.bufferCanvasContext.lineTo(x + 10, y);
            Grid_1.GridBase.Container.bufferCanvasContext.lineTo(x + 5, y + 5);
            Grid_1.GridBase.Container.bufferCanvasContext.lineTo(x, y);
        }
        else {
            var y = cell.coords.y + cell.coords.height - 5;
            Grid_1.GridBase.Container.bufferCanvasContext.moveTo(x + 5, y);
            Grid_1.GridBase.Container.bufferCanvasContext.lineTo(x + 10, y + 5);
            Grid_1.GridBase.Container.bufferCanvasContext.lineTo(x, y + 5);
            Grid_1.GridBase.Container.bufferCanvasContext.lineTo(x + 5, y);
        }
        Grid_1.GridBase.Container.bufferCanvasContext.fill();
    };
    Canvas.prototype.drawRect = function (coords, color) {
        Grid_1.GridBase.Container.bufferCanvasContext.beginPath();
        Grid_1.GridBase.Container.bufferCanvasContext.fillStyle = color;
        Grid_1.GridBase.Container.bufferCanvasContext.fillRect(coords.x, coords.y, coords.width, coords.height);
    };
    Canvas.prototype.drawOffsetRect = function (coords, color) {
        Grid_1.GridBase.Container.bufferCanvasContext.beginPath();
        Grid_1.GridBase.Container.bufferCanvasContext.fillStyle = color;
        Grid_1.GridBase.Container.bufferCanvasContext.fillRect(coords.x - 0.5, coords.y - 0.5, coords.width + 0.5, coords.height + 0.5);
    };
    Canvas.prototype.drawLines = function (cell, coords, contrast) {
        if (contrast === void 0) { contrast = 'light'; }
        var gridlineColor = 'd4d4d4';
        if (cell.style.tbs !== 'none' && cell.style.tbw !== 'none') {
            this.lines.push([
                { x: coords.x, y: coords.y },
                { x: coords.x + coords.width, y: coords.y },
                { width: this.borders.widths[cell.style.tbw], color: cell.style.tbc },
            ]);
        }
        else if (Grid_1.GridBase.Settings.gridlines) {
        }
        if (cell.style.rbs !== 'none' && cell.style.rbw !== 'none') {
            this.lines.push([
                { x: coords.x + coords.width, y: coords.y },
                { x: coords.x + coords.width, y: coords.y + coords.height },
                { width: this.borders.widths[cell.style.rbw], color: cell.style.rbc },
            ]);
        }
        else if (Grid_1.GridBase.Settings.gridlines) {
            this.lines.push([
                { x: coords.x + coords.width, y: coords.y },
                { x: coords.x + coords.width, y: coords.y + coords.height },
                { width: 1, color: gridlineColor },
            ]);
        }
        if (cell.style.bbs !== 'none' && cell.style.bbw !== 'none') {
            this.lines.push([
                { x: coords.x, y: coords.y + coords.height },
                { x: coords.x + coords.width, y: coords.y + coords.height },
                { width: this.borders.widths[cell.style.bbw], color: cell.style.bbc },
            ]);
        }
        else if (Grid_1.GridBase.Settings.gridlines) {
            this.lines.push([
                { x: coords.x, y: coords.y + coords.height },
                { x: coords.x + coords.width, y: coords.y + coords.height },
                { width: 1, color: gridlineColor },
            ]);
        }
        if (cell.style.lbs !== 'none' && cell.style.lbw !== 'none') {
            this.lines.push([
                { x: coords.x, y: coords.y },
                { x: coords.x, y: coords.y + coords.height },
                { width: this.borders.widths[cell.style.lbw], color: cell.style.lbc },
            ]);
        }
        else if (Grid_1.GridBase.Settings.gridlines) {
            this.lines.push([
                { x: coords.x, y: coords.y },
                { x: coords.x, y: coords.y + coords.height },
                { width: 1, color: gridlineColor },
            ]);
        }
    };
    Canvas.prototype.drawLine = function (start, end, options) {
        var t = this.translateCanvas(options.width || 1);
        Grid_1.GridBase.Container.bufferCanvasContext.beginPath();
        Grid_1.GridBase.Container.bufferCanvasContext.lineWidth = options.width || 1;
        Grid_1.GridBase.Container.bufferCanvasContext.strokeStyle =
            options.color.indexOf('rgb') > -1 ? options.color : "#" + (options.color || '000000').replace('#', '');
        Grid_1.GridBase.Container.bufferCanvasContext.moveTo(start.x, start.y);
        Grid_1.GridBase.Container.bufferCanvasContext.lineTo(end.x, end.y);
        Grid_1.GridBase.Container.bufferCanvasContext.stroke();
        this.resetCanvas(t);
    };
    Canvas.prototype.drawText = function (cell, coords, cellMeta) {
        var _this = this;
        if (!cellMeta.heading && (cellMeta.allow === 'no' || cellMeta.button || cellMeta.image)) {
            return;
        }
        var v = this.cleanValue(cell.formatted_value || cell.value);
        if (!v.length)
            return;
        var color = "#" + cell.style['color'].replace('#', '');
        if (!cellMeta.heading && cellMeta.formatting && cellMeta.formatting.color) {
            color = cellMeta.formatting.color;
        }
        Grid_1.GridBase.Container.bufferCanvasContext.fillStyle = color;
        Grid_1.GridBase.Container.bufferCanvasContext.font = this.getFontString(cell);
        Grid_1.GridBase.Container.bufferCanvasContext.textAlign = cell.style['text-align']
            .replace('justify', 'left')
            .replace('start', 'left');
        var x = 0;
        var xPad = 3;
        var y = 0;
        var yPad = 5;
        switch (cell.style['text-align']) {
            case 'right':
                x = coords.x + cellMeta.coords.columnWidth - xPad;
                break;
            case 'middle':
            case 'center':
                x = coords.x + cellMeta.coords.columnWidth / 2;
                break;
            default:
                x = coords.x + xPad;
                break;
        }
        var wrapOffset = 0;
        var fontHeight = Grid_1.GridBase.Container.bufferCanvasContext.measureText('M').width;
        var lines = [];
        var wrap = (cell.style['text-wrap'] && cell.style['text-wrap'] === 'wrap') || cell.style['word-wrap'] === 'break-word';
        if (wrap) {
            lines = this.findLines(v, fontHeight, cellMeta.coords.columnWidth - xPad * 2);
            wrapOffset = lines.offset;
        }
        switch (cell.style['vertical-align']) {
            case 'top':
                y = coords.y + fontHeight + yPad;
                break;
            case 'center':
            case 'middle':
                y = coords.y + coords.height / 2 - wrapOffset / 2 + fontHeight / 2;
                break;
            default:
                y = coords.y + coords.height - yPad - wrapOffset;
                break;
        }
        Grid_1.GridBase.Container.bufferCanvasContext.save();
        Grid_1.GridBase.Container.bufferCanvasContext.beginPath();
        Grid_1.GridBase.Container.bufferCanvasContext.rect(coords.x, coords.y, coords.width, coords.height);
        Grid_1.GridBase.Container.bufferCanvasContext.clip();
        if (wrap) {
            lines.lines.forEach2(function (line) {
                Grid_1.GridBase.Container.bufferCanvasContext.fillText(line.value, x, y + line.y);
                if (cell.link && !cellMeta.heading) {
                    var yLine = Math.round(y + 2 + line.y);
                    var tWidth = Math.round(Grid_1.GridBase.Container.bufferCanvasContext.measureText(line.value).width);
                    var xLine = x;
                    switch (cell.style['text-align']) {
                        case 'right':
                            xLine = x - tWidth;
                            break;
                        case 'middle':
                        case 'center':
                            xLine = x - tWidth / 2;
                            break;
                        default:
                            break;
                    }
                    _this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
                }
            });
        }
        else {
            if (cell.style['number-format'] && cell.style['number-format'].indexOf('0.00') > -1 && /(¥|€|£|\$)/.test(v)) {
                var num = v.split(' ');
                Grid_1.GridBase.Container.bufferCanvasContext.fillText(num.length > 1 ? num[1] : v, x, y);
                if (num.length > 1) {
                    Grid_1.GridBase.Container.bufferCanvasContext.textAlign = 'left';
                    Grid_1.GridBase.Container.bufferCanvasContext.fillText(num[0], coords.x + xPad, y);
                }
            }
            else {
                Grid_1.GridBase.Container.bufferCanvasContext.fillText(v, x, y);
            }
            if (cell.link && !cellMeta.heading) {
                var yLine = Math.round(y + 2);
                var tWidth = Math.round(Grid_1.GridBase.Container.bufferCanvasContext.measureText(v).width);
                var xLine = x;
                switch (cell.style['text-align']) {
                    case 'right':
                        xLine = x - tWidth;
                        break;
                    case 'middle':
                    case 'center':
                        xLine = x - tWidth / 2;
                        break;
                    default:
                        break;
                }
                this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
            }
        }
        Grid_1.GridBase.Container.bufferCanvasContext.restore();
    };
    Canvas.prototype.drawHidden = function (cell, coords, cellMeta) {
        var color = '#' + cell.style['background-color'];
        var contrast = this.helpers.getContrastYIQ(color);
        for (var img in Grid_1.GridBase.Settings.noAccessImages) {
            if (contrast !== img || !Grid_1.GridBase.Settings.noAccessImages[img]) {
                continue;
            }
            var pat = Grid_1.GridBase.Container.bufferCanvasContext.createPattern(document.getElementById(Grid_1.GridBase.Settings.noAccessImages[img]), 'repeat');
            Grid_1.GridBase.Container.bufferCanvasContext.beginPath();
            Grid_1.GridBase.Container.bufferCanvasContext.rect(coords.x, coords.y, coords.width, coords.height);
            Grid_1.GridBase.Container.bufferCanvasContext.fillStyle = pat;
            Grid_1.GridBase.Container.bufferCanvasContext.fill();
        }
    };
    Canvas.prototype.findLines = function (str, fontHeight, cellWidth) {
        var words = str.split(' ');
        var lines = [];
        var word = [];
        var lineY = 0;
        var lineOffset = fontHeight + 4;
        for (var w = 0; w < words.length; w++) {
            var wordWidth = Grid_1.GridBase.Container.bufferCanvasContext.measureText(words[w]).width + 2;
            if (wordWidth > cellWidth) {
                var letters = words[w].split('');
                var width_1 = 0;
                wordWidth = 0;
                word = [];
                for (var i = 0; i < letters.length; i++) {
                    width_1 = Grid_1.GridBase.Container.bufferCanvasContext.measureText(word.join('')).width + 2;
                    if (width_1 < cellWidth) {
                        word.push(letters[i]);
                        wordWidth = width_1;
                    }
                    else {
                        lines.push({
                            value: word.join(''),
                            width: wordWidth,
                        });
                        word = [letters[i]];
                    }
                }
                if (word.length) {
                    lines.push({
                        value: word.join(''),
                        width: wordWidth,
                    });
                }
            }
            else {
                lines.push({
                    value: words[w],
                    width: wordWidth,
                });
            }
        }
        var rows = [];
        var width = 0;
        var lineWords = [];
        for (var i = 0; i < lines.length; i++) {
            width += lines[i].width;
            if (width < cellWidth) {
                lineWords.push(lines[i].value);
            }
            else {
                rows.push({
                    value: lineWords.join(' '),
                    y: lineY,
                });
                lineY += lineOffset;
                lineWords = [lines[i].value];
                width = lines[i].width;
            }
        }
        if (lineWords.length) {
            rows.push({
                value: lineWords.join(' '),
                y: lineY,
            });
        }
        return {
            lines: rows,
            offset: fontHeight * (rows.length - 1) + (rows.length - 1) * 4,
        };
    };
    Canvas.prototype.cleanValue = function (value) {
        return (value + '').trim().replace(/\s\s+/g, ' ');
    };
    Canvas.prototype.getFontString = function (cell) {
        return cell.style['font-style'] + " " + cell.style['font-weight'] + " " + cell.style['font-size'] + " \"" + cell.style['font-family'] + "\", sans-serif";
    };
    Canvas.prototype.translateCanvas = function (w) {
        var translate = (w % 2) / 2;
        Grid_1.GridBase.Container.bufferCanvasContext.translate(translate, translate);
        return translate;
    };
    Canvas.prototype.resetCanvas = function (translate) {
        Grid_1.GridBase.Container.bufferCanvasContext.translate(-translate, -translate);
    };
    return Canvas;
}());
exports.default = Canvas;
//# sourceMappingURL=Canvas.js.map