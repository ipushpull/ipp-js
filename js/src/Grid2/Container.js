"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var popper_js_1 = require("popper.js");
var Grid_1 = require("./Grid");
var Scrollbars_1 = require("./Scrollbars");
var Classy_1 = require("../Classy");
var Selector_1 = require("./Selector");
var GridLine_1 = require("./GridLine");
var Container = (function () {
    function Container() {
        var _this = this;
        this.links = {};
        this.lines = {};
        this.images = {};
        this.zIndexes = {
            history: 20,
            button: 10,
            highlight: 300,
            external: 200,
        };
        this.onEvents = function (name, evt) {
            if (name === 'mousedown') {
                if (_this.popper.style.display === 'block' &&
                    !_this.isTarget(evt.target, _this.Selector.element) &&
                    !_this.isTarget(evt.target, _this.popperControl.reference) &&
                    !_this.isTarget(evt.target, _this.popper)) {
                    _this.popper.style.display = 'none';
                }
            }
        };
        this.classy = new Classy_1.default();
    }
    Container.prototype.init = function (id) {
        this.destroy();
        Grid_1.GridBase.Events.register(this.onEvents);
        this.container = document.getElementById(id);
        if (!this.container) {
            throw new Error('Container not found');
        }
        this.container.style.overflow = 'hidden';
        this.containerRect = this.container.getBoundingClientRect();
        this.canvas = document.createElement('canvas');
        this.canvasContext = this.canvas.getContext('2d');
        this.bufferCanvas = document.createElement('canvas');
        this.bufferCanvasContext = this.bufferCanvas.getContext('2d');
        this.container.appendChild(this.canvas);
        this.setSize();
        this.createInputElement();
        if (!Grid_1.GridBase.Settings.touch && !Grid_1.GridBase.Settings.fluid) {
            this.Scrollbars = new Scrollbars_1.Scrollbars();
            this.container.appendChild(this.Scrollbars.colAxis);
            this.container.appendChild(this.Scrollbars.rowAxis);
        }
        this.Selector = new Selector_1.Selector();
        this.Selector.element.appendChild(this.input);
        this.container.appendChild(this.Selector.element);
    };
    Container.prototype.update = function () {
        if (this.Scrollbars) {
            this.Scrollbars.update();
        }
        this.Selector.update();
        if (!Grid_1.GridBase.Content.hasFocus) {
            this.hideInput();
        }
        this.updateLinks();
    };
    Container.prototype.destroy = function () {
        Grid_1.GridBase.Events.unRegister(this.onEvents);
        if (!this.container) {
            return;
        }
        this.container.innerHTML = '';
        if (this.Scrollbars) {
            this.Scrollbars.destroy();
        }
    };
    Container.prototype.showInput = function () {
        var _this = this;
        console.log('showInput');
        var cell = Grid_1.GridBase.Content.getContentCell(Grid_1.GridBase.Content.cellSelection.from.index);
        var cellMeta = Grid_1.GridBase.Content.getContentCellMeta(Grid_1.GridBase.Content.cellSelection.from.index);
        var element = cellMeta.button && cellMeta.button.type ? cellMeta.button.type : 'input';
        if (['select'].indexOf(element) > -1) {
            if (element === 'chat') {
                this.createSelectOptions(['Embed', 'New Window']);
            }
            else {
                this.createSelectOptions(cellMeta.button.options);
            }
            this.popper.style.display = 'block';
            var link = this.getLink('button', cellMeta);
            this.popperControl = new popper_js_1.default(link.element, this.popper, { placement: 'bottom-start' });
            return;
        }
        if (['rotate'].indexOf(element) > -1) {
            return;
        }
        var unit = cell.style['font-size'].indexOf('px') > -1 ? 'px' : 'pt';
        this.input.style.visibility = 'visible';
        var fontSize = Grid_1.GridBase.Content.scaleDimension(parseFloat(cell.style['font-size']));
        this.input.style['font-size'] = "" + fontSize + unit;
        this.input.style['font-family'] = cell.style['font-family'] + ", sans-serif";
        this.input.style['font-weight'] = cell.style['font-weight'];
        this.input.style['text-align'] = cell.style['text-align'];
        this.input.style['visibility'] = 'visible';
        if (Grid_1.GridBase.Content.keyValue !== undefined) {
            this.input.value = Grid_1.GridBase.Content.keyValue;
        }
        if (!Grid_1.GridBase.Settings.touch) {
            var interval_1 = setInterval(function () {
                if (_this.input.offsetParent === null)
                    return;
                clearInterval(interval_1);
                if (Grid_1.GridBase.Content.keyValue === undefined) {
                    _this.input.value = "" + (cell.formatted_value || cell.value);
                }
                _this.input.focus();
                if (element === 'input') {
                    if (Grid_1.GridBase.Content.keyValue !== undefined) {
                        _this.input.selectionStart = 999;
                    }
                    else if (_this.input.value) {
                        _this.input.selectionStart = 0;
                        _this.input.selectionEnd = 999;
                    }
                }
            }, 20);
        }
        else {
            if (Grid_1.GridBase.Content.keyValue === undefined) {
                this.input.value = "" + (cell.formatted_value || cell.value);
            }
            this.input.focus();
            if (element === 'input') {
                if (Grid_1.GridBase.Content.keyValue !== undefined) {
                    this.input.selectionStart = 999;
                }
                else if (this.input.value) {
                    this.input.selectionStart = 0;
                    this.input.selectionEnd = 999;
                }
            }
        }
    };
    Container.prototype.hideInput = function () {
        this.input.blur();
        this.input.style.visibility = 'hidden';
        this.input.value = '';
    };
    Container.prototype.setSize = function () {
        this.containerRect = this.container.getBoundingClientRect();
        this.canvas.width = this.containerRect.width * Grid_1.GridBase.Settings.ratio;
        this.canvas.height = this.containerRect.height * Grid_1.GridBase.Settings.ratio;
        this.canvas.style.width = this.containerRect.width + 'px';
        this.canvas.style.height = this.containerRect.height + 'px';
        this.bufferCanvas.width = this.containerRect.width * Grid_1.GridBase.Settings.ratio;
        this.bufferCanvas.height = this.containerRect.height * Grid_1.GridBase.Settings.ratio;
        this.bufferCanvas.style.width = this.containerRect.width + 'px';
        this.bufferCanvas.style.height = this.containerRect.height + 'px';
    };
    Container.prototype.hasFocus = function (evt) {
        var _this = this;
        var focus = false;
        [this.canvas, this.Selector.element, this.input].forEach2(function (element) {
            if (_this.isTarget(evt.target, element)) {
                focus = true;
            }
        });
        if (!focus) {
            if (evt.target.className && evt.target.className.indexOf && evt.target.className.indexOf('-link') > -1) {
                focus = true;
            }
        }
        if (focus) {
            this.classy.addClass(this.container, 'focus');
        }
        else {
            this.classy.removeClass(this.container, 'focus');
        }
        return focus;
    };
    Container.prototype.addElement = function (element) {
        this.container.appendChild(element);
    };
    Container.prototype.addLink = function (type, options) {
        var key = type + "-" + options.cell.index.row + "-" + options.cell.index.col;
        if (this.links[key]) {
            return;
        }
        var className = "overlay-link " + type + "-link";
        if (type === 'history') {
            className += " user-" + options.user;
        }
        var elOptions = {
            cell: options.cell,
            className: className,
            color: options.color,
        };
        if (options.external) {
            elOptions.href = options.address;
            elOptions.target = '_blank';
        }
        var link = {
            cell: options.cell,
            element: this.createDomElement(type, elOptions),
        };
        this.links[key] = link;
    };
    Container.prototype.getLink = function (type, cell) {
        var key = type + "-" + cell.index.row + "-" + cell.index.col;
        return this.links[key] || null;
    };
    Container.prototype.removeAllLinks = function (type) {
        var _this = this;
        Object.keys(this.links).forEach(function (key) {
            if (type && key.indexOf(type) < 0 || key.indexOf('highlight') > -1)
                return;
            _this.removeLinkByKey(key);
        });
    };
    Container.prototype.removeLink = function (type, cell) {
        var key = type + "-" + cell.index.row + "-" + cell.index.col;
        this.removeLinkByKey(key);
    };
    Container.prototype.removeLinkByKey = function (key) {
        var link = this.links[key];
        if (!link) {
            return;
        }
        if (link.line) {
            link.line.destroy();
        }
        this.removeDomElement(this.links[key].element);
        this.links[key] = undefined;
    };
    Container.prototype.addImage = function (cellMeta) {
        var cell = Grid_1.GridBase.Content.getContentCell(cellMeta.index);
        var key = "image-" + cellMeta.index.row + "-" + cellMeta.index.col;
        var image = this.images[key];
        if (image) {
            if (image.element.src !== cell.value) {
                image.element.src = cell.value;
            }
        }
        else {
            var image_1 = {
                cell: cellMeta,
                element: this.createImageElement(cellMeta, cell),
            };
            this.images[key] = image_1;
            var element = this.createDomElement('image', {
                cell: cellMeta,
                className: 'overlay-link image-link',
            });
            element.appendChild(image_1.element);
            var link = {
                cell: cellMeta,
                element: element,
            };
            this.links[key] = link;
        }
    };
    Container.prototype.removeImage = function (row, col) {
        var key = "image-" + row + "-" + col;
        this.removeLinkByKey(key);
        this.images[key] = undefined;
    };
    Container.prototype.addGridLine = function (axis, cell) {
        if (Grid_1.GridBase.Settings.touch) {
            return;
        }
        var key = "line-" + cell.index.row + "-" + cell.index.col;
        this.removeGridLine(key);
        var line = {
            cell: cell,
            element: this.createDomElement("line-" + axis, {
                cell: cell,
                className: "overlay-link line-link line-" + axis,
            }),
        };
        line.line = new GridLine_1.GridLine(line.element, axis, cell);
        this.links[key] = line;
    };
    Container.prototype.removeGridLine = function (key) {
        if (!this.links[key])
            return;
        this.removeDomElement(this.links[key].element);
        delete this.links[key];
    };
    Container.prototype.createInputElement = function () {
        var _this = this;
        this.input = document.createElement('input');
        this.input.setAttribute('autocapitalize', 'none');
        this.input.setAttribute('list', 'gridlist');
        this.input.style['position'] = 'absolute';
        this.input.style['border'] = '0';
        this.input.style['font-size'] = '12px';
        this.input.style['color'] = 'black';
        this.input.style['background-color'] = 'ivory';
        this.input.style['box-sizing'] = 'border-box';
        this.input.style['margin'] = '0';
        this.input.style['padding'] = '0 2px';
        this.input.style['outline'] = '0';
        this.input.style['left'] = '0px';
        this.input.style['top'] = '0px';
        this.input.style['width'] = '100%';
        this.input.style['text-align'] = 'left';
        this.input.style['z-index'] = 10;
        this.input.style['visibility'] = 'hidden';
        this.input.style['display'] = 'display';
        this.input.style['height'] = '100%';
        this.input.addEventListener('keyup', function (evt) {
            var cell = Grid_1.GridBase.Content.getContentCell(Grid_1.GridBase.Content.cellSelection.from.index);
            if (cell.formatted_value === _this.input.value)
                return;
            Grid_1.GridBase.Content.dirty = true;
            Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_CELL_VALUE, {
                cell: Grid_1.GridBase.Content.cellSelection.from,
                value: "" + _this.input.value,
            });
            cell.formatted_value = _this.input.value;
        });
        this.input.addEventListener('paste', function (evt) {
            evt.stopPropagation();
            var text = '';
            try {
                text = evt.clipboardData.getData('text/plain');
            }
            catch (exception) { }
            try {
                if (!text) {
                    text = window.clipboardData.getData('Text');
                }
            }
            catch (exception) { }
            Grid_1.GridBase.Content.dirty = true;
            Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_CELL_VALUE, { cell: Grid_1.GridBase.Content.cellSelection.from, value: "" + text });
        });
        this.popper = document.createElement('div');
        this.popper.className = 'button-menu';
        this.popper.style['display'] = 'none';
        this.popper.addEventListener('mouseleave', function (evt) {
            _this.popper.style['display'] = 'none';
        });
        this.container.appendChild(this.popper);
    };
    Container.prototype.createSelectOptions = function (options) {
        var _this = this;
        this.popper.innerHTML = '';
        options.forEach(function (value) {
            var option = document.createElement('a');
            option.innerText = value;
            option.addEventListener('click', function () {
                Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_CELL_VALUE, {
                    cell: Grid_1.GridBase.Content.cellSelection.from,
                    value: "" + value,
                });
                _this.popper.style.display = 'none';
                var cell = Grid_1.GridBase.Content.getContentCell(Grid_1.GridBase.Content.cellSelection.from.index);
                cell.formatted_value = value;
            });
            _this.popper.appendChild(option);
        });
    };
    Container.prototype.createImageElement = function (cellMeta, cell) {
        var image = new Image();
        image.id = "image-" + cellMeta.index.row + "-" + cellMeta.index.col;
        image.className = 'img-link';
        image.style.position = 'absolute';
        image.style['z-index'] = 10;
        image.style['left'] = '0';
        image.style['top'] = '0';
        image.style['width'] = '100%';
        image.style['height'] = '100%';
        image.onload = function () {
            image.dataset.height = "" + image.height;
            image.dataset.width = "" + image.width;
        };
        image.src = cell.value;
        return image;
    };
    Container.prototype.createDomElement = function (type, options) {
        var element = document.createElement('a');
        element.dataset.type = type;
        if (options.attrs && Object.keys(options.attrs).length) {
            for (var a in options.attrs) {
                element[a] = options.attrs[a];
            }
        }
        if (type === 'external' && options.href) {
            element.href = options.href;
            element.target = options.target;
        }
        element.className = options.className || '';
        element.style.position = 'absolute';
        element.style['id'] = "overlay-" + options.cell.index.row + "-" + options.cell.index.col;
        element.style['z-index'] = this.zIndexes[type] || 10;
        element.style['top'] = '0';
        element.style['left'] = '0';
        element.style['width'] = options.cell.coords.width + "px";
        element.style['height'] = options.cell.coords.height + "px";
        element.style['display'] = 'block';
        var inner = document.createElement('div');
        inner.className = 'inner-link';
        if (type === 'highlight') {
            inner.style['background-color'] = "#" + options.color;
        }
        if (options.styles && Object.keys(options.styles).length) {
            for (var style in options.styles) {
                inner.style[style] = options.styles[style];
            }
        }
        if (type === 'button') {
            if (options.cell.wrap) {
                inner.style['text-align'] = 'center';
                inner.style['white-space'] = 'normal';
            }
        }
        if (type.indexOf('line') > -1 || type === 'history') {
            var inner2 = document.createElement('div');
            inner2.className = type === 'history' ? 'inner-inner-link' : 'inner-line';
            element.appendChild(inner2);
        }
        element.appendChild(inner);
        return element;
    };
    Container.prototype.removeDomElement = function (element) {
        if (this.container.contains(element)) {
            this.container.removeChild(element);
        }
    };
    Container.prototype.updateDomElement = function (link) {
        var colFreeze = Grid_1.GridBase.Content.getFreezeIndex('col');
        var rowFreeze = Grid_1.GridBase.Content.getFreezeIndex('row');
        var offsetCol = link.cell.index.col < colFreeze ? 0 : Grid_1.GridBase.Content.offsets.colOffset;
        var offsetRow = link.cell.index.row < rowFreeze ? 0 : Grid_1.GridBase.Content.offsets.rowOffset;
        var x = Grid_1.GridBase.Content.scaleDimension(link.cell.coords.x - offsetCol);
        var y = Grid_1.GridBase.Content.scaleDimension(link.cell.coords.y - offsetRow);
        var height = Grid_1.GridBase.Content.scaleDimension(link.cell.coords.height);
        var width = Grid_1.GridBase.Content.scaleDimension(link.cell.coords.width);
        link.element.style.left = x + "px";
        link.element.style.top = y + "px";
        link.element.style.height = height + "px";
        link.element.style.width = width + "px";
        var cell = Grid_1.GridBase.Content.getContentCell(link.cell.index);
        if (!cell) {
            console.log(link.cell.index);
            return;
        }
        var unit = cell.style['font-size'].indexOf('px') > -1 ? 'px' : 'pt';
        var fontSize = Grid_1.GridBase.Content.scaleDimension(parseFloat(cell.style['font-size']));
        link.element.style['font-size'] = "" + fontSize + unit;
        link.element.style['font-family'] = cell.style['font-family'] + ", sans-serif";
        link.element.style['font-weight'] = cell.style['font-weight'];
        var inner = link.element.getElementsByClassName('inner-link')[0];
        if (link.element.dataset.type === 'button') {
            if (link.cell.formatting) {
                inner.style['color'] = link.cell.formatting.color;
                inner.style['background-color'] = link.cell.formatting.background;
            }
            else {
                inner.style['color'] = "#" + cell.style.color;
                inner.style['background-color'] = "#" + cell.style['background-color'];
            }
            inner.innerHTML = cell.formatted_value;
        }
    };
    Container.prototype.isTarget = function (evtTarget, element) {
        var clickedEl = evtTarget;
        while (clickedEl && clickedEl !== element) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === element;
    };
    Container.prototype.updateLinks = function () {
        var _this = this;
        var _loop_1 = function (key) {
            var link = this_1.links[key];
            if (!link) {
                return "continue";
            }
            if (link.element.dataset.type === 'highlight' && !link.element.dataset.active) {
                link.element.dataset.active = '1';
                link.element.classList.add('flash');
                if (Grid_1.GridBase.Content.isVisible(link.cell)) {
                    this_1.updateDomElement(link);
                    this_1.container.appendChild(link.element);
                }
                setTimeout(function () {
                    link.element.classList.add('done');
                    setTimeout(function () {
                        if (_this.container.contains(link.element)) {
                            _this.container.removeChild(link.element);
                        }
                        if (_this.links[key]) {
                            delete _this.links[key];
                        }
                    }, Grid_1.GridBase.Settings.highlightsRemoveTime);
                }, Grid_1.GridBase.Settings.highlightsShowTime);
            }
            else if (Grid_1.GridBase.Content.isVisible(link.cell)) {
                this_1.updateDomElement(link);
                if (!this_1.container.contains(link.element)) {
                    this_1.container.appendChild(link.element);
                }
            }
            else {
                if (this_1.container.contains(link.element)) {
                    this_1.container.removeChild(link.element);
                }
            }
        };
        var this_1 = this;
        for (var key in this.links) {
            _loop_1(key);
        }
    };
    return Container;
}());
exports.default = Container;
//# sourceMappingURL=Container.js.map