"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("underscore");
var Grid_1 = require("./Grid");
var Helpers_1 = require("../Helpers");
var Emitter_1 = require("../Emitter");
var Classy_1 = require("../Classy");
var Range_1 = require("../Page/Range");
var Content = (function (_super) {
    __extends(Content, _super);
    function Content() {
        var _this = _super.call(this) || this;
        _this.originalContent = [];
        _this.content = [];
        _this.contentMeta = [[]];
        _this.dimensions = {
            width: 0,
            height: 0,
            rows: [],
            cols: [],
        };
        _this.offsets = {
            row: 0,
            col: 0,
            rowTo: 0,
            colTo: 0,
            rowMax: 0,
            colMax: 0,
            colOffset: 0,
            rowOffset: 0,
        };
        _this.freezeRange = {
            valid: false,
            height: 0,
            width: 0,
            index: {
                row: 0,
                col: 0,
            },
        };
        _this.headingSelected = '';
        _this.dirty = false;
        _this.hidden = false;
        _this.scrolling = false;
        _this.resizing = false;
        _this.hasFocus = false;
        _this.ctrlDown = false;
        _this.shiftDown = false;
        _this.unshift = false;
        _this.noAccess = false;
        _this.cellSelection = {
            from: undefined,
            to: undefined,
            last: undefined,
            go: false,
            on: false,
            clicked: 0,
            found: undefined,
            multiple: false,
            flipped: false,
            reference: '',
        };
        _this.styles = [];
        _this.buttons = [];
        _this.history = [];
        _this.highlights = [];
        _this.hiddenColumns = [];
        _this.tracking = false;
        _this.borders = {
            widths: {
                none: 0,
                thin: 1,
                medium: 2,
                thick: 3,
            },
            styles: {
                none: 'solid',
                solid: 'solid',
                double: 'double',
            },
            names: {
                t: 'top',
                r: 'right',
                b: 'bottom',
                l: 'left',
            },
        };
        _this._editing = false;
        _this.cellStyles = [
            'background-color',
            'color',
            'font-family',
            'font-size',
            'font-style',
            'font-weight',
            'height',
            'number-format',
            'text-align',
            'text-wrap',
            'width',
            'vertical-align',
        ];
        _this.accessRanges = [];
        _this.found = [];
        _this.foundSelection = {
            query: '',
            count: 0,
            selected: 0,
            cell: undefined,
        };
        _this.scrollbarSize = {
            y: 8,
            x: 8,
        };
        _this.sorting = {};
        _this.filters = {};
        _this.onEvents = function (name, evt) {
            if (!_this.content.length) {
                return;
            }
            if (name === 'keydown') {
                if (evt.key === 'Escape') {
                    _this.dirty = false;
                    _this.clearCellSelection();
                    Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_CELL_RESET, _this.cellSelection.last);
                    return;
                }
            }
            if (name === 'mousemove') {
                _this.updateCellSelection(evt);
            }
            if (!_this.hasFocus) {
                if (name === 'keydown' && evt.key === 'F2' && !evt.shiftKey) {
                    _this.setCellSelection(_this.getContentCellMeta({ row: 0, col: 0 }, true));
                    _this.hasFocus = true;
                }
                return;
            }
            if (name === 'mousedown') {
                _this.selectCell(evt);
            }
            if (name === 'mouseup') {
                _this.onMouseUp(evt);
            }
            if (name === 'keydown') {
                _this.onKeyDown(evt);
            }
            if (name === 'keyup') {
                if (evt.key === 'Control') {
                    _this.ctrlDown = false;
                }
                if (evt.key === 'Shift') {
                    _this.shiftDown = false;
                    _this.unshift = false;
                }
                if (_this.editing)
                    return;
                if (_this.cellSelection.from &&
                    [
                        'Home',
                        'End',
                        'ArrowLeft',
                        'ArrowRight',
                        'ArrowDown',
                        'ArrowUp',
                        'Enter',
                        'Tab',
                        'PageUp',
                        'PageDown',
                    ].indexOf(evt.key) > -1) {
                    Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_CELL_SELECTED, {
                        rightClick: Grid_1.GridBase.Events.rightClick,
                        selection: _this.cellSelection,
                        event: evt,
                    });
                }
            }
        };
        _this.helpers = new Helpers_1.default();
        _this.classy = new Classy_1.default();
        return _this;
    }
    Object.defineProperty(Content.prototype, "editing", {
        get: function () {
            return this._editing;
        },
        set: function (value) {
            this._editing = value;
            Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_EDITING, {
                cell: this.cellSelection.from,
                editing: this._editing,
                dirty: this.dirty,
            });
            if (value) {
                var cell = this.cellSelection.from
                    ? this.getContentCell(this.cellSelection.from.index)
                    : null;
                if (!this.cellSelection.from ||
                    this.cellSelection.from.heading ||
                    this.cellSelection.from.allow !== 'yes' ||
                    (cell && cell.access === 'no') ||
                    !Grid_1.GridBase.Settings.canEdit) {
                    this._editing = false;
                    return;
                }
                this.cellSelection.go = false;
                Grid_1.GridBase.Container.showInput();
            }
            else {
                this.keyValue = undefined;
                Grid_1.GridBase.Container.hideInput();
                if (this.dirty) {
                    this.dirty = false;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Content.prototype.init = function () {
        Grid_1.GridBase.Events.register(this.onEvents);
    };
    Content.prototype.update = function () {
        if (!this.resizing) {
            this.setFit(Grid_1.GridBase.Settings.fit);
        }
        if (this.content.length) {
            this.setAllOffsets();
        }
    };
    Content.prototype.destroy = function () {
        Grid_1.GridBase.Events.unRegister(this.onEvents);
        this.buttons = [];
        this.styles = [];
        this.history = [];
        this.highlights = [];
        this.hiddenColumns = [];
        this.tracking = false;
        this.content = [];
    };
    Content.prototype.setContent = function (content, ranges, userId) {
        if (userId === void 0) { userId = undefined; }
        this.content = content.slice();
        this.contentMeta = [];
        this.offsets.rowTo = 0;
        this.offsets.colTo = 0;
        this.offsets.colMax = 0;
        this.offsets.rowMax = 0;
        this.setRanges(ranges, userId);
        this.updateContent();
    };
    Content.prototype.resetButtons = function () {
    };
    Content.prototype.updateContent = function (refresh) {
        if (!refresh) {
            this.applyFilters();
            this.applySorting();
            if (this.content.length) {
                this.setHeadings();
            }
        }
        if (!this.content.length) {
            return;
        }
        this.setDimensions(refresh);
        this.setCellMetaData(refresh);
        this.setButtons(refresh);
        this.find({ query: this.foundSelection.query });
    };
    Content.prototype.setDeltaContent = function (content) {
        var offset = Grid_1.GridBase.Settings.headings ? 1 : 0;
        for (var rowIndex = 0; rowIndex < content.length; rowIndex++) {
            if (!content[rowIndex] || !this.content[rowIndex + offset])
                continue;
            for (var colIndex = 0; colIndex < content[rowIndex].length; colIndex++) {
                var cell = content[rowIndex][colIndex];
                if (!cell)
                    continue;
                this.content[rowIndex + offset][colIndex + offset] = cell;
            }
            this.updateSoftmerges(rowIndex);
        }
        this.setButtons();
        this.find({ query: this.foundSelection.query });
    };
    Content.prototype.setOffsets = function (row, col, set) {
        if (set === void 0) { set = false; }
        if (set) {
            this.offsets.row = row;
            this.offsets.col = col;
        }
        else {
            this.offsets.row += row;
            this.offsets.col += col;
        }
        this.setAllOffsets();
    };
    Content.prototype.getSize = function () {
        var offset = Grid_1.GridBase.Settings.headings ? 1 : 0;
        var rows = this.contentMeta.length ? this.contentMeta.length - offset : 0;
        var cols = this.contentMeta.length ? this.contentMeta[0].length - offset : 0;
        return {
            rows: rows,
            cols: cols,
        };
    };
    Content.prototype.setSelectorByRef = function (ref) {
        var size = this.getSize();
        if (!size.rows) {
            this.clearCellSelection();
            return;
        }
        var range = this.helpers.cellRange(ref, size.rows, size.cols);
        var fromCellMeta = this.getContentCellMetaByPosition(range.from);
        if (!fromCellMeta) {
            this.clearCellSelection();
            return;
        }
        var toCellMeta = this.getContentCellMetaByPosition(range.to);
        this.cellSelection.from = fromCellMeta;
        this.cellSelection.to = toCellMeta || fromCellMeta;
        this.cellSelection.reference = ref;
    };
    Content.prototype.setSelector = function (from, to) {
        if (!from) {
            this.clearCellSelection();
            return;
        }
        if (!to) {
            to = from;
        }
        var offset = Grid_1.GridBase.Settings.headings ? 1 : 0;
        var toRow = to.row < 0 ? this.content.length - 1 : to.row + offset;
        var toCol = to.col < 0 ? this.content.length[0] - 1 : to.col + offset;
        if (!this.contentMeta[from.row + offset] ||
            !this.contentMeta[from.row + offset][from.col + offset] ||
            !this.contentMeta[toRow] ||
            !this.contentMeta[toRow][toCol]) {
            this.clearCellSelection();
            return;
        }
        this.cellSelection.from = this.contentMeta[from.row + offset][from.col + offset];
        this.cellSelection.to = this.contentMeta[toRow][toCol];
        this.keepSelectorWithinView();
    };
    Content.prototype.setColSize = function (headingCell, value) {
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var cell = this.content[rowIndex][headingCell.index.col];
            var cellMeta = this.contentMeta[rowIndex][headingCell.index.row];
            cell.style.width = value + "px";
            cellMeta.coords.width = value;
        }
        this.updateContent(true);
        this.update();
    };
    Content.prototype.setRowSize = function (headingCell, value) {
        for (var colIndex = 0; colIndex < this.content[headingCell.index.row].length; colIndex++) {
            var cell = this.content[headingCell.index.row][colIndex];
            var cellMeta = this.contentMeta[headingCell.index.row][colIndex];
            cell.style.height = value + "px";
            cellMeta.coords.height = value;
        }
        this.updateContent(true);
        this.update();
    };
    Content.prototype.keepSelectorWithinView = function () {
        if (!this.cellSelection.from || (this.cellSelection.from === this.cellSelection.to && this.cellSelection.go)) {
            return;
        }
        var rect = Grid_1.GridBase.Container.containerRect;
        var colOffset = 0;
        var rowOffset = 0;
        var set = false;
        var setRow = false;
        var setCol = false;
        var cell = this.scaleCellCoords(this.cellSelection.to || this.cellSelection.from);
        if (this.cellSelection.to &&
            (this.cellSelection.from.index.col > this.cellSelection.to.index.col ||
                this.cellSelection.from.index.row > this.cellSelection.to.index.row)) {
            var from = __assign({}, this.cellSelection.from.index);
            var to = __assign({}, this.cellSelection.to.index);
            if (to.row < from.row) {
                from.row = to.row;
                to.row = this.cellSelection.from.index.row;
            }
            if (to.col < from.col) {
                from.col = to.col;
                to.col = this.cellSelection.from.index.col;
            }
            this.cellSelection.flipped = true;
            this.cellSelection.flippedFrom = from;
            this.cellSelection.flippedTo = to;
        }
        if (cell.scaledCoords.x - this.scaleDimension(this.offsets.colOffset) > Grid_1.GridBase.Container.containerRect.width ||
            cell.scaledCoords.x - this.scaleDimension(this.offsets.colOffset) < 0) {
            for (var colIndex = 0; colIndex < this.dimensions.cols.length; colIndex++) {
                if (this.scaleDimension(this.dimensions.cols[colIndex] - this.offsets.colOffset) >
                    cell.scaledCoords.x +
                        cell.scaledCoords.width -
                        this.scaleDimension(this.offsets.colOffset) -
                        Grid_1.GridBase.Container.containerRect.width) {
                    colOffset = colIndex;
                    break;
                }
            }
            setCol = true;
        }
        if (cell.scaledCoords.y - this.scaleDimension(this.offsets.rowOffset) > Grid_1.GridBase.Container.containerRect.height ||
            cell.scaledCoords.y - this.scaleDimension(this.offsets.rowOffset) < 0) {
            for (var rowIndex = 0; rowIndex < this.dimensions.rows.length; rowIndex++) {
                if (this.scaleDimension(this.dimensions.rows[rowIndex] - this.offsets.rowOffset) >
                    cell.scaledCoords.y +
                        cell.scaledCoords.height -
                        this.scaleDimension(this.offsets.rowOffset) -
                        Grid_1.GridBase.Container.containerRect.height) {
                    rowOffset = rowIndex;
                    break;
                }
            }
            setRow = true;
        }
        if (!setRow && setCol && cell.index.col < this.getFreezeIndex('col')) {
            return;
        }
        if (!setCol && setRow && cell.index.row <= this.getFreezeIndex('row')) {
            return;
        }
        if (setRow || setCol) {
            this.setOffsets(setRow ? rowOffset : this.offsets.row, setCol ? colOffset : this.offsets.col, true);
            return;
        }
        if (this.scaleDimension(cell.coords.x - this.offsets.colOffset) < this.dimensions.cols[this.getFreezeIndex('col')]) {
            colOffset = -1;
        }
        else if (this.scaleDimension(cell.coords.x + cell.coords.width - this.offsets.colOffset) > rect.width - 8) {
            colOffset = 1;
        }
        if (this.scaleDimension(cell.coords.y - this.offsets.rowOffset) < this.dimensions.rows[this.getFreezeIndex('row')]) {
            rowOffset = -1;
        }
        else if (this.scaleDimension(cell.coords.y + cell.coords.height - this.offsets.rowOffset) > rect.height - 8) {
            rowOffset = 1;
        }
        this.setOffsets(rowOffset, colOffset, set);
    };
    Content.prototype.setFit = function (value) {
        if (!/(scroll|width|height|contain)/.test(value)) {
            return;
        }
        Grid_1.GridBase.Settings.set('fit', value);
        var scrollbarWidth = Grid_1.GridBase.Settings.scrollbarWidth;
        switch (value) {
            case 'scroll':
                this.setScale(1);
                break;
            case 'width':
                this.setScale((Grid_1.GridBase.Container.containerRect.width - scrollbarWidth) / this.dimensions.width);
                break;
            case 'height':
                this.setScale((Grid_1.GridBase.Container.containerRect.height - scrollbarWidth) / this.dimensions.height);
                break;
            case 'contain':
                var w = Grid_1.GridBase.Container.containerRect.width / this.dimensions.width;
                var h = Grid_1.GridBase.Container.containerRect.height / this.dimensions.height;
                this.setScale(h < w ? h : w);
                break;
        }
    };
    Content.prototype.setScale = function (n) {
        Grid_1.GridBase.Settings.scale = n * Grid_1.GridBase.Settings.ratio;
    };
    Content.prototype.scaleDimension = function (num) {
        return (num * 1 * Grid_1.GridBase.Settings.scale) / Grid_1.GridBase.Settings.ratio;
    };
    Content.prototype.setFreeze = function (row, col) {
        if (row === void 0) { row = 0; }
        if (col === void 0) { col = 0; }
        this.freezeRange.index.row = row < 0 ? 0 : row;
        this.freezeRange.index.col = col < 0 ? 0 : col;
    };
    Content.prototype.setHeadings = function () {
        this.content = this.content.slice();
        this.contentMeta = [];
        if (!Grid_1.GridBase.Settings.headings) {
            return;
        }
        var row = this.createHeadings('row');
        row.unshift(this.createHeadings('corner'));
        this.content.unshift(row);
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            if (!rowIndex) {
                continue;
            }
            this.content[rowIndex] = this.content[rowIndex].slice();
            this.content[rowIndex].unshift(this.createHeadings('col', rowIndex));
        }
    };
    Content.prototype.getFreezeIndex = function (which) {
        return Grid_1.GridBase.Settings.headings ? this.freezeRange.index[which] + 1 : this.freezeRange.index[which];
    };
    Content.prototype.getFreezeSize = function (which, headings) {
        var index = this.getFreezeIndex(which);
        var size = Grid_1.GridBase.Settings.headings && headings ? (which === 'row' ? 20 : 40) : 0;
        return this.dimensions[which + "s"][index] - size;
    };
    Content.prototype.updateSoftmerges = function (row) {
        var rowIndex = Grid_1.GridBase.Settings.headings ? row + 1 : row;
        this.setCellMetaByRow(this.content[rowIndex], rowIndex);
    };
    Content.prototype.isVisible = function (cell) {
        var colFreeze = this.getFreezeIndex('col');
        var rowFreeze = this.getFreezeIndex('row');
        var colFrom = Grid_1.GridBase.Settings.headings ? 1 : 0;
        if (cell.heading) {
            if (cell.heading === 'col') {
                return cell.index.row >= this.offsets.row && cell.index.row <= this.offsets.rowTo;
            }
            else if (cell.heading === 'row') {
                return cell.index.col >= this.offsets.col && cell.index.col <= this.offsets.colTo;
            }
            return;
        }
        if (cell.index.row < rowFreeze && cell.index.col < colFreeze) {
            return true;
        }
        if (cell.index.row - this.offsets.row < rowFreeze && cell.position.row >= this.freezeRange.index.row) {
            return false;
        }
        if (cell.index.col - this.offsets.col < colFreeze && cell.position.col >= this.freezeRange.index.col) {
            return false;
        }
        if (cell.position.row < this.freezeRange.index.row) {
            return true;
        }
        if (cell.position.col < this.freezeRange.index.col &&
            ((cell.index.row >= this.offsets.row && cell.index.row <= this.offsets.rowTo) ||
                cell.position.row < this.freezeRange.index.row)) {
            return true;
        }
        return (cell.index.row >= this.offsets.row &&
            cell.index.row <= this.offsets.rowTo &&
            cell.index.col >= this.offsets.col &&
            cell.index.col <= this.offsets.colTo);
    };
    Content.prototype.find = function (data) {
        this.clearFound();
        var value = data.query;
        if (!value) {
            return;
        }
        this.foundSelection.query = value;
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                var cellMeta = this.getContentCellMeta({ row: rowIndex, col: colIndex });
                var regex = new RegExp(value, 'ig');
                if (cellMeta.heading ||
                    cellMeta.allow === 'no' ||
                    (!regex.test(col.value) && !regex.test(col.formatted_value))) {
                    continue;
                }
                if (!this.found[rowIndex]) {
                    this.found[rowIndex] = [];
                }
                this.found[rowIndex][colIndex] = col;
                this.foundSelection.count++;
                Grid_1.GridBase.Container.addLink('found', {
                    cell: cellMeta,
                });
            }
        }
    };
    Content.prototype.clearFound = function () {
        Grid_1.GridBase.Container.removeAllLinks('found');
        this.found = [];
        this.foundSelection.count = 0;
        this.foundSelection.selected = 0;
        this.foundSelection.query = '';
    };
    Content.prototype.getContentHtml = function () {
        var html = "<table style=\"border-collapse: collapse;\">";
        var offset = Grid_1.GridBase.Settings.headings ? 1 : 0;
        for (var rowIndex = offset; rowIndex < this.content.length; rowIndex++) {
            var row = this.content[rowIndex];
            if (this.cellSelection.from &&
                (rowIndex < this.cellSelection.from.index.row || rowIndex > this.cellSelection.to.index.row)) {
                continue;
            }
            html += "<tr>";
            for (var colIndex = offset; colIndex < row.length; colIndex++) {
                var col = row[colIndex];
                if (this.cellSelection.from &&
                    this.cellSelection.to &&
                    (colIndex < this.cellSelection.from.index.col || colIndex > this.cellSelection.to.index.col)) {
                    continue;
                }
                var cell = this.content[rowIndex][colIndex];
                var styles = this.flattenStyles(cell.style);
                html += "<td style=\"" + styles + "\">" + (col.allow === 'no' ? '' : cell.formatted_value) + "</td>";
            }
            html += "</tr>";
        }
        html += "</table>";
        return html;
    };
    Content.prototype.setHiddenColumns = function (data) {
        this.hiddenColumns =
            data && data.length
                ? data.sort(function (a, b) {
                    return b - a;
                })
                : [];
    };
    Content.prototype.cellHistory = function (data) {
        var _this = this;
        this.history = [];
        if (!data.length || !Grid_1.GridBase.Settings.tracking) {
            return;
        }
        var users = [];
        var count = 0;
        data.forEach2(function (push) {
            count++;
            if (count === 1) {
                return;
            }
            if (users.indexOf(push.modified_by.id) === -1) {
                users.push(push.modified_by.id);
            }
            var userIndex = users.indexOf(push.modified_by.id);
            push.content_diff.forEach2(function (row, rowIndex) {
                if (!row) {
                    return;
                }
                if (!_this.history[rowIndex]) {
                    _this.history[rowIndex] = [];
                }
                row.forEach2(function (cellData, colIndex) {
                    if (!cellData) {
                        return;
                    }
                    _this.history[rowIndex][colIndex] = userIndex;
                });
            });
        });
    };
    Content.prototype.cellHighlights = function (data) {
        this.highlights = [];
        if (!data.content_diff || !data.content_diff.length) {
            return;
        }
        for (var rowIndex = 0; rowIndex < data.content_diff.length; rowIndex++) {
            var row = data.content_diff[rowIndex];
            if (!row)
                continue;
            if (!this.highlights[rowIndex]) {
                this.highlights[rowIndex] = [];
            }
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                if (!row[colIndex])
                    continue;
                this.highlights[rowIndex][colIndex] = true;
            }
        }
    };
    Content.prototype.clearFilters = function () {
        this.filters = {};
    };
    Content.prototype.setFilters = function (filters) {
        var _this = this;
        this.clearFilters();
        if (filters.col >= 0) {
            filters = [filters];
        }
        filters.forEach2(function (filter) {
            var key = "col-" + filter.col + "-" + filter.exp;
            if (!filter.value || filter.remove) {
                if (_this.filters["col" + filter.col]) {
                    delete _this.filters[key];
                }
                return;
            }
            if (!filter.type)
                filter.type = 'number';
            filter.type = /(number|string|date)/.test(filter.type) ? filter.type : 'number';
            if (!filter.exp ||
                (filter.type === 'number' && ['>', '<', '<=', '>=', '==', '===', '!='].indexOf(filter.exp) === -1)) {
                filter.exp = '==';
            }
            _this.filters[key] = filter;
        });
    };
    Content.prototype.clearSorting = function () {
        this.sorting = {};
    };
    Content.prototype.setSorting = function (filters) {
        var _this = this;
        this.clearSorting();
        if (filters.col >= 0) {
            filters = [filters];
        }
        filters.forEach2(function (filter) {
            if (filter.remove) {
                if (_this.filters["col" + filter.col]) {
                    delete _this.filters["col" + filter.col];
                }
                return;
            }
            if (!filter.direction)
                filter.direction = 'ASC';
            filter.direction = ("" + filter.direction).toUpperCase();
            filter.direction = /(ASC|DESC)/.test(filter.direction) ? filter.direction : 'ASC';
            filter.col = filter.col * 1 || 0;
            filter.type = /(number|string|date)/.test(filter.type) ? filter.type : 'number';
            _this.sorting["col" + filter.col] = filter;
        });
    };
    Content.prototype.clearRanges = function () {
        this.accessRanges = [];
    };
    Content.prototype.setRanges = function (ranges, userId) {
        if (userId === void 0) { userId = undefined; }
        this.clearRanges();
        if (!ranges || !ranges.length)
            return;
        var offset = Grid_1.GridBase.Settings.headings ? 1 : 0;
        for (var i = 0; i < ranges.length; i++) {
            var range = ranges[i];
            var rowEnd = void 0;
            var colEnd = void 0;
            if (range.range) {
                rowEnd = _.clone(range.range.to.row);
                colEnd = _.clone(range.range.to.col);
                if (rowEnd === -1) {
                    rowEnd = this.content.length - 1;
                }
                if (colEnd === -1) {
                    colEnd = this.content[0].length - 1;
                }
            }
            if (range instanceof Range_1.PermissionRange && this.content.length) {
                var userRight = range.getPermission(userId || 0) || (userId ? 'yes' : 'no');
                var rowEnd_1 = _.clone(range.rowEnd);
                var colEnd_1 = _.clone(range.colEnd);
                if (rowEnd_1 === -1) {
                    rowEnd_1 = this.content.length - 1;
                }
                else {
                    rowEnd_1 += offset;
                }
                if (colEnd_1 === -1) {
                    colEnd_1 = this.content[0].length - 1;
                }
                else {
                    colEnd_1 += offset;
                }
                for (var i_1 = range.rowStart + offset; i_1 <= rowEnd_1; i_1++) {
                    for (var k = range.colStart + offset; k <= colEnd_1; k++) {
                        if (!this.accessRanges[i_1]) {
                            this.accessRanges[i_1] = [];
                        }
                        if (!this.accessRanges[i_1][k]) {
                            this.accessRanges[i_1][k] = [];
                        }
                        if (userRight) {
                            this.accessRanges[i_1][k].push(userRight);
                        }
                    }
                }
            }
        }
    };
    Content.prototype.getStyle = function (position) {
        if (!position || !this.styles[position.row] || !this.styles[position.row][position.col])
            return null;
        return this.styles[position.row][position.col];
    };
    Content.prototype.setDimensions = function (refresh) {
        var _this = this;
        this.dimensions = {
            width: 0,
            height: 0,
            rows: [],
            cols: [],
        };
        var width = 0;
        var height = 0;
        var colOffset = Grid_1.GridBase.Settings.headings ? 1 : 0;
        if (!refresh) {
            this.hiddenColumns.forEach(function (colIndex) {
                for (var rowIndex = 0; rowIndex < _this.content.length; rowIndex++) {
                    _this.content[rowIndex].splice(colIndex + colOffset, 1);
                }
            });
        }
        for (var colIndex = 0; colIndex < this.content[0].length; colIndex++) {
            this.dimensions.cols.push(this.dimensions.width);
            width = parseFloat(this.content[0][colIndex].style.width);
            this.dimensions.width += width;
        }
        this.dimensions.cols.push(this.dimensions.width);
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            this.dimensions.rows.push(this.dimensions.height);
            height = parseFloat(this.content[rowIndex][0].style.height);
            this.dimensions.height += height;
        }
        this.dimensions.rows.push(this.dimensions.height);
    };
    Content.prototype.setAllOffsets = function () {
        this.offsets.colMax = 0;
        this.offsets.rowMax = 0;
        var width = Grid_1.GridBase.Settings.headings ? this.scaleDimension(40) : Grid_1.GridBase.Container.containerRect.width;
        var height = Grid_1.GridBase.Settings.headings ? this.scaleDimension(20) : Grid_1.GridBase.Container.containerRect.height;
        if (Grid_1.GridBase.Settings.headings) {
            this.offsets.colMax = this.dimensions.cols.length - 1 - this.getFreezeIndex('col') - 1;
            this.offsets.rowMax = this.dimensions.rows.length - 1 - this.getFreezeIndex('row') - 1;
        }
        else {
            var scrollbarX = Grid_1.GridBase.Settings.fit === 'height' ||
                Grid_1.GridBase.Settings.fit === 'contain' ||
                Grid_1.GridBase.Container.containerRect.height > this.scaleDimension(this.dimensions.height)
                ? 0
                : Grid_1.GridBase.Settings.scrollbarWidth;
            var scrollbarY = Grid_1.GridBase.Settings.fit === 'width' ||
                Grid_1.GridBase.Settings.fit === 'contain' ||
                Grid_1.GridBase.Container.containerRect.width > this.scaleDimension(this.dimensions.width)
                ? 0
                : Grid_1.GridBase.Settings.scrollbarWidth;
            if (!scrollbarY) {
                this.offsets.colMax = 0;
            }
            else {
                var offsetWidth = this.scaleDimension(this.dimensions.width) - Grid_1.GridBase.Container.containerRect.width + scrollbarX;
                for (var colIndex = 0; colIndex < this.dimensions.cols.length; colIndex++) {
                    if (this.scaleDimension(this.dimensions.cols[colIndex]) > offsetWidth) {
                        this.offsets.colMax = colIndex;
                        break;
                    }
                }
            }
            if (!scrollbarX) {
                this.offsets.rowMax = 0;
            }
            else {
                var offsetHeight = this.scaleDimension(this.dimensions.height) - Grid_1.GridBase.Container.containerRect.height + scrollbarY;
                for (var rowIndex = 0; rowIndex < this.dimensions.rows.length; rowIndex++) {
                    if (this.scaleDimension(this.dimensions.rows[rowIndex]) > offsetHeight) {
                        this.offsets.rowMax = rowIndex;
                        break;
                    }
                }
            }
        }
        if (this.offsets.colMax < 0)
            this.offsets.colMax = 0;
        if (this.offsets.rowMax < 0)
            this.offsets.rowMax = 0;
        if (this.offsets.row < 0)
            this.offsets.row = 0;
        if (this.offsets.col < 0)
            this.offsets.col = 0;
        if (this.offsets.col < 0) {
            this.offsets.col = 0;
        }
        else if (this.offsets.col > this.offsets.colMax) {
            this.offsets.col = this.offsets.colMax;
        }
        if (this.offsets.row < 0) {
            this.offsets.row = 0;
        }
        else if (this.offsets.row > this.offsets.rowMax) {
            this.offsets.row = this.offsets.rowMax;
        }
        this.calcOffset();
        this.offsets.rowTo = this.content.length - 1;
        for (var rowIndex = this.offsets.row; rowIndex < this.dimensions.rows.length; rowIndex++) {
            if (this.scaleDimension(this.dimensions.rows[rowIndex] - this.dimensions.rows[this.offsets.row]) >
                Grid_1.GridBase.Container.containerRect.height) {
                this.offsets.rowTo = rowIndex + this.getFreezeIndex('row');
                break;
            }
        }
        this.offsets.colTo = this.content[0].length - 1;
        for (var colIndex = this.offsets.col; colIndex < this.dimensions.cols.length; colIndex++) {
            if (this.dimensions.cols[this.offsets.col + 1]
                && this.scaleDimension(this.dimensions.cols[colIndex] - this.dimensions.cols[this.offsets.col + 1]) > Grid_1.GridBase.Container.containerRect.width) {
                this.offsets.colTo = colIndex + this.getFreezeIndex('col');
                break;
            }
        }
    };
    Content.prototype.calcOffset = function () {
        var offsetColIndex = this.offsets.col + this.getFreezeIndex('col');
        if (offsetColIndex >= this.dimensions.cols.length)
            offsetColIndex = this.dimensions.cols.length - 1;
        var offsetRowIndex = this.offsets.row + this.getFreezeIndex('row');
        if (offsetRowIndex >= this.dimensions.rows.length)
            offsetRowIndex = this.dimensions.rows.length - 1;
        var offsetCol = this.dimensions.cols[offsetColIndex];
        var offsetRow = this.dimensions.rows[offsetRowIndex];
        offsetCol -= this.dimensions.cols[this.getFreezeIndex('col')] || 0;
        offsetRow -= this.dimensions.rows[this.getFreezeIndex('row')] || 0;
        if (!Grid_1.GridBase.Settings.headings) {
        }
        this.offsets.colOffset = offsetCol;
        this.offsets.rowOffset = offsetRow;
    };
    Content.prototype.setCellMetaData = function (refresh) {
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            this.setCellMetaByRow(this.content[rowIndex], rowIndex, refresh);
        }
    };
    Content.prototype.setCellMetaByRow = function (dataRow, dataRowIndex, refresh) {
        var _this = this;
        if (dataRow === void 0) { dataRow = []; }
        if (dataRowIndex === void 0) { dataRowIndex = -1; }
        if (!refresh) {
            this.contentMeta[dataRowIndex] = Array.from(Array(dataRow.length), function () { return new CellMeta(); });
        }
        dataRow.forEach2(function (col, colIndex) {
            if (!col)
                return;
            var cellMeta = _this.contentMeta[dataRowIndex][colIndex];
            cellMeta.link = col.link;
            cellMeta.index = {
                col: colIndex,
                row: dataRowIndex,
            };
            cellMeta.position = {
                col: col.index.col,
                row: col.index.row,
            };
            cellMeta.reference = "" + _this.helpers.toColumnName(col.index.col + 1) + (col.index.row + 1);
            var width = parseFloat(col.style.width);
            var wrap = _this.doesWrap(col);
            if ((col.formatted_value || col.value) && !wrap) {
                Grid_1.GridBase.Container.canvasContext.font = _this.getFontString(col);
                var textWidth = Grid_1.GridBase.Container.canvasContext.measureText(_this.cleanValue(col.formatted_value || col.value)).width + 3;
                if (textWidth > width) {
                    var merge = _this.getTextOverlayWidth(dataRowIndex, colIndex, width, textWidth);
                    cellMeta.merge = merge;
                }
            }
            cellMeta.wrap = wrap;
            var height = parseFloat(col.style.height);
            cellMeta.coords = new CellMetaCoords({
                x: _this.dimensions.cols[colIndex],
                y: _this.dimensions.rows[dataRowIndex],
                width: cellMeta.merge ? cellMeta.merge.width : width,
                height: height,
                columnWidth: width,
            });
            if (Grid_1.GridBase.Settings.headings && !refresh) {
                if (!dataRowIndex || !colIndex) {
                    var headingType = !dataRowIndex && !colIndex ? 'corner' : !dataRowIndex ? 'row' : 'col';
                    cellMeta.heading = headingType;
                    if (!dataRowIndex && colIndex) {
                        Grid_1.GridBase.Container.addGridLine('col', cellMeta);
                    }
                    else if (!colIndex && dataRowIndex) {
                        Grid_1.GridBase.Container.addGridLine('row', cellMeta);
                    }
                }
            }
            if (_this.accessRanges[dataRowIndex] && _this.accessRanges[dataRowIndex][colIndex]) {
                _this.accessRanges[dataRowIndex][colIndex].forEach2(function (element) {
                    cellMeta.allow = element;
                });
            }
            if (!refresh) {
                if (!cellMeta.heading &&
                    _this.found[cellMeta.position.row] &&
                    _this.found[cellMeta.position.row][cellMeta.position.col]) {
                    Grid_1.GridBase.Container.addLink('found', {
                        cell: cellMeta,
                    });
                }
                if (!cellMeta.heading &&
                    _this.history[cellMeta.position.row] &&
                    _this.history[cellMeta.position.row][cellMeta.position.col] >= 0) {
                    Grid_1.GridBase.Container.addLink('history', {
                        cell: cellMeta,
                        user: _this.history[cellMeta.position.row][cellMeta.position.col],
                    });
                }
                if (!cellMeta.heading &&
                    _this.highlights[cellMeta.position.row] &&
                    _this.highlights[cellMeta.position.row][cellMeta.position.col] >= 0) {
                    Grid_1.GridBase.Container.removeLink('highlight', cellMeta);
                    Grid_1.GridBase.Container.addLink('highlight', {
                        cell: cellMeta,
                        color: _this.helpers.getContrastYIQ(col.style['background-color']) === 'light' ? '000000' : 'FFFFFF',
                    });
                    delete _this.highlights[cellMeta.position.row][cellMeta.position.col];
                }
                if (("" + col.value).indexOf('data:image') === 0) {
                    Grid_1.GridBase.Container.removeImage(dataRowIndex, colIndex);
                    cellMeta.image = true;
                    Grid_1.GridBase.Container.addImage(cellMeta);
                }
                if (!cellMeta.heading && col.link) {
                    Grid_1.GridBase.Container.removeLink('external', cellMeta);
                    Grid_1.GridBase.Container.addLink('external', {
                        address: col.link.address,
                        external: col.link.external,
                        cell: cellMeta,
                    });
                }
            }
        });
    };
    Content.prototype.setButtons = function (refresh) {
        var _this = this;
        var offset = Grid_1.GridBase.Settings.headings ? 1 : 0;
        var colOffset = 0;
        var size = this.getSize();
        this.buttons.forEach(function (button) {
            if (!size.rows)
                return;
            var range = _this.helpers.cellRange(button.range, size.rows, size.cols);
            var cellTo = _this.getContentCellMetaByPosition(range.to);
            if (!cellTo)
                return;
            for (var colIndex = range.from.col; colIndex <= cellTo.position.col; colIndex++) {
                if (_this.hiddenColumns.indexOf(colIndex) > -1)
                    continue;
                for (var rowIndex = range.from.row; rowIndex <= cellTo.position.row; rowIndex++) {
                    var cellMeta = _this.getContentCellMetaByPosition({ row: rowIndex, col: colIndex });
                    if (!cellMeta)
                        continue;
                    var cell = _this.getContentCell({ row: cellMeta.index.row, col: cellMeta.index.col });
                    Grid_1.GridBase.Container.removeLink('button', cellMeta);
                    if (cell.access !== 'no' && cellMeta.allow === 'yes') {
                        cellMeta.button = button;
                        Grid_1.GridBase.Container.addLink('button', {
                            cell: cellMeta,
                            className: 'button-link',
                        });
                    }
                }
            }
        });
    };
    Content.prototype.applyFilters = function () {
        if (!Object.keys(this.filters).length) {
            return false;
        }
        var rowsToRemove = [];
        for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            if (rowIndex < this.freezeRange.index.row) {
                continue;
            }
            var _loop_1 = function (f) {
                var filter = this_1.filters[f];
                var col = this_1.content[rowIndex][filter.col];
                if (!col)
                    return "continue";
                var colValue = filter.type === 'number' ? parseFloat(col.value) : "" + col.formatted_value;
                var filterValue = filter.type === 'number' ? parseFloat(filter.value) : "" + filter.value;
                var validValue = false;
                switch (filter.type) {
                    case 'number':
                        validValue = eval(colValue + " " + filter.exp + " " + filterValue);
                        break;
                    case 'date':
                        var dates = filterValue.split(',');
                        var fromDate = new Date(dates[0]).toISOString();
                        var toDate = new Date(dates[1]).toISOString();
                        var date = new Date(this_1.helpers.parseDateExcel(colValue)).toISOString();
                        validValue = date >= fromDate && date <= toDate;
                        break;
                    default:
                        filterValue.split(',').forEach2(function (value) {
                            if (validValue) {
                                return;
                            }
                            switch (filter.exp) {
                                case 'contains':
                                    validValue = colValue.toLowerCase().indexOf(value.toLowerCase()) > -1;
                                    break;
                                case 'starts_with':
                                    validValue = colValue.toLowerCase().substring(0, value.length) === value.toLowerCase();
                                    break;
                                case '!=':
                                    validValue = colValue != value;
                                    break;
                                default:
                                    validValue = colValue == value;
                                    break;
                            }
                        });
                        break;
                }
                if (!validValue && rowsToRemove.indexOf(rowIndex) < 0)
                    rowsToRemove.push(rowIndex);
            };
            var this_1 = this;
            for (var f in this.filters) {
                _loop_1(f);
            }
        }
        for (var i = rowsToRemove.length - 1; i >= 0; i--)
            this.content.splice(rowsToRemove[i], 1);
    };
    Content.prototype.applySorting = function () {
        if (!Object.keys(this.sorting).length) {
            return false;
        }
        var key = Object.keys(this.sorting)[0];
        var filter = this.sorting[key];
        var data = this.freezeRange.index.row
            ? this.content.slice(this.freezeRange.index.row, this.content.length)
            : this.content.slice(0);
        var exp = filter.direction === 'DESC' ? '>' : '<';
        var opp = filter.direction === 'DESC' ? '<' : '>';
        data.sort(function (a, b) {
            var col = filter.col;
            if (!a || !b || !a[col] || !b[col]) {
                return 1;
            }
            var aVal = filter.type === 'number' ? parseFloat(a[col].value) || 0 : "\"" + a[col].value + "\"";
            var bVal = filter.type === 'number' ? parseFloat(b[col].value) || 0 : "\"" + b[col].value + "\"";
            if (eval(aVal + " " + exp + " " + bVal)) {
                return -1;
            }
            if (eval(aVal + " " + opp + " " + bVal)) {
                return 1;
            }
            return 0;
        });
        this.content = this.freezeRange.index.row ? this.content.slice(0, this.freezeRange.index.row).concat(data) : data;
    };
    Content.prototype.getFontString = function (col) {
        return col.style['font-style'] + " " + col.style['font-weight'] + " " + col.style['font-size'] + " \"" + col.style['font-family'] + "\", sans-serif";
    };
    Content.prototype.getTextOverlayWidth = function (row, col, width, textWidth) {
        var cell = this.content[row][col];
        var textAlign = cell.style['text-align'];
        var left = 0;
        var right = 0;
        switch (textAlign) {
            case 'right':
                left = -1;
                break;
            case 'center':
                left = -1;
                right = 1;
                break;
            default:
                right = 1;
                break;
        }
        if (!this.content[row][col + right] || !this.content[row][col + left]) {
            return {
                col: col,
                width: width,
            };
        }
        var endCol = col;
        var startCol = col;
        if (right > 0) {
            for (var i = col + 1; i < this.content[row].length; i++) {
                if (this.content[row][i].formatted_value ||
                    "" + this.content[row][i].value ||
                    this.content[row][i].allow === 'no') {
                    break;
                }
                width += parseFloat(this.content[row][i].style.width);
                endCol = i;
                this.contentMeta[row][i].hidden = true;
                if (width > textWidth) {
                    break;
                }
            }
        }
        if (left < 0) {
        }
        return {
            left: left,
            right: right,
            endCol: endCol,
            startCol: startCol,
            width: width,
        };
    };
    Content.prototype.cleanValue = function (value) {
        return (value + '').trim().replace(/\s\s+/g, ' ');
    };
    Content.prototype.createHeadings = function (which, index) {
        if (which === 'row') {
            var row = JSON.parse(JSON.stringify(this.content[0]));
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                row[colIndex] = this.setHeadingCellStyle(row[colIndex], 'col');
                row[colIndex].value = row[colIndex].formatted_value = this.helpers.toColumnName(colIndex + 1);
            }
            return row;
        }
        if (which === 'col') {
            var cell = JSON.parse(JSON.stringify(this.content[index][0]));
            cell = this.setHeadingCellStyle(cell, 'row');
            cell.value = cell.formatted_value = cell.index.row + 1;
            return cell;
        }
        if (which === 'corner') {
            var cell = JSON.parse(JSON.stringify(this.content[0][0]));
            cell = this.setHeadingCellStyle(cell, 'corner');
            cell.value = cell.formatted_value = '';
            return cell;
        }
    };
    Content.prototype.setHeadingCellStyle = function (cell, pos) {
        cell.access = null;
        cell.link = null;
        cell.style['text-align'] = 'center';
        cell.style['background-color'] = Grid_1.GridBase.Settings.contrast === 'light' ? 'e3e3e4' : '333333';
        if (pos === 'corner' || pos === 'row') {
            cell.style['width'] = '40px';
        }
        if (pos === 'corner' || pos === 'col') {
            cell.style['height'] = '20px';
        }
        cell.style['font-family'] = 'Calibri';
        cell.style['font-size'] = '9pt';
        cell.style['font-weight'] = 'normal';
        cell.style['font-style'] = 'normal';
        cell.style['color'] = Grid_1.GridBase.Settings.contrast === 'light' ? '000000' : '999999';
        ['b', 't', 'r', 'l'].forEach(function (d) {
            cell.style[d + "bc"] = Grid_1.GridBase.Settings.contrast === 'light' ? '999999' : '000000';
            cell.style[d + "bs"] = 'solid';
            cell.style[d + "bw"] = 'thin';
        });
        return cell;
    };
    Content.prototype.doesWrap = function (col) {
        if ((col.style['text-wrap'] && col.style['text-wrap'] === 'wrap') || col.style['word-wrap'] === 'break-word') {
            return true;
        }
        return false;
    };
    Content.prototype.getCell = function (x, y) {
        var colFreeze = this.getFreezeIndex('col');
        var rowFreeze = this.getFreezeIndex('row');
        var rect = Grid_1.GridBase.Container.containerRect;
        var offsetX = x - rect.left;
        var offsetY = y - rect.top;
        x -= rect.left - this.scaleDimension(this.offsets.colOffset);
        y -= rect.top - this.scaleDimension(this.offsets.rowOffset);
        var cell;
        for (var rowIndex = 0; rowIndex < this.dimensions.rows.length - 1; rowIndex++) {
            for (var colIndex = 0; colIndex < this.dimensions.cols.length - 1; colIndex++) {
                var top_1 = this.scaleDimension(this.dimensions.rows[rowIndex]);
                var left = this.scaleDimension(this.dimensions.cols[colIndex]);
                var cellMeta = this.contentMeta[rowIndex][colIndex];
                var xCol = x;
                var yRow = y;
                if (rowIndex < rowFreeze) {
                    yRow = offsetY;
                }
                if (colIndex < colFreeze) {
                    xCol = offsetX;
                }
                if (xCol >= left &&
                    yRow >= top_1 &&
                    xCol <= this.scaleDimension(cellMeta.coords.x + cellMeta.coords.columnWidth) &&
                    yRow <= this.scaleDimension(cellMeta.coords.y + cellMeta.coords.height)) {
                    cell = cellMeta;
                    break;
                }
            }
            if (cell)
                break;
        }
        return cell;
    };
    Content.prototype.getContentCell = function (index, headings) {
        var offset = Grid_1.GridBase.Settings.headings && headings ? 1 : 0;
        return this.content[index.row + offset] && this.content[index.row + offset][index.col + offset]
            ? this.content[index.row + offset][index.col + offset]
            : null;
    };
    Content.prototype.getContentCellMeta = function (index, headings) {
        var offset = Grid_1.GridBase.Settings.headings && headings ? 1 : 0;
        var row = index.row + offset;
        var col = index.col + offset;
        if (index.row < 0)
            row = this.contentMeta.length - 1;
        if (index.col < 0)
            col = this.contentMeta[0].length - 1;
        return this.contentMeta[row] && this.contentMeta[row][col] ? this.contentMeta[row][col] : null;
    };
    Content.prototype.getContentCellMetaByPosition = function (position) {
        if (!this.contentMeta.length || !this.contentMeta[0].length) {
            return null;
        }
        var offset = Grid_1.GridBase.Settings.headings ? 1 : 0;
        var index = __assign({}, position);
        if (index.row < 0 && index.col < 0) {
            return this.contentMeta[this.contentMeta.length - 1][this.contentMeta[0].length - 1];
        }
        if (index.row < 0) {
            var rowPosition = 0;
            for (var rowIndex = 0; rowIndex < this.contentMeta.length; rowIndex++) {
                if (!this.contentMeta[rowIndex])
                    continue;
                var cell = this.contentMeta[rowIndex][0];
                if (cell.position.row > rowPosition) {
                    rowPosition = cell.position.row;
                }
            }
            index.row = rowPosition;
        }
        if (index.col < 0) {
            var cell = this.contentMeta[0][this.contentMeta[0].length - 1];
            index.col = cell.position.col;
        }
        var foundCellMeta;
        for (var rowIndex = 0; rowIndex < this.contentMeta.length; rowIndex++) {
            var row = this.contentMeta[rowIndex];
            if (!row)
                continue;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var cellMeta = row[colIndex];
                if (!cellMeta || cellMeta.heading)
                    continue;
                if (cellMeta.position.row === index.row && cellMeta.position.col === index.col) {
                    foundCellMeta = cellMeta;
                    break;
                }
            }
            if (foundCellMeta)
                break;
        }
        return foundCellMeta;
    };
    Content.prototype.getPageOffset = function (direction) {
        var rowOffset = 0;
        var colOffset = 0;
        var selectionOffset = 0;
        if (direction === 'down') {
            rowOffset = this.content.length - 1;
            for (var rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
                if (this.scaleDimension(this.dimensions.rows[rowIndex] - this.offsets.rowOffset) >
                    Grid_1.GridBase.Container.containerRect.height) {
                    rowOffset = rowIndex - this.getFreezeIndex('row') - 1;
                    selectionOffset = this.getFreezeIndex('row');
                    break;
                }
            }
        }
        else if (direction === 'up') {
            selectionOffset = this.getFreezeIndex('row');
            for (var rowIndex = this.content.length - 1; rowIndex >= 0; rowIndex--) {
                if (this.scaleDimension(this.offsets.rowOffset - this.dimensions.rows[rowIndex]) >
                    Grid_1.GridBase.Container.containerRect.height) {
                    rowOffset = rowIndex + this.getFreezeIndex('row') + 1;
                    break;
                }
            }
        }
        else if (direction === 'right') {
            colOffset = this.content[0].length - 1;
            for (var colIndex = 0; colIndex < this.content[0].length; colIndex++) {
                if (this.scaleDimension(this.dimensions.cols[colIndex] - this.offsets.colOffset) >
                    Grid_1.GridBase.Container.containerRect.width) {
                    colOffset = colIndex - this.getFreezeIndex('col') - 1;
                    selectionOffset = this.getFreezeIndex('col');
                    break;
                }
            }
        }
        else if (direction === 'left') {
            selectionOffset = this.getFreezeIndex('col');
            for (var colIndex = this.content[0].length - 1; colIndex >= 0; colIndex--) {
                if (this.scaleDimension(this.offsets.colOffset - this.dimensions.cols[colIndex]) >
                    Grid_1.GridBase.Container.containerRect.width) {
                    colOffset = colIndex + this.getFreezeIndex('col') + 1;
                    break;
                }
            }
        }
        return {
            row: rowOffset,
            col: colOffset,
            selection: selectionOffset,
        };
    };
    Content.prototype.getCellsByIndex = function (from, to, flat) {
        var cells = [];
        var row = 0;
        var col = 0;
        for (var rowIndex = from.row; rowIndex <= to.row; rowIndex++) {
            if (!flat) {
                cells[row] = [];
            }
            col = 0;
            for (var colIndex = from.col; colIndex <= to.col; colIndex++) {
                var cellMeta = this.contentMeta[rowIndex][colIndex];
                if (flat) {
                    cells.push(cellMeta);
                }
                else {
                    cells[row][col] = cellMeta;
                }
                col++;
            }
            row++;
        }
        return cells;
    };
    Content.prototype.onKeyDown = function (evt) {
        if (evt.key === 'Control') {
            this.ctrlDown = true;
            return;
        }
        if (evt.key === 'Shift') {
            this.shiftDown = true;
        }
        var moved = this.onCtrlNavigate(evt.key);
        if (moved) {
            evt.preventDefault();
            return;
        }
        if (['Home', 'End'].indexOf(evt.key) > -1 && this.cellSelection.from) {
            if (this.editing)
                return;
            var offset = Grid_1.GridBase.Settings.headings ? 1 : 0;
            this.setCellSelection(this.getContentCellMeta({ row: this.cellSelection.from.index.row, col: evt.key === 'Home' ? offset : -1 }));
            evt.preventDefault();
            return;
        }
        if (['ArrowLeft', 'ArrowUp', 'ArrowRight', 'ArrowDown'].indexOf(evt.key) > -1 && this.cellSelection.from) {
            if (this.editing && this.keyValue !== undefined) {
                this.editing = false;
                this.selectNextCell(this.cellSelection.from || this.cellSelection.last || this.getContentCellMeta({ row: 0, col: 0 }), evt.key.replace('Arrow', '').toLowerCase());
                return;
            }
            if (this.editing) {
                return;
            }
            var direction = evt.key.replace('Arrow', '').toLocaleLowerCase();
            this.selectNextCell(this.shiftDown ? this.cellSelection.to : this.cellSelection.from, direction);
            evt.preventDefault();
            return;
        }
        if (['PageUp', 'PageDown'].indexOf(evt.key) > -1 && this.cellSelection.from) {
            if (this.editing)
                return;
            var direction = evt.key.replace('Page', '').toLocaleLowerCase();
            var offsets = this.getPageOffset(direction);
            this.setOffsets(offsets.row, this.offsets.col, true);
            this.setCellSelection(this.getContentCellMeta({
                row: offsets.row + offsets.selection,
                col: this.shiftDown ? this.cellSelection.to.index.col : this.cellSelection.from.index.col,
            }));
            evt.preventDefault();
            return;
        }
        if (evt.key === 'Enter') {
            this.editing = false;
            this.unshift = true;
            this.selectNextCell(this.cellSelection.from || this.cellSelection.last || this.getContentCellMeta({ row: 0, col: 0 }), this.shiftDown ? 'up' : 'down');
            return;
        }
        if (evt.key === 'Tab' && this.cellSelection.from) {
            this.editing = false;
            this.unshift = true;
            this.selectNextCell(this.cellSelection.from, this.shiftDown ? 'left' : 'right');
            evt.preventDefault();
            return;
        }
        if (!this.editing &&
            this.cellSelection &&
            this.cellSelection.from &&
            !this.cellSelection.from.heading &&
            !Grid_1.GridBase.Settings.disallowSelection &&
            Grid_1.GridBase.Settings.canEdit) {
            if (evt.key === 'Delete') {
                Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_DELETE, {
                    selection: this.cellSelection,
                    event: evt,
                });
                return;
            }
            if (this.cellSelection.from !== this.cellSelection.to) {
                return;
            }
            if (evt.key === 'F2' && !evt.shiftKey) {
                this.setCellSelection(this.cellSelection.from || this.getContentCellMeta({ row: 0, col: 0 }, true), 0, true);
                return;
            }
            if (this.cellSelection.from && !this.ctrlDown) {
                if (evt.key.length === 1 && /[a-zA-Z0-9-_ ]/.test(evt.key)) {
                    this.keyValue = evt.key;
                }
                if (evt.key === 'Backspace') {
                    this.keyValue = '';
                }
                if (this.keyValue !== undefined) {
                    this.cellSelection.on = true;
                    if (!this.editing) {
                        this.setCellSelection(this.cellSelection.from, 0, true);
                    }
                    this.ctrlDown = false;
                }
            }
        }
        else if (this.editing) {
            this.dirty = true;
        }
    };
    Content.prototype.onCtrlNavigate = function (which) {
        if (this.editing ||
            ['Home', 'End', 'ArrowLeft', 'ArrowRight', 'ArrowDown', 'ArrowUp'].indexOf(which) < 0 ||
            !this.ctrlDown ||
            !this.cellSelection.from ||
            Grid_1.GridBase.Settings.disallowSelection) {
            return false;
        }
        if (which === 'Home') {
            var offset = Grid_1.GridBase.Settings.headings ? -1 : 0;
            this.setCellSelection(this.getContentCellMeta({ row: this.getFreezeIndex('row') + offset, col: this.getFreezeIndex('col') + offset }, true));
            return true;
        }
        if (which === 'End') {
            this.setCellSelection(this.getContentCellMeta({ row: -1, col: -1 }));
            return true;
        }
        var x = 0;
        var y = 0;
        var lookForEmptyValue = true;
        var refCell = this.shiftDown ? this.cellSelection.to : this.cellSelection.from;
        if (which === 'ArrowRight') {
            if (this.shiftDown) {
                this.setCellSelection(this.getContentCellMeta({ row: refCell.index.row, col: -1 }));
                return true;
            }
            for (var colIndex = refCell.index.col + 1; colIndex < this.content[0].length; colIndex++) {
                var cell = this.content[refCell.index.row][colIndex];
                if (colIndex === refCell.index.col + 1 && !cell.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell.value && lookForEmptyValue) {
                    x = colIndex - 1;
                    break;
                }
                if (cell.value && !lookForEmptyValue) {
                    x = colIndex;
                    break;
                }
            }
            if (!x) {
                x = this.content[0].length - 1;
            }
            this.setCellSelection(this.getContentCellMeta({ row: refCell.index.row, col: x }));
        }
        else if (which === 'ArrowLeft') {
            if (this.shiftDown) {
                this.setCellSelection(this.getContentCellMeta({ row: refCell.index.row, col: 0 }));
                return true;
            }
            for (var colIndex = refCell.index.col - 1; colIndex >= 0; colIndex--) {
                var cell = this.content[refCell.index.row][colIndex];
                if (colIndex === refCell.index.col - 1 && !cell.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell.value && lookForEmptyValue) {
                    x = colIndex + 1;
                    break;
                }
                if (cell.value && !lookForEmptyValue) {
                    x = colIndex;
                    break;
                }
            }
            if (!x && Grid_1.GridBase.Settings.headings) {
                x = 1;
            }
            this.setCellSelection(this.getContentCellMeta({ row: refCell.index.row, col: x }));
        }
        else if (which === 'ArrowDown') {
            if (this.shiftDown) {
                this.setCellSelection(this.getContentCellMeta({ row: -1, col: refCell.index.col }));
                return true;
            }
            for (var rowIndex = refCell.index.row + 1; rowIndex < this.content.length; rowIndex++) {
                var cell = this.content[rowIndex][refCell.index.col];
                if (rowIndex === refCell.index.row + 1 && !cell.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell.value && lookForEmptyValue) {
                    y = rowIndex - 1;
                    break;
                }
                if (cell.value && !lookForEmptyValue) {
                    y = rowIndex;
                    break;
                }
            }
            if (!y) {
                y = this.content.length - 1;
            }
            this.setCellSelection(this.getContentCellMeta({ row: y, col: refCell.index.col }));
        }
        else if (which === 'ArrowUp') {
            if (this.shiftDown) {
                this.setCellSelection(this.getContentCellMeta({ row: 0, col: refCell.index.col }));
                return true;
            }
            for (var rowIndex = refCell.index.row - 1; rowIndex >= 0; rowIndex--) {
                var cell = this.content[rowIndex][refCell.index.col];
                if (rowIndex === refCell.index.row - 1 && !cell.value) {
                    lookForEmptyValue = false;
                    continue;
                }
                if (!cell.value && lookForEmptyValue) {
                    y = rowIndex + 1;
                    break;
                }
                if (cell.value && !lookForEmptyValue) {
                    y = rowIndex;
                    break;
                }
            }
            if (!y && Grid_1.GridBase.Settings.headings)
                y = 1;
            this.setCellSelection(this.getContentCellMeta({ row: y, col: refCell.index.col }));
        }
        return true;
    };
    Content.prototype.onMouseUp = function (evt) {
        this.cellSelection.go = false;
        if (!this.cellSelection.from) {
            return;
        }
        if (this.cellSelection.flipped) {
            this.cellSelection.from = this.contentMeta[this.cellSelection.flippedFrom.row][this.cellSelection.flippedFrom.col];
            this.cellSelection.to = this.contentMeta[this.cellSelection.flippedTo.row][this.cellSelection.flippedTo.col];
            this.cellSelection.flipped = false;
        }
        if (this.cellSelection.from === this.cellSelection.to && !Grid_1.GridBase.Events.rightClick) {
            if (this.cellSelection.from.button.type === 'rotate') {
                var cell = this.getContentCell(this.cellSelection.from.index);
                var value = this.helpers.getNextValueInArray(cell.formatted_value, this.cellSelection.from.button.options);
                Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_CELL_VALUE, {
                    cell: this.cellSelection.from,
                    value: value,
                });
                cell.formatted_value = value;
            }
            if (this.cellSelection.from.button.type === 'select') {
                Grid_1.GridBase.Container.showInput();
            }
        }
        if (this.headingSelected) {
            this.selectHeadingCell(evt);
            return;
        }
        this.cellSelection.from = this.scaleCellCoords(this.cellSelection.from);
        this.cellSelection.to = this.scaleCellCoords(this.cellSelection.to);
        if (this.cellSelection.from === this.cellSelection.to) {
            this.cellSelection.multiple = true;
        }
        this.cellSelection.reference = this.getCellsReference();
        Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_CELL_SELECTED, {
            rightClick: Grid_1.GridBase.Events.rightClick,
            selection: this.cellSelection,
            event: evt,
        });
    };
    Content.prototype.getCellsReference = function (headingSelected) {
        return this.helpers.getCellsReference(this.cellSelection.from.position, this.cellSelection.to.position, headingSelected);
    };
    Content.prototype.scaleCellCoords = function (cell) {
        cell.scaledCoords = {
            x: (cell.coords.x * Grid_1.GridBase.Settings.scale) / Grid_1.GridBase.Settings.ratio,
            y: (cell.coords.y * Grid_1.GridBase.Settings.scale) / Grid_1.GridBase.Settings.ratio,
            width: (cell.coords.columnWidth * Grid_1.GridBase.Settings.scale) / Grid_1.GridBase.Settings.ratio,
            height: (cell.coords.height * Grid_1.GridBase.Settings.scale) / Grid_1.GridBase.Settings.ratio,
        };
        return cell;
    };
    Content.prototype.selectHeadingCell = function (evt) {
        var maxRow = 0;
        var maxCol = 0;
        var indexFromRow = 0;
        var indexToRow = 0;
        var indexFromCol = 0;
        var indexToCol = 0;
        for (var rowIndex = this.content.length - 1; rowIndex >= 0; rowIndex--) {
            var row = this.content[rowIndex];
            if (!row)
                continue;
            if (!maxRow) {
                maxRow = rowIndex;
            }
            var col = this.contentMeta[rowIndex][0];
            if (this.cellSelection.from.index.row === col.index.row) {
                indexFromRow = rowIndex;
            }
            if (this.cellSelection.to.index.row === col.index.row) {
                indexToRow = rowIndex;
            }
        }
        for (var colIndex = this.content[0].length - 1; colIndex >= 0; colIndex--) {
            var col = this.contentMeta[0][colIndex];
            if (!col)
                continue;
            if (!maxCol) {
                maxCol = colIndex;
            }
            if (this.cellSelection.from.index.col === col.index.col) {
                indexFromCol = colIndex;
            }
            if (this.cellSelection.to.index.col === col.index.col) {
                indexToCol = colIndex;
            }
        }
        if (this.headingSelected === 'all') {
            this.cellSelection.from = this.contentMeta[0][0];
            this.cellSelection.to = this.contentMeta[maxRow][maxCol];
        }
        else if (this.headingSelected === 'col') {
            this.cellSelection.from = this.contentMeta[0][indexFromCol];
            this.cellSelection.to = this.contentMeta[maxRow][indexToCol];
        }
        else {
            this.cellSelection.from = this.contentMeta[indexFromRow][0];
            this.cellSelection.to = this.contentMeta[indexToRow][maxCol];
        }
        this.cellSelection.from = this.scaleCellCoords(this.cellSelection.from);
        this.cellSelection.to = this.scaleCellCoords(this.cellSelection.to);
        this.cellSelection.reference = this.getCellsReference(this.headingSelected);
        var n = {
            rightClick: Grid_1.GridBase.Events.rightClick,
            which: this.headingSelected,
            selection: this.cellSelection,
            event: evt,
            count: this.headingSelected === 'all'
                ? -1
                : this.cellSelection.to.index[this.headingSelected] - this.cellSelection.from.index[this.headingSelected] + 1,
        };
        Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_HEADING_CLICKED, n);
        this.headingSelected = '';
    };
    Content.prototype.selectCell = function (evt) {
        this.cellSelection.go = false;
        if (evt.target === Grid_1.GridBase.Container.input) {
            return;
        }
        if (evt.touches) {
            if (evt.touches.length > 1) {
                return;
            }
            evt.clientX = evt.touches[0].pageX;
            evt.clientY = evt.touches[0].pageY;
        }
        var cellMeta = this.getCell(evt.clientX, evt.clientY);
        if (!cellMeta) {
            this.clearCellSelection(true);
            Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_NO_CELL);
            return;
        }
        if (Grid_1.GridBase.Events.rightClick) {
            Grid_1.GridBase.Events.preventContextMenu = true;
        }
        if (this.editing) {
            this.editing = false;
        }
        if (cellMeta.heading) {
            if (!cellMeta.index.row && !cellMeta.index.col) {
                this.headingSelected = 'all';
            }
            else if (!cellMeta.index.row) {
                this.headingSelected = 'col';
            }
            else if (!cellMeta.index.col) {
                this.headingSelected = 'row';
            }
            if (Grid_1.GridBase.Events.rightClick) {
                if (this.cellSelection.from) {
                    if (this.headingSelected === 'col' &&
                        cellMeta.index.col >= this.cellSelection.from.index.col &&
                        cellMeta.index.col <= this.cellSelection.to.index.col) {
                        return;
                    }
                    if (this.headingSelected === 'row' &&
                        cellMeta.index.row >= this.cellSelection.from.index.row &&
                        cellMeta.index.row <= this.cellSelection.to.index.row) {
                        return;
                    }
                }
            }
        }
        var d = new Date();
        if (!Grid_1.GridBase.Events.rightClick &&
            this.cellSelection.clicked &&
            this.cellSelection.from === this.cellSelection.to &&
            d.getTime() - this.cellSelection.clicked <= 300 &&
            !cellMeta.button) {
            Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_CELL_DOUBLE_CLICKED, { cell: cellMeta, event: evt });
            this.cellSelection.clicked = d.getTime();
            this.keyValue = undefined;
            this.editing = true;
            if (Grid_1.GridBase.Settings.touch) {
                evt.stopPropagation();
                evt.preventDefault();
            }
            return;
        }
        if (Grid_1.GridBase.Events.rightClick &&
            this.cellSelection.from &&
            cellMeta.index.row >= this.cellSelection.from.index.row &&
            cellMeta.index.col >= this.cellSelection.from.index.col &&
            cellMeta.index.col <= this.cellSelection.to.index.col &&
            cellMeta.index.row <= this.cellSelection.to.index.row) {
            return;
        }
        this.setCellSelection(cellMeta, d.getTime());
        Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_CELL_MOUSEDOWN, { cell: cellMeta, event: evt });
    };
    Content.prototype.selectNextCell = function (cell, direction) {
        if (direction === void 0) { direction = ''; }
        var row = cell.index.row;
        var col = cell.index.col;
        if (direction === 'left') {
            col = col - 1 < 0 ? 0 : col - 1;
        }
        if (direction === 'up') {
            row = row - 1 < 0 ? 0 : row - 1;
        }
        if (direction === 'down') {
            row = row + 1;
        }
        if (direction === 'right') {
            col = col + 1;
        }
        if (Grid_1.GridBase.Settings.headings) {
            if (!row)
                row = 1;
            if (!col)
                col = 1;
        }
        var cellMeta = this.getContentCellMeta({ row: row, col: col });
        if (cellMeta) {
            this.setCellSelection(cellMeta);
        }
    };
    Content.prototype.setCellSelection = function (cellMeta, time, editing) {
        if (editing === void 0) { editing = false; }
        if (editing) {
            if (cellMeta.allow !== 'yes') {
                this.editing = false;
                return;
            }
            else {
            }
            if (this.keyValue !== undefined) {
                this.dirty = true;
                Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_CELL_VALUE, {
                    cell: this.cellSelection.from,
                    value: this.keyValue,
                });
                var cell_1 = Grid_1.GridBase.Content.getContentCell(Grid_1.GridBase.Content.cellSelection.from.index);
                cell_1.formatted_value = this.keyValue;
            }
        }
        var cell = this.content[cellMeta.index.row][cellMeta.index.col];
        this.cellSelection.clicked = time;
        if (!this.shiftDown || this.unshift) {
            if (this.cellSelection.from) {
                this.cellSelection.last = this.cellSelection.from;
            }
            this.cellSelection.from = cellMeta;
        }
        this.cellSelection.to = cellMeta;
        this.cellSelection.on = false;
        if (editing) {
            this.editing = true;
        }
        if (time) {
            this.cellSelection.go = true;
        }
        if (!Grid_1.GridBase.Events.rightClick) {
            this.cellSelection.multiple = false;
        }
        this.cellSelection.reference = this.getCellsReference();
    };
    Content.prototype.clearCellSelection = function (hasFocus) {
        if (hasFocus === void 0) { hasFocus = false; }
        if (this.editing) {
            this.editing = false;
        }
        this.cellSelection.last = this.cellSelection.from;
        this.cellSelection.from = undefined;
        this.cellSelection.to = undefined;
        this.cellSelection.go = false;
        this.cellSelection.reference = '';
        this.hasFocus = hasFocus;
    };
    Content.prototype.updateCellSelection = function (evt) {
        var cellMeta = this.getCell(evt.clientX, evt.clientY);
        if (!cellMeta) {
            this.noAccess = false;
            return;
        }
        if (this.cellSelection.go) {
            this.cellSelection.to = cellMeta;
        }
        var cell = this.getContentCell(cellMeta.index);
        this.noAccess = cell.access === 'no' || cellMeta.allow === 'no';
        if (this.noAccess) {
            this.classy.addClass(Grid_1.GridBase.Container.container, 'denied');
        }
        else {
            this.classy.removeClass(Grid_1.GridBase.Container.container, 'denied');
        }
    };
    Content.prototype.flattenStyles = function (styles) {
        var htmlStyle = {};
        this.cellStyles.forEach(function (s) {
            htmlStyle[s] = styles[s];
        });
        for (var key in this.borders.names) {
            var name_1 = this.borders.names[key];
            var width = this.borders.widths[styles[key + "bw"]] + 'px';
            var style = styles[key + "bs"];
            var color = '#' + styles[key + "bc"].replace('#', '');
            if (style === "none")
                continue;
            htmlStyle["border-" + name_1] = width + " " + style + " " + color;
        }
        var str = '';
        for (var attr in htmlStyle) {
            var value = htmlStyle[attr];
            if ((attr === 'background-color' || attr === 'color') && value.indexOf('#') === -1) {
                value = "#" + value;
            }
            str += attr + ": " + value + "; ";
        }
        return str;
    };
    return Content;
}(Emitter_1.default));
var CellMeta = (function () {
    function CellMeta(data) {
        var _this = this;
        this.allow = 'yes';
        this.button = false;
        this.hidden = false;
        this.heading = false;
        this.history = false;
        this.link = false;
        this.merge = null;
        this.coords = null;
        this.index = null;
        this.position = null;
        this.reference = '';
        this.wrap = false;
        this._formatting = null;
        if (!data) {
            return;
        }
        Object.keys(data).forEach(function (key) {
            var value = data[key];
            if (key === 'formatting') {
                key = '_formatting';
            }
            _this[key] = value;
        });
    }
    Object.defineProperty(CellMeta.prototype, "formatting", {
        get: function () {
            return this.position ? Grid_1.GridBase.Content.getStyle(this.position) : null;
        },
        set: function (value) {
            this._formatting = value;
        },
        enumerable: true,
        configurable: true
    });
    return CellMeta;
}());
exports.CellMeta = CellMeta;
var CellMetaCoords = (function () {
    function CellMetaCoords(data) {
        var _this = this;
        Object.keys(data).forEach(function (key) {
            var value = data[key];
            if (key === 'columnWidth') {
                key = '_columnWidth';
            }
            _this[key] = value;
        });
    }
    Object.defineProperty(CellMetaCoords.prototype, "columnWidth", {
        get: function () {
            return !isNaN(this._columnWidth) ? this._columnWidth : this.width;
        },
        set: function (value) {
            this._columnWidth = value;
        },
        enumerable: true,
        configurable: true
    });
    return CellMetaCoords;
}());
exports.CellMetaCoords = CellMetaCoords;
exports.default = Content;
//# sourceMappingURL=Content.js.map