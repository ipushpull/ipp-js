"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Hammer = require("hammerjs");
var Emitter_1 = require("../Emitter");
var Grid_1 = require("./Grid");
var Events = (function (_super) {
    __extends(Events, _super);
    function Events() {
        var _this = _super.call(this) || this;
        _this.rightClick = false;
        _this.preventContextMenu = false;
        _this.keydown = false;
        _this.blur = false;
        _this.freeScroll = {
            go: false,
            startX: 0,
            startY: 0,
        };
        _this.time = 0;
        _this.onVisibilityEvent = function (evt) {
            Grid_1.GridBase.Content.hidden = document.hidden;
            if (!Grid_1.GridBase.Content.hidden) {
                _this.emit('visibility');
            }
        };
        _this.onFocus = function (evt) {
            console.warn('focus/blur', document.hasFocus());
        };
        _this.onBlur = function (evt) {
            _this.blur = true;
            console.warn('focus/blur', document.hasFocus());
        };
        _this.onResize = function (evt) {
            if (_this.resizeTimer) {
                clearTimeout(_this.resizeTimer);
                _this.resizeTimer = null;
            }
            _this.resizeTimer = setTimeout(function () {
                _this.emit('resize');
            }, 300);
        };
        _this.step = function () {
            if (!Grid_1.GridBase.Content.editing && !Grid_1.GridBase.Settings.touch) {
                Grid_1.GridBase.Content.keepSelectorWithinView();
                Grid_1.GridBase.Canvas.update();
                Grid_1.GridBase.Container.update();
            }
            _this.keydown = false;
        };
        _this.onKeydown = function (evt) {
            if (_this.keydown)
                return;
            _this.keydown = true;
            _this.emit('keydown', evt);
            window.requestAnimationFrame(_this.step);
            evt.stopPropagation();
        };
        _this.onKeyup = function (evt) {
            console.log('onKeyUp', evt);
            _this.time = 0;
            _this.emit('keyup', evt);
            if (Grid_1.GridBase.Content.editing)
                return;
            Grid_1.GridBase.Content.keepSelectorWithinView();
            Grid_1.GridBase.Canvas.update();
            Grid_1.GridBase.Container.update();
        };
        _this.onMouseOut = function (evt) {
            console.warn('onMouseOut');
        };
        _this.onMouseDown = function (evt) {
            var timer = 0;
            Grid_1.GridBase.Content.hasFocus = Grid_1.GridBase.Container.hasFocus(evt);
            Grid_1.GridBase.Content.ctrlDown = false;
            Grid_1.GridBase.Content.shiftDown = false;
            _this.rightClick = _this.isRightClick(evt);
            _this.emit('mousedown', evt);
            console.log('onMouseDown', evt);
            Grid_1.GridBase.Canvas.update();
            console.log('focus', Grid_1.GridBase.Content.hasFocus, 'go', Grid_1.GridBase.Content.cellSelection.go);
            Grid_1.GridBase.Container.update();
            if (Grid_1.GridBase.Content.hasFocus && evt.target !== Grid_1.GridBase.Container.input) {
                if (!_this.blur)
                    evt.preventDefault();
            }
            _this.blur = false;
        };
        _this.onMouseOver = function (evt) {
            _this.emit('mouseover', evt);
            if (!Grid_1.GridBase.Content.content.length)
                return;
            Grid_1.GridBase.Canvas.update();
            Grid_1.GridBase.Container.update();
        };
        _this.onMouseUp = function (evt) {
            _this.emit('mouseup', evt);
            if (evt.which === 2) {
                if (_this.freeScroll.go) {
                    _this.freeScroll.go = false;
                }
                else {
                    _this.freeScroll = {
                        go: true,
                        startX: evt.clientX,
                        startY: evt.clientY,
                    };
                }
            }
            Grid_1.GridBase.Container.update();
            Grid_1.GridBase.Canvas.update();
            _this.rightClick = false;
        };
        _this.onMouseMove = function (evt) {
            _this.emit('mousemove', evt);
            if (_this.freeScroll.go) {
                var x = Math.round((evt.clientX - _this.freeScroll.startX) / 20);
                var y = Math.round((evt.clientY - _this.freeScroll.startY) / 20);
                Grid_1.GridBase.Content.setOffsets(y, x);
                Grid_1.GridBase.Container.update();
                Grid_1.GridBase.Canvas.update();
            }
            else if (Grid_1.GridBase.Content.scrolling || Grid_1.GridBase.Content.resizing) {
                Grid_1.GridBase.Container.update();
                Grid_1.GridBase.Canvas.update();
            }
            else if (Grid_1.GridBase.Content.cellSelection.go) {
                Grid_1.GridBase.Content.keepSelectorWithinView();
                Grid_1.GridBase.Container.update();
                Grid_1.GridBase.Canvas.update();
            }
        };
        _this.onWheelEvent = function (evt) {
            if (!Grid_1.GridBase.Content.content.length) {
                return;
            }
            var currentOffsets = __assign({}, Grid_1.GridBase.Content.offsets);
            var offsetX = evt.deltaX ? (Math.abs(evt.deltaX) >= 100 ? Math.round(Math.abs(evt.deltaX) / 100) : 1) : 0;
            var offsetY = evt.deltaY ? (Math.abs(evt.deltaY) >= 100 ? Math.round(Math.abs(evt.deltaY) / 100) : 1) : 0;
            var directionX = evt.deltaX > 0 ? 1 : -1;
            var directionY = evt.deltaY > 0 ? 1 : -1;
            offsetX *= directionX;
            offsetY *= directionY;
            if (!Grid_1.GridBase.Content.offsets.rowMax) {
                offsetX = offsetY;
            }
            Grid_1.GridBase.Content.setOffsets(offsetY, offsetX);
            Grid_1.GridBase.Canvas.update();
            Grid_1.GridBase.Container.update();
            if (currentOffsets.row !== Grid_1.GridBase.Content.offsets.row || currentOffsets.col !== Grid_1.GridBase.Content.offsets.col) {
                evt.preventDefault();
            }
        };
        return _this;
    }
    Events.prototype.init = function () {
        var _this = this;
        var interval = setInterval(function () {
            if (!Grid_1.GridBase.Container.container.contains(Grid_1.GridBase.Container.canvas)) {
                return;
            }
            clearInterval(interval);
            interval = undefined;
            document.addEventListener('visibilitychange', _this.onVisibilityEvent, false);
            window.addEventListener('focus', _this.onFocus, false);
            window.addEventListener('resize', _this.onResize);
            window.addEventListener('keydown', _this.onKeydown, false);
            window.addEventListener('keyup', _this.onKeyup, false);
            window.addEventListener('blur', _this.onBlur, false);
            if (Grid_1.GridBase.Settings.touch) {
                _this.setupTouch();
                Grid_1.GridBase.Container.container.addEventListener('touchstart', _this.onMouseDown, false);
                Grid_1.GridBase.Container.container.addEventListener('touchend', _this.onMouseUp, false);
            }
            else {
                Grid_1.GridBase.Container.container.addEventListener('wheel', _this.onWheelEvent, false);
                Grid_1.GridBase.Container.container.addEventListener('mouseover', _this.onMouseOver, false);
                Grid_1.GridBase.Container.container.addEventListener('mouseleave', _this.onMouseOut, false);
                window.addEventListener('mousemove', _this.onMouseMove, false);
                window.addEventListener('mouseup', _this.onMouseUp, false);
                window.addEventListener('mousedown', _this.onMouseDown, false);
            }
            window.oncontextmenu = function () {
                if (_this.preventContextMenu) {
                    _this.preventContextMenu = false;
                    return false;
                }
            };
        }, 50);
    };
    Events.prototype.destroy = function () {
        document.removeEventListener('visibilitychange', this.onVisibilityEvent, false);
        window.removeEventListener('focus', this.onFocus, false);
        window.removeEventListener('reszie', this.onResize);
        window.removeEventListener('keydown', this.onKeydown, false);
        window.removeEventListener('keyup', this.onKeyup, false);
        window.removeEventListener('blur', this.onBlur, false);
        if (Grid_1.GridBase.Settings.touch) {
            Grid_1.GridBase.Container.container.removeEventListener('touchstart', this.onMouseDown, false);
            Grid_1.GridBase.Container.container.removeEventListener('touchend', this.onMouseUp, false);
            this.setupTouch();
        }
        else {
            Grid_1.GridBase.Container.container.removeEventListener('wheel', this.onWheelEvent, false);
            Grid_1.GridBase.Container.container.removeEventListener('mouseover', this.onMouseOver, false);
            Grid_1.GridBase.Container.container.removeEventListener('mouseleave', this.onMouseOut, false);
            window.removeEventListener('mousemove', this.onMouseMove, false);
            window.removeEventListener('mouseup', this.onMouseUp, false);
            window.removeEventListener('mousedown', this.onMouseDown, false);
        }
        if (this.hammer) {
            this.hammer.destroy();
        }
    };
    Events.prototype.update = function () { };
    Events.prototype.setupTouch = function () {
        this.hammer = new Hammer(Grid_1.GridBase.Container.container);
        var pan = new Hammer.Pan();
        var pinch = new Hammer.Pinch();
        this.hammer.add([pan, pinch]);
        var currentScale = (Grid_1.GridBase.Settings.scale / Grid_1.GridBase.Settings.ratio) * 1;
        this.hammer.on('pinchstart', function (evt) {
            Grid_1.GridBase.Settings.fit = 'scroll';
            currentScale = (Grid_1.GridBase.Settings.scale / Grid_1.GridBase.Settings.ratio) * 1;
            Grid_1.GridBase.Content.resizing = true;
        });
        this.hammer.on('pinchmove', function (evt) {
            Grid_1.GridBase.Content.setScale(currentScale * evt.scale);
            Grid_1.GridBase.Content.update();
            Grid_1.GridBase.Canvas.update();
            Grid_1.GridBase.Container.update();
        });
        this.hammer.on('pinchend', function (evt) {
            currentScale = (Grid_1.GridBase.Settings.scale / Grid_1.GridBase.Settings.ratio) * 1;
            Grid_1.GridBase.Content.resizing = false;
        });
        var currentOffsets = {
            x: 0,
            y: 0,
        };
        this.hammer.on('panstart', function (evt) {
            currentOffsets = {
                x: evt.center.x,
                y: evt.center.y,
            };
        });
        this.hammer.on('panmove', function (evt) {
            var moveBy = Math.round(Grid_1.GridBase.Settings.ratio / Grid_1.GridBase.Settings.scale);
            if (moveBy < 1)
                moveBy = 1;
            var x = 0;
            if (Math.abs(evt.center.x - currentOffsets.x) > 5 * Grid_1.GridBase.Settings.scale) {
                var directionX = evt.velocityX > 0 ? -1 : 1;
                currentOffsets.x = evt.center.x;
                x = moveBy * directionX;
            }
            var y = 0;
            if (Math.abs(evt.center.y - currentOffsets.y) > 5 * Grid_1.GridBase.Settings.scale) {
                var directionY = evt.velocityY > 0 ? -1 : 1;
                currentOffsets.y = evt.center.y;
                y = moveBy * directionY;
            }
            Grid_1.GridBase.Content.setOffsets(y, x);
            Grid_1.GridBase.Container.update();
            Grid_1.GridBase.Canvas.update();
        });
        this.hammer.on('panend', function (evt) { });
    };
    Events.prototype.isRightClick = function (evt) {
        var isRightMB;
        if ('which' in evt)
            isRightMB = evt.which === 3;
        else if ('button' in evt)
            isRightMB = evt.button === 2;
        return isRightMB;
    };
    return Events;
}(Emitter_1.default));
exports.default = Events;
//# sourceMappingURL=Events.js.map