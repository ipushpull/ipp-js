"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = require("../Emitter");
var Settings_1 = require("./Settings");
var Content_1 = require("./Content");
var Container_1 = require("./Container");
var Canvas_1 = require("./Canvas");
var Events_1 = require("./Events");
var GridEvents_1 = require("./GridEvents");
exports.GridBase = {
    Container: null,
    Settings: null,
    Events: null,
    Canvas: null,
    Content: null,
    GridEvents: null,
};
var Grid = (function (_super) {
    __extends(Grid, _super);
    function Grid(id, options) {
        var _this = _super.call(this) || this;
        _this.onEvent = function (name, value) {
            if (name === 'visibility' || name === 'resize') {
                _this.update();
            }
        };
        _this.onGridEvent = function (name, value) {
            _this.emit(name, value);
        };
        _this.init(id, options);
        return _this;
    }
    Grid.prototype.init = function (id, options) {
        var _this = this;
        exports.GridBase = {
            Content: new Content_1.default(),
            Container: new Container_1.default(),
            Settings: new Settings_1.default(),
            Events: new Events_1.default(),
            Canvas: new Canvas_1.default(),
            GridEvents: new GridEvents_1.default(),
        };
        Object.keys(exports.GridBase).forEach(function (key) {
            _this[key] = exports.GridBase[key];
        });
        exports.GridBase.Settings.init(options);
        exports.GridBase.Content.init();
        exports.GridBase.Container.init(id);
        exports.GridBase.Events.init();
        exports.GridBase.Events.register(this.onEvent);
        exports.GridBase.GridEvents.register(this.onGridEvent);
    };
    Grid.prototype.setContent = function (content, ranges, userId, diff) {
        if (userId === void 0) { userId = undefined; }
        if (diff === void 0) { diff = false; }
        var ref = exports.GridBase.Content.cellSelection.reference;
        exports.GridBase.Container.removeAllLinks();
        exports.GridBase.Content.setContent(content, ranges, userId);
        this.update();
        if (ref)
            this.setSelectorByRef(ref);
    };
    Grid.prototype.setDeltaContent = function (content) {
        exports.GridBase.Content.setDeltaContent(content);
        if (exports.GridBase.Settings.highlights) {
            this.update();
        }
        else {
            this.render();
        }
    };
    Grid.prototype.setOffsets = function (row, col) {
        exports.GridBase.Content.setOffsets(row, col);
    };
    Grid.prototype.cellHistory = function (data) {
        exports.GridBase.Content.cellHistory(data);
    };
    Grid.prototype.cellHighlights = function (data) {
        exports.GridBase.Content.cellHighlights(data);
    };
    Grid.prototype.setRanges = function (data) {
        exports.GridBase.Content.setRanges(data);
    };
    Grid.prototype.update = function () {
        exports.GridBase.Container.setSize();
        exports.GridBase.Content.update();
        exports.GridBase.Canvas.update();
        exports.GridBase.Container.update();
    };
    Grid.prototype.render = function () {
        exports.GridBase.Canvas.render();
        exports.GridBase.Container.update();
    };
    Grid.prototype.destroy = function () {
        exports.GridBase.Container.destroy();
        exports.GridBase.Content.destroy();
        exports.GridBase.Canvas.destroy();
        exports.GridBase.Events.unRegister(this.onEvent);
        exports.GridBase.GridEvents.unRegister(this.onGridEvent);
        exports.GridBase.Events.destroy();
    };
    Grid.prototype.setOption = function (key, value) {
        exports.GridBase.Settings.set(key, value);
        if (key === 'headings') {
        }
    };
    Grid.prototype.setFit = function (value) {
        exports.GridBase.Settings.set('fit', value);
        this.update();
    };
    Grid.prototype.find = function (value) {
        exports.GridBase.Content.find(value);
    };
    Grid.prototype.clearFound = function () {
        exports.GridBase.Content.clearFound();
    };
    Grid.prototype.getCellMeta = function (row, col) {
        return exports.GridBase.Content.getContentCellMeta({ row: row, col: col });
    };
    Grid.prototype.setSelector = function (from, to) {
        exports.GridBase.Content.setSelector(from, to);
        exports.GridBase.Container.update();
        exports.GridBase.Canvas.update();
    };
    Grid.prototype.setSelectorByRef = function (ref) {
        exports.GridBase.Content.setSelectorByRef(ref);
        exports.GridBase.Container.update();
        exports.GridBase.Canvas.update();
    };
    Grid.prototype.hideSelector = function () {
        exports.GridBase.Content.setSelector();
        exports.GridBase.Container.update();
        exports.GridBase.Canvas.update();
    };
    Grid.prototype.setHiddenColumns = function (data) {
        this.hideSelector();
        exports.GridBase.Content.setHiddenColumns(data);
    };
    Grid.prototype.setSorting = function (data) {
        this.hideSelector();
        exports.GridBase.Content.setSorting(data);
    };
    Grid.prototype.setFilters = function (data) {
        this.hideSelector();
        exports.GridBase.Content.setFilters(data);
    };
    Grid.prototype.clearSorting = function () {
        this.hideSelector();
        exports.GridBase.Content.clearSorting();
    };
    Grid.prototype.clearFilters = function () {
        this.hideSelector();
        exports.GridBase.Content.clearFilters();
    };
    return Grid;
}(Emitter_1.default));
exports.Grid = Grid;
//# sourceMappingURL=Grid.js.map