"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = require("../Emitter");
var GridEvents = (function (_super) {
    __extends(GridEvents, _super);
    function GridEvents() {
        return _super.call(this) || this;
    }
    Object.defineProperty(GridEvents.prototype, "ON_CELL_CLICKED", {
        get: function () {
            return 'cell_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_NO_CELL", {
        get: function () {
            return 'no_cell';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_CELL_CLICKED_RIGHT", {
        get: function () {
            return 'cell_clicked_right';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_CELL_HISTORY_CLICKED", {
        get: function () {
            return 'cell_history_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_CELL_LINK_CLICKED", {
        get: function () {
            return 'cell_link_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_CELL_DOUBLE_CLICKED", {
        get: function () {
            return 'cell_double_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_TAG_CLICKED", {
        get: function () {
            return 'tag_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_CELL_VALUE", {
        get: function () {
            return 'cell_value';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_CELL_SELECTED", {
        get: function () {
            return 'cell_selected';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_CELL_MOUSEDOWN", {
        get: function () {
            return 'cell_mousedown';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_CELL_RESET", {
        get: function () {
            return 'cell_reset';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_EDITING", {
        get: function () {
            return 'editing';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_RESIZE", {
        get: function () {
            return 'resize';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_HEADING_CLICKED", {
        get: function () {
            return 'heading_clicked';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GridEvents.prototype, "ON_DELETE", {
        get: function () {
            return 'delete';
        },
        enumerable: true,
        configurable: true
    });
    return GridEvents;
}(Emitter_1.default));
exports.default = GridEvents;
//# sourceMappingURL=GridEvents.js.map