"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Grid_1 = require("./Grid");
var GridLine = (function () {
    function GridLine(element, axis, cell) {
        var _this = this;
        this.element = element;
        this.axis = axis;
        this.cell = cell;
        this.moving = false;
        this.width = 0;
        this.height = 0;
        this.onEvents = function (name, evt) {
            if (name === 'mousedown') {
                _this.onMouseDown(evt);
            }
            if (name === 'mouseup') {
                _this.onMouseUp(evt);
            }
            if (name === 'mousemove') {
                _this.onMouseMove(evt);
            }
        };
        this.onMouseDown = function (evt) {
            if (evt.target !== _this.innerLine) {
                return;
            }
            Grid_1.GridBase.Content.setSelector();
            Grid_1.GridBase.Content.resizing = true;
            _this.moving = true;
            _this.width = _this.cell.coords.width * 1;
            _this.height = _this.cell.coords.height * 1;
            _this.startX = evt.x;
            _this.startY = evt.y;
            _this.zIndex = _this.element.style['z-index'];
            _this.element.style['z-index'] = _this.zIndex * 1000;
            _this.element.classList.add('active');
        };
        this.onMouseUp = function (evt) {
            if (!_this.moving) {
                return;
            }
            Grid_1.GridBase.Content.resizing = false;
            _this.moving = false;
            _this.element.style['z-index'] = _this.zIndex / 1000;
            _this.element.classList.remove('active');
            _this.emitSize();
        };
        this.onMouseMove = function (evt) {
            if (!_this.moving) {
                return;
            }
            _this.offsetX = evt.x - _this.startX;
            _this.offsetY = evt.y - _this.startY;
            var width = _this.width + _this.offsetX;
            if (width < 20)
                width = 20;
            var height = _this.height + _this.offsetY;
            if (height < 20)
                height = 20;
            if (_this.axis === 'col') {
                Grid_1.GridBase.Content.setColSize(_this.cell, width);
            }
            else {
                Grid_1.GridBase.Content.setRowSize(_this.cell, height);
            }
        };
        this.innerLine = element.getElementsByClassName('inner-line')[0];
        if (!this.innerLine) {
            return;
        }
        this.innerLine.addEventListener('mousedown', this.onMouseDown);
        window.addEventListener('mouseup', this.onMouseUp);
        window.addEventListener('mousemove', this.onMouseMove);
        this.width = cell.coords.width * 1;
        this.height = cell.coords.height * 1;
    }
    GridLine.prototype.destroy = function () {
        this.innerLine.removeEventListener('mousedown', this.onMouseDown);
        window.removeEventListener('mouseup', this.onMouseUp);
        window.removeEventListener('mousemove', this.onMouseMove);
    };
    GridLine.prototype.emitSize = function () {
        Grid_1.GridBase.GridEvents.emit(Grid_1.GridBase.GridEvents.ON_RESIZE, { axis: this.axis, x: this.offsetX, y: this.offsetY, cell: this.cell });
    };
    return GridLine;
}());
exports.GridLine = GridLine;
//# sourceMappingURL=GridLine.js.map