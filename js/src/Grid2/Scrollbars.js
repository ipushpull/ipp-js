"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Grid_1 = require("./Grid");
var Scrollbars = (function () {
    function Scrollbars() {
        var _this = this;
        this.inset = false;
        this.size = 8;
        this.colShow = false;
        this.rowShow = false;
        this.axis = '';
        this.scrollbars = {
            col: {
                axis: 'x',
                pos: 0,
                drag: false,
                dimension: 'width',
                label: 'col',
                anchor: 'left',
                increments: 0,
                offset: 0,
                size: 0,
            },
            row: {
                axis: 'y',
                pos: 0,
                drag: false,
                dimension: 'height',
                label: 'row',
                anchor: 'top',
                increments: 0,
                offset: 0,
                size: 0,
            },
        };
        this._visible = false;
        this.onAxisMouseDown = function (evt) {
            ['row', 'col'].forEach2(function (axis) {
                if (_this[axis + "Axis"] === evt.target) {
                    if (axis === 'row') {
                        var handlePos = _this.rowAxisHandle.getBoundingClientRect();
                        if (evt.y > handlePos.top + handlePos.height) {
                            var offsets = Grid_1.GridBase.Content.getPageOffset('down');
                            Grid_1.GridBase.Content.setOffsets(offsets.row, Grid_1.GridBase.Content.offsets.col, true);
                        }
                        else if (evt.y < handlePos.top) {
                            var offsets = Grid_1.GridBase.Content.getPageOffset('up');
                            Grid_1.GridBase.Content.setOffsets(offsets.row, Grid_1.GridBase.Content.offsets.col, true);
                        }
                    }
                    else {
                        var handlePos = _this.colAxisHandle.getBoundingClientRect();
                        if (evt.x > handlePos.left + handlePos.width) {
                            var offsets = Grid_1.GridBase.Content.getPageOffset('right');
                            Grid_1.GridBase.Content.setOffsets(Grid_1.GridBase.Content.offsets.row, offsets.col, true);
                        }
                        else if (evt.x < handlePos.left) {
                            var offsets = Grid_1.GridBase.Content.getPageOffset('left');
                            Grid_1.GridBase.Content.setOffsets(Grid_1.GridBase.Content.offsets.row, offsets.col, true);
                        }
                    }
                }
            });
            evt.preventDefault();
        };
        this.onMouseOut = function (evt) {
            ['row', 'col'].forEach(function (axis) {
                if (!_this.scrollbars[axis].drag) {
                    _this[axis + "Axis"].classList.remove('active');
                }
            });
        };
        this.onMouseOver = function (evt) {
            ['row', 'col'].forEach(function (axis) {
                if (_this[axis + "Axis"] === evt.target || _this[axis + "AxisHandle"] === evt.target) {
                    _this[axis + "Axis"].classList.add('active');
                }
            });
        };
        this.onMouseDown = function (evt) {
            ['row', 'col'].forEach2(function (axis) {
                if (_this[axis + "AxisHandle"] === evt.target) {
                    Grid_1.GridBase.Content.scrolling = true;
                    _this.scrollbars[axis].pos = evt[_this.scrollbars[axis].axis];
                    _this.scrollbars[axis].drag = true;
                    _this.axis = axis;
                    var size = Grid_1.GridBase.Container.containerRect[_this.scrollbars[axis].dimension] - _this.scrollbars[axis].size;
                    _this.scrollbars[axis].increments = size / Grid_1.GridBase.Content.dimensions[_this.axis + "s"].length;
                }
                _this.scrollbars[axis].offset = Grid_1.GridBase.Content.offsets[axis];
            });
            evt.preventDefault();
        };
        this.onMouseUp = function (evt) {
            Grid_1.GridBase.Content.scrolling = false;
            ['row', 'col'].forEach(function (axis) {
                if (_this[axis + "Axis"] !== evt.target && _this[axis + "AxisHandle"] !== evt.target) {
                    _this[axis + "Axis"].classList.remove('active');
                }
                _this.scrollbars[axis].drag = false;
            });
        };
        this.onMouseMove = function (evt) {
            if (!Grid_1.GridBase.Content.scrolling)
                return;
            var offset = {
                row: _this.scrollbars.row.offset,
                col: _this.scrollbars.col.offset,
            };
            var scrollbar = _this.scrollbars[_this.axis];
            var pos = evt[scrollbar.axis];
            var diff = Math.round((pos - scrollbar.pos) / scrollbar.increments);
            offset[_this.axis] = scrollbar.offset + diff;
            Grid_1.GridBase.Content.setOffsets(offset.row, offset.col, true);
        };
        this.colAxis = document.createElement('div');
        this.colAxis.className = 'track track-x';
        this.colAxis.style.display = "none";
        this.colAxis.style.height = this.size + "px";
        this.colAxis.style.position = "absolute";
        this.colAxis.style.left = "0px";
        this.colAxis.style.bottom = "0px";
        this.colAxis.style.right = "8px";
        this.colAxis.style['z-index'] = "1000";
        this.colAxisHandle = document.createElement('div');
        this.colAxisHandle.className = 'handle';
        this.colAxisHandle.style.position = "absolute";
        this.colAxisHandle.style.top = "0px";
        this.colAxisHandle.style.left = "0px";
        this.colAxisHandle.style.bottom = "0px";
        this.colAxisHandle.style['z-index'] = 1;
        this.colAxis.appendChild(this.colAxisHandle);
        this.rowAxis = document.createElement('div');
        this.rowAxis.className = 'track track-y';
        this.rowAxis.style.display = "none";
        this.rowAxis.style.width = this.size + "px";
        this.rowAxis.style.position = "absolute";
        this.rowAxis.style.top = "0px";
        this.rowAxis.style.bottom = "8px";
        this.rowAxis.style.right = "0px";
        this.rowAxis.style['z-index'] = "1000";
        this.rowAxisHandle = document.createElement('div');
        this.rowAxisHandle.className = 'handle';
        this.rowAxisHandle.style.position = "absolute";
        this.rowAxisHandle.style.top = "0px";
        this.rowAxisHandle.style.left = "0px";
        this.rowAxisHandle.style.right = "0px";
        this.rowAxisHandle.style['z-index'] = 1;
        this.rowAxis.appendChild(this.rowAxisHandle);
        this.colAxis.addEventListener('mouseover', this.onMouseOver);
        this.rowAxis.addEventListener('mouseover', this.onMouseOver);
        this.colAxis.addEventListener('mouseout', this.onMouseOut);
        this.rowAxis.addEventListener('mouseout', this.onMouseOut);
        this.colAxisHandle.addEventListener('mousedown', this.onMouseDown);
        this.rowAxisHandle.addEventListener('mousedown', this.onMouseDown);
        this.colAxis.addEventListener('mousedown', this.onAxisMouseDown);
        this.rowAxis.addEventListener('mousedown', this.onAxisMouseDown);
        window.addEventListener('mousemove', this.onMouseMove);
        window.addEventListener('mouseup', this.onMouseUp);
    }
    Object.defineProperty(Scrollbars.prototype, "visible", {
        get: function () {
            return this._visible;
        },
        set: function (value) {
            this._visible = value;
        },
        enumerable: true,
        configurable: true
    });
    Scrollbars.prototype.update = function () {
        var _this = this;
        ['row', 'col'].forEach2(function (axis) {
            if (!Grid_1.GridBase.Content.offsets[axis + "Max"]) {
                _this[axis + "Axis"].style.display = 'none';
                _this[axis + "AxisHandle"].style.display = 'none';
                _this.scrollbars[axis].show = false;
                return;
            }
            var dimension = _this.scrollbars[axis].dimension;
            var canvasSize = Grid_1.GridBase.Container.containerRect[dimension] - 8;
            _this.scrollbars[axis].show = true;
            _this[axis + "AxisHandle"].style.display = 'block';
            var length = Grid_1.GridBase.Settings.headings
                ? canvasSize + Grid_1.GridBase.Content.dimensions[dimension]
                : Grid_1.GridBase.Content.dimensions[dimension] - 8;
            var size = canvasSize * (canvasSize / (length * Grid_1.GridBase.Settings.scale));
            _this[axis + "AxisHandle"].style[dimension] = size + "px";
            _this.scrollbars[axis].size = size;
            var offset = (Grid_1.GridBase.Content.offsets[axis] / Grid_1.GridBase.Content.offsets[axis + "Max"]) * (canvasSize - size);
            var anchor = _this.scrollbars[axis].anchor;
            _this[axis + "AxisHandle"].style[anchor] = offset + "px";
            _this[axis + "Axis"].style.display = 'block';
        });
    };
    Scrollbars.prototype.destroy = function () {
        this.colAxisHandle.removeEventListener('mousedown', this.onMouseDown);
        this.rowAxisHandle.removeEventListener('mousedown', this.onMouseDown);
        window.removeEventListener('mousemove', this.onMouseMove);
        window.removeEventListener('mouseup', this.onMouseUp);
    };
    return Scrollbars;
}());
exports.Scrollbars = Scrollbars;
//# sourceMappingURL=Scrollbars.js.map