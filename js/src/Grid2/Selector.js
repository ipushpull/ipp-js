"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Grid_1 = require("./Grid");
var Content_1 = require("./Content");
var Selector = (function () {
    function Selector() {
        this._visible = false;
        this._height = 0;
        this._width = 0;
        this._x = 0;
        this._y = 0;
        this.buttons = {};
        this.element = document.createElement('div');
        this.element.className = 'selection';
        this.element.style.position = 'absolute';
        this.element.style['z-index'] = 100;
        this.element.style.left = '0px';
        this.element.style.top = '0px';
        this.element.style.display = 'none';
    }
    Object.defineProperty(Selector.prototype, "visible", {
        get: function () {
            return this._visible;
        },
        set: function (value) {
            this._visible = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Selector.prototype, "height", {
        set: function (value) {
            this._height = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Selector.prototype, "width", {
        set: function (value) {
            this._width = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Selector.prototype, "x", {
        set: function (value) {
            this._x = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Selector.prototype, "y", {
        set: function (value) {
            this._y = value;
        },
        enumerable: true,
        configurable: true
    });
    Selector.prototype.update = function () {
        var show = Grid_1.GridBase.Content.cellSelection.from ? true : false;
        if (!show || Grid_1.GridBase.Settings.disallowSelection) {
            this.element.style.display = 'none';
            return;
        }
        this.updateSelectorAttributes(Grid_1.GridBase.Content.cellSelection.from, Grid_1.GridBase.Content.cellSelection.to || Grid_1.GridBase.Content.cellSelection.from, this.element);
    };
    Selector.prototype.updateSelectorAttributes = function (cell, cellTo, selector) {
        var cellCoords = new Content_1.CellMetaCoords(cell.coords);
        var cellToCoords = new Content_1.CellMetaCoords(cellTo.coords);
        var flip = false;
        var flipFromIndex;
        var flipToIndex;
        if (cellTo.index.col < cell.index.col && cellTo.index.row > cell.index.row) {
            flip = true;
            flipFromIndex = {
                col: cellTo.index.col,
                row: cell.index.row,
            };
            flipToIndex = {
                col: cell.index.col,
                row: cellTo.index.row,
            };
        }
        else if (cellTo.index.col > cell.index.col && cellTo.index.row < cell.index.row) {
            flip = true;
            flipFromIndex = {
                col: cell.index.col,
                row: cellTo.index.row,
            };
            flipToIndex = {
                col: cellTo.index.col,
                row: cell.index.row,
            };
        }
        else if (cellTo.index.col < cell.index.col || cellTo.index.row < cell.index.row) {
            flip = true;
            flipFromIndex = {
                col: cellTo.index.col,
                row: cellTo.index.row,
            };
            flipToIndex = {
                col: cell.index.col,
                row: cell.index.row,
            };
        }
        if (flip) {
            cell = Grid_1.GridBase.Content.getContentCellMeta(flipFromIndex);
            cellTo = Grid_1.GridBase.Content.getContentCellMeta(flipToIndex);
            cellCoords = new Content_1.CellMetaCoords(cell.coords);
            cellToCoords = new Content_1.CellMetaCoords(cellTo.coords);
        }
        var rowFreeze = Grid_1.GridBase.Content.getFreezeIndex('row');
        var colFreeze = Grid_1.GridBase.Content.getFreezeIndex('col');
        cellCoords.x -= (cell.index.col < colFreeze ? 0 : Grid_1.GridBase.Content.offsets.colOffset);
        cellCoords.y -= (cell.index.row < rowFreeze ? 0 : Grid_1.GridBase.Content.offsets.rowOffset);
        if (cell.index.col >= colFreeze && cellCoords.x < Grid_1.GridBase.Content.getFreezeSize('col')) {
            cellCoords.x = Grid_1.GridBase.Content.getFreezeSize('col');
        }
        if (cell.index.row >= rowFreeze && cellCoords.y < Grid_1.GridBase.Content.getFreezeSize('row')) {
            cellCoords.y = Grid_1.GridBase.Content.getFreezeSize('row');
        }
        cellToCoords.x -= (cellTo.index.col < colFreeze ? 0 : Grid_1.GridBase.Content.offsets.colOffset);
        cellToCoords.y -= (cellTo.index.row < rowFreeze ? 0 : Grid_1.GridBase.Content.offsets.rowOffset);
        cellCoords.columnWidth = cellToCoords.x - cellCoords.x + cellToCoords.columnWidth;
        cellCoords.height = cellToCoords.y - cellCoords.y + cellToCoords.height;
        if (cell.index.col < colFreeze && cellTo.index.col >= colFreeze && cellCoords.columnWidth < Grid_1.GridBase.Content.getFreezeSize('col')) {
            cellCoords.columnWidth = cellTo.coords.x + cellTo.coords.columnWidth - cell.coords.x - Grid_1.GridBase.Content.offsets.colOffset;
            if (cellCoords.columnWidth < Grid_1.GridBase.Content.getFreezeSize('col') - cell.coords.x) {
                cellCoords.columnWidth = Grid_1.GridBase.Content.getFreezeSize('col') - cell.coords.x;
            }
        }
        if (cell.index.row < rowFreeze && cellTo.index.row >= rowFreeze && cellCoords.height < Grid_1.GridBase.Content.getFreezeSize('row')) {
            cellCoords.height = cellTo.coords.y + cellTo.coords.height - cell.coords.y - Grid_1.GridBase.Content.offsets.rowOffset;
            if (cellCoords.height < Grid_1.GridBase.Content.getFreezeSize('row') - cell.coords.y) {
                cellCoords.height = Grid_1.GridBase.Content.getFreezeSize('row') - cell.coords.y;
            }
        }
        if (cellCoords.columnWidth < 0) {
            cellCoords.columnWidth = 0;
        }
        if (cellCoords.height < 0) {
            cellCoords.height = 0;
        }
        selector.style['left'] = Grid_1.GridBase.Content.scaleDimension(cellCoords.x) + "px";
        selector.style['width'] = Grid_1.GridBase.Content.scaleDimension(cellCoords.columnWidth) + "px";
        selector.style['height'] = Grid_1.GridBase.Content.scaleDimension(cellCoords.height) + "px";
        selector.style['top'] = Grid_1.GridBase.Content.scaleDimension(cellCoords.y) + "px";
        selector.style['display'] = 'block';
    };
    return Selector;
}());
exports.Selector = Selector;
//# sourceMappingURL=Selector.js.map