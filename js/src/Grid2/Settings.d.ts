import { IGridModule } from "./Grid";
interface INoAccessIMages {
    light: string;
    dark: string;
}
export interface ISettings extends IGridModule {
    contrast: string;
    tracking: boolean;
    highlights: boolean;
    highlightsShowTime: number;
    highlightsRemoveTime: number;
    touch: boolean;
    pause: boolean;
    headings: boolean;
    canEdit: boolean;
    alwaysEditing: boolean;
    disallowSelection: boolean;
    hasFocus: boolean;
    fluid: boolean;
    ratio: number;
    scale: number;
    fit: string;
    scrollbarWidth: number;
    scrollbarInset: boolean;
    userId: number;
    userTrackingColors: string[];
    noAccessImages: INoAccessIMages;
    historyIndicatorSize: number;
    backgroundColor: string;
    update: () => void;
    set: (key: string, value: any) => void;
}
export declare class Settings implements ISettings {
    contrast: string;
    tracking: boolean;
    highlights: boolean;
    highlightsShowTime: number;
    highlightsRemoveTime: number;
    touch: boolean;
    pause: boolean;
    headings: boolean;
    canEdit: boolean;
    alwaysEditing: boolean;
    disallowSelection: boolean;
    hasFocus: boolean;
    fluid: boolean;
    scrollbarWidth: number;
    scrollbarInset: boolean;
    scale: number;
    ratio: number;
    fit: string;
    userId: number;
    userTrackingColors: string[];
    noAccessImages: INoAccessIMages;
    historyIndicatorSize: number;
    backgroundColor: string;
    constructor(options?: any);
    init(): void;
    set(key: string, value: any): void;
    destroy(): void;
    update(): void;
}
export {};
