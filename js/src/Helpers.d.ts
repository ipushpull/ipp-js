declare class Helpers {
    constructor();
    isNumber(value: any): boolean;
    toColumnName(num: any): string;
    validHex(hex: string): boolean;
    rgbToHex(rgb: string): string;
    componentToHex(c: any): string;
    addEvent(element: HTMLElement, eventName: string, func: any): void;
    capitalizeFirstLetter(string: string): string;
    removeEvent(element: HTMLElement, eventName: string, func: any): void;
    isTouch(): boolean;
    clickEvent(): string;
    openWindow(link: string, target?: string, params?: {
        [s: string]: string;
    }): Window;
    createSlug(str: string): string;
    getScrollbarWidth(): number;
    parseDateExcel(excelTimestamp: any): number;
    getContrastYIQ(hexcolor: string): string;
    convertTime(timestamp: any): string;
    isValidEmail(string: string): boolean;
}
export default Helpers;
