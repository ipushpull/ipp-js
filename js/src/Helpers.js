"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var uuidV4 = require("uuid/v4");
var Helpers = (function () {
    function Helpers() {
        this.letters = 'ABCDEFGHIJKLMNOPQRSTUVWXY';
        return;
    }
    Helpers.prototype.getUuid = function () {
        return uuidV4();
    };
    Helpers.prototype.getNextValueInArray = function (value, arr) {
        if (!arr || !arr.length) {
            return value;
        }
        var n = value;
        for (var i = 0; i < arr.length; i++) {
            if (n !== arr[i]) {
                continue;
            }
            if (!arr[i + 1]) {
                n = arr[0];
            }
            else {
                n = arr[i + 1];
            }
            break;
        }
        if (n === value) {
            n = arr[0];
        }
        return n;
    };
    Helpers.prototype.isNumber = function (value) {
        return !/^[0-9\.\-]+$/.test(value) || isNaN(value) ? false : true;
    };
    Helpers.prototype.quoteString = function (value, quote) {
        if (quote === void 0) { quote = '"'; }
        return this.isNumber(Number) ? parseFloat(value) : "" + quote + value + quote;
    };
    Helpers.prototype.getCellsReference = function (from, to, headingSelected) {
        var colFrom = this.toColumnName(from.col + 1);
        var rowFrom = from.row + 1;
        var colTo = this.toColumnName(to.col + 1);
        var rowTo = to.row + 1;
        if (headingSelected === 'all') {
            return '1:-1';
        }
        else if (headingSelected === 'col') {
            if (colFrom === colTo)
                return colFrom;
            return colFrom + ":" + colTo;
        }
        else if (headingSelected === 'row') {
            if (rowFrom === rowTo)
                return "" + rowFrom;
            return rowFrom + ":" + rowTo;
        }
        if (colFrom === colTo && rowFrom === rowTo) {
            return "" + colFrom + rowFrom;
        }
        if (!rowTo) {
            return "" + colFrom + rowFrom + ":" + colTo;
        }
        return "" + colFrom + rowFrom + ":" + colTo + rowTo;
    };
    Helpers.prototype.getRefIndex = function (str, obj) {
        var _this = this;
        str = str.toUpperCase();
        var regex = /[A-Z]/gm;
        var m;
        var col = -1;
        var count = 0;
        while ((m = regex.exec(str)) !== null) {
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            if (col < 0) {
                col = 0;
            }
            m.forEach(function (match) {
                var i = _this.letters.indexOf(match);
                col += i + count;
                count += 26;
            });
        }
        var numbers = str.match(/\d/g);
        var row = numbers ? parseFloat(numbers.join('')) - 1 : -1;
        return obj ? { row: row, col: col } : [row, col];
    };
    Helpers.prototype.toColumnName = function (num) {
        for (var ret = '', a = 1, b = 26; (num -= a) >= 0; a = b, b *= 26) {
            ret = String.fromCharCode(parseInt((num % b) / a) + 65) + ret;
        }
        return ret;
    };
    Helpers.prototype.cellRange = function (str, rows, cols) {
        var _a = str.split(':'), from = _a[0], to = _a[1];
        var fromRow = 0, fromCol = 0, toRow = 0, toCol = 0;
        var isFromNumber = this.isNumber(from);
        var fromCell = this.getRefIndex(from, true);
        if (to) {
            var isToNumber = this.isNumber(to);
            var toCell = this.getRefIndex(to, true);
            if (isFromNumber) {
                fromRow = parseInt(from) - 1;
                fromCol = 0;
            }
            else {
                fromCol = fromCell.col;
                fromRow = fromCell.row;
                if (fromCell.row < 0) {
                    fromRow = 0;
                }
            }
            if (isToNumber) {
                toRow = parseInt(to) < 0 ? rows - 1 : parseInt(to) - 1;
                toCol = cols - 1;
            }
            else {
                toRow = toCell.row < 0 ? -1 : toCell.row;
                toCol = toCell.col < 0 ? -1 : toCell.col;
            }
        }
        else {
            if (isFromNumber) {
                fromRow = parseInt(from) - 1;
                toRow = fromRow;
                fromCol = 0;
                toCol = cols - 1;
            }
            else {
                fromCol = fromCell.col;
                fromRow = fromCell.row;
                toCol = fromCol;
                if (fromCell.row < 0) {
                    fromRow = 0;
                    toRow = -1;
                }
                else {
                    toRow = fromRow;
                }
            }
        }
        return {
            from: { row: fromRow, col: fromCol },
            to: { row: toRow, col: toCol },
        };
    };
    Helpers.prototype.validHex = function (hex) {
        return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(hex);
    };
    Helpers.prototype.rgbToHex = function (rgb) {
        rgb = rgb
            .replace('rgba(', '')
            .replace('rgb(', '')
            .replace(')', '');
        var parts = rgb.split(',');
        if (parts.length === 4) {
            if (parts[3].trim() === '0') {
                return 'FFFFFF';
            }
        }
        return (this.componentToHex(parseInt(parts[0], 10)) +
            this.componentToHex(parseInt(parts[1], 10)) +
            this.componentToHex(parseInt(parts[2], 10)));
    };
    Helpers.prototype.componentToHex = function (c) {
        var hex = c.toString(16);
        return hex.length === 1 ? '0' + hex : hex;
    };
    Helpers.prototype.addEvent = function (element, eventName, func) {
        if (!element) {
            return;
        }
        if (element.addEventListener) {
            element.addEventListener(eventName, func, false);
            if (eventName === 'click') {
                element.addEventListener('touchstart', func, false);
            }
        }
        else if (element.attachEvent) {
            element.attachEvent('on' + eventName, func);
        }
    };
    Helpers.prototype.capitalizeFirstLetter = function (str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    };
    Helpers.prototype.removeEvent = function (element, eventName, func) {
        if (element.removeEventListener) {
            element.removeEventListener(eventName, func, false);
            if (eventName === 'click') {
                element.removeEventListener('touchstart', func, false);
            }
        }
        else if (element.detachEvent) {
            element.detachEvent('on' + eventName, func);
        }
    };
    Helpers.prototype.isTouch = function () {
        return 'ontouchstart' in document.documentElement;
    };
    Helpers.prototype.clickEvent = function () {
        return this.isTouch() ? 'touchstart' : 'click';
    };
    Helpers.prototype.openWindow = function (link, target, params) {
        if (target === void 0) { target = '_blank'; }
        if (params === void 0) { params = {}; }
        var paramsStr = '';
        for (var key in params) {
            if (!params.hasOwnProperty(key)) {
                continue;
            }
            paramsStr += key + "=" + params[key] + ",";
        }
        paramsStr = paramsStr.substring(0, paramsStr.length - 1);
        return window.open(link, target, paramsStr);
    };
    Helpers.prototype.createSlug = function (str) {
        return str.split(' ').join('_');
    };
    Helpers.prototype.getScrollbarWidth = function () {
        var outer = document.createElement('div');
        outer.style.visibility = 'hidden';
        outer.style.width = '100px';
        outer.style.msOverflowStyle = 'scrollbar';
        document.body.appendChild(outer);
        var widthNoScroll = outer.offsetWidth;
        outer.style.overflow = 'scroll';
        var inner = document.createElement('div');
        inner.style.width = '100%';
        outer.appendChild(inner);
        var widthWithScroll = inner.offsetWidth;
        outer.parentNode.removeChild(outer);
        return widthNoScroll - widthWithScroll;
    };
    Helpers.prototype.parseDateExcel = function (excelTimestamp) {
        var secondsInDay = 24 * 60 * 60;
        var excelEpoch = new Date(1899, 11, 31);
        var excelEpochAsUnixTimestamp = excelEpoch.getTime();
        var missingLeapYearDay = secondsInDay * 1000;
        var delta = excelEpochAsUnixTimestamp - missingLeapYearDay;
        var excelTimestampAsUnixTimestamp = excelTimestamp * secondsInDay * 1000;
        var parsed = excelTimestampAsUnixTimestamp + delta;
        return isNaN(parsed) ? null : parsed;
    };
    Helpers.prototype.getContrastYIQ = function (hexcolor) {
        hexcolor = hexcolor.replace('#', '');
        var r = parseInt(hexcolor.substr(0, 2), 16);
        var g = parseInt(hexcolor.substr(2, 2), 16);
        var b = parseInt(hexcolor.substr(4, 2), 16);
        var yiq = (r * 299 + g * 587 + b * 114) / 1000;
        return yiq >= 128 ? 'light' : 'dark';
    };
    Helpers.prototype.convertTime = function (timestamp) {
        if (typeof timestamp === 'string') {
            timestamp = new Date(timestamp);
        }
        var hours = timestamp.getHours();
        if (hours < 10) {
            hours = "0" + hours;
        }
        var minutes = timestamp.getMinutes();
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        var seconds = timestamp.getSeconds();
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        return hours + ":" + minutes + ":" + seconds;
    };
    Helpers.prototype.isValidEmail = function (str) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(str);
    };
    Helpers.prototype.safeTitle = function (str, replaceWith) {
        if (replaceWith === void 0) { replaceWith = '_'; }
        return str.replace(/[^a-zA-Z0-9_\.\-]/g, replaceWith);
    };
    Helpers.prototype.safeXmlChars = function (str) {
        var regex = /<[\w](.*?)\/>/gm;
        var m;
        var ignore = [];
        while ((m = regex.exec(str)) !== null) {
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            ignore.push(m[0]);
        }
        ignore.forEach(function (s, i) {
            str = str.replace(s, "[$" + i + "]");
        });
        str = str
            .replace(/&/g, '&amp;')
            .replace(/>/g, '&gt;')
            .replace(/</g, '&lt;')
            .replace(/'/g, '&apos;')
            .replace(/"/g, '&quot;');
        ignore.forEach(function (s, i) {
            str = str.replace("[$" + i + "]", s);
        });
        return str;
    };
    Helpers.prototype.isTarget = function (target, element) {
        var clickedEl = element;
        while (clickedEl && clickedEl !== target) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === target;
    };
    return Helpers;
}());
exports.default = Helpers;
//# sourceMappingURL=Helpers.js.map