export interface IPageContentLink {
    external: boolean;
    address: string;
}
export interface IPageCellStyle {
    "background-color"?: string;
    "color"?: string;
    "font-family"?: string;
    "font-size"?: string;
    "font-style"?: string;
    "font-weight"?: string;
    "height"?: string;
    "number-format"?: string;
    "text-align"?: string;
    "text-wrap"?: string;
    "width"?: string;
    "tbs"?: string;
    "rbs"?: string;
    "bbs"?: string;
    "lbs"?: string;
    "tbc"?: string;
    "rbc"?: string;
    "bbc"?: string;
    "lbc"?: string;
    "tbw"?: string;
    "rbw"?: string;
    "bbw"?: string;
    "lbw"?: string;
}
export interface IPageContentCell {
    value: string;
    formatted_value?: string;
    link?: IPageContentLink;
    style?: IPageCellStyle;
    originalStyle?: IPageCellStyle;
    dirty?: boolean;
}
export interface IPageContent extends Array<any> {
    [index: number]: IPageContentCell[];
}
export interface IPageDeltaContentCol {
    col_index: number;
    cell_content: IPageContentCell;
}
export interface IPageDeltaContentRow {
    row_index: number;
    cols: IPageDeltaContentCol[];
}
export interface IPageDelta {
    new_rows: number[];
    new_cols: number[];
    content_delta: IPageDeltaContentRow[];
}
export interface IPageContentProvider {
    current: IPageContent;
    canDoDelta: boolean;
    dirty: boolean;
    update: (rawContent: IPageContent, clean?: boolean, replace?: boolean) => void;
    reset: () => void;
    getCell: (rowIndex: number, columnIndex: number) => IPageContentCell;
    getCells: (fromCell: any, toCell: any) => any;
    updateCell: (rowIndex: number, columnIndex: number, data: IPageContentCell) => void;
    addRow: (index?: number) => void;
    addColumn: (index?: number, direction?: string) => void;
    removeRow: (index?: number, direction?: string) => void;
    removeColumn: (index?: number) => void;
    setColSize(col: number, value: number): void;
    setRowSize(row: number, value: number): void;
    getDelta: () => IPageDelta;
    getFull: () => IPageContent;
}
export declare class PageContent implements IPageContentProvider {
    canDoDelta: boolean;
    dirty: boolean;
    private _original;
    private _current;
    private _newRows;
    private _newCols;
    private _utils;
    private _defaultStyles;
    private borders;
    private cellStyles;
    readonly current: IPageContent;
    constructor(rawContent: IPageContent);
    update(rawContent: IPageContent, clean?: boolean, replace?: boolean): void;
    reset(): void;
    getCell(rowIndex: number, columnIndex: number): IPageContentCell;
    getCells(fromCell: any, toCell: any): any;
    updateCell(rowIndex: number, columnIndex: number, data: IPageContentCell): void;
    addRow(index?: number, direction?: string): void;
    addColumn(index?: number, direction?: string): void;
    removeRow(index?: number): void;
    removeColumn(index: number): void;
    setRowSize(row: number, value: number): void;
    setColSize(col: number, value: number): void;
    getDelta(): IPageDelta;
    getFull(): IPageContent;
    getHtml(): any;
    private flattenStyles;
    private defaultContent;
}
