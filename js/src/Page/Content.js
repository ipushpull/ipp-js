"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils_1 = require("../Utils");
var merge = require("merge");
var _ = require("underscore");
var Helpers_1 = require("../Helpers");
var PageContent = (function () {
    function PageContent(rawContent) {
        this.canDoDelta = true;
        this.dirty = false;
        this._original = [[]];
        this._current = [[]];
        this._newRows = [];
        this._newCols = [];
        this._defaultStyles = {
            "background-color": "FFFFFF",
            "color": "000000",
            "font-family": "sans-serif",
            "font-size": "11pt",
            "font-style": "normal",
            "font-weight": "normal",
            "height": "20px",
            "number-format": "",
            "text-align": "left",
            "text-wrap": "normal",
            "width": "64px",
            "tbs": "none",
            "rbs": "none",
            "bbs": "none",
            "lbs": "none",
            "tbc": "000000",
            "rbc": "000000",
            "bbc": "000000",
            "lbc": "000000",
            "tbw": "none",
            "rbw": "none",
            "bbw": "none",
            "lbw": "none",
        };
        this.borders = {
            widths: {
                none: 0,
                thin: 1,
                medium: 2,
                thick: 3
            },
            styles: {
                none: "solid",
                solid: "solid",
                double: "double"
            },
            names: {
                t: "top",
                r: "right",
                b: "bottom",
                l: "left",
            }
        };
        this.cellStyles = [
            "background-color",
            "color",
            "font-family",
            "font-size",
            "font-style",
            "font-weight",
            "height",
            "text-align",
            "text-wrap",
            "width",
            "vertical-align",
        ];
        if (!rawContent || rawContent.constructor !== Array || !rawContent.length || !rawContent[0].length) {
            rawContent = this.defaultContent();
        }
        this._utils = new Utils_1.default();
        this._helpers = new Helpers_1.default();
        this.update(rawContent, true);
    }
    Object.defineProperty(PageContent.prototype, "current", {
        get: function () { return this._current; },
        enumerable: true,
        configurable: true
    });
    PageContent.prototype.update = function (rawContent, clean, replace) {
        if (clean === void 0) { clean = false; }
        if (replace === void 0) { replace = false; }
        if (this.dirty && !replace) {
            return;
        }
        if (replace) {
            this.dirty = true;
            this._current = rawContent;
            return;
        }
        else {
            this._original = rawContent;
        }
        var style = merge({}, this._defaultStyles);
        this._current = this._utils.mergePageContent(this._original, this._current, style, clean);
    };
    PageContent.prototype.reset = function () {
        for (var i = 0; i < this._newRows.length; i++) {
            this._current.splice(this._newRows[i], 1);
        }
        for (var i = 0; i < this._newCols.length; i++) {
            for (var j = 0; j < this._current.length; j++) {
                this._current[j].splice(this._newCols[i], 1);
            }
        }
        this.dirty = false;
        this.update(this._original, true);
    };
    PageContent.prototype.getCellByRef = function (ref) {
        var rowCol = this._helpers.getRefIndex(ref);
        return this.getCell(rowCol[0], rowCol[1]);
    };
    PageContent.prototype.getCell = function (rowIndex, columnIndex) {
        if (!this._current[rowIndex]) {
            throw new Error("row out of bounds");
        }
        if (!this._current[rowIndex][columnIndex]) {
            throw new Error("column out of bounds");
        }
        return this._current[rowIndex][columnIndex];
    };
    PageContent.prototype.getCells = function (fromCell, toCell) {
        var cellUpdates = [];
        for (var rowIndex = fromCell.row; rowIndex <= toCell.row; rowIndex++) {
            if (!this._current[rowIndex])
                continue;
            for (var colIndex = fromCell.col; colIndex <= toCell.col; colIndex++) {
                if (!this._current[rowIndex][colIndex])
                    continue;
                if (!cellUpdates[rowIndex])
                    cellUpdates[rowIndex] = [];
                cellUpdates[rowIndex][colIndex] = this._current[rowIndex][colIndex];
            }
        }
        return cellUpdates;
    };
    PageContent.prototype.updateCellByRef = function (ref, data) {
        var rowCol = this._helpers.getRefIndex(ref);
        this.updateCell(rowCol[0], rowCol[1], data);
    };
    PageContent.prototype.updateCell = function (rowIndex, columnIndex, data) {
        if (!this._current[rowIndex]) {
            throw new Error("row out of bounds");
        }
        if (!this._current[rowIndex][columnIndex]) {
            throw new Error("column out of bounds");
        }
        data.dirty = true;
        var cell = merge({}, this._current[rowIndex][columnIndex], data);
        if (data.style) {
            cell.style = merge({}, this._current[rowIndex][columnIndex].style, data.style);
            cell.originalStyle = merge({}, this._current[rowIndex][columnIndex].style);
        }
        this._current[rowIndex][columnIndex] = cell;
    };
    PageContent.prototype.addRow = function (index, direction) {
        if (!this._current[index]) {
            index = this._current.length - 1;
            direction = "below";
        }
        var inc = direction === "below" ? 1 : 0;
        var newRowData = [];
        if (this._current.length) {
            for (var i = 0; i < this._current[index].length; i++) {
                var clone = JSON.parse(JSON.stringify(this._current[index][i]));
                clone.value = "";
                clone.formatted_value = "";
                clone.dirty = true;
                newRowData.push(clone);
            }
        }
        this._current.splice(index + inc, 0, newRowData);
        this._current = this._utils.mergePageContent(this._current);
    };
    PageContent.prototype.moveColumns = function (fromIndex, toIndex, newIndex) {
        if (!this._current[0]) {
            throw new Error("row out of bounds");
        }
        if (!this._current[0][fromIndex]) {
            throw new Error("column out of bounds");
        }
        if (!this._current[0][newIndex]) {
            throw new Error("column out of bounds");
        }
        for (var i = 0; i < this._current.length; i++) {
            var clone = void 0;
            var cells = [];
            for (var k = 0; k < this._current[i].length; k++) {
                if (k < fromIndex || k > toIndex) {
                    continue;
                }
                clone = JSON.parse(JSON.stringify(this._current[i][k]));
                clone.dirty = true;
                cells.push(clone);
            }
            var cellStr = [];
            for (var n = 0; n < cells.length; n++) {
                cellStr.push("cells[" + n + "]");
            }
            var evalStr = "this._current[i].splice(newIndex, 0, " + cellStr.join(",") + ");";
            eval(evalStr);
            if (newIndex > fromIndex) {
                this._current[i].splice(fromIndex, 1 + toIndex - fromIndex);
            }
            else {
                this._current[i].splice(fromIndex + 1 + toIndex - fromIndex, 1 + toIndex - fromIndex);
            }
        }
        this._current = this._utils.mergePageContent(this._current);
    };
    PageContent.prototype.addColumn = function (index, direction) {
        if (!this._current[0][index]) {
            index = this._current[0].length - 1;
            direction = "right";
        }
        var inc = direction === "right" ? 1 : 0;
        for (var i = 0; i < this._current.length; i++) {
            var clone = void 0;
            clone = JSON.parse(JSON.stringify(this._current[i][index]));
            clone.dirty = true;
            clone.value = "";
            clone.formatted_value = "";
            this._current[i].splice(index + inc, 0, clone);
        }
        this._current = this._utils.mergePageContent(this._current);
    };
    PageContent.prototype.removeRow = function (index) {
        if (!this._current[index] || this._current.length === 1) {
            return;
        }
        this._current.splice(index, 1);
        this._current = this._utils.mergePageContent(this._current);
    };
    PageContent.prototype.removeColumn = function (index) {
        if (!this._current[0][index] || this._current[0].length === 1) {
            return;
        }
        for (var i = 0; i < this._current.length; i++) {
            this._current[i].splice(index, 1);
        }
        this._current = this._utils.mergePageContent(this._current);
    };
    PageContent.prototype.setRowSize = function (row, value) {
        if (!this._current[row]) {
            return;
        }
        for (var colIndex = 0; colIndex < this._current[row].length; colIndex++) {
            var cell = this._current[row][colIndex];
            cell.style.height = value + "px";
            cell.dirty = true;
        }
    };
    PageContent.prototype.setColSize = function (col, value) {
        if (!this._current[0][col]) {
            return;
        }
        for (var rowIndex = 0; rowIndex < this._current.length; rowIndex++) {
            var cell = this._current[rowIndex][col];
            cell.style.width = value + "px";
            cell.dirty = true;
        }
    };
    PageContent.prototype.getDelta = function (clean) {
        if (clean === void 0) { clean = false; }
        var current = this._utils.clonePageContent(this._current);
        var deltaStructure = {
            new_rows: [],
            new_cols: [],
            content_delta: [
                {
                    row_index: 0,
                    cols: [
                        {
                            col_index: 0,
                            cell_content: {
                                value: "",
                            },
                        },
                    ],
                },
            ],
        };
        deltaStructure.content_delta = [];
        deltaStructure.new_rows = this._newRows;
        deltaStructure.new_cols = this._newCols;
        var rowMovedBy = 0;
        var colMovedBy = 0;
        for (var i = 0; i < current.length; i++) {
            var rowData = {};
            var newRow = (this._newRows.indexOf(i) >= 0);
            colMovedBy = 0;
            if (newRow) {
                rowData = {
                    row_index: i,
                    cols: [],
                };
                rowMovedBy++;
            }
            for (var j = 0; j < current[i].length; j++) {
                if (newRow) {
                    var cell = _.clone(current[i][j]);
                    if (clean) {
                        delete this._current[i][j].dirty;
                    }
                    delete cell.dirty;
                    delete cell.formatting;
                    rowData.cols.push({
                        col_index: j,
                        cell_content: cell,
                    });
                }
                else {
                    var newCol = (this._newCols.indexOf(j) >= 0);
                    if (newCol) {
                        colMovedBy++;
                    }
                    if (newCol || current[i][j].dirty) {
                        if (!Object.keys(rowData).length) {
                            rowData = {
                                row_index: i,
                                cols: [],
                            };
                        }
                        var cell = _.clone(current[i][j]);
                        if (clean) {
                            delete this._current[i][j].dirty;
                        }
                        delete cell.dirty;
                        delete cell.originalStyle;
                        delete cell.formatting;
                        rowData.cols.push({
                            col_index: j,
                            cell_content: cell,
                        });
                    }
                }
            }
            if (Object.keys(rowData).length) {
                deltaStructure.content_delta.push(rowData);
            }
        }
        return deltaStructure;
    };
    PageContent.prototype.getFull = function () {
        var content = this._utils.clonePageContent(this._current);
        for (var i = 0; i < content.length; i++) {
            for (var j = 0; j < content[i].length; j++) {
                delete content[i][j].dirty;
                delete content[i][j].originalStyle;
                delete content[i][j].formatting;
            }
        }
        return content;
    };
    PageContent.prototype.getHtml = function () {
        var left = 0;
        var width = 0;
        var height = 0;
        var cells = [];
        var html = "<table style=\"border-collapse: collapse;\">";
        for (var rowIndex = 0; rowIndex < this._current.length; rowIndex++) {
            cells[rowIndex] = [];
            var row = this._current[rowIndex];
            html += "<tr>";
            left = 0;
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var cell = row[colIndex];
                var styles = this.flattenStyles(cell.style);
                html += "<td style=\"" + styles + "\"><div>" + cell.formatted_value + "</div></td>";
                left += parseFloat(cell.style.width);
                if (!rowIndex) {
                    width += parseFloat(cell.style.width);
                }
                if (!colIndex) {
                    height += parseFloat(cell.style.height);
                }
                cells[rowIndex].push({
                    value: cell.formatted_value,
                    style: styles,
                });
            }
            if (!rowIndex && row[row.length - 1].style.rbw !== "none") {
                width += parseFloat(row[row.length - 1].style.rbw);
            }
            html += "</tr>";
        }
        if (this._current[this._current.length - 1][0].style.rbw !== "none") {
            height += parseFloat(this._current[this._current.length - 1][0].style.rbw);
        }
        html += "</table>";
        return {
            width: width,
            height: height,
            html: html,
            cells: cells,
        };
    };
    PageContent.prototype.flattenStyles = function (styles) {
        var htmlStyle = {};
        this.cellStyles.forEach(function (s) {
            htmlStyle[s] = styles[s];
        });
        for (var key in this.borders.names) {
            var name_1 = this.borders.names[key];
            var width = this.borders.widths[styles[key + "bw"]] + "px";
            var style = styles[key + "bs"];
            var color = "#" + styles[key + "bc"].replace("#", "");
            htmlStyle["border-" + name_1] = width + " " + style + " " + color;
        }
        var str = "";
        for (var attr in htmlStyle) {
            var value = htmlStyle[attr];
            if ((attr === "background-color" || attr === "color") && value.indexOf("#") === -1) {
                value = "#" + value;
            }
            str += attr + ": " + value + "; ";
        }
        return str;
    };
    PageContent.prototype.defaultContent = function () {
        return [
            [
                {
                    value: "",
                    formatted_value: "",
                },
            ],
        ];
    };
    return PageContent;
}());
exports.PageContent = PageContent;
//# sourceMappingURL=Content.js.map