import IPromise = angular.IPromise;
import IQService = angular.IQService;
import ITimeoutService = angular.ITimeoutService;
import IIntervalService = angular.IIntervalService;
import { IEmitter } from "../Emitter";
import { IPageContentProvider } from "./Content";
import { IPageContent } from "./Content";
import { IPageRangesCollection, IPageFreezingRange } from "./Range";
import { IEncryptionKey, ICryptoService } from "../Crypto";
import { IApiService } from "../Api";
import { IAuthService } from "../Auth";
import { IStorageService } from "../Storage";
import { IIPPConfig } from "../Config";
export interface IPageTypes {
    regular: number;
    pageAccessReport: number;
    domainUsageReport: number;
    globalUsageReport: number;
    pageUpdateReport: number;
    alert: number;
    pdf: number;
    liveUsage: number;
}
export interface IPageServiceContent {
    id: number;
    seq_no: number;
    content_modified_timestamp: Date;
    content: IPageContent;
    content_modified_by: any;
    push_interval: number;
    pull_interval: number;
    is_public: boolean;
    description: string;
    encrypted_content: string;
    encryption_key_used: string;
    encryption_type_used: number;
    special_page_type: number;
}
export interface IPageServiceMeta {
    by_name_url: string;
    id: number;
    name: string;
    description: string;
    url: string;
    uuid: string;
    access_rights: string;
    background_color: string;
    content: IPageContent;
    content_modified_by: any;
    content_modified_timestamp: Date;
    created_by: any;
    created_timestamp: Date;
    domain_id: number;
    domain_name: string;
    domain_url: string;
    encrypted_content: string;
    encryption_key_to_use: string;
    encryption_key_used: string;
    encryption_type_to_use: number;
    encryption_type_used: number;
    is_obscured_public: boolean;
    is_public: boolean;
    is_template: boolean;
    modified_by: any;
    modified_timestamp: Date;
    pull_interval: number;
    push_interval: number;
    record_history: boolean;
    seq_no: number;
    show_gridlines: boolean;
    special_page_type: number;
    ws_enabled: boolean;
}
export interface IPage extends IPageServiceMeta {
}
export interface IUserPageDomainCurrentUserAccess {
    default_page_id: number;
    default_page_url: string;
    domain_id: number;
    domain_url: string;
    is_active: boolean;
    is_administrator: boolean;
    is_default_domain: boolean;
    is_pending: boolean;
    page_count: number;
    user_id: number;
    user_url: string;
}
export interface IUserPageDomainAccess {
    alerts_enabled: boolean;
    by_name_url: string;
    current_user_domain_access: IUserPageDomainCurrentUserAccess;
    description: string;
    display_name: string;
    domain_type: number;
    encryption_enabled: boolean;
    id: number;
    is_page_access_mode_selectable: boolean;
    is_paying_customer: boolean;
    login_screen_background_color: "";
    logo_url: string;
    name: string;
    page_access_mode: number;
    page_access_url: string;
    url: string;
}
export interface IUserPageAccess {
    by_name_url: string;
    content_by_name_url: string;
    content_url: string;
    domain: IUserPageDomainAccess;
    domain_id: number;
    domain_name: string;
    domain_url: string;
    encryption_to_use: number;
    encryption_key_to_use: string;
    id: number;
    is_administrator: boolean;
    is_public: boolean;
    is_users_default_page: boolean;
    name: string;
    pull_interval: number;
    push_interval: number;
    special_page_type: number;
    url: string;
    write_access: boolean;
    ws_enabled: boolean;
}
export interface IPageTemplate {
    by_name_url: string;
    category: string;
    description: string;
    domain_id: number;
    domain_name: string;
    id: number;
    name: string;
    special_page_type: number;
    url: string;
    uuid: string;
}
export interface IPageCloneOptions {
    clone_ranges?: boolean;
}
export interface IPageCopyOptions {
    content?: boolean;
    access_rights?: boolean;
    settings?: boolean;
}
export interface IPageService extends IEmitter {
    TYPE_REGULAR: number;
    TYPE_ALERT: number;
    TYPE_PDF: number;
    TYPE_PAGE_ACCESS_REPORT: number;
    TYPE_DOMAIN_USAGE_REPORT: number;
    TYPE_GLOBAL_USAGE_REPORT: number;
    TYPE_PAGE_UPDATE_REPORT: number;
    TYPE_LIVE_USAGE_REPORT: number;
    EVENT_READY: string;
    EVENT_NEW_CONTENT: string;
    EVENT_NEW_META: string;
    EVENT_RANGES_UPDATED: string;
    EVENT_ACCESS_UPDATED: string;
    EVENT_DECRYPTED: string;
    EVENT_ERROR: string;
    ready: boolean;
    decrypted: boolean;
    updatesOn: boolean;
    types: IPageTypes;
    encryptionKeyPull: IEncryptionKey;
    encryptionKeyPush: IEncryptionKey;
    data: IPage;
    access: IUserPageAccess;
    Content: IPageContentProvider;
    Ranges: IPageRangesCollection;
    freeze: IPageFreezingRange;
    start: () => void;
    stop: () => void;
    push: (forceFull?: boolean) => IPromise<any>;
    saveMeta: (data: any) => IPromise<any>;
    destroy: () => void;
    decrypt: (key: IEncryptionKey) => void;
    clone: (folderId: number, name: string, options?: IPageCloneOptions) => IPromise<IPageService>;
    copy: (folderId: number, name: string, options?: IPageCopyOptions) => IPromise<IPageService>;
}
export declare class PageWrap {
    static $inject: string[];
    constructor(q: IQService, timeout: ITimeoutService, interval: IIntervalService, ippApi: IApiService, ippAuth: IAuthService, ippStorage: IStorageService, ippCrypto: ICryptoService, ippConf: IIPPConfig);
}
