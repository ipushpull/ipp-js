"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Utils_1 = require("../Utils");
var merge = require("merge");
var extend = require("extend");
var _ = require("underscore");
var Emitter_1 = require("../Emitter");
var Content_1 = require("./Content");
var Provider_1 = require("./Provider");
var Provider_2 = require("./Provider");
var Provider_3 = require("./Provider");
var Range_1 = require("./Range");
var Range_2 = require("./Range");
var $q, $timeout, $interval, api, auth, storage, crypto, config, utils, providers, ranges;
var PageWrap = (function () {
    function PageWrap(q, timeout, interval, ippApi, ippAuth, ippStorage, ippCrypto, ippConf) {
        $q = q;
        $timeout = timeout;
        $interval = interval;
        api = ippApi;
        auth = ippAuth;
        storage = ippStorage;
        crypto = ippCrypto;
        config = ippConf;
        utils = new Utils_1.default();
        providers = new Provider_1.Providers($q, $timeout, api, auth, storage, config);
        ranges = new Range_1.RangesWrap($q, api);
        return Page;
    }
    PageWrap.$inject = [
        "$q",
        "$timeout",
        "$interval",
        "ippApiService",
        "ippAuthService",
        "ippStorageService",
        "ippCryptoService",
        "ippConfig"
    ];
    return PageWrap;
}());
exports.PageWrap = PageWrap;
var Page = (function (_super) {
    __extends(Page, _super);
    function Page(pageId, folderId, uuid) {
        var _this = _super.call(this) || this;
        _this.ready = false;
        _this.decrypted = true;
        _this.updatesOn = true;
        _this.freeze = {
            valid: false,
            row: -1,
            col: -1
        };
        _this._supportsWS = true;
        _this._wsDisabled = false;
        _this._checkAccess = true;
        _this._error = false;
        _this._encryptionKeyPull = {
            name: "",
            passphrase: ""
        };
        _this._encryptionKeyPush = {
            name: "",
            passphrase: ""
        };
        _this.onPageError = function (err) {
            if (!err) {
                return;
            }
            err.code = err.httpCode || err.code;
            err.message = err.httpText || err.message || (typeof err.data === "string" ? err.data : "Page not found");
            _this.emit(_this.EVENT_ERROR, err);
            if (err.code === 404) {
                _this.destroy();
            }
            if (err.type === "redirect") {
                _this._wsDisabled = true;
                _this.init(true);
            }
            else {
                _this._error = true;
            }
        };
        _this.types = {
            regular: _this.TYPE_REGULAR,
            pageAccessReport: _this.TYPE_PAGE_ACCESS_REPORT,
            domainUsageReport: _this.TYPE_DOMAIN_USAGE_REPORT,
            globalUsageReport: _this.TYPE_GLOBAL_USAGE_REPORT,
            pageUpdateReport: _this.TYPE_PAGE_UPDATE_REPORT,
            alert: _this.TYPE_ALERT,
            pdf: _this.TYPE_PDF,
            liveUsage: _this.TYPE_LIVE_USAGE_REPORT
        };
        _this._supportsWS = "WebSocket" in window || "MozWebSocket" in window;
        _this._folderId = !isNaN(+folderId) ? folderId : undefined;
        _this._pageId = !isNaN(+pageId) ? pageId : undefined;
        _this._folderName = isNaN(+folderId) ? folderId : undefined;
        _this._pageName = isNaN(+pageId) ? pageId : undefined;
        _this._uuid = uuid;
        if (!_this._pageId && !_this._uuid) {
            _this.getPageId(_this._folderName, _this._pageName).then(function (res) {
                if (!res.pageId) {
                    _this.onPageError({
                        code: 404,
                        message: "Page not found"
                    });
                    return;
                }
                _this._pageId = res.pageId;
                _this._folderId = res.folderId;
                _this.init();
            }, function (err) {
                _this.onPageError(err || {
                    code: 404,
                    message: "Page not found"
                });
            });
        }
        else {
            _this.init(_this._uuid ? true : false);
        }
        return _this;
    }
    Object.defineProperty(Page.prototype, "TYPE_REGULAR", {
        get: function () {
            return 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_ALERT", {
        get: function () {
            return 5;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_PDF", {
        get: function () {
            return 6;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_PAGE_ACCESS_REPORT", {
        get: function () {
            return 1001;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_DOMAIN_USAGE_REPORT", {
        get: function () {
            return 1002;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_GLOBAL_USAGE_REPORT", {
        get: function () {
            return 1003;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_PAGE_UPDATE_REPORT", {
        get: function () {
            return 1004;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "TYPE_LIVE_USAGE_REPORT", {
        get: function () {
            return 1007;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_READY", {
        get: function () {
            return "ready";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_DECRYPTED", {
        get: function () {
            return "decrypted";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_NEW_CONTENT", {
        get: function () {
            return "new_content";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_NEW_CONTENT_DELTA", {
        get: function () {
            return "new_content_delta";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_EMPTY_UPDATE", {
        get: function () {
            return "empty_update";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_NEW_META", {
        get: function () {
            return "new_meta";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_RANGES_UPDATED", {
        get: function () {
            return "ranges_updated";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_ACCESS_UPDATED", {
        get: function () {
            return "access_updated";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "EVENT_ERROR", {
        get: function () {
            return "error";
        },
        enumerable: true,
        configurable: true
    });
    Page.create = function (folderId, name, type, template, organization_public) {
        if (type === void 0) { type = 0; }
        if (organization_public === void 0) { organization_public = false; }
        var q = $q.defer();
        if (template) {
            var page_1 = new Page(template.id, template.domain_id);
            page_1.on(page_1.EVENT_READY, function () {
                page_1.clone(folderId, name)
                    .then(q.resolve, q.reject)
                    .finally(function () {
                    page_1.destroy();
                });
            });
        }
        else {
            api.createPage({
                domainId: folderId,
                data: {
                    name: name,
                    special_page_type: type,
                    organization_public: organization_public
                }
            }).then(function (res) {
                var page = new Page(res.data.id, folderId);
                page.on(page.EVENT_READY, function () {
                    page.stop();
                    q.resolve(page);
                });
                page.on(page.EVENT_ERROR, function (err) {
                    page.stop();
                    q.reject(err);
                });
            }, function (err) {
                q.reject(err);
            });
        }
        return q.promise;
    };
    Object.defineProperty(Page.prototype, "encryptionKeyPull", {
        set: function (key) {
            this._encryptionKeyPull = key;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "encryptionKeyPush", {
        set: function (key) {
            this._encryptionKeyPush = key;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "data", {
        get: function () {
            return this._data;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Page.prototype, "access", {
        get: function () {
            return this._access;
        },
        enumerable: true,
        configurable: true
    });
    Page.prototype.start = function () {
        if (!this.updatesOn) {
            this._provider.start();
            this.updatesOn = true;
        }
    };
    Page.prototype.stop = function () {
        if (this.updatesOn) {
            this._provider.stop();
            this.updatesOn = false;
        }
    };
    Page.prototype.push = function (forceFull, otherRequestData) {
        var _this = this;
        if (forceFull === void 0) { forceFull = false; }
        if (otherRequestData === void 0) { otherRequestData = {}; }
        var q = $q.defer();
        var currentData = _.clone(this.Content ? this.Content._original : undefined);
        var delta = false;
        var onSuccess = function (data) {
            _this.Content.dirty = false;
            _this.Content.update(_this.Content.current);
            data = extend({}, _this._data, data.data);
            if (_this._provider instanceof Provider_2.ProviderREST) {
                _this._provider.seqNo = data.seq_no;
            }
            var diff = currentData ? utils.comparePageContent(currentData, _this.Content.current, true) : false;
            if (diff) {
                data.diff = diff;
            }
            else {
                data.diff = undefined;
            }
            data._provider = false;
            if (delta) {
                _this.emit(_this.EVENT_NEW_CONTENT_DELTA, data);
                if (_this._provider instanceof Provider_2.ProviderREST) {
                    _this._provider.requestOngoing = false;
                }
            }
            else {
                _this.emit(_this.EVENT_NEW_CONTENT, data);
            }
            q.resolve(data);
        };
        if (!this._data.encryption_type_to_use &&
            !this._data.encryption_type_used &&
            this.Content.canDoDelta &&
            !forceFull) {
            delta = true;
            if (this._provider instanceof Provider_2.ProviderREST) {
                this._provider.requestOngoing = true;
            }
            this.pushDelta(this.Content.getDelta(true), otherRequestData).then(onSuccess, q.reject);
        }
        else {
            this.pushFull(this.Content.getFull(), otherRequestData).then(onSuccess, q.reject);
        }
        return q.promise;
    };
    Page.prototype.saveMeta = function (data) {
        var _this = this;
        var q = $q.defer();
        if (data.encryption_type_to_use === 0) {
            data.encryption_key_to_use = "";
        }
        api.savePageSettings({
            domainId: this._folderId,
            pageId: this._pageId,
            data: data
        }).then(function (res) {
            _this._data = extend({}, _this._data, res.data);
            q.resolve(res);
        }, function (err) {
            q.reject(utils.parseApiError(err, "Could not save page settings"));
        });
        return q.promise;
    };
    Page.prototype.setAsFoldersDefault = function () {
        var _this = this;
        var q = $q.defer();
        var requestData = {
            domainId: this._folderId,
            data: {
                default_page_id: this._pageId
            }
        };
        api.setDomainDefault(requestData).then(function (res) {
            _this._access.is_users_default_page = true;
            q.resolve(res);
        }, q.reject);
        return q.promise;
    };
    Page.prototype.del = function () {
        var requestData = {
            domainId: this._folderId,
            pageId: this._pageId
        };
        return api.deletePage(requestData);
    };
    Page.prototype.decrypt = function (key) {
        if (!key) {
            key = this._encryptionKeyPull;
        }
        if (this._data.encryption_type_used && !key.passphrase) {
            this.decrypted = false;
            return;
        }
        if (this._data.encryption_type_used) {
            if (!crypto) {
                this.emit(this.EVENT_ERROR, new Error("Encrypted pages not supported"));
                this.decrypted = false;
                return;
            }
            var decrypted = crypto.decryptContent({
                name: key.name,
                passphrase: key.passphrase
            }, this._data.encrypted_content);
            if (decrypted) {
                this.decrypted = true;
                this._data.content = decrypted;
                this._encryptionKeyPull = key;
            }
            else {
                this.decrypted = false;
                this.emit(this.EVENT_ERROR, new Error("Could not decrypt page with key \"" + key.name + "\" and passphrase \"" + key.passphrase + "\""));
            }
        }
        else {
            this.decrypted = true;
        }
        if (this.decrypted) {
            if (this.Content) {
                this.Content.update(this._data.content, false, false, this._data.show_gridlines);
            }
            else {
                this.Content = new Content_1.PageContent(this._data.content);
            }
            this.emit(this.EVENT_DECRYPTED);
        }
    };
    Page.prototype.destroy = function () {
        if (this._provider) {
            this._provider.destroy();
        }
        this.removeEvent();
        this._checkAccess = false;
    };
    Page.prototype.clone = function (folderId, name, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var q = $q.defer();
        if (!this.ready) {
            q.reject("Page is not ready");
            return q.promise;
        }
        if (options.clone_ranges && this._folderId * 1 !== folderId * 1) {
            options.clone_ranges = false;
        }
        Page.create(folderId, name, this._data.special_page_type).then(function (newPage) {
            newPage.Content = _this.Content;
            $q.all([newPage.push(true)]).then(function (res) {
                if (options.clone_ranges) {
                    api.savePageSettings({
                        domainId: folderId,
                        pageId: newPage.data.id,
                        data: {
                            access_rights: _this._data.access_rights,
                            action_definitions: _this._data.action_definitions
                        }
                    }).finally(function () {
                        q.resolve(newPage);
                    });
                }
                else {
                    q.resolve(newPage);
                }
            }, q.reject);
        }, function (err) {
            q.reject(err);
        });
        return q.promise;
    };
    Page.prototype.copy = function (folderId, name, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var q = $q.defer();
        if (!this.ready) {
            q.reject("Page is not ready");
            return q.promise;
        }
        var data = {
            range_access: true,
            action_definitions: true,
            description: true,
            push_interval: true,
            pull_interval: true,
            is_public: true,
            encryption_type_to_use: true,
            encryption_key_to_use: true,
            background_color: true,
            is_obscured_public: true,
            record_history: true,
            symphony_sid: true
        };
        if (!options.access_rights || (options.access_rights && this._folderId * 1 !== folderId * 1)) {
            data.range_access = false;
            data.action_definitions = false;
        }
        var settingsData = {};
        for (var key in data) {
            if (!data[key] || !this._data[key]) {
                continue;
            }
            settingsData[key] = this._data[key];
        }
        Page.create(folderId, name, this._data.special_page_type).then(function (newPage) {
            if (options.content && Object.keys(settingsData).length > 0) {
                newPage.Content = _this.Content;
                newPage.push(true).then(function () {
                    api.savePageSettings({ domainId: folderId, pageId: newPage.data.id, data: settingsData }).then(function () {
                        q.resolve(newPage);
                    }, q.reject);
                }, q.reject);
            }
            else if (options.content) {
                newPage.Content = _this.Content;
                newPage.push(true).then(function () {
                    q.resolve(newPage);
                }, q.reject);
            }
            else if (Object.keys(settingsData).length > 0) {
                api.savePageSettings({ domainId: folderId, pageId: newPage.data.id, data: settingsData }).then(function () {
                    q.resolve(newPage);
                }, q.reject);
            }
            else {
                q.resolve(newPage);
            }
        }, function (err) {
            q.reject(err);
        });
        return q.promise;
    };
    Page.prototype.createProvider = function (ignoreWS) {
        if (this._provider) {
            this._provider.destroy();
        }
        this._provider =
            ignoreWS || !this._supportsWS || typeof io === "undefined" || config.transport === "polling"
                ? new Provider_2.ProviderREST(this._pageId, this._folderId, this._uuid)
                : new Provider_3.ProviderSocket(this._pageId, this._folderId);
        this._checkAccess = true;
    };
    Page.prototype.init = function (ignoreWS) {
        var _this = this;
        if (!this._supportsWS || typeof io === "undefined") {
            console.warn("[iPushPull] Cannot use websocket technology as it is not supported or websocket library is not included");
        }
        this.createProvider(ignoreWS);
        if (this._uuid) {
            this._accessLoaded = true;
            this.registerListeners();
            return;
        }
        this.Ranges = new Range_2.Ranges(this._folderId, this._pageId);
        this.Ranges.on(this.Ranges.EVENT_UPDATED, function () {
            _this.emit(_this.EVENT_RANGES_UPDATED);
        });
        this.checkPageAccess();
        this.registerListeners();
        this.setFreezeRange();
    };
    Page.prototype.setFreezeRange = function () {
        var _this = this;
        this.Ranges.ranges.forEach(function (range) {
            if (range.name === "frozen_rows") {
                _this.freeze.row = range.count;
            }
            else if (range.name === "frozen_cols") {
                _this.freeze.col = range.count;
            }
        });
        this.freeze.valid = this.freeze.row > 0 || this.freeze.col > 0;
    };
    Page.prototype.checkPageAccess = function () {
        var _this = this;
        if (!this._checkAccess) {
            return;
        }
        this.getPageAccess().finally(function () {
            $timeout(function () {
                _this.checkPageAccess();
            }, 30000);
        });
    };
    Page.prototype.getPageId = function (folderName, pageName) {
        var q = $q.defer();
        api.getPageByName({ domainId: folderName, pageId: pageName }).then(function (res) {
            q.resolve({ pageId: res.data.id, folderId: res.data.domain_id, wsEnabled: res.ws_enabled });
        }, function (err) {
            q.reject(err);
        });
        return q.promise;
    };
    Page.prototype.getPageAccess = function () {
        var _this = this;
        var q = $q.defer();
        api.getPageAccess({
            domainId: this._folderId,
            pageId: this._pageId
        }).then(function (res) {
            if (res.httpCode && res.httpCode >= 300) {
                q.reject({
                    code: res.httpCode,
                    message: res.data ? res.data.detail : "Unauthorized access"
                });
                return;
            }
            if (_this._error) {
                _this._error = false;
                _this.emit(_this.EVENT_READY, true);
                return;
            }
            var prevAccess = JSON.stringify(_this._access);
            var newAccess = JSON.stringify(res.data);
            _this._access = res.data;
            _this._accessLoaded = true;
            _this.checkReady();
            if (prevAccess !== newAccess) {
                _this.emit(_this.EVENT_ACCESS_UPDATED, {
                    before: prevAccess,
                    after: newAccess
                });
            }
            if (_this._access.ws_enabled && !(_this._provider instanceof Provider_3.ProviderSocket)) {
                if (_this._supportsWS && !_this._wsDisabled) {
                    _this._provider.destroy();
                    console.log("socket", "get");
                    _this._provider = new Provider_3.ProviderSocket(_this._pageId, _this._folderId);
                    _this.registerListeners();
                }
                else {
                    console.warn("Page should use websockets but cannot switch as client does not support them");
                }
            }
            q.resolve();
        }, function (err) {
            _this.onPageError(err);
            q.reject();
        });
        return q.promise;
    };
    Page.prototype.registerListeners = function () {
        var _this = this;
        this._provider.on("content_update", function (data) {
            data.special_page_type = _this.updatePageType(data.special_page_type);
            var currentData = _.clone(_this.Content ? _this.Content.current : 0);
            _this._data = merge(_this._data, data);
            _this.decrypt();
            _this._contentLoaded = true;
            _this.checkReady();
            var diff = currentData ? utils.comparePageContent(currentData, _this.Content.current, true) : false;
            if (diff) {
                _this._data.diff = diff;
            }
            else {
                _this._data.diff = undefined;
            }
            _this._data._provider = true;
            _this.emit(_this.EVENT_NEW_CONTENT, _this._data);
        });
        this._provider.on("meta_update", function (data) {
            data.special_page_type = _this.updatePageType(data.special_page_type);
            delete data.content;
            delete data.encrypted_content;
            if (!_this.Ranges) {
                _this.Ranges = new Range_2.Ranges(data.domain_id, data.id);
                _this.Ranges.on(_this.Ranges.EVENT_UPDATED, function () {
                    _this.emit(_this.EVENT_RANGES_UPDATED);
                });
            }
            var rangesUpdates = false;
            if (data.range_access && _this._data.range_access !== data.range_access) {
                _this.Ranges.parse(data.range_access || "[]");
                data.access_rights = JSON.stringify(_this.Ranges.ranges);
                rangesUpdates = true;
            }
            if (_this._data.action_definitions !== data.action_definitions) {
                rangesUpdates = true;
            }
            _this._data = extend({}, _this._data, data);
            _this._metaLoaded = true;
            _this.checkReady();
            _this.emit(_this.EVENT_NEW_META, data);
            if (rangesUpdates) {
                _this.emit(_this.EVENT_RANGES_UPDATED);
            }
            if (data.ws_enabled && !(_this._provider instanceof Provider_3.ProviderSocket)) {
                if (_this._supportsWS && !_this._wsDisabled) {
                    _this._provider.destroy();
                    _this._provider = new Provider_3.ProviderSocket(_this._pageId, _this._folderId);
                    _this.registerListeners();
                }
                else {
                    console.warn("Page should use websockets but cannot switch as client does not support them");
                }
            }
        });
        this._provider.on("error", this.onPageError);
        this._provider.on("empty_update", function () {
            _this.emit(_this.EVENT_EMPTY_UPDATE, _this._data);
        });
    };
    Page.prototype.pushFull = function (content, otherRequestData) {
        var _this = this;
        if (otherRequestData === void 0) { otherRequestData = {}; }
        var q = $q.defer();
        if (this._data.encryption_type_to_use) {
            if (!this._encryptionKeyPush || this._data.encryption_key_to_use !== this._encryptionKeyPush.name) {
                q.reject("None or wrong encryption key");
                return q.promise;
            }
            var encrypted = this.encrypt(this._encryptionKeyPush, content);
            if (encrypted) {
                this._data.encrypted_content = encrypted;
                this._data.encryption_type_used = 1;
                this._data.encryption_key_used = this._encryptionKeyPush.name;
                this._encryptionKeyPull = _.clone(this._encryptionKeyPush);
            }
            else {
                q.reject("Encryption failed");
                return q.promise;
            }
        }
        else {
            this._data.encryption_key_used = "";
            this._data.encryption_type_used = 0;
            this._data.content = content;
        }
        var data = {
            content: !this._data.encryption_type_used ? this._data.content : "",
            encrypted_content: this._data.encrypted_content,
            encryption_type_used: this._data.encryption_type_used,
            encryption_key_used: this._data.encryption_key_used
        };
        data = extend({}, data, otherRequestData);
        var requestData = {
            domainId: this._folderId,
            pageId: this._pageId,
            data: data
        };
        api.savePageContent(requestData).then(function (res) {
            _this._data.seq_no = res.data.seq_no;
            q.resolve(res);
        }, q.reject);
        return q.promise;
    };
    Page.prototype.pushDelta = function (data, otherRequestData) {
        if (otherRequestData === void 0) { otherRequestData = {}; }
        var q = $q.defer();
        data = extend({}, data, otherRequestData);
        var requestData = {
            domainId: this._folderId,
            pageId: this._pageId,
            data: data
        };
        api.savePageContentDelta(requestData).then(q.resolve, q.reject);
        return q.promise;
    };
    Page.prototype.checkReady = function () {
        if (this._contentLoaded && this._metaLoaded && this._accessLoaded && !this.ready) {
            this.ready = true;
            this.emit(this.EVENT_READY);
        }
    };
    Page.prototype.updatePageType = function (pageType) {
        if ((pageType > 0 && pageType < 5) || pageType === 7) {
            pageType += 1000;
        }
        return pageType;
    };
    Page.prototype.encrypt = function (key, content) {
        return crypto.encryptContent(key, content);
    };
    return Page;
}(Emitter_1.default));
//# sourceMappingURL=Page.js.map