"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = require("../Emitter");
var $q, $timeout, api, auth, storage, config;
var Providers = (function () {
    function Providers(q, timeout, ippApi, ippAuth, ippStorage, ippConf) {
        $q = q;
        $timeout = timeout;
        api = ippApi;
        auth = ippAuth;
        storage = ippStorage;
        config = ippConf;
    }
    return Providers;
}());
exports.Providers = Providers;
var ProviderREST = (function (_super) {
    __extends(ProviderREST, _super);
    function ProviderREST(_pageId, _folderId, _uuid) {
        var _this = _super.call(this) || this;
        _this._pageId = _pageId;
        _this._folderId = _folderId;
        _this._uuid = _uuid;
        _this._stopped = false;
        _this._requestOngoing = false;
        _this._timeout = 1000;
        _this._seqNo = 0;
        _this._error = false;
        _this.start();
        return _this;
    }
    Object.defineProperty(ProviderREST.prototype, "seqNo", {
        set: function (seqNo) { this._seqNo = seqNo; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderREST.prototype, "requestOngoing", {
        set: function (value) { this._requestOngoing = value; },
        enumerable: true,
        configurable: true
    });
    ProviderREST.prototype.start = function () {
        this._stopped = false;
        this.startPolling();
    };
    ProviderREST.prototype.stop = function () {
        this._stopped = true;
        $timeout.cancel(this._timer);
    };
    ProviderREST.prototype.destroy = function () {
        this.stop();
        this.removeEvent();
    };
    ProviderREST.prototype.getData = function () {
        return this._data;
    };
    ProviderREST.prototype.startPolling = function () {
        var _this = this;
        this.load();
        this._timer = $timeout(function () {
            _this.startPolling();
        }, this._timeout);
    };
    ProviderREST.prototype.load = function (ignoreSeqNo) {
        var _this = this;
        if (ignoreSeqNo === void 0) { ignoreSeqNo = false; }
        var q = $q.defer();
        if (this._requestOngoing || this._stopped) {
            q.reject({});
            return q.promise;
        }
        this._requestOngoing = true;
        var success = function (res) {
            if (res.httpCode === 200 || res.httpCode === 204) {
                if (res.httpCode === 200) {
                    _this._seqNo = res.data.seq_no;
                    _this._timeout = res.data.pull_interval * 1000;
                    _this._data = res.data;
                    _this.emit("content_update", _this.tempGetContentOb(res.data));
                    _this.emit("meta_update", _this.tempGetSettingsOb(res.data));
                }
                else {
                    _this.emit("empty_update");
                }
                _this._error = false;
                q.resolve(res.data);
            }
            else {
                _this.onError(res.data);
                q.reject({});
            }
        };
        if (this._uuid) {
            api.getPageByUuid({
                uuid: this._uuid,
                seq_no: (!ignoreSeqNo) ? this._seqNo : undefined,
            }).then(success, function (err) {
                _this.onError(err);
                q.reject(err);
            }).finally(function () {
                _this._requestOngoing = false;
            });
        }
        else {
            api.getPage({
                domainId: this._folderId,
                pageId: this._pageId,
                seq_no: (!ignoreSeqNo) ? this._seqNo : undefined,
            }).then(success, function (err) {
                _this.onError(err);
                q.reject(err);
            }).finally(function () {
                _this._requestOngoing = false;
            });
        }
        return q.promise;
    };
    ProviderREST.prototype.onError = function (err) {
        this._error = true;
        this.emit("error", err);
    };
    ProviderREST.prototype.tempGetContentOb = function (data) {
        return {
            id: data.id,
            background_color: data.background_color,
            domain_id: data.domain_id,
            seq_no: data.seq_no,
            content_modified_timestamp: data.content_modified_timestamp,
            content: data.content,
            content_modified_by: data.content_modified_by,
            push_interval: data.push_interval,
            pull_interval: data.pull_interval,
            is_public: data.is_public,
            description: data.description,
            encrypted_content: data.encrypted_content,
            encryption_key_used: data.encryption_key_used,
            encryption_type_used: data.encryption_type_used,
            record_history: data.record_history,
            special_page_type: data.special_page_type,
            symphony_sid: data.symphony_sid,
            show_gridlines: data.show_gridlines,
        };
    };
    ProviderREST.prototype.tempGetSettingsOb = function (data) {
        return JSON.parse(JSON.stringify(data));
    };
    return ProviderREST;
}(Emitter_1.default));
exports.ProviderREST = ProviderREST;
var ProviderSocket = (function (_super) {
    __extends(ProviderSocket, _super);
    function ProviderSocket(_pageId, _folderId) {
        var _this = _super.call(this) || this;
        _this._pageId = _pageId;
        _this._folderId = _folderId;
        _this._stopped = false;
        _this._redirectCounter = 0;
        _this._redirectLimit = 10;
        _this._error = false;
        _this.onConnect = function () {
            return;
        };
        _this.onReconnectingError = function (attempt) {
            _this.emit("error", { message: "Streaming is currently not available", code: 502, type: "redirect" });
            _this.destroy(true);
            return;
        };
        _this.onReconnectError = function () {
            _this.destroy(false);
            _this._wsUrl = config.ws_url + "/page/" + _this._pageId;
            _this.start();
        };
        _this.onDisconnect = function () {
            return;
        };
        _this.onRedirect = function (msg) {
            _this._wsUrl = msg;
            _this._redirectCounter++;
            if (_this._redirectCounter >= _this._redirectLimit) {
                console.log("socket", _this._redirectCounter);
                _this.emit("error", { message: "Streaming connection limit reached", code: 500, type: "redirect" });
                _this.destroy(true);
                _this._redirectCounter = 0;
                return;
            }
            else {
                _this.destroy(false);
            }
            _this.start();
        };
        _this.onPageContent = function (data) {
            _this._data = data;
            if (!_this._stopped) {
                $timeout(function () {
                    _this.emit("content_update", data);
                });
            }
        };
        _this.onPageSettings = function (data) {
            $timeout(function () {
                _this.emit("meta_update", data);
            });
        };
        _this.onPageError = function (data) {
            $timeout(function () {
                _this._error = true;
                if (data.code === 401) {
                    auth.emit(auth.EVENT_401);
                }
                else {
                    _this.emit("error", data);
                }
            });
        };
        _this.onOAuthError = function (data) {
        };
        _this.onAuthRefresh = function () {
            var dummy = _this._pageId;
            _this.start();
        };
        _this.supportsWebSockets = function () {
            return "WebSocket" in window || "MozWebSocket" in window;
        };
        _this._wsUrl = config.ws_url + "/page/" + _this._pageId;
        _this.start();
        return _this;
    }
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_ERROR", {
        get: function () {
            return "page_error";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_CONTENT", {
        get: function () {
            return "page_content";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_PUSH", {
        get: function () {
            return "page_push";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_SETTINGS", {
        get: function () {
            return "page_settings";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_DATA", {
        get: function () {
            return "page_data";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_USER_JOINED", {
        get: function () {
            return "page_user_joined";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProviderSocket, "SOCKET_EVENT_PAGE_USER_LEFT", {
        get: function () {
            return "page_user_left";
        },
        enumerable: true,
        configurable: true
    });
    ProviderSocket.prototype.start = function () {
        this._stopped = false;
        if (!this._socket || !this._socket.connected) {
            auth.on(auth.EVENT_LOGIN_REFRESHED, this.onAuthRefresh);
            this.init();
        }
        else {
            if (this._data) {
            }
        }
    };
    ProviderSocket.prototype.stop = function () {
        this._stopped = true;
    };
    ProviderSocket.prototype.destroy = function (hard) {
        if (hard === void 0) { hard = true; }
        this._socket.removeAllListeners();
        this._socket.disconnect();
        this.stop();
        auth.off(auth.EVENT_LOGIN_REFRESHED, this.onAuthRefresh);
        if (hard) {
            this.removeEvent();
        }
    };
    ProviderSocket.prototype.getData = function () {
        return this._data;
    };
    ProviderSocket.prototype.init = function () {
        this._socket = this.connect();
        this._socket.on("connect", this.onConnect);
        this._socket.on("reconnecting", this.onReconnectingError);
        this._socket.on("reconnect_error", this.onReconnectError);
        this._socket.on("redirect", this.onRedirect);
        this._socket.on(ProviderSocket.SOCKET_EVENT_PAGE_CONTENT, this.onPageContent);
        this._socket.on(ProviderSocket.SOCKET_EVENT_PAGE_SETTINGS, this.onPageSettings);
        this._socket.on(ProviderSocket.SOCKET_EVENT_PAGE_ERROR, this.onPageError);
        this._socket.on("oauth_error", this.onOAuthError);
        this._socket.on("disconnect", this.onDisconnect);
        this._stopped = false;
    };
    ProviderSocket.prototype.connect = function () {
        var token = api.tokens && api.tokens.access_token ? api.tokens.access_token : (storage ? storage.persistent.get("access_token") : "");
        var query = [
            "access_token=" + token,
        ];
        query = query.filter(function (val) {
            return (val.length > 0);
        });
        return io.connect(this._wsUrl, {
            query: query.join("&"),
            transports: (this.supportsWebSockets()) ? ["websocket"] : ["polling"],
            forceNew: true,
            reconnectionAttempts: 5,
        });
    };
    return ProviderSocket;
}(Emitter_1.default));
exports.ProviderSocket = ProviderSocket;
//# sourceMappingURL=Provider.js.map