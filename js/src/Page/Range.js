"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var dateFormat = require("dateformat");
var Emitter_1 = require("../Emitter");
var $q, api;
var RangesWrap = (function () {
    function RangesWrap(q, ippApi) {
        $q = q;
        api = ippApi;
    }
    return RangesWrap;
}());
exports.RangesWrap = RangesWrap;
var AccessRange = (function () {
    function AccessRange(name, range, config) {
        this.name = name;
        this.range = range;
        this.config = config;
        this.rights = {};
        this.exp = config.exp;
        this.rights = config.rights;
        this.permission = config.permission;
        if (config.range === "self") {
            this.updateRange = {
                from: "R:C",
                to: "R:C"
            };
        }
        else {
            this.updateRange = {
                from: config.range.start,
                to: config.range.end
            };
        }
    }
    AccessRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: this.range.from.row + ":" + this.range.from.col,
            end: this.range.to.row + ":" + this.range.to.col,
            exp: this.exp,
            permission: this.permission,
            range: {
                start: "" + this.updateRange.from,
                end: "" + this.updateRange.to
            },
            rights: this.rights,
            freeze: false,
            style: false,
            button: false,
            notification: false,
            access: true
        };
        return range;
    };
    return AccessRange;
}());
exports.AccessRange = AccessRange;
var NotificationRange = (function () {
    function NotificationRange(name, range, config) {
        this.name = name;
        this.range = range;
        this.config = config;
        this.exp = config.exp;
        this.message = config.message;
    }
    NotificationRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: this.range.from.row + ":" + this.range.from.col,
            end: this.range.to.row + ":" + this.range.to.col,
            exp: this.exp,
            message: this.message,
            freeze: false,
            style: false,
            button: false,
            notification: true
        };
        return range;
    };
    return NotificationRange;
}());
exports.NotificationRange = NotificationRange;
var ListRange = (function () {
    function ListRange(name, range, config) {
        this.name = name;
        this.range = range;
        this.config = config;
        this.exp = config.exp;
        this.message = config.message;
    }
    ListRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: this.range.from.row + ":" + this.range.from.col,
            end: this.range.to.row + ":" + this.range.to.col,
            exp: this.exp,
            message: this.message,
            freeze: false,
            style: false,
            button: false,
            notification: true
        };
        return range;
    };
    return ListRange;
}());
exports.ListRange = ListRange;
var StyleRange = (function () {
    function StyleRange(name, range, config) {
        this.name = name;
        this.range = range;
        this.config = config;
        this.rangeExp = false;
        this.exp = config.exp;
        this.formatting = config.formatting;
    }
    StyleRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: this.range.from.row + ":" + this.range.from.col,
            end: this.range.to.row + ":" + this.range.to.col,
            exp: this.exp,
            formatting: this.formatting,
            freeze: false,
            button: false,
            style: true
        };
        return range;
    };
    StyleRange.prototype.parseFormat = function (format, row, col, rowEnd, colEnd) {
        if (rowEnd === void 0) { rowEnd = -1; }
        if (colEnd === void 0) { colEnd = -1; }
        var start = format.start
            .replace(/R/gi, row)
            .replace(/C/gi, col)
            .split(":");
        var end = format.end
            .replace(/R/gi, row)
            .replace(/C/gi, col)
            .split(":");
        var startRow = parseInt(eval(start[0]), 10);
        var startCol = parseInt(eval(start[1]), 10);
        var endRow = parseInt(eval(end[0]), 10);
        var endCol = parseInt(eval(end[1]), 10);
        return {
            from: {
                row: startRow,
                col: startCol
            },
            to: {
                row: endRow < 0 ? rowEnd : endRow,
                col: endCol < 0 ? colEnd : endCol
            },
            style: format.style
        };
    };
    StyleRange.prototype.match = function (value) {
        var VALUE = parseFloat(value) || value;
        try {
            return eval(this.exp);
        }
        catch (e) {
            return false;
        }
    };
    return StyleRange;
}());
exports.StyleRange = StyleRange;
var ButtonRange = (function () {
    function ButtonRange(name, range, config) {
        this.name = name;
        this.range = range;
        this.config = config;
        this.rangeExp = false;
        this.exp = config.exp;
        this.setters = config.setters;
        if (config.range === "self") {
            this.updateRange = {
                from: "R:C",
                to: "R:C"
            };
        }
        else {
            this.updateRange = {
                from: config.range.start,
                to: config.range.end
            };
        }
    }
    ButtonRange.prototype.run = function (cells, vars) {
        var _this = this;
        if (vars === void 0) { vars = {}; }
        var cellUpdates = [];
        cells.forEach2(function (row, rowIndex) {
            if (!row)
                return;
            row.forEach2(function (col, colIndex) {
                if (!col)
                    return;
                cellUpdates.push(_this.data(rowIndex, colIndex, col, vars));
            });
        });
        return cellUpdates;
    };
    ButtonRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: this.range.from.row + ":" + this.range.from.col,
            end: this.range.to.row + ":" + this.range.to.col,
            exp: this.exp,
            setters: this.setters,
            range: {
                start: "" + this.updateRange.from,
                end: "" + this.updateRange.to
            },
            rangeExp: this.rangeExp,
            freeze: false,
            style: false,
            button: true
        };
        if (this.config.range === "self") {
            range.range = "self";
        }
        return range;
    };
    ButtonRange.prototype.parseRange = function (pos) {
        var fromRow = this.updateRange.from
            .split(":")[0]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        var fromCol = this.updateRange.from
            .split(":")[1]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        var toRow = this.updateRange.to
            .split(":")[0]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        var toCol = this.updateRange.to
            .split(":")[1]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        return {
            from: {
                row: eval(fromRow),
                col: eval(fromCol)
            },
            to: {
                row: eval(toRow),
                col: eval(toCol)
            }
        };
    };
    ButtonRange.prototype.actionRotate = function (value, arr) {
        if (!arr || !arr.length) {
            return value;
        }
        var n = value;
        for (var i = 0; i < arr.length; i++) {
            if (n !== arr[i]) {
                continue;
            }
            if (!arr[i + 1]) {
                n = arr[0];
            }
            else {
                n = arr[i + 1];
            }
            break;
        }
        if (n === value) {
            n = arr[0];
        }
        return n;
    };
    ButtonRange.prototype.actionToday = function (format) {
        if (format === void 0) { format = "yyyy-mm-dd"; }
        var now = new Date();
        return dateFormat(now, format);
    };
    ButtonRange.prototype.actionLink = function (value, url) {
        if (url === void 0) { url = ""; }
        window.open(url, "_blank");
        return value;
    };
    ButtonRange.prototype.actionConcat = function (separator, arr) {
        if (typeof arr !== "object") {
            arr = [];
            for (var i = 1; i < arguments.length; i++) {
                arr.push("" + arguments[i]);
            }
        }
        return arr.join(separator);
    };
    ButtonRange.prototype.data = function (row, col, cell, vars) {
        if (vars === void 0) { vars = {}; }
        var data = {
            value: cell.value,
            formatted_value: cell.formatted_value
        };
        vars["=CONCAT"] = "this.actionConcat";
        vars["=ROTATE"] = "this.actionRotate";
        vars["=TODAY"] = "this.actionToday";
        vars["=LINK\\("] = "this.actionLink(VALUE, ";
        var exp = "" + this.exp;
        for (var key in vars) {
            exp = exp.replace(new RegExp(key, "g"), vars[key]);
        }
        var VALUE = !/^[0-9\.\-]+$/.test(cell.value) || isNaN(cell.value) ? cell.value : parseFloat(cell.value);
        try {
            VALUE = eval(exp);
        }
        catch (e) {
            VALUE = VALUE;
        }
        data.value = VALUE;
        data.formatted_value = VALUE;
        return {
            row: row,
            col: col,
            data: data
        };
    };
    return ButtonRange;
}());
exports.ButtonRange = ButtonRange;
var PermissionRange = (function () {
    function PermissionRange(name, rowStart, rowEnd, colStart, colEnd, permissions) {
        if (rowStart === void 0) { rowStart = 0; }
        if (rowEnd === void 0) { rowEnd = 0; }
        if (colStart === void 0) { colStart = 0; }
        if (colEnd === void 0) { colEnd = 0; }
        this.name = name;
        this.rowStart = rowStart;
        this.rowEnd = rowEnd;
        this.colStart = colStart;
        this.colEnd = colEnd;
        this._permissions = {
            ro: [],
            no: []
        };
        if (permissions) {
            this._permissions = permissions;
        }
    }
    PermissionRange.prototype.setPermission = function (userId, permission) {
        if (this._permissions.ro.indexOf(userId) >= 0) {
            this._permissions.ro.splice(this._permissions.ro.indexOf(userId), 1);
        }
        if (this._permissions.no.indexOf(userId) >= 0) {
            this._permissions.no.splice(this._permissions.no.indexOf(userId), 1);
        }
        if (permission) {
            this._permissions[permission].push(userId);
        }
    };
    PermissionRange.prototype.getPermission = function (userId) {
        var permission = "";
        if (this._permissions.ro.indexOf(userId) >= 0) {
            permission = "ro";
        }
        else if (this._permissions.no.indexOf(userId) >= 0) {
            permission = "no";
        }
        return permission;
    };
    PermissionRange.prototype.toObject = function () {
        return {
            name: this.name,
            start: this.rowStart + ":" + this.colStart,
            end: this.rowEnd + ":" + this.colEnd,
            rights: this._permissions,
            freeze: false,
            button: false,
            style: false,
            access: false,
            notification: false,
            list: false
        };
    };
    return PermissionRange;
}());
exports.PermissionRange = PermissionRange;
var FreezingRange = (function () {
    function FreezingRange(name, subject, count) {
        if (subject === void 0) { subject = "rows"; }
        if (count === void 0) { count = 1; }
        this.name = name;
        this.subject = subject;
        this.count = count;
    }
    Object.defineProperty(FreezingRange, "SUBJECT_ROWS", {
        get: function () {
            return "rows";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FreezingRange, "SUBJECT_COLUMNS", {
        get: function () {
            return "cols";
        },
        enumerable: true,
        configurable: true
    });
    FreezingRange.prototype.toObject = function () {
        var range = {
            name: this.name,
            start: "0:0",
            end: "",
            rights: { ro: [], no: [] },
            freeze: true,
            button: false,
            style: false
        };
        if (this.subject === FreezingRange.SUBJECT_ROWS) {
            range.end = this.count - 1 + ":-1";
        }
        else {
            range.end = "-1:" + (this.count - 1);
        }
        return range;
    };
    return FreezingRange;
}());
exports.FreezingRange = FreezingRange;
var Ranges = (function (_super) {
    __extends(Ranges, _super);
    function Ranges(folderId, pageId, pageAccessRights) {
        var _this = _super.call(this) || this;
        _this._ranges = [];
        _this._folderId = folderId;
        _this._pageId = pageId;
        if (pageAccessRights) {
            _this.parse(pageAccessRights);
        }
        return _this;
    }
    Object.defineProperty(Ranges.prototype, "TYPE_PERMISSION_RANGE", {
        get: function () {
            return "permissions";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ranges.prototype, "TYPE_FREEZING_RANGE", {
        get: function () {
            return "freezing";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ranges.prototype, "EVENT_UPDATED", {
        get: function () {
            return "updated";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Ranges.prototype, "ranges", {
        get: function () {
            return this._ranges;
        },
        enumerable: true,
        configurable: true
    });
    Ranges.prototype.setRanges = function (ranges) {
        this._ranges = ranges;
        return this;
    };
    Ranges.prototype.addRange = function (range) {
        if (range instanceof FreezingRange) {
            for (var i = 0; i < this._ranges.length; i++) {
                if (this._ranges[i].subject === range.subject) {
                    this.removeRange(this._ranges[i].name);
                    break;
                }
            }
        }
        var nameUnique = false;
        var newName = range.name;
        var count = 1;
        while (!nameUnique) {
            nameUnique = true;
            for (var i = 0; i < this._ranges.length; i++) {
                if (this._ranges[i].name === newName) {
                    nameUnique = false;
                    newName = range.name + "_" + count;
                    count++;
                }
            }
        }
        range.name = newName;
        this._ranges.push(range);
        return this;
    };
    Ranges.prototype.removeRange = function (rangeName) {
        var index = -1;
        for (var i = 0; i < this._ranges.length; i++) {
            if (this._ranges[i].name === rangeName) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            this._ranges.splice(index, 1);
        }
        return this;
    };
    Ranges.prototype.save = function () {
        var _this = this;
        var q = $q.defer();
        var ranges = [];
        for (var i = 0; i < this._ranges.length; i++) {
            ranges.push(this._ranges[i].toObject());
        }
        var requestData = {
            domainId: this._folderId,
            pageId: this._pageId,
            data: {
                range_access: ranges.length ? JSON.stringify(ranges) : '',
            }
        };
        api.savePageSettings(requestData).then(function (data) {
            _this.emit(_this.EVENT_UPDATED);
            q.resolve(data);
        }, q.reject);
        return q.promise;
    };
    Ranges.prototype.parse = function (pageAccessRights) {
        var ar = JSON.parse(pageAccessRights);
        this._ranges = [];
        for (var i = 0; i < ar.length; i++) {
            if (ar[i].data) {
                continue;
            }
            var range = {
                from: {
                    row: parseInt(ar[i].start.split(":")[0], 10),
                    col: parseInt(ar[i].start.split(":")[1], 10)
                },
                to: {
                    row: parseInt(ar[i].end.split(":")[0], 10),
                    col: parseInt(ar[i].end.split(":")[1], 10)
                }
            };
            if (ar[i].style) {
                this._ranges.push(new StyleRange(ar[i].name, range, ar[i]));
            }
            else if (ar[i].notification) {
                this._ranges.push(new NotificationRange(ar[i].name, range, ar[i]));
            }
            else if (ar[i].list) {
                this._ranges.push(new ListRange(ar[i].name, range, ar[i]));
            }
            else if (ar[i].access) {
                this._ranges.push(new AccessRange(ar[i].name, range, ar[i]));
            }
            else if (ar[i].button) {
                this._ranges.push(new ButtonRange(ar[i].name, range, ar[i]));
            }
            else if (ar[i].freeze) {
                var subject = range.to.col >= 0 ? "cols" : "rows";
                var count = range.to.col >= 0 ? range.to.col + 1 : range.to.row + 1;
                this._ranges.push(new FreezingRange(ar[i].name, subject, count));
            }
            else {
                this._ranges.push(new PermissionRange(ar[i].name, range.from.row, range.to.row, range.from.col, range.to.col, ar[i].rights));
            }
        }
        return this._ranges;
    };
    return Ranges;
}(Emitter_1.default));
exports.Ranges = Ranges;
//# sourceMappingURL=Range.js.map