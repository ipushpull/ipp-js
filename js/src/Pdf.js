"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = require("./Emitter");
var Hammer = require("hammerjs");
var ua = require("ua-parser-js");
var $q, storage, config;
var PdfWrap = (function () {
    function PdfWrap(q, ippStorage, ippConf) {
        $q = q;
        storage = ippStorage;
        config = ippConf;
        return Pdf;
    }
    PdfWrap.$inject = [
        "$q",
        "ippStorageService",
        "ippConfig",
    ];
    return PdfWrap;
}());
exports.default = PdfWrap;
var Pdf = (function (_super) {
    __extends(Pdf, _super);
    function Pdf(container) {
        var _this = _super.call(this) || this;
        _this.container = container;
        _this.data = [];
        _this.rendering = false;
        _this.annotationId = "pdfAnnotations";
        _this.pdfPages = 1;
        _this.pdfPage = 1;
        _this._fit = "contain";
        _this.touch = false;
        _this.hammer = false;
        _this.scale = 1;
        _this.ratio = 1;
        _this.ratioScale = 1;
        _this.scrollbars = {
            width: 8,
            inset: false,
            scrolling: false,
            x: {
                size: 0,
                offset: 0,
                offsetPercent: 0,
                label: "width",
                style: "margin-left",
                show: false,
                anchor: "left",
                track: undefined,
                handle: undefined
            },
            y: {
                size: 0,
                offset: 0,
                offsetPercent: 0,
                label: "height",
                style: "margin-top",
                show: false,
                anchor: "top",
                track: undefined,
                handle: undefined
            }
        };
        _this.onResizeEvent = function (evt) {
            _this.setAnnotationsBounds();
            _this.updateScrollbars();
            _this.setFit(_this.fit);
        };
        _this.onWheelEvent = function (evt) {
            var offset = Math.abs(evt.deltaY) >= 100 ? Math.round(Math.abs(evt.deltaY) / 5) : 20;
            var direction = evt.deltaY > 0 ? 1 : -1;
            offset *= direction;
            var axis = "y";
            if (_this.canvasBounds.height <= _this.containerBounds.height && _this.canvasBounds.width > _this.containerBounds.width) {
                axis = "x";
            }
            console.log(axis, offset);
            _this.moveScrollbarTrack(axis, offset);
        };
        _this.onMouseUp = function (evt) {
            console.log("onMouseUp");
            _this.scrollbars.scrolling = false;
            ["x", "y"].forEach2(function (axis) {
                _this.scrollbars[axis].drag = false;
            });
        };
        _this.onMouseDown = function (evt) {
        };
        _this.onKeydown = function (evt) {
            switch (evt.keyCode) {
                case 37:
                    _this.page("prev");
                    break;
                case 39:
                    _this.page("next");
                    break;
            }
        };
        _this.onMouseMove = function (evt) {
            ["x", "y"].forEach2(function (axis) {
                if (!_this.scrollbars[axis].drag || !_this.scrollbars.scrolling) {
                    return;
                }
                var offset = evt[axis] - _this.scrollbars[axis].start;
                _this.moveScrollbarTrack(axis, offset);
                _this.scrollbars[axis].start = evt[axis];
            });
        };
        _this.destroy();
        var parser = new ua();
        var parseResult = parser.getResult();
        _this.touch = parseResult.device && (parseResult.device.type === "tablet" || parseResult.device.type === "mobile");
        _this.pdfLink = new PdfLink();
        _this.pdfLink.register(function (name, value) {
            console.log("pdf", name, value);
            if (name === _this.pdfLink.ON_GOTO_PAGE) {
                _this.pdfPage = value;
                _this.renderPdf();
            }
        });
        if (typeof container === "string") {
            _this.containerEl = document.getElementById(container);
        }
        else {
            _this.containerEl = container;
        }
        _this.containerEl.style["overflow"] = _this.touch ? "auto" : "hidden";
        _this.containerEl.style["text-align"] = "center";
        _this.canvasEl = document.createElement("canvas");
        _this.annotationsEl = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        _this.annotationsEl.style["position"] = "absolute";
        _this.annotationsEl.style["z-index"] = "1";
        _this.annotationsEl.style["left"] = "0";
        _this.annotationsEl.style["top"] = "0";
        _this.containerEl.appendChild(_this.canvasEl);
        _this.containerEl.appendChild(_this.annotationsEl);
        _this.ratio = window.devicePixelRatio;
        _this.scale = _this.ratio;
        _this.ratioScale = _this.scale;
        if (_this.touch) {
            _this.setupTouch();
        }
        else {
            _this.containerEl.addEventListener("wheel", _this.onWheelEvent, false);
            window.addEventListener("mouseup", _this.onMouseUp, false);
            window.addEventListener("mousedown", _this.onMouseDown, false);
            window.addEventListener("mousemove", _this.onMouseMove, false);
            window.addEventListener("keydown", _this.onKeydown, false);
            _this.createScrollbars();
        }
        window.addEventListener("resize", _this.onResizeEvent);
        return _this;
    }
    Object.defineProperty(Pdf.prototype, "ON_LOADING", {
        get: function () { return "loading"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pdf.prototype, "ON_LOADED", {
        get: function () { return "loaded"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Pdf.prototype, "fit", {
        get: function () {
            return this._fit;
        },
        set: function (value) {
            if (["scroll", "width", "height", "contain"].indexOf(value) === -1) {
                return;
            }
            this._fit = value;
        },
        enumerable: true,
        configurable: true
    });
    Pdf.prototype.destroy = function () {
        if (!this.containerEl) {
            return;
        }
        document.removeEventListener("resize", this.onResizeEvent);
        this.containerEl.innerHTML = "";
        if (this.hammer) {
            this.hammer.destroy();
        }
        else {
            this.containerEl.removeEventListener("wheel", this.onWheelEvent, false);
            window.removeEventListener("mouseup", this.onMouseUp, false);
            window.removeEventListener("mousedown", this.onMouseDown, false);
            window.removeEventListener("mousemove", this.onMouseMove, false);
            window.removeEventListener("keydown", this.onKeydown, false);
        }
    };
    Pdf.prototype.render = function (data) {
        var _this = this;
        var q = $q.defer();
        this.data = data;
        if (!this.data || !this.data[0] || !this.data[0][0]) {
            q.reject(false);
            return q.promise;
        }
        this.fileUrl = this.handleURL(this.data[0][0].formatted_value || this.data[0][0].value);
        this.rendering = true;
        var headers = {};
        if (this.fileUrl.indexOf("ipushpull.") >= 0) {
            headers = {
                "Authorization": "Bearer " + storage.persistent.get("access_token"),
                "x-ipp-client": config.api_key,
            };
        }
        var loading = PDFJS.getDocument({
            url: this.fileUrl,
            httpHeaders: headers,
        }, undefined, undefined, function (result) {
            _this.emit(_this.ON_LOADING, result);
        });
        loading.then(function (pdf) {
            _this.pdfObj = pdf;
            _this.pdfLink.setDocument(pdf);
            _this.pdfPages = pdf.numPages;
            _this.renderPdf();
            q.resolve(true);
        }, function (err) {
            q.reject(err);
        });
        return q.promise;
    };
    Pdf.prototype.setFit = function (dimension) {
        if (["scroll", "width", "height", "contain"].indexOf(dimension) === -1) {
            return;
        }
        this.fit = dimension;
        if (dimension === "scroll") {
            this.canvasEl.style["width"] = "auto";
            this.canvasEl.style["height"] = "auto";
        }
        else if (dimension === "width") {
            this.canvasEl.style["width"] = "100%";
            this.canvasEl.style["height"] = "auto";
        }
        else if (dimension === "height") {
            this.canvasEl.style["width"] = "auto";
            this.canvasEl.style["height"] = "100%";
        }
        else if (dimension === "contain") {
            var containerBounds = this.containerEl.getBoundingClientRect();
            var canvasBounds = this.canvasEl.getBoundingClientRect();
            var canvasProp = this.canvasEl.width / this.canvasEl.height;
            var containerProp = containerBounds.width / containerBounds.height;
            if (canvasProp > containerProp) {
                this.canvasEl.style["width"] = "100%";
                this.canvasEl.style["height"] = "auto";
            }
            else {
                this.canvasEl.style["width"] = "auto";
                this.canvasEl.style["height"] = "100%";
            }
        }
        this.containerEl.scrollLeft = 0;
        this.containerEl.scrollTop = 0;
        this.resetScrollbars();
        this.setAnnotationsBounds();
        this.updateScrollbars();
    };
    Pdf.prototype.page = function (direction) {
        if (direction === "next") {
            this.pdfPage++;
        }
        else if (direction === "prev") {
            this.pdfPage--;
        }
        else {
            this.pdfPage = parseInt(direction, 10);
        }
        this.setPage();
    };
    Pdf.prototype.resetScrollbars = function () {
        var _this = this;
        ["x", "y"].forEach2(function (axis) {
            var anchor = _this.scrollbars[axis].anchor;
            _this.scrollbars[axis].offset = 0;
            _this.scrollbars[axis].size = 0;
            _this.scrollbars[axis].show = false;
            _this.canvasEl.style[_this.scrollbars[axis].style] = "0px";
            _this.annotationsEl.style[_this.scrollbars[axis].style] = "0px";
            if (_this.scrollbars[axis].handle) {
                _this.scrollbars[axis].handle.style[anchor] = "0px";
            }
        });
    };
    Pdf.prototype.moveScrollbarTrack = function (axis, offset) {
        this.scrollbars[axis].offset += offset;
        var dimension = this.scrollbars[axis].label;
        var canvasSize = this.canvasBounds[dimension];
        var distance = this.containerBounds[dimension] - this.scrollbars[axis].size;
        if (this.scrollbars[axis].offset > distance) {
            this.scrollbars[axis].offset = distance;
        }
        else if (this.scrollbars[axis].offset < 0) {
            this.scrollbars[axis].offset = 0;
        }
        var anchor = this.scrollbars[axis].anchor;
        this.scrollbars[axis].handle.style[anchor] = this.scrollbars[axis].offset + "px";
        var offsetPercent = this.scrollbars[axis].offset / (this.containerBounds[dimension] - this.scrollbars[axis].size);
        var offsetDistance = (this.containerBounds[dimension] - this.canvasBounds[dimension]) * offsetPercent;
        this.scrollbars[axis].offsetPercent = offsetPercent;
        this.canvasEl.style[this.scrollbars[axis].style] = offsetDistance + "px";
        this.annotationsEl.style[this.scrollbars[axis].style] = offsetDistance + "px";
    };
    Pdf.prototype.createScrollbars = function () {
        var _this = this;
        ["x", "y"].forEach2(function (axis) {
            var anchor = _this.scrollbars[axis].anchor;
            _this.scrollbars[axis].track = document.createElement("div");
            _this.scrollbars[axis].handle = document.createElement("div");
            _this.scrollbars[axis].track.style["position"] = "absolute";
            _this.scrollbars[axis].track.style["z-index"] = 10;
            _this.scrollbars[axis].track.style["display"] = "none";
            _this.scrollbars[axis].track.className = "track";
            _this.scrollbars[axis].handle.style["position"] = "absolute";
            _this.scrollbars[axis].handle.style["z-index"] = 1;
            _this.scrollbars[axis].handle.style["left"] = 0;
            _this.scrollbars[axis].handle.style["top"] = 0;
            _this.scrollbars[axis].handle.className = "handle";
            _this.scrollbars[axis].handle.addEventListener("mousedown", function (evt) {
                console.log("mousedown");
                _this.scrollbars.scrolling = true;
                _this.scrollbars[axis].start = evt[axis];
                _this.scrollbars[axis].drag = true;
                _this.scrollbars[axis].offset = parseInt(_this.scrollbars[axis].handle.style[anchor], 10);
                evt.preventDefault();
            }, false);
            if (axis === "x") {
                _this.scrollbars[axis].track.style["left"] = 0;
                _this.scrollbars[axis].track.style["bottom"] = 0;
                _this.scrollbars[axis].track.style["right"] = 0;
                _this.scrollbars[axis].track.style["height"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].handle.style["height"] = _this.scrollbars.width + "px";
            }
            else {
                _this.scrollbars[axis].track.style["top"] = 0;
                _this.scrollbars[axis].track.style["right"] = 0;
                _this.scrollbars[axis].track.style["bottom"] = 0;
                _this.scrollbars[axis].track.style["width"] = _this.scrollbars.width + "px";
                _this.scrollbars[axis].handle.style["width"] = _this.scrollbars.width + "px";
            }
            _this.scrollbars[axis].track.appendChild(_this.scrollbars[axis].handle);
            _this.containerEl.appendChild(_this.scrollbars[axis].track);
        });
    };
    Pdf.prototype.updateScrollbars = function () {
        var _this = this;
        if (this.touch) {
            return;
        }
        ["x", "y"].forEach2(function (axis) {
            var dimension = _this.scrollbars[axis].label;
            if (_this.containerBounds[dimension] >= _this.canvasBounds[dimension]) {
                _this.scrollbars[axis].track.style.display = "none";
                return;
            }
            var canvasSize = _this.canvasBounds[dimension];
            var size = _this.containerBounds[dimension] * (_this.containerBounds[dimension] / canvasSize);
            _this.scrollbars[axis].handle.style[dimension] = size + "px";
            _this.scrollbars[axis].size = size;
            var anchor = _this.scrollbars[axis].anchor;
            var offsetPercent = _this.scrollbars[axis].offsetPercent;
            var offsetDistance = (_this.containerBounds[dimension] - size) * offsetPercent;
            _this.scrollbars[axis].handle.style[anchor] = offsetDistance + "px";
            _this.scrollbars[axis].track.style.display = "block";
            offsetDistance = (_this.containerBounds[dimension] - _this.canvasBounds[dimension]) * offsetPercent;
            _this.canvasEl.style[_this.scrollbars[axis].style] = offsetDistance + "px";
            _this.annotationsEl.style[_this.scrollbars[axis].style] = offsetDistance + "px";
        });
    };
    Pdf.prototype.setAnnotationsBounds = function () {
        this.containerBounds = this.containerEl.getBoundingClientRect();
        this.canvasBounds = this.canvasEl.getBoundingClientRect();
        this.annotationsEl.style.left = (this.canvasBounds.left - this.containerBounds.left) + "px";
        this.annotationsEl.style.width = this.canvasBounds.width + "px";
        this.annotationsEl.style.height = this.canvasBounds.height + "px";
    };
    Pdf.prototype.setupTouch = function () {
        var _this = this;
        this.hammer = new Hammer(this.containerEl);
        var pan = new Hammer.Pan();
        var pinch = new Hammer.Pinch();
        this.hammer.add([pan, pinch]);
        var width;
        this.hammer.on("pinchstart", function (evt) {
            var rect = _this.canvasEl.getBoundingClientRect();
            width = rect.width;
            _this.fit = "scroll";
            _this.canvasEl.style.width = width + "px";
            _this.canvasEl.style.height = "auto";
        });
        this.hammer.on("pinchmove", function (evt) {
            console.log(evt);
            _this.canvasEl.style.width = (width * evt.scale) + "px";
            _this.setAnnotationsBounds();
        });
        this.hammer.on("pinchend", function (evt) {
        });
        var offsets = { x: 0, y: 0, left: this.containerEl.scrollLeft, top: this.containerEl.scrollTop };
        this.hammer.on("panstart", function (evt) {
            offsets.x = evt.center.x;
            offsets.y = evt.center.y;
            offsets.left = _this.containerEl.scrollLeft;
            offsets.top = _this.containerEl.scrollTop;
        });
        this.hammer.on("panmove", function (evt) {
            _this.containerEl.scrollTop = offsets.top + offsets.y - evt.center.y;
            _this.containerEl.scrollLeft = offsets.left + offsets.x - evt.center.x;
        });
        this.hammer.on("panend", function (evt) {
            ;
        });
    };
    Pdf.prototype.setPage = function () {
        if (this.pdfPage > this.pdfPages) {
            this.pdfPage = 1;
        }
        else if (this.pdfPage <= 0) {
            this.pdfPage = this.pdfPages;
        }
        this.renderPdf();
    };
    Pdf.prototype.renderPdf = function () {
        var _this = this;
        var q = $q.defer();
        this.pdfObj.getPage(this.pdfPage).then(function (page) {
            var viewport = page.getViewport(2);
            var context = _this.canvasEl.getContext("2d");
            _this.canvasEl.height = viewport.height;
            _this.canvasEl.width = viewport.width;
            var renderContext = {
                canvasContext: context,
                viewport: viewport,
            };
            var render = page.render(renderContext);
            render.promise.then(function (a) {
                console.log("renderPdf", a);
                _this.setFit(_this.fit);
                _this.emit(_this.ON_LOADED, true);
                _this.renderAnnotations(page, viewport, context);
                q.resolve(a);
            }, q.reject);
        }, function (err) {
            console.log("renderPdf", err);
        });
        return q.promise;
    };
    Pdf.prototype.renderAnnotations = function (page, viewport, canvas) {
        var _this = this;
        var layer = this.annotationsEl;
        if (!layer) {
            return;
        }
        layer.innerHTML = "";
        this.setAnnotationsBounds();
        console.log("pdf", layer);
        var appendAnchor = function (element, url) {
            var svgNS = _this.annotationsEl.namespaceURI;
            var a = document.createElementNS(svgNS, "a");
            a.setAttribute("class", "pdf-link");
            a.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "#");
            a.addEventListener("click", function (e) {
                console.log("pdf", "click", url);
                if (url.link) {
                    window.open(url.link, "_blank");
                    e.preventDefault();
                }
                else if (url.internal) {
                    _this.page(url.page + 1);
                }
            });
            var rect = document.createElementNS(svgNS, "rect");
            rect.setAttribute("x", element.rect[0]);
            rect.setAttribute("y", viewport.viewBox[3] - element.rect[3]);
            rect.setAttribute("width", element.rect[2] - element.rect[0]);
            rect.setAttribute("height", element.rect[3] - element.rect[1]);
            rect.setAttribute("fill", "rgba(30,144,255,1)");
            a.appendChild(rect);
            _this.annotationsEl.appendChild(a);
        };
        page.getAnnotations().then(function (annotationsData) {
            console.log(annotationsData);
            viewport = viewport.clone({
                dontFlip: true,
            });
            layer.setAttribute("viewBox", viewport.viewBox.join(" "));
            annotationsData.forEach2(function (element) {
                if (element.dest && typeof element.dest === "string") {
                    _this.pdfObj.getDestination(element.dest).then(function (res) {
                        console.log("pdf", res);
                        _this.pdfObj.getPageIndex(res[0]).then(function (page) {
                            console.log("pdf", page);
                            appendAnchor(element, { internal: true, page: page });
                        }, function (err) {
                            console.error("pdf", err);
                        });
                    }, function (err) {
                        console.error("pdf", err);
                    });
                }
                else if (element.url) {
                    appendAnchor(element, { link: element.url });
                }
            });
        });
    };
    Pdf.prototype.handleURL = function (url) {
        if (url.indexOf("dropbox.com") > -1) {
            url = url.split("www.dropbox.com").join("dl.dropboxusercontent.com");
            return config.docs_url + "/default/relay?url=" + url;
        }
        return url;
    };
    return Pdf;
}(Emitter_1.default));
var PdfLink = (function (_super) {
    __extends(PdfLink, _super);
    function PdfLink() {
        var _this = _super.call(this) || this;
        _this._pagesRefCache = undefined;
        return _this;
    }
    Object.defineProperty(PdfLink.prototype, "ON_GOTO_PAGE", {
        get: function () { return "goto_page"; },
        enumerable: true,
        configurable: true
    });
    PdfLink.prototype.setDocument = function (pdfDocument, baseUrl) {
        this.baseUrl = baseUrl;
        this.pdfDocument = pdfDocument;
        this._pagesRefCache = {};
    };
    Object.defineProperty(PdfLink.prototype, "pagesCount", {
        get: function () {
            return this.pdfDocument.numPages;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PdfLink.prototype, "page", {
        get: function () {
            return this.pdfViewer.currentPageNumber;
        },
        set: function (value) {
            this.pdfViewer.currentPageNumber = value;
        },
        enumerable: true,
        configurable: true
    });
    PdfLink.prototype.navigateTo = function (dest) {
        var _this = this;
        var destString = "";
        var goToDestination = function (destRef) {
            var pageNumber = destRef instanceof Object ?
                _this._pagesRefCache[destRef.num + " " + destRef.gen + " R"] :
                (destRef + 1);
            if (pageNumber) {
                if (pageNumber > _this.pagesCount) {
                    pageNumber = _this.pagesCount;
                }
                _this.emit(_this.ON_GOTO_PAGE, pageNumber);
            }
            else {
                _this.pdfDocument.getPageIndex(destRef).then(function (pageIndex) {
                    var pageNum = pageIndex + 1;
                    var cacheKey = destRef.num + " " + destRef.gen + " R";
                    _this._pagesRefCache[cacheKey] = pageNum;
                    goToDestination(destRef);
                });
            }
        };
        var destinationPromise;
        if (typeof dest === "string") {
            destString = dest;
            destinationPromise = this.pdfDocument.getDestination(dest);
        }
        else {
            destinationPromise = Promise.resolve(dest);
        }
        destinationPromise.then(function (destination) {
            dest = destination;
            if (!(destination instanceof Array)) {
                return;
            }
            goToDestination(destination[0]);
        });
    };
    PdfLink.prototype.getDestinationHash = function (dest) {
        if (typeof dest === "string") {
            return this.getAnchorUrl("#" + escape(dest));
        }
        if (dest instanceof Array) {
            var destRef = dest[0];
            var pageNumber = destRef instanceof Object ?
                this._pagesRefCache[destRef.num + " " + destRef.gen + " R"] :
                (destRef + 1);
            if (pageNumber) {
                var pdfOpenParams = this.getAnchorUrl("#page=" + pageNumber);
                var destKind = dest[1];
                if (typeof destKind === "object" && "name" in destKind &&
                    destKind.name === "XYZ") {
                    var scale = (dest[4]);
                    var scaleNumber = parseFloat(scale);
                    if (scaleNumber) {
                        scale = scaleNumber * 100;
                    }
                    pdfOpenParams += "&zoom=" + scale;
                    if (dest[2] || dest[3]) {
                        pdfOpenParams += "," + (dest[2] || 0) + "," + (dest[3] || 0);
                    }
                }
                return pdfOpenParams;
            }
        }
        return "";
    };
    PdfLink.prototype.getAnchorUrl = function (anchor) {
        return (this.baseUrl || "") + anchor;
    };
    PdfLink.prototype.setHash = function (hash) { };
    PdfLink.prototype.executeNamedAction = function (action) { };
    PdfLink.prototype.cachePageRef = function (pageNum, pageRef) {
        var refStr = pageRef.num + " " + pageRef.gen + " R";
        this._pagesRefCache[refStr] = pageNum;
    };
    return PdfLink;
}(Emitter_1.default));
//# sourceMappingURL=Pdf.js.map