import { IIPPConfig } from "./Config";
export interface IStorageProvider {
    prefix: string;
    suffix: string;
    create: (key: string, value: string, expireDays?: number) => void;
    save: (key: string, value: string, expireDays?: number) => void;
    get: (key: string, defaultValue?: any) => any;
    remove: (key: string) => void;
}
export interface IStorageService {
    user: IStorageProvider;
    global: IStorageProvider;
    persistent: IStorageProvider;
}
export declare class StorageService {
    static $inject: string[];
    constructor(ippConfig: IIPPConfig);
}
