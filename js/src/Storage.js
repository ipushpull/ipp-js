"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LocalStorage = (function () {
    function LocalStorage() {
        this.prefix = "ipp";
    }
    LocalStorage.prototype.create = function (key, value) {
        localStorage.setItem(this.makeKey(key), value);
    };
    LocalStorage.prototype.save = function (key, value) {
        return this.create(key, value);
    };
    LocalStorage.prototype.get = function (key, defaultValue) {
        if (defaultValue === void 0) { defaultValue = null; }
        var val = localStorage.getItem(this.makeKey(key));
        if (!val) {
            return defaultValue;
        }
        if (this.isValidJSON(val)) {
            return JSON.parse(val);
        }
        else {
            return val;
        }
    };
    LocalStorage.prototype.remove = function (key) {
        localStorage.removeItem(this.makeKey(key));
    };
    LocalStorage.prototype.makeKey = function (key) {
        if (this.prefix && key.indexOf(this.prefix) !== 0) {
            key = this.prefix + "_" + key;
        }
        if (this.suffix) {
            key = key + "_" + this.suffix;
        }
        return key;
    };
    LocalStorage.prototype.isValidJSON = function (val) {
        try {
            var json = JSON.parse(val);
            return true;
        }
        catch (e) {
            return false;
        }
    };
    return LocalStorage;
}());
var CookieStorage = (function () {
    function CookieStorage() {
        this.prefix = "ipp";
        var domain = document.domain;
        if (document.domain.indexOf("ipushpull") > 0) {
            var domainParts = document.domain.split(".");
            domainParts.splice(0, 1);
            domain = domainParts.join(".");
        }
        this._domain = domain;
    }
    CookieStorage.prototype.create = function (key, value, expireDays, ignorePrefix) {
        var expires = "";
        if (expireDays) {
            var date = new Date();
            date.setTime(date.getTime() + (expireDays * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        var path = "path=/;";
        var newKey = ignorePrefix ? key : this.makeKey(key);
        document.cookie = newKey + "=" + value + expires + "; " + path + " domain=" + this._domain + (this.isSecure() ? ";secure;" : "");
    };
    CookieStorage.prototype.save = function (key, value, expireDays, ignorePrefix) {
        this.create(key, value, expireDays, ignorePrefix);
    };
    CookieStorage.prototype.get = function (key, defaultValue, ignorePrefix) {
        key = ignorePrefix ? key : this.makeKey(key);
        var nameEQ = key + "=";
        var ca = document.cookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === " ") {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEQ) === 0) {
                var val = c.substring(nameEQ.length, c.length);
                if (this.isValidJSON(val)) {
                    return JSON.parse(val);
                }
                else {
                    return val;
                }
            }
        }
        return defaultValue;
    };
    CookieStorage.prototype.remove = function (key) {
        this.create(this.makeKey(key), "", -1);
    };
    CookieStorage.prototype.isSecure = function () {
        return window.location.protocol === "https:";
    };
    CookieStorage.prototype.makeKey = function (key) {
        if (this.prefix && key.indexOf(this.prefix) !== 0) {
            key = this.prefix + "_" + key;
        }
        if (this.suffix) {
            key = key + "_" + this.suffix;
        }
        return key;
    };
    CookieStorage.prototype.isValidJSON = function (val) {
        try {
            var json = JSON.parse(val);
            return true;
        }
        catch (e) {
            return false;
        }
    };
    return CookieStorage;
}());
var StorageService = (function () {
    function StorageService(ippConfig) {
        var userStorage = new LocalStorage();
        userStorage.suffix = "GUEST";
        var globalStorage = new LocalStorage();
        var persistentStorage = (typeof navigator !== "undefined" && navigator.cookieEnabled) ? new CookieStorage() : new LocalStorage();
        if (ippConfig.storage_prefix) {
            userStorage.prefix = ippConfig.storage_prefix;
            globalStorage.prefix = ippConfig.storage_prefix;
            persistentStorage.prefix = ippConfig.storage_prefix;
        }
        return {
            user: userStorage,
            global: globalStorage,
            persistent: persistentStorage,
        };
    }
    StorageService.$inject = ["ippConfig"];
    return StorageService;
}());
exports.StorageService = StorageService;
//# sourceMappingURL=Storage.js.map