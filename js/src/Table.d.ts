import Emitter from "./Emitter";
declare class Table extends Emitter {
    readonly ON_CELL_CLICKED: string;
    readonly ON_CELL_DOUBLE_CLICKED: string;
    readonly ON_TAG_CLICKED: string;
    readonly ON_CELL_CHANGED: string;
    data: any;
    accessRanges: any;
    initialized: boolean;
    dirty: boolean;
    rendering: boolean;
    tags: boolean;
    isTouch: boolean;
    container: string;
    containerEl: any;
    viewEl: any;
    zoomEl: any;
    scrollEl: any;
    tableEl: any;
    tableHeadEl: any;
    tableBodyEl: any;
    tableFrozenRowsEl: any;
    tableFrozenColsEl: any;
    private validStyles;
    private _editing;
    private _tracking;
    private numOfCells;
    private classy;
    private helpers;
    private freeze;
    private freezeRange;
    private accessRangesClassNames;
    private clicked;
    constructor(container: string);
    editing: boolean;
    tracking: boolean;
    init(): boolean;
    render(data: any): boolean;
    update(data: any, content: any): boolean;
    getData(): any;
    setData(data: any): void;
    setAccessRanges(rangeAccess: any): void;
    setRanges(ranges: any, userId?: number): void;
    generateBlankData(): any;
    destroy(): void;
    getEditCell(): any;
    addRow(index: number, data: any): void;
    getRowIndex(): number;
    addColumn(): void;
    getColumnIndex(): number;
    setEditCell(el: any): any;
    freezePanes(): void;
    private renderRow(rowId, rowData);
    private renderCells(row, cellsData, startIndex);
    private setCellValues(cell, cellsData);
    private clickCellLink;
    private processLink(linkData);
    private getCellValue(cellData, escapeHtml);
    private onTableClick;
    private onWindowScroll;
    private showInput(cell);
    private nextCellInput(cell, direction?);
    private getClickedCell(clickedEl);
    private softMerges(rowEl);
    private updateFreezePanes(target);
    private clearCellClassNames(classNames);
}
export default Table;
