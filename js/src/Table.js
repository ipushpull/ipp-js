"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Emitter_1 = require("./Emitter");
var Classy_1 = require("./Classy");
var Helpers_1 = require("./Helpers");
var _ = require("underscore");
var Table = (function (_super) {
    __extends(Table, _super);
    function Table(container) {
        var _this = _super.call(this) || this;
        _this.data = [];
        _this.accessRanges = [];
        _this.initialized = false;
        _this.dirty = false;
        _this.rendering = false;
        _this.tags = false;
        _this.isTouch = false;
        _this.container = "content";
        _this.validStyles = [
            "color",
            "background",
            "background-color",
            "border",
            "border-left",
            "border-right",
            "border-top",
            "border-bottom",
            "width",
            "height",
            "max-height",
            "text-align",
            "vertical-align",
            "font-family",
            "font-size",
            "font-weight",
            "font-style",
            "text-wrap",
            "text-decoration",
            "number-format",
            "word-wrap",
            "white-space",
        ];
        _this._editing = false;
        _this._tracking = false;
        _this.numOfCells = 0;
        _this.freeze = false;
        _this.freezeRange = {
            valid: false,
            index: {
                row: 0,
                col: 0,
            }
        };
        _this.accessRangesClassNames = {
            "no": "no-access",
            "ro": "read-only",
        };
        _this.clicked = 0;
        _this.clickCellLink = function (e) {
            var clickedEl = e.target;
            while (clickedEl && clickedEl.nodeName.toLowerCase() !== "a") {
                clickedEl = clickedEl.parentNode;
            }
            if (!clickedEl || clickedEl.nodeName.toLowerCase() !== "a") {
                return false;
            }
            var external = _this.classy.hasClass(clickedEl, "external");
            if (external) {
                if (_this.app.isEmbedded && _this.app.externalLink) {
                    window.parent.postMessage({
                        event: "link",
                        link: clickedEl.getAttribute("href"),
                    }, "*");
                    e.preventDefault();
                }
                else {
                    return;
                }
            }
            if (("standalone" in window.navigator) && window.navigator.standalone) {
                e.preventDefault();
                _this.$location.path(clickedEl.getAttribute("href"));
            }
        };
        _this.onTableClick = function (e) {
            var cell = _this.getClickedCell(e.target);
            var d = new Date();
            if (_this.clicked) {
                if (d.getTime() - _this.clicked <= 300) {
                    _this.emit(_this.ON_CELL_DOUBLE_CLICKED, cell);
                    _this.clicked = d.getTime();
                    return;
                }
            }
            _this.clicked = d.getTime();
            var node = e.target.nodeName.toLowerCase();
            if (node === "span") {
                _this.emit(_this.ON_TAG_CLICKED, cell);
                return;
            }
            if (_this.editing) {
                _this.showInput(cell);
            }
            _this.emit(_this.ON_CELL_CLICKED, cell);
        };
        _this.onWindowScroll = function (e) {
            if (_this.freeze) {
                _this.updateFreezePanes(e.target);
            }
        };
        if (container) {
            _this.container = container;
        }
        _this.classy = new Classy_1.default();
        _this.helpers = new Helpers_1.default();
        _this.init();
        return _this;
    }
    Object.defineProperty(Table.prototype, "ON_CELL_CLICKED", {
        get: function () { return "cell_clicked"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Table.prototype, "ON_CELL_DOUBLE_CLICKED", {
        get: function () { return "cell_double_clicked"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Table.prototype, "ON_TAG_CLICKED", {
        get: function () { return "tag_clicked"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Table.prototype, "ON_CELL_CHANGED", {
        get: function () { return "cell_changed"; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Table.prototype, "editing", {
        get: function () {
            return this._editing;
        },
        set: function (value) {
            this._editing = value;
            if (value) {
                this.classy.addClass(this.containerEl, "edit-mode");
            }
            else {
                this.classy.removeClass(this.containerEl, "edit-mode");
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Table.prototype, "tracking", {
        get: function () {
            return this._tracking;
        },
        set: function (value) {
            this._tracking = value;
            this.clearCellClassNames(["changed"]);
        },
        enumerable: true,
        configurable: true
    });
    Table.prototype.init = function () {
        if (this.initialized) {
            return true;
        }
        this.containerEl = document.getElementById(this.container);
        if (!this.containerEl) {
            console.error("Could not init table");
            return false;
        }
        this.viewEl = document.createElement("div");
        this.viewEl.className = "page-content-view";
        this.scrollEl = document.createElement("div");
        this.scrollEl.className = "page-content-scroll";
        this.zoomEl = document.createElement("div");
        this.zoomEl.className = "page-content-zoom";
        this.tableEl = document.createElement("table");
        this.tableHeadEl = document.createElement("thead");
        this.tableBodyEl = document.createElement("tbody");
        this.tableEl.appendChild(this.tableHeadEl);
        this.tableEl.appendChild(this.tableBodyEl);
        this.tableFrozenRowsEl = document.createElement("table");
        this.tableFrozenRowsEl.className = "frozen-rows";
        this.tableFrozenColsEl = document.createElement("table");
        this.tableFrozenColsEl.className = "frozen-cols";
        this.zoomEl.appendChild(this.tableEl);
        this.zoomEl.appendChild(this.tableFrozenRowsEl);
        this.zoomEl.appendChild(this.tableFrozenColsEl);
        this.scrollEl.appendChild(this.zoomEl);
        this.viewEl.appendChild(this.scrollEl);
        this.containerEl.appendChild(this.viewEl);
        this.helpers.addEvent(this.tableEl, "click", this.onTableClick);
        this.helpers.addEvent(this.viewEl, "scroll", this.onWindowScroll);
        this.dirty = false;
        this.initialized = true;
        return true;
    };
    Table.prototype.render = function (data) {
        if (this.rendering) {
            return false;
        }
        this.rendering = true;
        this.setData(data);
        if (!this.data || !this.data[0] || !this.data[0][0] || !this.init()) {
            this.rendering = false;
            return false;
        }
        for (var i = 0; i < this.data.length; i++) {
            this.renderRow(-1, this.data[i]);
        }
        this.softMerges();
        this.rendering = false;
        return true;
    };
    Table.prototype.update = function (data, content) {
        if (this.rendering || !this.initialized) {
            return false;
        }
        this.rendering = true;
        this.setData(content);
        for (var i in data) {
            if (!data.hasOwnProperty(i)) {
                continue;
            }
            if (this.tableBodyEl.rows[i]) {
                for (var k in data[i]) {
                    if (!data[i].hasOwnProperty(k)) {
                        continue;
                    }
                    if (this.tableBodyEl.rows[i].cells[k]) {
                        this.setCellValues(this.tableBodyEl.rows[i].cells[k], data[i][k]);
                    }
                    else {
                        this.setCellValues(this.tableBodyEl.rows[i].insertCell(k), data[i][k]);
                    }
                }
            }
            else {
                this.renderRow(i, this.data[i]);
            }
            this.softMerges(this.tableBodyEl.rows[i]);
        }
        while (this.tableBodyEl.rows.length > content.length) {
            this.tableBodyEl.deleteRow(this.tableBodyEl.rows.length - 1);
        }
        if (this.tableBodyEl.rows[0].cells.length > content[0].length) {
            for (var i = 0; i < this.tableBodyEl.rows.length; i++) {
                while (this.tableBodyEl.rows[i].cells.length > content[i].length) {
                    this.tableBodyEl.rows[i].deleteCell(this.tableBodyEl.rows[i].cells.length - 1);
                }
                this.softMerges(this.tableBodyEl.rows[i]);
            }
        }
        this.rendering = false;
        return true;
    };
    Table.prototype.getData = function () {
        return this.data;
    };
    Table.prototype.setData = function (data) {
        if (!data) {
            data = [];
        }
        this.data = data;
        this.numOfCells = data.length && data[0].length ? data.length * data[0].length : 0;
    };
    Table.prototype.setAccessRanges = function (rangeAccess) {
        this.setRanges(rangeAccess);
    };
    Table.prototype.setRanges = function (ranges, userId) {
        if (userId === void 0) { userId = undefined; }
        this.accessRanges = [];
        this.clearCellClassNames(Object.values(this.accessRangesClassNames));
        this.freezeRange = {
            valid: false,
            index: {
                row: 0,
                col: 0,
            }
        };
        for (var i = 0; i < ranges.length; i++) {
            var range = ranges[i];
            if (range.name === "frozen_rows") {
                this.freezeRange.index.row = range.count;
            }
            else if (range.name === "frozen_cols") {
                this.freezeRange.index.col = range.count;
            }
            else {
                var userRight = range.getPermission(userId || 0);
                console.log("setupRanges", userRight, range);
                if (range.rowEnd === -1) {
                    range.rowEnd = this.data.length - 1;
                }
                if (range.colEnd === -1) {
                    range.colEnd = this.data[0].length - 1;
                }
                for (var i_1 = range.rowStart; i_1 <= range.rowEnd; i_1++) {
                    for (var k = range.colStart; k <= range.colEnd; k++) {
                        if (!this.accessRanges[i_1]) {
                            this.accessRanges[i_1] = [];
                        }
                        if (!this.accessRanges[i_1][k]) {
                            this.accessRanges[i_1][k] = [];
                        }
                        this.accessRanges[i_1][k].push(userRight);
                    }
                }
            }
        }
        if (this.freezeRange.index.row > 0 && this.freezeRange.index.col > 0) {
            this.freezeRange.valid = true;
        }
        if (this.initialized) {
            this.freezePanes();
        }
    };
    Table.prototype.generateBlankData = function () {
        var cell = {
            value: "",
            style: {
                width: "60px",
                height: "22px",
            },
        };
        var data = [];
        for (var i = 0; i < 10; i++) {
            data[i] = [];
            for (var k = 0; k < 6; k++) {
                var clone = _.clone(cell);
                if (i === 0 && k === 0) {
                    clone.value = "Click Me";
                }
                data[i][k] = clone;
            }
        }
        return data;
    };
    Table.prototype.destroy = function () {
        if (this.initialized) {
            this.helpers.removeEvent(this.containerEl, "scroll", this.onWindowScroll);
            while (this.containerEl.firstChild) {
                this.containerEl.removeChild(this.containerEl.firstChild);
            }
            this.initialized = false;
        }
    };
    Table.prototype.getEditCell = function () {
        return this.tableBodyEl ? this.tableBodyEl.getElementsByClassName("edit")[0] : undefined;
    };
    Table.prototype.addRow = function (index, data) {
        this.renderRow(index, data);
    };
    Table.prototype.getRowIndex = function () {
        var currentCell = this.getEditCell();
        if (currentCell) {
            return parseInt(currentCell.parentNode.rowIndex + 1, 10);
        }
        return -1;
    };
    Table.prototype.addColumn = function () {
        ;
    };
    Table.prototype.getColumnIndex = function () {
        var currentCell = this.getEditCell();
        if (currentCell) {
            return parseInt(currentCell.cellIndex + 1);
        }
        return -1;
    };
    Table.prototype.setEditCell = function (el) {
        var cell = this.getClickedCell(el);
        if (cell) {
            if (this.accessRanges && this.accessRanges[cell.parentNode.rowIndex] && this.accessRanges[cell.parentNode.rowIndex][cell.cellIndex]) {
                return false;
            }
            this.editing = true;
            this.showInput(cell);
        }
    };
    Table.prototype.freezePanes = function () {
        this.tableFrozenColsEl.innerHTML = "";
        this.tableFrozenColsEl.style.display = "none";
        this.tableFrozenRowsEl.innerHTML = "";
        this.tableFrozenRowsEl.style.display = "none";
        this.freeze = false;
        var containerRect = this.viewEl.getBoundingClientRect();
        var tableRect = this.tableEl.getBoundingClientRect();
        if ((containerRect.width >= tableRect.width && containerRect.height >= tableRect.height) || !this.freezeRange.valid) {
            return;
        }
        this.freeze = true;
        for (var i = 0; i < this.freezeRange.index.row; i++) {
            if (this.tableBodyEl.rows.length <= i) {
                continue;
            }
            this.tableFrozenRowsEl.appendChild(this.tableBodyEl.rows[i].cloneNode(true));
        }
        for (var i = 0; i < this.tableBodyEl.rows.length; i++) {
            this.tableFrozenColsEl.appendChild(this.tableBodyEl.rows[i].cloneNode(false));
            for (var j = 0; j < this.freezeRange.index.row; j++) {
                if (this.tableBodyEl.rows[i].cells.length <= j) {
                    continue;
                }
                this.tableFrozenColsEl.rows[i].appendChild(this.tableBodyEl.rows[i].cells[j].cloneNode(true));
            }
        }
        this.updateFreezePanes(this.viewEl);
    };
    Table.prototype.renderRow = function (rowId, rowData) {
        if (!rowId || typeof rowId === "undefined") {
            rowId = -1;
        }
        this.renderCells(this.tableEl.insertRow(rowId), rowData, undefined);
    };
    Table.prototype.renderCells = function (row, cellsData, startIndex) {
        if (typeof startIndex === "undefined") {
            startIndex = 0;
        }
        for (var i = 0; i < cellsData.length; i++) {
            this.setCellValues(row.insertCell(i + startIndex), cellsData[i]);
        }
    };
    Table.prototype.setCellValues = function (cell, cellsData) {
        var _this = this;
        cell.removeAttribute("style");
        for (var s in cellsData.style) {
            if (!cellsData.style.hasOwnProperty(s)) {
                continue;
            }
            var name_1 = s;
            if (name_1.indexOf("border-") > -1) {
                var nameParts = name_1.split("-");
                name_1 = nameParts[0] + "-" + nameParts[1];
            }
            if (this.validStyles.indexOf(name_1) === -1) {
                continue;
            }
            var v = cellsData.style[s];
            if (s === "font-family") {
                v += ", Arial, Helvetica, sans-serif";
            }
            if ((s === "color" || s === "background-color" || s.indexOf("-color") !== -1) && !~v.indexOf("#") && !~v.indexOf("rgb")) {
                v = "#" + v;
            }
            cell.style.setProperty(s, v);
        }
        var cellValue = this.getCellValue(cellsData);
        cell.setAttribute("data-value", cellValue);
        var divs = cell.getElementsByTagName("div");
        var div = divs.length ? divs[0] : document.createElement("div");
        var cellPos = cell.parentNode.rowIndex + cell.cellIndex;
        if (cellPos === 0) {
            cellPos = .5;
        }
        div.style["z-index"] = Math.round(this.numOfCells * this.numOfCells * 1 / cellPos);
        div.innerHTML = cellValue;
        div.dataset.format = cellsData.style ? cellsData.style["number-format"] || "@" : "@";
        if (this.tags) {
            var spans = cell.getElementsByTagName("span");
            var span = spans.length ? spans[0] : document.createElement("span");
            span.innerText = "tag";
            cell.appendChild(span);
            this.classy.addClass(div, "tag");
            div.dataset.tag = cellsData.tag || "";
        }
        if (typeof cellsData.link !== "undefined" && cellsData.link) {
            var link = this.processLink(cellsData.link);
            var links = cell.getElementsByTagName("a");
            var a = links.length ? links[0] : document.createElement("a");
            a.setAttribute("href", link.url);
            a.setAttribute("target", link.target);
            var linkCls = ((!cellsData.link.external) ? "internal" : "external");
            a.setAttribute("class", linkCls);
            if (!links.length) {
                this.helpers.addEvent(a, "click", this.clickCellLink, true);
            }
            if (!links.length) {
                cell.appendChild(a);
                a.appendChild(div);
            }
            cell.setAttribute("data-link-href", link.url);
            cell.setAttribute("data-link-target", link.target);
            cell.setAttribute("data-link-class", linkCls);
        }
        else {
            cell.appendChild(div);
        }
        if (this.accessRanges) {
            if (this.accessRanges[cell.parentNode.rowIndex] && this.accessRanges[cell.parentNode.rowIndex][cell.cellIndex]) {
                this.accessRanges[cell.parentNode.rowIndex][cell.cellIndex].forEach2(function (element) {
                    if (_this.accessRangesClassNames[element]) {
                        _this.classy.addClass(cell, _this.accessRangesClassNames[element]);
                    }
                });
            }
        }
        if (this.tracking) {
            this.classy.addClass(cell, "changed");
        }
    };
    Table.prototype.processLink = function (linkData) {
        var url = (linkData.external) ? linkData.address : "";
        var target = "_blank";
        if (!linkData.external) {
            var parts = linkData.address.split("/");
            url = "domains/" + parts[0] + "/pages/" + parts[1];
            target = "";
        }
        if (linkData.address.indexOf("tel:") >= 0 || linkData.address.indexOf("sms:") >= 0 || linkData.address.indexOf("mailto:") >= 0) {
            target = "";
        }
        return { url: url, target: target };
    };
    Table.prototype.getCellValue = function (cellData, escapeHtml) {
        if (typeof escapeHtml === "undefined") {
            escapeHtml = true;
        }
        var value = "&nbsp;";
        if (cellData.formatted_value && cellData.formatted_value.length) {
            value = cellData.formatted_value;
        }
        else if (cellData.value && cellData.value.length) {
            value = cellData.value;
        }
        if (escapeHtml) {
            value = value.replace(/</g, "&lt;").replace(/</g, "&rt;");
        }
        value = value.replace(/&amp;/g, "&");
        if (value.indexOf("data:image") === 0) {
            value = "<img src=\"" + cellData.value + "\" />";
        }
        return value;
    };
    Table.prototype.showInput = function (cell) {
        var _this = this;
        if (!cell || this.classy.hasClass(cell, "edit")) {
            return;
        }
        this.classy.addClass(cell, "edit");
        var div = cell.getElementsByTagName("div").length ? cell.getElementsByTagName("div")[0] : undefined;
        var input = document.createElement("input");
        var value = cell.dataset.newValue || cell.dataset.value;
        input.value = value === "&nbsp;" ? "" : value;
        cell.appendChild(input);
        input.focus();
        input.selectionStart = 0;
        input.selectionEnd = 999;
        this.helpers.addEvent(input, "keydown", function (e) {
            if (e.keyCode === 13 || e.keyCode === 9 || e.keyCode === 27) {
                e.preventDefault();
            }
            if (e.keyCode === 13) {
                if (!e.shiftKey) {
                    input.blur();
                    _this.nextCellInput(cell, "down");
                }
            }
            if (e.keyCode === 9) {
                _this.nextCellInput(cell);
            }
        });
        this.helpers.addEvent(input, "blur", function (e) {
            _this.classy.removeClass(cell, "edit");
            if (cell.dataset.value !== input.value) {
                _this.dirty = true;
                cell.setAttribute("data-new-value", input.value);
                if (div) {
                    div.innerHTML = input.value;
                }
                _this.emit(_this.ON_CELL_CHANGED, {
                    cell: cell,
                    value: input.value,
                    rowIndex: cell.parentNode.rowIndex,
                    colIndex: cell.cellIndex,
                });
            }
            cell.removeChild(input);
        });
        this.helpers.addEvent(input, "paste", function (e) {
            console.info("pasted");
        });
    };
    Table.prototype.nextCellInput = function (cell, direction) {
        if (direction === void 0) { direction = ""; }
        var row = cell.parentNode.rowIndex;
        var col = cell.cellIndex;
        if (direction === "down") {
            if (this.data[row + 1] && this.data[row + 1][col]) {
                this.showInput(this.tableBodyEl.rows[row + 1].cells[col]);
                return;
            }
        }
        if (this.data[row][col + 1]) {
            this.showInput(this.tableBodyEl.rows[row].cells[col + 1]);
            return;
        }
        if (this.data[row + 1]) {
            this.showInput(this.tableBodyEl.rows[row + 1].cells[0]);
            return;
        }
        this.showInput(this.tableBodyEl.rows[0].cells[0]);
        return;
    };
    Table.prototype.getClickedCell = function (clickedEl) {
        while (clickedEl && clickedEl.nodeName.toLowerCase() !== "td") {
            clickedEl = clickedEl.parentNode;
        }
        if (!clickedEl || clickedEl.nodeName.toLowerCase() !== "td") {
            return false;
        }
        if (this.classy.hasClass(clickedEl, "edit")) {
        }
        return clickedEl;
    };
    Table.prototype.softMerges = function (rowEl) {
        var _this = this;
        var applyWidth = function (row, col, width) {
            var div = _this.tableBodyEl.rows[row].cells[col].getElementsByTagName("div")[0];
            if (width) {
                div.style.width = Math.round(width + 1) + "px";
                _this.classy.addClass(div, "soft-merge");
            }
            else {
                div.style.width = "auto";
                _this.classy.removeClass(div, "soft-merge");
            }
        };
        var getTextWidth = function (text, font) {
            var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
            var context = canvas.getContext("2d");
            context.font = font;
            var metrics = context.measureText(text);
            return metrics.width;
        };
        var processRow = function (row, rowData) {
            for (var i = 0; i < rowData.length; i++) {
                if (i + 1 === rowData.length) {
                    applyWidth(row, i, 0);
                    continue;
                }
                var adjCellVal = (rowData[i + 1].formatted_value || rowData[i + 1].value);
                if (adjCellVal.length) {
                    applyWidth(row, i, 0);
                    continue;
                }
                var cellData = rowData[i];
                var cellVal = (cellData.formatted_value || cellData.value);
                var cell = _this.tableBodyEl.rows[row].cells[i];
                var cellWidth = cell.clientWidth;
                if (!cellVal.length
                    || cellData.style["word-wrap"] === "break-word"
                    || (cellData.style.hasOwnProperty("text-align") && cellData.style["text-align"] !== "left" && cellData.style["text-align"] !== "start")) {
                    applyWidth(row, i, 0);
                    continue;
                }
                var fontSize = cellData.style["font-size"] || "12pt";
                var fontFamily = cellData.style["font-family"] ? cellData.style["font-family"].replace(/"/ig, "") : "sans-serif";
                var fontWeight = cellData.style["font-weight"] || "normal";
                var contentWidth = getTextWidth(cellVal, fontWeight + " " + fontSize + " " + fontFamily);
                if (contentWidth < cellWidth) {
                    applyWidth(row, i, 0);
                    continue;
                }
                var mergeWidth = cellWidth;
                for (var k = i + 1; k < rowData.length; k++) {
                    var adjCellWidth = _this.tableBodyEl.rows[row].cells[k].clientWidth;
                    mergeWidth += adjCellWidth;
                    if (k + 1 === rowData.length) {
                        contentWidth = mergeWidth;
                        break;
                    }
                    var adjCellVal_1 = (rowData[k].formatted_value || rowData[i + 1].value).trim();
                    if (adjCellVal_1.length) {
                        if (contentWidth > mergeWidth) {
                            contentWidth = mergeWidth;
                        }
                        break;
                    }
                    if (mergeWidth > contentWidth) {
                        break;
                    }
                }
                applyWidth(row, i, contentWidth);
            }
        };
        var getRowIndex = function (el) {
            return el.rowIndex - _this.tableHeadEl.rows.length;
        };
        if (rowEl) {
            processRow(getRowIndex(rowEl), rowEl);
        }
        else {
            for (var i = 0; i < this.data.length; i++) {
                processRow(i, this.data[i]);
            }
        }
    };
    Table.prototype.updateFreezePanes = function (target) {
        console.log(target);
        if (target.scrollTop > 0) {
            this.tableFrozenRowsEl.style.top = target.scrollTop + "px";
            this.tableFrozenRowsEl.style.display = "table";
        }
        else {
            this.tableFrozenRowsEl.style.top = "0px";
            this.tableFrozenRowsEl.style.display = "none";
        }
        if (target.scrollLeft > 0) {
            this.tableFrozenColsEl.style.left = target.scrollLeft + "px";
            this.tableFrozenColsEl.style.display = "table";
        }
        else {
            this.tableFrozenColsEl.style.left = "0px";
            this.tableFrozenColsEl.style.display = "none";
        }
    };
    Table.prototype.clearCellClassNames = function (classNames) {
        var _this = this;
        if (!this.data || !this.data.length) {
            return;
        }
        var _loop_1 = function (i) {
            var _loop_2 = function (k) {
                if (this_1.tableBodyEl.rows[i] && this_1.tableBodyEl.rows[i].cells[k]) {
                    classNames.forEach2(function (element) {
                        _this.classy.removeClass(_this.tableBodyEl.rows[i].cells[k], element);
                    });
                }
            };
            for (var k = 0; k < this_1.data[i].length; k++) {
                _loop_2(k);
            }
        };
        var this_1 = this;
        for (var i = 0; i < this.data.length; i++) {
            _loop_1(i);
        }
    };
    return Table;
}(Emitter_1.default));
exports.default = Table;
//# sourceMappingURL=Table.js.map