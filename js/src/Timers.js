"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Timers = (function () {
    function Timers() {
        this.funcs = [];
    }
    Timers.prototype.set = function (func, delay) {
        this.funcs.push(setTimeout(func, delay));
    };
    Timers.prototype.cancel = function (func) {
        var i = this.funcs.indexOf(func);
        if (i > -1) {
            clearTimeout(this.funcs[i]);
            this.funcs[i] = undefined;
        }
    };
    return Timers;
}());
var timersProvider = new Timers();
function timers(func, delay) {
    timersProvider.set(func, delay);
}
exports.timers = timers;
timers.cancel = function (func) {
    timersProvider.cancel(func);
};
var Intervals = (function () {
    function Intervals() {
        this.funcs = [];
    }
    Intervals.prototype.set = function (func, delay) {
        this.funcs.push(setInterval(func, delay));
    };
    Intervals.prototype.cancel = function (func) {
        var i = this.funcs.indexOf(func);
        if (i > -1) {
            clearTimeout(this.funcs[i]);
            this.funcs[i] = undefined;
        }
    };
    return Intervals;
}());
var intervalsProvider = new Intervals();
function intervals(func, delay) {
    intervalsProvider.set(func, delay);
}
exports.intervals = intervals;
intervals.cancel = function (func) {
    intervalsProvider.cancel(func);
};
//# sourceMappingURL=Timers.js.map