interface IData {
    content_diff: any;
    modified_by: any;
    modified_by_timestamp: any;
}
export declare class Tracking {
    private $interval;
    private storage;
    static $inject: string[];
    history: any;
    historyFirst: any;
    private _enabled;
    private _interval;
    private _length;
    constructor($interval: any, storage: any);
    createInstance(): Tracking;
    enabled: boolean;
    addHistory(pageId: number, data: IData, first?: boolean): void;
    getHistory(pageId: number): any;
    getHistoryForCell(pageId: number, cellRow: number, cellCol: number): any;
    restore(): any;
    reset(): any;
    cancel(): void;
    private totalChanges;
}
export default Tracking;
