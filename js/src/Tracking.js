"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils_1 = require("./Utils");
var Tracking = (function () {
    function Tracking($interval, storage) {
        this.$interval = $interval;
        this.storage = storage;
        this.history = {};
        this.historyFirst = {};
        this._enabled = false;
        this._length = 0;
        this._utils = new Utils_1.default();
    }
    Tracking.prototype.createInstance = function () {
        return new Tracking(this.$interval, this.storage);
    };
    Object.defineProperty(Tracking.prototype, "enabled", {
        get: function () {
            return this._enabled;
        },
        set: function (value) {
            var _this = this;
            if (typeof value !== 'boolean') {
                return;
            }
            this._enabled = value;
            this.cancel();
            if (!value) {
                this.reset();
            }
            else {
                this.restore();
                this._length = this.totalChanges();
                this._interval = this.$interval(function () {
                    var length = _this.totalChanges();
                    if (_this._length !== length) {
                        _this.storage.user.save('history', JSON.stringify(_this.history));
                    }
                    _this._length = length;
                }, 10000);
            }
        },
        enumerable: true,
        configurable: true
    });
    Tracking.prototype.addHistory = function (pageId, data, first) {
        if (first === void 0) { first = false; }
        if (!this.enabled) {
            return;
        }
        if (first && (this.history["page_" + pageId] && this.history["page_" + pageId].length)) {
            return;
        }
        if (!this.history["page_" + pageId]) {
            this.history["page_" + pageId] = [];
        }
        this.history["page_" + pageId].push(this.compressData(data, first));
    };
    Tracking.prototype.getHistory = function (pageId) {
        if (!this.history["page_" + pageId]) {
            return [];
        }
        return this.history["page_" + pageId];
    };
    Tracking.prototype.getHistoryForCell = function (pageId, cellRow, cellCol) {
        var history = this.getHistory(pageId);
        if (!history.length) {
            return [];
        }
        var cellHistory = [];
        history.forEach2(function (snapshot) {
            snapshot.content_diff.forEach2(function (row, rowIndex) {
                if (!row) {
                    return;
                }
                row.forEach2(function (col, colIndex) {
                    if (!col) {
                        return;
                    }
                    if (cellRow === rowIndex && cellCol === colIndex) {
                        cellHistory.push({
                            cell: JSON.parse(JSON.stringify(col)),
                            modified_by: snapshot.modified_by,
                            modified_by_timestamp: snapshot.modified_by_timestamp,
                        });
                    }
                });
            });
        });
        return cellHistory;
    };
    Tracking.prototype.restore = function () {
        this.history = this.storage.user.get('history') || {};
    };
    Tracking.prototype.reset = function () {
        this.history = {};
        this.storage.user.remove('history');
    };
    Tracking.prototype.cancel = function () {
        if (this._interval) {
            this.$interval.cancel(this._interval);
        }
    };
    Tracking.prototype.totalChanges = function () {
        var length = 0;
        for (var id in this.history) {
            if (!this.history.hasOwnProperty(id)) {
                continue;
            }
            length += this.history[id].length;
        }
        return length;
    };
    Tracking.prototype.compressData = function (data, first) {
        if (first === void 0) { first = false; }
        if (first) {
            data.content_diff = this._utils.clonePageContent(data.content_diff);
        }
        return JSON.parse(JSON.stringify(data));
    };
    Tracking.$inject = ['$interval', 'ippStorageService'];
    return Tracking;
}());
exports.Tracking = Tracking;
exports.default = Tracking;
//# sourceMappingURL=Tracking.js.map