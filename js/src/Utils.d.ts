import { IPageContent } from "./Page/Content";
export interface IUtils {
    parseApiError: (err: any, def: string) => string;
    clonePageContent: (content: IPageContent) => IPageContent;
}
declare class Utils implements IUtils {
    constructor();
    parseApiError(err: any, def: string): string;
    clonePageContent(content: IPageContent): IPageContent;
    comparePageContent(currentContent: any, newContent: any, ignore_styles?: boolean): any;
}
export default Utils;
