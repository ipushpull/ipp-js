"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var merge = require("merge");
var _ = require("underscore");
var Utils = (function () {
    function Utils() {
        return;
    }
    Utils.prototype.parseApiError = function (err, def) {
        var msg = def;
        if (err.data) {
            var keys = Object.keys(err.data);
            if (keys.length) {
                if (_.isArray(err.data[keys[0]])) {
                    msg = err.data[keys[0]][0];
                }
                else if (typeof err.data[keys[0]] === "string") {
                    msg = err.data[keys[0]];
                }
                else {
                    msg = def;
                }
            }
            else {
                msg = def;
            }
        }
        else {
            msg = def;
        }
        return msg;
    };
    Utils.prototype.clonePageContent = function (content) {
        var copy = [];
        var deltaStyle = {};
        for (var i = 0; i < content.length; i++) {
            copy[i] = [];
            for (var j = 0; j < content[i].length; j++) {
                var cell = merge({}, content[i][j]);
                var style = {};
                for (var k in cell.style) {
                    if (cell.style[k] === deltaStyle[k])
                        continue;
                    style[k] = cell.style[k];
                }
                deltaStyle = merge({}, cell.style);
                cell.style = style;
                if (cell.index)
                    delete cell.index;
                if (cell.formatting)
                    delete cell.formatting;
                copy[i].push(cell);
            }
        }
        return copy;
    };
    Utils.prototype.comparePageContent = function (currentContent, newContent, ignore_styles) {
        if (ignore_styles === void 0) { ignore_styles = false; }
        if (!currentContent || !newContent) {
            return false;
        }
        var diffContent = [];
        var sizeDiff = false;
        for (var i = 0; i < newContent.length; i++) {
            for (var k = 0; k < newContent[i].length; k++) {
                var diff = false;
                if (currentContent[i] && currentContent[i][k]) {
                    if (newContent[i][k].value !== currentContent[i][k].value) {
                        diff = true;
                    }
                    if (!diff && !ignore_styles) {
                        for (var attr in newContent[i][k].style) {
                            if (newContent[i][k].style[attr] !== currentContent[i][k].style[attr]) {
                                diff = true;
                                if (['width', 'height'].indexOf(attr) > -1)
                                    sizeDiff = true;
                                break;
                            }
                        }
                    }
                }
                else {
                    diff = true;
                }
                if (diff) {
                    if (!diffContent[i]) {
                        diffContent[i] = [];
                    }
                    diffContent[i][k] = newContent[i][k];
                }
            }
        }
        if (diffContent.length || newContent.length < currentContent.length || newContent[0].length < currentContent[0].length) {
            var rowDiff = currentContent.length - newContent.length;
            var colDiff = currentContent[0].length - newContent[0].length;
            return { content: newContent, content_diff: diffContent, colDiff: colDiff, rowDiff: rowDiff, sizeDiff: sizeDiff };
        }
        return false;
    };
    Utils.prototype.mergePageContent = function (_original, _current, style, clean) {
        if (style === void 0) { style = {}; }
        if (clean === void 0) { clean = false; }
        var current = [];
        var colWidths = [];
        for (var rowIndex = 0; rowIndex < _original.length; rowIndex++) {
            var row = _original[rowIndex];
            current[rowIndex] = [];
            for (var colIndex = 0; colIndex < row.length; colIndex++) {
                var col = void 0;
                if (!clean && _current && _current[rowIndex] && _current[rowIndex][colIndex] && _current[rowIndex][colIndex].dirty) {
                    col = _current[rowIndex][colIndex];
                    style = merge({}, col.originalStyle || col.style);
                }
                else {
                    col = merge({}, row[colIndex]);
                    col.style = merge({}, style, col.style);
                    delete col.dirty;
                    style = merge({}, col.style);
                }
                if (!rowIndex) {
                    colWidths[colIndex] = col.style.width;
                }
                col.style.width = colWidths[colIndex];
                col.formatting = {};
                col.index = {
                    row: rowIndex,
                    col: colIndex
                };
                current[rowIndex].push(col);
            }
        }
        return current;
    };
    return Utils;
}());
exports.default = Utils;
//# sourceMappingURL=Utils.js.map