"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Q = require("q");
var query = require("querystring");
var merge = require("merge");
var req = require("request");
var Config_1 = require("./Config");
var Api_1 = require("./Api");
var Utils_1 = require("./Utils");
var Timers_1 = require("./Timers");
var Timers_2 = require("./Timers");
var Page_1 = require("./Page/Page");
Array.prototype.forEach2 = function (a) {
    var l = this.length;
    for (var i = 0; i < l; i++)
        a(this[i], i);
};
var IPushPull = (function () {
    function IPushPull(settings) {
        this.settings = settings;
        this.config = new Config_1.Config();
        this.config.set(settings);
        this.utils = new Utils_1.default();
        this.api = new Api_1.Api(query.stringify, Q, undefined, this.config, this.utils, req);
        this.page = new Page_1.PageWrap(Q, Timers_1.timers, Timers_2.intervals, this.api, this.auth, undefined, undefined, this.config);
    }
    return IPushPull;
}());
function createInstance(defaultConfig) {
    return new IPushPull(defaultConfig);
}
var ipushpull = createInstance();
ipushpull.IPushpull = IPushPull;
ipushpull.create = function (instanceConfig) {
    return createInstance(merge({
        api_url: "http://ipushpull.local/api/1.0",
        ws_url: "http://ipushpull.local",
        storage_prefix: "ipp_local",
        api_key: "LC8aOmGqqydL3n8HyKaz8ue34M13WPux2zyuBAl1",
        api_secret: "kXXRR7jsoEm6JgjvBDYGca1YlGIDynuxpM9kiu9GTlqzLtmCvNWLm3pmzWUc6Uptp4DwvS1kLlC6mh5PHGGnKRNh0eBPG1IIUB5NtsEY1oSgqLcteNCdGBLnIwnOO7SY",
        transport: "polling",
    }, instanceConfig));
};
module.exports = ipushpull;
module.exports.defaults = ipushpull;
//# sourceMappingURL=index-api.js.map