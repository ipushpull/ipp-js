"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Config_1 = require("./Config");
var Storage_1 = require("./Storage");
var Api_1 = require("./Api");
var Auth_1 = require("./Auth");
var Classy_1 = require("./Classy");
var Utils_1 = require("./Utils");
var Helpers_1 = require("./Helpers");
var Grid_1 = require("./Grid");
var Clipboard_1 = require("./Clipboard");
var Pdf_1 = require("./Pdf");
var Tracking_1 = require("./Tracking");
var Crypto_1 = require("./Crypto");
var Request_1 = require("./Request");
var Page_1 = require("./Page/Page");
var Range_1 = require("./Page/Range");
var Range_2 = require("./Page/Range");
var Range_3 = require("./Page/Range");
var Range_4 = require("./Page/Range");
var Range_5 = require("./Page/Range");
var Range_6 = require("./Page/Range");
Array.prototype.forEach2 = function (a) {
    var l = this.length;
    for (var i = 0; i < l; i++)
        a(this[i], i);
};
angular.module("ipushpull", [])
    .provider("ippConfig", Config_1.Config)
    .service("ippApiService", Api_1.Api)
    .service("ippPageService", Page_1.PageWrap)
    .factory("ippStorageService", Storage_1.StorageService)
    .service("ippAuthService", Auth_1.Auth)
    .factory("ippCryptoService", Crypto_1.Crypto._instance)
    .factory("ippReqService", Request_1.Request._instance)
    .service("ippClassyService", Classy_1.default)
    .service("ippUtilsService", Utils_1.default)
    .service("ippHelpersService", Helpers_1.default)
    .factory("ippClipboardService", Clipboard_1.ClipboardWrap)
    .factory("ippGridService", Grid_1.GridWrap)
    .factory("ippPdfService", Pdf_1.default)
    .service("ippTrackingService", Tracking_1.default);
var ipushpull = {
    FreezingRange: Range_1.FreezingRange,
    PermissionRange: Range_2.PermissionRange,
    ButtonRange: Range_3.ButtonRange,
    StyleRange: Range_4.StyleRange,
    NotificationRange: Range_5.NotificationRange,
    AccessRange: Range_6.AccessRange,
};
module.exports = ipushpull;
module.exports.defaults = ipushpull;
//# sourceMappingURL=index-ng.js.map