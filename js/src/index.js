'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var Q = require("q");
var query = require("querystring");
var merge = require("merge");
var request = require("xhr");
var Config_1 = require("./Config");
var Storage_1 = require("./Storage");
var Api_1 = require("./Api");
var Auth_1 = require("./Auth");
var Classy_1 = require("./Classy");
var Utils_1 = require("./Utils");
var Helpers_1 = require("./Helpers");
var Grid_1 = require("./Grid2/Grid");
var Clipboard_1 = require("./Clipboard");
var Pdf_1 = require("./Pdf");
var Tracking_1 = require("./Tracking");
var Crypto_1 = require("./Crypto");
var Timers_1 = require("./Timers");
var Timers_2 = require("./Timers");
var Page_1 = require("./Page/Page");
var Range_1 = require("./Page/Range");
var Range_2 = require("./Page/Range");
var Range_3 = require("./Page/Range");
var Range_4 = require("./Page/Range");
var Range_5 = require("./Page/Range");
var Range_6 = require("./Page/Range");
var Functions_1 = require("./Functions");
Array.prototype.forEach2 = function (a) {
    var l = this.length;
    for (var i = 0; i < l; i++)
        a(this[i], i);
};
if (!Array.from) {
    Array.from = (function () {
        var toStr = Object.prototype.toString;
        var isCallable = function (fn) {
            return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
        };
        var toInteger = function (value) {
            var number = Number(value);
            if (isNaN(number)) {
                return 0;
            }
            if (number === 0 || !isFinite(number)) {
                return number;
            }
            return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
        };
        var maxSafeInteger = Math.pow(2, 53) - 1;
        var toLength = function (value) {
            var len = toInteger(value);
            return Math.min(Math.max(len, 0), maxSafeInteger);
        };
        return function from(arrayLike) {
            var C = this;
            var items = Object(arrayLike);
            if (arrayLike == null) {
                throw new TypeError('Array.from requires an array-like object - not null or undefined');
            }
            var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
            var T;
            if (typeof mapFn !== 'undefined') {
                if (!isCallable(mapFn)) {
                    throw new TypeError('Array.from: when provided, the second argument must be a function');
                }
                if (arguments.length > 2) {
                    T = arguments[2];
                }
            }
            var len = toLength(items.length);
            var A = isCallable(C) ? Object(new C(len)) : new Array(len);
            var k = 0;
            var kValue;
            while (k < len) {
                kValue = items[k];
                if (mapFn) {
                    A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
                }
                else {
                    A[k] = kValue;
                }
                k += 1;
            }
            A.length = len;
            return A;
        };
    })();
}
if (typeof Object.assign != 'function') {
    Object.defineProperty(Object, 'assign', {
        value: function assign(target, varArgs) {
            'use strict';
            if (target == null) {
                throw new TypeError('Cannot convert undefined or null to object');
            }
            var to = Object(target);
            for (var index = 1; index < arguments.length; index++) {
                var nextSource = arguments[index];
                if (nextSource != null) {
                    for (var nextKey in nextSource) {
                        if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                            to[nextKey] = nextSource[nextKey];
                        }
                    }
                }
            }
            return to;
        },
        writable: true,
        configurable: true,
    });
}
var IPushPull = (function () {
    function IPushPull(settings) {
        this.settings = settings;
        this.config = new Config_1.Config();
        this.config.set(settings);
        this.crypto = new Crypto_1.Crypto();
        this.classy = new Classy_1.default();
        this.helpers = new Helpers_1.default();
        this.utils = new Utils_1.default();
        this.storage = new Storage_1.StorageService(this.config);
        this.api = new Api_1.Api(query.stringify, Q, this.storage, this.config, this.utils, request);
        this.auth = new Auth_1.Auth(Q, Timers_1.timers, this.api, this.storage, this.config, this.utils);
        this.Page = new Page_1.PageWrap(Q, Timers_1.timers, Timers_2.intervals, this.api, this.auth, this.storage, this.crypto, this.config);
        this.tracking = new Tracking_1.Tracking(Timers_2.intervals, this.storage);
        this.FreezingRange = Range_1.FreezingRange;
        this.PermissionRange = Range_2.PermissionRange;
        this.ButtonRange = Range_3.ButtonRange;
        this.StyleRange = Range_4.StyleRange;
        this.NotificationRange = Range_5.NotificationRange;
        this.AccessRange = Range_6.AccessRange;
        this.Functions = Functions_1.Functions;
        this.Grid = Grid_1.Grid;
        this.Pdf = new Pdf_1.default(Q, this.storage, this.config);
        this.Clipboard = Clipboard_1.Clipboard;
    }
    return IPushPull;
}());
function createInstance(defaultConfig) {
    return new IPushPull(defaultConfig);
}
var ipushpull = createInstance();
ipushpull.IPushpull = IPushPull;
ipushpull.Create = function (instanceConfig) {
    return createInstance(merge({
        api_url: 'http://ipushpull.dev/api/1.0',
        ws_url: 'http://ipushpull.dev',
        storage_prefix: 'ipp_local',
        api_key: 'LC8aOmGqqydL3n8HyKaz8ue34M13WPux2zyuBAl1',
        api_secret: 'kXXRR7jsoEm6JgjvBDYGca1YlGIDynuxpM9kiu9GTlqzLtmCvNWLm3pmzWUc6Uptp4DwvS1kLlC6mh5PHGGnKRNh0eBPG1IIUB5NtsEY1oSgqLcteNCdGBLnIwnOO7SY',
        transport: 'polling',
    }, instanceConfig));
};
module.exports = ipushpull;
module.exports.defaults = ipushpull;
//# sourceMappingURL=index.js.map