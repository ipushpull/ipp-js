"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ipushpull = require("../src/index-api");
global.window = {
    WebSocket: true,
};
global.io = require("socket.io")();
var ipp = new ipushpull.create();
var page = new ipp.page(1, 1);
console.log(ipp.config);
var pageReady = false;
page.on(page.EVENT_READY, function (data) {
    console.log("ready");
    pageReady = true;
});
page.on(page.EVENT_NEW_CONTENT, function (data) {
    if (!pageReady) {
        return;
    }
    console.log("new");
});
//# sourceMappingURL=node-api.test.js.map