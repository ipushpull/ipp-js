class Action {

    public valid: boolean = false;
    public type: string = "";
    public value: any;
    public cellRange: any = {
        from: {
            row: 0,
            col: 0,
        },
        to: {
            row: 0,
            col: 0,
        }
    };
    public func: string = "value";
    public exp: string = "++";
    public conditionMatch;

    private validFuncs = ["value", "row"];
    private alpha: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private styleConditions = [];

    constructor(public markup: string) {
        if (typeof markup !== "string") {
            return;
        }
        if (markup.indexOf(`_BUTTON`) === 0) {
            this.type = "button";
            this.valid = this.initButton(markup);
        }
        if (markup.indexOf(`_STYLE`) === 0) {
            this.type = "style";
            this.valid = this.initStyle(markup);
            if (this.valid) {
                this.matchCondition();
            }
        }
    }

    public run(cells: any): any {
        let cellUpdates = [];
        cells.forEach2((row, rowIndex) => {
            if (!row) return;
            row.forEach2((col, colIndex) => {
                if (!col) return;
                cellUpdates.push(this.data(rowIndex, colIndex, col));
            });
        });
        return cellUpdates;
    }

    public getAction(value): string {
        let conditions: any = [];
        this.styleConditions.forEach(condition => {
            conditions.push(JSON.stringify(condition).replace(/,/g,";"));
        });
        return `_STYLE(${value},${(conditions.join("|"))})`;
    }

    private data(row: number, col: number, cell: any): any {

        let data = {
            value: cell.value,
            formatted_value: cell.formatted_value,
        };

        let value;
        if (cell.action.valid && cell.action.type === "style") {
            value = cell.action.value;
        } else {
            value = cell.value;
        }
        value = parseFloat(value);
        value = eval(this.exp);
        if (cell.action.valid && cell.action.type === "style") {
            value = cell.action.getAction(value);
        }
        data.value = value;
        data.formatted_value = value; 

        return {
            row: row,
            col: col,
            data: data
        }
    }

    private getArguments(markup: string): any {
        let regExp = /\(([^)]+)\)/;
        return regExp.exec(markup);
    }

    private initStyle(markup): boolean {
        let matches = this.getArguments(markup);
        if (!matches || !matches.length) return false;
        let parts = matches[1].split(",");
        if (parts.length < 1) return false;
        this.value = parts[0];
        let conditions = parts[1].split("|");
        conditions.forEach2(string => {
            this.styleConditions.push(JSON.parse(string.replace(/;/g,",")));
        });
        return true;
    }

    private initButton(markup): boolean {
        let matches = this.getArguments(markup);
        if (!matches || !matches.length) return false;
        let parts = matches[1].split(",");
        if (parts.length < 3) return false;
        this.value = parts[0];
        this.setCellRange(parts[1]);
        // if (this.validFuncs.indexOf(parts[2]) > -1) {
        //     this.func = parts[2];
        // }
        this.exp = parts[2];
        return true;
    }

    private setCellRange(string: string): void {
        let cells = string.toUpperCase().split(":");
        if (cells.length < 2) {
            cells.push(cells[0]);
        }
        cells.forEach2((cell, index) => {
            let col: number = 0;
            let colLastIndex = 0;
            let row: string = "0";
            if (index >= 2) return;
            let parts = cell.split("");
            parts.forEach2((part, partIndex) => {
                if (this.alpha.indexOf(part) > -1) {
                    if (col) {
                        col += 26 + this.alpha.indexOf(part);
                    } else {
                        col = this.alpha.indexOf(part);
                    }
                } else {
                    colLastIndex = partIndex;
                }
            });
            row = parts.slice(colLastIndex, parts.length).join("");
            let where: string = index === 0 ? "from" : "to";
            this.cellRange[where].col = col;
            this.cellRange[where].row = parseInt(row, 10) - 1;
        })
    }

    private matchCondition(): void {
        this.conditionMatch = undefined;
        this.styleConditions.forEach(data => {
            let value = this.value;
            if (eval(data.condition)) {
                this.conditionMatch = data.style;
            }
        });
    }
}

export default Action;