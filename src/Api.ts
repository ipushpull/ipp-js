import * as uuidV4 from 'uuid/v4';
import Emitter from "./Emitter";
import * as merge from "merge";
import { IIPPConfig } from "./Config";
import { IStorageService } from "./Storage";

import IPromise = angular.IPromise;
import IDeferred = angular.IDeferred;

interface IRequest {
    method: (method: string) => IRequest;
    url: (method: string) => IRequest;
    headers: (method: { [s: string]: string }) => IRequest;
    data: (method: any) => IRequest;
    params: (method: { [s: string]: string }) => IRequest;
    cache: (method: boolean) => IRequest;
}

export interface IRequestResult<T> {
    success: boolean;
    data: T;
    httpCode: number;
    httpText: string;
}

export interface IApiTokens {
    access_token: string;
    refresh_token: string;
}

class Request implements IRequest {

    public static xipp: any = {
        uuid:  '',
        client: '',
        clientVersion: '',
        hsts: true
    };
    private _method: string;
    private _url: string;
    private _headers: { [s: string]: string } = {};
    private _data: any;
    private _params: any;
    private _cache: boolean = false;
    private _overrideLock: boolean = false;
    private _json: boolean = true;
    private _reponse_type: string = '';

    public static get(url: string): Request {
        return new Request("GET", url);
    }

    public static post(url: string): Request {
        return new Request("POST", url);
    }

    public static put(url: string): Request {
        return new Request("PUT", url);
    }

    public static del(url: string): Request {
        return new Request("DELETE", url);
    }

    constructor(method: string, url: string) {
        this._method = method;
        this._url = url;

        this._headers = {
            // "Strict-Transport-Security": "max-age=15768000;includeSubDomains",
            "Content-Type": "application/json",
            "x-requested-with": "XMLHttpRequest",
            "x-ipp-device-uuid": Request.xipp.uuid, // @todo get uuid somehow - local storage
            "x-ipp-client": Request.xipp.client, // @todo get client id - ipp config
            "x-ipp-client-version": Request.xipp.clientVersion // @todo get client version - ipp config
        };

        if (Request.xipp.hsts) {
            this._headers["Strict-Transport-Security"] = "max-age=15768000;includeSubDomains";
        }
    }

    // @todo Bleh...
    public get METHOD(): string {
        return this._method;
    }
    public get URL(): string {
        return this._url;
    }
    public get HEADERS(): { [s: string]: string } {
        return this._headers;
    }
    public get DATA(): any {
        return this._data;
    }
    public get PARAMS(): { [s: string]: string } {
        return this._params;
    }
    public get CACHE(): boolean {
        return this._cache;
    }
    public get OVERRIDE_LOCK(): boolean {
        return this._overrideLock;
    }
    public get JSON(): boolean {
        return this._json;
    }
    public get RESPONSE_TYPE(): string {
        return this._reponse_type;
    }

    public method(method: string): Request {
        this._method = method;
        return this;
    }

    public url(url: string): Request {
        this._url = url;
        return this;
    }

    public headers(headers: { [s: string]: string | number }, overwrite: boolean = false): Request {
        this._headers = overwrite ? headers : merge.recursive(true, this._headers, headers);
        return this;
    }

    public data(data: any, serialize: boolean = false): Request {
        if (serialize) {
            this._data = Object.keys(data)
                .map(function(k) {
                    return encodeURIComponent(k) + "=" + encodeURIComponent(data[k]);
                })
                .join("&");
        } else {
            this._data = data;
        }
        return this;
    }

    public params(params: { [s: string]: string | number }, overwrite: boolean = false): Request {
        this._params = overwrite ? params : merge.recursive(true, this._params, params);
        return this;
    }

    public cache(cache: boolean): Request {
        // Allow cache only for GET requests
        if (cache && this._method === "GET") {
            this._cache = cache;
        }

        return this;
    }

    public overrideLock(override: boolean = true): Request {
        this._overrideLock = override;
        return this;
    }

    public json(json: boolean = true): Request {
        this._json = json;
        return this;
    }

    public responseType(str: string = ""): Request {
        this._reponse_type = str;
        return this;
    }
}

export interface IApiService {
    tokens: IApiTokens;
    block: () => void;
    unblock: () => void;
    getSelfInfo: () => IPromise<IRequestResult>;
    refreshAccessTokens: (refreshToken: string) => IPromise<IRequestResult>;
    userLogin: (data: any) => IPromise<IRequestResult>;
    userLogout: (data?: any) => IPromise<IRequestResult>;
    createFolder: (data: any) => IPromise<IRequestResult>;
    getDomains: () => IPromise<IRequestResult>;
    getDomain: (domainId: number) => IPromise<IRequestResult>;
    updateDomain: (data: any) => IPromise<IRequestResult>;
    getDomainPages: (domainId: number) => IPromise<IRequestResult>;
    getDomainsAndPages: (client: string) => IPromise<IRequestResult>;
    getPage: (data: any) => IPromise<IRequestResult>;
    getPageByName: (data: any) => IPromise<IRequestResult>;
    getPageByUuid: (data: any) => IPromise<IRequestResult>;
    getPageAccess: (data: any) => IPromise<IRequestResult>;
    createPage: (data: any) => IPromise<IRequestResult>;
    createPageNotification: (data: any) => IPromise<IRequestResult>;
    createPageNotificationByUuid: (data: any) => IPromise<IRequestResult>;
    createAnonymousPage: (data: any) => IPromise<IRequestResult>;
    savePageContent: (data: any) => IPromise<IRequestResult>;
    savePageContentDelta: (data: any) => IPromise<IRequestResult>;
    savePageSettings: (data: any) => IPromise<IRequestResult>;
    deletePage: (data: any) => IPromise<IRequestResult>;
    saveUserInfo: (data: any) => IPromise<IRequestResult>;
    getUserMetaData: (data: any) => IPromise<IRequestResult>;
    saveUserMetaData: (data: any) => IPromise<IRequestResult>;
    deleteUserMetaData: (data: any) => IPromise<IRequestResult>;
    changePassword: (data: any) => IPromise<IRequestResult>;
    changeEmail: (data: any) => IPromise<IRequestResult>;
    forgotPassword: (data: any) => IPromise<IRequestResult>;
    resetPassword: (data: any) => IPromise<IRequestResult>;
    inviteUsers: (data: any) => IPromise<IRequestResult>;
    acceptInvitation: (data: any) => IPromise<IRequestResult>;
    refuseInvitation: (data: any) => IPromise<IRequestResult>;
    domainInvitations: (data: any) => IPromise<IRequestResult>;
    userInvitations: () => IPromise<IRequestResult>;
    domainAccessLog: (data: any) => IPromise<IRequestResult>;
    domainUsers: (data: any) => IPromise<IRequestResult>;
    signupUser: (data: any) => IPromise<IRequestResult>;
    activateUser: (data: any) => IPromise<IRequestResult>;
    setDomainDefault: (data: any) => IPromise<IRequestResult>;
    resendInvite: (data: any) => IPromise<IRequestResult>;
    updateDomainAccess: (data: any) => IPromise<IRequestResult>;
    removeUsersFromDomain: (data: any) => IPromise<IRequestResult>;
    getInvitation: (data: any) => IPromise<IRequestResult>;
    cancelInvitations: (data: any) => IPromise<IRequestResult>;
    getDomainAccessGroups: (data: any) => IPromise<IRequestResult>;
    getDomainAccessGroup: (data: any) => IPromise<IRequestResult>;
    addDomainAccessGroup: (data: any) => IPromise<IRequestResult>;
    putDomainAgroupMembers: (data: any) => IPromise<IRequestResult>;
    putDomainAgroupPages: (data: any) => IPromise<IRequestResult>;
    updateDomainAgroup: (data: any) => IPromise<IRequestResult>;
    deleteDomainAGroup: (data: any) => IPromise<IRequestResult>;
    getDomainPageAccess: (data: any) => IPromise<IRequestResult>;
    getDomainCustomers: (data: any) => IPromise<IRequestResult>;
    getDomainUsage: (data: any) => IPromise<IRequestResult>;
    saveDomainPageAccess: (data: any) => IPromise<IRequestResult>;
    getTemplates: (data: any) => IPromise<IRequestResult>;
    saveCustomer: (data: any) => IPromise<IRequestResult>;
    updateCustomer: (data: any) => IPromise<IRequestResult>;
    removeCustomer: (data: any) => IPromise<IRequestResult>;
    getDocEmailRules: (data: any) => IPromise<IRequestResult>;
    createDocEmailRule: (data: any) => IPromise<IRequestResult>;
    updateDocEmailRule: (data: any) => IPromise<IRequestResult>;
    deleteDocEmailRule: (data: any) => IPromise<IRequestResult>;
    getOrganization: (data: any) => IPromise<IRequestResult>;
    getOrganizationDomains: (data: any) => IPromise<IRequestResult>;
    getOrganizationUsers: (data: any) => IPromise<IRequestResult>;
    getOrganizationUsage: (data: any) => IPromise<IRequestResult>;
    getApplicationPageList: (client: string) => IPromise<IRequestResult>;
    getSsoStatus: (email: string) => IPromise<IRequestResult>;
    downloadPage: (data: any) => IPromise<IRequestResult>;
}

export class Api extends Emitter implements IApiService {
    public static $inject: string[] = [
        "$httpParamSerializerJQLike",
        "$q",
        "ippStorageService",
        "ippConfig",
        "ippUtilsService",
        "ippReqService"
    ];
    public get EVENT_401(): string {
        return "401";
    } // listener

    public tokens: IApiTokens = {
        access_token: "",
        refresh_token: ""
    };
    private _endPoint: string;
    private _locked: boolean = false;
    private request: any;

    constructor(
        private qs: any,
        private q: any,
        private storage: IStorageService,
        private config: IIPPConfig,
        private utils: any,
        private req: any
    ) {
        super();
        this.request = req;
        this._endPoint = `${this.config.api_url}`;
        let uuid = storage.persistent.get('ipp_uuid', '', true);
        if (!uuid) {
            uuid = uuidV4();
            storage.persistent.save('ipp_uuid', uuid, 365, true);
        }
        Request.xipp = {
            uuid:  this.config.uuid || uuid,
            client: this.config.api_key,
            clientVersion: this.config.client_version || '1.0',
            hsts: this.config.hsts === undefined || this.config.hsts,
        };
    }

    public block(): void {
        this._locked = true;
    }

    public unblock(): void {
        this._locked = false;
    }

    public getSelfInfo(): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/users/self/")
                .cache(false)
                .overrideLock()
        );
    }

    public refreshAccessTokens(refreshToken: string): IPromise<IRequestResult> {
        return this.send(
            Request.post(this._endPoint + "/oauth/token/")
                .data({
                    grant_type: "refresh_token",
                    client_id: this.config.api_key,
                    client_secret: this.config.api_secret,
                    refresh_token: refreshToken
                }, true)
                .headers({
                    "Content-Type": "application/x-www-form-urlencoded",
                    // "x-ipp-client": this.config.api_key
                })
                .json(false)
                .overrideLock()
        );
    }

    public userLogin(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.post(this._endPoint + "/oauth/token/")
                .data(
                    {
                        grant_type: "password",
                        client_id: this.config.api_key,
                        client_secret: this.config.api_secret,
                        username: data.email,
                        password: data.password
                    },
                    true
                )
                .headers({
                    "Content-Type": "application/x-www-form-urlencoded"
                })
                .json(false)
        );
    }

    public userLoginByCode(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.post(this._endPoint + "/oauth/token/")
                .params(
                    merge(
                        {
                            grant_type: "authorization_code",
                            client_id: this.config.api_key,
                            client_secret: this.config.api_secret
                        },
                        data
                    )
                )
                .headers({
                    "Content-Type": "application/x-www-form-urlencoded"
                })
        );
    }

    public userLogout(data: any = {}): IPromise<IRequestResult> {
        return this.send(
            Request.post(this._endPoint + "/oauth/logout/").params({ all: data.all || "" })
        );
    }

    public getDomains(): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/domains/"));
    }

    public getDomain(domainId: number): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/domains/" + domainId + "/"));
    }

    public createFolder(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/domains/").data(data.data));
    }

    public createDomain(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/domains/").data(data.data));
    }

    public updateDomain(data: any): IPromise<IRequestResult> {
        return this.send(Request.put(this._endPoint + "/domains/" + data.domainId + "/").data(data.data));
    }

    public disableDomain(data: any): IPromise<IRequestResult> {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/").data(data.data));
    }

    public getDomainPages(domainId: number): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/domains/" + domainId + "/page_access/"));
    }

    public getDomainsAndPages(client: string): IPromise<IRequestResult> {
        if (!client) {
            client = "";
        }
        return this.send(Request.get(this._endPoint + "/domain_page_access/").params({ client: client }));
    }

    public getPage(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(
                this._endPoint + "/domains/id/" + data.domainId + "/page_content/id/" + data.pageId + "/"
            ).params({ client_seq_no: data.seq_no })
        );
    }

    public getPageByName(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(
                this._endPoint + "/domains/name/" + data.domainId + "/page_content/name/" + data.pageId + "/"
            ).params({ client_seq_no: data.seq_no })
        );
    }

    public getPageByUuid(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/internal/page_content/" + data.uuid + "/").params({
                client_seq_no: data.seq_no
            })
        );
    }

    public getPageVerions(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/page/id/" + data.pageId + "/versions/").params({
                before: data.before,
                after: data.after,
                max: data.max,
                page: data.page
            })
        );
    }

    public getPageVerion(data: any): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/page/id/" + data.pageId + "/version/" + data.seqNo + "/"));
    }

    public restorePageVerion(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(this._endPoint + "/page/id/" + data.pageId + "/version/" + data.seqNo + "/restore/")
        );
    }

    public getPageAccess(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/domains/id/" + data.domainId + "/page_access/id/" + data.pageId + "/")
        );
    }

    public createPage(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/pages/").data(data.data));
    }

    public createPageNotification(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/page/" + data.pageId + "/notification/").data(data.data));
    }

    public createPageNotificationByUuid(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/page/" + data.uuid + "/notification/").data(data.data));
    }

    public createAnonymousPage(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/anonymous/page/").data(data.data));
    }

    public savePageContent(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(this._endPoint + "/domains/id/" + data.domainId + "/page_content/id/" + data.pageId + "/").data(
                data.data
            )
        );
    }

    public savePageContentDelta(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(
                this._endPoint + "/domains/id/" + data.domainId + "/page_content_delta/id/" + data.pageId + "/"
            ).data(data.data)
        );
    }

    public savePageSettings(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(this._endPoint + "/domains/" + data.domainId + "/pages/" + data.pageId + "/").data(data.data)
        );
    }

    public deletePage(data: any): IPromise<IRequestResult> {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/pages/" + data.pageId + "/"));
    }

    public saveUserInfo(data: any): IPromise<IRequestResult> {
        return this.send(Request.put(this._endPoint + "/users/self/").data(data));
    }

    public getUserMetaData(data: any): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/users/" + data.userId + "/meta/").data(data.data));
    }

    public saveUserMetaData(data: any): IPromise<IRequestResult> {
        return this.send(Request.put(this._endPoint + "/users/" + data.userId + "/meta/").data(data.data));
    }

    public deleteUserMetaData(data: any): IPromise<IRequestResult> {
        return this.send(Request.del(this._endPoint + "/users/" + data.userId + "/meta/").data(data.data));
    }

    public changePassword(data: any): IPromise<IRequestResult> {
        return this.send(Request.put(this._endPoint + "/credentials/self/").data(data));
    }

    public changeEmail(data: any): IPromise<IRequestResult> {
        return this.send(Request.put(this._endPoint + "/credentials/self/").data(data));
    }

    public forgotPassword(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/password_reset/").data(data));
    }

    public resetPassword(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/password_reset/confirm/").data(data));
    }

    public inviteUsers(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/invitations/").data(data.data));
    }

    public acceptInvitation(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/users/invitation/confirm/").data(data));
    }

    public refuseInvitation(data: any): IPromise<IRequestResult> {
        return this.send(Request.del(this._endPoint + "/users/invitation/confirm/").data(data));
    }

    public domainInvitations(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/domains/" + data.domainId + "/invitations/").params({ is_complete: "False" })
        );
    }

    public userInvitations(): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/users/self/invitations/").params({ is_complete: "False" }));
    }

    public domainAccessLog(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/domain_access/" + data.domainId + "/events/").params({
                page_size: data.limit
            })
        );
    }

    public domainUsers(data: any): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/domain_access/" + data.domainId + "/users/"));
    }

    public signupUser(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/users/signup/").data(data));
    }

    public activateUser(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/users/signup/confirm/").data(data));
    }

    public setDomainDefault(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(this._endPoint + "/domain_access/" + data.domainId + "/users/self/").data(data.data)
        );
    }

    public resendInvite(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(this._endPoint + "/domains/" + data.domainId + "/invitations/" + data.inviteId + "/resend/")
        );
    }

    public updateDomainAccess(data: any): IPromise<IRequestResult> {
        return this.send(Request.put(this._endPoint + "/domain_access/" + data.domainId + "/users/").data(data.data));
    }

    public removeUsersFromDomain(data: any): IPromise<IRequestResult> {
        return this.send(Request.del(this._endPoint + "/domain_access/" + data.domainId + "/users/").data(data.data));
    }

    public getInvitation(data: any): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/users/invitations/" + data.token + "/"));
    }

    public cancelInvitations(data: any): IPromise<IRequestResult> {
        return this.send(Request.del(this._endPoint + "/domains/" + data.domainId + "/invitations/").data(data.data));
    }

    public getDomainAccessGroups(data: any): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/access_groups/"));
    }

    public getDomainAccessGroup(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.groupId + "/")
        );
    }

    public addDomainAccessGroup(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.post(this._endPoint + "/domains/" + data.domainId + "/access_groups/").data(data.data)
        );
    }

    public putDomainAgroupMembers(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.post(
                this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/members/"
            ).data(data.data)
        );
    }

    public putDomainAgroupPages(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.post(
                this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/pages/"
            ).data(data.data)
        );
    }

    public updateDomainAgroup(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/").data(
                data.data
            )
        );
    }

    public deleteDomainAGroup(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.del(this._endPoint + "/domains/" + data.domainId + "/access_groups/" + data.agroupId + "/")
        );
    }

    public getDomainPageAccess(data: any): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/domain_page_access/" + data.domainId + "/"));
    }

    public getDomainCustomers(data: any): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/customers/"));
    }

    public getDomainUsage(data: any = {}): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/domains/id/" + data.domainId + "/usage/").params({
                from_date: data.fromDate,
                to_date: data.toDate
            })
        );
    }

    public saveDomainPageAccess(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(this._endPoint + "/domain_page_access/" + data.domainId + "/basic/").data(data.data)
        );
    }

    public getTemplates(data: any): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/templates/"));
    }

    public saveCustomer(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/customers/").data(data.data));
    }

    public updateCustomer(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(this._endPoint + "/domains/" + data.domainId + "/customers/" + data.data.id + "/").data(
                data.data
            )
        );
    }

    public removeCustomer(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.del(this._endPoint + "/domains/" + data.domainId + "/customers/" + data.customerId + "/")
        );
    }

    public getDocEmailRules(data: any): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/domains/" + data.domainId + "/docsnames/"));
    }

    public createDocEmailRule(data: any): IPromise<IRequestResult> {
        return this.send(Request.post(this._endPoint + "/domains/" + data.domainId + "/docsnames/").data(data.data));
    }

    public updateDocEmailRule(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(this._endPoint + "/domains/" + data.domainId + "/docsnames/" + data.docRuleId + "/").data(
                data.data
            )
        );
    }

    public deleteDocEmailRule(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.del(this._endPoint + "/domains/" + data.domainId + "/docsnames/" + data.docRuleId + "/")
        );
    }

    private send(request: Request): IPromise<IRequestResult> {
        let q: IDeferred<any> = this.q.defer();

        // Add auth header
        let token: string = "";
        if (this.storage) {
            token = this.storage.persistent.get("access_token");
        }
        if (!token && this.tokens && this.tokens.access_token) {
            token = this.tokens.access_token;
        }

        if (token) {
            request.headers({
                Authorization: `Bearer ${token}`
            });
        }

        // @todo Proper type...
        let provider: any = this._locked && !request.OVERRIDE_LOCK ? this.dummyRequest : this.request;

        // for now, disabled cache on all requests
        request.cache(false);

        // @todo Add micro time to get requests - !!STUPID IE!!
        /*if (request.METHOD === "GET" && ipp.config.isIE){
            request.params({ie: new Date().getTime()});
        }*/

        let r: IPromise<any> = provider(
            {
                url: `${request.URL}?${this.qs(request.PARAMS)}`,
                cache: request.CACHE,
                method: request.METHOD,
                // qs: request.PARAMS,
                data: request.DATA,
                headers: request.HEADERS,
                resolveWithFullResponse: true,
                json: request.JSON,
                responseType: request.RESPONSE_TYPE
            },
            (err, resp, body) => {
                if (err || resp.statusCode >= 300) {
                    if (err) {
                        q.reject(err);
                    } else {
                        if (!request.JSON) {
                            try {
                                body = JSON.parse(body);
                            } catch (e) {
                                body = {};
                            }
                        }
                        let error: any = {
                            method: resp.method,
                            data: body,
                            code: resp.statusCode,
                            statusCode: resp.statusCode,
                            httpCode: resp.statusCode,
                            error: this.utils.parseApiError({ data: body }) || "Error",
                            httpText: this.utils.parseApiError({ data: body }) || "Error",
                            message: this.utils.parseApiError({ data: body }) || "Error"
                        };
                        // Emit 401
                        if (error.code === 401 && !this._locked && error.message !== "invalid_grant") {
                            this.emit(this.EVENT_401);
                        }
                        q.reject(error);
                    }
                } else {
                    if (!request.JSON) {
                        try {
                            resp.body = JSON.parse(resp.body);
                        } catch (e) {
                            resp.body = {};
                        }
                    }
                    q.resolve(this.handleSuccess(resp));
                }
            }
        );

        return q.promise;
    }

    public getApplicationPageList(client: string = ""): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/application_page_list/").params({
                client: client
            })
        );
    }

    public getOrganization(data: any): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/"));
    }

    public getOrganizationUsers(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/organizations/" + data.organizationId + "/users/").params({
                query: data.query
            })
        );
    }

    public getOrganizationUsage(data: any = {}): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/organizations/self/usage/").params({
                from_date: data.fromDate,
                to_date: data.toDate
            })
        );
    }

    public getOrganizationUser(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/organizations/" + data.organizationId + "/users/" + data.userId + "/")
        );
    }

    public createOrganizationUser(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.post(this._endPoint + "/organizations/" + data.organizationId + "/users/").data(data.data)
        );
    }

    public saveOrganizationUser(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(this._endPoint + "/organizations/" + data.organizationId + "/users/" + data.userId + "/").data(
                data.data
            )
        );
    }

    public getOrganizationDomains(data: any): IPromise<IRequestResult> {
        return this.send(Request.get(this._endPoint + "/organizations/" + data.organizationId + "/domains/"));
    }

    public getOrganizationDomain(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/organizations/" + data.organizationId + "/domains/" + data.domainId + "/")
        );
    }

    public createOrganizationDomain(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.post(this._endPoint + "/organizations/" + data.organizationId + "/domains/").data(data.data)
        );
    }

    public saveOrganizationDomain(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.put(
                this._endPoint + "/organizations/" + data.organizationId + "/domains/" + data.domainId + "/"
            ).data(data.data)
        );
    }

    public getSsoStatus(email: string): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/sso/status/").params({
                user: email,
            })
        );
    }

    public downloadPage(data: any): IPromise<IRequestResult> {
        return this.send(
            Request.get(this._endPoint + "/page/" + data.pageId + "/download/" )
            .params({
                live: data.live === undefined ? "true" : "false",
                snapshot: data.snapshot === undefined ? "true" : "false"
            })
            .headers({
                // "Content-Disposition": "attachment; filename='test.xlxs'",
                // "Content-Type": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            })
            .responseType("arraybuffer")
        );
    }

    private dummyRequest = (data: any): IPromise<any> => {
        console.log("Api is locked down, preventing call " + data.url);

        let q: IDeferred<any> = this.q.defer();

        q.reject({
            data: {},
            status: 666,
            statusText: "Api is locked",
            config: data
        });

        return q.promise;
    };

    private handleSuccess = (response: any): IPromise<IRequestResult> => {
        return {
            success: true,
            data: response.body,
            httpCode: parseInt(response.statusCode, 10),
            httpText: response.statusMessage
        };

        // console.log(response);
        let q: IDeferred<IRequestResult> = this.q.defer();

        q.resolve({
            success: true,
            data: response.body,
            httpCode: parseInt(response.statusCode, 10),
            httpText: response.statusMessage
        });

        return q.promise;
    };
}
