export interface IIPPConfig {
    api_url?: string;
    ws_url?: string;
    docs_url?: string;
    api_key: string;
    api_secret: string;
    transport?: string;
    storage_prefix?: string;
    cookie?: any;
    client_version?: any;
    uuid?: any;
    hsts?: boolean;
}

export class Config {

    private _config: IIPPConfig = {
        api_url: "https://www.ipushpull.com/api/1.0",
        ws_url: "https://www.ipushpull.com",
        docs_url: "https://docs.ipushpull.com",
        api_key: "",
        api_secret: "",
        transport: "",
        storage_prefix: "ipp",
        cookie: {
            oauth_access_token: "access_token",
            ouath_refresh_token: "refresh_token",
            uuid: "uuid",
        },
        client_version: "",
        uuid: "",
        hsts: true
    };

    public set(config: IIPPConfig): void {

        if (!config) {
            return;
        }

        for (let prop in config) {
            if (config.hasOwnProperty(prop)) {
                this._config[prop] = config[prop];
            }
        }

        if (config.api_url && !config.ws_url) {
            let parts: string[] = config.api_url.split("/");
            this._config.ws_url = parts[0] + "//" + parts[2];
        }

    }

    public get api_url() {
        return this._config.api_url;
    }

    public get ws_url() {
        return this._config.ws_url;
    }

    public get docs_url() {
        return this._config.docs_url;
    }

    public get api_key() {
        return this._config.api_key;
    }

    public get api_secret() {
        return this._config.api_secret;
    }

    public get transport() {
        return this._config.transport;
    }

    public get storage_prefix() {
        return this._config.storage_prefix;
    }

    public get cookie() {
        return this._config.cookie;
    }

    public get uuid() {
        return this._config.uuid;
    }

    public get client_version() {
        return this._config.client_version;
    }

    public get hsts() {
        return this._config.hsts;
    }

    public $get(): IIPPConfig {
        return this._config;
    }

}

// export default Config;