import * as merge from 'merge';
import Emitter from './Emitter';
import { IPage } from './Page/Page';
import { IContentCellMeta } from './Grid2/Content';
import Helpers, { ICellRange, IHelpers } from './Helpers';
import { IPageService } from './Page/Page';
import { IPageContentCell } from './Page/Content';

import { Parser } from 'hot-formula-parser';
import { IPageContentProvider } from './Content/Content';

interface IData {
  value: any;
  formatted_value?: any;
  reset_value?: any;
  reset_time?: any;
}

interface IAction {
  cell?: string;
  offset?: string;
  data: IData;
}

interface ITask {
  condition?: string;
  cell?: string;
  operator?: string;
  value?: string;
  actions: IAction[];
}

interface ITaskGroup {
  ref?: string;
  range?: string;
}

interface ICellUpdate {
  row: number;
  col: number;
  data: IData;
}

interface IUser {
  id: any;
  username: string;
  last_name: string;
  first_name: string;
  email: string;
}

interface IVar {
  email: string;
  firstname: string;
  lastname: string;
}

interface IStyleCondition {
  exp?: string;
  operator?: string;
  cell?: string;
  value?: string;
  style: any;
  columns: any[];
}

interface IStyle {
  ref?: string;
  range?: string;
  conditions: IStyleCondition[];
}

interface IButton {
  name: string;
  range: string;
}

export class Functions extends Emitter {
  private helpers: IHelpers;
  // private user: IUser;
  private userMapping = {
    USER_ID: 'id',
    USER_NAME: 'username',
    USER_FIRST_NAME: 'first_name',
    USER_LAST_NAME: 'last_name',
    USER_EMAIL: 'email',
  };
  private functions = ['VALUE', 'OFFSET', 'USER', 'SYMPHONY_TAG', 'DATE_TIME'];
  private parser: any;
  private cell: IPageContentCell;
  constructor(public page: IPageService, public vars: IUser) {
    super();
    this.helpers = new Helpers();
    this.parser = new Parser();
    this.parser.on('callCellValue', (cellCoord, done) => {
      if (
        this.page.Content.current[cellCoord.row.index] &&
        this.page.Content.current[cellCoord.row.index][cellCoord.column.index]
      ) {
        done(this.page.Content.current[cellCoord.row.index][cellCoord.column.index]);
      } else {
        done('#ERROR!');
      }
    });
    this.parser.setFunction('IPPVAR', params => {
      if (!params[0]) {
        return '#ERROR!';
      }
      let p = `${params[0]}`.toLowerCase();
      if (!this.vars[p]) {
        return '#ERROR!';
      }
      return this.vars[p];
    });
    this.parser.setFunction('VALUE', params => {
      if (params[0] === '#ERROR!') {
        return '#ERROR!';
      }
      if (!params[0]) {
        return this.cell ? this.getCellValue(this.cell) : '#ERROR!';
      }
      return this.getCellValue(params[0]);
    });
    this.parser.setFunction('RAWVALUE', params => {
      if (params[0] === '#ERROR!') {
        return '#ERROR!';
      }
      if (!params[0]) {
        return this.cell ? this.getCellValue(this.cell, true) : '#ERROR!';
      }
      return this.getCellValue(params[0], true);
    });
    this.parser.setFunction('OFFSET', params => {
      if (!this.cell || params[0] === undefined || params[1] === undefined || isNaN(params[0]) || isNaN(params[1])) {
        return '#ERROR!';
      }
      let row: number = parseFloat(params[0]);
      let col: number = parseFloat(params[1]);
      if (
        this.page.Content.current[this.cell.index.row + row] &&
        this.page.Content.current[this.cell.index.row + row][this.cell.index.col + col]
      ) {
        return this.page.Content.current[this.cell.index.row + row][this.cell.index.col + col];
      }
      return '#ERROR!';
    });
    this.parser.setFunction('DATETIME', params => {
      const time = moment
        .tz(params[1] || 'UTC')
        .add(params[2] || 0, params[3] || 'minutes')
        .format(params[0]);
      return time;
    });
    this.parser.setFunction('SYTAG', params => {
      if (params[0] === undefined) {
        return '#ERROR!';
      }
      let message: string = '';
      let arg: string = params[1];
      switch (params[0]) {
        case 'mention':
          // arg = arg.replace(/[^a-z0-9]/gi, '');
          if (this.helpers.isValidEmail(arg)) {
            message = `<mention email="${arg}" strict="false"/>`;
          } else {
            message = '#ERROR!';
          }
          break;
        case 'cash':
          if (!arg) {
            arg = `ipp.${this.page.data.domain_name}.${this.page.data.name}`;
          }
          message = `<cash tag="${this.helpers.safeTitle(arg)}" />`;
          break;
        case 'hash':
          message = `<hash tag="${this.helpers.safeTitle(arg)}" />`;
          break;
        case 'chime':
          message = `<chime />`;
          break;
        case 'emoji':
          message = `<emoji shortcode="${this.helpers.safeTitle(arg)}" />`;
          break;
        default:
          message = '#ERROR!';
          break;
      }
      return message;
    });
  }
  public parse(str: string): string {
    let results: string[] = [];
    let lines = str.split('\n');
    lines.forEach(line => {
      line = line.trim();
      if (!line || line[0] !== '=') {
        results.push(line);
        return;
      }
      let result = this.parser.parse(this.toUppercaseFunctions(line.replace('=', '')));
      results.push(result.result || result.error);
    });
    return results.join('<br />');
  }
  public getCellReference(str: string): IPageContentCell {
    let result = this.parser.parse(str.trim());
    return result.result || null;
  }
  public setCellReference(cell: IPageContentCell): void {
    this.cell = cell;
  }
  public isButtonWithinTaskRange(buttonCell: IContentCellMeta, taskGroup: ITaskGroup): boolean {
    if (!taskGroup.range && taskGroup.ref !== buttonCell.button.name) return false;
    try {
      const range: string = taskGroup.ref ? buttonCell.button.range : taskGroup.range;
      let cellRange: ICellRange = this.helpers.cellRange(
        range,
        this.page.Content.current.length,
        this.page.Content.current[0].length,
      );
      // const [taskFrom, taskTo] = this.helpers.getRefIndex(range); // range.split(':').map(str => this.helpers.getRefIndex(str));
      // let endRow: number = taskTo[0] < 0 ? this.page.Content.current.length : taskTo[0];
      //   const [buttonsFrom, buttonsTo] = buttonRange.split(':').map(str => this.helpers.getRefIndex(str));
      let endRow: number = cellRange.to.row < 0 ? this.page.Content.current.length : cellRange.to.row;
      if (buttonCell.position.row < cellRange.from.row || buttonCell.position.row > endRow) return false;
      if (buttonCell.position.col < cellRange.from.col || buttonCell.position.col > cellRange.to.col) return false;
      return true;
    } catch (e) {
      return false;
    }
  }
  public isTaskValid(task: ITask): boolean {
    if (!task.condition) return true;
    try {
      return this.parse(task.condition) === 'true';
    } catch (e) {
      return false;
    }
  }
  public updateCell(action: IAction): ICellUpdate {
    try {
      if (!action.cell) return null;
      const cell: IPageContentCell = this.getCellReference(action.cell.replace('=', '').toUpperCase());
      if (!cell) return null;

      let formatted_value = this.parse(action.data.formatted_value);
      let value = this.parse(action.data.value);

      if (this.helpers.isNumber(formatted_value)) {
        formatted_value = parseFloat(formatted_value);
      }

      if (this.helpers.isNumber(value)) {
        value = parseFloat(value);
      }

      const update: ICellUpdate = {
        row: cell.index.row,
        col: cell.index.col,
        data: {
          formatted_value,
          value,
        },
      };
      this.page.Content.updateCell(update.row, update.col, update.data);

      return update;
    } catch (e) {
      return null;
    }
  }
  public setCellFormatting(styles: IStyle[], buttons: IButton[], contentDiff?: IPageContentProvider): any {
    if (!styles || !styles.length) {
      return [];
    }
    // let dirty: boolean = false;
    let cellStyles: any[] = [];
    styles.forEach(style => {
      // check for a button ref
      let range: string = style.range || '';
      if (!style.range) {
        buttons.forEach(button => {
          if (button.name === style.ref) {
            range = button.range;
          }
        });
      }
      if (!range) {
        return;
      }
      // let content: IPageContentProvider = contentDiff || this.page.Content.current;
      let cellRange: ICellRange = this.helpers.cellRange(
        range,
        this.page.Content.current.length,
        this.page.Content.current[0].length,
      );
      // const from = cellRange.from;
      // const to = cellRange.to;
      // const [from, to] = range.split(':').map(str => this.helpers.getRefIndex(str));
      let endRow: number = cellRange.to.row < 0 ? this.page.Content.current.length : cellRange.to.row;
      for (let colIndex = cellRange.from.col; colIndex <= cellRange.to.col; colIndex++) {
        for (let rowIndex = 0; rowIndex < this.page.Content.current.length; rowIndex++) {
          if (rowIndex < cellRange.from.row || rowIndex > endRow) continue;
          let cell: IPageContentCell = contentDiff && contentDiff[rowIndex] && contentDiff[rowIndex][colIndex] ? contentDiff[rowIndex][colIndex] : this.page.Content.getCell(rowIndex, colIndex);
          // cell.formatting = {};
          this.setCellReference(cell);
          style.conditions.forEach(condition => {
            if (this.parse(condition.exp) !== 'true') return;
            // cell.formatting = condition.style;


            if (condition.columns.indexOf(-1) > -1) {
              for (let colConditionIndex = 0; colConditionIndex < this.page.Content.current[rowIndex].length; colConditionIndex++) {
                if (!cellStyles[rowIndex]) {
                  cellStyles[rowIndex] = [];
                }
                cellStyles[rowIndex][colConditionIndex] = condition.style;
              }
            } else {
              if (!cellStyles[rowIndex]) {
                cellStyles[rowIndex] = [];
              }
              cellStyles[rowIndex][colIndex] = condition.style;
            }
          });
        }
      }
    });
    return cellStyles;
  }
  public toUppercaseFunctions(expression: string): string {
    // =value()="three" -> =VALUE()="three"

    const regex = /("(.*?)")/gm;
    let m;
    let replace = [];

    while ((m = regex.exec(expression)) !== null) {
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }
      replace.push(m[1]);
    }

    replace.forEach((str, i) => {
      expression = expression.replace(str, `[${i}]`);
    });

    expression = expression.toUpperCase();

    replace.forEach((str, i) => {
      expression = expression.replace(`[${i}]`, str);
    });

    return expression;
  }
  private getCellValue(cell: IPageContentCell, raw: boolean = false): any {
    let value;
    if (raw) {
      value = cell.value;
    } else {
      value = cell.formatted_value || cell.value;
    }
    return this.helpers.isNumber(value) ? parseFloat(value) : value;
  }
}
