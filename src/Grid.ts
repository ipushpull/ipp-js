import Emitter from "./Emitter";
import Classy from "./Classy";
import Helpers from "./Helpers";
import { FreezingRange } from "./Page/Range";
import { PermissionRange } from "./Page/Range";
import { ButtonRange } from "./Page/Range";
import { StyleRange } from "./Page/Range";
import * as _ from "underscore";
import * as Hammer from "hammerjs";
import * as ua from "ua-parser-js";
import * as merge from "merge";

interface IStage {
    x: IStageCoords;
    y: IStageCoords;
    // height: number;
    // width: number;
    // scale: number;
}
interface IStageCoords {
    pos: number;
    // offset: number;
    // index: number;
    increments: any;
    size: any;
    stop: boolean;
    label: string;
}
interface ICanvas {
    buffer: any;
    bufferContext: any;
    canvas: any;
    canvasContext: any;
}

export class Grid extends Emitter {
    public get ON_CELL_CLICKED(): string {
        return "cell_clicked";
    }
    public get ON_CELL_CLICKED_RIGHT(): string {
        return "cell_clicked_right";
    }
    public get ON_CELL_HISTORY_CLICKED(): string {
        return "cell_history_clicked";
    }
    public get ON_CELL_LINK_CLICKED(): string {
        return "cell_link_clicked";
    }
    public get ON_CELL_DOUBLE_CLICKED(): string {
        return "cell_double_clicked";
    }
    public get ON_TAG_CLICKED(): string {
        return "tag_clicked";
    }
    public get ON_CELL_VALUE(): string {
        return "cell_value";
    }
    public get ON_CELL_SELECTED(): string {
        return "cell_selected";
    }
    public get ON_CELL_RESET(): string {
        return "cell_reset";
    }
    public get ON_EDITING(): string {
        return "editing";
    }

    public containerEl: any;
    public scale: number = 1;
    public ratio: number = 1;
    public ratioScale: number = 1;
    public pause: boolean = false;
    public canEdit = false;
    public scrollbars = {
        width: 8,
        inset: false,
        scrolling: false,
        x: {
            dimension: "width",
            label: "col",
            show: false,
            anchor: "left",
            track: undefined,
            handle: undefined
        },
        y: {
            dimension: "height",
            label: "row",
            show: false,
            anchor: "top",
            track: undefined,
            handle: undefined
        }
    };
    public alwaysEditing: boolean = false;
    public disallowSelection: boolean = false;
    public historyIndicatorSize: number = 16;
    public trackingHighlightClassname: string = "yellow";
    public trackingHighlightMatchColor: boolean = true;
    public trackingHighlightLifetime: number = 3000;
    public hasFocus: boolean = false;
    public fluid: boolean = false;
    public noAccessImages: any = {
        light: "",
        dark: ""
    };

    private stage: IStage;
    private borders = {
        widths: {
            none: 0,
            thin: 1,
            medium: 2,
            thick: 3
        },
        styles: {
            none: "solid",
            solid: "solid",
            double: "double"
        },
        names: {
            t: "top",
            r: "right",
            b: "bottom",
            l: "left"
        }
    };
    private originalContent: any = [];
    private content: any = [];
    private history: any = [];
    private trackingHighlights: any = [];
    private gc: ICanvas;
    private offsets = {
        x: {
            offset: 0,
            index: 0,
            max: 0,
            maxOffset: 0
        },
        y: {
            offset: 0,
            index: 0,
            max: 0,
            maxOffset: 0
        }
    };
    // visible area
    private width: number = 0;
    private height: number = 0;
    private visible: any = [];
    private gridLines: any = [];
    private found: any = [];
    private foundSelection: any = {
        count: 0,
        selected: 0,
        cell: undefined
    };
    private touch: boolean = false;
    private allowBrowserScroll: boolean = false;
    private hammer;
    private links: any[] = [];

    private styleRanges: any = [];
    private buttonRanges: any = [];
    private accessRanges: any = [];
    private freezeRange: any = {
        valid: false,
        index: {
            row: 0,
            col: 0
        }
    };
    private classy;
    private helpers;
    private containerRect = {
        x: 0,
        y: 0,
        height: 0,
        width: 0
    };
    private _tracking: boolean = false;
    private _editing: boolean = false;
    private _fit: string = "scroll";
    private userTrackingColors = ["deeppink", "aqua", "tomato", "orange", "yellow", "limegreen"];
    private selection;
    private historyIndicator;
    private input;
    private cellSelection = {
        from: undefined,
        to: undefined,
        last: undefined,
        go: false, // allows multiple cell selections
        on: false,
        clicked: 0,
        found: undefined
    };
    private filters = {};
    private sorting = {};
    private dirty = false;
    private rightClick = false;
    private animation: any;
    private keyValue: string = "";
    private ctrlDown: boolean = false;
    private shiftDown: boolean = false;
    private freeScroll: any;
    private cellStyles: string[] = [
        "background-color",
        "color",
        "font-family",
        "font-size",
        "font-style",
        "font-weight",
        "height",
        "number-format",
        "text-align",
        "text-wrap",
        "width",
        "vertical-align"
    ];
    private init: boolean = false;
    private hidden: boolean = false;

    constructor(container: any, fluid: boolean = false, options: any = {}) {
        super();

        this.destroy();

        this.fluid = fluid;
        this.init = false;
        if (options.inset_scrollbars) {
            this.scrollbars.inset = options.inset_scrollbars;
        }

        let parser = new ua();
        let parseResult = parser.getResult();
        this.touch =
            parseResult.device && (parseResult.device.type === "tablet" || parseResult.device.type === "mobile");
        this.classy = new Classy();
        this.helpers = new Helpers();

        if (typeof container === "string") {
            this.containerEl = document.getElementById(container);
        } else {
            this.containerEl = container;
        }
        this.containerEl.style.overflow = "hidden";
        let buffer = document.createElement("canvas");
        let canvas = document.createElement("canvas");
        // canvas.style.width = "100%";
        this.gc = {
            buffer: buffer,
            bufferContext: buffer.getContext("2d"),
            canvas: canvas,
            canvasContext: canvas.getContext("2d")
        };
        // this.gc.buffer.style.letterSpacing = "-.5px";
        // this.gc.canvas.style.letterSpacing = "-.5px";
        this.containerEl.appendChild(this.gc.canvas);
        this.ratio = window.devicePixelRatio;
        this.scale = this.ratio;
        this.ratioScale = this.scale;

        this.resetStage();
        // this.setSize();
        this.createSelector();

        document.addEventListener("visibilitychange", this.onVisibilityEvent, false);
        window.addEventListener("resize", this.onResizeEvent);
        window.addEventListener("keydown", this.onKeydown, false);
        window.addEventListener("keyup", this.onKeyup, false);
        window.oncontextmenu = () => {
            if (this.rightClick && !this.cellSelection.from) {
                // return false;
            }
            // this.rightClick = !this.rightClick;
            // return this.rightClick;
        };

        if (this.touch) {
            this.setupTouch();
            this.containerEl.addEventListener("touchstart", this.onMouseDown, false);
            this.containerEl.addEventListener("touchend", this.onMouseUp, false);
        } else {
            if (!fluid) {
                this.createScrollbars();
            }
            this.containerEl.addEventListener("wheel", this.onWheelEvent, false);
            this.containerEl.addEventListener("mouseover", this.onMouseOver, false);
            window.addEventListener("mousemove", this.onMouseMove, false);
            window.addEventListener("mouseup", this.onMouseUp, false);
            window.addEventListener("mousedown", this.onMouseDown, false);
            // this.containerEl.addEventListener("mouseout", this.onMouseUp, false);
        }

        // this.drawGrid();
        // this.drawGrid();
    }

    get fit(): string {
        return this._fit;
    }

    set fit(value: string) {
        if (["scroll", "width", "height", "contain"].indexOf(value) === -1) {
            return;
        }
        this._fit = value;
    }

    get editing(): boolean {
        return this._editing;
    }

    set editing(value: boolean) {
        // console.log("editing", value);
        this._editing = value;

        this.emit(this.ON_EDITING, { cell: this.cellSelection.from, editing: this._editing, dirty: this.dirty });

        if (value) {
            // check if allow
            if (!this.cellSelection.from || this.cellSelection.from.allow !== true || !this.canEdit) {
                this._editing = false;
                return;
            }

            this.showInput();
        } else {
            this.keyValue = undefined;
            if (this.selection.contains(this.input)) {
                this.input.value = "";
                this.input.blur();
                this.input.style["visibility"] = "hidden";
            }
            if (!this.touch && this.selection.contains(this.input)) {
                this.selection.removeChild(this.input);
                this.selection.contentEditable = true;
            }
            // document.activeElement.blur();
            if (this.dirty) {
                this.dirty = false;
            }
        }
    }

    get tracking(): boolean {
        return this._tracking;
    }

    set tracking(value: boolean) {
        this._tracking = value;
        if (!value) {
            this.history = [];
        }
        this.drawGrid();
        // this.clearCellClassNames(["changed"]);
    }

    public destroy() {
        if (!this.containerEl) {
            return;
        }
        try {
            window.removeEventListener("resize", this.onResizeEvent);
            window.removeEventListener("keydown", this.onKeydown, false);
            window.removeEventListener("keyup", this.onKeyup, false);
            document.removeEventListener("visibilitychange", this.onVisibilityEvent, false);
            if (!this.touch) {
                this.containerEl.removeEventListener("wheel", this.onWheelEvent);
                this.containerEl.removeEventListener("mouseover", this.onMouseOver, false);
                window.removeEventListener("mousemove", this.onMouseMove, false);
                window.removeEventListener("mouseup", this.onMouseUp, false);
                window.removeEventListener("mousedown", this.onMouseDown, false);
                // this.containerEl.removeEventListener("mouseout", this.onMouseUp, false);
            } else {
                this.containerEl.removeEventListener("touchstart", this.onMouseDown, false);
                this.containerEl.removeEventListener("touchend", this.onMouseUp, false);
            }
            this.containerEl.innerHTML = "";
            if (this.hammer) {
                this.hammer.destroy();
            }
            this.removeEvent();
        } catch (e) {
            console.log(e);
        }
    }

    public cellHistory(data) {
        this.history = [];
        if (!data.length) {
            return;
        }
        let users: any = [];
        let count: number = 0;
        data.forEach2((push: any) => {
            count++;
            if (count === 1) {
                return;
            }
            if (users.indexOf(push.modified_by.id) === -1) {
                users.push(push.modified_by.id);
            }
            let userIndex: number = users.indexOf(push.modified_by.id);
            push.content_diff.forEach2((row: any, rowIndex: number) => {
                if (!row) {
                    return;
                }
                if (!this.history[rowIndex]) {
                    this.history[rowIndex] = [];
                }
                row.forEach2((cellData: any, cellIndex: number) => {
                    if (!cellData) {
                        return;
                    }
                    this.history[rowIndex][cellIndex] = userIndex;
                });
            });
        });
    }

    public setTrackingHighlights(data) {
        this.clearTrackingHighlights();
        if (!data || !data.content_diff || !data.content_diff.length) {
            return;
        }
        for (let rowIndex = 0; rowIndex < data.content_diff.length; rowIndex++) {
            let row = data.content_diff[rowIndex];
            if (!row) continue;
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                if (!row[colIndex]) continue;
                if (!this.trackingHighlights[rowIndex]) {
                    this.trackingHighlights[rowIndex] = [];
                }
                // if (!this.trackingHighlights[rowIndex][colIndex]) {
                //     this.trackingHighlights[rowIndex][colIndex] = [];
                // }
                this.trackingHighlights[rowIndex][colIndex] = true;
            }
        }
    }

    public clearTrackingHighlights() {
        let highlights: any = this.containerEl.getElementsByClassName("tracking");
        while (highlights[0]) {
            highlights[0].parentNode.removeChild(highlights[0]);
        }
        this.trackingHighlights = [];
    }

    public setContent(content) {
        // this.dirty = false;
        this.originalContent = content;
        this.updateContent();
    }

    public getContentHtml() {
        let html = `<table style="border-collapse: collapse;">`;
        for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            let row = this.content[rowIndex];
            if (
                this.cellSelection.from &&
                (rowIndex < this.cellSelection.from.index.row || rowIndex > this.cellSelection.to.index.row)
            ) {
                continue;
            }
            html += `<tr>`;
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                let col = row[colIndex];
                if (
                    this.cellSelection.to &&
                    (colIndex < this.cellSelection.from.index.col || colIndex > this.cellSelection.to.index.col)
                ) {
                    continue;
                }
                let cell: any = this.originalContent[rowIndex][colIndex];
                let styles = this.flattenStyles(cell.style);
                html += `<td style="${styles}">${col.allow === "no" ? "" : cell.formatted_value}</td>`;
            }
            html += `</tr>`;
        }
        html += `</table>`;
        return html;
    }

    public getCells(fromCell: any, toCell: any, refCell: any): any {
        let rowEnd = toCell.row;
        if (rowEnd === -1) {
            rowEnd = this.originalContent.length - 1;
        }
        let colEnd = toCell.col;
        if (colEnd === -1) {
            colEnd = this.originalContent[0].length - 1;
        }
        let cellUpdates = [];
        for (let row = fromCell.row; row <= rowEnd; row++) {
            for (let col = fromCell.col; col <= colEnd; col++) {
                if (!cellUpdates[row]) cellUpdates[row] = [];
                cellUpdates[row][col] = this.content[row][col];
            }
        }
        return cellUpdates;
    }

    public renderGrid(clear: boolean = false) {
        if (this.hidden) {
            return;
        }

        let diff =
            !this.width ||
            clear ||
            (!this.content.length && !this.content.length) ||
            this.content.length !== this.content.length ||
            this.content[0].length !== this.content[0].length;

        this.buildCells();

        this.updateCellAccess();
        if (diff) {
            this.setSize();
        }
        this.setOffsets(undefined);
        if (diff) {
            // this.setSize();
            this.setFit(this.fit);
        }
        this.updateScrollbars();
        // this.hideSelector();

        if (!this.init) {
            this.init = true;
            requestAnimationFrame(this.drawGrid);
        } else {
            this.drawGrid();
        }
    }

    public drawGrid = () => {
        let stageWidth = this.width * 1 / this.scale * this.ratio;
        let stageHeight = this.height * 1 / this.scale * this.ratio;

        this.gc.bufferContext.save();
        this.gc.bufferContext.clearRect(0, 0, this.width * this.ratio, this.height * this.ratio);
        this.gc.bufferContext.scale(this.scale, this.scale);

        this.gridLines = [];
        this.visible = [];
        let frozenCells = {
            cols: [],
            rows: [],
            common: []
        };
        let trackedCells = [];

        for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            let row = this.content[rowIndex];

            let y = row[0].y - this.offsets.y.offset;
            let b = row[0].y - this.offsets.y.offset + row[0].height;
            let visibleRow = (y >= 0 && y <= stageHeight) || (b >= 0 && b <= stageHeight);
            if (visibleRow || (this.freezeRange.valid && rowIndex < this.freezeRange.index.row)) {
                this.visible[rowIndex] = [];
            } else {
                continue;
            }

            // soft merges
            if (!row[0].overlayWidth) {
                this.softMerges(row, rowIndex);
            }

            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                let col = row[colIndex];
                // let cell = this.originalContent[rowIndex][colIndex];

                let width = col.overlayWidth;
                let height = col.height;
                let xPos = col.overlayStart < colIndex ? this.content[rowIndex][col.overlayStart].x : col.x;
                // top left coords
                let x = xPos - this.offsets.x.offset;
                let _x = col.x - this.offsets.x.offset;
                let y = col.y - this.offsets.y.offset;
                // bottom right coords
                let a = col.x - this.offsets.x.offset + width;
                let _a = col.x - this.offsets.x.offset + col.width;
                let b = col.y - this.offsets.y.offset + height;
                let visibleCol =
                    (x >= 0 && x <= stageWidth) ||
                    (a >= 0 && a <= stageWidth) ||
                    (col.overlayEnd && colIndex <= col.overlayEnd) ||
                    (col.overlayStart && colIndex >= col.overlayStart);

                if (!col.width) {
                    continue;
                }

                if (
                    (visibleRow && visibleCol) ||
                    (this.freezeRange.valid && colIndex < this.freezeRange.index.col) ||
                    (this.freezeRange.valid && rowIndex < this.freezeRange.index.row)
                ) {
                    let originalCell = this.originalContent[col.position.row][col.position.col];
                    this.visible[rowIndex][colIndex] = {
                        originalCell: originalCell,
                        coords: { x: x, y: y, a: a, b: b, width: width, height: height, _x: _x, _a: _a },
                        cell: col
                    };

                    if (this.freezeRange.valid) {
                        let freeze = "";
                        if (rowIndex < this.freezeRange.index.row && colIndex < this.freezeRange.index.col) {
                            x = _x = col.x;
                            a = _a = col.x + width;
                            y = col.y;
                            b = col.y + height;
                            freeze = "common";
                        } else if (rowIndex < this.freezeRange.index.row) {
                            y = col.y;
                            b = col.y + height;
                            freeze = visibleCol ? "rows" : "";
                        } else if (colIndex < this.freezeRange.index.col) {
                            x = _x = col.x;
                            a = _a = col.x + width;
                            freeze = visibleRow ? "cols" : "";
                        }
                        if (freeze) {
                            frozenCells[freeze].push({
                                originalCell: originalCell,
                                coords: { x: x, y: y, a: a, b: b, width: width, height: height, _x: _x, _a: _a },
                                col: col
                            });
                            continue;
                        }
                    }

                    // this.drawOffsetRect({ x: x, y: y, a: a, b: b, width: width, height: height }, col);
                    // this.drawGraphics({ x: x, y: y, a: a, b: b, width: width, height: height }, col);

                    if (
                        this.tracking &&
                        this.history[col.position.row] &&
                        this.history[col.position.row][col.position.col] >= 0 &&
                        !col.button
                    ) {
                        trackedCells.push({
                            originalCell: originalCell,
                            coords: { x: x, y: y, a: a, b: b, width: width, height: height },
                            col: col
                        });
                    }
                }
            }
        }

        // render graphics
        for (let rowIndex = 0; rowIndex < this.visible.length; rowIndex++) {
            if (!this.visible[rowIndex]) continue;
            for (let colIndex = 0; colIndex < this.visible[rowIndex].length; colIndex++) {
                if (!this.visible[rowIndex][colIndex]) continue;
                let col = this.visible[rowIndex][colIndex];
                if (col.cell.allow !== "no" || !this.imgPattern) {
                    this.drawOffsetRect(col.originalCell, col.coords, col.cell);
                }
            }
        }
        for (let rowIndex = 0; rowIndex < this.visible.length; rowIndex++) {
            if (!this.visible[rowIndex]) continue;
            for (let colIndex = 0; colIndex < this.visible[rowIndex].length; colIndex++) {
                if (!this.visible[rowIndex][colIndex]) continue;
                let col = this.visible[rowIndex][colIndex];
                this.drawGraphics(col.originalCell, col.coords, col.cell);
            }
        }

        // render gridlines
        this.gridLines.forEach2(line => {
            this.drawLine(line[0], line[1], line[2]);
        });
        // render tracked cell
        trackedCells.forEach2(cell => {
            this.drawTrackingRect(cell.coords, cell.col);
        });
        // render frozen cells
        for (let freeze in frozenCells) {
            this.gridLines = [];
            frozenCells[freeze].forEach2(cell => {
                this.drawOffsetRect(cell.originalCell, cell.coords, cell.col);
            });
            frozenCells[freeze].forEach2(cell => {
                this.drawGraphics(cell.originalCell, cell.coords, cell.col);
            });
            // render gridlines
            this.gridLines.forEach2(line => {
                this.drawLine(line[0], line[1], line[2]);
            });
        }

        // render sorting and filters indicators
        for (let i in this.sorting) {
            let col = this.sorting[i];
            let cell = this.content[0][col.col];
            this.drawSortingIndicator(cell, col.direction);
        }

        this.gc.bufferContext.restore();
        this.gc.canvasContext.clearRect(0, 0, this.width * this.ratio, this.height * this.ratio);
        this.gc.canvasContext.drawImage(this.gc.buffer, 0, 0);
        this.updateSelector(true);
    };

    public setScale(n) {
        this.scale = n * this.ratio;
        this.setOffsets(undefined);
        // this.setSize();
        this.updateScrollbars();
        // this.updateSelector();
        requestAnimationFrame(this.drawGrid);
        // if (this.touch) {
        // } else {
        // this.drawGrid();
        // }
    }

    public setFit(n, retain) {
        if (!/(scroll|width|height|contain)/.test(n)) {
            return;
        }

        console.log("setFit", n);
        this.updateFit(n, !this.touch);
        this.setSize();
        if (!this.touch) {
            // update scrollbars
            this.updateFit(n, true);
        }
    }

    public clearRanges(): void {
        this.accessRanges = [];
        this.buttonRanges = [];
        this.styleRanges = [];
        // this.clearCellClassNames(Object.values(this.accessRangesClassNames));
        this.freezeRange = {
            valid: false,
            index: {
                row: 0,
                col: 0
            }
        };
    }

    public setRanges(ranges: any, userId: number = undefined): void {
        this.clearRanges();
        for (let i: number = 0; i < ranges.length; i++) {
            let range: any = ranges[i];

            let rowEnd: number;
            let colEnd: number;
            if (range.range) {
                rowEnd = _.clone(range.range.to.row);
                colEnd = _.clone(range.range.to.col);
                if (rowEnd === -1) {
                    rowEnd = this.originalContent.length - 1;
                }
                if (colEnd === -1) {
                    colEnd = this.originalContent[0].length - 1;
                }
            }

            if (range.name === "frozen_rows") {
                this.freezeRange.index.row = range.count;
            } else if (range.name === "frozen_cols") {
                this.freezeRange.index.col = range.count;
            } else if (range instanceof StyleRange) {
                for (let i: number = range.range.from.row; i <= rowEnd; i++) {
                    for (let k: number = range.range.from.col; k <= colEnd; k++) {
                        if (!this.styleRanges[i]) {
                            this.styleRanges[i] = [];
                        }
                        if (!this.styleRanges[i][k]) {
                            this.styleRanges[i][k] = [];
                        }
                        this.styleRanges[i][k].push(range);
                    }
                }
            } else if (range instanceof ButtonRange) {
                for (let i: number = range.range.from.row; i <= rowEnd; i++) {
                    for (let k: number = range.range.from.col; k <= colEnd; k++) {
                        if (!this.buttonRanges[i]) {
                            this.buttonRanges[i] = [];
                        }
                        if (!this.buttonRanges[i][k]) {
                            this.buttonRanges[i][k] = [];
                        }
                        this.buttonRanges[i][k].push(range);
                    }
                }
            } else if (range instanceof PermissionRange && this.originalContent.length) {
                let userRight: any = range.getPermission(userId || 0) || (userId ? "yes" : "no");
                let rowEnd: number = _.clone(range.rowEnd);
                let colEnd: number = _.clone(range.colEnd);
                if (rowEnd === -1) {
                    rowEnd = this.originalContent.length - 1;
                }
                if (colEnd === -1) {
                    colEnd = this.originalContent[0].length - 1;
                }
                for (let i: number = range.rowStart; i <= rowEnd; i++) {
                    for (let k: number = range.colStart; k <= colEnd; k++) {
                        if (!this.accessRanges[i]) {
                            this.accessRanges[i] = [];
                        }
                        if (!this.accessRanges[i][k]) {
                            this.accessRanges[i][k] = [];
                        }
                        if (userRight) {
                            this.accessRanges[i][k].push(userRight);
                        }
                    }
                }
            }
        }

        this.updateCellAccess();

        if (this.freezeRange.index.row > 0 || this.freezeRange.index.col > 0) {
            this.freezeRange.valid = true;
        }

        // this.drawGrid();
        // console.log('setRanges', this.accessRanges, this.freezeRange);
    }

    public clearFilters() {
        this.filters = {};
        this.updateContent();
    }

    public setFilters(filters) {
        if (filters.col >= 0) {
            filters = [filters];
        }
        filters.forEach2(filter => {
            if (!filter.value || filter.remove) {
                if (this.filters[`col${filter.col}`]) {
                    delete this.filters[`col${filter.col}`];
                }
                return;
            }
            if (!filter.type) filter.type = "number";
            filter.type = /(number|string|date)/.test(filter.type) ? filter.type : "number";
            if (
                !filter.exp ||
                (filter.type === "number" && [">", "<", "<=", ">=", "==", "==="].indexOf(filter.exp) === -1)
            ) {
                filter.exp = "==";
            }
            this.filters[`col${filter.col}`] = filter;
        });
        this.updateContent();
    }

    public clearSorting() {
        this.sorting = {};
        this.updateContent();
    }

    public setSorting(filters) {
        this.clearSorting();
        if (filters.col >= 0) {
            filters = [filters];
        }
        filters.forEach2(filter => {
            if (filter.remove) {
                if (this.filters[`col${filter.col}`]) {
                    delete this.filters[`col${filter.col}`];
                }
                return;
            }
            if (!filter.direction) filter.direction = "ASC";
            filter.direction = `${filter.direction}`.toUpperCase();
            filter.direction = /(ASC|DESC)/.test(filter.direction) ? filter.direction : "ASC";
            filter.col = filter.col * 1 || 0;
            filter.type = /(number|string|date)/.test(filter.type) ? filter.type : "number";
            this.sorting[`col${filter.col}`] = filter;
        });
        this.updateContent();
    }

    public hideSelector(focus: boolean = false) {
        // this.hasFocus = focus;
        this.emitCellChange(this.cellSelection.from);
        this.cellSelection.last = this.cellSelection.from;
        this.cellSelection.on = false;
        this.cellSelection.from = undefined;
        this.cellSelection.to = undefined;
        this.selection.style["display"] = "none";
        this.emit(this.ON_CELL_SELECTED, this.cellSelection);
        this.hideHistoryIndicator();
    }

    public find(value: any): void {
        let highlights: any = this.containerEl.getElementsByClassName("highlight");
        while (highlights[0]) {
            highlights[0].parentNode.removeChild(highlights[0]);
        }
        this.found = [];
        this.foundSelection.count = 0;
        this.foundSelection.selected = 0;
        if (!value) {
            return;
        }
        for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            let row = this.content[rowIndex];
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                let col = row[colIndex];
                let regex = new RegExp(value, "ig");
                let cell: any = this.originalContent[rowIndex][colIndex];
                if (col.allow === "no" || (!regex.test(cell.value) && !regex.test(cell.formatted_value))) {
                    continue;
                }
                if (!this.found[rowIndex]) {
                    this.found[rowIndex] = [];
                }
                this.foundSelection.count++;
                this.found[rowIndex][colIndex] = {
                    cell: col,
                    highlight: this.createHighlight(cell),
                    originalCell: cell
                };
            }
        }
        this.updateSelector();
    }

    public gotoFoundCell(which: any): void {
        if (typeof which === "number") {
            if (which < 0 || which > this.foundSelection.count) {
                return;
            }
            this.foundSelection.selected = which;
        } else if (["prev", "next"].indexOf(which) > -1) {
            if (which === "next") {
                this.foundSelection.selected++;
            } else {
                this.foundSelection.selected--;
            }
            if (this.foundSelection.selected > this.foundSelection.count) {
                this.foundSelection.selected = 1;
            } else if (this.foundSelection.selected < 1) {
                this.foundSelection.selected = this.foundSelection.count;
            }
        } else {
            return;
        }
        let count = 0;
        this.found.forEach2((row, rowIndex) => {
            if (!row) return;
            row.forEach2((col, colIndex) => {
                if (!col) return;
                count++;
                if (count === this.foundSelection.selected) {
                    this.cellSelection.from = col.cell;
                    this.cellSelection.to = col.cell;
                    this.cellSelection.on = true;
                    // this.updateSelector();
                    // this.keepSelectorWithinView();

                    this.goto(col.cell.index.col, col.cell.index.row, true);
                }
            });
        });
    }

    public setSelector(from: any, to?: any): void {
        if (!to) {
            to = from;
        }
        if (!this.content[from.row] || !this.content[from.row][from.col]) {
            return;
        }
        this.updateSelectorAttributes(this.content[from.row][from.col], this.content[to.row][to.col], this.selection);
        this.cellSelection.from = this.content[from.row][from.col];
        this.cellSelection.to = this.content[to.row][to.col];
        this.emit(this.ON_CELL_SELECTED, this.cellSelection);
    }

    private updateContent() {
        this.content = this.setCellIndexes(this.originalContent);
        this.applyFilters();
        this.applySorting();
        if (this.cellSelection.from && !this.content[this.cellSelection.from.position.row]) {
            this.hideSelector();
        }
    }

    private showInput(): void {
        // this.selection.appendChild(this.input);
        let cell: any = this.originalContent[this.cellSelection.from.position.row][
            this.cellSelection.from.position.col
        ];

        let unit: string = cell.style["font-size"].indexOf("px") > -1 ? "px" : "pt";
        this.input.style["font-size"] = `${parseFloat(cell.style["font-size"]) * this.scale / this.ratio}${unit}`;
        this.input.style["font-family"] = `${cell.style["font-family"]}, sans-serif`;
        this.input.style["font-weight"] = cell.style["font-weight"];
        this.input.style["text-align"] = cell.style["text-align"];
        this.input.style["visibility"] = "visible";

        if (!this.touch) {
            this.selection.appendChild(this.input);
            this.selection.contentEditable = false;
            let interval = setInterval(() => {
                if (!this.selection.contains(this.input)) return;
                clearInterval(interval);
                if (this.keyValue === undefined) {
                    this.input.value = `${cell.formatted_value}`;
                }
                this.input.focus();
                if (this.keyValue !== undefined) {
                    this.input.selectionStart = 999;
                } else if (this.input.value) {
                    this.input.selectionStart = 0;
                    this.input.selectionEnd = 999;
                }
            }, 20);
        } else {
            if (this.keyValue === undefined) {
                this.input.value = `${cell.formatted_value}`;
            }
            this.input.focus();
            if (this.keyValue !== undefined) {
                this.input.selectionStart = 999;
            } else if (this.input.value) {
                this.input.selectionStart = 0;
                this.input.selectionEnd = 999;
            }
        }
    }

    private setCellIndexes(content: any[]): any {
        let clone: any = [];
        this.links = [];
        let links: any = this.containerEl.getElementsByClassName("external-link");
        while (links[0]) {
            links[0].parentNode.removeChild(links[0]);
        }
        for (let rowIndex = 0; rowIndex < content.length; rowIndex++) {
            let row = content[rowIndex];
            clone[rowIndex] = [];
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                let cell = content[rowIndex][colIndex];
                let col = {
                    value: _.clone(cell.value),
                    formatted_value: _.clone(cell.formatted_value),
                    width: parseFloat(cell.style.width),
                    position: {
                        row: rowIndex,
                        col: colIndex
                    }
                };
                clone[rowIndex].push(col);
                if (cell.link && cell.link.external) {
                    if (!this.links[rowIndex]) this.links[rowIndex] = [];
                    this.links[rowIndex][colIndex] = {
                        cell: col,
                        highlight: this.createHighlight(col, {
                            className: "external-link",
                            href: cell.link.address,
                            target: "_blank"
                        })
                    };
                }
            }
        }
        return clone;
    }

    private updateFound(cell): void {
        if (!this.found[cell.index.row] || !this.found[cell.index.row][cell.index.col]) {
            return;
        }
        let count = 0;
        this.found.forEach2((row, rowIndex) => {
            if (!row) return;
            row.forEach2((col, colIndex) => {
                if (!col) return;
                count++;
                if (col.cell === cell) {
                    this.foundSelection.selected = count;
                }
            });
        });
    }

    private createHighlight(cell, attrs: any = {}, styles: any = {}): HTMLDivElement {
        let highlight = document.createElement(attrs.href ? "a" : "div");
        let inner = document.createElement("div");
        for (let attr in attrs) {
            highlight[attr] = attrs[attr];
        }
        if (!highlight.className) highlight.className = "highlight";
        // highlight.contentEditable = true;
        highlight.style.position = "absolute";
        highlight.style["z-index"] = 10;
        highlight.style["left"] = "0";
        highlight.style["top"] = "0";
        highlight.style["width"] = `${cell.width}px`;
        highlight.style["height"] = `${cell.height}px`;
        highlight.style["display"] = "none";
        for (let style in styles) {
            inner.style[style] = styles[style];
        }
        highlight.appendChild(inner);
        this.containerEl.appendChild(highlight);
        return highlight;
    }

    private applyFilters() {
        // let originalData = JSON.parse(JSON.stringify(content));
        if (!Object.keys(this.filters).length) {
            return false;
        }
        // let data = [];

        let rowsToRemove: number[] = [];

        for (let rowIndex = 0; rowIndex < this.originalContent.length; rowIndex++) {
            if (this.freezeRange.valid && rowIndex < this.freezeRange.index.row) {
                // rowsToKeep.push(rowIndex);
                continue;
            }

            for (let f in this.filters) {
                let filter = this.filters[f];
                let col = this.originalContent[rowIndex][filter.col];
                if (!col || rowsToRemove.indexOf(rowIndex) > -1) continue;
                let colValue: any = filter.type === "number" ? parseFloat(col.value) : `${col.formatted_value}`;
                let filterValue: any = filter.type === "number" ? parseFloat(filter.value) : `${filter.value}`;

                let validValue = false;
                switch (filter.type) {
                    case "number":
                        validValue = eval(`${colValue} ${filter.exp} ${filterValue}`);
                        break;
                    case "date":
                        let dates = filterValue.split(",");
                        let fromDate = new Date(dates[0]).toISOString();
                        let toDate = new Date(dates[1]).toISOString();
                        let date = new Date(this.helpers.parseDateExcel(colValue)).toISOString();
                        validValue = date >= fromDate && date <= toDate;
                        break;
                    default:
                        filterValue.split(",").forEach2(value => {
                            if (validValue) {
                                return;
                            }
                            switch (filter.exp) {
                                case "contains":
                                    validValue = colValue.toLowerCase().indexOf(value.toLowerCase()) > -1;
                                    break;
                                case "starts_with":
                                    validValue =
                                        colValue.toLowerCase().substring(0, value.length) === value.toLowerCase();
                                    break;
                                default:
                                    validValue = colValue == value; // eval(`"${colValue}" == "${value}"`);
                                    break;
                            }
                        });
                        break;
                }
                if (!validValue) rowsToRemove.push(rowIndex);
            }
        }
        for (let i = rowsToRemove.length - 1; i >= 0; i--) this.content.splice(rowsToRemove[i], 1);
        // return data;
    }

    private applySorting() {
        if (!Object.keys(this.sorting).length) {
            return false;
        }
        let key = Object.keys(this.sorting)[0];
        let filter = this.sorting[key];
        let data = this.freezeRange.index.row
            ? this.content.slice(this.freezeRange.index.row, this.content.length)
            : this.content.slice(0);
        let exp = filter.direction === "DESC" ? ">" : "<";
        let opp = filter.direction === "DESC" ? "<" : ">";
        data.sort((a, b) => {
            let aVal = filter.type === "number" ? parseFloat(a[filter.col].value) || 0 : `"${a[filter.col].value}"`;
            let bVal = filter.type === "number" ? parseFloat(b[filter.col].value) || 0 : `"${b[filter.col].value}"`;

            if (eval(`${aVal} ${exp} ${bVal}`)) {
                return -1;
            }
            if (eval(`${aVal} ${opp} ${bVal}`)) {
                return 1;
            }
            return 0;
        });

        this.content = this.freezeRange.index.row
            ? this.content.slice(0, this.freezeRange.index.row).concat(data)
            : data;
    }

    private buildCells() {
        // let data = [];

        this.resetStage();

        this.stage.x.increments.push(0);
        this.stage.y.increments.push(0);

        let freezeOffset = {
            x: 0,
            y: 0
        };

        for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            let row = this.content[rowIndex];
            this.stage.x.pos = 0;
            let col;
            let originalCell;
            // data[rowIndex] = [];

            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                col = row[colIndex];
                originalCell = this.originalContent[col.position.row][col.position.col];

                let wrap = false;
                if (
                    (originalCell.style["text-wrap"] && originalCell.style["text-wrap"] === "wrap") ||
                    originalCell.style["word-wrap"] === "break-word"
                ) {
                    wrap = true;
                }

                col.wrap = wrap;
                col.x = this.stage.x.pos;
                col.y = this.stage.y.pos;
                col.width = parseFloat(originalCell.style.width);
                col.height = parseFloat(originalCell.style.height);

                // cell = this.cellData(col, { x: this.stage.x.pos, y: this.stage.y.pos });

                // cell position
                col.index = {
                    row: rowIndex,
                    col: colIndex
                };

                // check access
                col.allow = true;

                // store link
                col.link = originalCell.link;

                // add cell
                // data[rowIndex][colIndex] = cell;

                // keep track of stage width
                this.stage.x.pos += col.width;
                if (colIndex === row.length - 1 && this.borders.widths[originalCell.style.rbw]) {
                    this.stage.x.pos += this.borders.widths[originalCell.style.rbw];
                }

                if (!rowIndex) {
                    if (!this.freezeRange.valid || (this.freezeRange.valid && colIndex >= this.freezeRange.index.col)) {
                        this.stage.x.increments.push(this.stage.x.pos - freezeOffset.x);
                    } else {
                        freezeOffset.x += col.width;
                    }
                }

                // create history highlights
                if (this.trackingHighlights[rowIndex] && this.trackingHighlights[rowIndex][colIndex]) {
                    let styles = {};
                    if (this.trackingHighlightMatchColor)
                        styles["background-color"] = `#${originalCell.style.color.replace("#", "")}`;
                    this.trackingHighlights[rowIndex][colIndex] = new GridTrackingHighlight(
                        col,
                        this.createHighlight(
                            col,
                            {
                                className: `tracking ${this.trackingHighlightClassname}`
                            },
                            styles
                        )
                    );
                    this.trackingHighlights[rowIndex][colIndex].lifetime = this.trackingHighlightLifetime;
                    // this.trackingHighlights[rowIndex][colIndex].forEach2((e, i) => {
                    // if (typeof e !== "number") return;
                    // });
                    // this.updateSelectorAttributes(cell, cell, this.historyHighlights[rowIndex][colIndex]);
                }
            }
            if (this.stage.x.pos > this.stage.x.size) {
                this.stage.x.size = this.stage.x.pos;
            }
            this.stage.y.pos += col.height;
            this.stage.y.size += col.height;
            if (rowIndex === this.content.length - 1 && this.borders.widths[originalCell.style.bbw]) {
                this.stage.y.pos += this.borders.widths[originalCell.style.bbw];
                this.stage.y.size += this.borders.widths[originalCell.style.bbw];
            }
            if (!this.freezeRange.valid || (this.freezeRange.valid && rowIndex >= this.freezeRange.index.row)) {
                this.stage.y.increments.push(this.stage.y.pos - freezeOffset.y);
            } else {
                freezeOffset.y += col.height;
            }
        }

        // return data;
    }

    private updateCellAccess() {
        if (!this.content.length) {
            return;
        }
        // console.log("updateCellAccess");

        for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            let row = this.content[rowIndex];
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                let col = row[colIndex];
                col.allow = true;
                col.button = false;
                col.formatting = false;
            }
        }
        this.accessRanges.forEach2((row, rowIndex) => {
            if (!row) return;
            row.forEach2((col, colIndex) => {
                if (!col || col.indexOf("yes") !== -1) return;
                col.forEach2(element => {
                    if (["ro", "no"].indexOf(element) === -1) {
                        return;
                    }
                    if (this.content[rowIndex] && this.content[rowIndex][colIndex]) {
                        this.content[rowIndex][colIndex].allow = element;
                    }
                });
            });
        });
        this.buttonRanges.forEach2((row, rowIndex) => {
            if (!row) return;
            row.forEach2((col, colIndex) => {
                if (!col) return;
                if (this.content[rowIndex] && this.content[rowIndex][colIndex]) {
                    this.content[rowIndex][colIndex].button = col;
                }
            });
        });
        let rowEnd: number = this.originalContent.length - 1;
        let colEnd: number = this.originalContent[0].length - 1;
        this.styleRanges.forEach2((row, rowIndex) => {
            if (!row) return;

            row.forEach2((col, colIndex) => {
                if (!col) return;

                if (this.content[rowIndex] && this.content[rowIndex][colIndex]) {
                    col.forEach2(styleRange => {
                        if (!styleRange.match(this.content[rowIndex][colIndex].value)) {
                            return;
                        }

                        styleRange.formatting.forEach2(format => {
                            let cells: any = styleRange.parseFormat(format, rowIndex, colIndex, rowEnd, colEnd);

                            for (let i: number = cells.from.row; i <= cells.to.row; i++) {
                                for (let k: number = cells.from.col; k <= cells.to.col; k++) {
                                    if (this.content[i] && this.content[i][k]) {
                                        if (!this.content[i][k].formatting) {
                                            this.content[i][k].formatting = cells.style;
                                        } else {
                                            this.content[i][k].formatting = merge(
                                                this.content[i][k].formatting,
                                                cells.style
                                            );
                                        }
                                    }
                                }
                            }
                        });
                    });
                }
            });
        });
    }

    private updateFit(n, retain) {
        this.fit = n;

        switch (n) {
            case "scroll":
                this.setScale(retain ? this.ratio : this.ratioScale / this.ratio);
                break;
            case "width":
                if (!this.stage.x.size) {
                    return;
                }
                this.setScale(this.width / this.stage.x.size);
                break;
            case "height":
                if (!this.stage.y.size) {
                    return;
                }
                this.setScale(this.height / this.stage.y.size);
                break;
            case "contain":
                if (!this.stage.x.size || !this.stage.y.size) {
                    return;
                }
                let w = this.width / this.stage.x.size;
                let h = this.height / this.stage.y.size;
                this.setScale(h < w ? h : w);
                break;
        }
    }

    private setupTouch() {
        this.hammer = new Hammer(this.containerEl);
        let pan: any = new Hammer.Pan();
        // let tap: any = new Hammer.Tap({ event: "singletap" });
        // let doubleTap: any = new Hammer.Tap({ event: "doubletap", taps: 2 });
        // doubleTap.recognizeWith(tap);
        let pinch: any = new Hammer.Pinch();
        this.hammer.add([pan, pinch]);

        let currentScale = this.scale / this.ratio * 1;
        this.hammer.on("pinchstart", evt => {
            this.fit = "scroll";
            currentScale = this.scale / this.ratio * 1;
        });
        this.hammer.on("pinchmove", evt => {
            this.setScale(currentScale * evt.scale);
            // this.updateSelector();
        });
        this.hammer.on("pinchend", evt => {
            currentScale = this.scale / this.ratio * 1;
        });

        let currentOffsets = {
            x: 0,
            y: 0
        };

        this.hammer.on("panstart", evt => {
            currentOffsets = {
                x: evt.center.x,
                y: evt.center.y
            };
        });
        this.hammer.on("panmove", evt => {
            // console.log(evt, evt.deltaY, currentOffsets.y);
            let moveBy = Math.round(this.ratio / this.scale);
            if (moveBy < 1) moveBy = 1;

            let x = 0;
            if (Math.abs(evt.center.x - currentOffsets.x) > 5 * this.scale) {
                let directionX = evt.velocityX > 0 ? -1 : 1;
                currentOffsets.x = evt.center.x;
                x = moveBy * directionX;
            }
            let y = 0;
            if (Math.abs(evt.center.y - currentOffsets.y) > 5 * this.scale) {
                let directionY = evt.velocityY > 0 ? -1 : 1;
                currentOffsets.y = evt.center.y;
                y = moveBy * directionY;
            }

            this.setOffsets({ x: x, y: y });
            // console.log(this.offsets.y.index);
            // this.updateSelector();
            this.drawGrid();
        });
        this.hammer.on("panend", evt => {});
    }

    private onWheelEvent: any = evt => {
        if (this.animation) {
            cancelAnimationFrame(this.animation);
        }
        let offsetX = evt.deltaX ? (Math.abs(evt.deltaX) >= 100 ? Math.round(Math.abs(evt.deltaX) / 100) : 1) : 0;
        let offsetY = evt.deltaY ? (Math.abs(evt.deltaY) >= 100 ? Math.round(Math.abs(evt.deltaY) / 100) : 1) : 0;
        let directionX = evt.deltaX > 0 ? 1 : -1;
        let directionY = evt.deltaY > 0 ? 1 : -1;
        offsetX *= directionX;
        offsetY *= directionY;
        // let scrollY = "y";
        if (
            this.stage.y.size * this.scale / this.ratio <= this.height &&
            this.stage.x.size * this.scale / this.ratio > this.width
        ) {
            offsetX = offsetY;
        }
        let curretOffset = JSON.parse(JSON.stringify(this.offsets));
        this.setOffsets({
            x: offsetX,
            y: offsetY
        });
        this.updateScrollbars();
        // this.updateSelector();
        this.animation = this.drawGrid();
        // console.log(offset, curretOffset);
        if (curretOffset.x.offset !== this.offsets.x.offset || curretOffset.y.offset !== this.offsets.y.offset) {
            evt.preventDefault();
        }
    };

    private onResizeEvent: any = evt => {
        console.log("onResizeEvent");

        this.ratio = window.devicePixelRatio;
        this.setSize();
        // this.setCanvasSize();
        if (!this.touch) {
            this.setScale(this.ratio);
        }
        this.setFit(this.fit, true);

        // scroll to input box
        if (this.touch && this.editing) {
            setTimeout(() => {
                // let xIndex: number = this.cellSelection.from.position.col; // - this.offsets.x.index;
                this.keepSelectorWithinView();
            }, 300);
        } else if (this.touch && this.cellSelection.from) {
            this.setOffsets({
                y: this.cellSelection.from.position.row
            });
        }
    };

    private onMouseMove: any = evt => {
        ["x", "y"].forEach2(axis => {
            if (!this.scrollbars[axis].drag || !this.scrollbars.scrolling) {
                return;
            }

            // check if handle has reached limits
            let dimension = this.stage[axis].label;
            let canvasSize = this[dimension];
            let size = canvasSize * (canvasSize / (this.stage[axis].size * this.scale));
            let distance = canvasSize - size;
            let offset = evt[axis] - this.scrollbars[axis].start + this.scrollbars[axis].offset;
            if (offset > distance) {
                offset = distance;
            } else if (offset < 0) {
                offset = 0;
            }

            // new index based on how far scroll has moved
            let anchor = this.scrollbars[axis].anchor;
            this.scrollbars[axis].handle.style[anchor] = `${offset}px`;
            let ratio = offset / distance;
            let index = Math.round(this.offsets[axis].max * ratio);

            // assign new offset
            this.offsets[axis].index = index;
            let scrollOffset = this.stage[axis].increments[index];
            if (scrollOffset > this.offsets[axis].maxOffset) {
                scrollOffset = this.offsets[axis].maxOffset;
            }
            this.offsets[axis].offset = scrollOffset;
        });
        // this.setOffsets(newOffets);
        // this.drawGrid();
        if (this.scrollbars.scrolling) {
            // this.updateSelector();
            this.drawGrid();
        } else {
            this.classy.removeClass(this.containerEl, "link");
            this.classy.removeClass(this.containerEl, "denied");
            let cell = this.getCell(evt.clientX, evt.clientY);
            if (cell) {
                this.cellHasHistory(cell, { x: evt.clientX, y: evt.clientY });

                if (cell.allow !== true) {
                    this.classy.addClass(this.containerEl, "denied");
                } else if (cell.filter || cell.link || cell.button) {
                    this.classy.addClass(this.containerEl, "link");
                }

                // cell history indicator
                if (cell.history.valid && !cell.button) {
                    if (!this.containerEl.contains(this.historyIndicator)) {
                        this.containerEl.appendChild(this.historyIndicator);
                    }
                    this.updateSelectorAttributes(cell, cell, this.historyIndicator, true);
                } else if (this.containerEl.contains(this.historyIndicator)) {
                    this.containerEl.removeChild(this.historyIndicator);
                }

                // when mouse middle button is clicked
                if (this.freeScroll) {
                    let x: number = Math.round((evt.clientX - this.freeScroll.startX) / 20);
                    let y: number = Math.round((evt.clientY - this.freeScroll.startY) / 20);
                    this.freeScroll.x = x;
                    this.freeScroll.y = y;
                }

                if (this.cellSelection.go && !this.alwaysEditing) {
                    this.cellSelection.to = cell;
                    this.updateSelector();
                }
            }
        }
    };

    private onMouseUp: any = evt => {
        console.log("onMouseUp");
        this.scrollbars.scrolling = false;
        this.cellSelection.go = false;
        ["x", "y"].forEach2(axis => {
            this.scrollbars[axis].drag = false;
        });
        this.emit(this.ON_CELL_SELECTED, this.cellSelection);

        // detect right click
        this.rightClick = this.isRightClick(evt);
        // if (this.rightClick && this.isCanvas(evt.target)) {
        //     this.rightClick = true;
        //     this.emit(this.ON_CELL_CLICKED_RIGHT, this.cellSelection.from);
        // }
        // middle click
        if (evt.which === 2) {
            // start quick scroll
            if (this.freeScroll) {
                this.classy.removeClass(this.containerEl, "move");
                cancelAnimationFrame(this.freeScroll.animation);
                this.freeScroll = undefined;
            } else {
                this.freeScroll = {
                    x: 0,
                    y: 0,
                    startX: evt.clientX,
                    startY: evt.clientY,
                    row: this.cellSelection.from.position.row * 1,
                    col: this.cellSelection.from.position.col * 1,
                    animation: requestAnimationFrame(this.startFreeScroll)
                };
                this.classy.addClass(this.containerEl, "move");
            }
            // this.hideSelector();
        }
    };

    private isRightClick(evt: any): boolean {
        let isRightMB;
        if ("which" in evt)
            // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
            isRightMB = evt.which === 3;
        else if ("button" in evt)
            // IE, Opera
            isRightMB = evt.button === 2;
        return isRightMB;
    }

    private onVisibilityEvent: any = (evt: any) => {
        console.log("hidden", document.hidden);
        this.hidden = document.hidden;
        if (!this.hidden) {
            // requestAnimationFrame(this.drawGrid);
            this.renderGrid();
        }
    };

    private onMouseDown: any = evt => {
        console.log(evt);
        this.rightClick = this.isRightClick(evt);
        let scrollHandle = false;
        ["x", "y"].forEach2(axis => {
            if (this.scrollbars[axis].handle === evt.target) scrollHandle = true;
        });
        let hit: boolean =
            this.isCanvas(evt.target) ||
            this.isInput(evt.target) ||
            evt.target === this.selection ||
            evt.target === this.historyIndicator;
        if (!scrollHandle && hit) {
            if (evt.touches) {
                if (evt.touches.length > 1) {
                    return true;
                }
                evt.clientX = evt.touches[0].pageX;
                evt.clientY = evt.touches[0].pageY;
            }

            console.log(evt.clientX, evt.clientY, evt.clientX, evt.clientY);

            let cell = this.getCell(evt.clientX, evt.clientY);

            if (cell) {
                // right click
                if (this.rightClick) {
                    return;
                }

                // double click check
                let d = new Date();
                if (this.cellSelection.clicked) {
                    if (d.getTime() - this.cellSelection.clicked <= 300 && !this.cellSelection.from.button) {
                        this.emit(this.ON_CELL_DOUBLE_CLICKED, cell);
                        this.cellSelection.clicked = d.getTime();
                        this.editing = true;
                        if (this.touch) {
                            evt.stopPropagation();
                            evt.preventDefault();
                        }
                        return;
                    }
                }

                // single click
                this.editing = false;
                this.setCellSelection(cell, true, d.getTime(), { x: evt.clientX, y: evt.clientY });
                if (this.touch) {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
                return;
            } else {
                this.editing = false;
                this.hideSelector(true);
            }
        } else if (!hit && evt.target.nodeName === "INPUT") {
            this.hasFocus = false;
        } else if (!scrollHandle && !hit) {
            this.hasFocus = false;
        }
    };

    private onMouseOver: any = evt => {
        this.hasFocus = true;
    };

    private setCellSelection(cell: any, go: boolean = false, clickTime?: number, coords: any = { x: 0, y: 0 }): void {
        if (this.editing) {
            // this.emit(this.ON_EDITING, { cell: this.cellSelection.from, dirty: this.dirty });
        }
        this.cellHasHistory(cell, coords);
        this.cellSelection.clicked = clickTime;
        this.cellSelection.from = cell;
        this.cellSelection.to = cell;
        this.cellSelection.go = false;
        this.cellSelection.on = false;
        if (!this.disallowSelection) {
            if (!cell.link) {
                this.cellSelection.go = go;
                this.cellSelection.on = true;
                this.updateSelector();
            }
        }
        this.updateFound(cell);
        this.emit(this.ON_CELL_SELECTED, this.cellSelection);
        if (clickTime) {
            this.emit(this.ON_CELL_CLICKED, { cell: cell, history: cell.history.show });
        }
        this.hasFocus = true;
        this.keyValue = undefined;
        if (!this.touch) this.selection.innerText = "";
        if (this.editing) {
            this.showInput();
        } else if (this.alwaysEditing) {
            this.editing = true;
        }
    }

    private onKeydown: any = evt => {
        // esc
        if (evt.keyCode === 27) {
            // this.restoreCell(this.cellSelection.from);
            if (this.editing) {
                this.editing = false;
            } else {
                this.hideSelector(true);
            }
            this.emit(this.ON_CELL_RESET, { cell: this.cellSelection.from });
            this.drawGrid();
            return;
        }

        console.log("onKeydown", evt);

        if (!this.hasFocus) {
            return;
        }

        // ctrl is down
        if (evt.keyCode === 17 || evt.keyCode === 91) {
            this.ctrlDown = true;
            return;
        }

        let x = 0;
        let y = 0;

        switch (evt.keyCode) {
            // left
            case 37:
                x = -1;
                break;
            // up
            case 38:
                y = -1;
                break;
            // right
            case 39:
                x = 1;
                break;
            // down
            case 40:
                y = 1;
                break;
        }
        if ((x !== 0 || y !== 0) && !this.editing) {
            let cell = this.cellSelection.from;
            let row;
            let col;
            if (this.cellSelection.from && !this.disallowSelection) {
                row = cell.index.row + y;
                col = cell.index.col + x;
                if (row >= this.content.length) {
                    row = 0;
                } else if (row < 0) {
                    row = this.content.length - 1;
                }
                if (col >= this.content[0].length) {
                    col = 0;
                } else if (col < 0) {
                    col = this.content[0].length - 1;
                }
                this.setCellSelection(this.content[row][col]);
                this.keepSelectorWithinView();
                evt.preventDefault();
            } else {
                this.setOffsets({
                    x: x,
                    y: y
                });
                this.updateScrollbars();
                this.drawGrid();
                if ((y > 0 && this.offsets.y.index !== this.offsets.y.max) || (y < 0 && this.offsets.y.index)) {
                    evt.preventDefault();
                }
            }
            return;
        }

        // enter
        if (evt.keyCode === 13) {
            if (!this.alwaysEditing && !this.touch) {
                this.editing = false;
            }
            this.nextCellInput(
                this.cellSelection.from || this.cellSelection.last || this.content[0][0],
                this.shiftDown ? "up" : "down"
            );
            this.keepSelectorWithinView();
            return;
            // }
        }

        // tab
        if (evt.keyCode === 9 && this.cellSelection.from) {
            if (!this.alwaysEditing && !this.touch) {
                this.editing = false;
            }
            this.nextCellInput(this.cellSelection.from, this.shiftDown ? "left" : "");
            evt.preventDefault();
            this.keepSelectorWithinView();
            return;
        }

        // shift
        if (evt.key === "Shift") {
            this.shiftDown = true;
        }

        if (!this.editing && !this.disallowSelection && this.canEdit) {
            // enter
            if (evt.key === "F2") {
                if (!evt.shiftKey) {
                    this.setEditCell(this.cellSelection.from);
                    return;
                }
            }

            if (this.cellSelection.from && !this.ctrlDown) {
                if (evt.key.length === 1 && /[a-zA-Z0-9-_ ]/.test(evt.key)) {
                    this.keyValue = evt.key;
                    this.input.value = evt.key;
                    // this.input.value = evt.key;
                }
                if (evt.key === "Delete") {
                    this.keyValue = "";
                    this.input.value = "";
                }
                if (this.keyValue !== undefined) {
                    this.cellSelection.on = true;
                    this.setEditCell(this.cellSelection.from);
                    this.ctrlDown = false;
                }
            }
        } else if (this.editing) {
            this.dirty = true;
        }
    };

    private onKeyup: any = evt => {
        if (evt.keyCode === 17 || evt.keyCode === 91) {
            this.ctrlDown = false;
        }
        if (evt.key === "Shift") {
            this.shiftDown = false;
        }
    };

    private startFreeScroll = () => {
        if (!this.freeScroll) {
            return;
        }
        this.setOffsets({ x: this.freeScroll.x, y: this.freeScroll.y });
        this.updateScrollbars();
        // this.updateSelector();
        this.drawGrid();
        requestAnimationFrame(this.startFreeScroll);
    };

    private keepSelectorWithinView(): void {
        if (this.touch) {
            let yIndex: number = this.cellSelection.from.index.row; // - this.offsets.y.index;
            if (this.freezeRange.valid) {
                // xIndex += this.freezeRange.index.col;
                yIndex += this.freezeRange.index.row;
            }
            if (!this.visible[yIndex]) {
                this.setOffsets({
                    y: yIndex
                });
                requestAnimationFrame(this.drawGrid);
            }
            return;
        }

        let row = this.cellSelection.from.index.row;
        let col = this.cellSelection.from.index.col;
        let offsetX = 0,
            offsetY = 0;
        let rowStartIndex = -1,
            rowEndIndex = -1,
            colStartIndex = -1,
            colEndIndex = -1;
        let colRow;

        this.visible.forEach2((cellRow, cellRowIndex) => {
            if (!cellRow) return;
            if (rowStartIndex < 0 && (!this.freezeRange.valid || cellRowIndex > this.freezeRange.index.row)) {
                rowStartIndex = cellRowIndex + 0;
            }
            rowEndIndex = cellRowIndex;
            if (!colRow && (!this.freezeRange.valid || cellRowIndex > this.freezeRange.index.row)) {
                colRow = cellRow;
            }
        });
        colRow.forEach2((cellCol, cellColIndex) => {
            if (!cellCol) return;
            if (colStartIndex < 0 && (!this.freezeRange.valid || cellColIndex > this.freezeRange.index.col)) {
                colStartIndex = cellColIndex + 0;
            }
            colEndIndex = cellColIndex + 0;
        });

        if (!row) {
            this.offsets.y.index = 0;
        } else if (row === this.content.length - 1) {
            this.offsets.y.index = this.offsets.y.max;
        } else if (row < rowStartIndex + 2 + this.freezeRange.index.row) {
            offsetY = -1;
        } else if (row > rowEndIndex - 2) {
            offsetY = 1;
        }
        if (!col) {
            this.offsets.x.index = 0;
        } else if (col === this.content[0].length - 1) {
            this.offsets.x.index = this.offsets.x.max;
            // } else if (col < colStartIndex + this.freezeRange.index.col) {
            //     // offsetX = -1;
            //     this.offsets.x.index = col - this.freezeRange.index.col;
            // } else if (col > colEndIndex - 1) {
            //     // offsetX = 1;
            //     this.offsets.x.index = col - this.freezeRange.index.col;
        } else {
            // this.offsets.x.index = col; // + this.freezeRange.index.col;
            offsetX = col - this.offsets.x.index - this.freezeRange.index.col;
        }
        console.log(`offset: ${offsetX}, col: ${col}, start: ${colStartIndex}, end: ${colEndIndex}`);
        // this.setOffsets({ x: offsetX, y: offsetY });
        // this.updateScrollbars();
        // this.drawGrid();
        // this.updateSelector();
        this.goto(offsetX, offsetY);
    }

    private goto(x: number, y: number, set: boolean = false): void {
        this.setOffsets({ x: x, y: y }, set);
        this.updateScrollbars();
        this.drawGrid();
        // ¯\_(ツ)_/¯
        setTimeout(() => {
            // this.updateSelector();
        }, 50);
    }

    private restoreCell(cell): void {
        if (!cell) {
            return;
        }
        cell.dirty = false;
        cell.formatted_value = this.content[cell.position.row][cell.position.col].formatted_value;
        cell.value = this.content[cell.position.row][cell.position.col].value;
    }

    private nextCellInput(cell: any, direction: string = ""): void {
        // check if next cell
        let row: number = cell.index.row;
        let col: number = cell.index.col;
        if (direction === "left") {
            if (this.content[row][col - 1]) {
                this.setCellSelection(this.content[row][col - 1]);
            } else if (this.content[row - 1]) {
                this.setCellSelection(this.content[row - 1][this.content[row].length - 1]);
            }
            return;
        }
        if (direction === "up") {
            // show next cell in row
            if (this.content[row - 1] && this.content[row - 1][col]) {
                // this.setEditCell(this.content[row + 1][col]);
                this.setCellSelection(this.content[row - 1][col]);
                return;
            } else if (this.content[this.content.length - 1] && this.content[this.content.length - 1][col - 1]) {
                this.setCellSelection(this.content[this.content.length - 1][col - 1]);
                return;
            }
        }
        if (direction === "down") {
            // show next cell in row
            if (this.content[row + 1] && this.content[row + 1][col]) {
                // this.setEditCell(this.content[row + 1][col]);
                this.setCellSelection(this.content[row + 1][col]);
                return;
            } else if (this.content[0][col + 1]) {
                this.setCellSelection(this.content[0][col + 1]);
                return;
            }
        }
        // show next cell
        if (this.content[row][col + 1]) {
            this.setCellSelection(this.content[row][col + 1]);
            return;
        }
        // show next cell in row
        if (this.content[row + 1]) {
            this.setCellSelection(this.content[row + 1][0]);
            return;
        }
        // show start cell
        this.setCellSelection(this.content[0][0]);
        return;
    }

    private setEditCell(cell): any {
        if (cell.allow !== true) {
            this.editing = false;
            return false;
        } else {
            this.emitCellChange(this.cellSelection.from);
        }
        if (this.keyValue !== undefined) {
            cell.formatted_value = cell.value = _.clone(this.keyValue);
            cell.dirty = true;
        }
        this.cellSelection.from = cell;
        this.cellSelection.to = cell;
        this.keepSelectorWithinView();
        // this.updateSelector();
        this.editing = true;
        if (this.keyValue !== undefined) {
            // this.dirty = true;
            // this.emit(this.ON_CELL_VALUE, { cell: this.cellSelection.from, value: _.clone(this.keyValue) });
        }
        return true;
    }

    private emitCellChange(cell): any {
        if (!cell || !cell.dirty) {
            return;
        }
        // this.emit(this.ON_EDITING, { cell: cell, value: _.clone(this.keyValue) });
        this.keyValue = undefined;
        this.input.value = "";
        this.softMerges(this.content[this.cellSelection.from.index.row], this.cellSelection.from.index.row);
        this.drawGrid();
        cell.dirty = false;
    }

    private isCanvas(target) {
        let clickedEl: any = target;
        while (clickedEl && clickedEl !== this.gc.canvas) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === this.gc.canvas;
    }

    private isInput(target) {
        let clickedEl: any = target;
        while (clickedEl && clickedEl !== this.input) {
            clickedEl = clickedEl.parentNode;
        }
        return clickedEl === this.input;
    }

    private getCell(x, y): any {
        let rect = this.containerEl.getBoundingClientRect();
        x -= rect.left - this.offsets.x.offset * this.scale / this.ratio;
        y -= rect.top - this.offsets.y.offset * this.scale / this.ratio;
        let cell;

        for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
            let row = this.content[rowIndex];
            for (let colIndex = 0; colIndex < row.length; colIndex++) {
                let col = row[colIndex];
                if (x < col.x * this.scale / this.ratio) continue;
                if (x > (col.x + col.width) * this.scale / this.ratio) continue;
                if (y < col.y * this.scale / this.ratio) continue;
                if (y > (col.y + col.height) * this.scale / this.ratio) continue;
                cell = col;
                break;
            }
            if (cell) break;
        }

        return cell;
    }

    private cellHasHistory(cell, coords: any = { x: 0, y: 0 }): void {
        let rect = this.containerEl.getBoundingClientRect();
        coords.x -= rect.left - this.offsets.x.offset * this.scale / this.ratio;
        coords.y -= rect.top - this.offsets.y.offset * this.scale / this.ratio;
        let history: boolean =
            this.history[cell.position.row] && this.history[cell.position.row][cell.position.col] >= 0;
        cell.history = {
            valid: history,
            show:
                history &&
                coords.x > (cell.x + cell.overlayWidth - 16) * this.scale / this.ratio &&
                coords.y < (cell.y + 16) * this.scale / this.ratio
                    ? true
                    : false,
            color: history ? this.userTrackingColors[this.history[cell.position.row][cell.position.col]] : ""
        };
    }

    private createScrollbars() {
        ["x", "y"].forEach2(axis => {
            let anchor = this.scrollbars[axis].anchor;

            this.scrollbars[axis].track = document.createElement("div");
            this.scrollbars[axis].handle = document.createElement("div");

            // setup track
            this.scrollbars[axis].track.style["position"] = "absolute";
            this.scrollbars[axis].track.style["z-index"] = 100;
            this.scrollbars[axis].track.style["display"] = "none";
            this.scrollbars[axis].track.className = "track";

            // setup handle
            this.scrollbars[axis].handle.style["position"] = "absolute";
            this.scrollbars[axis].handle.style["z-index"] = 1;
            this.scrollbars[axis].handle.style["left"] = 0;
            this.scrollbars[axis].handle.style["top"] = 0;
            this.scrollbars[axis].handle.className = "handle";
            this.scrollbars[axis].handle.addEventListener(
                "mousedown",
                evt => {
                    console.log("mousedown");
                    this.scrollbars.scrolling = true;
                    this.scrollbars[axis].start = evt[axis];
                    this.scrollbars[axis].drag = true;
                    this.scrollbars[axis].offset = parseInt(this.scrollbars[axis].handle.style[anchor], 10);
                    // evt.stopPropagation();
                    evt.preventDefault();
                },
                false
            );
            // this.scrollbars[axis].handle.addEventListener("mouseenter", (evt) => {
            //     this.scrollbars[axis].pressed = true;
            //     evt.stopPropagation();
            // }, false);

            if (axis === "x") {
                this.scrollbars[axis].track.style["left"] = 0;
                this.scrollbars[axis].track.style["bottom"] = 0;
                this.scrollbars[axis].track.style["right"] = `${this.scrollbars.width}px`;
                this.scrollbars[axis].track.style["height"] = `${this.scrollbars.width}px`;
                this.scrollbars[axis].handle.style["height"] = `${this.scrollbars.width}px`;
            } else {
                this.scrollbars[axis].track.style["top"] = 0;
                this.scrollbars[axis].track.style["right"] = 0;
                this.scrollbars[axis].track.style["bottom"] = `${this.scrollbars.width}px`;
                this.scrollbars[axis].track.style["width"] = `${this.scrollbars.width}px`;
                this.scrollbars[axis].handle.style["width"] = `${this.scrollbars.width}px`;
            }

            this.scrollbars[axis].track.appendChild(this.scrollbars[axis].handle);
            this.containerEl.appendChild(this.scrollbars[axis].track);
        });
    }

    private updateScrollbars() {
        if (this.touch || this.fluid) {
            return;
        }
        let show = false;
        ["x", "y"].forEach2(axis => {
            if (!this.offsets[axis].maxOffset) {
                this.scrollbars[axis].track.style.display = "none";
                this.scrollbars[axis].handle.style.display = "none";
                this.scrollbars[axis].show = false;
                this.offsets[axis].index = 0;
                this.offsets[axis].offset = 0;
                return;
            }

            let dimension = this.stage[axis].label;
            let canvasSize = this[dimension];

            this.scrollbars[axis].show = true;
            this.scrollbars[axis].handle.style.display = "block";

            let size = canvasSize * (canvasSize / (this.stage[axis].size * this.scale));
            this.scrollbars[axis].handle.style[dimension] = `${size}px`;

            let offset = this.offsets[axis].index / this.offsets[axis].max * (canvasSize - size);
            let anchor = this.scrollbars[axis].anchor;
            this.scrollbars[axis].handle.style[anchor] = `${offset}px`;

            // show = true;
            this.scrollbars[axis].track.style.display = "block";
            this.scrollbars[axis].track.style[dimension] = `${this[dimension]}px`;
        });
        // if (show) {
        //     this.scrollbars.y.track.style.display = "block";
        // }
    }

    private createSelector() {
        this.selection = document.createElement("div");
        this.selection.className = "selection";
        if (!this.touch) {
            this.selection.contentEditable = true;
        }
        this.selection.style.position = "absolute";
        this.selection.style["z-index"] = 20;
        this.selection.style["left"] = 0;
        this.selection.style["top"] = 0;
        this.selection.style["display"] = "none";
        this.selection.addEventListener("paste", (evt: any) => {
            // evt.stopPropagation();
            evt.preventDefault();
        });

        this.historyIndicator = document.createElement("div");
        this.historyIndicator.className = "history";
        // this.selection.contentEditable = true;
        this.historyIndicator.style.position = "absolute";
        this.historyIndicator.style["z-index"] = 30;
        // this.historyIndicator.style["right"] = 0;
        // this.historyIndicator.style["top"] = 0;
        this.historyIndicator.style["display"] = "none";

        this.input = document.createElement("input");
        // this.input.setAttribute("contenteditable", "true");
        this.input.setAttribute("autocapitalize", "none");
        this.input.style["position"] = "absolute";
        this.input.style["border"] = "0";
        this.input.style["font-size"] = "12px";
        this.input.style["color"] = "black";
        this.input.style["background-color"] = "ivory";
        this.input.style["box-sizing"] = "border-box";
        this.input.style["margin"] = "0";
        this.input.style["padding"] = "0 2px";
        this.input.style["outline"] = "0";
        this.input.style["left"] = "0px";
        this.input.style["right"] = "0px";
        this.input.style["bottom"] = "0px";
        this.input.style["top"] = "0px";
        this.input.style["width"] = "100%";
        this.input.style["text-align"] = "left";
        this.input.style["z-index"] = 10;
        this.input.style["visibility"] = "hidden";
        this.input.style["display"] = "display";
        // this.input.style["white-space"] = "nowrap";
        // this.input.style["align-items"] = "center";
        // this.input.style["overflow"] = "hidden";
        this.input.style["height"] = "100%";
        // this.input.addEventListener("keydown", () => {
        //     // this.cellSelection.from.dirty = true;
        // });
        this.input.addEventListener("keyup", (evt: any) => {
            // this.cellSelection.from.formatted_value = this.cellSelection.from.value = _.clone(this.input.value);
            // this.emitCellChange(this.cellSelection.from, true);
            // this.keyValue = this.input.value;
            this.dirty = true;
            this.emit(this.ON_CELL_VALUE, { cell: this.cellSelection.from, value: `${this.input.value}` });
            // if (this.touch && evt.keyCode === 13) {
            //     this.editing = false;
            //     this.nextCellInput(this.cellSelection.from, "down");
            // }
        });
        this.input.addEventListener("touchstart", evt => {
            // console.log("touch");
            // evt.preventDefault();
        });
        // this.input.addEventListener("blur", (evt) => {
        //     console.log("blur", evt);
        // });
        this.input.addEventListener("paste", (evt: any) => {
            evt.stopPropagation();
            let text: string = "";
            try {
                // Other
                text = evt.clipboardData.getData("text/plain");
            } catch (exception) {}
            try {
                // IE
                if (!text) {
                    text = window.clipboardData.getData("Text");
                }
            } catch (exception) {}
            this.dirty = true;
            this.emit(this.ON_CELL_VALUE, { cell: this.cellSelection.from, value: `${text}` });
            // evt.preventDefault();
        });

        if (this.touch) {
            this.selection.appendChild(this.input);
        }
        this.containerEl.appendChild(this.selection);
    }

    private updateSelectorAttributes(cell, cellTo, selector, history: boolean = false) {
        let x = cell.x;
        let y = cell.y;
        let width = cellTo.x + cellTo.width - cell.x;
        let height = cellTo.y + cellTo.height - cell.y;

        if (cellTo.x < x) {
            x = cellTo.x;
            width = cell.x + cell.width - cellTo.x;
        }
        if (cellTo.y < y) {
            y = cellTo.y;
            height = cell.y + cell.height - cellTo.y;
        }

        if (!this.freezeRange.valid || cell.position.col >= this.freezeRange.index.col) {
            x -= this.offsets.x.offset;
        }
        x = x * this.scale / this.ratio;
        if (!this.freezeRange.valid || cell.position.row >= this.freezeRange.index.row) {
            y -= this.offsets.y.offset;
        }
        y = y * this.scale / this.ratio;

        width = (width + 1) * this.scale / this.ratio;
        height = (height + 1) * this.scale / this.ratio;

        if (history) {
            selector.style["left"] = `${x + width - this.historyIndicatorSize}px`;
            selector.style["background-color"] = cell.history.color;
        } else {
            selector.style["left"] = `${x}px`;
            selector.style["width"] = `${width}px`;
            selector.style["height"] = `${height}px`;
        }
        selector.style["top"] = `${y}px`;
        let show: boolean = true;
        // if (y < 0 || y + height > this.height) show = false;
        // if (x < 0 || x + width > this.width) show = false;
        if (this.freezeRange.valid) {
            if (
                cell.position.row > this.freezeRange.index.row - 1 &&
                cell.position.row - this.freezeRange.index.row < this.offsets.y.index
            ) {
                show = false;
            }
            if (
                cell.position.col > this.freezeRange.index.col - 1 &&
                cell.position.col - this.freezeRange.index.col < this.offsets.x.index
            ) {
                show = false;
            }
        }
        selector.style["display"] = show ? "block" : "none";
    }

    private hideHistoryIndicator() {
        if (this.containerEl.contains(this.historyIndicator)) {
            this.containerEl.removeChild(this.historyIndicator);
        }
    }

    private updateSelector(history?: boolean) {
        if (!history) {
            this.hideHistoryIndicator();
        }

        // selected cells
        if (this.cellSelection.on) {
            this.updateSelectorAttributes(this.cellSelection.from, this.cellSelection.to, this.selection);
        }

        // found cells
        this.found.forEach2((row, rowIndex) => {
            if (!row) return;
            row.forEach2((col, colIndex) => {
                if (!col) return;
                if (this.visible[rowIndex] && this.visible[rowIndex][colIndex]) {
                    if (!this.containerEl.contains(col.highlight)) {
                        this.containerEl.appendChild(col.highlight);
                    }
                    this.updateSelectorAttributes(col.cell, col.cell, col.highlight);
                } else {
                    if (this.containerEl.contains(col.highlight)) {
                        this.containerEl.removeChild(col.highlight);
                    }
                }
            });
        });

        // external links
        this.links.forEach2((row, rowIndex) => {
            if (!row) return;
            row.forEach2((col, colIndex) => {
                if (!col) return;
                if (this.visible[rowIndex] && this.visible[rowIndex][colIndex]) {
                    if (!this.containerEl.contains(col.highlight)) {
                        this.containerEl.appendChild(col.highlight);
                    }
                    this.updateSelectorAttributes(col.cell, col.cell, col.highlight);
                } else {
                    if (this.containerEl.contains(col.highlight)) {
                        this.containerEl.removeChild(col.highlight);
                    }
                }
            });
        });

        // tracking highlights
        this.trackingHighlights.forEach2((row, rowIndex) => {
            if (!row) return;
            row.forEach2((highlight: GridTrackingHighlight, colIndex) => {
                if (!highlight || highlight === true) return;

                // col.forEach((highlight: GridTrackingHighlight, highlightIndex: number) => {

                if (this.visible[rowIndex] && this.visible[rowIndex][colIndex]) {
                    this.updateSelectorAttributes(highlight.cell, highlight.cell, highlight.highlight);
                    if (!this.containerEl.contains(highlight.highlight)) {
                        this.containerEl.appendChild(highlight.highlight);
                    }
                    if (!highlight.run) {
                        highlight.flash();
                    } else {
                        highlight.destroy();
                    }
                } else {
                    // if (this.containerEl.contains(highlight.highlight) || highlight.done) {
                    //     this.containerEl.removeChild(highlight.highlight);
                    // }
                    // if (highlight.done) {
                    //     this.trackingHighlights[rowIndex][colIndex].splice(highlightIndex, 1);
                    // }
                    highlight.destroy();
                }

                // });
            });
        });

        // this.selection.style["display"] = "block";
    }

    private setOffsets(deltas: any, set: boolean = false) {
        // max index
        ["x", "y"].forEach2(axis => {
            let stage = this[this.stage[axis].label] * 1 / this.scale * this.ratio;
            this.offsets[axis].max = 0;
            this.offsets[axis].maxOffset = this.stage[axis].size - stage;
            if (this.offsets[axis].maxOffset < 0) {
                this.offsets[axis].maxOffset = 0;
            }
            this.stage[axis].increments.forEach2((offset, index) => {
                if (offset > this.offsets[axis].maxOffset && !this.offsets[axis].max) {
                    this.offsets[axis].max = index;
                }
            });
        });
        // update offset index
        if (deltas) {
            for (let axis in deltas) {
                if (!deltas[axis]) continue;
                if (set) {
                    let index: number = deltas[axis];
                    let label: string = axis === "x" ? "col" : "row";
                    if (this.freezeRange.valid) {
                        index -= this.freezeRange.index[label] + 2;
                    }
                    this.offsets[axis].index = index;
                } else {
                    this.offsets[axis].index += deltas[axis];
                }
                if (this.offsets[axis].index < 0) {
                    this.offsets[axis].index = 0;
                }
                if (this.offsets[axis].index >= this.offsets[axis].max) {
                    this.offsets[axis].index = this.offsets[axis].max;
                }
            }
        }
        // set offset
        ["x", "y"].forEach2(axis => {
            let offset = this.stage[axis].increments[this.offsets[axis].index];
            if (offset > this.offsets[axis].maxOffset) {
                offset = this.offsets[axis].maxOffset;
            }
            this.offsets[axis].offset = offset;
            this.allowBrowserScroll = this.offsets[axis].index === this.offsets[axis].max;
        });

        // if (this.touch) {
        //     if (this.allowBrowserScroll) {
        //         this.containerEl.style["touch-action"] = "auto";
        //     } else {
        //         this.containerEl.style["touch-action"] = "none";
        //     }
        // }
    }

    private resetStage() {
        this.stage = {
            x: {
                label: "width",
                pos: 0,
                increments: [],
                size: 0,
                stop: false
            },
            y: {
                label: "height",
                pos: 0,
                increments: [],
                size: 0,
                stop: false
            }
        };
    }

    private setSize() {
        let rect = (this.containerRect = this.containerEl.getBoundingClientRect());
        if (!rect.width) {
            rect.width = this.stage.x.size;
            rect.height = this.stage.y.size;
        }
        let scrollbarWidth =
            (this.offsets.x.maxOffset || this.offsets.y.maxOffset) && !this.touch ? this.scrollbars.width : 0;
        let scrollbarHeight =
            (this.offsets.x.maxOffset || this.offsets.y.maxOffset) && !this.touch ? this.scrollbars.width : 0;
        let stageWidth = this.stage.x.size * this.scale / this.ratio;
        let stageHeight = this.stage.y.size * this.scale / this.ratio;
        if (rect.width > stageWidth || this.fluid || this.scrollbars.inset) {
            scrollbarHeight = 0;
        }
        if (rect.height > stageHeight || this.fluid || this.scrollbars.inset) {
            scrollbarWidth = 0;
        }
        this.width = this.fluid ? this.stage.x.size : rect.width - scrollbarWidth;
        this.height = this.fluid ? this.stage.y.size : rect.height - scrollbarHeight;

        this.gc.buffer.width = (this.fluid ? this.stage.x.size : rect.width - scrollbarWidth) * this.ratio;
        this.gc.buffer.height = (this.fluid ? this.stage.y.size : rect.height - scrollbarHeight) * this.ratio;
        this.gc.canvas.width = (this.fluid ? this.stage.x.size : rect.width - scrollbarWidth) * this.ratio;
        this.gc.canvas.height = (this.fluid ? this.stage.y.size : rect.height - scrollbarHeight) * this.ratio;
        this.gc.canvas.style.width = this.fluid
            ? `${this.stage.x.size}px`
            : `${Math.floor(rect.width - scrollbarWidth)}px`;
        this.gc.canvas.style.height = this.fluid
            ? `${this.stage.y.size}px`
            : `${Math.floor(rect.height - scrollbarHeight)}px`;
    }

    private getTextOverlayWidth(row, col, width, textWidth): any {
        let cell = this.originalContent[row][col];
        let textAlign: string = cell.style["text-align"];
        let left: number = 0;
        let right: number = 0;
        switch (textAlign) {
            case "right":
                left = -1;
                break;
            case "center":
                left = -1;
                right = 1;
                break;
            default:
                right = 1;
                break;
        }
        if (!this.content[row][col + right] || !this.content[row][col + left]) {
            return {
                col: col,
                width: width
            };
        }
        let endCol: number = col;
        let startCol: number = col;
        if (right > 0) {
            for (let i = col + 1; i < this.content[row].length; i++) {
                if (
                    this.content[row][i].formatted_value ||
                    `${this.content[row][i].value}` ||
                    this.content[row][i].allow === "no"
                ) {
                    break;
                }
                width += this.content[row][i].width;
                endCol = i;
                this.content[row][i].hidden = true;
                if (width > textWidth) {
                    break;
                }
            }
        }
        if (left < 0) {
            for (let i = col - 1; i >= 0; i--) {
                if (
                    this.content[row][i].formatted_value ||
                    `${this.content[row][i].value}` ||
                    this.content[row][i].allow === "no"
                ) {
                    break;
                }
                width += this.content[row][i].width;
                startCol = i;
                this.content[row][i].hidden = true;
                if (width > textWidth) {
                    break;
                }
            }
        }
        return {
            left: left,
            right: right,
            endCol: endCol,
            startCol: startCol,
            width: width
        };
    }

    private softMerges(dataRow: any = [], dataRowIndex: number = -1) {
        // this.content.forEach2((row, rowIndex) => {
        dataRow.forEach2((col, colIndex) => {
            let cell = this.originalContent[dataRowIndex][colIndex];
            let width = parseFloat(cell.style.width);
            col.overlayWidth = width;
            col.overlayEnd = colIndex;
            col.overlayStart = colIndex;
            if ((cell.formatted_value || cell.value) && !col.wrap) {
                this.gc.bufferContext.font = this.getFontString(cell);
                let textWidth: number =
                    this.gc.bufferContext.measureText(this.cleanValue(cell.formatted_value || cell.value)).width + 3;
                if (textWidth > width) {
                    let overlay: any = this.getTextOverlayWidth(dataRowIndex, colIndex, width, textWidth);
                    col.overlayWidth = overlay.width;
                    col.overlayEnd = overlay.endCol;
                    col.overlayStart = overlay.startCol;
                }
            }
        });
        // });
    }

    private getFontString(col: any): string {
        return `${col.style["font-style"]} ${col.style["font-weight"]} ${col.style["font-size"]} "${
            col.style["font-family"]
        }", sans-serif`;
    }

    private flattenStyles(styles) {
        let htmlStyle: any = {};

        this.cellStyles.forEach(s => {
            htmlStyle[s] = styles[s];
        });

        for (let key in this.borders.names) {
            let name: string = this.borders.names[key];
            let width: string = this.borders.widths[styles[`${key}bw`]] + "px";
            let style: string = styles[`${key}bs`];
            let color: string = "#" + styles[`${key}bc`].replace("#", "");
            htmlStyle[`border-${name}`] = `${width} ${style} ${color}`;
        }

        let str = "";
        for (let attr in htmlStyle) {
            let value = htmlStyle[attr];
            if ((attr === "background-color" || attr === "color") && value.indexOf("#") === -1) {
                value = `#${value}`;
            }
            str += `${attr}: ${value}; `;
        }
        return str;
    }

    private getBackgroundColor(originalCell: any, cell: any) {
        // let cell = this.originalContent[col.position.row][col.position.col];
        let color = "" + originalCell.style["background-color"] || "white";
        let hexColor = `#${color.replace("#", "")}`;
        if (this.helpers.validHex(hexColor)) {
            color = hexColor;
        } else if (color === "none") {
            color = "white";
        }
        // if (cell.allow === "no") {
        //     color = "#CCCCCC";
        // }
        if (cell.formatting && cell.formatting.background) {
            color = cell.formatting.background;
        }
        return color;
    }

    private drawGraphics(cell, coords, col) {
        if (`${col.value}`.indexOf("data:image") === 0) {
            let image = new Image();
            image.onload = () => {
                this.gc.canvasContext.drawImage(
                    image,
                    coords.x,
                    coords.y,
                    coords.width * this.scale,
                    coords.height * this.scale
                );
                // this.gc.canvasContext.drawImage(this.gc.buffer, 0, 0);
            };
            image.src = col.value;
        } else if (!col.hidden) {
            this.drawRect(cell, { x: coords.x, y: coords.y, width: coords.width, height: coords.height }, col);
            this.drawText(cell, { x: coords.x, y: coords.y, width: coords.width, height: coords.height }, col);

            if (col.allow === "no") {
                let color = this.getBackgroundColor(cell, col);
                let contrast = this.helpers.getContrastYIQ(color);
                for (let img in this.noAccessImages) {
                    if (contrast !== img || !this.noAccessImages[img]) {
                        continue;
                    }
                    let pat = this.gc.bufferContext.createPattern(
                        document.getElementById(this.noAccessImages[img]),
                        "repeat"
                    );
                    this.gc.bufferContext.beginPath();
                    this.gc.bufferContext.rect(coords.x, coords.y, coords.width, coords.height);
                    this.gc.bufferContext.fillStyle = pat;
                    this.gc.bufferContext.fill();
                }
            }
        }

        // // top line
        if (cell.style.tbs !== "none") {
            this.gridLines.push([
                { x: coords._x - this.borders.widths[cell.style.tbw] / 2, y: coords.y },
                { x: coords._a, y: coords.y },
                { width: this.borders.widths[cell.style.tbw], color: cell.style.tbc }
            ]);
        }

        // right line
        if (cell.style.rbs !== "none" && coords.a === coords._a && !col.hidden) {
            this.gridLines.push([
                { x: coords._a, y: coords.y },
                { x: coords._a, y: coords.b },
                { width: this.borders.widths[cell.style.rbw], color: cell.style.rbc }
            ]);
        }

        // bottom line
        if (cell.style.bbs !== "none") {
            this.gridLines.push([
                { x: coords._x, y: coords.b },
                { x: coords._a, y: coords.b },
                { width: this.borders.widths[cell.style.bbw], color: cell.style.bbc }
            ]);
        }

        // left line
        if (cell.style.lbs !== "none" && coords.x === coords._x && !col.hidden) {
            this.gridLines.push([
                { x: coords.x, y: coords.y },
                { x: coords.x, y: coords.b },
                { width: this.borders.widths[cell.style.lbw], color: cell.style.lbc }
            ]);
        }
    }

    private drawLine(start, end, options) {
        let t = this.translateCanvas(options.width || 1);
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.lineWidth = options.width || 1;
        this.gc.bufferContext.strokeStyle = `#${(options.color || "000000").replace("#", "")}`;
        this.gc.bufferContext.moveTo(start.x, start.y);
        this.gc.bufferContext.lineTo(end.x, end.y);
        this.gc.bufferContext.stroke();
        this.resetCanvas(t);
    }

    private drawTrackingRect(coords, cell) {
        // this.gc.bufferContext.strokeStyle
        // let t = this.translateCanvas(2);
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.strokeStyle = this.userTrackingColors[this.history[cell.position.row][cell.position.col]];
        this.gc.bufferContext.lineWidth = 2;
        this.gc.bufferContext.rect(coords.x + 2, coords.y + 2, coords.width - 3, coords.height - 3);
        this.gc.bufferContext.stroke();
        // this.resetCanvas(t);
    }

    private drawSortingIndicator(cell, direction) {
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.fillStyle = "red";
        let x = cell.x + cell.width - 10 - this.offsets.x.offset;
        if (direction === "DESC") {
            let y = cell.y;
            this.gc.bufferContext.moveTo(x, y);
            this.gc.bufferContext.lineTo(x + 10, y);
            this.gc.bufferContext.lineTo(x + 5, y + 5);
            this.gc.bufferContext.lineTo(x, y);
        } else {
            let y = cell.y + cell.height - 5;
            this.gc.bufferContext.moveTo(x + 5, y);
            this.gc.bufferContext.lineTo(x + 10, y + 5);
            this.gc.bufferContext.lineTo(x, y + 5);
            this.gc.bufferContext.lineTo(x + 5, y);
        }
        this.gc.bufferContext.fill();
    }

    private drawOffsetRect(originalCell, coords, cell) {
        // let t = this.translateCanvas(1);
        let color = this.getBackgroundColor(originalCell, cell);
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.fillStyle = color;
        this.gc.bufferContext.fillRect(coords.x - 0.5, coords.y - 0.5, coords.width + 0.5, coords.height + 0.5);

        // this.resetCanvas(t);
    }

    private drawRect(originalCell, coords, cell) {
        // let t = this.translateCanvas(1);
        let color = this.getBackgroundColor(originalCell, cell);
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.fillStyle = color;
        this.gc.bufferContext.fillRect(coords.x, coords.y, coords.width, coords.height);
        // this.resetCanvas(t);
    }

    private cleanValue(value) {
        return (value + "").trim().replace(/\s\s+/g, " ");
    }

    private drawText(originalCell, coords, cell) {
        // check access
        if (cell.allow === "no") {
            return;
        }

        let v = this.cleanValue(originalCell.formatted_value || originalCell.value);
        let color = `#${originalCell.style["color"].replace("#", "")}`;
        if (cell.formatting && cell.formatting.color) {
            color = cell.formatting.color;
        }
        this.gc.bufferContext.fillStyle = color;
        this.gc.bufferContext.font = this.getFontString(originalCell); //  `${cell.style["font-style"]} ${cell.style["font-weight"]} ${cell.style["font-size"]} "${cell.style["font-family"].replace(/"/g, "").replace(/'/g, "")}", sans-serif`;
        this.gc.bufferContext.textAlign = originalCell.style["text-align"]
            .replace("justify", "left")
            .replace("start", "left");
        // let m = bufferContext.measureText(v);
        let x = 0;
        let xPad = 3;
        let y = 0;
        let yPad = 5;
        switch (originalCell.style["text-align"]) {
            case "right":
                x = coords.x + coords.width - xPad;
                break;
            case "middle":
            case "center":
                x = coords.x + coords.width / 2;
                break;
            default:
                x = coords.x + xPad;
                break;
        }
        let wrapOffset = 0;
        let fontHeight = this.gc.bufferContext.measureText("M").width;
        let lines: any = [];
        if (cell.wrap) {
            lines = this.findLines(v, fontHeight, coords.width - xPad * 2);
            wrapOffset = lines.offset;
        }
        switch (originalCell.style["vertical-align"]) {
            case "top":
                y = coords.y + fontHeight + yPad;
                break;
            case "center":
            case "middle":
                y = coords.y + coords.height / 2 - wrapOffset / 2 + fontHeight / 2;
                break;
            default:
                y = coords.y + coords.height - yPad - wrapOffset;
                break;
        }
        this.gc.bufferContext.save();
        this.gc.bufferContext.beginPath();
        this.gc.bufferContext.rect(coords.x, coords.y, coords.width, coords.height);
        this.gc.bufferContext.clip();
        if (cell.wrap) {
            lines.lines.forEach2(line => {
                this.gc.bufferContext.fillText(line.value, x, y + line.y);
                if (originalCell.link) {
                    let yLine: number = Math.round(y + 2 + line.y);
                    let tWidth: number = Math.round(this.gc.bufferContext.measureText(line.value).width);
                    let xLine: number = x;
                    switch (originalCell.style["text-align"]) {
                        case "right":
                            xLine = x - tWidth;
                            break;
                        case "middle":
                        case "center":
                            xLine = x - tWidth / 2;
                            break;
                        default:
                            break;
                    }
                    this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
                }
            });
        } else {
            if (
                originalCell.style["number-format"] &&
                originalCell.style["number-format"].indexOf("0.00") > -1 &&
                /(¥|€|£|\$)/.test(v)
            ) {
                let num = v.split(" ");
                this.gc.bufferContext.fillText(num.length > 1 ? num[1] : v, x, y);
                if (num.length > 1) {
                    this.gc.bufferContext.textAlign = "left";
                    this.gc.bufferContext.fillText(num[0], coords.x + xPad, y);
                }
            } else {
                this.gc.bufferContext.fillText(v, x, y);
            }
            if (originalCell.link) {
                let yLine: number = Math.round(y + 2);
                let tWidth: number = Math.round(this.gc.bufferContext.measureText(v).width);
                let xLine: number = x;
                switch (originalCell.style["text-align"]) {
                    case "right":
                        xLine = x - tWidth;
                        break;
                    case "middle":
                    case "center":
                        xLine = x - tWidth / 2;
                        break;
                    default:
                        break;
                }
                this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
            }
        }
        this.gc.bufferContext.restore(); // discard clipping region
    }

    private findLines(str, fontHeight, cellWidth) {
        let words = str.split(" ");
        let lines = [];

        let word = [];
        let lineY = 0;
        let lineOffset = fontHeight + 4; // leading?

        for (let w = 0; w < words.length; w++) {
            let wordWidth = this.gc.bufferContext.measureText(words[w]).width + 2;

            if (wordWidth > cellWidth) {
                let letters = words[w].split("");
                let width = 0;
                wordWidth = 0;
                word = [];
                for (let i = 0; i < letters.length; i++) {
                    width = this.gc.bufferContext.measureText(word.join("")).width + 2;
                    if (width < cellWidth) {
                        word.push(letters[i]);
                        wordWidth = width;
                    } else {
                        lines.push({
                            value: word.join(""),
                            width: wordWidth
                        });
                        word = [letters[i]];
                    }
                }
                if (word.length) {
                    lines.push({
                        value: word.join(""),
                        width: wordWidth
                    });
                }
            } else {
                lines.push({
                    value: words[w],
                    width: wordWidth
                });
            }
        }

        let rows = [];
        let width = 0;
        let lineWords = [];
        for (let i = 0; i < lines.length; i++) {
            width += lines[i].width;
            if (width < cellWidth) {
                lineWords.push(lines[i].value);
            } else {
                rows.push({
                    value: lineWords.join(" "),
                    y: lineY
                });
                lineY += lineOffset;
                lineWords = [lines[i].value];
                width = lines[i].width;
            }
        }
        if (lineWords.length) {
            rows.push({
                value: lineWords.join(" "),
                y: lineY
            });
        }

        return {
            lines: rows,
            offset: fontHeight * (rows.length - 1) + (rows.length - 1) * 4
        };
    }

    private translateCanvas(w) {
        let translate = (w % 2) / 2;
        this.gc.bufferContext.translate(translate, translate);
        return translate;
        // bufferContext.translate(-iTranslate, -iTranslate);
    }

    private resetCanvas(translate) {
        this.gc.bufferContext.translate(-translate, -translate);
    }
}

class GridTrackingHighlight {
    public run: boolean = false;
    public lifetime: number = 3000;
    public done: boolean = false;
    private classy: any;
    private interval: any;
    constructor(public cell: any, public highlight: HTMLDivElement) {
        this.classy = new Classy();
    }
    public flash(): void {
        this.run = true;
        this.interval = setInterval(() => {
            if (!this.highlight.parentNode) {
                return;
            }
            this.classy.addClass(this.highlight, "flash");
            clearInterval(this.interval);
            setTimeout(() => {
                this.done = true;
                this.classy.addClass(this.highlight, "done");
            }, this.lifetime);
        }, 50);
    }
    public destroy(): void {
        if (!this.done || !this.highlight.parentNode) {
            return;
        }
        this.highlight.parentNode.removeChild(this.highlight);
    }
}

// export default Grid;

export class GridWrap {
    public static $inject: string[] = [];
    constructor() {
        return Grid;
    }
}
