import Helpers from '../Helpers';
import { IContentCellMeta, IContentCellMetaCoords, IContentCellPosition } from './Content';
import { IPageContentCell } from '../Page/Content';

import { GridBase } from './Grid';

export interface ICanvas {
  render();
  destroy();
  update();
}

interface ICanvasSelection {
  from: IContentCellPosition;
  to: IContentCellPosition;
}

class Canvas implements ICanvas {
  public rectangles: any = [];
  public offsetRectangles: any = [];
  public texts: any = [];
  public lines: any = [];
  public hidden: any = [];

  public borders = {
    widths: {
      none: 0,
      thin: 1,
      medium: 2,
      thick: 3,
    },
    styles: {
      none: 'solid',
      solid: 'solid',
      double: 'double',
    },
    names: {
      t: 'top',
      r: 'right',
      b: 'bottom',
      l: 'left',
    },
  };

  public helpers: any;

  private selection: ICanvasSelection = {
    from: undefined,
    to: undefined,
  };

  constructor() {
    this.helpers = new Helpers();
  }
  public init() {}
  public destroy() {}
  public update() {
    this.render();
  }

  public render() {
    this.resetElements();

    // reset canvas
    this.clearCanvas();

    if (!GridBase.Content.content.length) {
      // this.drawRect({ x: 0, y: 0, width: GridBase.Container.containerRect.width, height: GridBase.Container.containerRect.height }, '#FFFFFF');
      GridBase.Container.bufferCanvasContext.fillStyle = GridBase.Settings.contrast === 'dark' ? '#FFFFFF' : '#000000';
      GridBase.Container.bufferCanvasContext.font = `14px Arial`;
      GridBase.Container.bufferCanvasContext.textAlign = 'left';
      GridBase.Container.bufferCanvasContext.fillText('No rows match filter', 5, 15);
      this.drawCanvas();
      return;
    }

    // set from to
    if (GridBase.Content.cellSelection.from) {
      this.selection.from = GridBase.Content.cellSelection.flipped ? GridBase.Content.cellSelection.flippedFrom : GridBase.Content.cellSelection.from.index;
      this.selection.to = GridBase.Content.cellSelection.flipped ? GridBase.Content.cellSelection.flippedTo : GridBase.Content.cellSelection.to.index;
    }

    // shift offset if reach max offset
    let offsetCol = GridBase.Content.offsets.colOffset;
    let offsetRow = GridBase.Content.offsets.rowOffset;

    // loop thru content
    for (let rowIndex = GridBase.Content.offsets.row - 1; rowIndex <= GridBase.Content.offsets.rowTo; rowIndex++) {
      if (rowIndex < 0) continue;
      let row = GridBase.Content.content[rowIndex];
      if (!row) {
        continue;
      }
      for (let colIndex = GridBase.Content.offsets.col - 1; colIndex <= GridBase.Content.offsets.colTo; colIndex++) {
        if (colIndex < 0) continue;
        let cell: IPageContentCell = row[colIndex];
        if (!cell) continue;
        let cellMeta: IContentCellMeta = GridBase.Content.contentMeta[rowIndex][colIndex];
        if (cellMeta.hidden === true) {
          // ensure previous visible cell is visible
          for (let i = colIndex; i >= 0; i--) {
            let c: IPageContentCell = row[i];
            let cM: IContentCellMeta = GridBase.Content.contentMeta[rowIndex][i];
            if (cM.hidden || cM.index.col >= GridBase.Content.offsets.col - 1) continue;
            this.pushElements(c, this.setCoords(cM.coords, offsetRow, offsetCol), cM);
          }
          continue;
        }
        // const coords = this.setCoords(cell, rowIndex, colIndex, offsetRow, offsetCol);
        this.pushElements(cell, this.setCoords(cellMeta.coords, offsetRow, offsetCol), cellMeta);
      }
    }

    this.createGraphics();
    this.resetElements();

    // frozen columns
    for (let rowIndex = GridBase.Content.offsets.row; rowIndex < GridBase.Content.content.length; rowIndex++) {
      // if (GridBase.Settings.headings && !rowIndex) {
      //   continue;
      // }
      let row = GridBase.Content.content[rowIndex];
      for (let colIndex = 0; colIndex < GridBase.Content.getFreezeIndex('col'); colIndex++) {
        let cell: IPageContentCell = row[colIndex];
        let cellMeta: IContentCellMeta = GridBase.Content.contentMeta[rowIndex][colIndex];
        this.pushElements(cell, this.setCoords(cellMeta.coords, offsetRow, 0), cellMeta);
      }
    }
    // frozen rows
    for (let rowIndex = 0; rowIndex < GridBase.Content.getFreezeIndex('row'); rowIndex++) {
      let row = GridBase.Content.content[rowIndex];
      if (!row) continue;
      for (let colIndex = GridBase.Content.offsets.col; colIndex < row.length; colIndex++) {
        let cell: IPageContentCell = row[colIndex];
        let cellMeta: IContentCellMeta = GridBase.Content.contentMeta[rowIndex][colIndex];
        this.pushElements(cell, this.setCoords(cellMeta.coords, 0, offsetCol), cellMeta);
      }
    }

    this.createGraphics();
    this.resetElements();

    // frozen common
    for (let rowIndex = 0; rowIndex < GridBase.Content.getFreezeIndex('row'); rowIndex++) {
      let row = GridBase.Content.content[rowIndex];
      if (!row) continue;
      for (let colIndex = 0; colIndex < GridBase.Content.getFreezeIndex('col'); colIndex++) {
        let cell: IPageContentCell = row[colIndex];
        let cellMeta: IContentCellMeta = GridBase.Content.contentMeta[rowIndex][colIndex];
        this.pushElements(cell, cellMeta.coords, cellMeta);
      }
    }

    this.createGraphics();

    // render sorting and filters indicators
    if (Object.keys(GridBase.Content.sorting).length) {
      const colName = Object.keys(GridBase.Content.sorting)[0];
      const col = GridBase.Content.sorting[colName];
      let offset = GridBase.Settings.headings ? 1 : 0;
      let cellMeta: IContentCellMeta = GridBase.Content.getContentCellMeta({ row: 0, col: col.col + offset });
      // let cell = GridBase.GridBase.Content.content[firstCell.row][col.col + offset];
      if (cellMeta) {
        this.drawSortingIndicator(cellMeta, col.direction);
      }
    }

    this.drawCanvas();
  }
  private resetElements() {
    // reset
    this.lines = [];
    this.offsetRectangles = [];
    this.rectangles = [];
    this.texts = [];
    this.hidden = [];
  }
  private clearCanvas() {
    GridBase.Container.bufferCanvasContext.save();
    GridBase.Container.bufferCanvasContext.clearRect(
      0,
      0,
      GridBase.Container.containerRect.width * GridBase.Settings.ratio,
      GridBase.Container.containerRect.height * GridBase.Settings.ratio,
      // GridBase.Content.dimensions.width * GridBase.Settings.ratio,
      // GridBase.Content.dimensions.height * GridBase.Settings.ratio,
    );
    GridBase.Container.bufferCanvasContext.scale(GridBase.Settings.scale, GridBase.Settings.scale);
  }
  private drawCanvas() {
    GridBase.Container.bufferCanvasContext.restore();
    GridBase.Container.canvasContext.clearRect(
      0,
      0,
      GridBase.Container.containerRect.width * GridBase.Settings.ratio,
      GridBase.Container.containerRect.height * GridBase.Settings.ratio,
    );
    GridBase.Container.canvasContext.drawImage(GridBase.Container.bufferCanvas, 0, 0);
  }
  private createGraphics() {
    // create graphics
    for (let i = 0; i < this.offsetRectangles.length; i++) {
      this.drawOffsetRect(this.offsetRectangles[i][0], this.offsetRectangles[i][1]);
    }
    for (let i = 0; i < this.rectangles.length; i++) {
      this.drawRect(this.rectangles[i][0], this.rectangles[i][1]);
    }
    for (let i = 0; i < this.texts.length; i++) {
      this.drawText(this.texts[i][0], this.texts[i][1], this.texts[i][2]);
    }
    for (let i = 0; i < this.hidden.length; i++) {
      this.drawHidden(this.hidden[i][0], this.hidden[i][1], this.hidden[i][2]);
    }
    for (let i = 0; i < this.lines.length; i++) {
      this.drawLine(this.lines[i][0], this.lines[i][1], this.lines[i][2]);
    }
  }
  private setCoords(coords: IContentCellMetaCoords, offsetRow: number, offsetCol: number): IContentCellMetaCoords {
    return {
      x: coords.x - offsetCol,
      y: coords.y - offsetRow,
      width: coords.width,
      height: coords.height,
    };
  }
  private pushElements(cell, coords, cellMeta?: IContentCellMeta) {
    let color = '#' + cell.style['background-color'];

    if (cellMeta.heading && this.highlightHeading(cellMeta)) {
      color = GridBase.Settings.contrast === 'dark' ? '#555555' : '#b7b7b7';
    } else if (!cellMeta.heading && cellMeta.formatting && cellMeta.formatting.background) {
      color = cellMeta.formatting.background;
    } else if (!this.helpers.validHex(color)) {
      color = '#FFFFFF';
    }
    this.offsetRectangles.push([coords, color]);
    this.rectangles.push([coords, color]);
    if (cellMeta.allow !== 'no') {
      this.texts.push([cell, coords, cellMeta]);
    }
    if ((cell.access && cell.access === 'no') || cellMeta.allow === 'no') {
      this.hidden.push([cell, coords, cellMeta]);
    }
    this.drawLines(cell, coords, this.helpers.getContrastYIQ(color));
  }
  private highlightHeading(cellMeta: IContentCellMeta): boolean {
    if (!GridBase.Content.cellSelection.from) return false;
    if (
      (cellMeta.heading === 'row'
      && cellMeta.index.col >= this.selection.from.col
      && cellMeta.index.col <= this.selection.to.col)
      ||
      (cellMeta.heading === 'col'
      && cellMeta.index.row >= this.selection.from.row
      && cellMeta.index.row <= this.selection.to.row)
    ) {
        return true;
    }
    return false;
  }
  private drawSortingIndicator(cell: IContentCellMeta, direction) {
    GridBase.Container.bufferCanvasContext.beginPath();
    GridBase.Container.bufferCanvasContext.fillStyle = 'red';
    let x = cell.coords.x + cell.coords.width - 10 - GridBase.Content.offsets.colOffset;
    if (direction === 'DESC') {
      let y = cell.coords.y;
      GridBase.Container.bufferCanvasContext.moveTo(x, y);
      GridBase.Container.bufferCanvasContext.lineTo(x + 10, y);
      GridBase.Container.bufferCanvasContext.lineTo(x + 5, y + 5);
      GridBase.Container.bufferCanvasContext.lineTo(x, y);
    } else {
      let y = cell.coords.y + cell.coords.height - 5;
      GridBase.Container.bufferCanvasContext.moveTo(x + 5, y);
      GridBase.Container.bufferCanvasContext.lineTo(x + 10, y + 5);
      GridBase.Container.bufferCanvasContext.lineTo(x, y + 5);
      GridBase.Container.bufferCanvasContext.lineTo(x + 5, y);
    }
    GridBase.Container.bufferCanvasContext.fill();
  }
  private drawRect(coords: any, color: string) {
    // let t = this.translateCanvas(1);
    // let color = this.getBackgroundColor(cell);
    GridBase.Container.bufferCanvasContext.beginPath();
    GridBase.Container.bufferCanvasContext.fillStyle = color;
    GridBase.Container.bufferCanvasContext.fillRect(coords.x, coords.y, coords.width, coords.height);
    // this.resetCanvas(t);
  }
  private drawOffsetRect(coords: any, color: string) {
    // let t = this.translateCanvas(1);
    GridBase.Container.bufferCanvasContext.beginPath();
    GridBase.Container.bufferCanvasContext.fillStyle = color;
    GridBase.Container.bufferCanvasContext.fillRect(
      coords.x - 0.5,
      coords.y - 0.5,
      coords.width + 0.5,
      coords.height + 0.5,
    );

    // this.resetCanvas(t);
  }
  private drawLines(cell, coords, contrast: string = 'light') {
    // if (!coords) {
    //   console.log(cell, coords);
    // }

    const gridlineColor: string = 'd4d4d4';

    // // top line
    if (cell.style.tbs !== 'none' && cell.style.tbw !== 'none') {
      this.lines.push([
        { x: coords.x, y: coords.y },
        { x: coords.x + coords.width, y: coords.y },
        { width: this.borders.widths[cell.style.tbw], color: cell.style.tbc },
      ]);
    } else if (GridBase.Settings.gridlines) {
      // this.lines.push([
      //   { x: coords.x, y: coords.y },
      //   { x: coords.x + coords.width, y: coords.y },
      //   { width: 1, color: 'd4d4d4' },
      // ]);
    }

    // right line
    if (cell.style.rbs !== 'none' && cell.style.rbw !== 'none') {
      this.lines.push([
        { x: coords.x + coords.width, y: coords.y },
        { x: coords.x + coords.width, y: coords.y + coords.height },
        { width: this.borders.widths[cell.style.rbw], color: cell.style.rbc },
      ]);
    } else if (GridBase.Settings.gridlines) {
      this.lines.push([
        { x: coords.x + coords.width, y: coords.y },
        { x: coords.x + coords.width, y: coords.y + coords.height },
        { width: 1, color: gridlineColor },
      ]);
    }

    // bottom line
    if (cell.style.bbs !== 'none' && cell.style.bbw !== 'none') {
      this.lines.push([
        { x: coords.x, y: coords.y + coords.height },
        { x: coords.x + coords.width, y: coords.y + coords.height },
        { width: this.borders.widths[cell.style.bbw], color: cell.style.bbc },
      ]);
    } else if (GridBase.Settings.gridlines) {
      this.lines.push([
        { x: coords.x, y: coords.y + coords.height },
        { x: coords.x + coords.width, y: coords.y + coords.height },
        { width: 1, color: gridlineColor },
      ]);
    }

    // left line
    if (cell.style.lbs !== 'none' && cell.style.lbw !== 'none') {
      this.lines.push([
        { x: coords.x, y: coords.y },
        { x: coords.x, y: coords.y + coords.height },
        { width: this.borders.widths[cell.style.lbw], color: cell.style.lbc },
      ]);
    } else if (GridBase.Settings.gridlines) {
      this.lines.push([
        { x: coords.x, y: coords.y },
        { x: coords.x, y: coords.y + coords.height },
        { width: 1, color: gridlineColor },
      ]);
    }
  }
  private drawLine(start, end, options) {
    let t = this.translateCanvas(options.width || 1);
    GridBase.Container.bufferCanvasContext.beginPath();
    GridBase.Container.bufferCanvasContext.lineWidth = options.width || 1;
    GridBase.Container.bufferCanvasContext.strokeStyle =
      options.color.indexOf('rgb') > -1 ? options.color : `#${(options.color || '000000').replace('#', '')}`;
    GridBase.Container.bufferCanvasContext.moveTo(start.x, start.y);
    GridBase.Container.bufferCanvasContext.lineTo(end.x, end.y);
    GridBase.Container.bufferCanvasContext.stroke();
    this.resetCanvas(t);
  }
  private drawText(cell: IPageContentCell, coords: IContentCellMetaCoords, cellMeta: IContentCellMeta) {
    // check access
    if (!cellMeta.heading && (cellMeta.allow === 'no' || cellMeta.button || cellMeta.image)) {
      return;
    }
    let v = this.cleanValue(cell.formatted_value || cell.value);
    if (!v.length) return;
    let color = `#${cell.style['color'].replace('#', '')}`;
    if (!cellMeta.heading && cellMeta.formatting && cellMeta.formatting.color) {
      color = cellMeta.formatting.color;
    }
    GridBase.Container.bufferCanvasContext.fillStyle = color;
    GridBase.Container.bufferCanvasContext.font = this.getFontString(cell); //  `${cell.style["font-style"]} ${cell.style["font-weight"]} ${cell.style["font-size"]} "${cell.style["font-family"].replace(/"/g, "").replace(/'/g, "")}", sans-serif`;
    GridBase.Container.bufferCanvasContext.textAlign = cell.style['text-align']
      .replace('justify', 'left')
      .replace('start', 'left');
    // cell.style['font-size'] = '9pt';
    // let m = canvasContext.measureText(v);
    let x = 0;
    let xPad = 3;
    let y = 0;
    let yPad = 5;
    switch (cell.style['text-align']) {
      case 'right':
        x = coords.x + cellMeta.coords.columnWidth - xPad;
        break;
      case 'middle':
      case 'center':
        x = coords.x + cellMeta.coords.columnWidth / 2;
        break;
      default:
        x = coords.x + xPad;
        break;
    }
    let wrapOffset = 0;
    let fontHeight = GridBase.Container.bufferCanvasContext.measureText('M').width;
    let lines: any = [];
    let wrap =
      (cell.style['text-wrap'] && cell.style['text-wrap'] === 'wrap') || cell.style['word-wrap'] === 'break-word';
    if (wrap) {
      lines = this.findLines(v, fontHeight, cellMeta.coords.columnWidth - xPad * 2);
      wrapOffset = lines.offset;
    }
    switch (cell.style['vertical-align']) {
      case 'top':
        y = coords.y + fontHeight + yPad;
        break;
      case 'center':
      case 'middle':
        y = coords.y + coords.height / 2 - wrapOffset / 2 + fontHeight / 2;
        break;
      default:
        y = coords.y + coords.height - yPad - wrapOffset;
        break;
    }
    GridBase.Container.bufferCanvasContext.save();
    GridBase.Container.bufferCanvasContext.beginPath();
    GridBase.Container.bufferCanvasContext.rect(coords.x, coords.y, coords.width, coords.height);
    GridBase.Container.bufferCanvasContext.clip();
    if (wrap) {
      lines.lines.forEach2(line => {
        GridBase.Container.bufferCanvasContext.fillText(line.value, x, y + line.y);
        if (cell.link && !cellMeta.heading) {
          let yLine: number = Math.round(y + 2 + line.y);
          let tWidth: number = Math.round(GridBase.Container.bufferCanvasContext.measureText(line.value).width);
          let xLine: number = x;
          switch (cell.style['text-align']) {
            case 'right':
              xLine = x - tWidth;
              break;
            case 'middle':
            case 'center':
              xLine = x - tWidth / 2;
              break;
            default:
              break;
          }
          this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
        }
      });
    } else {
      if (cell.style['number-format'] && cell.style['number-format'].indexOf('0.00') > -1 && /(¥|€|£|\$)/.test(v)) {
        let num = v.split(' ');
        GridBase.Container.bufferCanvasContext.fillText(num.length > 1 ? num[1] : v, x, y);
        if (num.length > 1) {
          GridBase.Container.bufferCanvasContext.textAlign = 'left';
          GridBase.Container.bufferCanvasContext.fillText(num[0], coords.x + xPad, y);
        }
      } else {
        GridBase.Container.bufferCanvasContext.fillText(v, x, y);
      }
      if (cell.link && !cellMeta.heading) {
        let yLine: number = Math.round(y + 2);
        let tWidth: number = Math.round(GridBase.Container.bufferCanvasContext.measureText(v).width);
        let xLine: number = x;
        switch (cell.style['text-align']) {
          case 'right':
            xLine = x - tWidth;
            break;
          case 'middle':
          case 'center':
            xLine = x - tWidth / 2;
            break;
          default:
            break;
        }
        this.drawLine({ x: xLine, y: yLine }, { x: xLine + tWidth, y: yLine }, { width: 1, color: color });
      }
    }
    GridBase.Container.bufferCanvasContext.restore(); // discard clipping region
  }
  private drawHidden(cell: IPageContentCell, coords: IContentCellMetaCoords, cellMeta: IContentCellMeta) {
    let color = '#' + cell.style['background-color'];
    let contrast = this.helpers.getContrastYIQ(color);
    for (let img in GridBase.Settings.noAccessImages) {
      if (contrast !== img || !GridBase.Settings.noAccessImages[img]) {
        continue;
      }
      let pat = GridBase.Container.bufferCanvasContext.createPattern(
        document.getElementById(GridBase.Settings.noAccessImages[img]),
        'repeat',
      );
      GridBase.Container.bufferCanvasContext.beginPath();
      GridBase.Container.bufferCanvasContext.rect(coords.x, coords.y, coords.width, coords.height);
      GridBase.Container.bufferCanvasContext.fillStyle = pat;
      GridBase.Container.bufferCanvasContext.fill();
    }
  }
  private findLines(str, fontHeight, cellWidth) {
    let words = str.split(' ');
    let lines = [];

    let word = [];
    let lineY = 0;
    let lineOffset = fontHeight + 4; // leading?

    for (let w = 0; w < words.length; w++) {
      let wordWidth = GridBase.Container.bufferCanvasContext.measureText(words[w]).width + 2;

      if (wordWidth > cellWidth) {
        let letters = words[w].split('');
        let width = 0;
        wordWidth = 0;
        word = [];
        for (let i = 0; i < letters.length; i++) {
          width = GridBase.Container.bufferCanvasContext.measureText(word.join('')).width + 2;
          if (width < cellWidth) {
            word.push(letters[i]);
            wordWidth = width;
          } else {
            lines.push({
              value: word.join(''),
              width: wordWidth,
            });
            word = [letters[i]];
          }
        }
        if (word.length) {
          lines.push({
            value: word.join(''),
            width: wordWidth,
          });
        }
      } else {
        lines.push({
          value: words[w],
          width: wordWidth,
        });
      }
    }

    let rows = [];
    let width = 0;
    let lineWords = [];
    for (let i = 0; i < lines.length; i++) {
      width += lines[i].width;
      if (width < cellWidth) {
        lineWords.push(lines[i].value);
      } else {
        rows.push({
          value: lineWords.join(' '),
          y: lineY,
        });
        lineY += lineOffset;
        lineWords = [lines[i].value];
        width = lines[i].width;
      }
    }
    if (lineWords.length) {
      rows.push({
        value: lineWords.join(' '),
        y: lineY,
      });
    }

    return {
      lines: rows,
      offset: fontHeight * (rows.length - 1) + (rows.length - 1) * 4,
    };
  }
  private cleanValue(value) {
    return (value + '').trim().replace(/\s\s+/g, ' ');
  }
  private getFontString(cell: any): string {
    return `${cell.style['font-style']} ${cell.style['font-weight']} ${cell.style['font-size']} "${
      cell.style['font-family']
    }", sans-serif`;
  }
  private translateCanvas(w) {
    let translate = (w % 2) / 2;
    GridBase.Container.bufferCanvasContext.translate(translate, translate);
    return translate;
    // canvasContext.translate(-iTranslate, -iTranslate);
  }

  private resetCanvas(translate) {
    GridBase.Container.bufferCanvasContext.translate(-translate, -translate);
  }
  // private createHeadings(which, index?: number) {
  //   if (which === 'row') {
  //     let row = JSON.parse(JSON.stringify(GridBase.Content.content[0]));
  //     for (let colIndex = 0; colIndex < row.length; colIndex++) {
  //       row[colIndex] = this.setHeadingCellStyle(row[colIndex], 'col');
  //       row[colIndex].value = row[colIndex].formatted_value = this.helpers.toColumnName(colIndex + 1);
  //     }
  //     return row;
  //   }
  //   if (which === 'col') {
  //     let cell = JSON.parse(JSON.stringify(GridBase.Content.content[index][0]));
  //     cell = this.setHeadingCellStyle(cell, 'row');
  //     cell.value = cell.formatted_value = index + 1;
  //     return cell;
  //   }
  //   if (which === 'corner') {
  //     let cell = JSON.parse(JSON.stringify(GridBase.Content.content[0][0]));
  //     cell = this.setHeadingCellStyle(cell, 'corner');
  //     cell.value = cell.formatted_value = '';
  //     return cell;
  //   }
  // }
  // private setHeadingCellStyle(cell: IContentCell, pos: string): IContentCell {
  //   cell.style['text-align'] = pos === 'col' ? 'center' : 'right';
  //   cell.style['background-color'] = GridBase.Settings.contrast === 'light' ? 'e3e3e4' : '333333';
  //   if (pos === 'corner' || pos === 'row') {
  //     cell.style['width'] = '40px';
  //   }
  //   if (pos === 'corner' || pos === 'col') {
  //     cell.style['height'] = '20px';
  //   }
  //   cell.style['font-family'] = 'Calibri';
  //   cell.style['font-size'] = '9pt';
  //   cell.style['font-weight'] = 'normal';
  //   cell.style['font-style'] = 'normal';
  //   cell.style['color'] = GridBase.Settings.contrast === 'light' ? '000000' : '999999';
  //   ['b', 't', 'r', 'l'].forEach(d => {
  //     cell.style[`${d}bc`] = GridBase.Settings.contrast === 'light' ? '999999' : '000000';
  //     cell.style[`${d}bs`] = 'solid';
  //     cell.style[`${d}bw`] = 'thin';
  //     cell.heading = true;
  //   });
  //   return cell;
  // }
}

export default Canvas;
