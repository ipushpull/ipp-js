import Popper from 'popper.js';
import { GridBase } from './Grid';
import { Scrollbars, IScrollbars } from './Scrollbars';
import Classy from '../Classy';
import { Selector, ISelectorButton, ISelector } from './Selector';
import { GridLine, IGridLine } from './GridLine';
import { IPageContentCell } from '../Page/Content';
import { IContentCellMeta } from './Content';

interface ILink {
  cell: IContentCellMeta;
  element: HTMLElement;
  line?: IGridLine;
}

interface ILinkOptions {
  address?: string;
  external?: boolean;
  cell: any;
  className?: string;
  styles?: any;
  user?: number;
  color?: string;
  wrap?: boolean;
}

interface IElement {
  cell: IContentCellMeta;
  className?: string;
  href?: string;
  color?: string;
  target?: string;
  styles?: any;
  attrs?: any;
}

export interface IContainer {
  init(id: string);
  container: HTMLElement;
  containerRect: ClientRect;
  canvas: HTMLCanvasElement;
  canvasContext: CanvasRenderingContext2D;
  bufferCanvas: HTMLCanvasElement;
  bufferCanvasContext: CanvasRenderingContext2D;
  input: HTMLInputElement;
  update();
  destroy();
  setSize();
  removeAllLinks(type?: string);
  addGridLine(axis: string, cell: IContentCellMeta): void;
  addLink(type: string, options: ILinkOptions): void;
  removeLink(type: string, cell: IContentCellMeta): void;
  showInput();
  hideInput();
  hasFocus(evt: Event): boolean;
}

class Container implements IContainer {
  public container: HTMLElement;
  public containerRect: ClientRect;
  public canvas: HTMLCanvasElement;
  public canvasContext: CanvasRenderingContext2D;

  public bufferCanvas: HTMLCanvasElement;
  public bufferCanvasContext: CanvasRenderingContext2D;

  public links: any = {};
  public lines: any = {};
  public images: any = {};

  public popper: HTMLElement;
  public input: HTMLInputElement;
  private Scrollbars: IScrollbars;
  private Selector: ISelector;

  private popperControl: any;
  private classy: any;
  private zIndexes: any = {
    history: 20,
    button: 10,
    highlight: 300,
    external: 200,
  };

  constructor() {
    this.classy = new Classy();
  }
  public init(id: string) {
    // Destroy
    this.destroy();

    // Events
    GridBase.Events.register(this.onEvents);

    // Container and Canva Sizes
    this.container = document.getElementById(id);
    if (!this.container) {
      throw new Error('Container not found');
    }
    this.container.style.overflow = 'hidden';
    this.containerRect = this.container.getBoundingClientRect();
    this.canvas = document.createElement('canvas');
    this.canvasContext = this.canvas.getContext('2d');
    this.bufferCanvas = document.createElement('canvas');
    this.bufferCanvasContext = this.bufferCanvas.getContext('2d');
    this.container.appendChild(this.canvas);
    this.setSize();

    // Cell Editing Input
    this.createInputElement();

    // scrollbars
    if (!GridBase.Settings.touch && !GridBase.Settings.fluid) {
      this.Scrollbars = new Scrollbars();
      this.container.appendChild(this.Scrollbars.colAxis);
      this.container.appendChild(this.Scrollbars.rowAxis);
    }

    // Selector
    this.Selector = new Selector();
    this.Selector.element.appendChild(this.input);
    this.container.appendChild(this.Selector.element);
  }
  public update() {
    // this.setSizes();
    if (this.Scrollbars) {
      this.Scrollbars.update();
    }
    this.Selector.update();
    if (!GridBase.Content.hasFocus) {
      this.hideInput();
    }
    // if (GridBase.Content.noAccess) {
    //   this.classy.addClass(this.container, 'denied');
    // } else {
    //   this.classy.removeClass(this.container, 'denied');
    // }
    this.updateLinks();
  }
  public destroy() {
    GridBase.Events.unRegister(this.onEvents);
    if (!this.container) {
      return;
    }
    this.container.innerHTML = '';
    if (this.Scrollbars) {
      this.Scrollbars.destroy();
    }
  }
  public showInput() {
    console.log('showInput');
    let cell: any = GridBase.Content.getContentCell(GridBase.Content.cellSelection.from.index);
    let cellMeta: any = GridBase.Content.getContentCellMeta(GridBase.Content.cellSelection.from.index);
    let element = cellMeta.button && cellMeta.button.type ? cellMeta.button.type : 'input';
    if (['select'].indexOf(element) > -1) {
      if (element === 'chat') {
        this.createSelectOptions(['Embed', 'New Window']);
      } else {
        this.createSelectOptions(cellMeta.button.options);
      }
      this.popper.style.display = 'block';
      const link: ILink = this.getLink('button', cellMeta);
      this.popperControl = new Popper(link.element, this.popper, { placement: 'bottom-start' });
      return;
    }
    if (['rotate'].indexOf(element) > -1) {
      return;
    }
    let unit: string = cell.style['font-size'].indexOf('px') > -1 ? 'px' : 'pt';
    this.input.style.visibility = 'visible';
    const fontSize: number = GridBase.Content.scaleDimension(parseFloat(cell.style['font-size']));
    this.input.style['font-size'] = `${fontSize}${unit}`;
    this.input.style['font-family'] = `${cell.style['font-family']}, sans-serif`;
    this.input.style['font-weight'] = cell.style['font-weight'];
    this.input.style['text-align'] = cell.style['text-align'];
    this.input.style['visibility'] = 'visible';
    // this.input.style['width'] = cell.style.width;
    // this.input.style['height'] = cell.style.height;
    if (GridBase.Content.keyValue !== undefined) {
      this.input.value = GridBase.Content.keyValue;
    }

    if (!GridBase.Settings.touch) {
      // this.selection.appendChild(this.input);
      // this.selection.contentEditable = false;
      let interval = setInterval(() => {
        if (this.input.offsetParent === null) return;
        // if (!this.selection.contains(this.input)) return;
        clearInterval(interval);
        if (GridBase.Content.keyValue === undefined) {
          this.input.value = `${cell.formatted_value || cell.value}`;
        }
        this.input.focus();
        if (element === 'input') {
          if (GridBase.Content.keyValue !== undefined) {
            this.input.selectionStart = 999;
          } else if (this.input.value) {
            this.input.selectionStart = 0;
            this.input.selectionEnd = 999;
          }
        }
      }, 20);
    } else {
      if (GridBase.Content.keyValue === undefined) {
        this.input.value = `${cell.formatted_value || cell.value}`;
      }
      this.input.focus();
      if (element === 'input') {
        if (GridBase.Content.keyValue !== undefined) {
          this.input.selectionStart = 999;
        } else if (this.input.value) {
          this.input.selectionStart = 0;
          this.input.selectionEnd = 999;
        }
      }
    }
  }
  public hideInput() {
    this.input.blur();
    this.input.style.visibility = 'hidden';
    this.input.value = '';
  }
  public setSize() {
    this.containerRect = this.container.getBoundingClientRect();
    this.canvas.width = this.containerRect.width * GridBase.Settings.ratio;
    this.canvas.height = this.containerRect.height * GridBase.Settings.ratio;
    this.canvas.style.width = this.containerRect.width + 'px';
    this.canvas.style.height = this.containerRect.height + 'px';
    this.bufferCanvas.width = this.containerRect.width * GridBase.Settings.ratio;
    this.bufferCanvas.height = this.containerRect.height * GridBase.Settings.ratio;
    this.bufferCanvas.style.width = this.containerRect.width + 'px';
    this.bufferCanvas.style.height = this.containerRect.height + 'px';
  }
  public hasFocus(evt: Event): boolean {
    let focus: boolean = false;
    [this.canvas, this.Selector.element, this.input].forEach2(element => {
      if (this.isTarget(evt.target, element)) {
        focus = true;
      }
    });
    if (!focus) {
      if (evt.target.className && evt.target.className.indexOf && evt.target.className.indexOf('-link') > -1) {
        focus = true;
      }
    }
    if (focus) {
      this.classy.addClass(this.container, 'focus');
    } else {
      this.classy.removeClass(this.container, 'focus');
    }
    return focus;
  }

  public addElement(element: HTMLElement): void {
    this.container.appendChild(element);
  }
  public addLink(type: string, options: ILinkOptions): void {
    let key: string = `${type}-${options.cell.index.row}-${options.cell.index.col}`;
    if (this.links[key]) {
      return;
    }
    let className: string = `overlay-link ${type}-link`;
    if (type === 'history') {
      className += ` user-${options.user}`;
    }
    let elOptions: IElement = {
      cell: options.cell,
      className: className,
      // href: options.address,
      // target: "_blank",
      color: options.color,
    };
    if (options.external) {
      elOptions.href = options.address;
      elOptions.target = '_blank';
    }
    let link: ILink = {
      cell: options.cell,
      element: this.createDomElement(type, elOptions),
    };
    this.links[key] = link;
  }
  public getLink(type: string, cell: IContentCellMeta): ILink {
    let key: string = `${type}-${cell.index.row}-${cell.index.col}`;
    return this.links[key] || null;
  }
  public removeAllLinks(type?: string): void {
    Object.keys(this.links).forEach(key => {
      if (type && key.indexOf(type) < 0 || key.indexOf('highlight') > -1) return;
      this.removeLinkByKey(key);
    });
  }
  public removeLink(type: string, cell: IContentCellMeta): void {
    let key: string = `${type}-${cell.index.row}-${cell.index.col}`;
    this.removeLinkByKey(key);
  }
  public removeLinkByKey(key: string): void {
    let link: ILink = this.links[key];
    if (!link) {
      return;
    }
    if (link.line) {
      link.line.destroy();
    }
    this.removeDomElement(this.links[key].element);
    this.links[key] = undefined;
  }
  public addImage(cellMeta: IContentCellMeta): void {
    const cell: IPageContentCell = GridBase.Content.getContentCell(cellMeta.index);
    let key: string = `image-${cellMeta.index.row}-${cellMeta.index.col}`;
    let image: ILink = this.images[key];
    if (image) {
      if (image.element.src !== cell.value) {
        image.element.src = cell.value;
      }
    } else {
      let image: ILink = {
        cell: cellMeta,
        element: this.createImageElement(cellMeta, cell),
      };
      this.images[key] = image;
      let element: HTMLElement = this.createDomElement('image', {
        cell: cellMeta,
        className: 'overlay-link image-link',
      });
      element.appendChild(image.element);
      let link: ILink = {
        cell: cellMeta,
        element: element,
      };
      this.links[key] = link;
    }
  }
  public removeImage(row: number, col: number): void {
    let key: string = `image-${row}-${col}`;
    this.removeLinkByKey(key);
    this.images[key] = undefined;
    // if (this.images[key]) {
    //     this.removeDomElement(this.images[key].element);
    // }
  }

  public addGridLine(axis: string, cell: IContentCellMeta): void {
    if (GridBase.Settings.touch) {
      return;
    }
    let key: string = `line-${cell.index.row}-${cell.index.col}`;
    this.removeGridLine(key);
    // console.log("addGridLine", this.links[key], key, axis, cell);
    let line: ILink = {
      cell: cell,
      element: this.createDomElement(`line-${axis}`, {
        cell: cell,
        className: `overlay-link line-link line-${axis}`,
      }),
    };
    line.line = new GridLine(line.element, axis, cell);
    this.links[key] = line;
    // console.log("addGridLine", cell);
    // this.container.appendChild(line.element);
  }
  public removeGridLine(key: string): void {
    if (!this.links[key]) return;
    this.removeDomElement(this.links[key].element);
    delete this.links[key];
  }
  private createInputElement() {
    this.input = document.createElement('input');
    // this.input.setAttribute("contenteditable", "true");
    this.input.setAttribute('autocapitalize', 'none');
    this.input.setAttribute('list', 'gridlist');
    this.input.style['position'] = 'absolute';
    this.input.style['border'] = '0';
    this.input.style['font-size'] = '12px';
    this.input.style['color'] = 'black';
    this.input.style['background-color'] = 'ivory';
    this.input.style['box-sizing'] = 'border-box';
    this.input.style['margin'] = '0';
    this.input.style['padding'] = '0 2px';
    this.input.style['outline'] = '0';
    this.input.style['left'] = '0px';
    // this.input.style['right'] = '0px';
    // this.input.style['bottom'] = '0px';
    this.input.style['top'] = '0px';
    this.input.style['width'] = '100%';
    this.input.style['text-align'] = 'left';
    this.input.style['z-index'] = 10;
    this.input.style['visibility'] = 'hidden';
    this.input.style['display'] = 'display';
    this.input.style['height'] = '100%';
    this.input.addEventListener('keyup', (evt: any) => {
      const cell: IPageContentCell = GridBase.Content.getContentCell(GridBase.Content.cellSelection.from.index);
      if (cell.formatted_value === this.input.value) return;
      GridBase.Content.dirty = true;
      GridBase.GridEvents.emit(GridBase.GridEvents.ON_CELL_VALUE, {
        cell: GridBase.Content.cellSelection.from,
        value: `${this.input.value}`,
      });
      cell.formatted_value = this.input.value;
    });
    this.input.addEventListener('paste', (evt: any) => {
      evt.stopPropagation();
      let text: string = '';
      try {
        // Other
        text = evt.clipboardData.getData('text/plain');
      } catch (exception) {}
      try {
        // IE
        if (!text) {
          text = window.clipboardData.getData('Text');
        }
      } catch (exception) {}
      GridBase.Content.dirty = true;
      GridBase.GridEvents.emit(GridBase.GridEvents.ON_CELL_VALUE, { cell: GridBase.Content.cellSelection.from, value: `${text}` });
      // evt.preventDefault();
    });

    this.popper = document.createElement('div');
    this.popper.className = 'button-menu';
    this.popper.style['display'] = 'none';
    // this.popper.innerHTML = "Hello World";
    // this.popper.addEventListener("click", (evt: MouseEvent) => {
    //     console.log("pop!");
    // });
    this.popper.addEventListener('mouseleave', (evt: MouseEvent) => {
      this.popper.style['display'] = 'none';
    });
    this.container.appendChild(this.popper);

    // let pop = new Popper(this.Selector.element);

    // this.datalist = document.createElement("datalist");
    // this.datalist.id = "gridlist";
    // let option = document.createElement("option");
    // option.value = "Fubar";
    // this.datalist.appendChild(option);
  }
  private createSelectOptions(options: string[]): void {
    this.popper.innerHTML = '';
    options.forEach(value => {
      let option = document.createElement('a');
      option.innerText = value;
      option.addEventListener('click', () => {
        // GridBase.Content.dirty = true;
        GridBase.GridEvents.emit(GridBase.GridEvents.ON_CELL_VALUE, {
          cell: GridBase.Content.cellSelection.from,
          value: `${value}`,
        });
        this.popper.style.display = 'none';
        const cell: IPageContentCell = GridBase.Content.getContentCell(GridBase.Content.cellSelection.from.index);
        cell.formatted_value = value;
        // GridBase.Content.editing = false;
      });
      this.popper.appendChild(option);
    });
  }

  private createImageElement(cellMeta: IContentCellMeta, cell: IPageContentCell): HTMLElement {
    let image = new Image();
    image.id = `image-${cellMeta.index.row}-${cellMeta.index.col}`;
    image.className = 'img-link';
    image.style.position = 'absolute';
    image.style['z-index'] = 10;
    image.style['left'] = '0';
    image.style['top'] = '0';
    image.style['width'] = '100%';
    image.style['height'] = '100%';
    // image.style.display = "none";
    image.onload = () => {
      image.dataset.height = `${image.height}`;
      image.dataset.width = `${image.width}`;
    };
    // const cell = GridBase.Content.getContentCell(cellMeta.index);
    image.src = cell.value;
    return image;
  }
  private createDomElement(type: string, options: IElement): HTMLElement {
    let element: HTMLElement | HTMLLinkElement = document.createElement('a');
    element.dataset.type = type;
    if (options.attrs && Object.keys(options.attrs).length) {
      for (let a in options.attrs) {
        element[a] = options.attrs[a];
      }
    }
    if (type === 'external' && options.href) {
      element.href = options.href;
      element.target = options.target;
    }
    element.className = options.className || '';
    element.style.position = 'absolute';
    element.style['id'] = `overlay-${options.cell.index.row}-${options.cell.index.col}`;
    element.style['z-index'] = this.zIndexes[type] || 10;
    element.style['top'] = '0';
    element.style['left'] = '0';
    element.style['width'] = `${options.cell.coords.width}px`;
    element.style['height'] = `${options.cell.coords.height}px`;
    element.style['display'] = 'block';
    // element.style["display"] = "none";
    let inner: HTMLElement = document.createElement('div');
    inner.className = 'inner-link';
    if (type === 'highlight') {
      inner.style['background-color'] = `#${options.color}`;
    }
    if (options.styles && Object.keys(options.styles).length) {
      for (let style in options.styles) {
        inner.style[style] = options.styles[style];
      }
    }
    if (type === 'button') {
      // inner.innerHTML = options.cell.formatted_value;
      if (options.cell.wrap) {
        inner.style['text-align'] = 'center';
        inner.style['white-space'] = 'normal';
      }
    }
    if (type.indexOf('line') > -1 || type === 'history') {
      let inner2: HTMLElement = document.createElement('div');
      inner2.className = type === 'history' ? 'inner-inner-link' : 'inner-line';
      element.appendChild(inner2);
    }
    element.appendChild(inner);
    return element;
  }
  private removeDomElement(element: HTMLElement): void {
    if (this.container.contains(element)) {
      this.container.removeChild(element);
    }
  }
  private updateDomElement(link: ILink): void {
    const colFreeze = GridBase.Content.getFreezeIndex('col');
    const rowFreeze = GridBase.Content.getFreezeIndex('row');
    const offsetCol = link.cell.index.col < colFreeze ? 0 : GridBase.Content.offsets.colOffset;
    const offsetRow = link.cell.index.row < rowFreeze ? 0 : GridBase.Content.offsets.rowOffset;

    let x: number = GridBase.Content.scaleDimension(link.cell.coords.x - offsetCol);
    let y: number = GridBase.Content.scaleDimension(link.cell.coords.y - offsetRow);
    let height: number = GridBase.Content.scaleDimension(link.cell.coords.height);
    let width: number = GridBase.Content.scaleDimension(link.cell.coords.width);

    link.element.style.left = `${x}px`;
    link.element.style.top = `${y}px`;
    link.element.style.height = `${height}px`;
    link.element.style.width = `${width}px`;
    let cell: IPageContentCell = GridBase.Content.getContentCell(link.cell.index);
    if (!cell) {
      console.log(link.cell.index);
      return;
    }
    let unit: string = cell.style['font-size'].indexOf('px') > -1 ? 'px' : 'pt';
    const fontSize: number = GridBase.Content.scaleDimension(parseFloat(cell.style['font-size']));
    link.element.style['font-size'] = `${fontSize}${unit}`;
    link.element.style['font-family'] = `${cell.style['font-family']}, sans-serif`;
    link.element.style['font-weight'] = cell.style['font-weight'];
    // update inner text
    let inner = link.element.getElementsByClassName('inner-link')[0];
    if (link.element.dataset.type === 'button') {
      if (link.cell.formatting) {
        inner.style['color'] = link.cell.formatting.color;
        inner.style['background-color'] = link.cell.formatting.background;
      } else {
        inner.style['color'] = `#${cell.style.color}`;
        inner.style['background-color'] = `#${cell.style['background-color']}`;
      }
      inner.innerHTML = cell.formatted_value;
    }
    // let imageKey = `image-${link.cell.index.row}-${link.cell.index.col}`;
    // let image: ILink = this.images[imageKey];
    // if (image) {
    //     image.element.style.height = `${(parseFloat(image.element.dataset.height) * GridBase.Settings.scale) / GridBase.Settings.ratio}`;
    //     image.element.style.width = `${(parseFloat(image.element.dataset.width) * GridBase.Settings.scale) / GridBase.Settings.ratio}`;
    // }
  }

  private isTarget(evtTarget, element: HTMLElement) {
    let clickedEl: any = evtTarget;
    while (clickedEl && clickedEl !== element) {
      clickedEl = clickedEl.parentNode;
    }
    return clickedEl === element;
  }
  private updateLinks() {
    for (let key in this.links) {
      let link: ILink = this.links[key];
      if (!link) {
        continue;
      }
      if (link.element.dataset.type === 'highlight' && !link.element.dataset.active) {
        link.element.dataset.active = '1';
        link.element.classList.add('flash');
        if (GridBase.Content.isVisible(link.cell)) {
          this.updateDomElement(link);
          this.container.appendChild(link.element);
        }
        setTimeout(() => {
          link.element.classList.add('done');
          setTimeout(() => {
            if (this.container.contains(link.element)) {
              this.container.removeChild(link.element);
            }
            if (this.links[key]) {
              delete this.links[key];
            }
          }, GridBase.Settings.highlightsRemoveTime);
        }, GridBase.Settings.highlightsShowTime);
      } else if (GridBase.Content.isVisible(link.cell)) {
        this.updateDomElement(link);
        if (!this.container.contains(link.element)) {
          this.container.appendChild(link.element);
        }
      } else {
        if (this.container.contains(link.element)) {
          this.container.removeChild(link.element);
        }
      }
    }
  }

  private onEvents = (name: string, evt?: any) => {
    if (name === 'mousedown') {
      if (
        this.popper.style.display === 'block' &&
        !this.isTarget(evt.target, this.Selector.element) &&
        !this.isTarget(evt.target, this.popperControl.reference) &&
        !this.isTarget(evt.target, this.popper)
      ) {
        this.popper.style.display = 'none';
      }
    }
  };
}

export default Container;
