import * as _ from 'underscore';
// import * as merge from 'merge';
import { GridBase, Grid } from './Grid';
import Helpers, { IHelpers, ICellRange } from '../Helpers';
import Emitter from '../Emitter';
import { IPageContentCell, IPageCellFormatting } from '../Page/Content';
import Classy from '../Classy';
import Container from './Container';
import { PermissionRange } from '../Page/Range';

export interface IContent {
  cellSelection: IContentCellSelection;
  content: any[];
  contentMeta: IContentMeta;
  offsets: IContentOffets;
  editing: boolean;
  ctrlDown: boolean;
  shiftDown: boolean;
  hasFocus: boolean;
  scrolling: boolean;
  resizing: boolean;
  dimensions: IContentDimensions;
  update();
  destroy();
  setContent(content: any, ranges?: any, userId?: number);
  setDeltaContent(content: any);
  setHiddenColumns: (data: any) => void;
  setOffsets(row: number, col: number, set?: boolean);
  setFit(value: string);
  setFreeze(row: number, col: number);
  setSelector(from?: any, to?: any);
  setSelectorByRef(ref?: string);
  getContentCell(index: IContentCellPosition, headings?: boolean);
  getContentCellMeta(index: IContentCellPosition, headings?: boolean): IContentCellMeta;
  cellHistory(data: any);
  cellHighlights(data: any);
  updateContent();
  keepSelectorWithinView();
  scaleDimension(num: number);
  find: (data: IContentFind) => void;
  clearFound: () => void;
  setSorting: (data: any) => void;
  clearSorting: () => void;
  setFilters: (data: any) => void;
  clearFilters: () => void;
  getFreezeIndex(which: string): number;
  getFreezeSize(which: string, headings?: boolean): number;
  isVisible(cell: IContentCellMeta): boolean;
  getPageOffset(direction: string): IContentPageOffset;
  getStyle(position: IContentCellPosition): IPageCellFormatting;
}

export interface IContentDimensions {
  width: number;
  height: number;
  rows: number[];
  cols: number[];
}

export interface IContentOffets {
  row: number;
  col: number;
  rowTo: number;
  colTo: number;
  rowMax: number;
  colMax: number;
  colOffset: number;
  rowOffset: number;
}

export interface IContentCellMetaCoords {
  x: number;
  y: number;
  width: number;
  height: number;
  columnWidth?: number;
}

export interface IContentCellMetaMerge {
  col?: number;
  left?: number;
  right?: number;
  endCol?: number;
  startCol?: number;
  width?: number;
}
interface IContentCellMetaFormatting {
  color?: string;
  background?: string;
}
export interface IContentCellMeta {
  allow?: any;
  button?: any;
  image?: any;
  hidden?: boolean;
  heading?: boolean | string;
  history?: boolean;
  link?: boolean;
  formatting?: IContentCellMetaFormatting;
  merge?: IContentCellMetaMerge;
  coords?: IContentCellMetaCoords;
  scaledCoords?: IContentCellMetaCoords;
  index?: IContentCellPosition;
  position?: IContentCellPosition;
  reference: string;
  wrap?: boolean;
}

export interface IContentCellPosition {
  row: number;
  col: number;
}

export interface IContentFind {
  query: string;
}

export interface IContentCellSelection {
  from: IContentCellMeta;
  to: IContentCellMeta;
  last: IContentCellMeta;
  go: boolean; // allows multiple cell selections
  on: boolean;
  multiple: boolean;
  flipped: boolean;
  flippedFrom: IContentCellPosition;
  flippedTo: IContentCellPosition;
  clicked: number;
  found: any;
  reference: string;
}

export interface IContentSize {
  rows: number;
  cols: number;
}

export interface IContentMeta extends Array<any> {
  [index: number]: IContentCellMeta[];
}

interface IContentStyles extends Array<any> {
  [index: number]: IContentCellMetaFormatting[];
}

export interface IHeadingSelection {
  rightClick: boolean;
  which: string;
  selection: IContentCellSelection;
  event: MouseEvent;
  count: number;
}

export interface IContentPageOffset {
  row: number;
  col: number;
  selection: number;
}

class Content extends Emitter implements IContent {
  public originalContent: any[] = [];
  public content: any[] = [];
  public contentMeta: IContentMeta = [[]];
  public dimensions: IContentDimensions = {
    width: 0,
    height: 0,
    rows: [],
    cols: [],
  };
  public offsets: IContentOffets = {
    row: 0,
    col: 0,
    rowTo: 0,
    colTo: 0,
    rowMax: 0,
    colMax: 0,
    colOffset: 0,
    rowOffset: 0,
  };
  public freezeRange = {
    valid: false,
    height: 0,
    width: 0,
    index: {
      row: 0,
      col: 0,
    },
  };
  public headingSelected: string = '';
  public dirty: boolean = false;
  public hidden: boolean = false;
  public scrolling: boolean = false;
  public resizing: boolean = false;
  public hasFocus: boolean = false;
  public ctrlDown: boolean = false;
  public shiftDown: boolean = false;
  public unshift: boolean = false;
  public noAccess: boolean = false;
  public keyValue: string;
  public cellSelection: IContentCellSelection = {
    from: undefined,
    to: undefined,
    last: undefined,
    go: false, // allows multiple cell selections
    on: false,
    clicked: 0,
    found: undefined,
    multiple: false,
    flipped: false,
    reference: '',
  };
  public classy: any;
  public helpers: IHelpers;
  public styles: IContentStyles = [];
  public buttons: any[] = [];
  public history: any[] = [];
  public highlights: any[] = [];
  public hiddenColumns: any = [];
  public tracking: boolean = false;

  public borders = {
    widths: {
      none: 0,
      thin: 1,
      medium: 2,
      thick: 3,
    },
    styles: {
      none: 'solid',
      solid: 'solid',
      double: 'double',
    },
    names: {
      t: 'top',
      r: 'right',
      b: 'bottom',
      l: 'left',
    },
  };

  private _editing: boolean = false;

  private cellStyles: string[] = [
    'background-color',
    'color',
    'font-family',
    'font-size',
    'font-style',
    'font-weight',
    'height',
    'number-format',
    'text-align',
    'text-wrap',
    'width',
    'vertical-align',
  ];
  private accessRanges: any = [];
  private found: any = [];
  private foundSelection: any = {
    query: '',
    count: 0,
    selected: 0,
    cell: undefined,
  };
  private scrollbarSize: any = {
    y: 8,
    x: 8,
  };

  public sorting: any = {};
  public filters = {};

  get editing(): boolean {
    return this._editing;
  }

  set editing(value: boolean) {
    this._editing = value;
    GridBase.GridEvents.emit(GridBase.GridEvents.ON_EDITING, {
      cell: this.cellSelection.from,
      editing: this._editing,
      dirty: this.dirty,
    });

    if (value) {
      // check if allow
      const cell: IPageContentCell = this.cellSelection.from
        ? this.getContentCell(this.cellSelection.from.index)
        : null;
      if (
        !this.cellSelection.from ||
        this.cellSelection.from.heading ||
        this.cellSelection.from.allow !== 'yes' ||
        (cell && cell.access === 'no') ||
        !GridBase.Settings.canEdit
      ) {
        this._editing = false;
        return;
      }
      this.cellSelection.go = false;

      GridBase.Container.showInput();
    } else {
      this.keyValue = undefined;
      GridBase.Container.hideInput();
      if (this.dirty) {
        this.dirty = false;
      }
      // soft merges. TODO: Check if tracking is set.
      // this.setCellMetaByRow(this.content[this.cellSelection.from.index.row], this.cellSelection.from.index.row);
    }
  }

  public constructor() {
    super();
    this.helpers = new Helpers();
    this.classy = new Classy();
  }
  public init() {
    GridBase.Events.register(this.onEvents);
  }
  public update() {
    // this.setHeadings();
    if (!this.resizing) {
      this.setFit(GridBase.Settings.fit);
    }
    if (this.content.length) {
      this.setAllOffsets();
    }
    // this.render();
  }
  public destroy() {
    GridBase.Events.unRegister(this.onEvents);
    this.buttons = [];
    this.styles = [];
    this.history = [];
    this.highlights = [];
    this.hiddenColumns = [];
    this.tracking = false;
    this.content = [];
  }
  public setContent(content: any, ranges: any, userId: number = undefined) {
    this.content = [...content];
    this.contentMeta = [];
    this.offsets.rowTo = 0;
    this.offsets.colTo = 0;
    this.offsets.colMax = 0;
    this.offsets.rowMax = 0;
    this.setRanges(ranges, userId);
    this.updateContent();
  }
  public resetButtons(): void {
    // GridBase.Container.removeAllLinks('buttons');
  }
  public updateContent(refresh?: boolean) {
    if (!refresh) {
      this.applyFilters();
      this.applySorting();
      if (this.content.length) {
        this.setHeadings();
      }
    }
    if (!this.content.length) {
      return;
    }
    this.setDimensions(refresh);
    this.setCellMetaData(refresh);
    this.setButtons(refresh);
    this.find({ query: this.foundSelection.query });
  }
  public setDeltaContent(content: any) {
    let offset = GridBase.Settings.headings ? 1 : 0;
    for (let rowIndex = 0; rowIndex < content.length; rowIndex++) {
      if (!content[rowIndex] || !this.content[rowIndex + offset]) continue;
      for (let colIndex = 0; colIndex < content[rowIndex].length; colIndex++) {
        let cell = content[rowIndex][colIndex];
        if (!cell) continue;
        this.content[rowIndex + offset][colIndex + offset] = cell;
      }
      this.updateSoftmerges(rowIndex);
    }
    this.setButtons();
    this.find({ query: this.foundSelection.query });
  }
  public setOffsets(row: number, col: number, set: boolean = false) {
    if (set) {
      this.offsets.row = row;
      this.offsets.col = col;
    } else {
      this.offsets.row += row;
      this.offsets.col += col;
    }
    this.setAllOffsets();
  }
  public getSize(): IContentSize {
    const offset = GridBase.Settings.headings ? 1 : 0;
    let rows = this.contentMeta.length ? this.contentMeta.length - offset : 0;
    // if (!rows) {
    //   this.clearCellSelection();
    //   return;
    // }
    let cols = this.contentMeta.length ? this.contentMeta[0].length - offset : 0;
    return {
      rows,
      cols,
    };
  }
  public setSelectorByRef(ref: string): void {
    const size: IContentSize = this.getSize();
    if (!size.rows) {
      this.clearCellSelection();
      return;
    }
    const range: ICellRange = this.helpers.cellRange(ref, size.rows, size.cols);
    let fromCellMeta: IContentCellMeta = this.getContentCellMetaByPosition(range.from);
    if (!fromCellMeta) {
      this.clearCellSelection();
      return;
    }
    let toCellMeta: IContentCellMeta = this.getContentCellMetaByPosition(range.to);
    this.cellSelection.from = fromCellMeta;
    this.cellSelection.to = toCellMeta || fromCellMeta;
    this.cellSelection.reference = ref;
  }
  public setSelector(from?: any, to?: any): void {
    if (!from) {
      this.clearCellSelection();
      return;
    }
    if (!to) {
      to = from;
    }
    const offset: number = GridBase.Settings.headings ? 1 : 0;
    let toRow = to.row < 0 ? this.content.length - 1 : to.row + offset;
    let toCol = to.col < 0 ? this.content.length[0] - 1 : to.col + offset;
    if (
      !this.contentMeta[from.row + offset] ||
      !this.contentMeta[from.row + offset][from.col + offset] ||
      !this.contentMeta[toRow] ||
      !this.contentMeta[toRow][toCol]
    ) {
      this.clearCellSelection();
      return;
    }
    this.cellSelection.from = this.contentMeta[from.row + offset][from.col + offset];
    this.cellSelection.to = this.contentMeta[toRow][toCol];
    this.keepSelectorWithinView();
  }
  public setColSize(headingCell: IContentCellMeta, value: number): void {
    // headingCell.style.width = `${value}px`;
    for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
      let cell: IPageContentCell = this.content[rowIndex][headingCell.index.col];
      let cellMeta: IContentCellMeta = this.contentMeta[rowIndex][headingCell.index.row];
      cell.style.width = `${value}px`;
      cellMeta.coords.width = value;
    }
    // this.setStage();
    // this.setVisibility();
    this.updateContent(true);
    this.update();
  }
  public setRowSize(headingCell: IContentCellMeta, value: number): void {
    // headingCell.style.height = `${value}px`;
    for (let colIndex = 0; colIndex < this.content[headingCell.index.row].length; colIndex++) {
      let cell: IPageContentCell = this.content[headingCell.index.row][colIndex];
      let cellMeta: IContentCellMeta = this.contentMeta[headingCell.index.row][colIndex];
      cell.style.height = `${value}px`;
      cellMeta.coords.height = value;
    }
    this.updateContent(true);
    this.update();
  }
  public keepSelectorWithinView() {
    if (!this.cellSelection.from || (this.cellSelection.from === this.cellSelection.to && this.cellSelection.go)) {
      return;
    }

    let rect: ClientRect = GridBase.Container.containerRect;
    let colOffset = 0;
    let rowOffset = 0;
    let set: boolean = false;
    let setRow: boolean = false;
    let setCol: boolean = false;
    let cell: IContentCellMeta = this.scaleCellCoords(this.cellSelection.to || this.cellSelection.from);

    // is flipped?
    if (
      this.cellSelection.to &&
      (this.cellSelection.from.index.col > this.cellSelection.to.index.col ||
        this.cellSelection.from.index.row > this.cellSelection.to.index.row)
    ) {
      let from: IContentCellPosition = { ...this.cellSelection.from.index };
      let to: IContentCellPosition = { ...this.cellSelection.to.index };
      // to row < from row
      if (to.row < from.row) {
        from.row = to.row;
        to.row = this.cellSelection.from.index.row;
      }
      if (to.col < from.col) {
        from.col = to.col;
        to.col = this.cellSelection.from.index.col;
      }
      this.cellSelection.flipped = true;
      this.cellSelection.flippedFrom = from;
      this.cellSelection.flippedTo = to;
    }

    if (
      cell.scaledCoords.x - this.scaleDimension(this.offsets.colOffset) > GridBase.Container.containerRect.width ||
      cell.scaledCoords.x - this.scaleDimension(this.offsets.colOffset) < 0
    ) {
      for (let colIndex = 0; colIndex < this.dimensions.cols.length; colIndex++) {
        if (
          this.scaleDimension(this.dimensions.cols[colIndex] - this.offsets.colOffset) >
          cell.scaledCoords.x +
            cell.scaledCoords.width -
            this.scaleDimension(this.offsets.colOffset) -
            GridBase.Container.containerRect.width
        ) {
          colOffset = colIndex;
          break;
        }
      }
      setCol = true;
    }

    if (
      cell.scaledCoords.y - this.scaleDimension(this.offsets.rowOffset) > GridBase.Container.containerRect.height ||
      cell.scaledCoords.y - this.scaleDimension(this.offsets.rowOffset) < 0
    ) {
      for (let rowIndex = 0; rowIndex < this.dimensions.rows.length; rowIndex++) {
        if (
          this.scaleDimension(this.dimensions.rows[rowIndex] - this.offsets.rowOffset) >
          cell.scaledCoords.y +
            cell.scaledCoords.height -
            this.scaleDimension(this.offsets.rowOffset) -
            GridBase.Container.containerRect.height
        ) {
          rowOffset = rowIndex;
          break;
        }
      }
      setRow = true;
    }

    // if (cell.index.col - this.offsets.col < this.getFreezeIndex('col')) {
    //   setCol = true;
    //   colOffset = 0;
    // }

    // if (cell.index.row - this.offsets.row < this.getFreezeIndex('row')) {
    //   setRow = true;
    //   rowOffset = 0;
    // }

    if (!setRow && setCol && cell.index.col < this.getFreezeIndex('col')) {
      return;
    }

    if (!setCol && setRow && cell.index.row <= this.getFreezeIndex('row')) {
      return;
    }

    // check if in view
    // if (cell.index.row > this.offsets.rowTo || cell.index.row < this.offsets.row) {
    // rowOffset = cell.index.row - this.getFreezeIndex('row');
    // for (let rowIndex = 0; rowIndex < this.dimensions.rows.length; rowIndex++) {
    //   if (this.dimensions.rows[rowIndex] > GridBase.Container.containerRect.height) {
    //     rowOffset = rowIndex;
    //     break;
    //   }
    // }
    // setRow = true;
    // }
    // if (cell.index.col > this.offsets.colTo || cell.index.col < this.offsets.col) {
    //   colOffset = cell.index.col - this.getFreezeIndex('col');
    // for (let colIndex = 0; colIndex < this.dimensions.cols.length; colIndex++) {
    //   if (this.dimensions.cols[colIndex] > GridBase.Container.containerRect.width) {
    //     colOffset = colIndex;
    //     break;
    //   }
    // }
    // setCol = true;
    // }
    if (setRow || setCol) {
      // console.warn('keepSelectorWithinView', setRow, rowOffset, setCol, colOffset);
      // rowOffset = cell.index.row - this.getFreezeIndex('row');
      // colOffset = cell.index.col - this.getFreezeIndex('col');
      this.setOffsets(setRow ? rowOffset : this.offsets.row, setCol ? colOffset : this.offsets.col, true);
      return;
    }

    if (
      this.scaleDimension(cell.coords.x - this.offsets.colOffset) < this.dimensions.cols[this.getFreezeIndex('col')]
    ) {
      colOffset = -1;
    } else if (this.scaleDimension(cell.coords.x + cell.coords.width - this.offsets.colOffset) > rect.width - 8) {
      colOffset = 1;
    }
    if (
      this.scaleDimension(cell.coords.y - this.offsets.rowOffset) < this.dimensions.rows[this.getFreezeIndex('row')]
    ) {
      rowOffset = -1;
    } else if (this.scaleDimension(cell.coords.y + cell.coords.height - this.offsets.rowOffset) > rect.height - 8) {
      rowOffset = 1;
    }
    // console.warn('keepSelectorWithinView', rowOffset, colOffset, set);

    // check if hidden col
    this.setOffsets(rowOffset, colOffset, set);
  }
  public setFit(value: string) {
    if (!/(scroll|width|height|contain)/.test(value)) {
      return;
    }
    GridBase.Settings.set('fit', value);
    let scrollbarWidth: number = GridBase.Settings.scrollbarWidth;
    // this.offsets.row = 0;
    // this.offsets.col = 0;
    switch (value) {
      case 'scroll':
        this.setScale(1);
        break;
      case 'width':
        // if (!this.offsets.maxCol) scrollbarWidth = 0;
        this.setScale((GridBase.Container.containerRect.width - scrollbarWidth) / this.dimensions.width);
        break;
      case 'height':
        // if (!this.offsets.maxRow) scrollbarWidth =  0;
        this.setScale((GridBase.Container.containerRect.height - scrollbarWidth) / this.dimensions.height);
        break;
      case 'contain':
        let w = GridBase.Container.containerRect.width / this.dimensions.width;
        let h = GridBase.Container.containerRect.height / this.dimensions.height;
        this.setScale(h < w ? h : w);
        break;
    }
  }
  public setScale(n: number): void {
    GridBase.Settings.scale = n * GridBase.Settings.ratio;
  }
  public scaleDimension(num: number): number {
    return (num * 1 * GridBase.Settings.scale) / GridBase.Settings.ratio;
  }
  public setFreeze(row: number = 0, col: number = 0): void {
    // this.freezeRange.valid = false;
    this.freezeRange.index.row = row < 0 ? 0 : row;
    this.freezeRange.index.col = col < 0 ? 0 : col;
  }
  public setHeadings() {
    this.content = [...this.content];
    this.contentMeta = [];
    if (!GridBase.Settings.headings) {
      return;
    }
    const row = this.createHeadings('row');
    row.unshift(this.createHeadings('corner'));
    this.content.unshift(row);
    for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
      if (!rowIndex) {
        continue;
      }
      this.content[rowIndex] = [...this.content[rowIndex]];
      this.content[rowIndex].unshift(this.createHeadings('col', rowIndex));
    }
  }
  public getFreezeIndex(which: string) {
    // return this.freezeRange.index[which];
    return GridBase.Settings.headings ? this.freezeRange.index[which] + 1 : this.freezeRange.index[which];
  }
  public getFreezeSize(which: string, headings?: boolean) {
    const index = this.getFreezeIndex(which);
    const size: number = GridBase.Settings.headings && headings ? (which === 'row' ? 20 : 40) : 0;
    return this.dimensions[`${which}s`][index] - size;
  }
  public updateSoftmerges(row: number) {
    const rowIndex: number = GridBase.Settings.headings ? row + 1 : row;
    this.setCellMetaByRow(this.content[rowIndex], rowIndex);
  }
  public isVisible(cell: IContentCellMeta): boolean {
    const colFreeze = this.getFreezeIndex('col');
    const rowFreeze = this.getFreezeIndex('row');
    const colFrom = GridBase.Settings.headings ? 1 : 0;
    // if (cell.index.col < colFreeze) {
    //   return cell.index.row >= this.offsets.row && cell.index.row <= this.offsets.rowTo;
    // } else if (cell.index.row < rowFreeze) {
    //   return cell.index.col >= this.offsets.col && cell.index.col <= this.offsets.colTo;
    // }

    // if (cell.heading === 'row') {
    //    return cell.index.col >= this.offsets.col && cell.index.col <= this.offsets.colTo;
    // } else if (cell.heading === 'col') {
    //   return cell.index.row >= this.offsets.row && cell.index.row <= this.offsets.rowTo;
    // } else if (cell.heading === 'corner') {
    //   return true;
    // }

    if (cell.heading) {
      if (cell.heading === 'col') {
        return cell.index.row >= this.offsets.row && cell.index.row <= this.offsets.rowTo;
      } else if (cell.heading === 'row') {
        return cell.index.col >= this.offsets.col && cell.index.col <= this.offsets.colTo;
      }
      return;
    }

    // frozen corner
    if (cell.index.row < rowFreeze && cell.index.col < colFreeze) {
      return true;
    }

    if (cell.index.row - this.offsets.row < rowFreeze && cell.position.row >= this.freezeRange.index.row) {
      return false;
    }
    if (cell.index.col - this.offsets.col < colFreeze && cell.position.col >= this.freezeRange.index.col) {
      return false;
    }
    // TODO check for col offset as well
    if (cell.position.row < this.freezeRange.index.row) {
      return true;
    }
    if (
      cell.position.col < this.freezeRange.index.col &&
      ((cell.index.row >= this.offsets.row && cell.index.row <= this.offsets.rowTo) ||
        cell.position.row < this.freezeRange.index.row)
    ) {
      return true;
    }

    return (
      cell.index.row >= this.offsets.row &&
      cell.index.row <= this.offsets.rowTo &&
      cell.index.col >= this.offsets.col &&
      cell.index.col <= this.offsets.colTo
    );
  }
  public find(data: IContentFind): void {
    this.clearFound();
    let value: string = data.query;
    if (!value) {
      return;
    }
    this.foundSelection.query = value;
    for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
      let row = this.content[rowIndex];
      for (let colIndex = 0; colIndex < row.length; colIndex++) {
        let col = row[colIndex];
        let cellMeta: IContentCellMeta = this.getContentCellMeta({ row: rowIndex, col: colIndex });
        let regex = new RegExp(value, 'ig');
        // let cell: any = this.content[rowIndex][colIndex];
        if (
          cellMeta.heading ||
          cellMeta.allow === 'no' ||
          (!regex.test(col.value) && !regex.test(col.formatted_value))
        ) {
          continue;
        }
        if (!this.found[rowIndex]) {
          this.found[rowIndex] = [];
        }
        this.found[rowIndex][colIndex] = col;
        this.foundSelection.count++;
        // GridBase.Container.removeLink('found', col);
        GridBase.Container.addLink('found', {
          cell: cellMeta,
        });
        // this.found[rowIndex][colIndex] = false;
      }
    }
  }
  public clearFound(): void {
    GridBase.Container.removeAllLinks('found');
    this.found = [];
    this.foundSelection.count = 0;
    this.foundSelection.selected = 0;
    this.foundSelection.query = '';
  }
  public getContentHtml(): string {
    let html = `<table style="border-collapse: collapse;">`;
    const offset: number = GridBase.Settings.headings ? 1 : 0;
    for (let rowIndex = offset; rowIndex < this.content.length; rowIndex++) {
      let row = this.content[rowIndex];
      if (
        this.cellSelection.from &&
        (rowIndex < this.cellSelection.from.index.row || rowIndex > this.cellSelection.to.index.row)
      ) {
        continue;
      }
      html += `<tr>`;
      for (let colIndex = offset; colIndex < row.length; colIndex++) {
        let col = row[colIndex];
        if (
          this.cellSelection.from &&
          this.cellSelection.to &&
          (colIndex < this.cellSelection.from.index.col || colIndex > this.cellSelection.to.index.col)
        ) {
          continue;
        }
        let cell: any = this.content[rowIndex][colIndex];
        let styles = this.flattenStyles(cell.style);
        html += `<td style="${styles}">${col.allow === 'no' ? '' : cell.formatted_value}</td>`;
      }
      html += `</tr>`;
    }
    html += `</table>`;
    return html;
  }
  public setHiddenColumns(data): void {
    this.hiddenColumns =
      data && data.length
        ? data.sort(function(a, b) {
            return b - a;
          })
        : [];
  }
  public cellHistory(data: any) {
    this.history = [];
    if (!data.length || !GridBase.Settings.tracking) {
      return;
    }
    let users: any = [];
    let count: number = 0;
    data.forEach2((push: any) => {
      count++;
      if (count === 1) {
        return;
      }
      if (users.indexOf(push.modified_by.id) === -1) {
        users.push(push.modified_by.id);
      }
      let userIndex: number = users.indexOf(push.modified_by.id);
      push.content_diff.forEach2((row: any, rowIndex: number) => {
        if (!row) {
          return;
        }
        if (!this.history[rowIndex]) {
          this.history[rowIndex] = [];
        }
        row.forEach2((cellData: any, colIndex: number) => {
          if (!cellData) {
            return;
          }
          this.history[rowIndex][colIndex] = userIndex;
          // const cell: IPageContentCell = this.getContentCell({row: rowIndex, col: colIndex}, true);
          // const cellMeta: IContentCellMeta = this.getContentCellMeta({row: rowIndex, col: colIndex}, true);
          // GridBase.Container.addLink("history", {
          //   cell: cellMeta,
          //   user: userIndex
          // });
        });
      });
    });
  }
  public cellHighlights(data: any) {
    this.highlights = [];
    if (!data.content_diff || !data.content_diff.length) {
      return;
    }
    for (let rowIndex = 0; rowIndex < data.content_diff.length; rowIndex++) {
      let row = data.content_diff[rowIndex];
      if (!row) continue;
      if (!this.highlights[rowIndex]) {
        this.highlights[rowIndex] = [];
      }
      for (let colIndex = 0; colIndex < row.length; colIndex++) {
        if (!row[colIndex]) continue;
        // const cell: IPageContentCell = this.getContentCell({ row: rowIndex, col: colIndex }, true);
        // const cellMeta: IContentCellMeta = this.getContentCellMeta({ row: rowIndex, col: colIndex }, true);
        this.highlights[rowIndex][colIndex] = true;
        // GridBase.Container.addLink('highlight', {
        //   cell: cellMeta,
        //   color: this.helpers.getContrastYIQ(cell.style['background-color']) === 'light' ? '000000' : 'FFFFFF',
        // });
      }
    }
  }
  public clearFilters() {
    this.filters = {};
    // this.updateContent();
  }

  public setFilters(filters) {
    this.clearFilters();
    if (filters.col >= 0) {
      filters = [filters];
    }
    filters.forEach2(filter => {
      let key: string = `col-${filter.col}-${filter.exp}`;
      if (!filter.value || filter.remove) {
        if (this.filters[`col${filter.col}`]) {
          delete this.filters[key];
        }
        return;
      }
      if (!filter.type) filter.type = 'number';
      filter.type = /(number|string|date)/.test(filter.type) ? filter.type : 'number';
      if (
        !filter.exp ||
        (filter.type === 'number' && ['>', '<', '<=', '>=', '==', '===', '!='].indexOf(filter.exp) === -1)
      ) {
        filter.exp = '==';
      }
      this.filters[key] = filter;
    });
    // this.updateContent();
  }

  public clearSorting() {
    this.sorting = {};
    // this.updateContent();
  }

  public setSorting(filters) {
    this.clearSorting();
    if (filters.col >= 0) {
      filters = [filters];
    }
    filters.forEach2(filter => {
      if (filter.remove) {
        if (this.filters[`col${filter.col}`]) {
          delete this.filters[`col${filter.col}`];
        }
        return;
      }
      if (!filter.direction) filter.direction = 'ASC';
      filter.direction = `${filter.direction}`.toUpperCase();
      filter.direction = /(ASC|DESC)/.test(filter.direction) ? filter.direction : 'ASC';
      filter.col = filter.col * 1 || 0;
      filter.type = /(number|string|date)/.test(filter.type) ? filter.type : 'number';
      this.sorting[`col${filter.col}`] = filter;
    });
    // this.updateContent();
  }

  public clearRanges(): void {
    this.accessRanges = [];
  }

  public setRanges(ranges: any, userId: number = undefined): void {
    this.clearRanges();
    if (!ranges || !ranges.length) return;
    let offset = GridBase.Settings.headings ? 1 : 0;
    for (let i: number = 0; i < ranges.length; i++) {
      let range: any = ranges[i];

      let rowEnd: number;
      let colEnd: number;
      if (range.range) {
        rowEnd = _.clone(range.range.to.row);
        colEnd = _.clone(range.range.to.col);
        if (rowEnd === -1) {
          rowEnd = this.content.length - 1;
        }
        if (colEnd === -1) {
          colEnd = this.content[0].length - 1;
        }
      }

      if (range instanceof PermissionRange && this.content.length) {
        let userRight: any = range.getPermission(userId || 0) || (userId ? 'yes' : 'no');
        let rowEnd: number = _.clone(range.rowEnd);
        let colEnd: number = _.clone(range.colEnd);
        if (rowEnd === -1) {
          rowEnd = this.content.length - 1;
        } else {
          rowEnd += offset;
        }
        if (colEnd === -1) {
          colEnd = this.content[0].length - 1;
        } else {
          colEnd += offset;
        }
        for (let i: number = range.rowStart + offset; i <= rowEnd; i++) {
          for (let k: number = range.colStart + offset; k <= colEnd; k++) {
            if (!this.accessRanges[i]) {
              this.accessRanges[i] = [];
            }
            if (!this.accessRanges[i][k]) {
              this.accessRanges[i][k] = [];
            }
            if (userRight) {
              this.accessRanges[i][k].push(userRight);
            }
          }
        }
      }
    }
  }

  // public setStyles(styles): void {
  //   this.styles = [];
  //   if (!styles || !styles.length) return;
  //   for (let rowIndex = 0; rowIndex < styles.length; rowIndex++) {
  //     let row = styles[rowIndex];
  //     if (!row) continue;
  //     for (let colIndex = 0; colIndex < row.length; colIndex++) {
  //       let cell = row[colIndex];
  //       if (cell === undefined) continue;
  //       let cellMeta = this.getContentCellMetaByPosition({ row: rowIndex, col: colIndex });
  //       cellMeta.formatting = cell;
  //     }
  //   }
  //   this.styles = styles;
  // }

  public getStyle(position: IContentCellPosition): IPageCellFormatting {
    if (!position || !this.styles[position.row] || !this.styles[position.row][position.col]) return null;
    return this.styles[position.row][position.col];
  }

  private setDimensions(refresh?: boolean) {
    this.dimensions = {
      width: 0,
      height: 0,
      rows: [],
      cols: [],
    };
    let width = 0;
    let height = 0;
    let colOffset = GridBase.Settings.headings ? 1 : 0;

    // remove hidden columns
    if (!refresh) {
      this.hiddenColumns.forEach(colIndex => {
        for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
          this.content[rowIndex].splice(colIndex + colOffset, 1);
          // this.content[rowIndex][colIndex + colOffset] = null;
        }
      });
    }

    // cols
    for (let colIndex = 0; colIndex < this.content[0].length; colIndex++) {
      this.dimensions.cols.push(this.dimensions.width);
      width = parseFloat(this.content[0][colIndex].style.width);
      this.dimensions.width += width;
    }
    this.dimensions.cols.push(this.dimensions.width);

    // rows
    for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
      this.dimensions.rows.push(this.dimensions.height);
      height = parseFloat(this.content[rowIndex][0].style.height);
      this.dimensions.height += height;
    }
    this.dimensions.rows.push(this.dimensions.height);
  }
  private setAllOffsets() {
    this.offsets.colMax = 0;
    this.offsets.rowMax = 0;

    let width = GridBase.Settings.headings ? this.scaleDimension(40) : GridBase.Container.containerRect.width;
    let height = GridBase.Settings.headings ? this.scaleDimension(20) : GridBase.Container.containerRect.height;

    if (GridBase.Settings.headings) {
      this.offsets.colMax = this.dimensions.cols.length - 1 - this.getFreezeIndex('col') - 1;
      this.offsets.rowMax = this.dimensions.rows.length - 1 - this.getFreezeIndex('row') - 1;
    } else {
      const scrollbarX: number =
        GridBase.Settings.fit === 'height' ||
        GridBase.Settings.fit === 'contain' ||
        GridBase.Container.containerRect.height > this.scaleDimension(this.dimensions.height)
          ? 0
          : GridBase.Settings.scrollbarWidth;
      const scrollbarY: number =
        GridBase.Settings.fit === 'width' ||
        GridBase.Settings.fit === 'contain' ||
        GridBase.Container.containerRect.width > this.scaleDimension(this.dimensions.width)
          ? 0
          : GridBase.Settings.scrollbarWidth;

      // max col offset
      if (!scrollbarY) {
        this.offsets.colMax = 0;
      } else {
        let offsetWidth =
          this.scaleDimension(this.dimensions.width) - GridBase.Container.containerRect.width + scrollbarX;
        for (let colIndex = 0; colIndex < this.dimensions.cols.length; colIndex++) {
          if (this.scaleDimension(this.dimensions.cols[colIndex]) > offsetWidth) {
            this.offsets.colMax = colIndex;
            break;
          }
        }
      }
      // max row offset
      if (!scrollbarX) {
        this.offsets.rowMax = 0;
      } else {
        let offsetHeight =
          this.scaleDimension(this.dimensions.height) - GridBase.Container.containerRect.height + scrollbarY;
        for (let rowIndex = 0; rowIndex < this.dimensions.rows.length; rowIndex++) {
          if (this.scaleDimension(this.dimensions.rows[rowIndex]) > offsetHeight) {
            this.offsets.rowMax = rowIndex;
            break;
          }
        }
      }
    }

    if (this.offsets.colMax < 0) this.offsets.colMax = 0;
    if (this.offsets.rowMax < 0) this.offsets.rowMax = 0;
    if (this.offsets.row < 0) this.offsets.row = 0;
    if (this.offsets.col < 0) this.offsets.col = 0;

    if (this.offsets.col < 0) {
      this.offsets.col = 0;
    } else if (this.offsets.col > this.offsets.colMax) {
      this.offsets.col = this.offsets.colMax;
    }
    if (this.offsets.row < 0) {
      this.offsets.row = 0;
    } else if (this.offsets.row > this.offsets.rowMax) {
      this.offsets.row = this.offsets.rowMax;
    }

    this.calcOffset();

    this.offsets.rowTo = this.content.length - 1;
    for (let rowIndex = this.offsets.row; rowIndex < this.dimensions.rows.length; rowIndex++) {
      if (
        this.scaleDimension(this.dimensions.rows[rowIndex] - this.dimensions.rows[this.offsets.row]) >
        GridBase.Container.containerRect.height
      ) {
        this.offsets.rowTo = rowIndex + this.getFreezeIndex('row');
        break;
      }
    }
    this.offsets.colTo = this.content[0].length - 1;
    for (let colIndex = this.offsets.col; colIndex < this.dimensions.cols.length; colIndex++) {
      if (
        this.dimensions.cols[this.offsets.col + 1]
        && this.scaleDimension(this.dimensions.cols[colIndex] - this.dimensions.cols[this.offsets.col + 1]) > GridBase.Container.containerRect.width
      ) {
        this.offsets.colTo = colIndex + this.getFreezeIndex('col');
        break;
      }
    }

    // this.setCoordOffsets();
  }
  private calcOffset() {
    let offsetColIndex = this.offsets.col + this.getFreezeIndex('col');
    if (offsetColIndex >= this.dimensions.cols.length) offsetColIndex = this.dimensions.cols.length - 1;
    let offsetRowIndex = this.offsets.row + this.getFreezeIndex('row');
    if (offsetRowIndex >= this.dimensions.rows.length) offsetRowIndex = this.dimensions.rows.length - 1;

    let offsetCol = this.dimensions.cols[offsetColIndex];
    let offsetRow = this.dimensions.rows[offsetRowIndex];
    offsetCol -= this.dimensions.cols[this.getFreezeIndex('col')] || 0; // + (GridBase.Settings.headings ? 20 : 0);
    offsetRow -= this.dimensions.rows[this.getFreezeIndex('row')] || 0; // + (GridBase.Settings.headings ? 40 : 0);

    // const scrollbarX: number =
    //   GridBase.Settings.fit === 'height' ||
    //   GridBase.Settings.fit === 'contain' ||
    //   GridBase.Container.containerRect.height > this.scaleDimension(this.dimensions.height)
    //     ? 0
    //     : GridBase.Settings.scrollbarWidth;
    // const scrollbarY: number =
    //   GridBase.Settings.fit === 'width' ||
    //   GridBase.Settings.fit === 'contain' ||
    //   GridBase.Container.containerRect.width > this.scaleDimension(this.dimensions.width)
    //     ? 0
    //     : GridBase.Settings.scrollbarWidth;

    // this.scrollbarSize.x = scrollbarX;
    // this.scrollbarSize.y = scrollbarY;

    if (!GridBase.Settings.headings) {
      // if (this.offsets.colMax > 0 && this.offsets.col === this.offsets.colMax) {
      //   offsetCol -=
      //     GridBase.Container.containerRect.width - scrollbarX - this.scaleDimension(this.dimensions.width - offsetCol);
      // }
      // if (this.offsets.rowMax > 0 && this.offsets.row === this.offsets.rowMax) {
      //   offsetRow -=
      //     GridBase.Container.containerRect.height -
      //     scrollbarY -
      //     this.scaleDimension(this.dimensions.height - offsetRow);
      // }
    }
    this.offsets.colOffset = offsetCol;
    this.offsets.rowOffset = offsetRow;
  }
  private setCellMetaData(refresh?: boolean) {
    for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
      this.setCellMetaByRow(this.content[rowIndex], rowIndex, refresh);
    }
  }
  private setCellMetaByRow(dataRow: any = [], dataRowIndex: number = -1, refresh?: boolean) {
    // this.content.forEach2((row, rowIndex) => {

    if (!refresh) {
      this.contentMeta[dataRowIndex] = Array.from(Array(dataRow.length), () => new CellMeta());
    }

    dataRow.forEach2((col, colIndex) => {
      // row might be shortened
      if (!col) return;
      // let cell = this.content[dataRowIndex][colIndex];
      let cellMeta: IContentCellMeta = this.contentMeta[dataRowIndex][colIndex];

      // cell link
      cellMeta.link = col.link;

      // cell index
      cellMeta.index = {
        col: colIndex,
        row: dataRowIndex,
      };
      // cell position
      cellMeta.position = {
        col: col.index.col,
        row: col.index.row,
      };
      cellMeta.reference = `${this.helpers.toColumnName(col.index.col + 1)}${col.index.row + 1}`;
      // set col width
      let width = parseFloat(col.style.width);
      let wrap = this.doesWrap(col);
      if ((col.formatted_value || col.value) && !wrap) {
        GridBase.Container.canvasContext.font = this.getFontString(col);
        let textWidth: number =
          GridBase.Container.canvasContext.measureText(this.cleanValue(col.formatted_value || col.value)).width + 3;
        if (textWidth > width) {
          const merge: IContentCellMetaMerge = this.getTextOverlayWidth(dataRowIndex, colIndex, width, textWidth);
          cellMeta.merge = merge;
          // width = merge.width;
        }
      }
      cellMeta.wrap = wrap;
      // cell coords
      let height = parseFloat(col.style.height);
      cellMeta.coords = new CellMetaCoords({
        x: this.dimensions.cols[colIndex],
        y: this.dimensions.rows[dataRowIndex],
        width: cellMeta.merge ? cellMeta.merge.width : width,
        height,
        columnWidth: width,
      });
      // grid lines
      if (GridBase.Settings.headings && !refresh) {
        if (!dataRowIndex || !colIndex) {
          const headingType = !dataRowIndex && !colIndex ? 'corner' : !dataRowIndex ? 'row' : 'col';
          cellMeta.heading = headingType;
          if (!dataRowIndex && colIndex) {
            GridBase.Container.addGridLine('col', cellMeta);
          } else if (!colIndex && dataRowIndex) {
            GridBase.Container.addGridLine('row', cellMeta);
          }
        }
      }
      // deprecated access
      if (this.accessRanges[dataRowIndex] && this.accessRanges[dataRowIndex][colIndex]) {
        this.accessRanges[dataRowIndex][colIndex].forEach2(element => {
          // if (['ro', 'no'].indexOf(element) === -1) {
          //   return;
          // }
          cellMeta.allow = element;
        });
      }
      // formatting
      // if (this.styles[cellMeta.position.row] && this.styles[cellMeta.position.row][cellMeta.position.col]) {
      //   cellMeta.formatting = this.styles[cellMeta.position.row][cellMeta.position.col];
      // }

      // dom elements
      if (!refresh) {
        if (
          !cellMeta.heading &&
          this.found[cellMeta.position.row] &&
          this.found[cellMeta.position.row][cellMeta.position.col]
        ) {
          // GridBase.Container.removeLink('found', col);
          GridBase.Container.addLink('found', {
            cell: cellMeta,
          });
          // this.found[rowIndex][colIndex] = false;
        }
        if (
          !cellMeta.heading &&
          this.history[cellMeta.position.row] &&
          this.history[cellMeta.position.row][cellMeta.position.col] >= 0
        ) {
          // GridBase.Container.removeLink('history', cellMeta);
          GridBase.Container.addLink('history', {
            cell: cellMeta,
            user: this.history[cellMeta.position.row][cellMeta.position.col],
          });
        }
        if (
          !cellMeta.heading &&
          this.highlights[cellMeta.position.row] &&
          this.highlights[cellMeta.position.row][cellMeta.position.col] >= 0
        ) {
          GridBase.Container.removeLink('highlight', cellMeta);
          GridBase.Container.addLink('highlight', {
            cell: cellMeta,
            color: this.helpers.getContrastYIQ(col.style['background-color']) === 'light' ? '000000' : 'FFFFFF',
          });
          delete this.highlights[cellMeta.position.row][cellMeta.position.col];
        }
        if (`${col.value}`.indexOf('data:image') === 0) {
          GridBase.Container.removeImage(dataRowIndex, colIndex);
          cellMeta.image = true;
          GridBase.Container.addImage(cellMeta);
        }
        if (!cellMeta.heading && col.link) {
          GridBase.Container.removeLink('external', cellMeta);
          GridBase.Container.addLink('external', {
            address: col.link.address,
            external: col.link.external,
            cell: cellMeta,
          });
        }
      }
    });
    // });
  }
  private setButtons(refresh?: boolean) {
    const offset = GridBase.Settings.headings ? 1 : 0;
    const colOffset = 0;
    const size: IContentSize = this.getSize();

    this.buttons.forEach(button => {
      if (!size.rows) return;
      const range: ICellRange = this.helpers.cellRange(button.range, size.rows, size.cols);

      let cellTo: IContentCellMeta = this.getContentCellMetaByPosition(range.to);

      if (!cellTo) return;

      for (let colIndex = range.from.col; colIndex <= cellTo.position.col; colIndex++) {
        if (this.hiddenColumns.indexOf(colIndex) > -1) continue;
        for (let rowIndex = range.from.row; rowIndex <= cellTo.position.row; rowIndex++) {
          // let cellMeta: IContentCellMeta = this.getContentCellMeta({ row: rowIndex, col: colIndex });
          // if (!cellMeta || cellMeta.heading) continue;
          // if (cellMeta.position.row < range.from.row || (range.to.row > -1 && cellMeta.position.row > range.to.row)) continue;
          // let cell: IPageContentCell = this.getContentCell({ row: rowIndex, col: colIndex });
          // if (!cell) continue;
          let cellMeta: IContentCellMeta = this.getContentCellMetaByPosition({ row: rowIndex, col: colIndex });
          if (!cellMeta) continue;
          let cell: IPageContentCell = this.getContentCell({ row: cellMeta.index.row, col: cellMeta.index.col });
          GridBase.Container.removeLink('button', cellMeta);
          if (cell.access !== 'no' && cellMeta.allow === 'yes') {
            cellMeta.button = button;
            GridBase.Container.addLink('button', {
              cell: cellMeta,
              className: 'button-link',
            });
          }
        }
      }
    });
  }
  private applyFilters() {
    // let originalData = JSON.parse(JSON.stringify(content));
    if (!Object.keys(this.filters).length) {
      return false;
    }
    // let data = [];

    let rowsToRemove: number[] = [];
    // let rowOffset = GridBase.Settings.headings ? 1 : 0;

    for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
      // let offset = GridBase.Settings.headings ? -1 : 0;
      if (rowIndex < this.freezeRange.index.row) {
        // rowsToKeep.push(rowIndex);
        continue;
      }

      for (let f in this.filters) {
        let filter = this.filters[f];
        let col = this.content[rowIndex][filter.col];
        // if (!col || rowsToRemove.indexOf(rowIndex) > -1) continue;
        if (!col) continue;
        let colValue: any = filter.type === 'number' ? parseFloat(col.value) : `${col.formatted_value}`;
        let filterValue: any = filter.type === 'number' ? parseFloat(filter.value) : `${filter.value}`;

        let validValue = false;
        switch (filter.type) {
          case 'number':
            validValue = eval(`${colValue} ${filter.exp} ${filterValue}`);
            break;
          case 'date':
            let dates = filterValue.split(',');
            let fromDate = new Date(dates[0]).toISOString();
            let toDate = new Date(dates[1]).toISOString();
            let date = new Date(this.helpers.parseDateExcel(colValue)).toISOString();
            validValue = date >= fromDate && date <= toDate;
            break;
          default:
            filterValue.split(',').forEach2(value => {
              if (validValue) {
                return;
              }
              switch (filter.exp) {
                case 'contains':
                  validValue = colValue.toLowerCase().indexOf(value.toLowerCase()) > -1;
                  break;
                case 'starts_with':
                  validValue = colValue.toLowerCase().substring(0, value.length) === value.toLowerCase();
                  break;
                case '!=':
                  validValue = colValue != value; // eval(`"${colValue}" == "${value}"`);
                  break;
                default:
                  validValue = colValue == value; // eval(`"${colValue}" == "${value}"`);
                  break;
              }
            });
            break;
        }
        if (!validValue && rowsToRemove.indexOf(rowIndex) < 0) rowsToRemove.push(rowIndex);
        // if (!validValue) {
        //   this.content.splice(rowIndex, 1);
        // }
      }
    }
    for (let i = rowsToRemove.length - 1; i >= 0; i--) this.content.splice(rowsToRemove[i], 1);
    // return data;
  }

  private applySorting() {
    if (!Object.keys(this.sorting).length) {
      return false;
    }
    let key = Object.keys(this.sorting)[0];
    let filter = this.sorting[key];
    let data = this.freezeRange.index.row
      ? this.content.slice(this.freezeRange.index.row, this.content.length)
      : this.content.slice(0);
    let exp = filter.direction === 'DESC' ? '>' : '<';
    let opp = filter.direction === 'DESC' ? '<' : '>';
    data.sort((a, b) => {
      let col = filter.col;
      if (!a || !b || !a[col] || !b[col]) {
        return 1;
      }
      let aVal = filter.type === 'number' ? parseFloat(a[col].value) || 0 : `"${a[col].value}"`;
      let bVal = filter.type === 'number' ? parseFloat(b[col].value) || 0 : `"${b[col].value}"`;

      if (eval(`${aVal} ${exp} ${bVal}`)) {
        return -1;
      }
      if (eval(`${aVal} ${opp} ${bVal}`)) {
        return 1;
      }
      return 0;
    });

    this.content = this.freezeRange.index.row ? this.content.slice(0, this.freezeRange.index.row).concat(data) : data;
  }
  private getFontString(col: any): string {
    return `${col.style['font-style']} ${col.style['font-weight']} ${col.style['font-size']} "${
      col.style['font-family']
    }", sans-serif`;
  }
  private getTextOverlayWidth(row, col, width, textWidth): IContentCellMetaMerge {
    let cell = this.content[row][col];
    let textAlign: string = cell.style['text-align'];
    let left: number = 0;
    let right: number = 0;
    switch (textAlign) {
      case 'right':
        left = -1;
        break;
      case 'center':
        left = -1;
        right = 1;
        break;
      default:
        right = 1;
        break;
    }
    if (!this.content[row][col + right] || !this.content[row][col + left]) {
      return {
        col: col,
        width: width,
      };
    }
    let endCol: number = col;
    let startCol: number = col;
    if (right > 0) {
      for (let i = col + 1; i < this.content[row].length; i++) {
        if (
          this.content[row][i].formatted_value ||
          `${this.content[row][i].value}` ||
          this.content[row][i].allow === 'no'
        ) {
          break;
        }
        width += parseFloat(this.content[row][i].style.width);
        endCol = i;
        this.contentMeta[row][i].hidden = true;
        if (width > textWidth) {
          break;
        }
      }
    }
    if (left < 0) {
      // for (let i = col - 1; i >= 0; i--) {
      //   if (
      //     this.content[row][i].formatted_value ||
      //     `${this.content[row][i].value}` ||
      //     this.content[row][i].allow === 'no'
      //   ) {
      //     break;
      //   }
      //   width += parseFloat(this.content[row][i].width);
      //   startCol = i;
      //   // this.contentMeta[row][i].hidden = true;
      //   if (width > textWidth) {
      //     break;
      //   }
      // }
    }
    return {
      left: left,
      right: right,
      endCol: endCol,
      startCol: startCol,
      width: width,
    };
  }
  private cleanValue(value) {
    return (value + '').trim().replace(/\s\s+/g, ' ');
  }
  private createHeadings(which, index?: number) {
    if (which === 'row') {
      let row = JSON.parse(JSON.stringify(this.content[0]));
      for (let colIndex = 0; colIndex < row.length; colIndex++) {
        row[colIndex] = this.setHeadingCellStyle(row[colIndex], 'col');
        row[colIndex].value = row[colIndex].formatted_value = this.helpers.toColumnName(colIndex + 1);
      }
      return row;
    }
    if (which === 'col') {
      let cell = JSON.parse(JSON.stringify(this.content[index][0]));
      cell = this.setHeadingCellStyle(cell, 'row');
      cell.value = cell.formatted_value = cell.index.row + 1;
      return cell;
    }
    if (which === 'corner') {
      let cell = JSON.parse(JSON.stringify(this.content[0][0]));
      cell = this.setHeadingCellStyle(cell, 'corner');
      cell.value = cell.formatted_value = '';
      return cell;
    }
  }
  private setHeadingCellStyle(cell: IPageContentCell, pos: string): IPageContentCell {
    cell.access = null;
    cell.link = null;
    cell.style['text-align'] = 'center';
    cell.style['background-color'] = GridBase.Settings.contrast === 'light' ? 'e3e3e4' : '333333';
    if (pos === 'corner' || pos === 'row') {
      cell.style['width'] = '40px';
    }
    if (pos === 'corner' || pos === 'col') {
      cell.style['height'] = '20px';
    }
    cell.style['font-family'] = 'Calibri';
    cell.style['font-size'] = '9pt';
    cell.style['font-weight'] = 'normal';
    cell.style['font-style'] = 'normal';
    cell.style['color'] = GridBase.Settings.contrast === 'light' ? '000000' : '999999';
    ['b', 't', 'r', 'l'].forEach(d => {
      cell.style[`${d}bc`] = GridBase.Settings.contrast === 'light' ? '999999' : '000000';
      cell.style[`${d}bs`] = 'solid';
      cell.style[`${d}bw`] = 'thin';
      // cell.heading = true;
    });
    return cell;
  }
  private doesWrap(col) {
    if ((col.style['text-wrap'] && col.style['text-wrap'] === 'wrap') || col.style['word-wrap'] === 'break-word') {
      return true;
    }
    return false;
  }

  private getCell(x, y): IContentCellMeta {
    const colFreeze = this.getFreezeIndex('col');
    const rowFreeze = this.getFreezeIndex('row');

    let rect = GridBase.Container.containerRect;
    let offsetX = x - rect.left;
    let offsetY = y - rect.top;
    x -= rect.left - this.scaleDimension(this.offsets.colOffset);
    y -= rect.top - this.scaleDimension(this.offsets.rowOffset);
    let cell: IContentCellMeta;

    for (let rowIndex = 0; rowIndex < this.dimensions.rows.length - 1; rowIndex++) {
      for (let colIndex = 0; colIndex < this.dimensions.cols.length - 1; colIndex++) {
        const top = this.scaleDimension(this.dimensions.rows[rowIndex]);
        const left = this.scaleDimension(this.dimensions.cols[colIndex]);
        const cellMeta: IContentCellMeta = this.contentMeta[rowIndex][colIndex];
        let xCol = x;
        let yRow = y;

        if (rowIndex < rowFreeze) {
          yRow = offsetY;
        }
        if (colIndex < colFreeze) {
          xCol = offsetX;
        }

        if (
          xCol >= left &&
          yRow >= top &&
          xCol <= this.scaleDimension(cellMeta.coords.x + cellMeta.coords.columnWidth) &&
          yRow <= this.scaleDimension(cellMeta.coords.y + cellMeta.coords.height)
        ) {
          cell = cellMeta;
          break;
        }
      }
      if (cell) break;
    }

    return cell;
  }

  public getContentCell(index: IContentCellPosition, headings?: boolean): IPageContentCell {
    const offset = GridBase.Settings.headings && headings ? 1 : 0;
    return this.content[index.row + offset] && this.content[index.row + offset][index.col + offset]
      ? this.content[index.row + offset][index.col + offset]
      : null;
  }

  public getContentCellMeta(index: IContentCellPosition, headings?: boolean): IContentCellMeta {
    const offset = GridBase.Settings.headings && headings ? 1 : 0;
    let row = index.row + offset;
    let col = index.col + offset;
    if (index.row < 0) row = this.contentMeta.length - 1;
    if (index.col < 0) col = this.contentMeta[0].length - 1;
    return this.contentMeta[row] && this.contentMeta[row][col] ? this.contentMeta[row][col] : null;
  }

  public getContentCellMetaByPosition(position: IContentCellPosition): IContentCellMeta {
    if (!this.contentMeta.length || !this.contentMeta[0].length) {
      return null;
    }
    let offset = GridBase.Settings.headings ? 1 : 0;
    let index: IContentCellPosition = { ...position };
    // bottom right cell
    if (index.row < 0 && index.col < 0) {
      return this.contentMeta[this.contentMeta.length - 1][this.contentMeta[0].length - 1];
    }
    if (index.row < 0) {
      let rowPosition = 0;
      for (let rowIndex = 0; rowIndex < this.contentMeta.length; rowIndex++) {
        if (!this.contentMeta[rowIndex]) continue;
        let cell: IContentCellMeta = this.contentMeta[rowIndex][0];
        if (cell.position.row > rowPosition) {
          rowPosition = cell.position.row;
        }
      }
      index.row = rowPosition;
    }
    if (index.col < 0) {
      let cell: IContentCellMeta = this.contentMeta[0][this.contentMeta[0].length - 1];
      index.col = cell.position.col;
    }
    let foundCellMeta: IContentCellMeta;
    for (let rowIndex = 0; rowIndex < this.contentMeta.length; rowIndex++) {
      let row = this.contentMeta[rowIndex];
      if (!row) continue;
      for (let colIndex = 0; colIndex < row.length; colIndex++) {
        let cellMeta: IContentCellMeta = row[colIndex];
        if (!cellMeta || cellMeta.heading) continue;
        if (cellMeta.position.row === index.row && cellMeta.position.col === index.col) {
          foundCellMeta = cellMeta;
          break;
        }
      }
      if (foundCellMeta) break;
    }
    return foundCellMeta;
  }

  public getPageOffset(direction: string): IContentPageOffset {
    let rowOffset = 0;
    let colOffset = 0;
    let selectionOffset = 0;
    if (direction === 'down') {
      rowOffset = this.content.length - 1;
      for (let rowIndex = 0; rowIndex < this.content.length; rowIndex++) {
        if (
          this.scaleDimension(this.dimensions.rows[rowIndex] - this.offsets.rowOffset) >
          GridBase.Container.containerRect.height
        ) {
          rowOffset = rowIndex - this.getFreezeIndex('row') - 1;
          selectionOffset = this.getFreezeIndex('row');
          break;
        }
      }
    } else if (direction === 'up') {
      selectionOffset = this.getFreezeIndex('row');
      for (let rowIndex = this.content.length - 1; rowIndex >= 0; rowIndex--) {
        if (
          this.scaleDimension(this.offsets.rowOffset - this.dimensions.rows[rowIndex]) >
          GridBase.Container.containerRect.height
        ) {
          rowOffset = rowIndex + this.getFreezeIndex('row') + 1;
          break;
        }
      }
    } else if (direction === 'right') {
      colOffset = this.content[0].length - 1;
      for (let colIndex = 0; colIndex < this.content[0].length; colIndex++) {
        if (
          this.scaleDimension(this.dimensions.cols[colIndex] - this.offsets.colOffset) >
          GridBase.Container.containerRect.width
        ) {
          colOffset = colIndex - this.getFreezeIndex('col') - 1;
          selectionOffset = this.getFreezeIndex('col');
          break;
        }
      }
    } else if (direction === 'left') {
      selectionOffset = this.getFreezeIndex('col');
      for (let colIndex = this.content[0].length - 1; colIndex >= 0; colIndex--) {
        if (
          this.scaleDimension(this.offsets.colOffset - this.dimensions.cols[colIndex]) >
          GridBase.Container.containerRect.width
        ) {
          colOffset = colIndex + this.getFreezeIndex('col') + 1;
          break;
        }
      }
    }
    return {
      row: rowOffset,
      col: colOffset,
      selection: selectionOffset,
    };
  }

  public getCellsByIndex(from: IContentCellPosition, to: IContentCellPosition, flat?: boolean): IContentMeta {
    let cells = [];
    let row = 0;
    let col = 0;
    for (let rowIndex = from.row; rowIndex <= to.row; rowIndex++) {
      if (!flat) {
        cells[row] = [];
      }
      col = 0;
      for (let colIndex = from.col; colIndex <= to.col; colIndex++) {
        let cellMeta: IContentCellMeta = this.contentMeta[rowIndex][colIndex];
        if (flat) {
          cells.push(cellMeta);
        } else {
          cells[row][col] = cellMeta;
        }
        col++;
      }
      row++;
    }
    return cells;
  }

  private onEvents = (name: string, evt?: any) => {
    if (!this.content.length) {
      return;
    }
    if (name === 'keydown') {
      // esc
      if (evt.key === 'Escape') {
        this.dirty = false;
        this.clearCellSelection();
        GridBase.GridEvents.emit(GridBase.GridEvents.ON_CELL_RESET, this.cellSelection.last);
        return;
      }
    }
    if (name === 'mousemove') {
      this.updateCellSelection(evt);
      // if (this.scrolling) {
      //   this.setVisibility();
      // }
    }
    if (!this.hasFocus) {
      if (name === 'keydown' && evt.key === 'F2' && !evt.shiftKey) {
        this.setCellSelection(this.getContentCellMeta({ row: 0, col: 0 }, true));
        this.hasFocus = true;
      }
      return;
    }
    if (name === 'mousedown') {
      this.selectCell(evt);
    }
    if (name === 'mouseup') {
      this.onMouseUp(evt);
    }
    if (name === 'keydown') {
      this.onKeyDown(evt);
    }
    if (name === 'keyup') {
      if (evt.key === 'Control') {
        this.ctrlDown = false;
      }
      if (evt.key === 'Shift') {
        this.shiftDown = false;
        this.unshift = false;
      }
      if (this.editing) return;
      if (
        this.cellSelection.from &&
        [
          'Home',
          'End',
          'ArrowLeft',
          'ArrowRight',
          'ArrowDown',
          'ArrowUp',
          'Enter',
          'Tab',
          'PageUp',
          'PageDown',
        ].indexOf(evt.key) > -1
      ) {
        GridBase.GridEvents.emit(GridBase.GridEvents.ON_CELL_SELECTED, {
          rightClick: GridBase.Events.rightClick,
          selection: this.cellSelection,
          event: evt,
        });
      }
    }
  };

  private onKeyDown(evt: KeyboardEvent): void {
    // ctrl is down
    if (evt.key === 'Control') {
      this.ctrlDown = true;
      return;
    }

    // shift
    if (evt.key === 'Shift') {
      this.shiftDown = true;
    }

    const moved = this.onCtrlNavigate(evt.key);
    if (moved) {
      evt.preventDefault();
      return;
    }

    // home, end
    if (['Home', 'End'].indexOf(evt.key) > -1 && this.cellSelection.from) {
      if (this.editing) return;
      // this.selectNextCell(this.shiftDown ? this.cellSelection.to : this.cellSelection.from, direction);
      let offset = GridBase.Settings.headings ? 1 : 0;
      this.setCellSelection(
        this.getContentCellMeta({ row: this.cellSelection.from.index.row, col: evt.key === 'Home' ? offset : -1 }),
      );
      evt.preventDefault();
      return;
    }

    // arrow keys
    if (['ArrowLeft', 'ArrowUp', 'ArrowRight', 'ArrowDown'].indexOf(evt.key) > -1 && this.cellSelection.from) {
      if (this.editing && this.keyValue !== undefined) {
        this.editing = false;
        this.selectNextCell(
          this.cellSelection.from || this.cellSelection.last || this.getContentCellMeta({ row: 0, col: 0 }),
          evt.key.replace('Arrow', '').toLowerCase(),
        );
        return;
      }
      if (this.editing) {
        return;
      }
      let direction = evt.key.replace('Arrow', '').toLocaleLowerCase();
      this.selectNextCell(this.shiftDown ? this.cellSelection.to : this.cellSelection.from, direction);
      evt.preventDefault();
      return;
    }

    if (['PageUp', 'PageDown'].indexOf(evt.key) > -1 && this.cellSelection.from) {
      if (this.editing) return;
      let direction = evt.key.replace('Page', '').toLocaleLowerCase();
      const offsets: IContentPageOffset = this.getPageOffset(direction);
      this.setOffsets(offsets.row, this.offsets.col, true);
      this.setCellSelection(
        this.getContentCellMeta({
          row: offsets.row + offsets.selection,
          col: this.shiftDown ? this.cellSelection.to.index.col : this.cellSelection.from.index.col,
        }),
      );
      evt.preventDefault();
      return;
    }

    // enter
    if (evt.key === 'Enter') {
      // if (!GridBase.Settings.alwaysEditing) {
      this.editing = false;
      // }
      this.unshift = true;
      this.selectNextCell(
        this.cellSelection.from || this.cellSelection.last || this.getContentCellMeta({ row: 0, col: 0 }),
        this.shiftDown ? 'up' : 'down',
      );
      return;
    }

    // tab
    if (evt.key === 'Tab' && this.cellSelection.from) {
      // if (!GridBase.Settings.alwaysEditing && !GridBase.Settings.touch) {
      this.editing = false;
      // }
      this.unshift = true;
      this.selectNextCell(this.cellSelection.from, this.shiftDown ? 'left' : 'right');
      evt.preventDefault();
      return;
    }

    if (
      !this.editing &&
      this.cellSelection &&
      this.cellSelection.from &&
      !this.cellSelection.from.heading &&
      !GridBase.Settings.disallowSelection &&
      GridBase.Settings.canEdit
    ) {
      if (evt.key === 'Delete') {
        GridBase.GridEvents.emit(GridBase.GridEvents.ON_DELETE, {
          selection: this.cellSelection,
          event: evt,
        });
        return;
      }

      if (this.cellSelection.from !== this.cellSelection.to) {
        return;
      }

      // enter
      if (evt.key === 'F2' && !evt.shiftKey) {
        // this.setEditCell(this.cellSelection.from);
        this.setCellSelection(this.cellSelection.from || this.getContentCellMeta({ row: 0, col: 0 }, true), 0, true);
        return;
      }

      if (this.cellSelection.from && !this.ctrlDown) {
        if (evt.key.length === 1 && /[a-zA-Z0-9-_ ]/.test(evt.key)) {
          this.keyValue = evt.key;
          // this.input.value = evt.key;
          // this.input.value = evt.key;
        }
        if (evt.key === 'Backspace') {
          this.keyValue = '';
          // this.input.value = "";
        }
        if (this.keyValue !== undefined) {
          this.cellSelection.on = true;
          // this.setEditCell(this.cellSelection.from);
          if (!this.editing) {
            this.setCellSelection(this.cellSelection.from, 0, true);
          }
          this.ctrlDown = false;
        }
      }
    } else if (this.editing) {
      this.dirty = true;
    }
  }

  private onCtrlNavigate(which: string): boolean {
    if (
      this.editing ||
      ['Home', 'End', 'ArrowLeft', 'ArrowRight', 'ArrowDown', 'ArrowUp'].indexOf(which) < 0 ||
      !this.ctrlDown ||
      !this.cellSelection.from ||
      GridBase.Settings.disallowSelection
    ) {
      return false;
    }
    if (which === 'Home') {
      // this.setOffsets(0, 0, true);
      let offset = GridBase.Settings.headings ? -1 : 0;
      this.setCellSelection(
        this.getContentCellMeta(
          { row: this.getFreezeIndex('row') + offset, col: this.getFreezeIndex('col') + offset },
          true,
        ),
      );
      return true;
    }
    if (which === 'End') {
      // this.setOffsets(this.offsets.rowMax, this.offsets.colMax, true);
      this.setCellSelection(this.getContentCellMeta({ row: -1, col: -1 }));
      return true;
    }

    let x = 0;
    let y = 0;
    // look for next empty cell value
    let lookForEmptyValue = true;
    let refCell = this.shiftDown ? this.cellSelection.to : this.cellSelection.from;
    if (which === 'ArrowRight') {
      if (this.shiftDown) {
        this.setCellSelection(this.getContentCellMeta({ row: refCell.index.row, col: -1 }));
        // this.setCellSelection(this.content[refCell.index.row][this.content[0].length - 1]);
        return true;
      }
      for (let colIndex = refCell.index.col + 1; colIndex < this.content[0].length; colIndex++) {
        let cell: IPageContentCell = this.content[refCell.index.row][colIndex];
        if (colIndex === refCell.index.col + 1 && !cell.value) {
          lookForEmptyValue = false;
          continue;
        }
        if (!cell.value && lookForEmptyValue) {
          x = colIndex - 1;
          break;
        }
        if (cell.value && !lookForEmptyValue) {
          x = colIndex;
          break;
        }
      }
      if (!x) {
        x = this.content[0].length - 1;
      }
      this.setCellSelection(this.getContentCellMeta({ row: refCell.index.row, col: x }));
    } else if (which === 'ArrowLeft') {
      if (this.shiftDown) {
        this.setCellSelection(this.getContentCellMeta({ row: refCell.index.row, col: 0 }));
        // this.setCellSelection(this.content[refCell.index.row][0]);
        return true;
      }
      for (let colIndex = refCell.index.col - 1; colIndex >= 0; colIndex--) {
        let cell: IPageContentCell = this.content[refCell.index.row][colIndex];
        if (colIndex === refCell.index.col - 1 && !cell.value) {
          lookForEmptyValue = false;
          continue;
        }
        if (!cell.value && lookForEmptyValue) {
          x = colIndex + 1;
          break;
        }
        if (cell.value && !lookForEmptyValue) {
          x = colIndex;
          break;
        }
      }
      if (!x && GridBase.Settings.headings) {
        x = 1;
      }
      this.setCellSelection(this.getContentCellMeta({ row: refCell.index.row, col: x }));
    } else if (which === 'ArrowDown') {
      if (this.shiftDown) {
        // this.setCellSelection(this.content[this.content.length - 1][refCell.index.col]);
        this.setCellSelection(this.getContentCellMeta({ row: -1, col: refCell.index.col }));
        return true;
      }
      for (let rowIndex = refCell.index.row + 1; rowIndex < this.content.length; rowIndex++) {
        let cell: IPageContentCell = this.content[rowIndex][refCell.index.col];
        if (rowIndex === refCell.index.row + 1 && !cell.value) {
          lookForEmptyValue = false;
          continue;
        }
        if (!cell.value && lookForEmptyValue) {
          y = rowIndex - 1;
          break;
        }
        if (cell.value && !lookForEmptyValue) {
          y = rowIndex;
          break;
        }
      }
      if (!y) {
        y = this.content.length - 1;
      }
      this.setCellSelection(this.getContentCellMeta({ row: y, col: refCell.index.col }));
    } else if (which === 'ArrowUp') {
      if (this.shiftDown) {
        // this.setCellSelection(this.content[0][refCell.index.col]);
        this.setCellSelection(this.getContentCellMeta({ row: 0, col: refCell.index.col }));
        return true;
      }
      for (let rowIndex = refCell.index.row - 1; rowIndex >= 0; rowIndex--) {
        let cell: IPageContentCell = this.content[rowIndex][refCell.index.col];
        if (rowIndex === refCell.index.row - 1 && !cell.value) {
          lookForEmptyValue = false;
          continue;
        }
        if (!cell.value && lookForEmptyValue) {
          y = rowIndex + 1;
          break;
        }
        if (cell.value && !lookForEmptyValue) {
          y = rowIndex;
          break;
        }
      }
      if (!y && GridBase.Settings.headings) y = 1;
      this.setCellSelection(this.getContentCellMeta({ row: y, col: refCell.index.col }));
    }
    return true;
  }

  private onMouseUp(evt: MouseEvent): void {
    this.cellSelection.go = false;
    if (!this.cellSelection.from) {
      return;
    }
    // flipped cell selection
    if (this.cellSelection.flipped) {
      this.cellSelection.from = this.contentMeta[this.cellSelection.flippedFrom.row][
        this.cellSelection.flippedFrom.col
      ];
      this.cellSelection.to = this.contentMeta[this.cellSelection.flippedTo.row][this.cellSelection.flippedTo.col];
      this.cellSelection.flipped = false;
    }
    // buttons
    if (this.cellSelection.from === this.cellSelection.to && !GridBase.Events.rightClick) {
      if (this.cellSelection.from.button.type === 'rotate') {
        const cell: IPageContentCell = this.getContentCell(this.cellSelection.from.index);
        let value = this.helpers.getNextValueInArray(cell.formatted_value, this.cellSelection.from.button.options);
        // this.dirty = true;
        GridBase.GridEvents.emit(GridBase.GridEvents.ON_CELL_VALUE, {
          cell: this.cellSelection.from,
          value: value,
        });
        // GridBase.GridEvents.emit(GridBase.GridEvents.ON_EDITING, {
        //   cell: this.cellSelection.from,
        //   editing: this._editing,
        //   dirty: true,
        // });
        cell.formatted_value = value;
      }
      if (this.cellSelection.from.button.type === 'select') {
        GridBase.Container.showInput();
      }
    }
    // check for heading selection
    if (this.headingSelected) {
      this.selectHeadingCell(evt);
      return;
    }
    this.cellSelection.from = this.scaleCellCoords(this.cellSelection.from);
    this.cellSelection.to = this.scaleCellCoords(this.cellSelection.to);
    if (this.cellSelection.from === this.cellSelection.to) {
      this.cellSelection.multiple = true;
    }
    this.cellSelection.reference = this.getCellsReference();
    GridBase.GridEvents.emit(GridBase.GridEvents.ON_CELL_SELECTED, {
      rightClick: GridBase.Events.rightClick,
      selection: this.cellSelection,
      event: evt,
    });
  }

  private getCellsReference(headingSelected?: string): string {
    return this.helpers.getCellsReference(
      this.cellSelection.from.position,
      this.cellSelection.to.position,
      headingSelected,
    );
  }

  private scaleCellCoords(cell: IContentCellMeta): IContentCellMeta {
    cell.scaledCoords = {
      x: (cell.coords.x * GridBase.Settings.scale) / GridBase.Settings.ratio,
      y: (cell.coords.y * GridBase.Settings.scale) / GridBase.Settings.ratio,
      width: (cell.coords.columnWidth * GridBase.Settings.scale) / GridBase.Settings.ratio,
      height: (cell.coords.height * GridBase.Settings.scale) / GridBase.Settings.ratio,
    };
    return cell;
  }

  private selectHeadingCell(evt: MouseEvent): void {
    // find max row
    let maxRow: number = 0;
    let maxCol: number = 0;
    let indexFromRow: number = 0;
    let indexToRow: number = 0;
    let indexFromCol: number = 0;
    let indexToCol: number = 0;
    for (let rowIndex = this.content.length - 1; rowIndex >= 0; rowIndex--) {
      let row = this.content[rowIndex];
      if (!row) continue;
      if (!maxRow) {
        maxRow = rowIndex;
      }
      let col: IContentCellMeta = this.contentMeta[rowIndex][0];
      if (this.cellSelection.from.index.row === col.index.row) {
        indexFromRow = rowIndex;
      }
      if (this.cellSelection.to.index.row === col.index.row) {
        indexToRow = rowIndex;
      }
    }
    // find max col
    for (let colIndex = this.content[0].length - 1; colIndex >= 0; colIndex--) {
      let col: IContentCellMeta = this.contentMeta[0][colIndex];
      if (!col) continue;
      if (!maxCol) {
        maxCol = colIndex;
      }
      if (this.cellSelection.from.index.col === col.index.col) {
        indexFromCol = colIndex;
      }
      if (this.cellSelection.to.index.col === col.index.col) {
        indexToCol = colIndex;
      }
    }
    if (this.headingSelected === 'all') {
      this.cellSelection.from = this.contentMeta[0][0];
      this.cellSelection.to = this.contentMeta[maxRow][maxCol];
    } else if (this.headingSelected === 'col') {
      this.cellSelection.from = this.contentMeta[0][indexFromCol];
      this.cellSelection.to = this.contentMeta[maxRow][indexToCol];
    } else {
      this.cellSelection.from = this.contentMeta[indexFromRow][0];
      this.cellSelection.to = this.contentMeta[indexToRow][maxCol];
    }
    this.cellSelection.from = this.scaleCellCoords(this.cellSelection.from);
    this.cellSelection.to = this.scaleCellCoords(this.cellSelection.to);
    this.cellSelection.reference = this.getCellsReference(this.headingSelected);
    let n: IHeadingSelection = {
      rightClick: GridBase.Events.rightClick,
      which: this.headingSelected,
      selection: this.cellSelection,
      event: evt,
      count:
        this.headingSelected === 'all'
          ? -1
          : this.cellSelection.to.index[this.headingSelected] - this.cellSelection.from.index[this.headingSelected] + 1,
    };
    GridBase.GridEvents.emit(GridBase.GridEvents.ON_HEADING_CLICKED, n);
    this.headingSelected = '';
  }

  private selectCell(evt: MouseEvent): void {
    this.cellSelection.go = false;
    // check if input
    if (evt.target === GridBase.Container.input) {
      return;
    }

    if (evt.touches) {
      if (evt.touches.length > 1) {
        return;
      }
      evt.clientX = evt.touches[0].pageX;
      evt.clientY = evt.touches[0].pageY;
    }
    let cellMeta: IContentCellMeta = this.getCell(evt.clientX, evt.clientY);
    if (!cellMeta) {
      this.clearCellSelection(true);
      GridBase.GridEvents.emit(GridBase.GridEvents.ON_NO_CELL);
      return;
    }
    if (GridBase.Events.rightClick) {
      GridBase.Events.preventContextMenu = true;
    }
    if (this.editing) {
      this.editing = false;
    }
    if (cellMeta.heading) {
      if (!cellMeta.index.row && !cellMeta.index.col) {
        // this.cellSelection.from = this.content[1][1];
        this.headingSelected = 'all';
      } else if (!cellMeta.index.row) {
        // this.cellSelection.from = this.content[1][cell.index.col];
        this.headingSelected = 'col';
      } else if (!cellMeta.index.col) {
        this.headingSelected = 'row';
        // this.cellSelection.from = this.content[cell.index.row][1];
      }
      if (GridBase.Events.rightClick) {
        if (this.cellSelection.from) {
          if (
            this.headingSelected === 'col' &&
            cellMeta.index.col >= this.cellSelection.from.index.col &&
            cellMeta.index.col <= this.cellSelection.to.index.col
          ) {
            return;
          }
          if (
            this.headingSelected === 'row' &&
            cellMeta.index.row >= this.cellSelection.from.index.row &&
            cellMeta.index.row <= this.cellSelection.to.index.row
          ) {
            return;
          }
        }
      }
      // this.cellSelection.to = undefined;
      // return;
    }
    let d = new Date();
    // double click cell
    if (
      !GridBase.Events.rightClick &&
      this.cellSelection.clicked &&
      this.cellSelection.from === this.cellSelection.to &&
      d.getTime() - this.cellSelection.clicked <= 300 &&
      !cellMeta.button
    ) {
      GridBase.GridEvents.emit(GridBase.GridEvents.ON_CELL_DOUBLE_CLICKED, { cell: cellMeta, event: evt });
      // const cell: IPageContentCell = this.getContentCell(cellMeta.index);
      this.cellSelection.clicked = d.getTime();
      this.keyValue = undefined;
      this.editing = true;
      if (GridBase.Settings.touch) {
        evt.stopPropagation();
        evt.preventDefault();
      }
      return;
    }

    // single click
    if (
      GridBase.Events.rightClick &&
      this.cellSelection.from &&
      cellMeta.index.row >= this.cellSelection.from.index.row &&
      cellMeta.index.col >= this.cellSelection.from.index.col &&
      cellMeta.index.col <= this.cellSelection.to.index.col &&
      cellMeta.index.row <= this.cellSelection.to.index.row
    ) {
      return;
    }
    this.setCellSelection(cellMeta, d.getTime());
    GridBase.GridEvents.emit(GridBase.GridEvents.ON_CELL_MOUSEDOWN, { cell: cellMeta, event: evt });
  }

  private selectNextCell(cell: IContentCellMeta, direction: string = ''): void {
    // check if next cell
    let row: number = cell.index.row;
    let col: number = cell.index.col;

    if (direction === 'left') {
      col = col - 1 < 0 ? 0 : col - 1;
    }
    if (direction === 'up') {
      row = row - 1 < 0 ? 0 : row - 1;
    }
    if (direction === 'down') {
      row = row + 1;
    }
    if (direction === 'right') {
      col = col + 1;
    }
    if (GridBase.Settings.headings) {
      if (!row) row = 1;
      if (!col) col = 1;
    }
    let cellMeta = this.getContentCellMeta({ row, col });
    if (cellMeta) {
      this.setCellSelection(cellMeta);
    }
  }

  private setCellSelection(cellMeta: IContentCellMeta, time?: number, editing: boolean = false): void {
    // TODO if allowed?
    if (editing) {
      if (cellMeta.allow !== 'yes') {
        this.editing = false;
        return;
      } else {
        // this.emitCellChange(this.cellSelection.from);
      }
      if (this.keyValue !== undefined) {
        // cell.formatted_value = cell.value = _.clone(this.keyValue);
        // cell.dirty = true;
        this.dirty = true;
        GridBase.GridEvents.emit(GridBase.GridEvents.ON_CELL_VALUE, {
          cell: this.cellSelection.from,
          value: this.keyValue,
        });
        const cell: IPageContentCell = GridBase.Content.getContentCell(GridBase.Content.cellSelection.from.index);
        cell.formatted_value = this.keyValue;
      }
      // } else {
      //     this.editing = false;
    }
    let cell: IPageContentCell = this.content[cellMeta.index.row][cellMeta.index.col];
    this.cellSelection.clicked = time;
    if (!this.shiftDown || this.unshift) {
      if (this.cellSelection.from) {
        this.cellSelection.last = this.cellSelection.from;
      }
      this.cellSelection.from = cellMeta;
    }
    this.cellSelection.to = cellMeta;
    this.cellSelection.on = false;
    if (editing) {
      this.editing = true;
    }
    if (time) {
      this.cellSelection.go = true;
    }
    if (!GridBase.Events.rightClick) {
      this.cellSelection.multiple = false;
    }
    this.cellSelection.reference = this.getCellsReference();
    // cell reference
    // this.keepSelectorWithinView();
  }

  private clearCellSelection(hasFocus: boolean = false): void {
    if (this.editing) {
      this.editing = false;
    }
    this.cellSelection.last = this.cellSelection.from;
    this.cellSelection.from = undefined;
    this.cellSelection.to = undefined;
    this.cellSelection.go = false;
    this.cellSelection.reference = '';
    this.hasFocus = hasFocus;
  }

  private updateCellSelection(evt: MouseEvent): void {
    let cellMeta = this.getCell(evt.clientX, evt.clientY);
    if (!cellMeta) {
      this.noAccess = false;
      return;
    }
    // console.log(cell.position.row, cell.position.col);
    if (this.cellSelection.go) {
      this.cellSelection.to = cellMeta;
    }
    let cell: IPageContentCell = this.getContentCell(cellMeta.index);
    // console.log(cell.access);
    this.noAccess = cell.access === 'no' || cellMeta.allow === 'no';
    if (this.noAccess) {
      this.classy.addClass(GridBase.Container.container, 'denied');
    } else {
      this.classy.removeClass(GridBase.Container.container, 'denied');
    }
  }
  private flattenStyles(styles) {
    let htmlStyle: any = {};

    this.cellStyles.forEach(s => {
      htmlStyle[s] = styles[s];
    });

    for (let key in this.borders.names) {
      let name: string = this.borders.names[key];
      let width: string = this.borders.widths[styles[`${key}bw`]] + 'px';
      let style: string = styles[`${key}bs`];
      let color: string = '#' + styles[`${key}bc`].replace('#', '');
      if (style === "none") continue;
      htmlStyle[`border-${name}`] = `${width} ${style} ${color}`;
    }

    let str = '';
    for (let attr in htmlStyle) {
      let value = htmlStyle[attr];
      if ((attr === 'background-color' || attr === 'color') && value.indexOf('#') === -1) {
        value = `#${value}`;
      }
      str += `${attr}: ${value}; `;
    }
    return str;
  }
}

export class CellMeta implements IContentCellMeta {
  public allow: string = 'yes';
  public button: boolean = false;
  public hidden: boolean = false;
  public heading: boolean = false;
  public history: boolean = false;
  public link: boolean = false;
  public merge: IContentCellMetaMerge = null;
  public coords: IContentCellMetaCoords = null;
  public index: IContentCellPosition = null;
  public position: IContentCellPosition = null;
  public reference: string = '';
  public wrap: boolean = false;
  _formatting: IPageCellFormatting = null;
  constructor(data?: any) {
    if (!data) {
      return;
    }
    Object.keys(data).forEach(key => {
      let value = data[key];
      if (key === 'formatting') {
        key = '_formatting';
      }
      this[key] = value;
    });
  }
  set formatting(value: IContentCellMetaFormatting) {
    this._formatting = value;
  }
  get formatting(): IContentCellMetaFormatting {
    return this.position ? GridBase.Content.getStyle(this.position) : null;
  }
}
export class CellMetaCoords implements IContentCellMetaCoords {
  public x: number;
  public y: number;
  public width: number;
  public height: number;
  public _columnWidth: number;
  constructor(data?: any) {
    Object.keys(data).forEach(key => {
      let value = data[key];
      if (key === 'columnWidth') {
        key = '_columnWidth';
      }
      this[key] = value;
    });
  }
  set columnWidth(value: number) {
    this._columnWidth = value;
  }
  get columnWidth(): number {
    return !isNaN(this._columnWidth) ? this._columnWidth : this.width;
  }
}

export default Content;
