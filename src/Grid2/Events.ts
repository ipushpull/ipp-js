import * as Hammer from 'hammerjs';
// import * as keypress from 'keypress.js';
import Emitter, { IEmitter } from '../Emitter';
import { GridBase } from './Grid';

// const kpListener = keypress.Listener();

export interface IEvents extends IEmitter {
  rightClick: boolean;
  preventContextMenu: boolean;
  init: () => void;
  destroy: () => void;
}

class Events extends Emitter implements IEvents {
  public rightClick: boolean = false;
  public preventContextMenu: boolean = false;
  public keydown: boolean = false;
  private blur: boolean = false;
  private hammer;
  private resizeTimer: any;
  private freeScroll: any = {
    go: false,
    startX: 0,
    startY: 0,
  };
  private time: number = 0;
  constructor() {
    super();
  }
  public init(): void {
    // need canvas
    let interval: any = setInterval(() => {
      if (!GridBase.Container.container.contains(GridBase.Container.canvas)) {
        return;
      }
      clearInterval(interval);
      interval = undefined;
      document.addEventListener('visibilitychange', this.onVisibilityEvent, false);
      window.addEventListener('focus', this.onFocus, false);
      window.addEventListener('resize', this.onResize);
      window.addEventListener('keydown', this.onKeydown, false);
      window.addEventListener('keyup', this.onKeyup, false);
      window.addEventListener('blur', this.onBlur, false);
      if (GridBase.Settings.touch) {
        this.setupTouch();
        GridBase.Container.container.addEventListener('touchstart', this.onMouseDown, false);
        GridBase.Container.container.addEventListener('touchend', this.onMouseUp, false);
      } else {
        GridBase.Container.container.addEventListener('wheel', this.onWheelEvent, false);
        GridBase.Container.container.addEventListener('mouseover', this.onMouseOver, false);
        GridBase.Container.container.addEventListener('mouseleave', this.onMouseOut, false);
        window.addEventListener('mousemove', this.onMouseMove, false);
        window.addEventListener('mouseup', this.onMouseUp, false);
        window.addEventListener('mousedown', this.onMouseDown, false);
      }
      window.oncontextmenu = () => {
        if (this.preventContextMenu) {
          this.preventContextMenu = false;
          return false;
        }
      };
    }, 50);
  }
  public destroy(): void {
    document.removeEventListener('visibilitychange', this.onVisibilityEvent, false);
    window.removeEventListener('focus', this.onFocus, false);
    window.removeEventListener('reszie', this.onResize);
    window.removeEventListener('keydown', this.onKeydown, false);
    window.removeEventListener('keyup', this.onKeyup, false);
    window.removeEventListener('blur', this.onBlur, false);
    // window.removeEventListener('mouseout', this.onMouseOut, false);
    if (GridBase.Settings.touch) {
      GridBase.Container.container.removeEventListener('touchstart', this.onMouseDown, false);
      GridBase.Container.container.removeEventListener('touchend', this.onMouseUp, false);
      this.setupTouch();
    } else {
      GridBase.Container.container.removeEventListener('wheel', this.onWheelEvent, false);
      GridBase.Container.container.removeEventListener('mouseover', this.onMouseOver, false);
      GridBase.Container.container.removeEventListener('mouseleave', this.onMouseOut, false);
      window.removeEventListener('mousemove', this.onMouseMove, false);
      window.removeEventListener('mouseup', this.onMouseUp, false);
      window.removeEventListener('mousedown', this.onMouseDown, false);
    }
    if (this.hammer) {
      this.hammer.destroy();
    }
  }
  public update(): void {}
  private onVisibilityEvent: any = (evt: any) => {
    GridBase.Content.hidden = document.hidden;
    if (!GridBase.Content.hidden) {
      // requestAnimationFrame(this.drawGrid);
      // this.renderGrid();
      // GridBase.update();
      this.emit('visibility');
    }
  };
  private onFocus: any = (evt: any) => {
    console.warn('focus/blur', document.hasFocus());
  };
  private onBlur: any = (evt: any) => {
    this.blur = true;
    console.warn('focus/blur', document.hasFocus());
  };
  private setupTouch() {
    this.hammer = new Hammer(GridBase.Container.container);
    let pan: any = new Hammer.Pan();
    // let tap: any = new Hammer.Press();
    // let doubleTap: any = new Hammer.Tap({ event: "doubletap", taps: 2 });
    // doubleTap.recognizeWith(tap);
    let pinch: any = new Hammer.Pinch();
    this.hammer.add([pan, pinch]);

    let currentScale = (GridBase.Settings.scale / GridBase.Settings.ratio) * 1;
    this.hammer.on('pinchstart', evt => {
      // this.fit = "scroll";
      GridBase.Settings.fit = 'scroll';
      currentScale = (GridBase.Settings.scale / GridBase.Settings.ratio) * 1;
      GridBase.Content.resizing = true;
    });
    this.hammer.on('pinchmove', evt => {
      GridBase.Content.setScale(currentScale * evt.scale);
      // this.updateSelector();
      GridBase.Content.update();
      GridBase.Canvas.update();
      GridBase.Container.update();
      // GridBase.update();
    });
    this.hammer.on('pinchend', evt => {
      currentScale = (GridBase.Settings.scale / GridBase.Settings.ratio) * 1;
      GridBase.Content.resizing = false;
    });

    let currentOffsets = {
      x: 0,
      y: 0,
    };

    this.hammer.on('panstart', evt => {
      currentOffsets = {
        x: evt.center.x,
        y: evt.center.y,
      };
    });
    this.hammer.on('panmove', evt => {
      // console.log(evt, evt.deltaY, currentOffsets.y);
      let moveBy = Math.round(GridBase.Settings.ratio / GridBase.Settings.scale);
      if (moveBy < 1) moveBy = 1;

      let x = 0;
      if (Math.abs(evt.center.x - currentOffsets.x) > 5 * GridBase.Settings.scale) {
        let directionX = evt.velocityX > 0 ? -1 : 1;
        currentOffsets.x = evt.center.x;
        x = moveBy * directionX;
      }
      let y = 0;
      if (Math.abs(evt.center.y - currentOffsets.y) > 5 * GridBase.Settings.scale) {
        let directionY = evt.velocityY > 0 ? -1 : 1;
        currentOffsets.y = evt.center.y;
        y = moveBy * directionY;
      }

      GridBase.Content.setOffsets(y, x);
      // console.log(this.offsets.y.index);
      // this.updateSelector();
      // this.drawGrid();
      GridBase.Container.update();
      GridBase.Canvas.update();
    });
    this.hammer.on('panend', evt => {});
  }

  private onResize = (evt: Event) => {
    if (this.resizeTimer) {
      clearTimeout(this.resizeTimer);
      this.resizeTimer = null;
    }
    this.resizeTimer = setTimeout(() => {
      // GridBase.Content.setFit(GridBase.Settings.fit);
      this.emit('resize');
    }, 300);
  };
  private step = () => {
    if (!GridBase.Content.editing && !GridBase.Settings.touch) {
      GridBase.Content.keepSelectorWithinView();
      GridBase.Canvas.update();
      GridBase.Container.update();
    }
    // window.requestAnimationFrame(this.step);
    this.keydown = false;
  }
  private onKeydown = (evt: Event) => {
    if (this.keydown) return;
    this.keydown = true;
    this.emit('keydown', evt);
    window.requestAnimationFrame(this.step);
    evt.stopPropagation();
  };
  private onKeyup = (evt: Event) => {
    console.log('onKeyUp', evt);
    // this.keydown = false;
    this.time = 0;
    this.emit('keyup', evt);
    if (GridBase.Content.editing) return;
    GridBase.Content.keepSelectorWithinView();
    GridBase.Canvas.update();
    GridBase.Container.update();
  };
  private onMouseOut = (evt: Event) => {
    console.warn('onMouseOut');
    // GridBase.Content.hasFocus = false;
    // GridBase.Content.ctrlDown = false;
    // GridBase.Content.shiftDown = false;
    // GridBase.Content.cellSelection.go = false;
  }
  private onMouseDown = (evt: Event) => {
    // console.log('onMouseDown', evt);
    let timer = 0;
    // hack for touch devices
    // if (evt.target.className.indexOf("-link") > -1) {
    //     if (GridBase.Settings.touch) {
    //         timer = 300;
    //     }
    // }
    // setTimeout(() => {
    GridBase.Content.hasFocus = GridBase.Container.hasFocus(evt);
    GridBase.Content.ctrlDown = false;
    GridBase.Content.shiftDown = false;
    this.rightClick = this.isRightClick(evt);
    this.emit('mousedown', evt);
    console.log('onMouseDown', evt);
    // GridBase.Content.keepSelectorWithinView();
    GridBase.Canvas.update();
    console.log('focus', GridBase.Content.hasFocus, 'go', GridBase.Content.cellSelection.go);
    // }, timer);
    GridBase.Container.update();
    if (GridBase.Content.hasFocus && evt.target !== GridBase.Container.input) {
      if (!this.blur) evt.preventDefault();
    }
    this.blur = false;
  };
  private onMouseOver = (evt: Event) => {
    this.emit('mouseover', evt);
    if (!GridBase.Content.content.length) return;
    GridBase.Canvas.update();
    GridBase.Container.update();
  };
  private onMouseUp = (evt: MouseEvent) => {
    // console.log('onMouseUp', evt);
    this.emit('mouseup', evt);
    // middle click
    if (evt.which === 2) {
      if (this.freeScroll.go) {
        this.freeScroll.go = false;
      } else {
        this.freeScroll = {
          go: true,
          startX: evt.clientX,
          startY: evt.clientY,
        };
      }
    }
    // GridBase.Content.keepSelectorWithinView();
    GridBase.Container.update();
    GridBase.Canvas.update();
    this.rightClick = false;
    // this.preventContextMenu = false;
  };
  private onMouseMove = (evt: Event) => {
    this.emit('mousemove', evt);
    // GridBase.Content.move();
    if (this.freeScroll.go) {
      let x: number = Math.round((evt.clientX - this.freeScroll.startX) / 20);
      let y: number = Math.round((evt.clientY - this.freeScroll.startY) / 20);
      GridBase.Content.setOffsets(y, x);
      GridBase.Container.update();
      GridBase.Canvas.update();
    } else if (GridBase.Content.scrolling || GridBase.Content.resizing) {
      // console.log('resizing');
      GridBase.Container.update();
      GridBase.Canvas.update();
    } else if (GridBase.Content.cellSelection.go) {
      GridBase.Content.keepSelectorWithinView();
      GridBase.Container.update();
      GridBase.Canvas.update();
    }
  };
  private onWheelEvent = (evt: WheelEvent) => {
    if (!GridBase.Content.content.length) {
      return;
    }
    // current offets
    const currentOffsets = {...GridBase.Content.offsets};
    let offsetX = evt.deltaX ? (Math.abs(evt.deltaX) >= 100 ? Math.round(Math.abs(evt.deltaX) / 100) : 1) : 0;
    let offsetY = evt.deltaY ? (Math.abs(evt.deltaY) >= 100 ? Math.round(Math.abs(evt.deltaY) / 100) : 1) : 0;
    let directionX = evt.deltaX > 0 ? 1 : -1;
    let directionY = evt.deltaY > 0 ? 1 : -1;
    offsetX *= directionX;
    offsetY *= directionY;
    if (!GridBase.Content.offsets.rowMax) {
      offsetX = offsetY;
    }
    GridBase.Content.setOffsets(offsetY, offsetX);
    GridBase.Canvas.update();
    GridBase.Container.update();
    if (currentOffsets.row !== GridBase.Content.offsets.row || currentOffsets.col !== GridBase.Content.offsets.col) {
      evt.preventDefault();
    }
  };
  private isRightClick(evt: any): boolean {
    let isRightMB;
    if ('which' in evt)
      // Gecko (Firefox), WebKit (Safari/Chrome) & Opera
      isRightMB = evt.which === 3;
    else if ('button' in evt)
      // IE, Opera
      isRightMB = evt.button === 2;
    return isRightMB;
  }
}

export default Events;
