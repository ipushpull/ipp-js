import Emitter from '../Emitter';
import Settings, { ISettings } from './Settings';
import Content, { IContent, IContentFind, IContentCellMeta } from './Content';
import Container from './Container';
import Canvas from './Canvas';
import Events, { IEvents } from './Events';
import { IContainer } from './Container';
import { ICanvas } from './Canvas';
import GridEvents from './GridEvents';

interface IBase {
  Container: IContainer;
  Settings: ISettings;
  Events: IEvents;
  Canvas: ICanvas;
  Content: IContent;
  GridEvents: any;
}

export let GridBase: IBase = {
  Container: null,
  Settings: null,
  Events: null,
  Canvas: null,
  Content: null,
  GridEvents: null,
};

export interface IGrid {
  Container: IContainer;
  Settings: ISettings;
  Events: IEvents;
  Canvas: ICanvas;
  Content: IContent;
  GridEvents: any;

  init: (id: string, options?: any) => void;
  destroy: () => void;
  render: (data, ranges?) => void;
  setFit(value: string): void;
  cellHistory(data): void;
  cellHighlights(data): void;
  setRanges(data): void;
  //   setMergeRanges(data): void;
  //   setSorting(data): void;
  //   setFilters(data): void;
  //   clearSorting(): void;
  //   clearFilters(): void;
  setOption(key: string, value: any): void;
  find(value: IContentFind): void;
  clearFound(): void;
  getContentHtml(): string;
  getCell(row: number, col: number): any;
  // getCells(fromCell: any, toCell: any): any;
  setSelector(from: any, to?: any): void;
  setSelectorByRef(ref: string): void;
  hideSelector(): void;
  setContent(content: any, ranges?: any, userId?: number);
  setDeltaContent(content: any);
  setOffsets(row: number, col: number);
  update();
}

export class Grid extends Emitter implements IGrid {
  public Container: IContainer;
  public Settings: ISettings;
  public Events: IEvents;
  public Canvas: ICanvas;
  public Content: IContent;
  public GridEvents: any;

  constructor(id: string, options?: any) {
    super();
    this.init(id, options);
  }
  public init(id: string, options?: any) {
    GridBase = {
      Content: new Content(),
      Container: new Container(),
      Settings: new Settings(),
      Events: new Events(),
      Canvas: new Canvas(),
      GridEvents: new GridEvents(),
    };

    Object.keys(GridBase).forEach(key => {
      this[key] = GridBase[key];
    });

    GridBase.Settings.init(options);
    GridBase.Content.init();
    GridBase.Container.init(id);
    GridBase.Events.init();
    GridBase.Events.register(this.onEvent);
    GridBase.GridEvents.register(this.onGridEvent);
  }
  public setContent(content: any, ranges: any, userId: number = undefined, diff: boolean = false) {
    // if (!diff) GridBase.Content.resizing = true;
    let ref: string = GridBase.Content.cellSelection.reference;
    GridBase.Container.removeAllLinks();
    GridBase.Content.setContent(content, ranges, userId);
    this.update();
    if (ref) this.setSelectorByRef(ref);
    // if (!diff) GridBase.Content.resizing = false;
  }
  public setDeltaContent(content: any) {
    GridBase.Content.setDeltaContent(content);
    if (GridBase.Settings.highlights) {
      this.update();
    } else {
      this.render();
    }
  }
  public setOffsets(row, col) {
    GridBase.Content.setOffsets(row, col);
  }
  public cellHistory(data): void {
    GridBase.Content.cellHistory(data);
  }
  public cellHighlights(data): void {
    GridBase.Content.cellHighlights(data);
    // GridBase.Container.update();
  }
  public setRanges(data): void {
    GridBase.Content.setRanges(data);
  }
  public update() {
    GridBase.Container.setSize();
    GridBase.Content.update();
    GridBase.Canvas.update();
    GridBase.Container.update();
  }
  public render() {
    GridBase.Canvas.render();
    GridBase.Container.update();
  }
  public destroy() {
    GridBase.Container.destroy();
    GridBase.Content.destroy();
    GridBase.Canvas.destroy();
    GridBase.Events.unRegister(this.onEvent);
    GridBase.GridEvents.unRegister(this.onGridEvent);
    GridBase.Events.destroy();
  }
  public setOption(key: string, value: any): void {
    GridBase.Settings.set(key, value);
    if (key === 'headings') {
      // Content.updateContent();
    }
    // GridBase.Content.setSelector();
  }
  public setFit(value: string): void {
    // this.update(value);
    GridBase.Settings.set('fit', value);
    this.update();
  }
  public find(value: IContentFind): void {
    GridBase.Content.find(value);
  }
  public clearFound(): void {
    GridBase.Content.clearFound();
  }
  public getCellMeta(row: number, col: number): IContentCellMeta {
    return GridBase.Content.getContentCellMeta({ row, col });
  }
  public setSelector(from: any, to?: any): void {
    GridBase.Content.setSelector(from, to);
    GridBase.Container.update();
    GridBase.Canvas.update();
  }
  public setSelectorByRef(ref: string): void {
    GridBase.Content.setSelectorByRef(ref);
    GridBase.Container.update();
    GridBase.Canvas.update();
  }
  public hideSelector(): void {
    GridBase.Content.setSelector();
    GridBase.Container.update();
    GridBase.Canvas.update();
  }
  public setHiddenColumns(data): void {
    this.hideSelector();
    GridBase.Content.setHiddenColumns(data);
  }
  public setSorting(data): void {
    this.hideSelector();
    GridBase.Content.setSorting(data);
  }
  public setFilters(data): void {
    this.hideSelector();
    GridBase.Content.setFilters(data);
  }
  public clearSorting(): void {
    this.hideSelector();
    GridBase.Content.clearSorting();
  }
  public clearFilters(): void {
    this.hideSelector();
    GridBase.Content.clearFilters();
  }
  private onEvent = (name, value) => {
    if (name === 'visibility' || name === 'resize') {
      this.update();
    }
  };
  private onGridEvent = (name, value) => {
    this.emit(name, value);
  };
}

// export interface IBase {
//   init: () => void;
//   destroy: () => void;
//   update: () => void;
// }

// export class Base {}
