import Emitter from '../Emitter';

class GridEvents extends Emitter {
  constructor() {
    super();
  }
  public get ON_CELL_CLICKED(): string {
    return 'cell_clicked';
  }
  public get ON_NO_CELL(): string {
    return 'no_cell';
  }
  public get ON_CELL_CLICKED_RIGHT(): string {
    return 'cell_clicked_right';
  }
  public get ON_CELL_HISTORY_CLICKED(): string {
    return 'cell_history_clicked';
  }
  public get ON_CELL_LINK_CLICKED(): string {
    return 'cell_link_clicked';
  }
  public get ON_CELL_DOUBLE_CLICKED(): string {
    return 'cell_double_clicked';
  }
  public get ON_TAG_CLICKED(): string {
    return 'tag_clicked';
  }
  public get ON_CELL_VALUE(): string {
    return 'cell_value';
  }
  public get ON_CELL_SELECTED(): string {
    return 'cell_selected';
  }
  public get ON_CELL_MOUSEDOWN(): string {
    return 'cell_mousedown';
  }
  public get ON_CELL_RESET(): string {
    return 'cell_reset';
  }
  public get ON_EDITING(): string {
    return 'editing';
  }
  public get ON_RESIZE(): string {
    return 'resize';
  }
  public get ON_HEADING_CLICKED(): string {
    return 'heading_clicked';
  }
  public get ON_DELETE(): string {
    return 'delete';
  }
}

export default GridEvents;
