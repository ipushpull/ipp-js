import { GridBase } from './Grid';
import { IContentCellMeta } from './Content';

export interface IGridLine {
  moving: boolean;
  destroy(): void;
}

export class GridLine implements IGridLine {
  public moving: boolean = false;
  public innerLine: HTMLElement;
  public startX: number;
  public offsetX: number;
  public startY: number;
  public offsetY: number;
  public zIndex: number;
  private emitTimeout;
  private width: number = 0;
  private height: number = 0;
  constructor(public element: HTMLElement, public axis: string, public cell: IContentCellMeta) {
    this.innerLine = element.getElementsByClassName('inner-line')[0];
    if (!this.innerLine) {
      return;
    }
    // Events.register(this.onEvents);
    this.innerLine.addEventListener('mousedown', this.onMouseDown);
    window.addEventListener('mouseup', this.onMouseUp);
    window.addEventListener('mousemove', this.onMouseMove);
    this.width = cell.coords.width * 1;
    this.height = cell.coords.height * 1;
  }
  public destroy(): void {
    this.innerLine.removeEventListener('mousedown', this.onMouseDown);
    window.removeEventListener('mouseup', this.onMouseUp);
    window.removeEventListener('mousemove', this.onMouseMove);
    // Events.unRegister(this.onEvents);
  }
  private onEvents = (name: string, evt?: any) => {
    if (name === 'mousedown') {
      this.onMouseDown(evt);
    }
    if (name === 'mouseup') {
      this.onMouseUp(evt);
    }
    if (name === 'mousemove') {
      this.onMouseMove(evt);
    }
  };
  private onMouseDown = (evt: MouseEvent) => {
    if (evt.target !== this.innerLine) {
      return;
    }
    GridBase.Content.setSelector();
    GridBase.Content.resizing = true;
    this.moving = true;
    this.width = this.cell.coords.width * 1;
    this.height = this.cell.coords.height * 1;
    this.startX = evt.x;
    this.startY = evt.y;
    this.zIndex = this.element.style['z-index'];
    this.element.style['z-index'] = this.zIndex * 1000;
    this.element.classList.add('active');
  };
  private onMouseUp = (evt: MouseEvent) => {
    if (!this.moving) {
      return;
    }
    GridBase.Content.resizing = false;
    this.moving = false;
    this.element.style['z-index'] = this.zIndex / 1000;
    this.element.classList.remove('active');
    this.emitSize();
  };
  private onMouseMove = (evt: MouseEvent) => {
    if (!this.moving) {
      return;
    }
    this.offsetX = evt.x - this.startX;
    this.offsetY = evt.y - this.startY;
    let width = this.width + this.offsetX;
    if (width < 20) width = 20;
    let height = this.height + this.offsetY;
    if (height < 20) height = 20;
    if (this.axis === 'col') {
      GridBase.Content.setColSize(this.cell, width);
    } else {
      GridBase.Content.setRowSize(this.cell, height);
    }
    // console.log(this.width, width, GridBase.Content.cells[0][0].style.width);
  };
  private emitSize(): void {
    GridBase.GridEvents.emit(GridBase.GridEvents.ON_RESIZE, { axis: this.axis, x: this.offsetX, y: this.offsetY, cell: this.cell });
  }
}
