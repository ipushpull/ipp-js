import { GridBase } from './Grid';
import { Grid } from '../../js/src/Grid/Grid';
import { IContentPageOffset } from './Content';

export interface IScrollbars {
  visible: boolean;
  inset: boolean;
  size: number;
  colAxis: HTMLElement;
  colShow: boolean;
  rowAxis: HTMLElement;
  rowShow: boolean;
  update: () => void;
  destroy: () => void;
}

export class Scrollbars implements IScrollbars {
  public inset: boolean = false;
  public size: number = 8;

  public colAxis: HTMLElement;
  public colAxisHandle: HTMLElement;
  public colShow: boolean = false;

  public rowAxis: HTMLElement;
  public rowAxisHandle: HTMLElement;
  public rowShow: boolean = false;

  private axis: string = '';
  private scrollbars: any = {
    col: {
      axis: 'x',
      pos: 0,
      drag: false,
      dimension: 'width',
      label: 'col',
      anchor: 'left',
      increments: 0,
      offset: 0,
      size: 0,
    },
    row: {
      axis: 'y',
      pos: 0,
      drag: false,
      dimension: 'height',
      label: 'row',
      anchor: 'top',
      increments: 0,
      offset: 0,
      size: 0,
    },
  };

  private _visible: boolean = false;

  constructor() {
    this.colAxis = document.createElement('div');
    this.colAxis.className = 'track track-x';
    this.colAxis.style.display = `none`;
    this.colAxis.style.height = `${this.size}px`;
    this.colAxis.style.position = `absolute`;
    this.colAxis.style.left = `0px`;
    this.colAxis.style.bottom = `0px`;
    this.colAxis.style.right = `8px`;
    this.colAxis.style['z-index'] = `1000`;

    this.colAxisHandle = document.createElement('div');
    this.colAxisHandle.className = 'handle';
    this.colAxisHandle.style.position = `absolute`;
    // this.colAxisHandle.style.height = `${this.size}px`;
    this.colAxisHandle.style.top = `0px`;
    this.colAxisHandle.style.left = `0px`;
    this.colAxisHandle.style.bottom = `0px`;
    this.colAxisHandle.style['z-index'] = 1;
    this.colAxis.appendChild(this.colAxisHandle);

    this.rowAxis = document.createElement('div');
    this.rowAxis.className = 'track track-y';
    this.rowAxis.style.display = `none`;
    this.rowAxis.style.width = `${this.size}px`;
    this.rowAxis.style.position = `absolute`;
    this.rowAxis.style.top = `0px`;
    this.rowAxis.style.bottom = `8px`;
    this.rowAxis.style.right = `0px`;
    this.rowAxis.style['z-index'] = `1000`;

    this.rowAxisHandle = document.createElement('div');
    this.rowAxisHandle.className = 'handle';
    // this.rowAxisHandle.style.width = `${this.size}px`;
    this.rowAxisHandle.style.position = `absolute`;
    this.rowAxisHandle.style.top = `0px`;
    this.rowAxisHandle.style.left = `0px`;
    this.rowAxisHandle.style.right = `0px`;
    this.rowAxisHandle.style['z-index'] = 1;
    this.rowAxis.appendChild(this.rowAxisHandle);

    // GridBase.Events.register(this.onEvents);
    this.colAxis.addEventListener('mouseover', this.onMouseOver);
    this.rowAxis.addEventListener('mouseover', this.onMouseOver);
    this.colAxis.addEventListener('mouseout', this.onMouseOut);
    this.rowAxis.addEventListener('mouseout', this.onMouseOut);

    this.colAxisHandle.addEventListener('mousedown', this.onMouseDown);
    this.rowAxisHandle.addEventListener('mousedown', this.onMouseDown);
    this.colAxis.addEventListener('mousedown', this.onAxisMouseDown);
    this.rowAxis.addEventListener('mousedown', this.onAxisMouseDown);
    window.addEventListener('mousemove', this.onMouseMove);
    window.addEventListener('mouseup', this.onMouseUp);
  }

  get visible(): boolean {
    return this._visible;
  }
  set visible(value: boolean) {
    this._visible = value;
  }

  public update(): void {
    ['row', 'col'].forEach2(axis => {
      if (!GridBase.Content.offsets[`${axis}Max`]) {
        this[`${axis}Axis`].style.display = 'none';
        this[`${axis}AxisHandle`].style.display = 'none';
        this.scrollbars[axis].show = false;
        // GridBase.Content.offsets[axis] = 0;
        // GridBase.Content.offsets[`${axis}To`] = 0;
        return;
      }

      let dimension = this.scrollbars[axis].dimension;
      let canvasSize = GridBase.Container.containerRect[dimension] - 8;

      this.scrollbars[axis].show = true;
      this[`${axis}AxisHandle`].style.display = 'block';

      let length = GridBase.Settings.headings
        ? canvasSize + GridBase.Content.dimensions[dimension]
        : GridBase.Content.dimensions[dimension] - 8;
      let size = canvasSize * (canvasSize / (length * GridBase.Settings.scale));
      this[`${axis}AxisHandle`].style[dimension] = `${size}px`;
      this.scrollbars[axis].size = size;

      let offset = (GridBase.Content.offsets[axis] / GridBase.Content.offsets[`${axis}Max`]) * (canvasSize - size);
      let anchor = this.scrollbars[axis].anchor;
      this[`${axis}AxisHandle`].style[anchor] = `${offset}px`;

      // show = true;
      this[`${axis}Axis`].style.display = 'block';
      // this[`${axis}Axis`].style[dimension] = `${canvasSize}px`;
    });
  }
  public destroy(): void {
    // GridBase.Events.unRegister(this.onEvents);
    this.colAxisHandle.removeEventListener('mousedown', this.onMouseDown);
    this.rowAxisHandle.removeEventListener('mousedown', this.onMouseDown);
    window.removeEventListener('mousemove', this.onMouseMove);
    window.removeEventListener('mouseup', this.onMouseUp);
  }

  // private onEvents = (name: string, args?: any) => {
  //   if (name === 'mousedown') {
  //     // this.onMouseDown(args);
  //   }
  //   if (name === 'mouseup') {
  //     this.onMouseUp(args);
  //   }
  //   if (name === 'mousemove') {
  //     this.onMouseMove(args);
  //   }
  // };

  private onAxisMouseDown = (evt: MouseEvent) => {
    ['row', 'col'].forEach2(axis => {
      if (this[`${axis}Axis`] === evt.target) {
        if (axis === 'row') {
          const handlePos = this.rowAxisHandle.getBoundingClientRect();
          // above or below
          if (evt.y > handlePos.top + handlePos.height) {
            const offsets: IContentPageOffset = GridBase.Content.getPageOffset('down');
            GridBase.Content.setOffsets(offsets.row, GridBase.Content.offsets.col, true);
          } else if (evt.y < handlePos.top) {
            const offsets: IContentPageOffset = GridBase.Content.getPageOffset('up');
            GridBase.Content.setOffsets(offsets.row, GridBase.Content.offsets.col, true);
          }
        } else {
          const handlePos = this.colAxisHandle.getBoundingClientRect();
          if (evt.x > handlePos.left + handlePos.width) {
            const offsets: IContentPageOffset = GridBase.Content.getPageOffset('right');
            GridBase.Content.setOffsets(GridBase.Content.offsets.row, offsets.col, true);
          } else if (evt.x < handlePos.left) {
            const offsets: IContentPageOffset = GridBase.Content.getPageOffset('left');
            GridBase.Content.setOffsets(GridBase.Content.offsets.row, offsets.col, true);
          }
        }
      }
    });
    evt.preventDefault();
  };
  private onMouseOut = (evt: MouseEvent) => {
    ['row', 'col'].forEach(axis => {
      if (!this.scrollbars[axis].drag) {
        this[`${axis}Axis`].classList.remove('active');
      }
    });
  };
  private onMouseOver = (evt: MouseEvent) => {
    ['row', 'col'].forEach(axis => {
      if (this[`${axis}Axis`] === evt.target || this[`${axis}AxisHandle`] === evt.target) {
        this[`${axis}Axis`].classList.add('active');
      }
    });
  };
  private onMouseDown = (evt: MouseEvent) => {
    ['row', 'col'].forEach2(axis => {
      if (this[`${axis}AxisHandle`] === evt.target) {
        GridBase.Content.scrolling = true;
        this.scrollbars[axis].pos = evt[this.scrollbars[axis].axis];
        this.scrollbars[axis].drag = true;
        this.axis = axis;
        let size = GridBase.Container.containerRect[this.scrollbars[axis].dimension] - this.scrollbars[axis].size;
        this.scrollbars[axis].increments = size / GridBase.Content.dimensions[`${this.axis}s`].length;
      }
      this.scrollbars[axis].offset = GridBase.Content.offsets[axis];
    });
    evt.preventDefault();
  };
  private onMouseUp = (evt: MouseEvent) => {
    GridBase.Content.scrolling = false;
    ['row', 'col'].forEach(axis => {
      if (this[`${axis}Axis`] !== evt.target && this[`${axis}AxisHandle`] !== evt.target) {
        this[`${axis}Axis`].classList.remove('active');
      }
      this.scrollbars[axis].drag = false;
    });
  };
  private onMouseMove = (evt: MouseEvent) => {
    if (!GridBase.Content.scrolling) return;
    let offset = {
      row: this.scrollbars.row.offset,
      col: this.scrollbars.col.offset,
    };
    const scrollbar = this.scrollbars[this.axis];
    const pos = evt[scrollbar.axis];
    const diff = Math.round((pos - scrollbar.pos) / scrollbar.increments);
    offset[this.axis] = scrollbar.offset + diff;
    GridBase.Content.setOffsets(offset.row, offset.col, true);
  };
}
