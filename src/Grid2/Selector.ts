import { GridBase } from './Grid';

import { IContentCellMeta, IContentCellMetaCoords, IContentCellPosition, CellMetaCoords } from './Content';

export interface ISelectorButton {
  which: string;
  direction: string;
  action: string;
  element: HTMLElement;
}

export interface ISelector {
  visible: boolean;
  height: number;
  width: number;
  x: number;
  y: number;
  element: HTMLElement;
  update: () => void;
}

export class Selector implements ISelector {
  public element: HTMLElement;
  private _visible: boolean = false;
  private _height: number = 0;
  private _width: number = 0;
  private _x: number = 0;
  private _y: number = 0;
  private buttons: any = {};
  constructor() {
    this.element = document.createElement('div');
    this.element.className = 'selection';
    this.element.style.position = 'absolute';
    this.element.style['z-index'] = 100;
    this.element.style.left = '0px';
    this.element.style.top = '0px';
    this.element.style.display = 'none';
  }
  get visible(): boolean {
    return this._visible;
  }
  set visible(value: boolean) {
    this._visible = value;
  }
  set height(value: number) {
    this._height = value;
  }
  set width(value: number) {
    this._width = value;
  }
  set x(value: number) {
    this._x = value;
  }
  set y(value: number) {
    this._y = value;
  }
  public update(): void {
    // TODO: Freeze
    let show: boolean = GridBase.Content.cellSelection.from ? true : false;
    // if (GridBase.Content.freezeRange.index.row) {
    //     GridBase.Content.offsets.y
    // }
    if (!show || GridBase.Settings.disallowSelection) {
      this.element.style.display = 'none';
      return;
    }
    this.updateSelectorAttributes(
      GridBase.Content.cellSelection.from,
      GridBase.Content.cellSelection.to || GridBase.Content.cellSelection.from,
      this.element,
    );
  }

  public updateSelectorAttributes(cell: IContentCellMeta, cellTo: IContentCellMeta, selector: HTMLElement) {
    let cellCoords: IContentCellMetaCoords = new CellMetaCoords(cell.coords);
    let cellToCoords: IContentCellMetaCoords = new CellMetaCoords(cellTo.coords);

    // flip ya
    let flip: boolean = false;
    let flipFromIndex: IContentCellPosition;
    let flipToIndex: IContentCellPosition;
    if (cellTo.index.col < cell.index.col && cellTo.index.row > cell.index.row) {
      flip = true;
      flipFromIndex = {
        col: cellTo.index.col,
        row: cell.index.row,
      };
      flipToIndex = {
        col: cell.index.col,
        row: cellTo.index.row,
      };
    } else if (cellTo.index.col > cell.index.col && cellTo.index.row < cell.index.row) {
      flip = true;
      flipFromIndex = {
        col: cell.index.col,
        row: cellTo.index.row,
      };
      flipToIndex = {
        col: cellTo.index.col,
        row: cell.index.row,
      };
    } else if (cellTo.index.col < cell.index.col || cellTo.index.row < cell.index.row) {
      flip = true;
      flipFromIndex = {
        col: cellTo.index.col,
        row: cellTo.index.row,
      };
      flipToIndex = {
        col: cell.index.col,
        row: cell.index.row,
      };
    }
    if (flip) {
      cell = GridBase.Content.getContentCellMeta(flipFromIndex);
      cellTo = GridBase.Content.getContentCellMeta(flipToIndex);
      cellCoords = new CellMetaCoords(cell.coords);
      cellToCoords = new CellMetaCoords(cellTo.coords);
    }

    const rowFreeze = GridBase.Content.getFreezeIndex('row');
    const colFreeze = GridBase.Content.getFreezeIndex('col');

    // frozen

    // if (GridBase.Content.cellSelection.go) {

    // } else {
    cellCoords.x -= (cell.index.col < colFreeze ? 0 : GridBase.Content.offsets.colOffset);
    cellCoords.y -= (cell.index.row < rowFreeze ? 0 : GridBase.Content.offsets.rowOffset);

    if (cell.index.col >= colFreeze && cellCoords.x < GridBase.Content.getFreezeSize('col')) {
      cellCoords.x = GridBase.Content.getFreezeSize('col');
    }
    if (cell.index.row >= rowFreeze && cellCoords.y < GridBase.Content.getFreezeSize('row')) {
      cellCoords.y = GridBase.Content.getFreezeSize('row');
    }

    cellToCoords.x -= (cellTo.index.col < colFreeze ? 0 : GridBase.Content.offsets.colOffset);
    cellToCoords.y -= (cellTo.index.row < rowFreeze ? 0 : GridBase.Content.offsets.rowOffset);

    cellCoords.columnWidth = cellToCoords.x - cellCoords.x + cellToCoords.columnWidth;
    cellCoords.height = cellToCoords.y - cellCoords.y + cellToCoords.height;

    // frozen and non frozen cells selected
    if (cell.index.col < colFreeze && cellTo.index.col >= colFreeze && cellCoords.columnWidth < GridBase.Content.getFreezeSize('col')) {
      cellCoords.columnWidth = cellTo.coords.x + cellTo.coords.columnWidth - cell.coords.x - GridBase.Content.offsets.colOffset;
      if (cellCoords.columnWidth < GridBase.Content.getFreezeSize('col') - cell.coords.x) {
        cellCoords.columnWidth = GridBase.Content.getFreezeSize('col') - cell.coords.x;
      }
    }
    if (cell.index.row < rowFreeze && cellTo.index.row >= rowFreeze  && cellCoords.height < GridBase.Content.getFreezeSize('row')) {
      // cellCoords.height = GridBase.Content.getFreezeSize('row') - cell.coords.y;

      cellCoords.height = cellTo.coords.y + cellTo.coords.height - cell.coords.y - GridBase.Content.offsets.rowOffset;
      if (cellCoords.height < GridBase.Content.getFreezeSize('row') - cell.coords.y) {
        cellCoords.height = GridBase.Content.getFreezeSize('row') - cell.coords.y;
      }
    }

    if (cellCoords.columnWidth < 0) {
      cellCoords.columnWidth = 0;
    }

    if (cellCoords.height < 0) {
      cellCoords.height = 0;
    }

    // console.log(cell.index, cellTo.index, GridBase.Content.offsets.colOffset, GridBase.Content.offsets.rowOffset);
    // }

    selector.style['left'] = `${GridBase.Content.scaleDimension(cellCoords.x)}px`;
    selector.style['width'] = `${GridBase.Content.scaleDimension(cellCoords.columnWidth)}px`;
    selector.style['height'] = `${GridBase.Content.scaleDimension(cellCoords.height)}px`;
    selector.style['top'] = `${GridBase.Content.scaleDimension(cellCoords.y)}px`;
    selector.style['display'] = 'block';

  }
}
