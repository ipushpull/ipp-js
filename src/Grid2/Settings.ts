import * as ua from 'ua-parser-js';

interface INoAccessIMages {
  light: string;
  dark: string;
}

export interface ISettings {
  contrast: string;
  tracking: boolean;
  highlights: boolean;
  highlightsShowTime: number;
  highlightsRemoveTime: number;
  touch: boolean;
  pause: boolean;
  headings: boolean;
  canEdit: boolean;
  alwaysEditing: boolean;
  disallowSelection: boolean;
  hasFocus: boolean;
  gridlines: boolean;
  fluid: boolean;
  ratio: number;
  scale: number;
  fit: string;
  scrollbarWidth: number;
  scrollbarInset: boolean;
  userId: number;
  userTrackingColors: string[];
  noAccessImages: INoAccessIMages;
  historyIndicatorSize: number;
  backgroundColor: string;
  init(options?: any);
  set: (key: string, value: any) => void;
  // setScale: (n: number) => void;
  // setFit: (n: string) => void;
}

class Settings implements ISettings {
  public contrast: string = 'light';
  public tracking: boolean = false;
  public highlights: boolean = false;
  public highlightsShowTime: number = 3000;
  public highlightsRemoveTime: number = 1000;
  public touch: boolean = false;
  public pause: boolean = false;
  public headings: boolean = false;
  public canEdit: boolean = true;
  public alwaysEditing: boolean = false;
  public disallowSelection: boolean = false;
  public hasFocus: boolean = false;
  public gridlines: boolean = false;
  public fluid: boolean = false;
  public scrollbarWidth: number = 8;
  public scrollbarInset: boolean = false;
  public scale: number = 1;
  public ratio: number = 1;
  public fit: string = 'scroll';
  public userId: number = 0;
  public userTrackingColors = ['deeppink', 'aqua', 'tomato', 'orange', 'yellow', 'limegreen'];
  public noAccessImages: INoAccessIMages = {
    light: '',
    dark: '',
  };
  public historyIndicatorSize: number = 16;
  public backgroundColor: string = 'transparent';

  constructor() {}

  public init(options: any = {}) {
    this.setOptions(options);
    let parser = new ua();
    let parseResult = parser.getResult();
    this.touch = parseResult.device && (parseResult.device.type === 'tablet' || parseResult.device.type === 'mobile');
    this.ratio = window.devicePixelRatio;
    if (this.touch) {
      this.scrollbarWidth = 0;
    }
  }

  public set(key: string, value: any): void {
    if (this[key] === undefined) {
      return;
    }
    this[key] = value;
  }

  private setOptions(options: any = {}) {
    for (let option in options) {
      if (!options.hasOwnProperty(option) || this[option] === undefined) {
        continue;
      }
      this[option] = options[option];
    }
  }
}

export default Settings;
