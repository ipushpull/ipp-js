import * as uuidV4 from 'uuid/v4';
import { IPage } from './Page/Page';
import { IContentCell } from './Grid/Content';
import { IContentCellPosition } from './Grid2/Content';

export interface IButtonTask {
  actions: any;
  conditions: string[];
}

export interface ICellRange {
  from: IContentCellPosition;
  to: IContentCellPosition;
}

export interface IHelpers {
  getUuid();
  getNextValueInArray(value: string, arr: string[]): string;
  isNumber(value): boolean;
  getRefIndex(str: string, obj?: boolean): any;
  toColumnName(num): string;
  validHex(hex: string): boolean;
  rgbToHex(rgb: string): string;
  componentToHex(c: any): string;
  addEvent(element: HTMLElement, eventName: string, func: any): void;
  removeEvent(element: HTMLElement, eventName: string, func: any): void;
  capitalizeFirstLetter(str: string): string;
  createSlug(str: string): string;
  getContrastYIQ(hexcolor: string): string;
  parseDateExcel(excelTimestamp);
  isValidEmail(str: string): boolean;
  convertTime(timestamp: any): string;
  cellRange(str: string, rows: number, cols: number): ICellRange;
  getCellsReference(from: IContentCellPosition, to: IContentCellPosition, headingSelected?: string): string;
  safeTitle(str: string, replaceWith?: string): string;
  safeXmlChars(str: string): string;
  isTarget(elm: any, otherElm: any): any;
}

class Helpers {
  public letters: string = 'ABCDEFGHIJKLMNOPQRSTUVWXY';

  constructor() {
    return;
  }

  public getUuid() {
    return uuidV4();
  }

  public getNextValueInArray(value: string, arr: string[]): string {
    if (!arr || !arr.length) {
      return value;
    }
    let n: string = value;
    for (let i: number = 0; i < arr.length; i++) {
      if (n !== arr[i]) {
        continue;
      }
      if (!arr[i + 1]) {
        n = arr[0];
      } else {
        n = arr[i + 1];
      }
      break;
    }
    if (n === value) {
      n = arr[0];
    }
    return n;
  }

  public isNumber(value): boolean {
    return !/^[0-9\.\-]+$/.test(value) || isNaN(value) ? false : true;
  }

  public quoteString(value: any, quote: string = '"'): any {
    return this.isNumber(Number) ? parseFloat(value) : `${quote}${value}${quote}`;
  }

  public getCellsReference(from: IContentCellPosition, to: IContentCellPosition, headingSelected?: string): string {
    let colFrom = this.toColumnName(from.col + 1);
    let rowFrom = from.row + 1;
    let colTo = this.toColumnName(to.col + 1);
    let rowTo = to.row + 1;
    if (headingSelected === 'all') {
      return '1:-1';
    } else if (headingSelected === 'col') {
      if (colFrom === colTo) return colFrom;
      return `${colFrom}:${colTo}`;
    } else if (headingSelected === 'row') {
      if (rowFrom === rowTo) return `${rowFrom}`;
      return `${rowFrom}:${rowTo}`;
    }
    if (colFrom === colTo && rowFrom === rowTo) {
      return `${colFrom}${rowFrom}`;
    }
    if (!rowTo) {
      return `${colFrom}${rowFrom}:${colTo}`;
    }
    return `${colFrom}${rowFrom}:${colTo}${rowTo}`;
  }

  public getRefIndex(str: string, obj?: boolean): any {
    str = str.toUpperCase();

    const regex = /[A-Z]/gm;
    let m;
    let col = -1;
    let count = 0;

    while ((m = regex.exec(str)) !== null) {
      // This is necessary to avoid infinite loops with zero-width matches
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }

      if (col < 0) {
        col = 0;
      }

      // The result can be accessed through the `m`-variable.
      m.forEach(match => {
        let i = this.letters.indexOf(match);
        col += i + count;
        count += 26;
      });
    }

    let numbers = str.match(/\d/g);
    let row = numbers ? parseFloat(numbers.join('')) - 1 : -1;

    return obj ? { row, col } : [row, col];
  }

  public toColumnName(num): string {
    for (var ret = '', a = 1, b = 26; (num -= a) >= 0; a = b, b *= 26) {
      ret = String.fromCharCode(parseInt((num % b) / a) + 65) + ret;
    }
    return ret;
  }

  public cellRange(str: string, rows: number, cols: number): ICellRange {
    let [from, to] = str.split(':');

    let fromRow = 0,
      fromCol = 0,
      toRow = 0,
      toCol = 0;

    let isFromNumber = this.isNumber(from);
    let fromCell = this.getRefIndex(from, true);

    if (to) {
      let isToNumber = this.isNumber(to);
      let toCell = this.getRefIndex(to, true);

      // 1:? - multiple rows
      if (isFromNumber) {
        fromRow = parseInt(from) - 1;
        fromCol = 0;
      } else {
        // A1:? - cell range
        // A:? - multiple columns
        fromCol = fromCell.col;
        fromRow = fromCell.row;
        if (fromCell.row < 0) {
          fromRow = 0;
        }
      }

      // ?:2, ?:-1 - to end of row
      if (isToNumber) {
        toRow = parseInt(to) < 0 ? rows - 1 : parseInt(to) - 1;
        toCol = cols - 1;
        // ?:B, ?:B3
      } else {
        toRow = toCell.row < 0 ? -1 : toCell.row;
        toCol = toCell.col < 0 ? -1 : toCell.col;
      }
    } else {
      // 1 - single row
      if (isFromNumber) {
        fromRow = parseInt(from) - 1;
        toRow = fromRow;
        fromCol = 0;
        toCol = cols - 1;
      } else {
        // A1 - single cell
        // A - single column
        fromCol = fromCell.col;
        fromRow = fromCell.row;
        toCol = fromCol;
        if (fromCell.row < 0) {
          fromRow = 0;
          toRow = -1;
        } else {
          toRow = fromRow;
        }
      }
    }

    return {
      from: { row: fromRow, col: fromCol },
      to: { row: toRow, col: toCol },
    };
  }

  public validHex(hex: string): boolean {
    return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(hex);
  }

  public rgbToHex(rgb: string): string {
    rgb = rgb
      .replace('rgba(', '')
      .replace('rgb(', '')
      .replace(')', '');
    let parts: string[] = rgb.split(',');
    if (parts.length === 4) {
      if (parts[3].trim() === '0') {
        return 'FFFFFF';
      }
    }
    return (
      this.componentToHex(parseInt(parts[0], 10)) +
      this.componentToHex(parseInt(parts[1], 10)) +
      this.componentToHex(parseInt(parts[2], 10))
    );
  }

  public componentToHex(c: any): string {
    let hex: string = c.toString(16);
    return hex.length === 1 ? '0' + hex : hex;
  }

  public addEvent(element: HTMLElement, eventName: string, func: any): void {
    if (!element) {
      return;
    }
    if (element.addEventListener) {
      element.addEventListener(eventName, func, false);
      if (eventName === 'click') {
        element.addEventListener('touchstart', func, false);
      }
    } else if (element.attachEvent) {
      element.attachEvent('on' + eventName, func);
    }
  }

  public capitalizeFirstLetter(str: string): string {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  /**
   * Remove event from element
   *
   * @param element
   * @param eventName
   * @param func
   * @returns {any}
   */
  public removeEvent(element: HTMLElement, eventName: string, func: any): void {
    if (element.removeEventListener) {
      element.removeEventListener(eventName, func, false);
      if (eventName === 'click') {
        element.removeEventListener('touchstart', func, false);
      }
    } else if (element.detachEvent) {
      element.detachEvent('on' + eventName, func);
    }
  }

  public isTouch(): boolean {
    return 'ontouchstart' in document.documentElement;
  }

  public clickEvent(): string {
    return this.isTouch() ? 'touchstart' : 'click';
  }

  public openWindow(link: string, target: string = '_blank', params: { [s: string]: string } = {}): Window {
    let paramsStr: string = '';
    for (let key in params) {
      if (!params.hasOwnProperty(key)) {
        continue;
      }

      paramsStr += `${key}=${params[key]},`;
    }
    paramsStr = paramsStr.substring(0, paramsStr.length - 1);

    return window.open(link, target, paramsStr);
  }

  public createSlug(str: string): string {
    return str.split(' ').join('_');
  }

  public getScrollbarWidth() {
    var outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.width = '100px';
    outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = 'scroll';

    // add innerdiv
    var inner = document.createElement('div');
    inner.style.width = '100%';
    outer.appendChild(inner);

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);

    return widthNoScroll - widthWithScroll;
  }

  public parseDateExcel(excelTimestamp) {
    const secondsInDay = 24 * 60 * 60;
    const excelEpoch = new Date(1899, 11, 31);
    const excelEpochAsUnixTimestamp = excelEpoch.getTime();
    const missingLeapYearDay = secondsInDay * 1000;
    const delta = excelEpochAsUnixTimestamp - missingLeapYearDay;
    const excelTimestampAsUnixTimestamp = excelTimestamp * secondsInDay * 1000;
    const parsed = excelTimestampAsUnixTimestamp + delta;
    return isNaN(parsed) ? null : parsed;
  }

  public getContrastYIQ(hexcolor: string): string {
    hexcolor = hexcolor.replace('#', '');
    let r: number = parseInt(hexcolor.substr(0, 2), 16);
    let g: number = parseInt(hexcolor.substr(2, 2), 16);
    let b: number = parseInt(hexcolor.substr(4, 2), 16);
    let yiq: number = (r * 299 + g * 587 + b * 114) / 1000;
    return yiq >= 128 ? 'light' : 'dark';
  }

  public convertTime(timestamp: any): string {
    if (typeof timestamp === 'string') {
      timestamp = new Date(timestamp);
    }
    let hours = timestamp.getHours();
    if (hours < 10) {
      hours = `0${hours}`;
    }
    let minutes = timestamp.getMinutes();
    if (minutes < 10) {
      minutes = `0${minutes}`;
    }
    let seconds = timestamp.getSeconds();
    if (seconds < 10) {
      seconds = `0${seconds}`;
    }
    return `${hours}:${minutes}:${seconds}`;
  }

  public isValidEmail(str: string): boolean {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(str);
  }

  public safeTitle(str: string, replaceWith: string = '_'): string {
    return str.replace(/[^a-zA-Z0-9_\.\-]/g, replaceWith);
  }
  public safeXmlChars(str: string): string {
    const regex = /<[\w](.*?)\/>/gm;
    // const str = `& <mention email="kyle.hengst@ipushpull.com" /> <>`;
    let m;
    let ignore = [];
    while ((m = regex.exec(str)) !== null) {
      if (m.index === regex.lastIndex) {
        regex.lastIndex++;
      }
      ignore.push(m[0]);
    }
    ignore.forEach((s, i) => {
      str = str.replace(s, `[$${i}]`);
    });
    str = str
      // .replace(/\<br \/\>/g, '[BR]')
      .replace(/&/g, '&amp;')
      .replace(/>/g, '&gt;')
      .replace(/</g, '&lt;')
      .replace(/'/g, '&apos;')
      .replace(/"/g, '&quot;');
    // .replace(/\[BR\]/g, '<br />');

    ignore.forEach((s, i) => {
      str = str.replace(`[$${i}]`, s);
    });

    return str;
  }
  public isTarget(target: any, element: any): any {
    let clickedEl = element;
    while (clickedEl && clickedEl !== target) {
      clickedEl = clickedEl.parentNode;
    }
    return clickedEl === target;
  }
}

export default Helpers;
