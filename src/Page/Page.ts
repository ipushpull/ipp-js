import Utils from "../Utils";
import * as merge from "merge";
import * as extend from "extend";
import * as _ from "underscore";

import IPromise = angular.IPromise;
import IDeferred = angular.IDeferred;
import IQService = angular.IQService;
import ITimeoutService = angular.ITimeoutService;
import IIntervalService = angular.IIntervalService;

import EventEmitter, { IEmitter } from "../Emitter";
import { IPageContentProvider } from "./Content";
import { IPageContent } from "./Content";
import { PageContent } from "./Content";
import { IPageProvider } from "./Provider";
import { Providers } from "./Provider";
import { ProviderREST } from "./Provider";
import { ProviderSocket } from "./Provider";
import { IPageRangesCollection, IPageFreezingRange } from "./Range";
import { RangesWrap } from "./Range";
import { Ranges } from "./Range";
import { IEncryptionKey, ICryptoService } from "../Crypto";
import { IApiService } from "../Api";
import { IAuthService } from "../Auth";
import { IStorageService } from "../Storage";
import { IIPPConfig } from "../Config";
import { IPageDelta } from "../Content/Content";

export interface IPageTypes {
    regular: number;
    pageAccessReport: number;
    domainUsageReport: number;
    globalUsageReport: number;
    pageUpdateReport: number;
    alert: number;
    pdf: number;
    liveUsage: number;
}

export interface IPageServiceContent {
    id: number;
    seq_no: number;
    content_modified_timestamp: Date;
    content: IPageContent;
    content_modified_by: any;
    push_interval: number;
    pull_interval: number;
    is_public: boolean;
    description: string;
    encrypted_content: string;
    encryption_key_used: string;
    encryption_type_used: number;
    special_page_type: number;
}

export interface IPageServiceMeta {
    by_name_url: string;
    id: number;
    name: string;
    description: string;
    url: string;
    uuid: string;
    access_rights: string;
    range_access: string;
    action_definitions: string;
    background_color: string;
    content: IPageContent;
    content_modified_by: any;
    content_modified_timestamp: Date;
    created_by: any;
    created_timestamp: Date;
    domain_id: number;
    domain_name: string;
    domain_url: string;
    encrypted_content: string;
    encryption_key_to_use: string;
    encryption_key_used: string;
    encryption_type_to_use: number;
    encryption_type_used: number;
    is_obscured_public: boolean;
    is_public: boolean;
    is_template: boolean;
    modified_by: any;
    modified_timestamp: Date;
    pull_interval: number;
    push_interval: number;
    record_history: boolean;
    seq_no: number;
    show_gridlines: boolean;
    special_page_type: number;
    ws_enabled: boolean;
}

export interface IPage extends IPageServiceMeta {}

// @todo This should be in some domain/folder service
export interface IUserPageDomainCurrentUserAccess {
    default_page_id: number;
    default_page_url: string;
    domain_id: number;
    domain_url: string;
    is_active: boolean;
    is_administrator: boolean;
    is_default_domain: boolean;
    is_pending: boolean;
    page_count: number;
    user_id: number;
    user_url: string;
}

// @todo This should be probably in some domain/folder service
export interface IUserPageDomainAccess {
    alerts_enabled: boolean;
    by_name_url: string;
    current_user_domain_access: IUserPageDomainCurrentUserAccess;
    description: string;
    display_name: string;
    domain_type: number;
    encryption_enabled: boolean;
    id: number;
    is_page_access_mode_selectable: boolean;
    is_paying_customer: boolean;
    login_screen_background_color: "";
    logo_url: string;
    name: string;
    page_access_mode: number;
    page_access_url: string;
    url: string;
    default_symphony_sid: string;
}

export interface IUserPageAccess {
    by_name_url: string;
    content_by_name_url: string;
    content_url: string;
    domain: IUserPageDomainAccess;
    domain_id: number;
    domain_name: string;
    domain_url: string;
    encryption_to_use: number;
    encryption_key_to_use: string;
    id: number;
    is_administrator: boolean;
    is_public: boolean;
    is_users_default_page: boolean;
    name: string;
    pull_interval: number;
    push_interval: number;
    special_page_type: number;
    url: string;
    write_access: boolean;
    ws_enabled: boolean;
}

export interface IPageTemplate {
    by_name_url: string;
    category: string;
    description: string;
    domain_id: number;
    domain_name: string;
    id: number;
    name: string;
    special_page_type: number;
    url: string;
    uuid: string;
}

export interface IPageCloneOptions {
    clone_ranges?: boolean;
}

export interface IPageCopyOptions {
    content?: boolean;
    access_rights?: boolean;
    settings?: boolean;
}

export interface IPageService extends IEmitter {
    TYPE_REGULAR: number;
    TYPE_ALERT: number;
    TYPE_PDF: number;
    TYPE_PAGE_ACCESS_REPORT: number;
    TYPE_DOMAIN_USAGE_REPORT: number;
    TYPE_GLOBAL_USAGE_REPORT: number;
    TYPE_PAGE_UPDATE_REPORT: number;
    TYPE_LIVE_USAGE_REPORT: number;

    EVENT_READY: string;
    EVENT_NEW_CONTENT: string;
    EVENT_NEW_CONTENT_DELTA: string;
    EVENT_NEW_META: string;
    EVENT_RANGES_UPDATED: string;
    EVENT_ACCESS_UPDATED: string;
    EVENT_DECRYPTED: string;
    EVENT_ERROR: string;

    ready: boolean;
    decrypted: boolean;
    updatesOn: boolean;
    types: IPageTypes;

    encryptionKeyPull: IEncryptionKey;
    encryptionKeyPush: IEncryptionKey;

    data: IPage;
    access: IUserPageAccess;
    Content: IPageContentProvider;
    Ranges: IPageRangesCollection;

    freeze: IPageFreezingRange;

    start: () => void;
    stop: () => void;
    push: (forceFull?: boolean) => IPromise<any>;
    saveMeta: (data: any) => IPromise<any>;
    destroy: () => void;
    decrypt: (key: IEncryptionKey) => void;
    clone: (folderId: number, name: string, options?: IPageCloneOptions) => IPromise<IPageService>;
    copy: (folderId: number, name: string, options?: IPageCopyOptions) => IPromise<IPageService>;
}

// Main/public page service
let $q: IQService,
    $timeout: ITimeoutService,
    $interval: IIntervalService,
    api: IApiService,
    auth: IAuthService,
    storage: IStorageService,
    crypto: ICryptoService,
    config: IIPPConfig,
    utils: any,
    providers: any,
    ranges: any;

export class PageWrap {
    public static $inject: string[] = [
        "$q",
        "$timeout",
        "$interval",
        "ippApiService",
        "ippAuthService",
        "ippStorageService",
        "ippCryptoService",
        "ippConfig"
    ];

    constructor(
        q: IQService,
        timeout: ITimeoutService,
        interval: IIntervalService,
        ippApi: IApiService,
        ippAuth: IAuthService,
        ippStorage: IStorageService,
        ippCrypto: ICryptoService,
        ippConf: IIPPConfig
    ) {
        $q = q;
        $timeout = timeout;
        $interval = interval;
        api = ippApi;
        auth = ippAuth;
        storage = ippStorage;
        crypto = ippCrypto;
        config = ippConf;
        utils = new Utils();
        providers = new Providers($q, $timeout, api, auth, storage, config);
        ranges = new RangesWrap($q, api);

        return Page;
    }
}

// export default PageWrap;

class Page extends EventEmitter implements IPageService {
    public get TYPE_REGULAR(): number {
        return 0;
    }
    public get TYPE_ALERT(): number {
        return 5;
    }
    public get TYPE_PDF(): number {
        return 6;
    }
    public get TYPE_PAGE_ACCESS_REPORT(): number {
        return 1001;
    }
    public get TYPE_DOMAIN_USAGE_REPORT(): number {
        return 1002;
    }
    public get TYPE_GLOBAL_USAGE_REPORT(): number {
        return 1003;
    }
    public get TYPE_PAGE_UPDATE_REPORT(): number {
        return 1004;
    }
    public get TYPE_LIVE_USAGE_REPORT(): number {
        return 1007;
    }

    public get EVENT_READY(): string {
        return "ready";
    }
    public get EVENT_DECRYPTED(): string {
        return "decrypted";
    }
    public get EVENT_NEW_CONTENT(): string {
        return "new_content";
    }
    public get EVENT_NEW_CONTENT_DELTA(): string {
        return "new_content_delta";
    }
    public get EVENT_EMPTY_UPDATE(): string {
        return "empty_update";
    }
    public get EVENT_NEW_META(): string {
        return "new_meta";
    }
    public get EVENT_RANGES_UPDATED(): string {
        return "ranges_updated";
    }
    public get EVENT_ACCESS_UPDATED(): string {
        return "access_updated";
    }
    public get EVENT_ERROR(): string {
        return "error";
    }

    /**
     * Indicates when page is ready (both content and settings/meta are loaded)
     * @type {boolean}
     */
    public ready: boolean = false;

    /**
     * Indicates if page is decrypted.
     * @type {boolean}
     */
    public decrypted: boolean = true;

    /**
     * Indicates if page updates are on - page is requesting/receiving new updates
     * @type {boolean}
     */
    public updatesOn: boolean = true; // @todo I dont like this...

    /**
     * Mapping list of page types label - id
     * @type {IPageTypes}
     */
    public types: IPageTypes;

    /**
     * Class for page range manipulation
     * @type {IPageRangesCollection}
     */
    public Ranges: IPageRangesCollection;

    /**
     * Page content provider class
     * @type {IPageContentProvider}
     */
    public Content: IPageContentProvider;

    public freeze: IPageFreezeRange = {
        valid: false,
        row: -1,
        col: -1
    };

    /**
     * Indicates if client supports websockets
     * @type {boolean}
     * @private
     */
    private _supportsWS: boolean = true; // let's be optimistic by default
    private _wsDisabled: boolean = false;
    private _checkAccess: boolean = true;

    /**
     * Object that holds page data provider
     * @type {IPageProvider}
     */
    private _provider: IPageProvider;

    /**
     * Object that holds the setInterval object for requesting page access data
     * @type {number}
     */
    private _accessInterval: IPromise<any>;

    /**
     * Holds page content and page meta together
     * @type {IPage}
     */
    private _data: IPage;

    /**
     * Holds page access values
     * @type {IUserPageAccess}
     */
    private _access: IUserPageAccess;

    private _pageId: number;
    private _folderId: number;
    private _pageName: string;
    private _folderName: string;
    private _uuid: string;

    // Ouch... but what else can I do....
    private _contentLoaded: boolean;
    private _metaLoaded: boolean;
    private _accessLoaded: boolean;
    private _error: boolean = false;

    private _encryptionKeyPull: IEncryptionKey = {
        name: "",
        passphrase: ""
    };
    private _encryptionKeyPush: IEncryptionKey = {
        name: "",
        passphrase: ""
    };

    // private _providers: any;

    /**
     * Creates new page in the system
     *
     * @param folderId
     * @param name
     * @param type
     * @param template
     * @returns {IPromise<IPageService>}
     */
    public static create(
        folderId: number,
        name: string,
        type: number = 0,
        template?: IPageTemplate,
        organization_public: boolean = false
    ): IPromise<IPageService> {
        let q: IDeferred<IPageService> = $q.defer();

        if (template) {
            let page: IPageService = new Page(template.id, template.domain_id);
            page.on(page.EVENT_READY, () => {
                page.clone(folderId, name)
                    .then(q.resolve, q.reject)
                    .finally(() => {
                        page.destroy();
                    });
            });
        } else {
            api.createPage({
                domainId: folderId,
                data: {
                    name: name,
                    special_page_type: type,
                    organization_public: organization_public
                }
            }).then(
                res => {
                    // Start new page
                    // @todo Why ?
                    let page: IPageService = new Page(res.data.id, folderId);
                    page.on(page.EVENT_READY, () => {
                        page.stop();
                        q.resolve(page);
                    });
                    page.on(page.EVENT_ERROR, err => {
                        page.stop();
                        q.reject(err);
                    });
                },
                err => {
                    q.reject(err);
                }
            );
        }

        return q.promise;
    }

    /**
     * Starts new page object
     *
     * @param pageId
     * @param folderId
     */
    constructor(pageId: number | string, folderId: number | string, uuid?: string) {
        super();

        // this._providers = providers;

        // Types
        this.types = {
            regular: this.TYPE_REGULAR,
            pageAccessReport: this.TYPE_PAGE_ACCESS_REPORT,
            domainUsageReport: this.TYPE_DOMAIN_USAGE_REPORT,
            globalUsageReport: this.TYPE_GLOBAL_USAGE_REPORT,
            pageUpdateReport: this.TYPE_PAGE_UPDATE_REPORT,
            alert: this.TYPE_ALERT,
            pdf: this.TYPE_PDF,
            liveUsage: this.TYPE_LIVE_USAGE_REPORT
        };

        // Decide if client can use websockets
        this._supportsWS = "WebSocket" in window || "MozWebSocket" in window;

        // Process page and folder id/name
        this._folderId = !isNaN(+folderId) ? <number>folderId : undefined;
        this._pageId = !isNaN(+pageId) ? <number>pageId : undefined;
        this._folderName = isNaN(+folderId) ? <string>folderId : undefined;
        this._pageName = isNaN(+pageId) ? <string>pageId : undefined;
        this._uuid = uuid;

        if (!this._pageId && !this._uuid) {
            // If we get folder name and page name, first get page id from REST and then continue with sockets - fiddly, but only way around it at the moment
            this.getPageId(this._folderName, this._pageName).then(
                (res: any) => {
                    if (!res.pageId) {
                        this.onPageError({
                            code: 404,
                            message: "Page not found"
                        });
                        return;
                    }
                    this._pageId = res.pageId;
                    this._folderId = res.folderId;
                    this.init();
                },
                err => {
                    this.onPageError(
                        err || {
                            code: 404,
                            message: "Page not found"
                        }
                    );
                }
            );
        } else {
            this.init(this._uuid ? true : false);
        }
    }

    /**
     * Setter for pull encryption key
     * @param key
     */
    public set encryptionKeyPull(key: IEncryptionKey) {
        this._encryptionKeyPull = key;
    }

    /**
     * Setter for push encryption key
     * @param key
     */
    public set encryptionKeyPush(key: IEncryptionKey) {
        this._encryptionKeyPush = key;
    }

    /**
     * Getter for page data
     * @returns {IPage}
     */
    public get data(): IPage {
        return this._data;
    }

    /**
     * Getter for page access
     * @returns {IUserPageAccess}
     */
    public get access(): IUserPageAccess {
        return this._access;
    }

    /**
     * Start page updates
     */
    public start(): void {
        if (!this.updatesOn) {
            this._provider.start();
            this.updatesOn = true;
        }
    }

    /**
     * Stop page updates
     */
    public stop(): void {
        if (this.updatesOn) {
            this._provider.stop();
            this.updatesOn = false;
        }
    }

    /**
     * Push new data to a page. This method accepts either full page content or delta content update
     * @param forceFull
     * @returns {IPromise<any>}
     */
    public push(forceFull: boolean = false, otherRequestData: any = {}): IPromise<any> {
        let q: IDeferred<any> = $q.defer();
        let currentData: any = _.clone(this.Content ? this.Content._original : undefined);
        let delta: boolean = false;

        let onSuccess: any = data => {
            // this.Content.cleanDirty();
            this.Content.dirty = false;
            this.Content.update(this.Content.current); // @todo Ouch!
            // @todo this._data.content has old value
            data = extend({}, this._data, data.data);

            if (this._provider instanceof ProviderREST) {
                this._provider.seqNo = data.seq_no;
            }

            let diff: any = currentData ? utils.comparePageContent(currentData, this.Content.current, true) : false;
            if (diff) {
                data.diff = diff;
            } else {
                data.diff = undefined;
            }
            data._provider = false;

            if (delta) {
                this.emit(this.EVENT_NEW_CONTENT_DELTA, data);
                if (this._provider instanceof ProviderREST) {
                    this._provider.requestOngoing = false;
                }
            } else {
                this.emit(this.EVENT_NEW_CONTENT, data);
            }

            // this.start();

            q.resolve(data);
        };

        if (
            !this._data.encryption_type_to_use &&
            !this._data.encryption_type_used &&
            this.Content.canDoDelta &&
            !forceFull
        ) {
            // this.stop();
            delta = true;
            if (this._provider instanceof ProviderREST) {
                this._provider.requestOngoing = true;
            }
            this.pushDelta(<IPageDelta>this.Content.getDelta(true), otherRequestData).then(onSuccess, q.reject);
        } else {
            this.pushFull(<IPageContent>this.Content.getFull(), otherRequestData).then(onSuccess, q.reject);
        }

        return q.promise;
    }

    /**
     * Save page settings/meta
     * @param data
     * @returns {IPromise<any>}
     */
    public saveMeta(data: any): IPromise<any> {
        let q: IDeferred<any> = $q.defer();
        // Remove access rights (if any)
        // delete data.access_rights;

        // Just a small condition - this seems to be left behind quite often
        if (data.encryption_type_to_use === 0) {
            data.encryption_key_to_use = "";
        }

        // @todo Validation
        api.savePageSettings({
            domainId: this._folderId,
            pageId: this._pageId,
            data: data
        }).then(
            res => {
                // Apply data to current object
                this._data = extend({}, this._data, res.data);
                // this.emit(this.EVENT_NEW_META, res.data); // @todo I am not sure about this...
                q.resolve(res);
            },
            err => {
                q.reject(utils.parseApiError(err, "Could not save page settings"));
            }
        );

        return q.promise;
    }

    /**
     * Sets current page as folders default for current user
     * @returns {IPromise<IRequestResult>}
     */
    public setAsFoldersDefault(): IPromise<any> {
        let q: IDeferred<any> = $q.defer();

        let requestData: any = {
            domainId: this._folderId,
            data: {
                default_page_id: this._pageId
            }
        };

        api.setDomainDefault(requestData).then(res => {
            // @todo probably not a best way to do that, but is there any other option?
            this._access.is_users_default_page = true;
            q.resolve(res);
        }, q.reject);

        return q.promise;
    }

    /**
     * Deletes current page
     * @returns {IPromise<IRequestResult>}
     */
    public del(): IPromise<any> {
        let requestData: any = {
            domainId: this._folderId,
            pageId: this._pageId
        };

        return api.deletePage(requestData);
    }

    /**
     * Check if page is encrypted and decrypt it if it is
     * @param key
     */
    // @todo This is NOT good
    public decrypt(key?: IEncryptionKey): void {
        // @todo Oh lord...
        if (!key) {
            key = this._encryptionKeyPull;
        }

        // Fail silently if we dont have passphrase
        // @todo oh sweet jesus...
        if (this._data.encryption_type_used && !key.passphrase) {
            this.decrypted = false;
            return;
        }

        // Check for encryption and decrypt
        if (this._data.encryption_type_used) {
            if (!crypto) {
                this.emit(this.EVENT_ERROR, new Error(`Encrypted pages not supported`));
                this.decrypted = false;
                return;
            }
            let decrypted: any = crypto.decryptContent(
                {
                    name: key.name,
                    passphrase: key.passphrase
                },
                this._data.encrypted_content
            );

            if (decrypted) {
                this.decrypted = true;
                this._data.content = decrypted;
                this._encryptionKeyPull = key;
            } else {
                this.decrypted = false;
                // @todo I am pretty sure we will want something more specific for decryption than just message
                this.emit(
                    this.EVENT_ERROR,
                    new Error(`Could not decrypt page with key "${key.name}" and passphrase "${key.passphrase}"`)
                );
            }
        } else {
            this.decrypted = true;
        }

        // @todo ouch... should not be here
        if (this.decrypted) {
            if (this.Content) {
                this.Content.update(this._data.content, false, false, this._data.show_gridlines);
            } else {
                this.Content = new PageContent(this._data.content);
            }

            // @todo Emitting Decrypted and New content events will lead to confusion. Eventually you will want to subscribe to both for rendering, so you will have double rendering
            this.emit(this.EVENT_DECRYPTED);
        }
    }

    /**
     * Destroy page object
     */
    public destroy(): void {
        if (this._provider) {
            this._provider.destroy();
        }

        // $interval.cancel(this._accessInterval);
        this.removeEvent();
        this._checkAccess = false;
    }

    /**
     * Clone current page. Clones page content and some settings, can specify more options via options param.
     * @param folderId
     * @param name
     * @param options
     * @deprecated
     * @returns {IPromise<IPageService>}
     */
    public clone(folderId: number, name: string, options: IPageCloneOptions = {}): IPromise<IPageService> {
        let q: IDeferred<IPageService> = $q.defer();

        if (!this.ready) {
            q.reject("Page is not ready");
            return q.promise;
        }

        // Prevent cloning ranges between folders
        // @todo This is done silently at the moment, should it reject the transaction?
        if (options.clone_ranges && this._folderId * 1 !== folderId * 1) {
            options.clone_ranges = false;
        }

        // Create new page
        Page.create(folderId, name, this._data.special_page_type).then(
            (newPage: IPageService) => {
                newPage.Content = this.Content;
                $q.all([newPage.push(true)]).then(res => {
                    if (options.clone_ranges) {
                        api.savePageSettings({
                            domainId: folderId,
                            pageId: newPage.data.id,
                            data: {
                                access_rights: this._data.access_rights,
                                action_definitions: this._data.action_definitions
                            }
                        }).finally(() => {
                            q.resolve(newPage);
                        });
                    } else {
                        q.resolve(newPage);
                    }
                }, q.reject); // @todo Handle properly
            },
            err => {
                q.reject(err);
            }
        );

        return q.promise;
    }

    /**
     * Clone current page. Clones page content and some settings, can specify more options via options param.
     * @param folderId
     * @param name
     * @param copyContent
     * @param data
     * @returns {IPromise<IPageService>}
     */
    public copy(folderId: number, name: string, options: IPageCopyOptions = {}): IPromise<IPageService> {
        let q: IDeferred<IPageService> = $q.defer();

        if (!this.ready) {
            q.reject("Page is not ready");
            return q.promise;
        }

        let data: any = {
            range_access: true,
            action_definitions: true,
            description: true,
            push_interval: true,
            pull_interval: true,
            is_public: true,
            encryption_type_to_use: true,
            encryption_key_to_use: true,
            background_color: true,
            is_obscured_public: true,
            record_history: true,
            symphony_sid: true
        };

        // Prevent cloning ranges between folders
        // @todo This is done silently at the moment, should it reject the transaction?
        if (!options.access_rights || (options.access_rights && this._folderId * 1 !== folderId * 1)) {
            data.range_access = false;
            data.action_definitions = false;
        }

        // Gather settings data
        let settingsData: any = {};
        for (let key in data) {
            if (!data[key] || !this._data[key]) {
                continue;
            }
            settingsData[key] = this._data[key];
        }

        // Create new page
        Page.create(folderId, name, this._data.special_page_type).then(
            (newPage: IPageService) => {
                if (options.content && Object.keys(settingsData).length > 0) {
                    newPage.Content = this.Content;
                    newPage.push(true).then(() => {
                        api.savePageSettings({ domainId: folderId, pageId: newPage.data.id, data: settingsData }).then(
                            () => {
                                q.resolve(newPage);
                            },
                            q.reject
                        );
                    }, q.reject);
                } else if (options.content) {
                    newPage.Content = this.Content;
                    newPage.push(true).then(() => {
                        q.resolve(newPage);
                    }, q.reject);
                } else if (Object.keys(settingsData).length > 0) {
                    api.savePageSettings({ domainId: folderId, pageId: newPage.data.id, data: settingsData }).then(
                        () => {
                            q.resolve(newPage);
                        },
                        q.reject
                    );
                } else {
                    q.resolve(newPage);
                }
            },
            err => {
                q.reject(err);
            }
        );

        return q.promise;
    }

    private createProvider(ignoreWS?: boolean): void {
        if (this._provider) {
            this._provider.destroy();
        }
        this._provider =
            ignoreWS || !this._supportsWS || typeof io === "undefined" || config.transport === "polling"
                ? new ProviderREST(this._pageId, this._folderId, this._uuid)
                : new ProviderSocket(this._pageId, this._folderId);
        this._checkAccess = true;
    }

    /**
     * Actual bootstrap of the page, starts page updates, registeres provider, starts page access updates
     */
    private init(ignoreWS?: boolean): void {
        if (!this._supportsWS || typeof io === "undefined") {
            console.warn("[iPushPull] Cannot use websocket technology as it is not supported or websocket library is not included");
        }

        // Create provider
        this.createProvider(ignoreWS);

        if (this._uuid) {
            this._accessLoaded = true;
            this.registerListeners();
            return;
        }

        // Create Ranges object
        this.Ranges = new Ranges(this._folderId, this._pageId);
        this.Ranges.on(this.Ranges.EVENT_UPDATED, () => {
            this.emit(this.EVENT_RANGES_UPDATED);
        });

        // Start pulling page access
        this.checkPageAccess();
        this.registerListeners();
        this.setFreezeRange();
    }

    private setFreezeRange(): void {
        this.Ranges.ranges.forEach(range => {
            if (range.name === "frozen_rows") {
                this.freeze.row = range.count;
            } else if (range.name === "frozen_cols") {
                this.freeze.col = range.count;
            }
        });
        this.freeze.valid = this.freeze.row > 0 || this.freeze.col > 0;
    }

    private checkPageAccess(): void {
        if (!this._checkAccess) {
            return;
        }
        this.getPageAccess().finally(() => {
            $timeout(() => {
                this.checkPageAccess();
            }, 30000);
        });
    }

    /**
     * In case page is requested with name, get page ID from service
     * @param folderName
     * @param pageName
     * @returns {IPromise<any>}
     */
    private getPageId(folderName: string, pageName: string): IPromise<any> {
        let q: IDeferred<any> = $q.defer();

        // @todo Need specific/lightweight endpoint - before arguing my way through this, I can use page detail (or write my own)

        api.getPageByName({ domainId: folderName, pageId: pageName }).then(
            res => {
                q.resolve({ pageId: res.data.id, folderId: res.data.domain_id, wsEnabled: res.ws_enabled });
            },
            err => {
                // Convert it into socket error
                q.reject(err);
            }
        );

        return q.promise;
    }

    /**
     * Load page access
     * @returns {IPromise<any>}
     */
    private getPageAccess(): IPromise<any> {
        let q: IDeferred<any> = $q.defer();

        api.getPageAccess({
            domainId: this._folderId,
            pageId: this._pageId
        }).then(
            (res: any) => {
                if (res.httpCode && res.httpCode >= 300) {
                    q.reject({
                        code: res.httpCode,
                        message: res.data ? res.data.detail : "Unauthorized access"
                    });
                    return;
                }

                // auth recovery
                if (this._error) {
                    this._error = false;
                    this.emit(this.EVENT_READY, true);
                    return;
                }

                const prevAccess = JSON.stringify(this._access);
                const newAccess = JSON.stringify(res.data);
                this._access = res.data;
                this._accessLoaded = true;
                this.checkReady(); // @todo ouch...

                if (prevAccess !== newAccess) {
                    this.emit(this.EVENT_ACCESS_UPDATED, {
                        before: prevAccess,
                        after: newAccess
                    });
                }

                // @todo duplicated form meta_update listener - remove once api updated
                // Check if this page should be handled by websockets and switch
                if (this._access.ws_enabled && !(this._provider instanceof ProviderSocket)) {
                    if (this._supportsWS && !this._wsDisabled) {
                        this._provider.destroy();
                        console.log("socket", "get");
                        this._provider = new ProviderSocket(this._pageId, this._folderId);

                        this.registerListeners();
                    } else {
                        console.warn("Page should use websockets but cannot switch as client does not support them");
                    }
                }

                q.resolve();
            },
            err => {
                this.onPageError(err);
                // @todo Should we emit something?
                // @todo Handle error?
                q.reject();
            }
        );

        return q.promise;
    }

    /**
     * Register listeners. THese are listeners on events emitted from page providers, which are processed and then re-emitted to public
     */
    private registerListeners(): void {
        // Setup listeners
        this._provider.on("content_update", data => {
            data.special_page_type = this.updatePageType(data.special_page_type);

            let currentData: any = _.clone(this.Content ? this.Content.current : 0);

            this._data = merge(this._data, data);
            this.decrypt();
            this._contentLoaded = true;

            // emit page if ready
            this.checkReady();

            // @todo This should be emitted before decryption probably
            let diff: any = currentData ? utils.comparePageContent(currentData, this.Content.current, true) : false;
            if (diff) {
                this._data.diff = diff;
            } else {
                this._data.diff = undefined;
            }
            this._data._provider = true;
            this.emit(this.EVENT_NEW_CONTENT, this._data);
        });

        this._provider.on("meta_update", data => {
            data.special_page_type = this.updatePageType(data.special_page_type);

            // Remove content fields (should not be here and in the future will not be here)
            delete data.content;
            delete data.encrypted_content;

            // Process ranges

            // Create Ranges object
            if (!this.Ranges) {
                this.Ranges = new Ranges(data.domain_id, data.id);
                this.Ranges.on(this.Ranges.EVENT_UPDATED, () => {
                    this.emit(this.EVENT_RANGES_UPDATED);
                });
                // this.setFreezeRange();
            }
            // @todo this is temporary hack because the way we get all-in-one from rest. This is NOT very reliable check
            // @todo I dont like that we emit this before we extend the data
            let rangesUpdates: boolean = false;
            if (data.range_access && this._data.range_access !== data.range_access) {
                this.Ranges.parse(data.range_access || "[]");
                data.access_rights = JSON.stringify(this.Ranges.ranges);
                // this.emit(this.EVENT_RANGES_UPDATED);
                rangesUpdates = true;
            }
            if (this._data.action_definitions !== data.action_definitions) {
                rangesUpdates = true;
            }

            this._data = extend({}, this._data, data);

            this._metaLoaded = true;
            this.checkReady();

            this.emit(this.EVENT_NEW_META, data);
            if (rangesUpdates) {
                this.emit(this.EVENT_RANGES_UPDATED);
            }

            // Check if this page should be handled by websockets and switch
            if (data.ws_enabled && !(this._provider instanceof ProviderSocket)) {
                if (this._supportsWS && !this._wsDisabled) {
                    this._provider.destroy();
                    this._provider = new ProviderSocket(this._pageId, this._folderId);

                    this.registerListeners();
                } else {
                    console.warn("Page should use websockets but cannot switch as client does not support them");
                }
            }
        });

        this._provider.on("error", this.onPageError);

        this._provider.on("empty_update", () => {
            this.emit(this.EVENT_EMPTY_UPDATE, this._data);
        });
    }

    private onPageError: any = err => {
        if (!err) {
            return;
        }

        err.code = err.httpCode || err.code;
        err.message = err.httpText || err.message || (typeof err.data === "string" ? err.data : "Page not found");

        this.emit(this.EVENT_ERROR, err);

        if (err.code === 404) {
            this.destroy();
        }

        // fallback to REST
        if (err.type === "redirect") {
            this._wsDisabled = true;
            this.init(true);
        } else {
            this._error = true;
        }
    };

    /**
     * Push full content update
     * @param content
     * @returns {IPromise<any>}
     */
    private pushFull(content: IPageContent, otherRequestData: any = {}): IPromise<any> {
        let q: IDeferred<any> = $q.defer();

        // If encrypted
        if (this._data.encryption_type_to_use) {
            if (!this._encryptionKeyPush || this._data.encryption_key_to_use !== this._encryptionKeyPush.name) {
                // @todo Proper error
                q.reject("None or wrong encryption key");
                return q.promise;
            }

            let encrypted: string = this.encrypt(this._encryptionKeyPush, content);

            if (encrypted) {
                this._data.encrypted_content = encrypted;
                this._data.encryption_type_used = 1;
                this._data.encryption_key_used = this._encryptionKeyPush.name;

                // Ehm...
                this._encryptionKeyPull = _.clone(this._encryptionKeyPush);
            } else {
                // @todo proper error
                q.reject("Encryption failed");
                return q.promise;
            }
        } else {
            // @todo: webservice should do this automatically
            this._data.encryption_key_used = "";
            this._data.encryption_type_used = 0;
            this._data.content = content;
        }

        let data: any = {
            content: !this._data.encryption_type_used ? this._data.content : "",
            encrypted_content: this._data.encrypted_content,
            encryption_type_used: this._data.encryption_type_used,
            encryption_key_used: this._data.encryption_key_used
        };
        data = extend({}, data, otherRequestData);

        let requestData: any = {
            domainId: this._folderId,
            pageId: this._pageId,
            data: data
        };

        api.savePageContent(requestData).then((res: any) => {
            // @todo Do we need this? should be probably updated in rest - if not updated rest will load update even though it already has it
            this._data.seq_no = res.data.seq_no;
            q.resolve(res);
        }, q.reject);

        return q.promise;
    }

    /**
     * Push delta content update
     * @param data
     * @returns {IPromise<any>}
     */
    private pushDelta(data: IPageDelta, otherRequestData: any = {}): IPromise<any> {
        let q: IDeferred<any> = $q.defer();

        // @todo Handle empty data/delta
        data = extend({}, data, otherRequestData);
        let requestData: any = {
            domainId: this._folderId,
            pageId: this._pageId,
            data: data
        };

        api.savePageContentDelta(requestData).then(q.resolve, q.reject);

        return q.promise;
    }

    /**
     * Check if page is considered to be ready
     */
    // @todo Not a great logic - When do we consider for a page to actually be ready?
    private checkReady(): void {
        if (this._contentLoaded && this._metaLoaded && this._accessLoaded && !this.ready) {
            this.ready = true;
            this.emit(this.EVENT_READY);
        }
    }

    /**
     * Temporary fix to update page types. This will take any page report types and adds 1000 to them. This way can do easier filtering.
     * @param pageType
     * @returns {number}
     */
    private updatePageType(pageType: number): number {
        if ((pageType > 0 && pageType < 5) || pageType === 7) {
            pageType += 1000;
        }

        return pageType;
    }

    /**
     * Encrypt page content with given key
     * @param key
     * @param content
     * @returns {string}
     */
    private encrypt(key: IEncryptionKey, content: IPageContent): string {
        // @todo: Handle encryption error
        return crypto.encryptContent(key, content);
    }
}
