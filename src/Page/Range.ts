import * as dateFormat from "dateformat";
import EventEmitter from "../Emitter";

// Main/public page service
let $q: IQService, api: IApiService;

export class RangesWrap {
    constructor(q: IQService, ippApi: IApiService) {
        $q = q;
        api = ippApi;
    }
}

export interface IPageRangeRights {
    ro: number[];
    no: number[];
}

export interface IPageRange {
    name: string;
    start: string;
    end: string;
    rights: IPageRangeRights;
    freeze: boolean;
    button: boolean;
    style: boolean;
    notification: boolean;
    access: boolean;
    list: boolean;
}

export interface IPageRangeItem {
    name: string;
    toObject: () => IPageRange;
}

export interface IPagePermissionRange extends IPageRangeItem {
    count: number;
    rowStart: number;
    rowEnd: number;
    colStart: number;
    colEnd: number;

    setPermission: (userId: number, permission?: string) => void;
    getPermission: (userId: number) => string;
}

export interface IPageAccessRange extends IPageRangeItem {
    range: any;
    config: any;
    exp: string;
    permission: string;
    rights: any;
}

export class AccessRange implements IPageAccessRange {
    public exp: string;
    public permission: string;
    public rights: any = {};
    public updateRange: any;
    constructor(public name: string, public range: any, public config: any) {
        this.exp = config.exp;
        this.rights = config.rights;
        this.permission = config.permission;
        if (config.range === "self") {
            this.updateRange = {
                from: "R:C",
                to: "R:C"
            };
        } else {
            this.updateRange = {
                from: config.range.start,
                to: config.range.end
            };
        }
    }
    public toObject(): IPageRange {
        let range: any = {
            name: this.name,
            start: `${this.range.from.row}:${this.range.from.col}`,
            end: `${this.range.to.row}:${this.range.to.col}`,
            exp: this.exp,
            permission: this.permission,
            range: {
                start: `${this.updateRange.from}`,
                end: `${this.updateRange.to}`
            },
            rights: this.rights,
            freeze: false,
            style: false,
            button: false,
            notification: false,
            access: true
        };
        return range;
    }
}

export interface IPageNotificationRange extends IPageRangeItem {
    range: any;
    config: any;
    exp: string;
    message: string;
}

export class NotificationRange implements IPageNotificationRange {
    public exp: string;
    public message: string;
    constructor(public name: string, public range: any, public config: any) {
        this.exp = config.exp;
        this.message = config.message;
    }
    public toObject(): IPageRange {
        let range: any = {
            name: this.name,
            start: `${this.range.from.row}:${this.range.from.col}`,
            end: `${this.range.to.row}:${this.range.to.col}`,
            exp: this.exp,
            message: this.message,
            freeze: false,
            style: false,
            button: false,
            notification: true
        };
        return range;
    }
}

export interface IPageListRange extends IPageRangeItem {
    range: any;
    config: any;
    exp: string;
    message: string;
}

export class ListRange implements IPageListRange {
    public exp: string;
    public message: string;
    constructor(public name: string, public range: any, public config: any) {
        this.exp = config.exp;
        this.message = config.message;
    }
    public toObject(): IPageRange {
        let range: any = {
            name: this.name,
            start: `${this.range.from.row}:${this.range.from.col}`,
            end: `${this.range.to.row}:${this.range.to.col}`,
            exp: this.exp,
            message: this.message,
            freeze: false,
            style: false,
            button: false,
            notification: true
        };
        return range;
    }
}

export interface IPageStyleRange extends IPageRangeItem {
    range: any;
    config: any;
    exp: string;
    formatting: any[];
}

/**
 *  Style Example
 *  {
        "name":"style",
        "start":"0:2",
        "end":"0:2",
        "conditions":[
            {
                "exp":"VALUE>0",
                "style":{
                    "background":"green"
                }
            },
            {
                "exp":"VALUE<0",
                "style":{
                    "background":"red"
                }
            }            
        ],
        "style":true
    }  
 */

export class StyleRange implements IPageStyleRange {
    public exp: string;
    public formatting: any[];
    public rangeExp: boolean = false;
    constructor(public name: string, public range: any, public config: any) {
        this.exp = config.exp;
        this.formatting = config.formatting;
    }
    public toObject(): IPageRange {
        let range: any = {
            name: this.name,
            start: `${this.range.from.row}:${this.range.from.col}`,
            end: `${this.range.to.row}:${this.range.to.col}`,
            exp: this.exp,
            formatting: this.formatting,
            freeze: false,
            button: false,
            style: true
        };
        return range;
    }
    public parseFormat(format: any, row: number, col: number, rowEnd: number = -1, colEnd: number = -1) {
        let start = format.start
            .replace(/R/gi, row)
            .replace(/C/gi, col)
            .split(":");
        let end = format.end
            .replace(/R/gi, row)
            .replace(/C/gi, col)
            .split(":");
        let startRow = parseInt(eval(start[0]), 10);
        let startCol = parseInt(eval(start[1]), 10);
        let endRow = parseInt(eval(end[0]), 10);
        let endCol = parseInt(eval(end[1]), 10);
        return {
            from: {
                row: startRow,
                col: startCol
            },
            to: {
                row: endRow < 0 ? rowEnd : endRow,
                col: endCol < 0 ? colEnd : endCol
            },
            style: format.style
        };
    }
    public match(value: any): boolean {
        let VALUE = parseFloat(value) || value;
        try {
            return eval(this.exp);
        } catch (e) {
            return false;
        }
    }
}

export interface IPageButtonRange extends IPageRangeItem {
    range: any;
    config: any;
    exp: string;
    updateRange: any;
}

/**
 * Button example
 *  {
        "name":"down",
        "start":"1:3",
        "end":"1:3",
        "exp":"VALUE-=10",
        "range":{
            "start":"2:1",
            "end":"2:1"
        },
        "button":true
    },
 */

export class ButtonRange implements IPageButtonRange {
    public exp: string;
    public setters: any;
    public rangeExp: boolean = false;
    public updateRange: any;
    constructor(public name: string, public range: any, public config: any) {
        this.exp = config.exp;
        this.setters = config.setters;
        // this.rangeExp = config.rangeExp;
        if (config.range === "self") {
            this.updateRange = {
                from: "R:C",
                to: "R:C"
            };
        } else {
            this.updateRange = {
                from: config.range.start,
                to: config.range.end
            };
        }
    }
    public run(cells: any, vars: any = {}) {
        let cellUpdates = [];
        cells.forEach2((row, rowIndex) => {
            if (!row) return;
            row.forEach2((col, colIndex) => {
                if (!col) return;
                cellUpdates.push(this.data(rowIndex, colIndex, col, vars));
            });
        });
        return cellUpdates;
    }
    public toObject(): IPageRange {
        let range: any = {
            name: this.name,
            start: `${this.range.from.row}:${this.range.from.col}`,
            end: `${this.range.to.row}:${this.range.to.col}`,
            exp: this.exp,
            setters: this.setters,
            range: {
                start: `${this.updateRange.from}`,
                end: `${this.updateRange.to}`
            },
            rangeExp: this.rangeExp,
            freeze: false,
            style: false,
            button: true
        };
        if (this.config.range === "self") {
            range.range = "self";
        }
        return range;
    }
    public parseRange(pos: any): any {
        let fromRow: string = this.updateRange.from
            .split(":")[0]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        let fromCol: string = this.updateRange.from
            .split(":")[1]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        let toRow: string = this.updateRange.to
            .split(":")[0]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        let toCol: string = this.updateRange.to
            .split(":")[1]
            .replace(/R/gi, pos.row)
            .replace(/C/gi, pos.col);
        return {
            from: {
                row: eval(fromRow),
                col: eval(fromCol)
            },
            to: {
                row: eval(toRow),
                col: eval(toCol)
            }
        };
    }

    public actionRotate(value: string, arr: string[]): string {
        if (!arr || !arr.length) {
            return value;
        }
        let n: string = value;
        for (let i: number = 0; i < arr.length; i++) {
            if (n !== arr[i]) {
                continue;
            }
            if (!arr[i + 1]) {
                n = arr[0];
            } else {
                n = arr[i + 1];
            }
            break;
        }
        if (n === value) {
            n = arr[0];
        }
        return n;
    }

    public actionToday(format: string = "yyyy-mm-dd"): string {
        let now: any = new Date();
        return dateFormat(now, format);
    }

    public actionLink(value: string, url: string = ""): string {
        window.open(url, "_blank");
        return value;
    }

    public actionConcat(separator: string, arr: string[]): string {
        if (typeof arr !== "object") {
            arr = [];
            for (let i = 1; i < arguments.length; i++) {
                arr.push(`${arguments[i]}`);
            }
        }
        return arr.join(separator);
    }

    private data(row: number, col: number, cell: any, vars: any = {}): any {
        let data = {
            value: cell.value,
            formatted_value: cell.formatted_value
        };

        // functions
        vars["=CONCAT"] = "this.actionConcat";
        vars["=ROTATE"] = "this.actionRotate";
        vars["=TODAY"] = "this.actionToday";
        vars["=LINK\\("] = "this.actionLink(VALUE, ";

        let exp: string = `${this.exp}`;
        for (let key in vars) {
            exp = exp.replace(new RegExp(key, "g"), vars[key]);
        }
        let VALUE = !/^[0-9\.\-]+$/.test(cell.value) || isNaN(cell.value) ? cell.value : parseFloat(cell.value);
        try {
            VALUE = eval(exp);
        } catch (e) {
            VALUE = VALUE;
        }
        data.value = VALUE;
        data.formatted_value = VALUE;

        return {
            row: row,
            col: col,
            data: data
        };
    }
}

export interface IPageFreezingRange extends IPageRangeItem {
    subject: string;
    count: number;
}

export class PermissionRange implements IPagePermissionRange {
    // @todo Constants for permissions ro, no etc
    private _permissions: IPageRangeRights = {
        ro: [],
        no: []
    };

    constructor(
        public name: string,
        public rowStart: number = 0,
        public rowEnd: number = 0,
        public colStart: number = 0,
        public colEnd: number = 0,
        permissions?: IPageRangeRights
    ) {
        if (permissions) {
            this._permissions = permissions;
        }
    }

    /**
     * Set permission for a user
     * @param userId
     * @param permission
     */
    public setPermission(userId: number, permission?: string): void {
        // First remove user rom all ranges
        if (this._permissions.ro.indexOf(userId) >= 0) {
            this._permissions.ro.splice(this._permissions.ro.indexOf(userId), 1);
        }

        if (this._permissions.no.indexOf(userId) >= 0) {
            this._permissions.no.splice(this._permissions.no.indexOf(userId), 1);
        }

        if (permission) {
            this._permissions[permission].push(userId);
        }
    }

    /**
     * Get permission for a user
     * @param userId
     * @returns {string}
     */
    public getPermission(userId: number): string {
        let permission: string = "";

        if (this._permissions.ro.indexOf(userId) >= 0) {
            permission = "ro";
        } else if (this._permissions.no.indexOf(userId) >= 0) {
            permission = "no";
        }

        return permission;
    }

    /**
     * Serialize range to final service-accepted object
     * @returns {{name: string, start: string, end: string, rights: IPageRangeRights, freeze: boolean}}
     */
    public toObject(): IPageRange {
        return {
            name: this.name,
            start: `${this.rowStart}:${this.colStart}`,
            end: `${this.rowEnd}:${this.colEnd}`,
            rights: this._permissions,
            freeze: false,
            button: false,
            style: false,
            access: false,
            notification: false,
            list: false
        };
    }
}

export type TFreezeSubject = "rows" | "cols";

export class FreezingRange implements IPageFreezingRange {
    public static get SUBJECT_ROWS(): string {
        return "rows";
    }
    public static get SUBJECT_COLUMNS(): string {
        return "cols";
    }

    constructor(public name: string, public subject: TFreezeSubject = "rows", public count: number = 1) {}

    /**
     * Serialize range to final service-accepted object
     * @returns {{name: string, start: string, end: string, rights: IPageRangeRights, freeze: boolean}}
     */
    public toObject(): IPageRange {
        let range: IPageRange = {
            name: this.name,
            start: "0:0",
            end: "",
            rights: { ro: [], no: [] },
            freeze: true,
            button: false,
            style: false
        };

        if (this.subject === FreezingRange.SUBJECT_ROWS) {
            range.end = `${this.count - 1}:-1`;
        } else {
            range.end = `-1:${this.count - 1}`;
        }

        return range;
    }
}

export interface IPageRangesCollection extends IEventEmitter {
    TYPE_PERMISSION_RANGE: string;
    TYPE_FREEZING_RANGE: string;
    EVENT_UPDATED: string;
    ranges: (IPagePermissionRange | IPageFreezingRange)[];
    setRanges: (ranges: IPageRangeItem[]) => IPageRangesCollection;
    addRange: (range: IPageRangeItem) => IPageRangesCollection;
    removeRange: (rangeName: string) => IPageRangesCollection;
    save: () => IPromise<any>;
    parse: (pageAccessRights: string) => IPageRangeItem[];
}

export class Ranges extends EventEmitter implements IPageRangesCollection {
    /**
     * List of ranges in collection
     * @type {Array}
     * @private
     */
    private _ranges: (
        | IPagePermissionRange
        | IPageFreezingRange
        | IPageStyleRange
        | IPageButtonRange
        | IPageNotificationRange
        | IPageListRange
        | IPageAccessRange)[] = [];

    private _folderId: number;
    private _pageId: number;

    public get TYPE_PERMISSION_RANGE(): string {
        return "permissions";
    }
    public get TYPE_FREEZING_RANGE(): string {
        return "freezing";
    }

    public get EVENT_UPDATED(): string {
        return "updated";
    }

    /**
     * Getter for list of ranges
     * @returns {IPageRangeItem[]}
     */
    public get ranges(): (IPagePermissionRange | IPageFreezingRange)[] {
        return this._ranges;
    }

    constructor(folderId: number, pageId: number, pageAccessRights?: string) {
        super();

        this._folderId = folderId;
        this._pageId = pageId;

        if (pageAccessRights) {
            this.parse(pageAccessRights);
        }
    }

    /**
     * Rewrites current collection of ranges with given ranges
     * @param ranges
     * @returns {ipushpull.Ranges}
     */
    public setRanges(ranges: (IPagePermissionRange | IPageFreezingRange)[]): IPageRangesCollection {
        this._ranges = ranges;

        return this;
    }

    /**
     * Adds range to collection. Also prevents duplicate names
     * @param range
     * @returns {ipushpull.Ranges}
     */
    public addRange(range: IPagePermissionRange | IPageFreezingRange): IPageRangesCollection {
        // Only one range per freezing subject allowed
        if (range instanceof FreezingRange) {
            for (let i: number = 0; i < this._ranges.length; i++) {
                if (this._ranges[i].subject === range.subject) {
                    this.removeRange(this._ranges[i].name);
                    break;
                }
            }
        }

        // Prevent duplicates
        let nameUnique: boolean = false;
        let newName: string = range.name;
        let count: number = 1;

        while (!nameUnique) {
            nameUnique = true;

            for (let i: number = 0; i < this._ranges.length; i++) {
                if (this._ranges[i].name === newName) {
                    nameUnique = false;
                    newName = range.name + "_" + count;
                    count++;
                }
            }
        }

        range.name = newName;

        this._ranges.push(range);

        return this;
    }

    /**
     * Removes range from collection
     *
     * @param rangeName
     * @returns {ipushpull.Ranges}
     */
    public removeRange(rangeName: string): IPageRangesCollection {
        let index: number = -1;

        for (let i: number = 0; i < this._ranges.length; i++) {
            if (this._ranges[i].name === rangeName) {
                index = i;
                break;
            }
        }

        if (index >= 0) {
            this._ranges.splice(index, 1);
        }

        return this;
    }

    /**
     * Save ranges to a page
     * @returns {IPromise<IRequestResult>}
     */
    // @todo This is way out of scope
    public save(): IPromise<any> {
        let q: IDeferred<any> = $q.defer();

        let ranges: IPageRange[] = [];

        // Convert all ranges to objects
        for (let i: number = 0; i < this._ranges.length; i++) {
            ranges.push(this._ranges[i].toObject());
        }

        let requestData: any = {
            domainId: this._folderId,
            pageId: this._pageId,
            data: {
                range_access: ranges.length ? JSON.stringify(ranges) : '',
            }
        };

        api.savePageSettings(requestData).then(data => {
            this.emit(this.EVENT_UPDATED);
            q.resolve(data);
        }, q.reject);

        return q.promise;
    }

    /**
     * Parse range data loaded from service into range collection
     * @param pageAccessRights
     * @returns {IPageRangeItem[]}
     */
    public parse(pageAccessRights: string): IPageRangeItem[] {
        let ar: IPageRange[] = JSON.parse(pageAccessRights);

        this._ranges = [];

        for (let i: number = 0; i < ar.length; i++) {

            if (ar[i].data) {
                continue;
            }

            let range: any = {
                from: {
                    row: parseInt(ar[i].start.split(":")[0], 10),
                    col: parseInt(ar[i].start.split(":")[1], 10)
                },
                to: {
                    row: parseInt(ar[i].end.split(":")[0], 10),
                    col: parseInt(ar[i].end.split(":")[1], 10)
                }
            };
            // let rowStart: number = parseInt(ar[i].start.split(":")[0], 10);
            // let rowEnd: number = parseInt(ar[i].end.split(":")[0], 10);
            // let colStart: number = parseInt(ar[i].start.split(":")[1], 10);
            // let colEnd: number = parseInt(ar[i].end.split(":")[1], 10);
            if (ar[i].style) {
                this._ranges.push(new StyleRange(ar[i].name, range, ar[i]));
            } else if (ar[i].notification) {
                this._ranges.push(new NotificationRange(ar[i].name, range, ar[i]));
            } else if (ar[i].list) {
                this._ranges.push(new ListRange(ar[i].name, range, ar[i]));
            } else if (ar[i].access) {
                this._ranges.push(new AccessRange(ar[i].name, range, ar[i]));
            } else if (ar[i].button) {
                this._ranges.push(new ButtonRange(ar[i].name, range, ar[i]));
            } else if (ar[i].freeze) {
                let subject: TFreezeSubject = range.to.col >= 0 ? "cols" : "rows";
                let count: number = range.to.col >= 0 ? range.to.col + 1 : range.to.row + 1;

                this._ranges.push(new FreezingRange(ar[i].name, subject, count));
            } else {
                this._ranges.push(
                    new PermissionRange(
                        ar[i].name,
                        range.from.row,
                        range.to.row,
                        range.from.col,
                        range.to.col,
                        ar[i].rights
                    )
                );
            }
        }

        return this._ranges;
    }
}
