import * as request from "xhr";

export class Request {
    public static _instance() {
        return request;
    }
}