import Emitter from "./Emitter";
import Classy from "./Classy";
import Helpers from "./Helpers";
import * as _ from "underscore";

class Table extends Emitter {

    public get ON_CELL_CLICKED(): string { return "cell_clicked"; }
    public get ON_CELL_DOUBLE_CLICKED(): string { return "cell_double_clicked"; }
    public get ON_TAG_CLICKED(): string { return "tag_clicked"; }
    public get ON_CELL_CHANGED(): string { return "cell_changed"; }

    public data: any = []; // cell data
    public accessRanges: any = [];
    public initialized: boolean = false;
    public dirty: boolean = false;
    public rendering: boolean = false;
    public tags: boolean = false;
    public isTouch: boolean = false;

    public container: string = "content";
    public containerEl: any;
    public viewEl: any;

    public zoomEl: any;
    public scrollEl: any;

    public tableEl: any;
    public tableHeadEl: any;
    public tableBodyEl: any;
    // public tableFrozenRowsId: string = "pageContentFrozenRows";
    public tableFrozenRowsEl: any;
    // public tableFrozenColsId: string = "pageContentFrozenCols";
    public tableFrozenColsEl: any;
    // private tableRect: any;
    // public tableBoundsId: string = "pageContentBounds";
    // public tableBoundsEl: any;

    private validStyles: any = [
        "color",
        "background",
        "background-color",
        "border",
        "border-left",
        "border-right",
        "border-top",
        "border-bottom",
        "width",
        "height",
        "max-height",
        "text-align",
        "vertical-align",
        "font-family",
        "font-size",
        "font-weight",
        "font-style",
        "text-wrap",
        "text-decoration",
        "number-format",
        "word-wrap",
        "white-space",
    ];
    private _editing: boolean = false;
    private _tracking: boolean = false;
    private numOfCells: number = 0;
    private classy: any;
    private helpers: any;
    private freeze: boolean = false;
    private freezeRange: any = {
        valid: false,
        index: {
            row: 0,
            col: 0,
        }
    };
    private accessRangesClassNames: any = {
        "no": "no-access",
        "ro": "read-only",
    };
    private clicked: number = 0;
    // private scrollBarWidth: number = 0;
    // private _fit: any = ""; // zoom, contain, width, height

    constructor(container: string) {
        super();
        if (container) {
            this.container = container;
        }
        this.classy = new Classy();
        this.helpers = new Helpers();
        this.init();
        // this.scrollBarWidth = this.helpers.getScrollbarWidth();
        return;
    }

    get editing(): boolean {
        return this._editing;
    }

    set editing(value: boolean) {
        this._editing = value;
        if (value) {
            this.classy.addClass(this.containerEl, "edit-mode");
        } else {
            this.classy.removeClass(this.containerEl, "edit-mode");
        }
    }

    get tracking(): boolean {
        return this._tracking;
    }

    set tracking(value: boolean) {
        this._tracking = value;
        this.clearCellClassNames(["changed"]);
    }

    // get fit(): string {
    //     return this._fit;
    // }

    // set fit(value: string) {
    //     if (["", "zoom", "contain", "width", "height"].indexOf(value) > -1) {
    //         this._fit = value;
    //     } else {
    //         this._fit = "";
    //     }
    // }

    public init(): boolean {


        if (this.initialized) {
            return true;
        }

        // main container
        this.containerEl = document.getElementById(this.container);
        if (!this.containerEl) {
            console.error("Could not init table");
            return false;
        }
        // this.containerEl.style.overflow = "hidden";

        // scroll and zoom
        this.viewEl = document.createElement("div");
        this.viewEl.className = "page-content-view";
        this.scrollEl = document.createElement("div");
        this.scrollEl.className = "page-content-scroll";
        this.zoomEl = document.createElement("div");
        this.zoomEl.className = "page-content-zoom";

        // table
        this.tableEl = document.createElement("table");
        this.tableHeadEl = document.createElement("thead");
        this.tableBodyEl = document.createElement("tbody");
        this.tableEl.appendChild(this.tableHeadEl);
        this.tableEl.appendChild(this.tableBodyEl);
        this.tableFrozenRowsEl = document.createElement("table");
        this.tableFrozenRowsEl.className = "frozen-rows";
        this.tableFrozenColsEl = document.createElement("table");
        this.tableFrozenColsEl.className = "frozen-cols";

        // add table to zoom
        this.zoomEl.appendChild(this.tableEl);
        this.zoomEl.appendChild(this.tableFrozenRowsEl);
        this.zoomEl.appendChild(this.tableFrozenColsEl);

        // add zoom to scroll
        this.scrollEl.appendChild(this.zoomEl);
        this.viewEl.appendChild(this.scrollEl);

        // add scroll to container
        this.containerEl.appendChild(this.viewEl);

        // setup events
        this.helpers.addEvent(this.tableEl, "click", this.onTableClick);
        this.helpers.addEvent(this.viewEl, "scroll", this.onWindowScroll);

        this.dirty = false;
        this.initialized = true;

        return true;
    }

    public render(data: any): boolean {
        if (this.rendering) {
            return false;
        }
        this.rendering = true;
        this.setData(data);
        if (!this.data || !this.data[0] || !this.data[0][0] || !this.init()) {
            this.rendering = false;
            return false;
        }
        for (let i: number = 0; i < this.data.length; i++) {
            this.renderRow(-1, this.data[i]);
        }
        this.softMerges();
        // this.setRect();
        // this.updateFit();
        this.rendering = false;
        return true;
    }

    public update(data: any, content: any): boolean {

        if (this.rendering || !this.initialized) {
            return false;
        }

        this.rendering = true;

        this.setData(content);

        // update cells
        for (let i in data) {

            if (!data.hasOwnProperty(i)) { continue; }

            // loop through rows
            if (this.tableBodyEl.rows[i]) {

                // loop through columns
                for (let k in data[i]) {

                    if (!data[i].hasOwnProperty(k)) { continue; }

                    if (this.tableBodyEl.rows[i].cells[k]) {
                        // update cell
                        this.setCellValues(this.tableBodyEl.rows[i].cells[k], data[i][k]);
                    } else {
                        this.setCellValues(
                            this.tableBodyEl.rows[i].insertCell(k),
                            data[i][k]
                        );
                    }

                }

            } else {
                // create new row
                this.renderRow(i, this.data[i]);
            }

            this.softMerges(this.tableBodyEl.rows[i]);
            // this.freezePanes();

        }

        // Remove rows/cells
        while (this.tableBodyEl.rows.length > content.length) {
            this.tableBodyEl.deleteRow(this.tableBodyEl.rows.length - 1);
        }
        if (this.tableBodyEl.rows[0].cells.length > content[0].length) {
            for (let i: number = 0; i < this.tableBodyEl.rows.length; i++) {
                while (this.tableBodyEl.rows[i].cells.length > content[i].length) {
                    this.tableBodyEl.rows[i].deleteCell(this.tableBodyEl.rows[i].cells.length - 1);
                }
                this.softMerges(this.tableBodyEl.rows[i]);
            }
        }

        this.rendering = false;
        // this.setRect();
        // this.updateFit();

        return true;

    }

    public getData(): any {
        return this.data;
    }

    public setData(data: any): void {
        if (!data) {
            data = [];
        }
        this.data = data;
        this.numOfCells = data.length && data[0].length ? data.length * data[0].length : 0;
    }

    public setAccessRanges(rangeAccess: any): void {
        this.setRanges(rangeAccess);
    }

    public setRanges(ranges: any, userId: number = undefined): void {
        this.accessRanges = [];
        this.clearCellClassNames(Object.values(this.accessRangesClassNames));
        this.freezeRange = {
            valid: false,
            index: {
                row: 0,
                col: 0,
            }
        };
        for (let i = 0; i < ranges.length; i++) {
            let range: any = ranges[i];
            if (range.name === "frozen_rows") {
                this.freezeRange.index.row = range.count;
            } else if (range.name === "frozen_cols") {
                this.freezeRange.index.col = range.count;
            } else {
                let userRight: any = range.getPermission(userId || 0);
                console.log("setupRanges", userRight, range);
                if (range.rowEnd === -1) {
                    range.rowEnd = this.data.length - 1;
                }
                if (range.colEnd === -1) {
                    range.colEnd = this.data[0].length - 1;
                }
                for (let i: number = range.rowStart; i <= range.rowEnd; i++) {
                    for (let k: number = range.colStart; k <= range.colEnd; k++) {
                        if (!this.accessRanges[i]) {
                            this.accessRanges[i] = [];
                        }
                        if (!this.accessRanges[i][k]) {
                            this.accessRanges[i][k] = [];
                        }
                        this.accessRanges[i][k].push(userRight);
                    }
                }
            }
        }

        if (this.freezeRange.index.row > 0 && this.freezeRange.index.col > 0) {
            this.freezeRange.valid = true;
        }

        if (this.initialized) {
            this.freezePanes();
        }

    }

    // private setRect(): void {
    //     this.tableRect = this.tableEl.getBoundingClientRect();
    //     this.scrollEl.style.width = `${this.tableRect.width}px`;
    //     this.scrollEl.style.height = `${this.tableRect.height}px`;
    //     this.scrollEl.style.overflow = `hidden`;
    // }

    public generateBlankData(): any {
        let cell: any = {
            value: "",
            style: {
                width: "60px",
                height: "22px",
            },
        };
        let data: any = [];
        for (let i: number = 0; i < 10; i++) {
            data[i] = [];
            for (let k: number = 0; k < 6; k++) {
                let clone: any = _.clone(cell);
                if (i === 0 && k === 0) {
                    clone.value = "Click Me";
                }
                data[i][k] = clone;
            }
        }
        return data;
    }

    public destroy(): void {
        if (this.initialized) {
            this.helpers.removeEvent(this.containerEl, "scroll", this.onWindowScroll);
            while (this.containerEl.firstChild) {
                this.containerEl.removeChild(this.containerEl.firstChild);
            }
            this.initialized = false;
        }
    }

    public getEditCell(): any {
        return this.tableBodyEl ? this.tableBodyEl.getElementsByClassName("edit")[0] : undefined;
    }

    public addRow(index: number, data: any): void {
        this.renderRow(index, data);
    }

    public getRowIndex(): number {
        let currentCell: any = this.getEditCell();
        if (currentCell) {
            return parseInt(currentCell.parentNode.rowIndex + 1, 10);
        }
        return -1;
    }

    public addColumn(): void {
        ;
    }

    public getColumnIndex(): number {
        let currentCell: any = this.getEditCell();
        if (currentCell) {
            return parseInt(currentCell.cellIndex + 1);
        }
        return -1;
    }

    public setEditCell(el: any): any {
        let cell: any = this.getClickedCell(el);
        if (cell) {
            if (this.accessRanges && this.accessRanges[cell.parentNode.rowIndex] && this.accessRanges[cell.parentNode.rowIndex][cell.cellIndex]) {
                return false;
            }
            this.editing = true;
            this.showInput(cell);
        }
    }

    public freezePanes() {

        this.tableFrozenColsEl.innerHTML = "";
        this.tableFrozenColsEl.style.display = "none";
        this.tableFrozenRowsEl.innerHTML = "";
        this.tableFrozenRowsEl.style.display = "none";
        this.freeze = false;

        let containerRect = this.viewEl.getBoundingClientRect();
        let tableRect = this.tableEl.getBoundingClientRect();

        if ((containerRect.width >= tableRect.width && containerRect.height >= tableRect.height) || !this.freezeRange.valid) {
            return;
        }
        this.freeze = true;

        for (let i = 0; i < this.freezeRange.index.row; i++) {
            if (this.tableBodyEl.rows.length <= i) {
                continue;
            }
            this.tableFrozenRowsEl.appendChild(this.tableBodyEl.rows[i].cloneNode(true));
        }

        for (let i = 0; i < this.tableBodyEl.rows.length; i++) {
            this.tableFrozenColsEl.appendChild(this.tableBodyEl.rows[i].cloneNode(false));

            for (let j = 0; j < this.freezeRange.index.row; j++) {
                if (this.tableBodyEl.rows[i].cells.length <= j) {
                    continue;
                }

                this.tableFrozenColsEl.rows[i].appendChild(this.tableBodyEl.rows[i].cells[j].cloneNode(true));
            }
        }

        this.updateFreezePanes(this.viewEl);

    }

    private renderRow(rowId: any, rowData: any): void {

        if (!rowId || typeof rowId === "undefined") {
            rowId = -1;
        }
        this.renderCells(this.tableEl.insertRow(rowId), rowData, undefined);

    }

    private renderCells(row: any, cellsData: any, startIndex: any): void {

        if (typeof startIndex === "undefined") {
            startIndex = 0;
        }

        for (let i: number = 0; i < cellsData.length; i++) {

            // create new cell
            this.setCellValues(row.insertCell(i + startIndex), cellsData[i]);
        }
    }

    private setCellValues(cell: any, cellsData: any): void {

        // apply cell styles
        cell.removeAttribute("style");
        for (let s in cellsData.style) {

            if (!cellsData.style.hasOwnProperty(s)) {
                continue;
            }

            // Ignore styles we dont need
            let name: string = s;
            if (name.indexOf("border-") > -1) {
                let nameParts: string[] = name.split("-");
                name = `${nameParts[0]}-${nameParts[1]}`;
            }
            if (this.validStyles.indexOf(name) === -1) {
                continue;
            }

            let v: string = cellsData.style[s];

            // ensure fonts
            if (s === "font-family") {
                v += ", Arial, Helvetica, sans-serif";
            }

            // fix colors
            if ((s === "color" || s === "background-color" || s.indexOf("-color") !== -1) && !~v.indexOf("#") && !~v.indexOf("rgb")) {
                v = `#${v}`;
            }
            cell.style.setProperty(s, v);

        }

        let cellValue: string = this.getCellValue(cellsData);
        cell.setAttribute("data-value", cellValue);
        let divs: any[] = cell.getElementsByTagName("div");
        let div: any = divs.length ? divs[0] : document.createElement("div");
        let cellPos: number = cell.parentNode.rowIndex + cell.cellIndex;
        if (cellPos === 0) {
            cellPos = .5;
        }
        div.style["z-index"] = Math.round(this.numOfCells * this.numOfCells * 1 / cellPos);
        div.innerHTML = cellValue;
        div.dataset.format = cellsData.style ? cellsData.style["number-format"] || "@" : "@";
        if (this.tags) {
            let spans: any[] = cell.getElementsByTagName("span");
            let span: any = spans.length ? spans[0] : document.createElement("span");
            span.innerText = "tag";
            cell.appendChild(span);
            this.classy.addClass(div, "tag");
            div.dataset.tag = cellsData.tag || "";
        }

        // Check for links
        if (typeof cellsData.link !== "undefined" && cellsData.link) {
            let link: any = this.processLink(cellsData.link);
            let links: any = cell.getElementsByTagName("a");
            let a: any = links.length ? links[0] : document.createElement("a");
            a.setAttribute("href", link.url);
            a.setAttribute("target", link.target);
            let linkCls: string = ((!cellsData.link.external) ? "internal" : "external");
            a.setAttribute("class", linkCls);
            if (!links.length) {
                this.helpers.addEvent(a, "click", this.clickCellLink, true);
            }
            if (!links.length) {
                cell.appendChild(a);
                a.appendChild(div);
            }
            cell.setAttribute("data-link-href", link.url);
            cell.setAttribute("data-link-target", link.target);
            cell.setAttribute("data-link-class", linkCls);
        } else {
            cell.appendChild(div);
        }

        // check for access range
        if (this.accessRanges) {
            if (this.accessRanges[cell.parentNode.rowIndex] && this.accessRanges[cell.parentNode.rowIndex][cell.cellIndex]) {
                this.accessRanges[cell.parentNode.rowIndex][cell.cellIndex].forEach2(element => {
                    if (this.accessRangesClassNames[element]) {
                        this.classy.addClass(cell, this.accessRangesClassNames[element]);
                    }
                });
            }
        }

        // add tracking class
        if (this.tracking) {
            this.classy.addClass(cell, "changed");
        }

    }

    private clickCellLink = (e: any) => {

        let clickedEl: any = e.target;
        while (clickedEl && clickedEl.nodeName.toLowerCase() !== "a") {
            clickedEl = clickedEl.parentNode;
        }
        if (!clickedEl || clickedEl.nodeName.toLowerCase() !== "a") {
            return false;
        }
        let external: boolean = this.classy.hasClass(clickedEl, "external");
        // TODO: Refactor. Table class will not handle links
        if (external) {
            if (this.app.isEmbedded && this.app.externalLink) {
                window.parent.postMessage({
                    event: "link",
                    link: clickedEl.getAttribute("href"),
                }, "*");
                e.preventDefault();
            } else {
                return;
            }
        }
        if (("standalone" in window.navigator) && window.navigator.standalone) {
            e.preventDefault();
            this.$location.path(clickedEl.getAttribute("href")); // TODO: Emitter?
        }

    };

    private processLink(linkData: any): any {
        let url: string = (linkData.external) ? linkData.address : "";
        let target: string = "_blank";

        if (!linkData.external) {
            let parts: any[] = linkData.address.split("/");
            url = "domains/" + parts[0] + "/pages/" + parts[1]; // TODO: Refactor
            target = "";
        }

        // Check for special links
        if (linkData.address.indexOf("tel:") >= 0 || linkData.address.indexOf("sms:") >= 0 || linkData.address.indexOf("mailto:") >= 0) {
            target = "";
        }

        return { url: url, target: target };
    }

    private getCellValue(cellData: any, escapeHtml: any): string {

        if (typeof escapeHtml === "undefined") {
            escapeHtml = true;
        }

        // let value = "&nbsp;";
        let value: string = "&nbsp;";
        if (cellData.formatted_value && cellData.formatted_value.length) {
            value = cellData.formatted_value;
        } else if (cellData.value && cellData.value.length) {
            value = cellData.value;
        }

        // This is how we remove html - not very good
        // @todo: Should use angular"s $sce
        if (escapeHtml) {
            value = value.replace(/</g, "&lt;").replace(/</g, "&rt;");
        }

        value = value.replace(/&amp;/g, "&");

        // Handle images
        if (value.indexOf("data:image") === 0) {
            value = `<img src="${cellData.value}" />`;
        }

        return value; // .trim();
    }

    private onTableClick = (e: any) => {
        let cell: any = this.getClickedCell(e.target);

        // check for double click
        let d = new Date();
        if (this.clicked) {
            if (d.getTime() - this.clicked <= 300) {
                this.emit(this.ON_CELL_DOUBLE_CLICKED, cell);
                this.clicked = d.getTime();
                return;
            }
        }

        this.clicked = d.getTime();
        let node: string = e.target.nodeName.toLowerCase();
        if (node === "span") {
            this.emit(this.ON_TAG_CLICKED, cell);
            return;
        }
        if (this.editing) {
            this.showInput(cell);
        }
        this.emit(this.ON_CELL_CLICKED, cell);
    };

    private onWindowScroll = (e: any) => {
        if (this.freeze) {
            this.updateFreezePanes(e.target);
        }
    }

    private showInput(cell: any): void {

        if (!cell || this.classy.hasClass(cell, "edit")) {
            return;
        }

        this.classy.addClass(cell, "edit");

        let div: any = cell.getElementsByTagName("div").length ? cell.getElementsByTagName("div")[0] : undefined;

        let input: any = document.createElement("input");
        let value: any = cell.dataset.newValue || cell.dataset.value;
        input.value = value === "&nbsp;" ? "" : value;
        cell.appendChild(input);

        input.focus();
        input.selectionStart = 0;
        input.selectionEnd = 999;

        this.helpers.addEvent(input, "keydown", (e) => {
            if (e.keyCode === 13 || e.keyCode === 9 || e.keyCode === 27) {
                e.preventDefault();
            }
            // enter
            if (e.keyCode === 13) {
                if (!e.shiftKey) {
                    input.blur();
                    this.nextCellInput(cell, "down");
                }
            }
            // tab
            if (e.keyCode === 9) {
                this.nextCellInput(cell);
            }
        });

        this.helpers.addEvent(input, "blur", (e) => {
            this.classy.removeClass(cell, "edit");
            // value has changed
            if (cell.dataset.value !== input.value) {
                this.dirty = true;
                cell.setAttribute("data-new-value", input.value);
                if (div) {
                    div.innerHTML = input.value;
                }
                this.emit(this.ON_CELL_CHANGED, {
                    cell: cell,
                    value: input.value,
                    rowIndex: cell.parentNode.rowIndex,
                    colIndex: cell.cellIndex,
                });
            }
            cell.removeChild(input);
        });

        this.helpers.addEvent(input, "paste", (e) => {
            console.info("pasted");
        });
    }

    private nextCellInput(cell: any, direction: string = ""): void {
        // check if next cell
        let row: number = cell.parentNode.rowIndex;
        let col: number = cell.cellIndex;
        if (direction === "down") {
            // show next cell in row
            if (this.data[row + 1] && this.data[row + 1][col]) {
                this.showInput(this.tableBodyEl.rows[row + 1].cells[col]);
                return;
            }
        }
        // show next cell
        if (this.data[row][col + 1]) {
            this.showInput(this.tableBodyEl.rows[row].cells[col + 1]);
            return;
        }
        // show next cell in row
        if (this.data[row + 1]) {
            this.showInput(this.tableBodyEl.rows[row + 1].cells[0]);
            return;
        }
        // show start cell
        this.showInput(this.tableBodyEl.rows[0].cells[0]);
        return;
    }

    // private hideInput(cell: any): void {
    //     let divs: any[] = cell.getElementsByTagName("div");
    //     divs[0].innerHTML = cell.dataset.newValue || cell.dataset.value;
    // }

    private getClickedCell(clickedEl: any): any {

        while (clickedEl && clickedEl.nodeName.toLowerCase() !== "td") {
            clickedEl = clickedEl.parentNode;
        }

        if (!clickedEl || clickedEl.nodeName.toLowerCase() !== "td") {
            return false;
        }

        if (this.classy.hasClass(clickedEl, "edit")) {
            // return false;
        }

        return clickedEl;

    }

    private softMerges(rowEl: any): void {

        let applyWidth: any = (row, col, width) => {
            let div: any = this.tableBodyEl.rows[row].cells[col].getElementsByTagName("div")[0];
            if (width) {
                div.style.width = `${Math.round(width + 1)}px`;
                this.classy.addClass(div, "soft-merge");
            } else {
                div.style.width = `auto`;
                this.classy.removeClass(div, "soft-merge");
            }
        };

        let getTextWidth: any = (text, font) => {
            // re-use canvas object for better performance
            let canvas: any = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
            let context: any = canvas.getContext("2d");
            context.font = font;
            let metrics: any = context.measureText(text);
            return metrics.width;
        };

        // process row
        let processRow: any = (row, rowData) => {
            // let perf: any = new ipp.Performance();
            // perf.add("soft_merge_start");
            for (let i: number = 0; i < rowData.length; i++) {

                // ignore end cell
                if (i + 1 === rowData.length) {
                    applyWidth(row, i, 0);
                    continue;
                }

                // ignore if adjacent cell has value
                let adjCellVal: any = (rowData[i + 1].formatted_value || rowData[i + 1].value);
                if (adjCellVal.length) {
                    applyWidth(row, i, 0);
                    continue;
                }

                let cellData: any = rowData[i];
                let cellVal: any = (cellData.formatted_value || cellData.value);
                // let fontSize: any = parseInt(cellData.style["font-size"] || "12pt", 10) / 0.75;
                let cell: any = this.tableBodyEl.rows[row].cells[i];
                let cellWidth: any = cell.clientWidth; // parseInt(cellData.style.width, 10) || 64;

                // ignore null values
                if (!cellVal.length
                    || cellData.style["word-wrap"] === "break-word"
                    || (cellData.style.hasOwnProperty("text-align") && cellData.style["text-align"] !== "left" && cellData.style["text-align"] !== "start")
                ) {
                    applyWidth(row, i, 0);
                    continue;
                }
                // check if overflow
                let fontSize: string = cellData.style["font-size"] || "12pt";
                let fontFamily: string = cellData.style["font-family"] ? cellData.style["font-family"].replace(/"/ig, "") : "sans-serif";
                let fontWeight: string = cellData.style["font-weight"] || "normal";
                let contentWidth: number = getTextWidth(cellVal, `${fontWeight} ${fontSize} ${fontFamily}`);
                // if (!doesOverflow(cellWidth, cellVal, fontSize)) {
                if (contentWidth < cellWidth) {
                    applyWidth(row, i, 0);
                    continue;
                }

                // how far to overflow
                // let contentWidth: number = cellVal.length * fontSize;
                let mergeWidth: number = cellWidth;
                for (let k: number = i + 1; k < rowData.length; k++) {
                    let adjCellWidth: any = this.tableBodyEl.rows[row].cells[k].clientWidth; // parseInt(rowData[k].style.width, 10) || 64;
                    mergeWidth += adjCellWidth;
                    // end cell
                    if (k + 1 === rowData.length) {
                        contentWidth = mergeWidth;
                        break;
                    }
                    // check if adjacent cell has a value
                    let adjCellVal: any = (rowData[k].formatted_value || rowData[i + 1].value).trim();
                    if (adjCellVal.length) {
                        if (contentWidth > mergeWidth) {
                            contentWidth = mergeWidth;
                        }
                        break;
                    }
                    // reached limit
                    if (mergeWidth > contentWidth) {
                        break;
                    }
                }

                // apply width
                applyWidth(row, i, contentWidth);

            }
            // perf.add("soft_merge_end");
            // console.log("Soft merge execution finished in %s ms for row %s", perf.get("soft_merge_end").time, row);
        };

        let getRowIndex: any = (el) => {
            return el.rowIndex - this.tableHeadEl.rows.length;
        };

        if (rowEl) {
            processRow(getRowIndex(rowEl), rowEl);
        } else {
            for (let i: number = 0; i < this.data.length; i++) {
                processRow(i, this.data[i]);
            }
        }

    }

    private updateFreezePanes(target) {
        console.log(target);
        if (target.scrollTop > 0) {
            this.tableFrozenRowsEl.style.top = `${target.scrollTop}px`;
            this.tableFrozenRowsEl.style.display = `table`;
        } else {
            this.tableFrozenRowsEl.style.top = `0px`;
            this.tableFrozenRowsEl.style.display = `none`;
        }
        if (target.scrollLeft > 0) {
            this.tableFrozenColsEl.style.left = `${target.scrollLeft}px`;
            this.tableFrozenColsEl.style.display = `table`;
        } else {
            this.tableFrozenColsEl.style.left = `0px`;
            this.tableFrozenColsEl.style.display = `none`;
        }
    }

    private clearCellClassNames(classNames: any): void {

        if (!this.data || !this.data.length) {
            return;
        }
        for (let i: number = 0; i < this.data.length; i++) {
            for (let k: number = 0; k < this.data[i].length; k++) {
                if (this.tableBodyEl.rows[i] && this.tableBodyEl.rows[i].cells[k]) {
                    classNames.forEach2(element => {
                        this.classy.removeClass(this.tableBodyEl.rows[i].cells[k], element);
                    });
                }
            }
        }

    }

}

export default Table;