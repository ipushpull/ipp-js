class Timers {
    public funcs = [];
    public set(func, delay) {
        this.funcs.push(setTimeout(func, delay));
    }
    public cancel(func) {
        let i = this.funcs.indexOf(func);
        if (i > -1) {
            clearTimeout(this.funcs[i]);
            this.funcs[i] = undefined;
        }
    }
}
let timersProvider = new Timers();
function timers(func, delay) {
    timersProvider.set(func, delay);
}
timers.cancel = function (func) {
    timersProvider.cancel(func);
};

class Intervals {
    public funcs = [];
    public set(func, delay) {
        this.funcs.push(setInterval(func, delay));
    }
    public cancel(func) {
        let i = this.funcs.indexOf(func);
        if (i > -1) {
            clearTimeout(this.funcs[i]);
            this.funcs[i] = undefined;
        }
    }
}
let intervalsProvider = new Intervals();
function intervals(func, delay) {
    intervalsProvider.set(func, delay);
}
intervals.cancel = function (func) {
    intervalsProvider.cancel(func);
};

// exports.Intervals = intervals;

export {timers, intervals};
