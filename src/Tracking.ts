import * as _ from 'underscore';
import Utils, { IUtils } from './Utils';

interface IData {
  content_diff: any;
  modified_by: any;
  modified_by_timestamp: any;
}

export class Tracking {
  public static $inject: string[] = ['$interval', 'ippStorageService'];

  public history: any = {};
  public historyFirst: any = {};

  private _enabled: boolean = false;
  private _interval: any;
  private _length: number = 0;
  private _utils: IUtils;

  constructor(private $interval: any, private storage: any) {
    this._utils = new Utils();
  }

  public createInstance() {
    return new Tracking(this.$interval, this.storage);
  }

  get enabled(): boolean {
    return this._enabled;
  }

  set enabled(value: boolean) {
    if (typeof value !== 'boolean') {
      return;
    }
    this._enabled = value;
    this.cancel();
    if (!value) {
      this.reset();
    } else {
      this.restore();
      this._length = this.totalChanges();
      this._interval = this.$interval(() => {
        let length: number = this.totalChanges();
        if (this._length !== length) {
          this.storage.user.save('history', JSON.stringify(this.history));
        }
        this._length = length;
      }, 10000);
    }
  }

  public addHistory(pageId: number, data: IData, first: boolean = false): void {
    if (!this.enabled) {
      return;
    }
    if (first && (this.history[`page_${pageId}`] && this.history[`page_${pageId}`].length)) {
      return;
    }
    if (!this.history[`page_${pageId}`]) {
      this.history[`page_${pageId}`] = [];
    }
    this.history[`page_${pageId}`].push(this.compressData(data, first));
  }

  public getHistory(pageId: number): any {
    if (!this.history[`page_${pageId}`]) {
      return [];
    }
    return this.history[`page_${pageId}`];
  }

  public getHistoryForCell(pageId: number, cellRow: number, cellCol: number): any {
    let history = this.getHistory(pageId);
    if (!history.length) {
      return [];
    }
    let cellHistory = [];
    history.forEach2(snapshot => {
      snapshot.content_diff.forEach2((row, rowIndex) => {
        if (!row) {
          return;
        }
        row.forEach2((col, colIndex) => {
          if (!col) {
            return;
          }
          if (cellRow === rowIndex && cellCol === colIndex) {
            cellHistory.push({
              cell: JSON.parse(JSON.stringify(col)),
              modified_by: snapshot.modified_by,
              modified_by_timestamp: snapshot.modified_by_timestamp,
            });
          }
        });
      });
    });
    return cellHistory;
  }

  public restore(): any {
    this.history = this.storage.user.get('history') || {};
    // if (history) {
    //     this.history = JSON.parse(history);
    // }
  }

  public reset(): any {
    this.history = {};
    this.storage.user.remove('history');
  }

  public cancel(): void {
    if (this._interval) {
      this.$interval.cancel(this._interval);
    }
  }

  private totalChanges(): number {
    let length: number = 0;
    for (let id in this.history) {
      if (!this.history.hasOwnProperty(id)) {
        continue;
      }
      length += this.history[id].length;
    }
    return length;
  }

  private compressData(data: IData, first: boolean = false): IData {
    if (first) {
      data.content_diff = this._utils.clonePageContent(data.content_diff);
    }
    return JSON.parse(JSON.stringify(data));
  }
}

export default Tracking;

// export class TrackingWrap {
//     public static $inject: string[] = [];
//     constructor() {
//         return Tracking;
//     }
// }
