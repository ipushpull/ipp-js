import * as Q from "q";
import * as query from "querystring";
import * as merge from "merge";
import * as req from "request";

import { IIPPConfig } from "./Config";

import { Config } from "./Config";
// import { StorageService } from "./Storage";
import { Api } from "./Api";
// import { Auth } from "./Auth";
// import Classy from "./Classy";
import Utils from "./Utils";
// import Helpers from "./Helpers";
// import Table from "./Table";
// import { Grid } from "./Grid";
// import { Clipboard } from "./Clipboard";
// import PdfWrap from "./Pdf";
// import { Tracking } from "./Tracking";
// import { Crypto } from "./Crypto";
import { timers } from "./Timers";
import { intervals } from "./Timers";
// import { intervals } from "./Timers";
import { PageWrap } from "./Page/Page";
// import { FreezingRange } from "./Page/Page";
// import { PermissionRange } from "./Page/Page";

Array.prototype.forEach2 = function (a) {
    var l = this.length;
    for (var i = 0; i < l; i++)a(this[i], i)
}

class IPushPull {

    public config;
    // public storage;
    // public crypto;
    public api;
    public classy;
    // public helpers;
    // public table;
    // public grid;
    // public pdf;
    public auth;
    public utils;
    public page;
    // public tracking;
    // public clipboard;
    // public freezingRange;
    // public permissionRange;

    // public test;

    constructor(public settings: IIPPConfig) {

        this.config = new Config();
        this.config.set(settings);
        // this.crypto = new Crypto();

        // static classes
        // this.classy = new Classy();
        // this.helpers = new Helpers();
        this.utils = new Utils();

        // this.storage = new StorageService(this.config);
        this.api = new Api(query.stringify, Q, undefined, this.config, this.utils, req);
        // this.auth = new Auth(Q, timers, this.api, undefined, this.config, this.utils);

        this.page = new PageWrap(Q, timers, intervals, this.api, this.auth, undefined, undefined, this.config);
        // this.tracking = new Tracking(intervals, this.storage);
        // this.freezingRange = FreezingRange;
        // this.permissionRange = PermissionRange;

        // this.table = Table;

        // this.grid = Grid;
        // this.pdf = new PdfWrap(Q, this.storage, this.config);
        // this.clipboard = Clipboard;

    }

}

function createInstance(defaultConfig) {
    return new IPushPull(defaultConfig);
}

let ipushpull = createInstance();
ipushpull.IPushpull = IPushPull;
ipushpull.create = function (instanceConfig) {
    return createInstance(merge({
        api_url: "http://ipushpull.local/api/1.0",
        ws_url: "http://ipushpull.local",
        storage_prefix: "ipp_local",
        api_key: "LC8aOmGqqydL3n8HyKaz8ue34M13WPux2zyuBAl1",
        api_secret: "kXXRR7jsoEm6JgjvBDYGca1YlGIDynuxpM9kiu9GTlqzLtmCvNWLm3pmzWUc6Uptp4DwvS1kLlC6mh5PHGGnKRNh0eBPG1IIUB5NtsEY1oSgqLcteNCdGBLnIwnOO7SY",
        transport: "polling", // polling or leave empty to use websockets
    }, instanceConfig));
};

module.exports = ipushpull;
module.exports.defaults = ipushpull;