"use strict";

import { Config } from "./Config";
import { StorageService } from "./Storage";
import { Api } from "./Api";
import { Auth } from "./Auth";
import Classy from "./Classy";
import Utils from "./Utils";
import Helpers from "./Helpers";
import { GridWrap } from "./Grid";
import { ClipboardWrap } from "./Clipboard";
import PdfWrap from "./Pdf";
import Tracking from "./Tracking";
import { Crypto } from "./Crypto";
import { Request } from "./Request";
import { PageWrap } from "./Page/Page";
import { FreezingRange } from "./Page/Range";
import { PermissionRange } from "./Page/Range";
import { ButtonRange } from "./Page/Range";
import { StyleRange } from "./Page/Range";
import { NotificationRange } from "./Page/Range";
import { AccessRange } from "./Page/Range";

Array.prototype.forEach2 = function (a) {
    var l = this.length;
    for (var i = 0; i < l; i++)a(this[i], i)
}

angular.module("ipushpull", [])
    .provider("ippConfig", Config)

    .service("ippApiService", Api)
    .service("ippPageService", PageWrap)
    .factory("ippStorageService", StorageService)
    .service("ippAuthService", Auth)
    .factory("ippCryptoService", Crypto._instance)
    .factory("ippReqService", Request._instance)

    .service("ippClassyService", Classy)
    .service("ippUtilsService", Utils)
    .service("ippHelpersService", Helpers)

    .factory("ippClipboardService", ClipboardWrap)
    .factory("ippGridService", GridWrap)
    .factory("ippPdfService", PdfWrap)
    .service("ippTrackingService", Tracking);

let ipushpull = {
    FreezingRange: FreezingRange,
    PermissionRange: PermissionRange,
    ButtonRange: ButtonRange,
    StyleRange: StyleRange,
    NotificationRange: NotificationRange,
    AccessRange: AccessRange,
};

module.exports = ipushpull;
module.exports.defaults = ipushpull;