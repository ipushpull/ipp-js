'use strict';

import * as Q from 'q';
import * as query from 'querystring';
import * as merge from 'merge';
import * as request from 'xhr';

import { IIPPConfig } from './Config';

import { Config } from './Config';
import { StorageService } from './Storage';
import { Api } from './Api';
import { Auth } from './Auth';
import Classy from './Classy';
import Utils from './Utils';
import Helpers from './Helpers';
// import Table from "./Table";
// import { Grid } from "./Grid";
// import { Grid as Grid2 } from "./Grid/Grid";
import { Grid } from './Grid2/Grid';
import { Clipboard } from './Clipboard';
import PdfWrap from './Pdf';
import { Tracking } from './Tracking';
import { Crypto } from './Crypto';
import { timers } from './Timers';
import { intervals } from './Timers';
// import { intervals } from "./Timers";
import { PageWrap } from './Page/Page';
import { FreezingRange } from './Page/Range';
import { PermissionRange } from './Page/Range';
import { ButtonRange } from './Page/Range';
import { StyleRange } from './Page/Range';
import { NotificationRange } from './Page/Range';
import { AccessRange } from './Page/Range';
import { Functions } from './Functions';

Array.prototype.forEach2 = function(a) {
  var l = this.length;
  for (var i = 0; i < l; i++) a(this[i], i);
};
// Production steps of ECMA-262, Edition 6, 22.1.2.1
if (!Array.from) {
  Array.from = (function() {
    var toStr = Object.prototype.toString;
    var isCallable = function(fn) {
      return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    };
    var toInteger = function(value) {
      var number = Number(value);
      if (isNaN(number)) {
        return 0;
      }
      if (number === 0 || !isFinite(number)) {
        return number;
      }
      return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    };
    var maxSafeInteger = Math.pow(2, 53) - 1;
    var toLength = function(value) {
      var len = toInteger(value);
      return Math.min(Math.max(len, 0), maxSafeInteger);
    };

    // The length property of the from method is 1.
    return function from(arrayLike /*, mapFn, thisArg */) {
      // 1. Let C be the this value.
      var C = this;

      // 2. Let items be ToObject(arrayLike).
      var items = Object(arrayLike);

      // 3. ReturnIfAbrupt(items).
      if (arrayLike == null) {
        throw new TypeError('Array.from requires an array-like object - not null or undefined');
      }

      // 4. If mapfn is undefined, then let mapping be false.
      var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
      var T;
      if (typeof mapFn !== 'undefined') {
        // 5. else
        // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
        if (!isCallable(mapFn)) {
          throw new TypeError('Array.from: when provided, the second argument must be a function');
        }

        // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
        if (arguments.length > 2) {
          T = arguments[2];
        }
      }

      // 10. Let lenValue be Get(items, "length").
      // 11. Let len be ToLength(lenValue).
      var len = toLength(items.length);

      // 13. If IsConstructor(C) is true, then
      // 13. a. Let A be the result of calling the [[Construct]] internal method
      // of C with an argument list containing the single item len.
      // 14. a. Else, Let A be ArrayCreate(len).
      var A = isCallable(C) ? Object(new C(len)) : new Array(len);

      // 16. Let k be 0.
      var k = 0;
      // 17. Repeat, while k < len… (also steps a - h)
      var kValue;
      while (k < len) {
        kValue = items[k];
        if (mapFn) {
          A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        } else {
          A[k] = kValue;
        }
        k += 1;
      }
      // 18. Let putStatus be Put(A, "length", len, true).
      A.length = len;
      // 20. Return A.
      return A;
    };
  })();
}
if (typeof Object.assign != 'function') {
  // Must be writable: true, enumerable: false, configurable: true
  Object.defineProperty(Object, 'assign', {
    value: function assign(target, varArgs) {
      // .length of function is 2
      'use strict';
      if (target == null) {
        // TypeError if undefined or null
        throw new TypeError('Cannot convert undefined or null to object');
      }

      var to = Object(target);

      for (var index = 1; index < arguments.length; index++) {
        var nextSource = arguments[index];

        if (nextSource != null) {
          // Skip over if undefined or null
          for (var nextKey in nextSource) {
            // Avoid bugs when hasOwnProperty is shadowed
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
      return to;
    },
    writable: true,
    configurable: true,
  });
}

class IPushPull {
  public config;
  public storage;
  public crypto;
  public api;
  public classy;
  public helpers;
  // public table;
  public Grid;
  // public Grid2;
  public Pdf;
  public auth;
  public utils;
  public Page;
  public tracking;
  public Clipboard;
  public FreezingRange;
  public PermissionRange;
  public ButtonRange;
  public StyleRange;
  public NotificationRange;
  public AccessRange;
  public Functions;

  public Create;
  public IPushpull;

  constructor(public settings: IIPPConfig) {
    this.config = new Config();
    this.config.set(settings);
    this.crypto = new Crypto();

    // static classes
    this.classy = new Classy();
    this.helpers = new Helpers();
    this.utils = new Utils();

    this.storage = new StorageService(this.config);
    this.api = new Api(query.stringify, Q, this.storage, this.config, this.utils, request);
    this.auth = new Auth(Q, timers, this.api, this.storage, this.config, this.utils);

    this.Page = new PageWrap(Q, timers, intervals, this.api, this.auth, this.storage, this.crypto, this.config);
    this.tracking = new Tracking(intervals, this.storage);
    this.FreezingRange = FreezingRange;
    this.PermissionRange = PermissionRange;
    this.ButtonRange = ButtonRange;
    this.StyleRange = StyleRange;
    this.NotificationRange = NotificationRange;
    this.AccessRange = AccessRange;
    this.Functions = Functions;

    // this.table = Table;

    // this.grid = Grid;
    this.Grid = Grid;
    // this.Grid2 = Grid3;
    this.Pdf = new PdfWrap(Q, this.storage, this.config);
    this.Clipboard = Clipboard;
  }
}

function createInstance(defaultConfig) {
  return new IPushPull(defaultConfig);
}

let ipushpull = createInstance();
ipushpull.IPushpull = IPushPull;
ipushpull.Create = function(instanceConfig) {
  return createInstance(
    merge(
      {
        api_url: 'http://ipushpull.dev/api/1.0',
        ws_url: 'http://ipushpull.dev',
        storage_prefix: 'ipp_local',
        api_key: 'LC8aOmGqqydL3n8HyKaz8ue34M13WPux2zyuBAl1',
        api_secret:
          'kXXRR7jsoEm6JgjvBDYGca1YlGIDynuxpM9kiu9GTlqzLtmCvNWLm3pmzWUc6Uptp4DwvS1kLlC6mh5PHGGnKRNh0eBPG1IIUB5NtsEY1oSgqLcteNCdGBLnIwnOO7SY',
        transport: 'polling', // polling or leave empty to use websockets
      },
      instanceConfig,
    ),
  );
};

module.exports = ipushpull;
module.exports.defaults = ipushpull;
