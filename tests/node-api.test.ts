import * as ipushpull from "../src/index-api";

global.window = {
    WebSocket: true,
};
global.io = require("socket.io")();

let ipp = new ipushpull.create();
let page = new ipp.page(1, 1);
console.log(ipp.config);
let pageReady = false;
page.on(page.EVENT_READY, (data) => {
    console.log("ready");
    pageReady = true;
});
page.on(page.EVENT_NEW_CONTENT, (data) => {
    if (!pageReady) {
        return;
    }
    console.log("new");
});
