var webpack = require('webpack');
var config = {};

function generateConfig(name) {
  var uglify = name.indexOf('min') > -1;
  var suffix = '';
  if(name.indexOf('ng-') > -1) suffix = '-ng';
  // if(name.indexOf('-api') > -1) suffix = '-api';
  var config = {
    entry: './js/src/index'+suffix+'.js',
    output: {
      path: __dirname + '/dist/',
      filename: name + '.js',
      sourceMapFilename: name + '.map',
      library: 'ipushpull',
      libraryTarget: 'umd'
    },
    node: {
      process: false
    },
    devtool: 'source-map'
  };

  config.plugins = [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': "production"
    })
  ];

  if (uglify) {
    config.plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false,
          drop_console: true
        }
      })
    );
  }

  return config;
}

['ipushpull', 'ipushpull.min', 'ng-ipushpull', 'ng-ipushpull.min'].forEach(function (key) {
  config[key] = generateConfig(key);
});

module.exports = config;